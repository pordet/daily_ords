function draw_paging_by_ajax(p){
	var page = p.page;
	var range = Math.floor((page-1)/10);
	var firstPage = range*10+1;
	var lastPage = (range+1)*10;
	var totPage = p.totalPages;
	if(totPage<lastPage){
		lastPage = totPage;
	}
	var html='<ul>';
	if(totPage > 10){
		html+='<li><a href="javascript:void(0)" onclick="javascript:doList(1);">First</a></li>';
	}
	if(firstPage !=1){
		html+='<li><a href="javascript:void(0)" onclick="javascript:doList('+(firstPage-1)+');">'+(firstPage-1)+'</a></li>';
	}
	for(var i = firstPage;i<lastPage+1;i++){
		if(page == i){
			html+='<li class="on"><a href="javascript:void(0)" onclick="javascript:doList('+i+');">'+i+'</a></li>';
		}else{
			html+='<li><a href="javascript:void(0)" onclick="javascript:doList('+i+');">'+i+'</a></li>';
		}
	}
	if(lastPage < totPage){
		html+='<li><a href="javascript:void(0)" onclick="javascript:doList('+(lastPage+1)+');">>>'+(lastPage+1)+'</a></li>';
		html+='<li><a href="javascript:void(0)" onclick="javascript:doList('+totPage+');">Last</a></li>';
	}
	html+='</ul>';
	$(".table-paging").html(html);
}

