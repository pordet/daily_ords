<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${map.meta_m_nm} 등록"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
			<c:choose>
				<c:when test="${empty meta_row}">
					<span id="curr_title">${title}</span>
				</c:when>
				<c:otherwise>
					<span id="curr_title">${map.meta_m_nm} 수정</span>
				</c:otherwise>
			</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="listFrm" name="listFrm" method="post">
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
                    <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}">
                    <input type="hidden" name="id_bid_noti" id="id_bid_noti" value="${param.id_bid_noti}">
                    <input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
				  	<input type="hidden" name="trg_meta_m" id="trg_meta_m" value="${param.trg_meta_m}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" value="${param.page}">
					<input type="hidden" name="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" value="${param.sord}"/>
				</form>
				<form id="frm" name="frm" method="post">
                    <input type="hidden" id="doubleYn" value="Y">
                    <input type="hidden" id="checkYn" value="N">
                    <input type="hidden" name="id_meta_row" id="id_meta_row" value="${meta_row.id_meta_row}">
                    <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}">
                    <input type="hidden" name="id_bid_noti" id="id_bid_noti" value="${param.id_bid_noti}">
                    <input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
				  	<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(meta_row eq null ? 'Y':'N')}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" id="page" value="${param.page}">
					<c:set var="curr_row_num" value="1"/>
					<c:set var="curr_col_num" value="1"/>
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="15%">
								<col width="30%">
								<col width="15%;">
								<col width="35%">
							</colgroup>
							<tbody>
                                <c:forEach var="row" items="${colList}" varStatus="i">
<%--                                 <c:if test="${i.count==1}"> --%>
									<c:choose>
										<c:when test="${row.typ == GlobalConst.DT_GRP_HEAD}">
			                                <commc:DynamicRegTrTag paramRowNum="${curr_row_num}" paramColNum="${curr_col_num}" row="${row}">
												<ctrl:dynamicRegChildTr child_list="${childList}" curr_row_num="${currRowNum}" curr_col_num="1"/>
											</commc:DynamicRegTrTag>
			 			 					<c:set var="curr_row_num" value="${currRowNum}"/>
											<c:set var="curr_col_num" value="${currColNum}"/>
										</c:when>
										<c:otherwise>
			                                <commc:DynamicRegTrTag paramRowNum="${curr_row_num}" paramColNum="${curr_col_num}" row="${row}">
											<ctrl:dynamicRegTc col="${col}" curr_row_num="${currRowNum}" curr_col_num="${currColNum}"/>
											</commc:DynamicRegTrTag>
								
			 			 					<c:set var="curr_row_num" value="${currRowNum}"/>
											<c:set var="curr_col_num" value="${currColNum}"/>
										</c:otherwise>
									</c:choose>
<%--                                 </c:if> --%>
<%--  			 					<c:set var="curr_row_num" value="${commc:DynamicRegTrTag.currRowNum}"/> --%>
<%-- 								<c:set var="curr_col_num" value="${commc:DynamicRegTrTag.currColNum}"/> --%>
<%-- 								[${curr_row_num}][${curr_col_num}]///[${commc:DynamicRegTrTag.currRowNum}][${commc:DynamicRegTrTag.currColNum}] --%>
                                
                                </c:forEach>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<c:if test="${!is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" id="btnInit">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete" onclick="javascript:delMeta();">
									삭제
								</button>
								<button type="button" class="btn-primary" onclick="javascript:register();">
									저장
								</button>
							</div>
						</c:if>
						<c:if test="${is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
						</c:if>
					</div>
				</form>
			</div>
	<script>
	function doList(page){
		$("#page").val(page);
		var frm =document.frm;
		var sys_code = $("#sys_code").val();
		if(sys_code){
			frm.action=contextPath+"/front/bid/bidNotiList.do";
		}else{
			frm.action=contextPath+"/front/meta/metaList.do";
		}
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function openRegContreg(id_bid_noti){
		$("#id_bid_noti").val(id_bid_noti);
		var frm =document.frm;
			frm.action=contextPath+"/front/meta/regMetaData.do";
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function delMeta(){
		var frm =document.frm;
			frm.action=contextPath+"/front/meta/deleteMetaData.do";
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function register(){
		var params = $("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/front/meta/insertMetaDataAjax.do";
		}else{
			$("#sort_order").val($("#sort_order_dis").val());
			url = contextPath+"/front/meta/modifyMetaDataAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		if(insert_yn =='N'){
						if(nanToStr(e.err_msg)!=''){
		    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
						}else{
		    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
						}
					}else{
						if(nanToStr(e.err_msg)!=''){
		    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
						}else{
		    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
						}
					}
		    	}else{
		    		if(insert_yn =='Y'){
			    		$("#insert_yn").val('N');
			    		$("#curr_title").html('${map.meta_m_nm} 수정');
			    		$("#id_meta_row").val(e.id_meta_row);
			    		$.each(e.insDataList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_id_meta_"'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
			    			html+='<input type="hidden" name="i_id_data_"'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
			    			if(d.typ=='${GlobalConst.DT_CALC}'){
			    				$("#id_meta_"+d.id_meta).val(d.val);
			    			}
			    			$("#id_meta_"+d.id_meta).append(html);
			    		});
		    		}else{
			    		$.each(e.insList,function(i,d){
			    			var html="";
			    			$("#id_meta_"+d.id_meta).val(d.val);
			    			html+='<input type="hidden" name="i_id_meta_"'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
			    			html+='<input type="hidden" name="i_id_data_"'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
			    			$("#id_meta_"+d.id_meta).append(html);
			    		});
			    		$.each(e.delList,function(i,d){
			    			$("#id_meta_"+d).val('');
			    			$("#i_id_meta_"+d).remove();
			    			$("#i_id_data_"+d).remove();
			    		});
			    		$.each(e.modList,function(i,d){
			    			var html="";
			    			if(d.typ =='${GlobalConst.DT_STR}' && d.len_type >${GlobalConst.DLT_1}){
				    			$("#id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_data_"+d.id_meta).val(d.id_data);
			    			}else{
				    			$("#id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_data_"+d.id_meta).val(d.id_data);
			    			}
			    		});
		    			
		    		}
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
	</script>

</front:FrontDynamicBody>
</html>