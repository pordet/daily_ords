<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="adm_commc" uri="/WEB-INF/tlds/adm-commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="hasNotCommonPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="hasLeft" required="false" %>
<%@ attribute name="hasImgTop" required="false" %>
<%@ attribute name="hasMenu" required="false" %>
<%@ attribute name="currLocale" required="false" %>
<%@ attribute name="hasHeader" required="false" %>
<%@ attribute name="treeType" required="false" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<c:if test="${empty id_user}">
	<jsp:forward page="/"/>
</c:if>
<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
	<div class="spinner" id="spinnerDiv" style="display:none;">
       <div class="in"><img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/></div>
   </div>
    <div class="header-wrap">
        <front:FrontDynamicMainTop currLocale="${currLocale}" title="${title}"/>
    </div>    
    <div class="content-wrap">
    	<div class="adm-content">
	        <c:if test="${not empty hasLeft}">
	        	<adm:AdmBodyMenu currLocale="${currLocale}"/>
	        </c:if>
			<div class="admin-main-area">
				<jsp:doBody/>
			</div>
		</div>
	</div>
	<div id="tooltip"></div>
    <!-- // mh-wrapper -->
    
    <!-- //spinner  -->
<front:FrontBaseTail currLocale="${currLocale}" hasPop="true"/>