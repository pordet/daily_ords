package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface IdentityMetaDAO {

	PageList<CustomMap> selectIdentityMetaList(Map<String, Object> map, PageBounds pageBounds);

	int getMaxDepthAtIdentity(Map<String, Object> map);

	List<CustomMap> selectTopDepthIdentityListForHeader(Map<String, Object> map);

	List<CustomMap> selectDepthIdentityListRemoveTopDepthForHeader(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaRelByMetaIdMetaM(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaFKBySlaveMeta(List<Integer> subKeyList);

	List<CustomMap> selectSubIdentityMetaColByKeyList(List<Integer> subKeyList);

	List<CustomMap> selectIdentityMetaRelList(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaDetailParamList(Map<String, Object> map);

	PageList<CustomMap> selectIdentityMetaDataContents(Map<String, Object> map, PageBounds pageBounds);

	PageList<CustomMap> selectTreeIdentityMetaDataContents(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectIdentityMetaDataForSearch(List<Integer> lowKey);

	CustomMap getIdentityMetaRowByKey(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaDataByIdMetaRow(Map<String, Object> map);

	List<CustomMap> selectIdentityMeta(Map<String, Object> map);

	List<CustomMap> selectIdentity(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaTabSearch(List<Map<String, Object>> params);

	List<CustomMap> selectIdentityMetaTabSearchList(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaRowForTabCode(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaCodeList(Map<String, Object> map);

	void modifyIdentityMetaRow(Map<String, Object> map);

	List<CustomMap> selectCalcuListByParam(Map<String, Object> map);

	void insertIdentityMetaRow(Map<String, Object> map);
	
	void insertMetaDataByGroup(List<Map> inInputList);

	void updateMetaData(Map<String, Object> map);

	void deleteMetaDataByList(List<Integer> delList);

	List<CustomMap> selectIdentityMetaDataByIdRow(int int1);

	void deleteIdentityMetaData(Map<String, Object> map);

	void deleteIdentityMetaRow(Map<String, Object> map);

	CustomMap getIdentityMetaIdMetaM(Map<String, Object> map);


}
