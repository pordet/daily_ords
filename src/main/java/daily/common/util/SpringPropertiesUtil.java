package daily.common.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.MessageSource;

public class SpringPropertiesUtil extends PropertyPlaceholderConfigurer {

	private Logger log = LoggerFactory.getLogger(SpringPropertiesUtil.class);
	@Autowired MessageSource messageSource;
	
    private static Map<String, String> propertiesMap;
    // Default as in PropertyPlaceholderConfigurer
    private int springSystemPropertiesMode = SYSTEM_PROPERTIES_MODE_FALLBACK;

    @Override
    public void setSystemPropertiesMode(int systemPropertiesMode) {
        super.setSystemPropertiesMode(systemPropertiesMode);
        springSystemPropertiesMode = systemPropertiesMode;
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException{
        super.processProperties(beanFactory, props);

        propertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String valStr = resolvePlaceholder(keyStr, props, springSystemPropertiesMode);
            String valueStr="";
			try {
				valueStr = new String(valStr.getBytes("ISO-8859-1"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.debug("Properties 파일에 대해 UnsupportedEncodingException 가 발생");
			}
//			System.out.println("keyStr:"+keyStr+",value:"+valueStr);
            propertiesMap.put(keyStr, valueStr);
        }
    }

    public static String getProperty(String name) {
    	if(propertiesMap.get(name)==null) {
    		return "";
    	}else {
            return propertiesMap.get(name).toString();
    	}
    }

    public static String getProperty(String name,Locale locale) {
     	MessageSource message = SpringApplicationContext.getBean( "messageSource", MessageSource.class );
    	return message.getMessage(name, null, locale);
    }

}