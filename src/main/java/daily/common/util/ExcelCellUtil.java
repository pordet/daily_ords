package daily.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.NumberToTextConverter;


public class ExcelCellUtil {
	// 수직 병합 처리를 위한 메서드
//    public static int getRowspan(Sheet sheet, int rowIndex, int cellIndex) {
//        int rowspan = 1; // rowspan 기본값은 1
//
//        // 현재 셀의 스타일을 가져옴
//        Cell cell = sheet.getRow(rowIndex).getCell(cellIndex);
//        CellStyle style = cell.getCellStyle();
//
//        if (style != null) {
//            // 현재 셀의 스타일에서 rowspan 정보를 가져옴
//            rowspan = style.getVerticalAlignment().equals(VerticalAlignment.CENTER) ? cell.getRowIndex() : 1;
//        }
//
//        return rowspan;
//    }
 // 수평 병합 처리를 위한 메서드
//    public static int getColspan(Sheet sheet, int rowIndex, int cellIndex) {
//        int colspan = 1; // colspan 기본값은 1
//
//        // 현재 셀의 스타일을 가져옴
//        Cell cell = sheet.getRow(rowIndex).getCell(cellIndex);
//        CellStyle style = cell.getCellStyle();
//
//        if (style != null) {
//            // 현재 셀의 스타일에서 colspan 정보를 가져옴
//            colspan = style.getAlignment().equals(HorizontalAlignment.CENTER) ? cell.getColSpan() : 1;
//        }
//
//        return colspan;
//    }
    public static  CellRangeAddress findHorizontalMergedRegion(Sheet sheet, int rowIndex, int cellIndex) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            if (mergedRegion.isInRange(rowIndex, cellIndex)) {
                return mergedRegion;
            }
        }
        return null;
    }
 // 수직 병합된 셀 정보 확인을 위한 메서드
    public static CellRangeAddress findVerticalMergedRegion(Sheet sheet, int rowIndex, int cellIndex) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            if (mergedRegion.isInRange(rowIndex, cellIndex) && (mergedRegion.getFirstRow() != rowIndex)) {
                return mergedRegion;
            }
        }
        return null;
    }
 // 수평 병합 처리를 위한 메서드
    public static int getColspan(Sheet sheet, int rowIndex, int cellIndex) {
        int colspan = 1;

        // 현재 셀의 정보 가져오기
        Cell cell = sheet.getRow(rowIndex).getCell(cellIndex);
        CellRangeAddress cra = ExcelCellUtil.getMergedRegion(sheet, rowIndex, cellIndex);
        // 현재 셀이 수평 병합된 셀인지 확인
        if (cra!=null) {
            // 수평 병합된 셀의 경우, 병합된 범위 정보를 가져와서 열 수 계산
            int firstColumn = cra.getFirstColumn();
            int lastColumn = cra.getLastColumn();
            colspan = lastColumn - firstColumn + 1;
        }

        return colspan;
    }
//  수직 병합 처리를 위한 메서드
    public static int getRowspan(Sheet sheet, int rowIndex, int cellIndex) {
        int rowspan = 1;

        // 현재 셀의 정보 가져오기
        Cell cell = sheet.getRow(rowIndex).getCell(cellIndex);

        // 현재 셀이 수직 병합된 셀인지 확인
        if (cell != null && ExcelCellUtil.isMergedCell(sheet,rowIndex,cellIndex)) {
            // 수직 병합된 셀의 경우, 병합된 범위 정보를 가져와서 행 수 계산
            CellRangeAddress mergedRegion = getMergedRegion(sheet, rowIndex, cellIndex);
            if (mergedRegion != null) {
                int firstRow = mergedRegion.getFirstRow();
                int lastRow = mergedRegion.getLastRow();
                rowspan = lastRow - firstRow + 1;
            }
        }

        return rowspan;
    }

    // 셀의 수직 병합 정보 가져오기
    public static  CellRangeAddress getMergedRegion(Sheet sheet, int rowIndex, int cellIndex) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            if (mergedRegion.isInRange(rowIndex, cellIndex)) {
                return mergedRegion;
            }
        }
        return null;
    }

 // 현재 셀이 병합된 셀인지 확인하는 메서드
    public static boolean isMergedCell(Sheet sheet, int rowIndex, int cellIndex) {
        for (CellRangeAddress mergedRegion : sheet.getMergedRegions()) {
            if (mergedRegion.isInRange(rowIndex, cellIndex)) {
                return true;
            }
        }
        return false;
    }
    public static boolean isFirstMergedCell(Sheet sheet, int rowIndex, int cellIndex) {
        // 주어진 행 및 열 인덱스에 대한 셀 가져오기
        Row row = sheet.getRow(rowIndex);
        Cell cell = row.getCell(cellIndex);

        // 셀이 병합된 셀 영역의 첫 번째 셀인지 확인
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            if (mergedRegion != null && mergedRegion.isInRange(rowIndex, cellIndex)) {
                int firstRow = mergedRegion.getFirstRow();
                int firstCol = mergedRegion.getFirstColumn();
                return (rowIndex == firstRow && cellIndex == firstCol);
            }
        }

        // 병합된 셀이 아닌 경우
        return false;
    }
    public static String getMergedCellValue(Sheet sheet, int rowIndex, int cellIndex) {
        for (CellRangeAddress mergedRegion : sheet.getMergedRegions()) {
            if (mergedRegion.isInRange(rowIndex, cellIndex)) {
                Row firstRow = sheet.getRow(mergedRegion.getFirstRow());
                Cell firstCell = firstRow.getCell(mergedRegion.getFirstColumn());
                return firstCell.toString();
            }
        }
        return null;
    }
    
    /**
     * ret의 size()가 2이면 성공, 1이면 오류
     * @param cell_id : 반드시 문자와 문자로만 이루어져야 한다.
     * @return
     */
    public static List<String> getCellPosListUsingCellSplit(String cell_id){
    	List<String> ret = new ArrayList<>();
    	Pattern pattern = Pattern.compile("([a-zA-Z]+)(\\d+)");
        Matcher matcher = pattern.matcher(cell_id);
        
        if (matcher.matches()) {
            String letters = matcher.group(1); // 문자
            String numbers = matcher.group(2); // 숫자
            
            System.out.println("문자: " + letters);
            ret.add(letters);
            System.out.println("숫자: " + numbers);
            ret.add(numbers);
        } else {
            ret.add("Err");
        }
        return ret;
    }
    public static String getCellValueAsString(Cell cell, FormulaEvaluator formulaEvaluator) {
        if (cell == null) {
            return ""; // 셀이 비어 있을 경우 빈 문자열 반환
        }

        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue().trim();
            case NUMERIC:
//                if (ExcelCellUtil.isCellDateFormatted(cell)) {
//                    // 날짜 형식의 값을 문자열로 반환
//                    return cell.getDateCellValue().toString();
//                } else {
//                    // 숫자 형식의 값을 문자열로 반환
	            	DataFormatter dataFormatter = new DataFormatter();
	                String formattedValue = dataFormatter.formatCellValue(cell);
                    return formattedValue;
//                }
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
            	CellValue cellValue = formulaEvaluator.evaluate(cell);
            	if (cellValue.getCellType() == CellType.NUMERIC) {
                    double calculatedValue = cellValue.getNumberValue();
                    String calculatedValueAsString = NumberToTextConverter.toText(calculatedValue);
                    System.out.println("계산된 값: " + calculatedValueAsString);
                    return calculatedValueAsString;
                }
            default:
                return "";
        }
    }
    public static boolean isCellDateFormatted(Cell cell) {
        if (cell == null) {
            return false;
        }

        CellType cellType = cell.getCellType();
        if (cellType == CellType.NUMERIC) {
            // 숫자 형식인 경우 추가적인 검사 수행
            if (ExcelCellUtil.isCellDateFormatted(cell)) {
                // 날짜 형식인 경우
                return true;
            }
        }

        return false;
    }
    public static int getCurDepthForParentId(Map<Integer, Integer> startMetaAtDepthMap, int parent_id_meta) {
        if (startMetaAtDepthMap.containsKey(parent_id_meta)) {
            return startMetaAtDepthMap.get(parent_id_meta);
        } else {
            // 기본값 또는 오류 처리 로직을 여기에 추가하세요.
            return -1; // 예를 들어, 값이 없을 때 -1을 반환하도록 설정
        }
    }
}
