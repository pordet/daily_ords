<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ attribute name="id_category" required="false" %>
<%@ attribute name="id_nat_area" required="false" %>
<%@ attribute name="id_city_area" required="false" %>
<%@ attribute name="id_zone" required="false" %>
<commc:NaviTag id_menu="${param.id_menu}" listName="list" locale="${locale}"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2020.11.11
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<div class="navi_tit_cont">
	<c:forEach var="v" items="${list}" varStatus="i">
		<c:choose>
			<c:when test="${i.count==1}">
			<a href="<c:url value='/${v.menu_url}?id_menu=${v.id_menu}'/>">${v.menu_nm}</a>
			</c:when>
			<c:otherwise>
			<c:if test="${not empty v.codeNm}">
		 > <a href="<c:url value='/${v.menu_url}?id_menu=${v.id_menu}'/>">${v.menu_nm}</a>
 			</c:if>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</div>
