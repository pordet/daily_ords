<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자 그룹 관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/group_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:choose>
					<c:when test="${empty param.id_group}">
						<span id="cur_title">사용자 그룹 등록</span>
					</c:when>
					<c:otherwise>
						<span id="cur_title">사용자 그룹 수정</span>
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm">
					<input type="hidden" name="id_group" id="id_group" value="${param.id_group}">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>그룹명</td>
									<td>
										<input type="text" class="" id="group_nm" name="group_nm" value="${map.group_nm}" onfocusout="checkStringFormatAndNull(this,50);">
									</td>
								</tr>
								<tr>
									<td>설명</td>
									<td>
										<input type="text" class="" id="group_desc" name="group_desc" value="${map.group_desc}" onfocusout="checkStringFormatAndNull(this,250);">
									</td>
								</tr>
								<tr>
									<td>시작페이지</td>
									<td>
										<select class="" name="id_main_menu" id="id_main_menu">
											<option value="">없음</option>
											<c:forEach var="cg" items="${programList}">
												<c:choose>
													<c:when test="${map.id_main_menu eq cg.id_menu}">
														<option value="${cg.id_menu}" selected>메뉴 Depth ${cg.menu_depth} / 메뉴명 : ${cg.menu_nm}</option>
													</c:when>
													<c:otherwise>
														<option value="${cg.id_menu}">메뉴 Depth ${cg.menu_depth} / 메뉴명 : ${cg.menu_nm}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>Role</td>
									<td>
										<div class="dual-list-area">
											<div class="list-group-wrap">
												<div>등록가능:</div>
												<div class="list-wrap">
													<ul class="" id="id_role">
														<c:forEach var="cg" items="${roleList}">
															<c:choose>
																<c:when test="${fn:length(groupRoleList) > 0}">
																	<c:set var="exist" value="false" />
																	<c:forEach var="gr" items="${groupRoleList}">
																		<c:if test="${cg.id_role == gr.id_role }">
																			<c:set var="exist" value="true" />
																		</c:if>
																	</c:forEach>
																	<c:if test="${exist eq 'false' }">
																		<li id="role_li_${cg.id_role}">
																			<div>
																				<input class="" type="checkbox" value="${cg.id_role}" id="role_${cg.id_role}">
																				<label class="" for="role_${cg.id_role}">${cg.role_nm}</label>
																			</div>
																		</li>
																	</c:if>
																</c:when>
																<c:otherwise>
																	<li id="role_li_${cg.id_role}">
																		<div>
																			<input class="" type="checkbox" value="${cg.id_role}" id="role_${cg.id_role}">
																			<label class="" for="role_${cg.id_role}">${cg.role_nm}</label>
																		</div>
																	</li>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</ul>
												</div>
											</div>
											<div class="list-btn-wrap">
												<button type="button" class="btn-blank" onclick="javascript:addRole();" title="추가">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<button type="button" class="btn-blank" onclick="javascript:removeRole();" title="삭제">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
											</div>
											<div class="list-group-wrap">
												<div>등록된 Role:</div>
												<div class="list-wrap">
													<ul class="" id="groupRoleList">
														<c:forEach var="gr" items="${groupRoleList}">
															
															<li id="role_li_${gr.id_role}">
																<div>
																	<input class="" type="checkbox" value="${gr.id_role}" id="role_${gr.id_role}" name="id_role">
																	<label class="" for="role_${gr.id_role}">${gr.role_nm}</label>
																</div>
															</li>
														</c:forEach>
													</ul>
												</div>
											</div>
										
										</div>
										
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" class="" name="use_yn" value="Y" id="ra1" ${(map.use_yn eq 'Y'?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" class="" name="use_yn" value="N" id="ra2" ${(map.use_yn ne 'Y'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
										
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" id="btnList">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" class="" onClick="javascript:initForm();">초기화</button>
							<button type="button" class="btn-danger" onClick="javascript:deleteUserGroup();">삭제</button>
							<button type="button" class="btn-primary" id="btnSave">등록</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
var menuArr = new Array();
<c:forEach var="cg" items="${roleList}">
	var menuItemArr = new Array();
	menuItemArr.push("${cg.id_role}");
	menuItemArr.push("${cg.role_nm}");
	menuArr.push(menuItemArr);
</c:forEach>
$("#btnList,#toList").click(function(){
	var frm =document.frm;
	var url = contextPath+"/adm/user/userGroupList.do?id_menu=${param.id_menu}";
	var parent_id = "${param.parent_id}";
	if(parent_id){
		url+="&parent_id="+parent_id;
	}
	location.href=url;
});

</script>
</html>
