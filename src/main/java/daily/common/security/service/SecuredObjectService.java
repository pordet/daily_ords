package daily.common.security.service;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Service;

import daily.common.security.dao.SecuredObjectDAO;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Service
public class SecuredObjectService {
//	private Logger log = LoggerFactory.getLogger(SecuredObjectService.class);
	@Autowired SecuredObjectDAO dao;

	public LinkedHashMap<Object,List<ConfigAttribute>> getRolesAndResource(String resourceType) throws Exception {
		LinkedHashMap<Object,List<ConfigAttribute>> resourceMap = new LinkedHashMap<Object,List<ConfigAttribute>>();
		
		List<CustomMap> data;
		boolean isResourceUrl = false;
		if(resourceType.equals("method")) {
			data = dao.getRolesAndMethod();
		}else if(resourceType.equals("pointcut")) {
			data = dao.getRolesAndPointcut();
		}else {
			data = dao.getRolesAndUrl();
			isResourceUrl = true;
		}
		String preResource = null;
		String currentResourceStr;
		Object currentResource;
		for(CustomMap map : data) {
			currentResourceStr = map.getString("resource_pattern", "");
			currentResource = isResourceUrl ? new AntPathRequestMatcher(currentResourceStr) : currentResourceStr;
			List<ConfigAttribute> cfgList = new LinkedList<ConfigAttribute>();
			
			if(preResource !=null && currentResourceStr.equals(preResource)) {
				List<ConfigAttribute> preAuthList = resourceMap.get(currentResource);
				for(ConfigAttribute auth: preAuthList) {
					SecurityConfig tempCfg = (SecurityConfig) auth;
					cfgList.add(tempCfg);
				}
			}
			cfgList.add(new SecurityConfig(map.getString("authority","")));
			
			resourceMap.put(currentResource, cfgList);
			preResource = currentResourceStr;
		}
		return resourceMap;
	}
	
	public LinkedHashMap<RequestMatcher, List<ConfigAttribute>> getRolesAndUrl() throws Exception {
//		log.debug("ZZZZZ:getRolesAndUrl() Start");
		LinkedHashMap<Object,List<ConfigAttribute>> data = this.getRolesAndResource("url");
		LinkedHashMap<RequestMatcher, List<ConfigAttribute>> ret = new LinkedHashMap<RequestMatcher, List<ConfigAttribute>>();
		Set<Object> keys = data.keySet();
		for(Object key : keys) {
			ret.put((AntPathRequestMatcher)key, data.get(key));
		}
		return ret;
	}
	
	public LinkedHashMap<String,List<ConfigAttribute>> getRolesAndPointCut() throws Exception {
//		log.debug("ZZZZZ:getRolesAndPointCut() Start");
		LinkedHashMap<Object,List<ConfigAttribute>> data = this.getRolesAndResource("pointcut");
		LinkedHashMap<String, List<ConfigAttribute>> ret = new LinkedHashMap<String, List<ConfigAttribute>>();
		Set<Object> keys = data.keySet();
		for(Object key : keys) {
			ret.put((String)key, data.get(key));
		}
		return ret;
	}
	
	public LinkedHashMap<String,List<ConfigAttribute>> getRolesAndMethod() throws Exception {
//		log.debug("ZZZZZ:getRolesAndMethod() Start");
		LinkedHashMap<Object,List<ConfigAttribute>> data = this.getRolesAndResource("method");
		
		LinkedHashMap<String, List<ConfigAttribute>> ret = new LinkedHashMap<String, List<ConfigAttribute>>();
		Set<Object> keys = data.keySet();
		for(Object key : keys) {
			ret.put((String)key, data.get(key));
		}
		return ret ;
	}
	public List<ConfigAttribute> getRegexMatchedRequestMapping(String url) throws Exception{
//		log.debug("ZZZZZ:getRegexMatchedRequestMapping() Start");
		CommandMap paramMap = new CommandMap();
		paramMap.put("url", url);
		List<CustomMap> result = dao.getRegexMatchedRequestMapping(paramMap);
		List<ConfigAttribute> cfgList = new LinkedList<ConfigAttribute>();
		for(CustomMap map : result) {
			cfgList.add(new SecurityConfig(map.getString("id_role","")));
		}
		return cfgList;
	}
	public String getHierarchicalRoles() throws Exception {
		return dao.getHierarchicalRoles();
	}
	public List<CustomMap> selectRolesAndUrlList(String id_user) throws Exception{
		Map<String,Object> map = new java.util.HashMap<String,Object>();
		map.put("id_user", id_user);
		List<CustomMap> data = dao.selectRolesAndUrlList(map);
		return data;
	}
}
