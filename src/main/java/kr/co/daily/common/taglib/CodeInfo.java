package kr.co.daily.common.taglib;

import daily.common.util.SimpleVoTagSupport;
import daily.common.util.SpringApplicationContext;
import kr.co.daily.common.dao.CommonDAOImpl;
import kr.co.daily.domain.CodeVO;

public class CodeInfo extends SimpleVoTagSupport {
	private String code_group;
	private String code;
	private String locale;

	public String getCode_group() {
		return code_group;
	}

	public void setCode_group(String code_group) {
		this.code_group = code_group;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public CodeVO getVo() {
		CodeVO ret = null;
		CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
		CodeVO vo = new CodeVO();
		vo.setCode(code);
		vo.setCode_group(code_group);
		vo.setLocale(locale);
		try {
			ret = dao.getCodeInfo(vo);
		}catch(Exception e) {
			ret = null;
		}
		return ret;
	}

}
