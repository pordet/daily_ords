<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<commc:MenuTag user_seq="${id_user}" locale="${locale}" listName="menuiList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <!-- 레프트네비 -->
        <aside class="mh-side-nav">
            <section class="mh-sn-bar">
                <!-- side bar -->
                <div class="mh-sn-menuwrap">
                    <nav class="mh-sn-menu"><!-- li 요소에 .mh-open 클래스를 넣으면 선택된 요소로 활용 -->
                        <ul class="mh-menu-list">
                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Tooltip on right"><span class="icon_dashboard"></span>Dashboard</a></l>
                            <li><a href="#"><span class="icon_task"></span>Task</a></li>
                            <li class="mh-mlview">
                                <a href="#"><span class="icon_newrecord"></span>New Record</a>
                                <ul class="mh-mlview-menu">
                                    <li><a href="#">Layout</a></li>
                                    <li><a href="#">Navigation</a></li>
                                    <li><a href="#">Main Frame</a></li>
                                    <li><a href="#">Contents</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><span class="icon_report"></span>Report</a></li>
                            <li class="mh-mlview mh-open">
                                <a href="#"><span class="icon_admin"></span>Administration</a>
                                <ul class="mh-mlview-menu">
                                    <li class="mh-open"><a href="#">User Management</a></li>
                                    <li><a href="#">Code Management</a></li>
                                    <li><a href="#">Menu Management</a></li>
                                    <li><a href="#">IECMS Sync Management</a></li>
                                   
                                </ul>
                            </li>
                            <li><a href="#"><span class="icon_search"></span>Search</a></li>

                        </ul>
                    </nav>
                </div>
                <div class="footer-content">
                    <span class="copyright-img"><img src="<c:url value='/resource/img/icon_gray.png'/>" alt=""/><em>National Public<br>Prosecution Authority</em></span>
                    <span class="copyright-footer">Copyright &copy; 2019 2018. <br>National Public Prosecution Authority.<br>All Rights Reserved.</span>
                </div>
            </section>
        </aside>
        <!-- //레프트네비 -->
<script>
</script>
