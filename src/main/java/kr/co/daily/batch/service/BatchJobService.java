package kr.co.daily.batch.service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import daily.common.util.SpringPropertiesUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
@Service
public class BatchJobService {
	
	@Autowired private BatchService batchService;
	private static Logger log = LoggerFactory.getLogger(BatchJobService.class);
	/**
	 * 주기적으로 실행 시키는 cron 속성
	 */
    //@Scheduled(cron = "00 06 23 * * *")		
    /**
     * 즉시실행시키는 fixedDelay 속성  , 1초가 1000
     * @throws ParseException 
     */
//    @Scheduled(initialDelay = 3000,fixedRate = 60000000)		
//    @Scheduled(cron = "00 12 25 * * *")		
    public void openApiBatch() throws ParseException,Exception {
    	log.debug("Batch job start");
//    	String search_date = StringDateUtil.getMinusFromToday("yyyyMMdd",DateEnum.DAY,1);
//    	String search_date="20230825";
    	String search_date = StringDateUtil.getToday("yyyyMMdd");
		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
		int num_of_rows = Integer.parseInt(numOfRows);
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,String> batInfoMap = new HashMap<String,String>();
    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    	batInfoMap.put("id_service", GlobalConst.META_M_TYPE_BID);
		batInfoMap.put("search_date", search_date);
		batInfoMap.put("lines_per_pg", numOfRows);
    	Map<String,Object> batRet = batchService.getRestfulInfoByCurl(search_date,curr_pg,num_of_rows,false, batInfoMap);
    	if(batRet.get("is_ins")!=null) {
        	boolean rowSuc=false;
        	String is_ins_str = String.valueOf(batRet.get("is_ins"));
        	boolean isIns = Boolean.valueOf(is_ins_str);
        	if(!isIns) {
        		return;
        	}
        	if(batRet.get("sucYn")!=null) {
        		boolean sucYn = (boolean)batRet.get("sucYn");
        		if(!sucYn) {
        			while(try_api_cnt<3) {
        		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
        		    	Map<String,Object> batTryRet = batchService.getRestfulInfoByCurl(search_date,curr_pg,num_of_rows,false,batInfoMap);
        		    	if(batTryRet.get("sucYn")!=null) {
        		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
        		    		if(trySucYn) {
        		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
        		        		total_pg = (int)batRet.get("tot_pg");
        		        		rowSuc=true;
        		    			curr_pg++;
        		    	        break;
        		    		}
        		    	}
        		    	try_api_cnt++;
        			}
        	    	log.debug("Batch job start search fail");
        		}else {
                	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
        			rowSuc=true;
        			curr_pg++;
            		total_pg = (int)batRet.get("tot_pg");
        	    	log.debug("Batch job start search start");
        		}
        	}else {
        		throw new Exception("Batch job Fail");
        	}
        	if(rowSuc) {
            	for(int i=curr_pg;i<=total_pg;i++) {
            		System.out.println("현재 "+i+"페이지 검색 시작");
            		batchService.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
            	}
            	int totPgCnt = batchService.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
            	int retCnt = batchService.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
            	if(totPgCnt==retCnt) {
            		batInfoMap.put("succ_yn", "Y");
            		batchService.updateSuccYnAtBatchM(batInfoMap);
            	}
        	}else {
        		CommandMap failMap = new CommandMap();
        		failMap.put("search_date",batInfoMap.get("search_date"));
        		failMap.put("id_service", batInfoMap.get("id_service"));
        		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
    			failMap.put("succ_yn", "F");
        		batchService.insertDailyBatchM(failMap);
        	}
    		
    	}else {
	    	log.debug("Batch job start passing");
    		
    	}
    	log.debug("Batch job end");
    }

//    @Scheduled(initialDelay = 3000,fixedRate = 60000000)	
    ////    @Scheduled(cron = "00 12 25 * * *")		
    @Scheduled(cron = "0 0 4,12,20 * * *")
    public void testBatch() throws Exception {
    	log.debug("Vwps Batch job start");
    	//testTem();
//    	curlHttp(null);
//    	openApiBatch();
    	String search_date = StringDateUtil.getToday("yyyyMMdd");
    	this.manualCallOpenApi(GlobalConst.META_M_TYPE_BID,search_date, search_date,"yyyyMMdd");
//    	this.manualOriginCallOpenApi("4","20230831", "20230831","yyyyMMdd");
//    	useWebClient(null);
//    	this.getTestRestfulInfo(null);
//    	log.debug("this.getRestfulInfoByString(null,300) start");
//    	this.getRestfulInfoByString(null,300);
//    	log.debug("this.getRestfulInfoByString(null,1,300) start");
//   	    this.getRestfulInfoByString(null,1,300);
//    	log.debug("Vwps Batch job end");
    }

	/**
     * 오늘 날짜를 제외하고 이전 날짜 검색용 함수(한 달 기간내 batch 실행여부만으로 실행 안된 경우만 처리할 때 사용)
     * @param id_service
     * @param st_date
     * @param ed_date
     * @param dateFormat
     * @throws ParseException
     * @throws Exception
     */
    public void manualCallOpenApi(String id_service,String st_date,String ed_date, String dateFormat) throws ParseException,Exception {
    	log.debug("manualCallOpenApi Batch job start");
		List<String> dateList = StringDateUtil.getPeriodDateListByFormat(st_date,ed_date,dateFormat);
		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
		try {
			for(String search_date:dateList) {
				System.out.println("!!!!Start Search_date:"+search_date);
		    	Map<String,String> batInfoMap = new HashMap<String,String>();
		    	batInfoMap.put("try_api_cnt", "1");
		    	batInfoMap.put("id_service", id_service);
				batInfoMap.put("search_date", search_date);
				batInfoMap.put("lines_per_pg", numOfRows);
				batInfoMap.put("date_format", dateFormat);
				batInfoMap.put("curr_pg", "1");
       			String today = StringDateUtil.getDateByFormat(dateFormat, Locale.KOREAN);
   	     		CustomMap daySuccYn = batchService.getSuccYnAtDailyBatchMBySearchDate(batInfoMap);
   				int curr_date_daily_bat_num = daySuccYn.getInt("daily_bat_num",0);
     			if(today.equals(search_date)) {
       				if(curr_date_daily_bat_num==0) {
       					batchService.insertBatchJob(batInfoMap);
       				}else {
       					batchService.updateBatchJob(batInfoMap);
       				}
       			}else {
       				if(curr_date_daily_bat_num==0) {
       					batchService.insertBatchJob(batInfoMap);
       				}else {
       					batchService.updateBatchJob(batInfoMap);
//       					System.out.println("!!!!Search_date:"+search_date+"에 데이터가 있으므로 패스");
//      					continue;
       				}
       			}
			}
		}catch(Exception e) {
	    	log.debug("Batch job exception orr");
		}
    }

    public void manualOriginCallOpenApi(String id_service,String st_date,String ed_date, String dateFormat) throws ParseException,Exception {
    	log.debug("manualCallOpenApi Batch job start");
		List<String> dateList = StringDateUtil.getPeriodDateListByFormat(st_date,ed_date,dateFormat);
		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
		try {
			for(String search_date:dateList) {
				System.out.println("!!!!Start Search_date:"+search_date);
		    	Map<String,String> batInfoMap = new HashMap<String,String>();
		    	batInfoMap.put("try_api_cnt", "1");
		    	batInfoMap.put("id_service", id_service);
				batInfoMap.put("search_date", search_date);
				batInfoMap.put("lines_per_pg", numOfRows);
				batInfoMap.put("date_format", dateFormat);
				batInfoMap.put("curr_pg", "1");
       			String today = StringDateUtil.getDateByFormat(dateFormat, Locale.KOREAN);
   	     		CustomMap daySuccYn = batchService.getSuccYnAtDailyBatchMBySearchDate(batInfoMap);
   				int curr_date_daily_bat_num = daySuccYn.getInt("daily_bat_num",0);
     			if(today.equals(search_date)) {
       				if(curr_date_daily_bat_num==0) {
       					batchService.insertOriginBatchJob(batInfoMap);
       				}else {
       					batchService.updateOriginBatchJob(batInfoMap);
       				}
       			}else {
       				if(curr_date_daily_bat_num==0) {
       					batchService.insertOriginBatchJob(batInfoMap);
       				}else {
       					System.out.println("!!!!Search_date:"+search_date+"에 데이터가 있으므로 패스");
      					continue;
       				}
       			}
			}
		}catch(Exception e) {
	    	log.debug("Batch job exception orr");
		}
    }
}
