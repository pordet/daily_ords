package kr.co.daily.common.dao;

import java.util.List;

import kr.co.daily.domain.BoardVO;
import kr.co.daily.domain.CodeVO;
import kr.co.daily.domain.MenuVO;
import kr.co.daily.domain.NaviVO;

public interface CommonDAO {
	public CodeVO getCodeInfo(CodeVO vo) throws Exception;
	
	public List<BoardVO> selectCodeListByCodeGroup(BoardVO vo) throws Exception;
	
	public List<MenuVO> selectMenu(MenuVO vo) throws Exception;
	
	public List<MenuVO> selectTopMenu(MenuVO vo) throws Exception;
	
	public MenuVO getMainMenuByGroupId(MenuVO vo)  throws Exception;
	
	public List<BoardVO> selectBoardMasterList(BoardVO vo) throws Exception;
	
	public List<BoardVO> selectBoardMasterListByKeyword(BoardVO vo) throws Exception;
	
	public List<BoardVO> selectBoardMasterRootList(BoardVO vo) throws Exception;
	
	public List<BoardVO> selectBoardMasterChildList(BoardVO vo) throws Exception;

	List<MenuVO> selectMenuListForNavigationBar(NaviVO vo) throws Exception;

	List<MenuVO> selectTopMenuByIdGroup(MenuVO vo) throws Exception;

	List<MenuVO> selectMenuByIdGroup(MenuVO vo) throws Exception;
	
}
