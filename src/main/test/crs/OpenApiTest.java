package crs;

import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;

public class OpenApiTest {

	@Test
	@RequestMapping(value = "/test/getAuthAjax.do", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.POST)
	public void test(Model model) {
		final String uri = "https://uat.nida.gov.rw:8081/onlineauthentication/claimtoken";
	    RestTemplate restTemplate = new RestTemplate();
//	    restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
	    
//	    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
//	    mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
//	    restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
	    
	    final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//	    map.add("username", NIDA_USER_NAME);
//	    map.add("password", NIDA_USER_PWD);
	    String valor ="{\"username\":\"NPPA\", \"password\":\"RHaUM3PraUWJC2jS6Ex-hYWZeyxHRTj&gSxjWuy6fEK9N$5*4un7nNnLnuXymp#PvN5N5@yBVE&EFgArANmcWxg=-^Q&ZRbdGm^k4veXAng#g4rQCJK-_tfNsQX=?Wrx\"}";
	    HttpEntity<String> request = new HttpEntity<String>(valor, headers);
//	    CustomMap result = restTemplate.getForObject(uri, CustomMap.class);
	    String result = restTemplate.postForObject(uri, request, String.class);
	    model.addAttribute("result", result);

	}
	@Test
	@RequestMapping(value = "/test/getAuthAjax.do", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.POST)
	public String getUsrJSON(CommandMap param,Model model)
	{
//		final String uri = "http://localhost:8080/test/getUsrTest.do?id="+id;
//		final String uri = "https://uat.nida.gov.rw:8081/onlineauthentication/GetCitizen";
		final String uri = "https://uat.nida.gov.rw:8081/onlineauthentication/GetCitizen";
	    RestTemplate restTemplate = new RestTemplate();
	    final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFYnBJZCI6IjE2IiwiRWJwTmFtZSI6Ik5QUEEiLCJPdGsiOiJIVGl2alNCeFZXdXNJa0tHZU5NeWdzK01qWkRHOG5nc1NjZnRFNFkzcnJENVN1NU9RbElzdHloT0VObFZiakVWIiwianRpIjoiNzIwMTE4NjItYjY1YS00NTdiLTk5MGEtZTVlZmE3YjlhYzczIiwibmJmIjoxNTY3NTExMTE5LCJleHAiOjE1Njc1MjU1MTksImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDI4Ni8iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQyODYvIn0.IUCYkyttAGwOAzbzf4fZgCDnt3atwBhuf4h3odqX-eE'); nested exception is com.fasterxml.jackson.databind.exc.MismatchedInputException: Cannot construct instance of `java.util.LinkedHashMap` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value ('Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFYnBJZCI6IjE2IiwiRWJwTmFtZSI6Ik5QUEEiLCJPdGsiOiJIVGl2alNCeFZXdXNJa0tHZU5NeWdzK01qWkRHOG5nc1NjZnRFNFkzcnJENVN1NU9RbElzdHloT0VObFZiakVWIiwianRpIjoiNzIwMTE4NjItYjY1YS00NTdiLTk5MGEtZTVlZmE3YjlhYzczIiwibmJmIjoxNTY3NTExMTE5LCJleHAiOjE1Njc1MjU1MTksImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDI4Ni8iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQyODYvIn0.IUCYkyttAGwOAzbzf4fZgCDnt3atwBhuf4h3odqX-eE");
 
	    MultiValueMap<String,String> map = CollectionUtil.getMultValueMapToMap(param.getMap());
	    map.add("DocumentNumber","180104259855");
//	    MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
	    //	    CustomMap result = restTemplate.getForObject(uri, CustomMap.class);
	    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
	    @SuppressWarnings("unchecked")
	    Map<String, Object> result = restTemplate.postForObject(uri, request, Map.class);
	    model.addAttribute("result", result);
	    return "jsonView";
	}
	
	@Test
	public void tranTest() {
//		String srcString="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFYnBJZCI6IjE2IiwiRWJwTmFtZSI6Ik5QUEEiLCJPdGsiOiJIVGl2alNCeFZXdXNJa0tHZU5NeWdzK01qWkRHOG5nc1NjZnRFNFkzcnJENVN1NU9RbElzdHloT0VObFZiakVWIiwianRpIjoiODMzMjgyNGUtODEyNi00YWI2LThkNDktODE0MmU5ZmY1YmZkIiwibmJmIjoxNTY3OTk0MDE4LCJleHAiOjE1NjgwMDg0MTgsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDI4Ni8iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQyODYvIn0.cD7dGXIEkera5FcaqFuNYER7_S1DiKr_va6uZcmidDg";
//		byte[] utf8String = Encoding.UTF8.GetBytes(srcString);
//        byte[] asciiString = Encoding.ASCII.GetBytes(srcString);
//		System.out.println("UTF-8  Bytes: {0}", Encoding.UTF8.GetString(utf8String));
//		System.out.println("ASCII  Bytes: {0}", Encoding.ASCII.GetString(asciiString));
	}
	
}
