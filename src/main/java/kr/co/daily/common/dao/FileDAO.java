package kr.co.daily.common.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface FileDAO {

	void insertFileInfo(Map<String, Object> map);

	void deleteFileInfo(Map<String, Object> map);

	CustomMap getRealFileInfo(Map<String, Object> map);

	List<CustomMap> selectDeleteFileList(Map<String, Object> map);

	CustomMap getPhotoInRequestInfo(Map<String, Object> map);

}
