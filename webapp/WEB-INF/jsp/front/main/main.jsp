<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.BBS_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>

<!DOCTYPE html>
<front:FrontDynamicHead title=""/>
<front:FrontDynamicPortalMainBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
<!-- 컨텐츠영역 : 여기에 작성 -->
	<div class="front-main">
		<div class="front-main-boundary">
			<c:choose>
				<c:when test="${is_guest}">
					<c:url value="/front/bbs/boardDetail.do" var="urlValue"/>
					<c:set var="link" value="${urlValue}"/>
				</c:when>
				<c:otherwise>
					<c:url value="/adm/bbs/boardDetail.do" var="urlValue"/>
					<c:set var="link" value="${urlValue}"/>
				</c:otherwise>
			</c:choose>
			<form id="boardFrm" name="boardFrm">
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="id_meta_m" name="id_meta_m"/>
				<input type="hidden" name="is_guest" value="${(is_guest? 'Y':'N')}"/>
				<input type="hidden" name="from_main" value="Y"/>
			</form>
			<div class="half-board">
				<div style="height:400px; padding:10px 0;">
					<div class="inner-area">
						<div class="half-board-title-area">
						 	<div>
								<span>게시판</span>
								<c:choose>
									<c:when test="${is_guest}">
						 		<a href="<c:url value='/front/bbs/boardList.do?id_meta_m=1'/>">더보기+</a>
									</c:when>
									<c:otherwise>
						 		<a href="<c:url value='/adm/bbs/boardList.do?id_meta_m=1'/>">더보기+</a>
									</c:otherwise>
								</c:choose>
						 	</div>
						</div>
						<div class="half-board-grid-area">
	                   <!-- 그리드 해더영역 -->
		                   <div class="grid-header">
		                       <c:forEach var="map" items="${infoHeaderList}" varStatus="i">
			                       <c:set var="width" value=""/>
			                       <c:if test="${empty map.id_meta }">
			                       	<c:set var="width" value=" style=\"width:15%\""/>
			                       </c:if>
			                       <c:if test="${not empty map.id_meta}">
			                       	<c:set var="width" value=" style=\"width:70%\""/>
			                       </c:if>
		                       <div class="header-unit"${width}>
		                       <input type="hidden" id="h_${map.id_meta}" value="${map.id_meta}">
		                       ${map.meta_nm}
		                       </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
		                       </c:forEach>
		                   </div>
		                   <!-- //그리드 해더영역 -->
		                <!-- 그리드 컨텐츠영역 -->
		                   <div class="grid-contents">
		                   <c:forEach var="low" items="${infoList}" varStatus="i">
		                       <div class="grid-loof-cont ${(not empty low.id_meta_row?'editable':'')}" id="row_${i.count}" ondblclick="javascript:openBoard('${low.id_meta_m}','${low.id_meta_row}')">
		                       <c:forEach var="col" items="${infoHeaderList}" varStatus="j">
			                       <c:set var="width" value=""/>
			                       <c:if test="${empty col.id_meta}">
			                       	<c:set var="width" value=" style=\"width:15%\""/>
			                       </c:if>
			                       <c:if test="${not empty col.id_meta}">
			                       	<c:set var="width" value=" style=\"width:70%\""/>
			                       </c:if>
			                       	<c:set var="meta_cd" value="${col.meta_cd}"/>
			                        <c:set var="cont" value="${low[meta_cd]}"/>
			                        <div class="cont-unit"${width}>
			                        <a class="title-link" href="${link}?id_meta_m=1&id_meta_row=${low.id_meta_row}&is_guest=${(is_guest?'Y':'N')}&from_home=Y">${cont}</a>
			          				</div>
			          			</c:forEach>	
			                    </div>
		                   	</c:forEach>
		                   </div>
						</div>
					</div>
				</div>
			</div>
			<div class="half-board">
				<div style="height:400px; padding:10px 0;">
					<div class="inner-area">
						<div class="half-board-title-area">
						 	<div>
								<span>공지사항</span>
								<c:choose>
									<c:when test="${is_guest}">
						 		<a href="<c:url value='/front/bbs/boardList.do?id_meta_m=2'/>">더보기+</a>
									</c:when>
									<c:otherwise>
						 		<a href="<c:url value='/adm/bbs/boardList.do?id_meta_m=2'/>">더보기+</a>
									</c:otherwise>
								</c:choose>
						 	</div>
						</div>
						<div class="half-board-grid-area">
	                   <!-- 그리드 해더영역 -->
		                   <div class="grid-header">
		                   <c:forEach var="map" items="${notiHeaderList}" varStatus="i">
		                       <c:set var="width" value=""/>
		                       <c:if test="${empty map.id_meta}">
		                       	<c:set var="width" value=" style=\"width:15%\""/>
		                       </c:if>
		                       <c:if test="${not empty map.id_meta}">
		                       	<c:set var="width" value=" style=\"width:70%\""/>
		                       </c:if>
		                       	<div class="header-unit"${width}>
		                       		<input type="hidden" id="h_${map.id_meta}" value="${map.id_meta}">
		                       		${map.meta_nm}
		                       	</div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
		                   </c:forEach>
		                   <!-- //그리드 해더영역 -->
		                <!-- 그리드 컨텐츠영역 -->
		                   <div class="grid-contents">
		                   <c:forEach var="low" items="${notiList}" varStatus="i">
		                       <div class="grid-loof-cont ${(not empty low.id_meta_row?'editable':'')}" id="row_${i.count}" ondblclick="javascript:openBoard('${low.id_meta_m}','${low.id_meta_row}')">
		                       <c:forEach var="col" items="${notiHeaderList}" varStatus="j">
			                       <c:set var="width" value=""/>
			                       <c:if test="${empty col.id_meta}">
			                       	<c:set var="width" value=" style=\"width:15%\""/>
			                       </c:if>
			                       <c:if test="${not empty col.id_meta}">
			                       	<c:set var="width" value=" style=\"width:70%\""/>
			                       </c:if>
			                       	<c:set var="meta_cd" value="${col.meta_cd}"/>
			                        <c:set var="cont" value="${low[meta_cd]}"/>
			                        <div class="cont-unit"${width}>
			                        <a class="title-link" href="${link}?id_meta_m=2&id_meta_row=${low.id_meta_row}&is_guest=${(is_guest?'Y':'N')}&from_home=Y">${cont}</a>
			          				</div>
			          			</c:forEach>	
		                       	</div>
		                   	</c:forEach>
		                   </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</front:FrontDynamicPortalMainBody>
<script>
function openBoard(id_meta_m,id_meta_row){
	$("#id_meta_m").val(id_meta_m);
	if(id_meta_row){
		$("#id_meta_row").val(id_meta_row);
	}else{
		$("#id_meta_row").val('');
	}
	var frm =document.boardFrm;
	<c:choose>
	<c:when test="${is_guest}">
		<c:set var="link" value="/front/bbs/boardDetail.do"/>
	</c:when>
	<c:otherwise>
		<c:set var="link" value="/adm/bbs/boardDetail.do"/>
	</c:otherwise>
	</c:choose>
		frm.action=contextPath+'${link}';
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
</script>