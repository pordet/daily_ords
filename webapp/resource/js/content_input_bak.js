/*
function openInput(){
	setInputTitle('I');	
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_year").val($("#search_year").val());
	$("#previewModal").modal('show');	
}
function setInputTitle(type){
	var title = "";
	
	if(nanToStr($("#id_category").val())!=''){
		title +=$("#id_category option:selected").text()+' ';
	}
	if(nanToStr($("#id_nat_area").val())!=''){
		title +=$("#id_nat_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_city_area").val())!=''){
		title +=$("#id_city_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_zone").val())!=''){
		title +=$("#id_zone option:selected").text()+' ';
	}
	if(type=='I'){
		title+='데이터 등록/수정';
	}else{
		title+='데이터 수정';
	}
	$("#data_input_pop_title").html(title);
    
}

function openAjaxInput(){
	setInputTitle('I');	
	var id_zone = nanToStr($("#id_zone").val());
	var id_category = $("#id_category").val();
//	if(id_zone!='' && id_category =='1'){
//		zoneAjaxInput();
	var id_meta = nanToStr($("#id_meta").val());
	if(id_meta !='135' && id_meta !='136' && id_category =='4'){
		detailAjaxInput();
	}else{
	
		if($("#id_city_area").val()!=''){
			$("#reg_id_city_area").val($("#id_city_area").val());
			$("#reg_id_nat_area").val($("#id_nat_area").val());
			if($("#id_zone").val()!=''){
				$("#reg_id_zone_area").val($("#id_zone").val());
			}
			
		}else if($("#id_nat_area").val()==''){
			alertOpen('도시, 국가를 선택하십시오.');
			return;
		}else{
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}
		if($("#id_category").val()==''){
			alertOpen('카테고리를 선택하십시오.');
			return;
		}else{
			$("#reg_id_category").val($("#id_category").val());
		}
		$("#reg_id_zone_area").val($("#id_zone").val());
		$("#reg_year").val($("#search_year").val());
		var params = $("#reg_frm").serialize();
		var url = contextPath+"/getDataAjax.do";
		var sel_year=$("#search_year").val();
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	drawInput(e);
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
}
function drawInput(e){
	var sel_year=$("#search_year").val();
	endProcessing();
	if(e.ret==1){
		alertOpen('실패하였습니다.');
	}else{
		if(e.rowlist.length!=0){
			var html="";
    		$.each(e.rowlist,function(i,d){
    			if(d.row_type == 'c'){
    				var row1 = d.row_1;
    				var row2 = d.row_2;
	    			html+='<div class="">';
	    			html+='	<div class="">';
	    			//html+='	  <div class="select-label">'+row1.unit_nm+'</div>';
	    			html+='	  <div class="select-label">'+ wordBreak(row1.unit_nm) +'</div>';
	    			html+='         <div class="form-group">';
	    			if(nanToStr(row1.val)!=""){
		    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
	    			}
	    			if(nanToStr(row1.type)=='N'){
	    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
		    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
	    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
	    				var funcParam = ','+nanToStr(row1.decimal_len);
		    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
	    			}else{
		    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" >';
	    			}
	    			html+='</div>';
	    			html+='</div>';
	    			if(row2!=undefined){
		    			html+='	<div>';
		    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
		    			html+='         <div class="form-group">';
		    			if(nanToStr(row2.val)!=""){
			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
		    			}
		    			if(nanToStr(row2.type)=='N'){
		    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
		    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
		    				var funcParam = ','+nanToStr(row2.decimal_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
		    			}else{
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" >';
		    			}
		    			html+='</div>';
		    			html+='</div>';
	    			}
	    			html+='</div>';
    			}else if(d.data_type == '7'){
    				var child = d.child;
	    			html+='<div>';
	    			html+='	<div class="multi-row-wrap">';
	    			html+='	  <div class="select-label multi-row-header">'+wordBreak(d.unit_nm)+'</div>';
	    			html+='         <div class="form-group">';
	    			html+='           	<input type="hidden" name="sp_'+sel_year+'_'+d.id_meta+'" value="'+nanToStr(d.id_meta)+'"/>';
	    			html+='<div class="child_table">';
    				$.each(child,function(i,c){
	    				var row1 = c.row_1;
	    				var row2 = c.row_2;
		    			html+='<div>';
		    			html+='<div>';
		    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
		    			html+='<div class="form-group">';
		    			if(nanToStr(row1.val)!=""){
			    			html+='           	<input type="hidden" name="di_'+sel_year+'_'+row1.id_meta_data+'_'+row1.id_meta_unit+'" id="di_'+sel_year+'_'+row1.id_meta_data+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'"/>';
		    			}
		    			if(nanToStr(row1.type)=='N'){
		    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
			    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
		    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
		    				var funcParam = ','+nanToStr(row1.decimal_len);
			    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
		    			}else{
			    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" >';
		    			}
		    			html+='</div>';
		    			html+='</div>';
		    			if(row2!=undefined){
			    			html+='	<div>';
			    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
			    			html+='         <div class="form-group">';
			    			if(nanToStr(row2.val)!=""){
				    			html+='           	<input type="hidden" name="di_'+sel_year+'_'+row2.id_meta_data+'_'+row2.id_meta_unit+'" id="di_'+sel_year+'_'+row2.id_meta_data+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'"/>';
			    			}
			    			if(nanToStr(row2.type)=='N'){
			    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
			    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
			    				var funcParam = ','+nanToStr(row2.decimal_len);
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
			    			}else{
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" >';
			    			}
			    			html+='</div>';
			    			html+='</div>';
		    			}
		    			html+='</div>';
    				});
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
    			}else if(d.row_type == 'f'){
    				var row1 = d.row_1;
	    			html+='<div>';
	    			html+='	<div id="file_div_'+row1.id_meta+'">';
	    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
	    			html+='         <div class="form-group inline-group">';
	    			if(nanToStr(row1.id_meta_data)!=''){
			    		html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
	    			}
	    			html+='           	<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" >';
	    		//	html+='<input type="text" id="fileName" disabled>';

	    			html+='           	<div class="attach-btn">';
	    			html+='           		<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
	    			html+='           		<input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
	    			html+='           	</div>';
	    			
	    			html+='         </div>';
	    			html+='	  </div>';
	    			if(nanToStr(row1.addfilelist)!=''){
	    				var file = row1.addfilelist[0];
		    			html+='	<div id="fe_div_'+row1.id_meta+'">';
		    			html+='	  <div class="select-label">첨부된 파일</div>';
		    			html+='   <div class="form-group">';
		    			html+='         <input type="hidden" name="fi_'+sel_year+'_'+row1.id_meta_data+'" id="fi_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.id_meta_data)+'"/>';
		    			html+='         <a href="'+contextPath+'/file/download_add.do?id_data_file='+nanToStr(file.id_data_file)+'">';
		    			html+='         <img src="'+contextPath+'/resource/img/ico_download.png" alt="delete" style="width:28px;height:16px;"/>';
		    			html+='         </a>';  
		    			html+='   </div>';
		    			html+='</div>';
	    			}
	    			html+='</div>';
    				
    			}else{
// 		    				html='';
    				var child = d.child;
	    			html+='<div>';
	    			html+='	<div class="multi-row-wrap">';
	    			html+='	  <div class="select-label multi-row-header">'+wordBreak(d.unit_nm)+'</div>';
	    			html+='         <div class="form-group">';
	    			html+='<div class="child_table">';
    				$.each(child,function(i,c){
	    				var row1 = c.row_1;
	    				var row2 = c.row_2;
		    			html+='<div>';
		    			html+='<div>';
		    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
		    			html+='<div class="form-group">';
		    			if(nanToStr(row1.val)!=""){
			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
		    			}
		    			if(nanToStr(row1.type)=='N'){
		    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
		    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
		    				var funcParam = ','+nanToStr(row1.decimal_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
		    			}else{
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" >';
		    			}
		    			html+='</div>';
		    			html+='</div>';
		    			if(row2!=undefined){
			    			html+='	<div>';
			    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
			    			html+='         <div class="form-group">';
			    			if(nanToStr(row2.val)!=""){
				    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
			    			}
			    			if(nanToStr(row2.type)=='N'){
			    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
			    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
			    				var funcParam = ','+nanToStr(row2.decimal_len);
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
			    			}else{
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" >';
			    			}
			    			html+='</div>';
			    			html+='</div>';
		    			}
		    			html+='</div>';
    				});
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
    			}
    		});
			$("#input-content").empty();
			$("#input-content").append(html);
			$("#previewModal").modal('show');	
		}
	}
	
}
function zoneAjaxInput(){
	if($("#id_zone").val()==''){
		alertOpen('존을 선택하십시오.');
		return;
	}
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_id_zone_area").val($("#id_zone").val());
	$("#reg_year").val($("#search_year").val());
	var arr = $("#id_zone").val().split("_");
	var st_zone_no = arr[0];
	var ed_zone_no = arr[1];
	var year = $("#search_year").val();
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getZoneDataAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
    			var html="";
				html+='<table class="tb03" id="result_table">';
    			html+='<thead>';
    			html+='	  <tr>';
    			html+='         <div class="form-group">';
    			var st_no = Number(st_zone_no);
    			var ed_no = Number(st_zone_no);
    			for(var i = st_no;i<Number(ed_no);i++){
    				if(i==st_no){
    	    			html+='<th class="first" colspan="1">항목 </th>';
    				}else{
    	    			html+='<th scope="col">Zone '+i+'</th>';
    				}
    			}
    			html+='	  </tr>';
    			html+='</thead>';
    			html+='<tbody>';
	    		if(nanToStr(e.rowlist)!='' && e.rowlist.length!=0){
		    		$.each(e.rowlist,function(i,d){
						html+='<tr>';
 		    			html+='	  <td">'+d.unit_nm+'</td>';
			    		$.each(d.child,function(j,f){
	 		    			if(nanToStr(f.val)!=""){
	 			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+f.id_meta_zone_data+'" id="i_'+sel_year+'_'+f.id_meta_zone_data+'" value="'+nanToStr(f.val)+'"/>';
	 		    			}
 			    			html+='<input type="text"  name="m_'+sel_year+'_'+f.id_meta_zone+'_'+f.id_meta+'" id="m_'+sel_year+'_'+f.id_meta_zone+'_'+f.id_meta+'" value="'+nanToStr(f.val)+'" >';
			    		});
						html+='</tr>';
		    		});
	    		}
    			html+='</tbody>';
    			html+='</table>';
    			$("#input-content").empty();
    			$("#input-content").append(html);
    			$("#previewModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

function detailAjaxInput(){
	console.log("으어")
	if($("#id_zone").val()==''){
		alertOpen('존을 선택하십시오.');
		return;
	}
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
		$("#reg_id_city_area").val('');
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	if($("#id_meta").val()==''){
		alertOpen('수요분석의 경우에는 세부항목을 선택해야 합니다.');
		return;
	}
	$("#reg_id_meta").val($("#id_meta").val());
	$("#reg_year").val($("#search_year").val());
	var year = $("#search_year").val();
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getDataByIdMetaAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	drawInput(e);
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
*/


function setInputTitle(){
	var docTitle = "";
	var bodyTitle = "";
	
	if(nanToStr(iniParam.category) != ''){
		docTitle += iniParam.cateName + ' ';
	}
	docTitle += '데이터 등록/수정';
	
	if(nanToStr(iniParam.cityArea) != ''){
		bodyTitle += iniParam.cityName + ' ';
	} else {
		if(nanToStr(iniParam.natArea) != ''){
			bodyTitle += iniParam.natName + ' ';
		}
	}
//	if(nanToStr(iniParam.meta) != ''){
//		bodyTitle += iniParam.metaName + ' ';
//	}
	bodyTitle += '데이터';
	
	$("#data_input_pop_title").html(docTitle);
	$("#modalBodyTitle").html(bodyTitle);
}


function openAjaxInput(){
	setInputTitle();
	
	var id_category = iniParam.category;
//	var id_meta = iniParam.meta;
	var id_meta = '';
	
	if(iniParam.cityArea != ''){
		$("#reg_id_city_area").val(iniParam.cityArea);
	}
	$("#reg_id_nat_area").val(iniParam.natArea);
	$("#reg_id_category").val(iniParam.category);
	$("#reg_id_zone_area").val(iniParam.zone);
	$("#reg_year").val($("#search_year").val());
	
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getDataAjax.do";
	//var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
			console.log("content페이지 openAjaxInput 수정모달 수신")
			console.log(e)
	    	drawInput(e);
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function drawInput(e){
	var sel_year = $("#search_year").val();
	endProcessing();
	if(e.ret==1){
		alertOpen('실패하였습니다.');
	}else{
		if(e.rowlist.length!=0){
			var html="";
			multi_file_map.clear();
    		$.each(e.rowlist,function(i,d){
    			if(d.row_type == 'c'){
    				var row1 = d.row_1;
    				var row2 = d.row_2;
	    			html+='	<div class="">';
	    			html+='		<div class="">';
	    			html+='			<div class="select-label">'+ wordBreak(row1.unit_nm) +'</div>';
	    			html+='			<div class="form-group">';
	    			if(nanToStr(row1.val)!=""){
		    			html+='			<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
	    			}
	    			if(nanToStr(row1.type)=='N'){
	    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
		    			html+='			<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
	    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
	    				var funcParam = ','+nanToStr(row1.decimal_len);
		    			html+='			<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
	    			}else{
		    			html+='			<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" >';
	    			}
	    			html+='			</div>';
	    			html+='		</div>';
	    			if(row2!=undefined){
		    			html+='	<div>';
		    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
		    			html+='         <div class="form-group">';
		    			if(nanToStr(row2.val)!=""){
			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
		    			}
		    			if(nanToStr(row2.type)=='N'){
		    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
		    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
		    				var funcParam = ','+nanToStr(row2.decimal_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
		    			}else{
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" >';
		    			}
		    			html+='</div>';
		    			html+='</div>';
	    			}
	    			html+='</div>';
    			}else if(d.data_type == '7'){
    				var child = d.child;
	    			if(child.length > 0) {
						html+='<div>';
		    			html+='	<div class="multi-row-wrap">';
		    			html+='	  <div class="select-label multi-row-header">'+wordBreak(d.unit_nm)+'</div>';
		    			html+='         <div class="form-group">';
		    			html+='           	<input type="hidden" name="sp_'+sel_year+'_'+d.id_meta+'" value="'+nanToStr(d.id_meta)+'"/>';
		    			html+='<div class="child_table">';
	    				$.each(child,function(i,c){
		    				var row1 = c.row_1;
		    				var row2 = c.row_2;
			    			html+='<div>';
			    			html+='<div>';
			    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
			    			html+='<div class="form-group">';
			    			if(nanToStr(row1.val)!=""){
				    			html+='           	<input type="hidden" name="di_'+sel_year+'_'+row1.id_meta_data+'_'+row1.id_meta_unit+'" id="di_'+sel_year+'_'+row1.id_meta_data+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'"/>';
			    			}
			    			if(nanToStr(row1.type)=='N'){
			    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
			    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
			    				var funcParam = ','+nanToStr(row1.decimal_len);
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
			    			}else{
				    			html+='<input type="text"  name="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" id="d_'+sel_year+'_'+row1.id_meta+'_'+row1.id_meta_unit+'" value="'+nanToStr(row1.val)+'" >';
			    			}
			    			html+='</div>';
			    			html+='</div>';
			    			if(row2!=undefined){
				    			html+='	<div>';
				    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
				    			html+='         <div class="form-group">';
				    			if(nanToStr(row2.val)!=""){
					    			html+='           	<input type="hidden" name="di_'+sel_year+'_'+row2.id_meta_data+'_'+row2.id_meta_unit+'" id="di_'+sel_year+'_'+row2.id_meta_data+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'"/>';
				    			}
				    			if(nanToStr(row2.type)=='N'){
				    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
					    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
				    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
				    				var funcParam = ','+nanToStr(row2.decimal_len);
					    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
				    			}else{
					    			html+='<input type="text"  name="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" id="d_'+sel_year+'_'+row2.id_meta+'_'+row2.id_meta_unit+'" value="'+nanToStr(row2.val)+'" >';
				    			}
				    			html+='</div>';
				    			html+='</div>';
			    			}
			    			html+='</div>';
	    				});
		    			html+='</div>';
		    			html+='</div>';
		    			html+='</div>';
		    			html+='</div>';
					}
    			}else if(d.row_type == 'f'){
    				var row1 = d.row_1;
	    			html+='<div>';
	    			html+='	<div id="file_div_'+row1.id_meta+'" class="single-file-row">';
	    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
	    			html+='         <div class="form-group">';
	    			if(nanToStr(row1.id_meta_data)!=''){
			    		html+='			<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
	    			}
	    			html+='           	<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'">';
					html+='				<button type="button" class="btn-primary" onclick="javascript:fileAttachBtn();" title="첨부">';
					html+='					<span class="glyphicon glyphicon-paperclip"></span>';
					html+='					첨부';
					html+='				</button>';
	    			html+='           	<input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
	    			
	    			html+='         </div>';
	    			html+='	  </div>';
	    			if(nanToStr(row1.addfilelist)!=''){
	    				var file = row1.addfilelist[0];
		    			html+='	<div id="fe_div_'+row1.id_meta+'">';
		    			html+='	  <div class="select-label">첨부된 파일</div>';
		    			html+='   <div class="form-group">';
		    			html+='         <input type="hidden" name="fi_'+sel_year+'_'+row1.id_meta_data+'" id="fi_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.id_meta_data)+'"/>';
		    			html+='         <a href="'+contextPath+'/file/download_add.do?id_data_file='+nanToStr(file.id_data_file)+'">';
						html+='				<span>'+ file.file_nm +'</span>';
						html+='				<span class="glyphicon glyphicon-download-alt"></span>';
		    			html+='         </a>';  
		    			html+='   </div>';
		    			html+='</div>';
	    			}
	    			html+='</div>';
    				
    			}else if(d.row_type == 'm'){
    				var row1 = d.row_1;
    				var multi_file_idx = 1;
	    			if(nanToStr(row1.addfilelist)!=''){
	    				multi_file_idx=row1.addfilelist.length+1;
	    			}
    				multi_file_map.set(row1.id_meta, multi_file_idx);
	    			html+='<div>';
	    			html+='	<div id="file_div_'+row1.id_meta+'" class="single-file-row" style="border-bottom:0;">';
	    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
	    			html+='   <div class="form-group">';
	    			html+='   	<button type="button" class="btn-primary" onclick="javascript:addMuitiFileRow('+sel_year+','+row1.id_meta+');">';
	    			html+='   		<span class="glyphicon glyphicon-plus"></span>추가';
    				html+='   	</button>';
//	    			html+='     <input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
					html+='   </div>';
					html+=' </div>';
					html+='</div>';
//					if(nanToStr(row1.id_meta_data)!=''){
//			    		html+='			<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
//	    			}
//	    			html+='           	<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" placeholder="파일을 등록하세요.">';
//					html+='				<button type="button" class="btn-primary" onclick="javascript:fileAttachBtn();" title="첨부">';
//					html+='					<span class="glyphicon glyphicon-paperclip"></span>';
//					html+='					첨부';
//					html+='				</button>';
//	    			html+='           	<input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
//	    			html+='				<input class="fileDesc" type="text" disabled="" value="네트워크">';		
//	    			html+='         </div>';
//	    			html+='	  </div>';
	    			html+='<div class="modal-input-area" style="min-height:0;border-bottom:1px solid lightgray;">';
	    			html+='<table class="file-list-table">';
	    			html+='	<colgroup>';
	    			html+='		<col width="40%">';
	    			html+='		<col width="30%">';
	    			html+='		<col width="30%">';
	    			html+='	</colgroup>';
	    			html+='	<tbody id="file_list_tab_'+row1.id_meta+'">';
//				</div>
	    			if(nanToStr(row1.addfilelist)!=''){
	    				$.each(row1.addfilelist,function(i,d){
		    				html+=' <tr class="file-list-row-wrap" id="mf_'+row1.id_meta+'_'+(i+1)+'">';
		    				html+='		<td>';
		    				html+='			<div class="file-upload-group">';
		    				html+='				<input class="fileName" type="text" disabled value="'+nanToStr(d.file_nm)+'" >';
		    				html+='				<button class="btn-primary" type="button" title="첨부">';
		    				html+='					<span class="glyphicon glyphicon-paperclip"></span>';
		    				html+='					첨부';
		    				html+='				</button>';
			    			html+='           	<input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
		    				html+='			</div>';
		    				html+='		</td>';
		    				html+='		<td>';
		    				html+='			<input class="fileDesc" type="text" disabled value="'+nanToStr(d.file_desc)+'">';
		    				html+='		</td>';
		    				html+='		<td>';
		    				html+='			<div class="multi-btn-area">'
		    				html+='				<div class="multi-btn-wrap">'
		    				html+='					<div class="btn-wrap">'
		    				html+='						<a href="downloadFile('+d.id_data_file+')" download="'+nanToStr(d.file_nm)+'">';
		    				html+='							<span class="glyphicon glyphicon-download-alt"></span>';
		    				html+='						</a>';
		    				html+='					</div>';
		    				html+='					<div class="btn-wrap">'
		    				html+='						<a href="javascript:void(0);" onClick="javascript:modifyRow('+ (i+1) +');">';
		    				html+='							<span class="glyphicon glyphicon-pencil" title="수정"></span>';
		    				html+='						</a>';
		    				html+='					</div>';
		    				html+='					<div class="btn-wrap">'
		    				html+='						<a href="javascript:void(0);" onClick="javascript:delRow('+d.id_data_file+','+ row1.id_meta+'_'+(i+1) +');">';
		    				html+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
		    				html+='						</a>';
		    				html+='					</div>';
		    				html+='				</div>';
		    				html+='			</div>';
		    				html+='		</td>';
		    				html+='	</tr>';
	    				});
	    			}
	    			html+='	</tbody>';
	    			html+='</table>'
//	    			html+='	</div>';
//	    			html+='	</div>';
	    			html+='</div>';
    			}else{
    				var child = d.child;
	    			html+='<div>';
	    			html+='	<div class="multi-row-wrap">';
	    			html+='	  <div class="select-label multi-row-header">'+wordBreak(d.unit_nm)+'</div>';
	    			html+='         <div class="form-group">';
	    			html+='<div class="child_table">';
    				$.each(child,function(i,c){
	    				var row1 = c.row_1;
	    				var row2 = c.row_2;
		    			html+='<div>';
		    			html+='<div>';
		    			html+='	  <div class="select-label">'+wordBreak(row1.unit_nm)+'</div>';
		    			html+='<div class="form-group">';
		    			if(nanToStr(row1.val)!=""){
			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
		    			}
		    			if(nanToStr(row1.type)=='N'){
		    				var funcParam = ','+nanToStr(row1.decimal_len)+','+nanToNum(row1.scale_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
		    			}else if(nanToStr(row1.type)=='C' && nanToStr(row1.data_type)=='1'){
		    				var funcParam = ','+nanToStr(row1.decimal_len);
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
		    			}else{
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" >';
		    			}
		    			html+='</div>';
		    			html+='</div>';
		    			if(row2!=undefined){
			    			html+='	<div>';
			    			html+='	  <div class="select-label">'+wordBreak(row2.unit_nm)+'</div>';
			    			html+='         <div class="form-group">';
			    			if(nanToStr(row2.val)!=""){
				    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
			    			}
			    			if(nanToStr(row2.type)=='N'){
			    				var funcParam = ','+nanToStr(row2.decimal_len)+','+nanToNum(row2.scale_len);
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" dataNumOnly="1" onkeyup="javascript:checkNumberFormat(this'+funcParam+')">';
			    			}else if(nanToStr(row2.type)=='C' && nanToStr(row2.data_type)=='1'){
			    				var funcParam = ','+nanToStr(row2.decimal_len);
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" onkeyup="checkStringFormat(this'+funcParam+')" placeholder="문자만 입력하십시오">';
			    			}else{
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" >';
			    			}
			    			html+='</div>';
			    			html+='</div>';
		    			}
		    			html+='</div>';
    				});
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
	    			html+='</div>';
    			}
    		});
			$("#input-content").empty();
			$("#input-content").append(html);
			$("#previewModal").modal('show');	
		}
	}
	
}
function addMuitiFileRow(sel_year,id_meta){
	var new_num = multi_file_map.get(id_meta)+1;
	multi_file_map.set(id_meta, new_num);
	var html='';
	html+=' <tr class="file-list-row-wrap" id="mf_'+id_meta+'_'+new_num+'">';
	html+='		<td>';
	html+='			<div class="file-upload-group">';
	html+='				<input class="fileName" type="text" disabled value="" id="mf1_'+id_meta+'_'+new_num+'">';
	html+='				<button class="btn-primary" type="button" onclick="javascript:fileAttachBtn();" title="첨부">';
	html+='					<span class="glyphicon glyphicon-paperclip"></span>';
	html+='					첨부';
	html+='				</button>'; 
//	html+='           	<input type="file" name="f_'+sel_year+'_'+id_meta+'_'+id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+id_meta+'_'+id_meta+'\').value = this.value"/>';
	html+='           	<input type="file" name="mf1_'+sel_year+'_'+id_meta+'_'+new_num+'" class="file-input-hidden" onchange="javascript: multiFileInput(this,\'mf1_'+id_meta+'_'+new_num+'\');"/>';
	html+='			</div>';
	html+='		</td>';
	html+='		<td>';
	html+='			<input class="fileDesc" type="text" name="mf2_'+sel_year+'_'+id_meta+'_'+new_num+'" id="mf2_'+id_meta+'_'+new_num+'" disabled value="">';
	html+='		</td>';
	html+='		<td>';
	html+='			<div class="multi-btn-area">'
	html+='				<div class="multi-btn-wrap">'
	html+='					<div class="btn-wrap"></div>';
	html+='					<div class="btn-wrap"></div>';
	html+='					<div class="btn-wrap">'
	html+='						<a href="javascript:void(0);" onClick="javascript:delRow(\'\',\''+ id_meta+'_'+new_num +'\');">';
	html+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
	html+='						</a>';
	html+='					</div>';
	html+='				</div>';
	html+='			</div>';
	html+='		</td>';
	html+='	</tr>';
	$("#file_list_tab_"+id_meta).append(html);
}
function multiFileInput(that,id){
	document.getElementById(id).value = that.value;
	var idSplit = id.split("_");
//	iniParam.meta = idSplit[2];
	$("#mf2_"+idSplit[1]+"_"+idSplit[2]).prop("disabled",false);
}
function delRow(id_data_file,idx){
	if(nanToStr(id_data_file)!=''){
		var params = {id_data_file:id_data_file};
		var url = contextPath+"/deleteMultiFileDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		$("#mf_"+idx).remove();
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		
	}else{
		$("#mf_"+idx).remove();
	}
}
function detailAjaxInput(){
	if(iniParam.cityArea != ''){
		$("#reg_id_city_area").val(iniParam.cityArea);
		$("#reg_id_nat_area").val(iniParam.natArea);
	} else{
		$("#reg_id_nat_area").val(iniParam.natArea);
		$("#reg_id_city_area").val('');
	}
	$("#reg_id_category").val(iniParam.category);
//	$("#reg_id_meta").val(iniParam.meta);
	$("#reg_year").val($("#search_year").val());
	
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getDataByIdMetaAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
			console.log("detailAjaxInput 수신내용");
			console.log(e);
	    	drawInput(e);
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function wordBreak(string) {
	var returnStr = string;
	if((returnStr.indexOf("(") != -1) && (returnStr.indexOf(")") != -1)) {
		var stringArr = returnStr.split("(");
		var newString = "";
		for(var i=0; i<stringArr.length; i++) {
			if(i != 0) {
				newString = newString + "<span>(" + stringArr[i] + "</span>";
			} else {
				newString = newString + "<span>" + stringArr[i] + "</span>";
			}
		}
		returnStr = "<div>" + newString + "</div>";
	} else {
		returnStr = "<div><span>" + returnStr + "</span></div>";
	}
	return returnStr;
}
//추가. fileInput click : 버튼의 다음에 있는 요소를 클릭한다. 버튼다음에 input을 배치할 것.
function fileAttachBtn() {
	var btnElement = event.target;
	if(btnElement.tagName != 'BUTTON') {
		btnElement = btnElement.parentNode;
		if(btnElement.tagName != 'BUTTON') return;
	}
	var fileElement = btnElement.nextElementSibling;
	fileElement.click();
}