<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<his:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_register.js'/>"></script>
</his:DataHead>
<his:DataTreeBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" hasLeft="true" type="${(empty param.type?'3':param.type)}" title="${master.title} ${master.board_nm}">
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;">
<%--                  <input type="hidden" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"> --%>
	                <input type="hidden" name="id_master" value="${param.id_master}">
	                <input type="hidden" name="id_board_master" value="${param.id_board_master}">
	                <input type="hidden" name="id_board" id="id_board" value="${param.id_board}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
                           <div class="row">
                               <div class="col-12">
                               <div class="tit_cont">제목</div>
                                 <div class="form-group">
                                   <input type="text" class="form-control mh-form-control" name="title" id="title" value="${map.title}">
                                 </div>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-12">
                               <div class="tit_cont">내용</div>
                                 <div class="form-group">
									<textarea class="form-control mh-form-control" name="comment" id="comment" rows="6" style="height:240px;">${map.comment}</textarea>
                                 </div>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-12">
                               <div class="tit_cont">키워드</div>
                                 <div class="form-group">
                                   <input type="text" class="form-control mh-form-control" name="keyword" id="keyword" value="${map.keyword}">
                                 </div>
                               </div>
                           </div>
                           <div class="row" id="file_list_div">
							 <h3 class="h3_tit col-12" id="file_btn_group">
								첨부파일 리스트<span class="star">*</span>
								<a href="javascript:void(0);" class="btn_add" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_add.png'/>" alt=""/>추가</a>
							</h3>
                            <div class="col-8 line" id="addFile_in_div_1">
                                <div class="form-group inline-group">
                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>
                                    <div class="attach-btn">
                                      <button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>
                                      <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
                                    </div>
                                </div>
                            </div>
							<div class="col-4 line" id="delFile_in_div_1" style="text-align:right;">
                                <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile(1);"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
                            </div>
                            <c:forEach var="file" items="${addFileList}" varStatus="i">
							<div class="col-8 line" id="attach_${file.id_board_file}">
                                <div class="form-group inline-group">
                                    <a href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}" class="ico_download" style="color:#fff;"><img src="<c:url value='/resource/img/history/ico_download.png'/>" alt="다운로드"/>다운로드</a> 
									<span>${file.file_nm}</span>
                               </div>
                            </div>
							<div class="col-4 line" id="attach_${file.id_board_file}_add" style="text-align:right;">
                                <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttachFile(${file.id_board_file});"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
                            </div>
                            </c:forEach>
                          </div>
			</form>
            <div class="btn-area mb-0p">
                <button type="button" class="btn back" id="btnDelete" onclick="delData();" style="display:${(not empty map.id_board?'block':'none')};">삭제</button>
                <button type="button" class="btn" id="btnSave" onclick="register();">저장</button>
                <button type="button" class="btn list" id="btnList" onclick="doList();">리스트</button>
            </div>	
</his:DataTreeBody>
<script>
var cur_file_num=1;
function addFile(){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
	var html ='<div class="col-8 line" id="addFile_in_div_'+file_num+'">';
	html+='		<div class="form-group inline-group">';
	html+='		   <input type="text" id="fileName'+file_num+'" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>';
	html+='		   <div class="attach-btn">';
	html+='			<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
	html+='		    <input type="file" name="file_'+file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+file_num+'\').value = this.value"/>';
	html+='		   </div>';
	html+='		</div>';
	html+='		</div>';
	html+='		   <div class="col-4 line" id="delFile_in_div_'+file_num+'" style="text-align:right;">';
	html+='			<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile('+file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png'+'" alt="delete"/></a>';
	html+='		  </div>';
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==0){
		$("#file_btn_group").after(html);
	}else{
		$("#delFile_in_div_"+bef_file_num).after(html);
	}
}

var attach_idx=0;
function delAttach(idx){
	attach_idx=idx;
	confirmOpen('삭제시 복구할 수 없습니다.',delAttachFile);
}
function delAttachFile(id_board_file){
	var params = {id_board_file : id_board_file};
	processing();
	$.ajax({
		url: contextPath+"/deleteHistoryFileAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret == '${GlobalConst.ACT_SUCCESS}'){
	     	   $("#attach_"+id_board_file).remove();
	    	   $("#attach_"+id_board_file+"_add").remove();
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}
function delFile(idx){
   $("#addFile_in_div_"+idx).remove();
   $("#delFile_in_div_"+idx).remove();
}

function registerData(){
	$("#dataModal").modal('show');	
}

function changeCategory(that){
	var new_val = $(that).val();
	search();
}
$('#dataModal').on('hidden.bs.modal', function () {
	refresh();
});
$('#dataViewModal').on('hidden.bs.modal', function () {
	refresh();
});
function refresh(){
	var frm =document.frm;
	frm.action=contextPath+"/history.do";
		$("#page").val(page);
//	frm.action="/excelDownload.do";
	$("#frm").submit();
	
}
function doList(){
	var frm =document.frm;
		frm.action=contextPath+"/history.do";
 //  		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function download(id_board_file){
	
}
</script>
</html>