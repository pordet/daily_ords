package daily.common.base;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import daily.common.test.AbstractJUnit4TestBase;

@RunWith(SpringJUnit4ClassRunner.class)
//ApplicationContext will be loaded from "classpath:/com/mrhaki/spring/test/ContextTest-context.xml"
@ContextConfiguration(locations= {"classpath*:/spring/context-common.xml"
		,"classpath*:/spring/context-datasource.xml","classpath*:/spring/context-mybatis.xml"
		,"classpath*:/spring/context-properties.xml","classpath*:/spring/test-context-transaction.xml"})
public class AbstractJUnit4ContextTestBase extends AbstractJUnit4TestBase {

}
