package kr.co.daily.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.IdentityMetaManageDAO;
import kr.co.daily.admin.dao.MetaManageDAO;
import kr.co.daily.admin.domain.LineVO;
import kr.co.daily.admin.domain.RelColVO;
import kr.co.daily.admin.domain.RelationVO;
import kr.co.daily.admin.domain.SvgVO;
import kr.co.daily.admin.domain.TableDataVO;
import kr.co.daily.common.GlobalConst;

@Service
public class IdenetityMetaManageService {
	@Autowired IdentityMetaManageDAO dao;

	public PageList<CustomMap> selectMetaRelMList(CommandMap map) {
		return dao.selectMetaRelMList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}
	
	public PageList<CustomMap> selectMetaMList(CommandMap map) {
		return dao.selectMetaMList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public CustomMap getMetaMByIdMetaM(CommandMap map) {
		return dao.getMetaMByIdMetaM(map.getMap());
	}

	public List<CustomMap> selectMetaListByIdMetaM(CommandMap map) {
		return dao.selectMetaListByIdMetaM(map.getMap());
	}

	public void insertRelation(CommandMap map, RelationVO data) throws Exception{
		dao.insertIdentityMetaRelM(map.getMap());
		if("".equals(map.getString("id_rel_m",""))) {
			throw new Exception("id_rel_m 키 생성 실패");
		}
    	Map<String, TableDataVO> tabDatas = data.getTableData();
    	for(String tab_id:tabDatas.keySet()) {
    		TableDataVO tab = tabDatas.get(tab_id);
    		tab.setId_rel_m(map.getString("id_rel_m",""));
    		dao.insertIdentityMetaFkPosi(tab);
    	}
    	Map<String, SvgVO> svgDatas = data.getSvgData();
    	Map<String, LineVO> lineDatas = data.getLineData();
    	Map<String, RelColVO> relColDatas = data.getRelColData();
    	for(String rel_id:relColDatas.keySet()) {
    		RelColVO relCol = relColDatas.get(rel_id);
    		relCol.setId_rel_m(map.getString("id_rel_m",""));
    		relCol.setId_rel(rel_id);
    		dao.insertIdentityMetaRel(relCol);
    		String svg_id = "svg_"+rel_id;
    		SvgVO srcVO = svgDatas.get(svg_id);
    		if(srcVO!=null) {
    			srcVO.setId_rel(svg_id);
    			srcVO.setId_meta_rel(relCol.getId_meta_rel());
        		dao.insertIdentityMetaFkSvg(srcVO);
    		}
    		LineVO lineVO = lineDatas.get(svg_id);
    		if(lineVO!=null) {
    			lineVO.setId_rel(svg_id);
    			lineVO.setId_meta_rel(relCol.getId_meta_rel());
        		dao.insertIdentityMetaFkLine(lineVO);
     		}
    	}
		
	}

	public void modifyRelation(CommandMap commandMap, RelationVO data) {
		// TODO Auto-generated method stub
		
	}

	public void deleteRelation(CommandMap commandMap) {
		// TODO Auto-generated method stub
		
	}

	public List<CustomMap> selectIdentityMetaFkPosi(CommandMap map) {
		return dao.selectIdentityMetaFkPosi(map.getMap());
	}

	public List<CustomMap> selectIdentityMetaFkSvg(CommandMap map) {
		return dao.selectIdentityMetaFkSvg(map.getMap());
	}

	public List<CustomMap> selectIdentityMetaFkLine(CommandMap map) {
		return dao.selectIdentityMetaFkLine(map.getMap());
	}

	public List<CustomMap> selectIdentityMetaFkM(CommandMap map) {
		return dao.selectIdentityMetaFkM(map.getMap());
	}

	public Map<String, List<CustomMap>> selectIdentityMetaList(CommandMap map) {
		Map<String,List<CustomMap>> ret = new HashMap<>();
		List<CustomMap> colList = dao.selectIdentityMetaList(map.getMap());
		for(CustomMap col : colList) {
			String id_meta_m = col.getString("id_meta_m");
			List<CustomMap> metaColList = ret.get(id_meta_m);
			if(metaColList==null) {
				List<CustomMap> inList = new ArrayList<>();
				inList.add(col);
				ret.put(id_meta_m, inList);
			}else {
				metaColList.add(col);
				ret.put(id_meta_m, metaColList);
			}
		}
		return ret;
	}




}
