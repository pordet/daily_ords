package daily.common.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.FileCopyUtils;

public class FileIOUtil {
	/**
	 * 프로젝트 배포 폴더의 최상위 폴더 패스를 얻는다.
	 * @return 최상위 패스
	 */
	public static String getRootPath() {
		String rootPath = System.getProperty("user.dir");
		return rootPath;
	}

	/**
	 * 파일을 삭제한다. 폴더인 경우는 하위 폴더와 파일을 모두 삭제한다.
	 * @param element
	 */
	public static void deleteFile(File element) {
	    if(element.isDirectory()) {
	        for(File sub : element.listFiles()) {
	            deleteFile(sub);
	        }
	    }
	    element.delete();
	}
	public static void deleteFile(String path) {
		File element = new File(path);
		deleteFile(element);
	}

	//	see EgovFileTool.copyFile(srcPath, dstPath)
	//	폴더 패스까지 생성해 준다.
	public static void copyFile(String srcPath, String dstPath) {
		try {
			File srcFile = new File(srcPath);
			File dstFile = new File(dstPath);
			FileCopyUtils.copy(srcFile, dstFile);
		} catch (Exception e) {
//			log.error("[copyFile] Fail. err=" + e.getMessage());
		}
	}

	public static boolean deletePath(String filePath) {
		Boolean ret = false;
		File folder = new File(filePath);
		try {
		    while(folder.exists()) {
		    	File[] folder_list = folder.listFiles(); //파일리스트 얻어오기
					
		    	for (int j = 0; j < folder_list.length; j++) {
					folder_list[j].delete(); //파일 삭제 
					System.out.println("파일이 삭제되었습니다.");
							
				}
					
				if(folder_list.length == 0 && folder.isDirectory()){ 
					folder.delete(); //대상폴더 삭제
					System.out.println("폴더가 삭제되었습니다.");
				}
	        }
		    ret = true;
		}catch (Exception e) {
		}		
		return ret;
	}
	
	public static boolean checkFileExst(String dir, String file) {
		Boolean ok = false;
		try {
//			ok = EgovFileTool.checkFileExstByName(dir, file);
		} catch (Exception e) { }
		return ok;
	}
	
	/**
	 * 파일명에서 패스와 확장자를 제외한 파일명을 얻는다.
	 * @param fileName 파일명 또는 파일패스
	 * @return 확장자를 제외한 파일명
	 */
	public static String getPureFileName(String fileName) {
		int idx1 = fileName.lastIndexOf("/");
		int idx2 = fileName.lastIndexOf(".");
		String fName = fileName.substring(idx1 == -1 ? 0 : idx1, idx2 == -1 ? fileName.length() : idx2);
		return fName;
	}

	/**
	 * 파일패스명에 대해 확장자를 얻는다. 확장자가 없는 경우 ""로 리턴
	 * @param fileName
	 * @return
	 */
	public static String getExtension(String fileName) {
		int idx = fileName.lastIndexOf(".");
		String extension = idx == -1 ? "" : fileName.substring(idx+1);
		return extension;
	}

	/**
	 * 파일패스에 대한 변경된 파일 확장자의 파일패스를 리턴한다.
	 * 	확장자가 없는 경우는 확장자를 붙여 준다.
	 * @param filePath
	 * @param extension
	 * @return
	 */
	public static String changeFileExtension(String filePath, String extension) {
		int idx = filePath.lastIndexOf(".");
		String fPath = "";
		if(idx == -1) {
			fPath = filePath + "." + extension;
		} else {
			fPath = filePath.substring(0, idx+1) + extension;
		}
		return fPath;
	}

	/**
	 * 파일명에 suffix를 붙여준다. (기존파일명 + "_" + suffix + ".확장자") 
	 * @param filePath
	 * @param suffix
	 * @return
	 */
	public static String appendNameSuffix(String filePath, String suffix) {
		int idx = filePath.lastIndexOf(".");
		String fPath = "";
		if(idx == -1) {
			fPath = filePath + "_" + suffix;
		} else {
			fPath = filePath.substring(0, idx) + "_" + suffix + filePath.substring(idx);
		}
		return fPath;
	}

	/**
	 * index(0부터) 증가 시키면서 존재하지 않는 폴더명을 얻는다. 
	 * @param dirPath 
	 * @param bindata 폴더명 prefix (예, "bindata")
	 * @return prefix에 index가 붙는 폴더 명 (예, "bindata1") 
	 */
	public static String getUniqueBindataFolder(String dirPath, String prefix) {
		File dir = null;
		String retPath = null;
		int i = 0;
		do {
			retPath = prefix + (i == 0 ? "" : String.valueOf(i));
			dir = new File(dirPath + "/" + retPath);
			i++;
		} while(dir.exists());
		return retPath;
	}

	/**
	 * 대상 폴더 내의 대상 확장자(들) 파일을 삭제한다.
	 * 	(*1) static 매소드 내에서 class instance 사용하기 위해 미리 생성해야 한다. (http://mainia.tistory.com/2675)
	 * @param dirPath 삭제 대상 파일이 있는 폴더 패스
	 * @param ext 삭제대상 확장자. 복수 경우는 comma 구분. (예, "bmp,pcx")
	 */
	public static void deleteFilesExtension(String dirPath, String ext) {
		ExtensionFilter filter = new ExtensionFilter(ext); //(*1)
		File dir = new File(dirPath);
	
		String[] list = dir.list(filter);
		File file;
		if(list.length == 0) return;
	
	     //파일이 있는 만큼
		for(int i = 0; i < list.length; i++) {
			file = new File(dirPath + "/" + list[i]);
			boolean isdeleted = file.delete();
			if(! isdeleted) {
//				log.error("[deleteFilesExtension] delete failure. file=" + file.getName() + ", dir=" + dirPath);
			}
		}
	}
	static class ExtensionFilter implements FilenameFilter { //(*1)
		private List<String> extension;
		public ExtensionFilter(String extension) {
			this.extension = new ArrayList<String>(Arrays.asList(extension.split(",")));
		}
		public boolean accept(File dir, String name) {
	    	for(String e : extension) {
	    		if(name.endsWith(e)) return true;
	    	}
			return false;
		}
	}

//	public static File getUniqueFile(final File file) {
//	if (!file.exists())
//		return file;
//
//	File tmpFile = new File(file.getAbsolutePath());
//	File parentDir = tmpFile.getParentFile();
//	int count = 1;
//	String extension = FilenameUtils.getExtension(tmpFile.getName());
//	String baseName = FilenameUtils.getBaseName(tmpFile.getName());
//	do {
//		tmpFile = new File(parentDir, baseName + "_" + count++ + "_." + extension);
//	} while (tmpFile.exists());
//	return tmpFile;
//}
}
