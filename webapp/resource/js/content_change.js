/*
function goDataMap(){
	window.open(contextPath+"/navi_map.do")
}
function changeCategory(that){
	var new_val = $(that).val();	
	if(new_val=="4" && $("#id_city_area").val()!=''){
		showZoneSelect(new_val);
	}else if(new_val=="4" && $("#id_city_area").val()==''){
			showZoneSelect(new_val);
	}else{
		$("#id_zone").val('');
		$("#id_meta_select_div").hide();
	}
}
function changeNation(that){
	var new_val = $(that).val();
	var params = {id_parent_area:new_val};
	var url = contextPath+"/adm/data/selectAreaDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.list.length!=0){
	    			var html='<option value="">선택</option>';
		    		$.each(e.list,function(i,d){
		    			html+='<option value="'+d.id_area+'">'+d.area_nm+'</option>';
		    			$("#id_city_area").html(html);
		    		});
	    		}
	    		if($("#id_category").val()==4){
	    			showZoneSelect($("#id_category").val());
	    		}else{
	    			$("#id_zone").val('');
	    			$("#id_meta_select_div").hide();
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});

// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val($("#id_nat_area").val());
// 	}
// 	if($("#id_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
// 	search();
	
}
function changeCity(that){
	var id_category=$("#id_category").val();
	var new_val = $(that).val();	
	if(new_val==''){
		$("#id_zone").val('');
		$("#id_meta_select_div").hide();
		if($("#id_category").val()=='4'){
			showZoneSelect(id_category);
		}
	}else{
		showZoneSelect(id_category);
	}
}

*/
function changeCategory(that){
	var new_val = $(that).val();	
//	$("#id_zone").val('');
//	$("#id_meta_select_div").hide();
}
function changeNation(that){
	var new_val = $(that).val();
	var params = {id_parent_area:new_val};
	var url = contextPath+"/adm/data/selectAreaDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.list.length!=0){
	    			var html='<option value="">선택</option>';
		    		$.each(e.list,function(i,d){
		    			html+='<option value="'+d.id_area+'">'+d.area_nm+'</option>';
		    			$("#id_city_area").html(html);
		    		});
	    		}
    			$("#id_zone").val('');
//    			$("#id_meta_select_div").hide();
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
	
}

function changeCity(that){
	var id_category=$("#id_category").val();
	var new_val = $(that).val();	
	if(new_val==''){
		$("#id_zone").val('');
	}else{
		showZoneSelect(id_category);
	}
}

function showZoneSelect(id_category){
	var id_nat_area = $("#id_nat_area").val();
	var id_city_area = $("#id_city_area").val();
	if(id_category=="4"){
		var params = {id_category:id_category,id_nat_area:id_nat_area,id_city_area:id_city_area,id_zone:''};
		var url = contextPath+"/zoneDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		$("#id_zone").val('');
		    		$("#id_meta_select_div").hide();
		    	}else{
		    		if(nanToStr(e.itemList) !='' && e.itemList.length!=0){
		    			var nat_id_meta = nanToStr(e.nat_id_meta);
		    			if(nat_id_meta=='162'){
			    			var html='<option value="">선택</option>';
				    		$.each(e.itemList,function(i,d){
//				    			if(d.id_meta)
				    			if(d.id_meta ==nat_id_meta){
					    			html+='<option value="'+d.id_meta+'" selected>'+d.unit_nm+'</option>';
				    			}else{
					    			html+='<option value="'+d.id_meta+'">'+d.unit_nm+'</option>';
				    			}
				    		});
			    			$("#id_meta").html(html);
			    			$("#id_meta_select_div").show();
		    			}else{
			    			var html='<option value="" selected>선택</option>';
				    		$.each(e.itemList,function(i,d){
//				    			if(d.id_meta)
				    			html+='<option value="'+d.id_meta+'">'+d.unit_nm+'</option>';
				    		});
			    			$("#id_meta").html(html);
			    			$("#id_meta_select_div").show();
		    			}
		    		}
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		
	}
	
}
