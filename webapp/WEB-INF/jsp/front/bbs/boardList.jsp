<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.BBS_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="${bbs.bbs_nm} 조회"/>
<!DOCTYPE html>
<html lang="ko">
<front:FrontDynamicHead title="${pageTitle}">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
<c:choose>
	<c:when test="${!is_guest}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/bbs/bbs_register.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/bbs/bbs_input.js'/>"></script>
	</c:when>
	<c:otherwise>
		<script charset="UTF-8" src="<c:url value='/resource/js/front/bbs/bbs_input.js'/>"></script>
	</c:otherwise>
</c:choose>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
		<div class="page-title-wrap">
			<span>${bbs.bbs_nm}</span>
		</div>
		
		<form name="frm" id="frm" method="post">
			<div class="page-content-wrap">
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="id_meta_m" name="id_meta_m" value="${param.id_meta_m}"/>
				<input type="hidden" id="insert_yn" name="insert_yn"/>
				<input type="hidden" id="sys_code" name="sys_code" value="${param.sys_code}"/>
				<input type="hidden" name="is_guest" value="${(is_guest? 'Y':'N')}"/>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
							<div class="search-group-single-input">
				              	<div class="tit_cont">검색조건</div>
					          	<select class="form-control mh-form-control" name="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${headerList}" varStatus="i">
					                <option value="${map.id_meta}" ${(param.search_col eq map.id_meta? 'selected':'')}>${map.meta_nm}</option>
					          	</c:forEach>
					       		</select>
		              			<div class="tit_cont">검색어</div>
<%-- 		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${(empty param.search_keyword ?'':param.search_keyword)}"> --%>
								<input type="text" class="form-control mh-form-control" name="search_keyword" id="search_keyword" value="${paramMap.search_keyword}">
		              			<div class="tit_cont">검색기간</div>
		              			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
		              			-
		              			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
								<button type="button" class="btn-primary" onclick="javascript:search();">
									<span class="btn_search"></span>
									검색
								</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
					</div>
					<div class="r">
					<c:if test="${!is_guest}">
						<button type="button" class="btn h34 fl" onClick="javascript:openBoard(${param.id_meta_m});"><span class="btn_add"></span>수동입력</button>
					</c:if>
					</div>					
				</div>
	            <div class="grid-area">
	                   <!-- 그리드 해더영역 -->
	                   <div class="grid-header">
	                       <c:forEach var="map" items="${headerList}" varStatus="i">
	                       <div class="header-unit">
	                       <input type="hidden" id="h_${map.id_meta}" value="${map.id_meta}">
	                       ${map.meta_nm}<ctrl:orderMark val="${map.id_meta}" sidx="${param.sidx}" sord="${param.sord}"/>
	                       </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
	                       </c:forEach>
	                   </div>
	                   <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
	                   <div class="grid-contents">
	                   <c:forEach var="low" items="${list}" varStatus="i">
	                       <div class="grid-loof-cont ${(not empty low.id_meta_row?'editable':'')}" id="row_${i.count}" ondblclick="javascript:openBoard('${low.id_meta_m}','${low.id_meta_row}')">
	                       <c:forEach var="col" items="${headerList}" varStatus="j">
	                       	<c:set var="meta_cd" value="${col.meta_cd}"/>
	                        <c:set var="cont" value="${low[meta_cd]}"/>
	                        <div class="cont-unit">
	                        ${cont}
	          				</div>
	                   	</c:forEach>
	                       </div>
	                   </c:forEach>
	                   </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
	                   <!--//그리드영역-->
	                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
		  </div>
                   <!-- //paging -->
    	</form>
</front:FrontDynamicBody>
<script>
var listItem = document.querySelectorAll(".listItem");
for(var i=0; i<listItem.length; i++) {
	var boardItemArea = listItem[i];
	var thumbArea = boardItemArea.querySelector(".thumbArea");
	var videoElement = boardItemArea.querySelectorAll("[name=player]");
	if(videoElement.length > 0) {
		var firstVideo = videoElement[0];
		
		var videoString = firstVideo.innerHTML;
		var videoUrlArr = videoString.split('/');
		var videoUrl = videoUrlArr[videoUrlArr.length -1];
		var thumbUrl = "https://img.youtube.com/vi/"+videoUrl+"/mqdefault.jpg";
		var newThumb = document.createElement("img");
		newThumb.setAttribute("src", thumbUrl);
		newThumb.setAttribute("z-Index", "10");
		
		thumbArea.style.marginRight = "20px";
		thumbArea.appendChild(newThumb);
	}
}

function enterKey(){
	if(window.event.keyCode == 13){
		search();
	}
}
function openBoard(id_meta_m,id_meta_row){
	$("#id_meta_m").val(id_meta_m);
	if(id_meta_row){
		$("#id_meta_row").val(id_meta_row);
	}else{
		$("#id_meta_row").val('');
	}
	var frm =document.frm;
	<c:choose>
	<c:when test="${is_guest}">
		<c:set var="link" value="/front/bbs/boardDetail.do"/>
	</c:when>
	<c:otherwise>
		<c:set var="link" value="/adm/bbs/boardDetail.do"/>
	</c:otherwise>
	</c:choose>
		frm.action=contextPath+'${link}';
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}

</script>
</html>