package kr.co.daily.admin.domain;

public class TableDataVO {
	private String id_rel_m;
	private String id;
	private float top;
	private float left;
	public String getId_rel_m() {
		return id_rel_m;
	}
	public void setId_rel_m(String id_rel_m) {
		this.id_rel_m = id_rel_m;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getTop() {
		return top;
	}
	public void setTop(float top) {
		this.top = top;
	}
	public float getLeft() {
		return left;
	}
	public void setLeft(float left) {
		this.left = left;
	}
	
}
