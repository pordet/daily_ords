package daily.common.service;

import daily.common.vo.CommandMap;

public interface SendMailService {
	public void sendMail(CommandMap map);
}
