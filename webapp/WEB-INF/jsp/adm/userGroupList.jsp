<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자 그룹 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
<!--
			<div class="tab-list tab-layer col-2">
                <ul id="usrTab">
                    <li>
                        <a id="tab_layer_1" href="javascript:void(0);">사용자 관리</a>
                    </li>
                    <li class="on">
                        <a id="tab_layer_2" href="javascript:void(0);">사용자 그룹 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_3" href="javascript:void(0);">Role 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_4" href="javascript:void(0);">권한 관리</a>
                    </li>
                </ul>
            </div>
            <div class="tab-wrap">
				<div id="tab_layer_1" class="tab-cont">
	            </div>
				<div id="tab_layer_2" class="tab-cont on">
	                       <div class="code-manag-btn">
								<button type="button" class="btn h34 fl" onClick="javascript:openRegUserGroup();"><span class="btn_add"></span>그룹 등록</button>
	                       </div>
								<form name="frm" id="frm">
				                    <div class="grid-area">
				                        <div class="grid-header">
				                            <div class="header-unit" style="width:25%"><a href="#">그룹 ID<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:25%"><a href="#">그룹 이름<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:25%"><a href="#">그룹 설명<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:25%"><a href="#">사용여부<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                        </div>
				                        <div class="grid-contents">
				                            <c:forEach var="map" items="${list}" varStatus="i">
				                            <div class="grid-loof-cont" ondblclick="javascript:openRegUserGroup('${map.id_group}')">
				                                <div class="cont-unit" style="width:25%">${map.id_group}</div>
				                                <div class="cont-unit" style="width:25%">${map.group_nm}</div>
				                                <div class="cont-unit" style="width:25%">${map.group_desc}</div>
				                                <div class="cont-unit" style="width:25%">${map.use_yn}</div>
				                            </div>
				                            </c:forEach>
				                        </div>
				                    </div>
									<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
								</form>
	        	</div>
				<div id="tab_layer_3" class="tab-cont">
	            </div>
				<div id="tab_layer_4" class="tab-cont">
	            </div>
    </div>
-->
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<div class="dbBtnbox">
					<div class="l"></div>
					<div class="r">
						<button type="button" class="btn-primary" onClick="javascript:openRegUserGroup();">
							그룹 등록
						</button>
						<button type="button" class="btn-danger" onClick="javascript:deleteList();return false;">
							그룹 삭제
						</button>
					</div>
				</div>
				<form name="frm" id="frm">
					
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit">
								<a href="#">
									그룹 ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									그룹 이름
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									그룹 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openRegUserGroup('${map.id_group}')">
									<div class="check-unit">
										<input type="checkbox" name="del_code" value="${map.id_group}">
									</div>
									<div class="cont-unit">${map.id_group}</div>
									<div class="cont-unit">${map.group_nm}</div>
									<div class="cont-unit desc-unit">${map.group_desc}</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function openRegUserGroup(id_group){
	if(id_group){
		location.href="<c:url value='/adm/user/regUserGroup.do'/>?id_menu=${param.id_menu}&id_group="+id_group;
	}else{
		location.href="<c:url value='/adm/user/regUserGroup.do'/>?id_menu=${param.id_menu}";
	}
}
function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/user/userGroupList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function deleteList(){
	if($("input:checkbox[name=del_code]:checked").length==0){
		alertOpen('삭제할 항목을 선택하십시오.');
	}else{
		confirmOpen('사용자 그룹을 삭제하시면 관련 그룹 사용자들도 삭제됩니다.주의하십시오.',"deleteGroupList");
	}
}
function deleteGroupList(){
	processing();
	var frm =document.frm;
	var page = $("#page").val();
	frm.action=contextPath+"/adm/user/deleteUsrGroup.do";
	frm.page.value=page;
//frm.action="/excelDownload.do";
    frm.submit();
}
function deleteRole(){
	confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
}
function deleteConfirm(){
	processing();
	if($("#confirm_yn").val()=='Y'){
	    $("#confirm_yn").val('N');
		var frm =document.frm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/user/deleteRoleList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
	}
}
</script>
</html>
