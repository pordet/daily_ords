package daily.context.test;

import java.io.FileNotFoundException;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import daily.common.base.AbstractJUnit4ContextTestBase;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;

@Transactional(value = "txManager")
public class IecmsTransactionTest  extends AbstractJUnit4ContextTestBase{
	
	@Test
	public void iecmsTest() throws Exception {
		CommandMap map = new CommandMap();
		map.put("id","test4");
		map.put("pwd", "1");
		map.put("nm", "test4_1");
		CommandMap map1 = new CommandMap();
		map1.put("id","test1");
		map1.put("pwd", "1");
		map1.put("nm", "test1_1");
		
		throw new FileNotFoundException();
		
	}

	@Test
	public void getToday() throws Exception {
	    String today = StringDateUtil.getToday("yyyy/MM/dd");
	    System.out.println(today);
		
	}

}
