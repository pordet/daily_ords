package daily.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.nfunk.jep.JEP;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;

public class MetaUtil {
	/**
	 * 
	 * @param ordList : Depth1부터 마지막 텝스까지 순서
	 * @param max_depth : 최대 뎁스
	 * @param parent_key_nm : 부모키명
	 * @param key_nm : 키명
	 * @param depth_col_nm : depth_col nm
	 * @return
	 */
    public static List<CustomMap> buildAloneList(List<CustomMap> ordList, int max_depth,String parent_key_nm,String key_nm,String depth_col_nm) {
    	Map<Integer, List<CustomMap>> childMap = new HashMap<>();
        List<CustomMap> aloneList = new ArrayList<>();

        for (CustomMap o : ordList) {
            int id_parent_meta = o.getInt(parent_key_nm, 0);
            if (!childMap.containsKey(id_parent_meta)) {
                childMap.put(id_parent_meta, new ArrayList<>());
            }
            childMap.get(id_parent_meta).add(o);
        }

        for (CustomMap o : ordList) {
            int cur_depth = o.getInt(depth_col_nm, 0);
            if (cur_depth == 1) {
            	MetaUtil.buildAloneListRecusive(o, childMap, max_depth, aloneList,key_nm,depth_col_nm);
            }
        }

        return aloneList;
	}

    public static void buildAloneListRecusive(CustomMap cell, Map<Integer, List<CustomMap>> childMap, int max_depth,
			List<CustomMap> aloneList,String key_nm,String depth_col_nm) {
    	int cur_id_meta = cell.getInt(key_nm);
        if (!hasChildren(cell, childMap, max_depth,key_nm,depth_col_nm)) {
            aloneList.add(cell);
        } else {
            if (childMap.containsKey(cur_id_meta)) {
                for (CustomMap child : childMap.get(cur_id_meta)) {
                	MetaUtil.buildAloneListRecusive(child, childMap, max_depth, aloneList,key_nm,depth_col_nm);
                }
            }
        }
	}
	// 셀이 자식을 가지는지 확인하는 함수
    public static  boolean hasChildren(CustomMap cell, Map<Integer,List<CustomMap>> childMap,int max_depth,String key_nm,String depth_col_nm) {
    	int cur_id_meta = cell.getInt(key_nm);
        if (childMap.containsKey(cur_id_meta)) {
            for (CustomMap child : childMap.get(cur_id_meta)) {
                if (child.getInt(depth_col_nm) <= max_depth) {
                    return true;
                }
            }
        }
        return false;
    }

    public static List<List<CustomMap>> buildHeaderList(List<CustomMap> lowList, int max_meta_depth) {
        List<List<CustomMap>> headerListByDepth = new ArrayList<>();
        Map<Integer, Integer> depthToChildCnt = new HashMap<>();

        for (int i = 1; i <= max_meta_depth; i++) {
            List<CustomMap> headerList = new ArrayList<>();

            for (CustomMap cellMap : lowList) {
                int childCnt = cellMap.getInt("child_cnt", 0);
                int curr_depth = cellMap.getInt("meta_depth",0);
                if(curr_depth!=i)
                	continue;
                if (curr_depth == i && childCnt > 0) {
//                    int rowspan = max_meta_depth - i + 1;
//                    int colspan = childCnt;
//
//                    if (!depthToChildCnt.containsKey(i)) {
//                        depthToChildCnt.put(i, childCnt);
//                    } else {
//                        colspan += depthToChildCnt.get(i);
//                        depthToChildCnt.put(i, colspan);
//                    }
                    int rowspan = MetaUtil.calculateRowspan(cellMap,max_meta_depth);
                    int colspan = MetaUtil.calculateColspan(cellMap);
                    // Create a header map with rowspan and colspan values
                    if(rowspan>1) {
                    	cellMap.put("rowspan", rowspan);
                    }
                    if(colspan>1) {
                    	cellMap.put("colspan", colspan);
                    }
                    headerList.add(cellMap);
                }else if(curr_depth == i && childCnt == 0) {
                	cellMap.put("rowspan", max_meta_depth);
                	headerList.add(cellMap);
                }
            }

            headerListByDepth.add(headerList);
        }

        return headerListByDepth;
    }
    public static int calculateRowspan(CustomMap cellMap, int max_meta_depth) {
    	int meta_depth= cellMap.getInt("meta_depth");
        return calculateRowspan(cellMap, max_meta_depth, meta_depth);
    }

    private static int calculateRowspan(CustomMap cellMap, int max_meta_depth, int currentDepth) {
        List<CustomMap> childList = (List<CustomMap>) cellMap.get("child_list");
        if(childList.size()>0) {
        	return 1;
        }else {
        	int curr_rowspan = max_meta_depth-currentDepth+1;
        	return curr_rowspan;
        }
    }
    
//    public static 

    public static int calculateRowspan(CustomMap cellMap) {
        int rowspan = 1;
        List<CustomMap> childList = (List<CustomMap>) cellMap.get("child_list");

        if (childList != null && !childList.isEmpty()) {
            for (CustomMap child : childList) {
                rowspan += calculateRowspan(child);
            }
        }

        return rowspan;
    }

    public static int calculateColspan(CustomMap cellMap) {
        int colspan = 1;
        List<CustomMap> childList = (List<CustomMap>) cellMap.get("child_list");

        if (childList != null && !childList.isEmpty()) {
            for (CustomMap child : childList) {
                colspan += calculateColspan(child);
            }
        }

        return colspan;
    }
    public static String generateTableHeader(List<CustomMap> headerList) {
        StringBuilder tableHtml = new StringBuilder("<table border=\"1\">");
        
        for (CustomMap header : headerList) {
            int childCnt = header.getInt("child_cnt");
            int metaDepth = header.getInt("meta_depth");

            tableHtml.append("<th>");
            tableHtml.append("Header for Depth " + metaDepth); // 헤더 텍스트를 여기에 설정하세요.
            tableHtml.append("</th>");

            if (childCnt > 0) {
                // 자식 헤더가 있는 경우
                List<CustomMap> childList = (List<CustomMap>) header.get("child_list");
                int rowspan = calculateRowspan(headerList, metaDepth); // rowspan 계산
                
                tableHtml.append("<tr>");
                
                for (CustomMap childHeader : childList) {
                    tableHtml.append("<td");
                    if (rowspan > 1) {
                        tableHtml.append(" rowspan=\"" + rowspan + "\"");
                    }
                    tableHtml.append(">");
                    
                    // 재귀 호출로 자식 헤더 생성
                    tableHtml.append(generateTableHeader(Collections.singletonList(childHeader)));
                    
                    tableHtml.append("</td>");
                }
                
                tableHtml.append("</tr>");
            }
        }
        
        tableHtml.append("</table>");
        return tableHtml.toString();
    }

    public static int calculateRowspan(List<CustomMap> headerList, int targetDepth) {
        int rowspan = 1;
        
        for (CustomMap header : headerList) {
            int metaDepth = header.getInt("meta_depth");
            int childCnt = header.getInt("child_cnt");
            
            if (metaDepth < targetDepth && childCnt > 0) {
                rowspan += calculateRowspan((List<CustomMap>) header.get("child_list"), targetDepth);
            }
        }
        
        return rowspan;
    }

    public static int findMaxKey(Map<Integer, ?> map) {
        int maxKey = Integer.MIN_VALUE;

        for (int key : map.keySet()) {
            if (key > maxKey) {
                maxKey = key;
            }
        }

        return maxKey;
    }

    /**2depth 부터 마지막 뎁스까지 계산
     *  
     * @param cell : 1depth cell
     * @param childMap : 1depth cell을 포함한 데이터 셀을 담고 있음
     * @return
     */
	public static List<CustomMap> generateRealDataRow(CustomMap cell, Map<Integer,Map<Integer, CustomMap>> childMap) {
		Map<Integer, CustomMap> childCellMap = childMap.get(cell.getInt("id_meta"));
		//자식이 없으면 자신만 리턴
        List<CustomMap> dataRow = new ArrayList<>();
		if(childCellMap==null || childCellMap.size()==0) {
	        dataRow.add(cell);
	    //자식이 있을 때 처리
		}else {
	        List<CustomMap> childDataRow = new ArrayList<>();
	        List<CustomMap> sortedList = new ArrayList<>();
	        SortedSet<Integer> sortedKeys = new TreeSet<>(childCellMap.keySet());
	        for (Integer key : sortedKeys) {
	        	sortedList.add(childCellMap.get(key));
	        }
	        for(CustomMap child : sortedList) {
	        	childDataRow = MetaUtil.generateRealDataRow(child,childMap);
	        	dataRow.addAll(childDataRow);
	        }
		}
        return dataRow;
	}
	/**
	 * 
	 * @param topList
	 * @param slaveList
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> generateDataAndHeaderLists(List<CustomMap> topList, List<CustomMap> slaveList) throws Exception {
	    Map<String, Object> ret = new HashMap<>();
	    List<List<CustomMap>> headerList = new ArrayList<>();
	    List<CustomMap> dataRow = new ArrayList<>();
	    //Map<id__meta,List<col> 형태로 child 맵 구성
	    Map<Integer,CustomMap> colMap = new HashMap<>();
	    //Map<depth,Map<id_meta,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthMap = new HashMap<>();
	    //Map<depth,Map<ord,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthOrdMap = new HashMap<>();
	    //id_parent_meta,Map<id_meta,Col> 자식담은 맵
	    Map<Integer,Map<Integer,CustomMap>> childMap = new HashMap<>();
	    
	    // 나머지 코드 (dataRow와 headerList 생성 부분)...
	    for(CustomMap sl:topList) {
	    	colMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    }
	    for(CustomMap sl:slaveList) {
	    	boolean isListShowCal = false;
//	    	if(sl.getInt("id_meta",0)==78||sl.getInt("id_meta",0)==79) {
//	    		System.out.println(sl.getInt("id_meta",0)+":id_meta 테스트 시작");
//	    	}
	    	if(sl.getInt("meta_depth",0)==2) {
	    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null) {
	    			isListShowCal = true;
	    		}
	    	}else {
	    		if(childMap.get(sl.getInt("id_parent_meta",0))!=null){
	    			isListShowCal = true;
	    		}
	    	}
	    	if(isListShowCal) {
		    	colMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerChildMap = childMap.computeIfAbsent(sl.getInt("id_parent_meta"), k -> new HashMap<>());
		    	innerChildMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    	}
	    }
	    int real_max_depth = MetaUtil.findMaxKey(depthOrdMap);

	    for (int depth = real_max_depth; depth >= 1; depth--) {
    		Map<Integer,CustomMap> depthColMap=depthMap.get(depth);
	    	if(depth==real_max_depth) {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		cellMap.put("rowspan", 1);
		    		cellMap.put("colspan", cellMap.getInt("colspan",1));
	    		}
	    	}else {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		Map<Integer, CustomMap> innerChildMap = childMap.get(cellMap.getInt("id_meta"));
		    		//없는 경우
		    		if(innerChildMap==null) {
			    		cellMap.put("rowspan", real_max_depth-depth+1);
			    		cellMap.put("colspan", 1);
		    		}else {
			    		cellMap.put("rowspan", 1);
			    		if(innerChildMap.size()==0) {
				    		cellMap.put("colspan", 1);
			    		}else {
				    		int colspan = 0;
				    		for(int inner_id_meta:innerChildMap.keySet()) {
				    			CustomMap innerChild = innerChildMap.get(inner_id_meta);
				    			colspan+=innerChild.getInt("colspan",1);
				    		}
				    		cellMap.put("colspan", colspan);
			    		}
		    		}
	    		}
	    	}
	    }
	    //1depth인 것 가져오서 정렬해서 List<CustomMap> 만들기
	    Map<Integer, CustomMap> levelOneMap = depthOrdMap.get(1);

        if (levelOneMap != null) {
            List<CustomMap> resultList = new ArrayList<>();
            // key를 정렬한 후 순서대로 CustomMap을 List에 추가
            SortedSet<Integer> sortedKeys = new TreeSet<>(levelOneMap.keySet());
            for (Integer key : sortedKeys) {
                resultList.add(levelOneMap.get(key));
            }
            // 결과 출력
            for (CustomMap cell : resultList) {
    	    	if(cell.getInt("id_meta",0)==78||cell.getInt("id_meta",0)==83) {
    	    		System.out.println(cell.getInt("id_meta",0)+":cell id_meta 테스트 시작");
    	    	}
    	    	
            	List<CustomMap> useColList = MetaUtil.generateRealDataRow(cell,childMap);
            	dataRow.addAll(useColList);
            }
        } else {
            throw new Exception("조회하려는 컬럼 설정을 찾지 못했습니다. 관리자에게 문의바랍니다.");
        }

	    TreeMap<Integer, Map<Integer, CustomMap>> sortedMap = new TreeMap<>(depthOrdMap);
	    for (Map.Entry<Integer, Map<Integer, CustomMap>> entry : sortedMap.entrySet()) {
            List<CustomMap> innerList = new ArrayList<>();

            // Map<Integer, CustomMap>에서 정렬된 키로 CustomMap을 가져옴
            Map<Integer, CustomMap> innerMap = entry.getValue();
            TreeMap<Integer, CustomMap> sortedInnerMap = new TreeMap<>(innerMap);

            for (Map.Entry<Integer, CustomMap> innerEntry : sortedInnerMap.entrySet()) {
                innerList.add(innerEntry.getValue());
            }

            headerList.add(innerList);
        }

	    ret.put("aloneList", dataRow);
	    ret.put("headerList", headerList);
	    return ret;
	}

	public static Map<String, Object> generateDataAndHeaderLists(List<CustomMap> topList, List<CustomMap> slaveList,
			Map<Integer, List<CustomMap>> fkColMap)  throws Exception{
	    Map<String, Object> ret = new HashMap<>();
	    List<List<CustomMap>> headerList = new ArrayList<>();
	    List<CustomMap> dataRow = new ArrayList<>();
	    //Map<id__meta,List<col> 형태로 child 맵 구성
	    Map<Integer,CustomMap> colMap = new HashMap<>();
	    //Map<depth,Map<id_meta,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthMap = new HashMap<>();
	    //Map<depth,Map<ord,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthOrdMap = new HashMap<>();
	    //id_parent_meta,Map<id_meta,Col> 자식담은 맵
	    Map<Integer,Map<Integer,CustomMap>> childMap = new HashMap<>();
	    
	    // 나머지 코드 (dataRow와 headerList 생성 부분)...
	    for(CustomMap sl:topList) {
	    	colMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    }
	    for(CustomMap sl:slaveList) {
	    	boolean isListShowCal = false;
//	    	if(sl.getInt("id_meta",0)==78||sl.getInt("id_meta",0)==79) {
//	    		System.out.println(sl.getInt("id_meta",0)+":id_meta 테스트 시작");
//	    	}
	    	if(sl.getInt("meta_depth",0)==2) {
	    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null) {
	    			isListShowCal = true;
	    		}
	    	}else {
	    		if(childMap.get(sl.getInt("id_parent_meta",0))!=null){
	    			isListShowCal = true;
	    		}
	    	}
	    	if(isListShowCal) {
		    	colMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerChildMap = childMap.computeIfAbsent(sl.getInt("id_parent_meta"), k -> new HashMap<>());
		    	innerChildMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    	}
	    }
	    int real_max_depth = MetaUtil.findMaxKey(depthOrdMap);

	    for (int depth = real_max_depth; depth >= 1; depth--) {
    		Map<Integer,CustomMap> depthColMap=depthMap.get(depth);
	    	if(depth==real_max_depth) {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		cellMap.put("rowspan", 1);
		    		cellMap.put("colspan", cellMap.getInt("colspan",1));
	    		}
	    	}else {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		Map<Integer, CustomMap> innerChildMap = childMap.get(cellMap.getInt("id_meta"));
		    		//없는 경우
		    		if(innerChildMap==null) {
			    		cellMap.put("rowspan", real_max_depth-depth+1);
			    		cellMap.put("colspan", 1);
		    		}else {
			    		cellMap.put("rowspan", 1);
			    		if(innerChildMap.size()==0) {
				    		cellMap.put("colspan", 1);
			    		}else {
				    		int colspan = 0;
				    		for(int inner_id_meta:innerChildMap.keySet()) {
				    			CustomMap innerChild = innerChildMap.get(inner_id_meta);
				    			colspan+=innerChild.getInt("colspan",1);
				    		}
				    		cellMap.put("colspan", colspan);
			    		}
		    		}
	    		}
	    	}
	    }
	    //1depth인 것 가져오서 정렬해서 List<CustomMap> 만들기
	    Map<Integer, CustomMap> levelOneMap = depthOrdMap.get(1);

        if (levelOneMap != null) {
            List<CustomMap> resultList = new ArrayList<>();
            // key를 정렬한 후 순서대로 CustomMap을 List에 추가
            SortedSet<Integer> sortedKeys = new TreeSet<>(levelOneMap.keySet());
            for (Integer key : sortedKeys) {
                resultList.add(levelOneMap.get(key));
            }
            // 결과 출력
            for (CustomMap cell : resultList) {
    	    	if(cell.getInt("id_meta",0)==78||cell.getInt("id_meta",0)==83) {
    	    		System.out.println(cell.getInt("id_meta",0)+":cell id_meta 테스트 시작");
    	    	}
    	    	
            	List<CustomMap> useColList = MetaUtil.generateRealDataRow(cell,childMap);
            	dataRow.addAll(useColList);
            }
        } else {
            throw new Exception("조회하려는 컬럼 설정을 찾지 못했습니다. 관리자에게 문의바랍니다.");
        }

	    TreeMap<Integer, Map<Integer, CustomMap>> sortedMap = new TreeMap<>(depthOrdMap);
	    for (Map.Entry<Integer, Map<Integer, CustomMap>> entry : sortedMap.entrySet()) {
            List<CustomMap> innerList = new ArrayList<>();

            // Map<Integer, CustomMap>에서 정렬된 키로 CustomMap을 가져옴
            Map<Integer, CustomMap> innerMap = entry.getValue();
            TreeMap<Integer, CustomMap> sortedInnerMap = new TreeMap<>(innerMap);

            for (Map.Entry<Integer, CustomMap> innerEntry : sortedInnerMap.entrySet()) {
                innerList.add(innerEntry.getValue());
            }

            headerList.add(innerList);
        }

	    ret.put("aloneList", dataRow);
	    ret.put("headerList", headerList);
	    return ret;
	}

	public static List<Map<CommandMap, List<CommandMap>>> calculateRealDataList(
			List<Map<CommandMap, List<CommandMap>>> realDataList, List<CustomMap> calColInfoMap) {
        Map<Integer,String> calMap = new HashMap<>();
        for(CustomMap cal:calColInfoMap) {
        	calMap.put(cal.getInt("id_meta"), cal.getString("formula"));
        }
        for (Map<CommandMap, List<CommandMap>> map : realDataList) {
        	//subList가 행을 나타남
            for (List<CommandMap> subList : map.values()) {
       	     	JEP jep = new JEP();
       	     	Map<String,String> varMap = new HashMap<>();
       	     	Map<String,String> varAllMap = new HashMap<>();
            	for(CommandMap sub:subList) {
            		if(sub.getInt("id_meta",0)==107) {
            			System.out.println("id_meta:"+107);
            		}
            		if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
            			String varNm = sub.getString("meta_cd");
            			String val = sub.getString("val","");
            			if(!"".equals(val)) {
                			varMap.put(varNm, val);
                			jep.addVariable(varNm, getStringToDouble(val));
                	        System.out.println("정수값:"+val);
            			}
            			varAllMap.put(varNm, val);
            		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
            			String varNm = sub.getString("meta_cd");
            			String val = sub.getString("val","");
            			if(!"".equals(val)) {
                			varMap.put(varNm, val);
                			jep.addVariable(varNm, getStringToDouble(val));
                	        System.out.println("실수값:"+val);
            			}
            			varAllMap.put(varNm, val);
            		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
            			String varNm = sub.getString("meta_cd");
            			String val = sub.getString("val","");
            			if(!"".equals(val)) {
            				String perNum = sub.getString("val","0").replaceAll("%", "");
                			varMap.put(varNm, perNum);
                	        double percentValue = getStringToDouble(perNum); // 50%
                	        double convertedValue = percentValue / 100.0; // 퍼센트 값을 소수로 변환하여 사용
                	        jep.addVariable(varNm, convertedValue);
                	        System.out.println("퍼센트값:"+convertedValue);
            			}else {
            				varMap.put(varNm, null);
            			}
            			varAllMap.put(varNm, val);
            		}
            	}
            	for(CommandMap sub:subList) {
            		if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)){
            			int sub_id_meta = sub.getInt("id_meta",0);
            			if(sub_id_meta>0) {
            				String formula = calMap.get(sub_id_meta);
            				if(formula!=null) {
            					if(havingAllVariableCal(varMap,varAllMap,formula)) {
	                    			int precision = sub.getInt("precision_len",0);
	                    			String result = getCalVal(jep,formula,precision);
	                    			sub.put("val", result);
	                    	        System.out.println("Result: " + result);
            					}else {
	                    			sub.put("val", "");
            					}
            				}else {
            					sub.put("val", "");
            				}
            			}
            		}
            	}
            }
        }
		return realDataList;
	}
	public static double getStringToDouble(String val) {
		BigDecimal bigDecimalValue = null;
        try {
            // 쉼표를 제거하고 문자열을 BigDecimal로 변환
            bigDecimalValue = new BigDecimal(val.replaceAll(",", ""));
        } catch (NumberFormatException e) {
            System.err.println("예외 발생: " + e.getMessage());
            e.printStackTrace();
        }
        
        // 변환된 BigDecimal 값을 출력
        if (bigDecimalValue != null) {
            System.out.println("BigDecimal 값: " + bigDecimalValue);
    		return bigDecimalValue.doubleValue();
        } else {
            return new BigDecimal("0").doubleValue();
        }		
	}
	private static String getCalVal(JEP jep,String formula,int precision) {
        System.out.println("formula: " + formula);
		jep.parseExpression(formula);
		BigDecimal bigDecimalValue = new BigDecimal(jep.getValue()).setScale(precision, RoundingMode.HALF_UP);
		return bigDecimalValue.toString();
	}
    private static boolean hasKeyAtMapInSentence(Map<String, String> map, String sentence) {
        for (String key : map.keySet()) {
            if (sentence.contains(key)) {
                return true; // 문장에 해당 키가 없으면 true를 반환
            }
        }
        return false; // 모든 키가 존재하면 false를 반환
    }
    public static boolean havingAllVariableCal(Map<String, String> map, Map<String, String> varAllMap, String sentence) {
    	int varNmMatchingCnt=0;
    	int varNmAllMatchingCnt=0;
    	Map<String,String> mMap = new HashMap<>();
        for (String key : map.keySet()) {
            if (sentence.contains(key)) {
            	System.out.println("matching Key:"+key);
            	mMap.put(key, map.get(key));
            	varNmMatchingCnt++;
            }
        }
        for (String key : varAllMap.keySet()) {
            if (sentence.contains(key)) {
            	System.out.println("matching Key:"+key);
            	mMap.put(key, map.get(key));
            	varNmAllMatchingCnt++;
            }
        }
        int quiCnt=0;
        for (String key : map.keySet()) {
        	System.out.println("varNmMatching Map:"+key);
        	String val = mMap.get(key);
        	if(val!=null) {
        		quiCnt++;
        	}
        }
        if(varNmMatchingCnt>0 && varNmMatchingCnt == varNmAllMatchingCnt && varNmMatchingCnt==quiCnt) {
        	return true;
        }else {
        	return false; // 모든 키가 존재하면 false를 반환
        }
    }

	public static void printCustomList(List<CustomMap> aloneList, List<String> showList) {
		for(CustomMap c:aloneList){
			for(String key:showList) {
				System.out.print(key+":"+c.getString(key)+",");
			}
			System.out.println("--");
		}
		
	}

	/**
	 * insert에서 사용할 목적으로 만든 메서드
	 * @param inList
	 * @param calColInfoList
	 * @param id_meta_row 
	 * @return
	 */
	public static List<CommandMap> calculateRowData(List<CustomMap> colList, List<CommandMap> inList, List<CustomMap> calColInfoList, int id_meta_row) {
        Map<Integer,String> calMap = new HashMap<>();
     	Map<String,String> varMap = new HashMap<>();
     	Map<String,String> varAllMap = new HashMap<>();
     	JEP jep = new JEP();
        if(calColInfoList!=null) {
            for(CustomMap cal:calColInfoList) {
            	calMap.put(cal.getInt("id_meta"), cal.getString("formula"));
            }
         	for(CustomMap map:colList) {
        		if(map.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
        			varAllMap.put(map.getString("meta_cd",""), map.getString("meta_cd",""));	    			
        		}else if(map.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
        			varAllMap.put(map.getString("meta_cd",""), map.getString("meta_cd",""));	    			
        		}else if(map.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
        			varAllMap.put(map.getString("meta_cd",""), map.getString("meta_cd",""));	    			
        		}
         		
         	}
        }
        for (CommandMap sub : inList) {
        	//subList가 행을 나타남
    		if(sub.getInt("id_meta",0)==455) {
    			System.out.println("id_meta:"+455);
    		}
    		if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
        			varMap.put(varNm, val);
        			jep.addVariable(varNm, getStringToDouble(val));
        			sub = transNumberingByCommandMap(sub);
    			}
    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
        			varMap.put(varNm, val);
        			jep.addVariable(varNm, getStringToDouble(val));
        			sub = transNumberingByCommandMap(sub);
   			}
    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
    				String perNum = sub.getString("val","0").replaceAll("%", "");
        			varMap.put(varNm, perNum);
        	        double percentValue = getStringToDouble(perNum); // 50%
        	        double convertedValue = percentValue / 100.0; // 퍼센트 값을 소수로 변환하여 사용
        	        jep.addVariable(varNm, convertedValue);
        			sub = transNumberingByCommandMap(sub);
    			}else {
    				varMap.put(varNm, null);
    			}
    		}
        }
    	for(CustomMap sub:colList) {
    		if(sub.getInt("id_meta",0)==455) {
    			System.out.println("id_meta:"+455);
    		}
    		if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)){
    			int sub_id_meta = sub.getInt("id_meta",0);
    			if(sub_id_meta>0) {
    				String formula = calMap.get(sub_id_meta);
    				if(formula!=null) {
    					if(havingAllVariableCal(varMap,varAllMap,formula)) {
                			int precision = sub.getInt("precision_len",0);
                			String result = getCalVal(jep,formula,precision);
                			sub.put("val", result);
                			sub.put("id_meta_row", id_meta_row);
                			inList.add(CollectionUtil.getCommandMapByCustomMap(sub));
                	        System.out.println("Result: " + result);
    					}else {
                			sub.put("val", "");
    					}
    				}else {
    					sub.put("val", "");
    				}
    			}
    		}
        }
    	inList = transNumbering(inList);
		return inList;
	}
	public static List<CommandMap> getModifyCommandListByCommandMapAndPrefix(List<CustomMap> colList,List<CustomMap> calColInfoList,String key,CommandMap in,String prefix,String bef_val_prefix,String bef_key_prefix,String mod_key,String attrNm){
		List<CommandMap> ret = new ArrayList<CommandMap>();
        Map<Integer,String> calMap = new HashMap<>();
        for(CustomMap cal:calColInfoList) {
        	calMap.put(cal.getInt("id_meta"), cal.getString("formula"));
        }
        //
		Map<Integer,CustomMap> calColMap = new HashMap<>();
		//Map<id_meta,val>
		Map<Integer,CommandMap> calHavingColMap = new HashMap<>();
		//Map<id_meta,colList의 col>
		Map<String,CustomMap> valMap = new HashMap<>();
		//Map<숫자로 된 id_meta,의미없는 값) - 존재 여부만 중요
     	Map<String,String> varAllMap = new HashMap<>();
		for(CustomMap map:colList) {
			String idMata = map.getString(key,"").trim();
			if(idMata.equals("10")||idMata.equals("455")) {
				int aa=0;
			}
			if(!"".equals(idMata)) {
				String val = in.getString(prefix+idMata,"").trim();
				String bef_val = in.getString(bef_val_prefix+idMata,"").trim();
				String bef_id_data = in.getString(bef_key_prefix+idMata,"").trim();
				String typ = map.getString("typ","");
	    		if(map.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
	    			varAllMap.put(map.getString("meta_cd",""), val);	    			
	    		}else if(map.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
	    			varAllMap.put(map.getString("meta_cd",""), val);	    			
	    		}else if(map.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
	    			varAllMap.put(map.getString("meta_cd",""), val);	    			
	    		}
				if(typ.equals(GlobalConst.DT_CALC)) {
					calColMap.put(map.getInt(key,0),map);
					if(!"".equals(bef_val)) {
						CommandMap calHavingCol = new CommandMap();
						calHavingCol.put(mod_key, bef_id_data);
						calHavingCol.put("bef_"+attrNm, bef_val);
						calHavingColMap.put(map.getInt(key,0),calHavingCol);
						valMap.put(idMata,map); 
					}
				}else {
					if(!val.equals(bef_val)) {
						if("".equals(bef_val) && !"".equals(val)) {
							map.put("modify_type", "I");
							map.put(attrNm, val);
							map.put("id_meta_row", in.get("id_meta_row"));
							valMap.put(idMata,map); 
						}else if(!"".equals(bef_val) && "".equals(val)){
							map.put(mod_key, bef_id_data);
							map.put("modify_type", "D");
							valMap.put(idMata,map); 
						}else if(!"".equals(bef_val) && !"".equals(val)){
							map.put(mod_key, bef_id_data);
							map.put("modify_type", "M");
							map.put(attrNm, val);
							valMap.put(idMata,map); 
						}
					}else {
						if(!"".equals(val)) {
							map.put(mod_key, bef_id_data);
							map.put(attrNm, val);
							valMap.put(idMata,map); 
						}
					}
				}
			}
		}
     	JEP jep = new JEP();
     	Map<String,String> varMap = new HashMap<>();
     	// 변수가 될 수 있는 값을 가지고 있는 col : 변수가 될 수 있는 col - 정수,소수점,퍼센트
        for (String id_meta : valMap.keySet()) {
        	CustomMap sub = valMap.get(id_meta);
        	//subList가 행을 나타남
    		if(sub.getInt("id_meta",0)==455) {
    			System.out.println("id_meta:"+455);
    		}
    		if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
        			varMap.put(varNm, val);
        			jep.addVariable(varNm, getStringToDouble(val));
        	        System.out.println("정수값:"+val);
    			}
    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
        			varMap.put(varNm, val);
        			jep.addVariable(varNm, getStringToDouble(val));
        	        System.out.println("실수값:"+val);
    			}
    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
    			String varNm = sub.getString("meta_cd");
    			String val = sub.getString("val","");
    			if(!"".equals(val)) {
    				String perNum = sub.getString("val","0").replaceAll("%", "");
        			varMap.put(varNm, perNum);
        	        double percentValue = getStringToDouble(perNum); // 50%
        	        double convertedValue = percentValue / 100.0; // 퍼센트 값을 소수로 변환하여 사용
        	        jep.addVariable(varNm, convertedValue);
        	        System.out.println("실수값:"+convertedValue);
    			}else {
    				varMap.put(varNm, null); 
    			}
    		}
        }
        // 계산식을 처리하는 부분 - havingAllVariableCal 가 true이면 계산하고, false이면 기존데이터가 있으면 valMap에서 삭제 처리, 없으면 무시
    	for(CustomMap sub:colList) {
    		if(sub.getInt("id_meta",0)==455) {
    			System.out.println("id_meta:"+455);
    		}
    		String idMeta = sub.getString("id_meta", "");
    		if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)){
    			int sub_id_meta = sub.getInt("id_meta",0);
    			if(sub_id_meta>0) {
    				String formula = calMap.get(sub_id_meta);
    				if(formula!=null) {
    					if(havingAllVariableCal(varMap,varAllMap,formula)) {
                			int precision = sub.getInt("precision_len",0);
                			String result = getCalVal(jep,formula,precision);
                	        System.out.println(sub.getString("meta_nm", "")+"의 계산값:"+result);
                			sub.put("val", result);
                			sub.put("id_meta_row", in.getInt("id_meta_row"));
                			if(calHavingColMap.get(sub.getInt("id_meta",0))==null) {
    							sub.put("modify_type", "I");
    							sub.put("id_meta_row", in.get("id_meta_row"));
    							valMap.put(sub.getString("id_meta",""),sub);
               				
                			}else {
                				CommandMap calHavingCol = calHavingColMap.get(sub.getInt("id_meta",0));
                				if(!calHavingCol.get("bef_"+attrNm).equals(result)){
	                				sub.put(mod_key,calHavingCol.get(mod_key));
	    							sub.put("modify_type", "M");
	    							valMap.put(sub.getString("id_meta",""),sub);
                				}
                			}
    					}else {
                			if(calHavingColMap.get(sub.getInt("id_meta",0))!=null) {
                				CommandMap calHavingCol = calHavingColMap.get(sub.getInt("id_meta",0));
                    			sub.put("val", "");
    							sub.put("modify_type", "D");
                				sub.put(mod_key,calHavingCol.get(mod_key));
    							valMap.put(sub.getString("id_meta",""),sub);
                			}
    					}
    				}else {
            			if(calHavingColMap.get(sub.getInt("id_meta",0))!=null) {
            				CommandMap calHavingCol = calHavingColMap.get(sub.getInt("id_meta",0));
                			sub.put("val", "");
							sub.put("modify_type", "D");
            				sub.put(mod_key,calHavingCol.get(mod_key));
							valMap.put(sub.getString("id_meta",""),sub);
            			}
    				}
    			}
    		}
        }
        for (String id_meta : valMap.keySet()) {
        	CustomMap sub = valMap.get(id_meta);
        	CommandMap val = CollectionUtil.getCommandMapByCustomMap(sub);
        	val = transNumbering(sub,val);
        	System.out.println("col toString:"+val.toString());
        	ret.add(val);
        }
		return ret;
	}
	public static String getFormattedNum(String numberString,int scale_len,int precision_len) {
		// 문자열을 BigDecimal로 파싱
		System.out.println("numberString:"+numberString);
        BigDecimal number = new BigDecimal(numberString);
        try {
	        // 소수점 길이를 precision_len에 따라 동적으로 설정
	        if(precision_len==0) {
	            String pattern = "#,###";
	            DecimalFormat decimalFormat = new DecimalFormat(pattern);
	            String formattedNumber = decimalFormat.format(number);
	            return formattedNumber;
	        }else {
	            String pattern = "#,##0.";
	            for (int i = 0; i < precision_len; i++) {
	                pattern += "0";
	            }
	            DecimalFormat decimalFormat = new DecimalFormat(pattern);
	            String formattedNumber = decimalFormat.format(number);
	            return formattedNumber;
	        }
        }catch(Exception e) {
    		System.out.println("getFormattedNum Error:"+numberString);
        	e.printStackTrace();
        	return numberString;
        }
	}
	public static CommandMap transNumbering(CustomMap sub,CommandMap val) {
		if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
			String str_val = val.getString("val","").replaceAll(",","");
			if(!"".equals(str_val)) {
				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
    			val.put("val", tran_val);
			}
		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
			String str_val = val.getString("val","").replaceAll(",","");
			if(!"".equals(str_val)) {
				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
    			val.put("val", tran_val);
			}
		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
			String str_val = val.getString("val","").replaceAll(",","");
			if(!"".equals(str_val)) {
				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
    			val.put("val", tran_val);
			}
		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)) {
			String str_val = val.getString("val","").replaceAll(",","");
			if(!"".equals(str_val)) {
				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
    			val.put("val", tran_val);
			}
		}
		return val;
		
	}
	public static List<CommandMap> transNumbering(List<CommandMap> inList){
        for (CommandMap sub : inList) {
    		try {
	    		if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
	    			String str_val = sub.getString("val","").replaceAll(",","");
	    			if(!"".equals(str_val)) {
	    				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
	    				sub.put("val", tran_val);
	    			}
	    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
	    			String str_val = sub.getString("val","").replaceAll(",","");
	    			if(!"".equals(str_val)) {
	    				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
	    				sub.put("val", tran_val);
	    			}
	    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
	    			String str_val = sub.getString("val","").replaceAll(",","");
	    			if(!"".equals(str_val)) {
	    				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
	    				sub.put("val", tran_val);
	    			}
	    		}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)) {
	    			String str_val = sub.getString("val","").replaceAll(",","");
	    			if(!"".equals(str_val)) {
	    				String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
	    				sub.put("val", tran_val);
	    			}
	    		}
    		}catch(Exception e) {
    			//(추후 수정 필요)
    			System.out.println("transNumbering exption 발생 sub:"+sub.toString());
    		}
        }
		return inList;
	}

	public static CustomMap transNumberingByCustomMap(CustomMap sub) {
		try {
			if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}
			return sub;
		}catch(Exception e) {
			return sub;
		}
	}
	public static CommandMap transNumberingByCommandMap(CommandMap sub) {
		try {
			if(sub.getString("typ","").trim().equals(GlobalConst.DT_NUM)){
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_FLOAT)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_PERCENT)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}else if(sub.getString("typ","").trim().equals(GlobalConst.DT_CALC)) {
				String str_val = sub.getString("val","").replaceAll(",","");
				if(!"".equals(str_val)) {
					String tran_val = getFormattedNum(str_val,sub.getInt("decimal_len"),sub.getInt("precision_len"));
					sub.put("val", tran_val);
				}
			}
			return sub;
		}catch(Exception e) {
			return sub;
		}
	}
}
