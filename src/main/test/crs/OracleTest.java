package crs;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OracleTest {
	private Logger log = LoggerFactory.getLogger(OracleTest.class);
	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// 테스트코드 작성, con 객체 생성
	@Test
	public void testConnection() {
		try(Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:sa",
				"traf_sa",
				"daily0704")){
			log.debug("start test");
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
