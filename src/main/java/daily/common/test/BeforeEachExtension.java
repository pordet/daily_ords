package daily.common.test;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BeforeEachExtension implements BeforeEachCallback {

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        // BeforeEach 애노테이션이 지정된 메소드를 찾아서 실행
        context.getTestMethod()
               .ifPresent(testMethod -> {
                   try {
                       testMethod.invoke(context.getRequiredTestInstance());
                   } catch (Exception e) {
                       throw new RuntimeException(e);
                   }
               });
    }
}
