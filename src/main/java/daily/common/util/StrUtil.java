package daily.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class StrUtil
{

	/**
	 * 문자열이면 문자열 배열로 만들어 리턴하고, 문자열배열이면 그대로 리턴해 준다.
	 * @param obj 문자열 또는 문자열배열
	 * @return 문자열배열. obj가 문자열 또는 문자열배열이 아닌 경우는 null.
	 */
	public static String[] getStringArray(Object obj) {
		String[] strArr = null;
		if(obj.getClass().isArray()) {
			strArr = (String[]) obj;
		} else if(obj instanceof String){
			strArr = new String[1];
			strArr[0] = (String)obj; 
		}
		return strArr;
	}

	/**
	 *  문자열 배열를 integer 배열로 변환 리턴한다.
	 * @param strArr
	 * @return
	 */
    public static Integer[] convertToIntArray(String[] strArr) {
    	Integer[] intArr = new Integer[strArr.length];
    	for(int i = 0; i < strArr.length; i++) {
    		if(strArr[i].isEmpty()) continue;
    		intArr[i] = Integer.parseInt(strArr[i]);    		
    	}
    	return intArr;
	}
    public static Integer[] convertToIntArray(Object[] strArr) {
    	Integer[] intArr = new Integer[strArr.length];
    	for(int i = 0; i < strArr.length; i++) {
    		intArr[i] = Integer.parseInt(strArr[i].toString());    		
    	}
    	return intArr;
   }
	/**
	 * Whitespace를 삭제한다.
	 * @param src
	 * @return
	 */
	public static String deleteWhitespace(String src) {
		return StringUtils.deleteWhitespace(src);
	}
	
	public static String getLowerCase(String src) {
		return src.toLowerCase();
	}
	
	public static String getString(Object str){
		return String.valueOf(str);
	}

	/**
	 * 문자를 split하여 문자열 배열로 만들어 리턴, mybatis iterator 용으로 사용.
	 * @param 문자열 src를 separator로 split하여 문자열 배열로 리턴
	 * @return 문자열배열.
	 */
	public static String[] getStrArrRemoveOverlap(String src,String separator){
		String[] arr = src.split(";");
		Set<String> set = new LinkedHashSet<String>();
		for(String arg : arr){
			set.add(arg);
		}
		String[] ret = {};
		return set.toArray(ret);
	}
	/**
	 * 문자를 split하여 문자열 배열로 만들어 리턴, mybatis iterator 용으로 사용.
	 * @param 문자열배열 strArr를 separator로 연결하는 문자열로 변환시켜 리턴
	 * @return 문자열.
	 */
	public static String getStringBySeparator(String[] strArr,String separator){
		String ret="";
		if(strArr.length==0){
			return ret;
		}else{
			for(String arg : strArr){
				ret+=arg+separator;
			}
			ret = ret.substring(0,ret.lastIndexOf(separator));
		}
		return ret;
	}
	
	public static Integer indexOf(String org,String param){
		int ret = org.indexOf(param);
		return ret;
	}

	public static String join(String[] strings) {
		StringBuffer sb = new StringBuffer("");
		for(String str : strings){
			sb.append(str);
		}
		return sb.toString();
	}
	
	public static boolean isEqual(String origin,String target) {
		if(origin.equals(target)) {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean isEqualIgnoreUpper(String origin,String target) {
		if(origin.toLowerCase().equals(target.toLowerCase())) {
			return true;
		}else {
			return false;
		}
	}
	
	public static String getStringByObject(Object obj) {
		String ret = "";
		if(obj==null) {
			return ret;
		}else {
			return String.valueOf(obj);
		}
	}
	/**
	 * List에 target 문장이 포함되어 있는지 여부를 확인하는 메서드
	 */
	public static boolean containsMidString(List<String> array, String target) throws Exception{
		boolean isMatch = false;
		try {
			for(String element : array) {
				String[] elArr = element.split(" ");
				int match_word_cnt = 0;
				for(String el:elArr) {
		            if (target.toUpperCase().contains(el.toUpperCase().trim()) || target.contains(el.trim())) {
		            	if(target.contains("ISP")|| target.contains("isp")) {
		            		System.out.println("isp 포함");
		            	}
		            	match_word_cnt++;
		            }
				}
				if(match_word_cnt==elArr.length) {
					isMatch=true;
					return isMatch;
				}else {
					isMatch=false;
				}
	        }
		}catch(Exception e) {
			throw new RuntimeException("containsMidString static 함수 실행 오류 발생");
		}
        return isMatch;
	}
	/**
	 * List에 target 문장이 포함되어 있는지 여부를 확인하는 메서드
	 */
	public static String matchingKeywordByContainsMidString(List<String> array, String target) throws Exception{
		try {
			for(String element : array) {
				String[] elArr = element.split(" ");
				boolean isMatch = false;
				int match_word_cnt = 0;
				for(String el:elArr) {
		            if (target.toUpperCase().contains(el.toUpperCase().trim())) {
		            	match_word_cnt++;
		            }else {
		            	break;
		            }
				}
				if(match_word_cnt==elArr.length) {
					return element;
				}
	        }
		}catch(Exception e) {
			throw new RuntimeException("containsMidString static 함수 실행 오류 발생");
		}
        return "";
	}

	public static boolean isInteger(String str) {
		try {
	        // 문자열을 정수로 변환하여 성공하면 true를 반환
	        Integer.parseInt(str);
	        return true;
	    } catch (NumberFormatException e) {
	        // 변환에 실패하면 NumberFormatException이 발생하고, false를 반환
	        return false;
	    }
	}
	public static String arrayToQueryString(String key, String[] array) throws UnsupportedEncodingException {
        StringBuilder queryString = new StringBuilder();
        
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                queryString.append("&");
            }
            queryString.append(URLEncoder.encode(key + "[" + i + "]", "UTF-8"));
            queryString.append("=");
            queryString.append(URLEncoder.encode(array[i], "UTF-8"));
        }
        
        return queryString.toString();
    }
    public static String stringToQueryString(String key, String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8")+"&";
    }
}
