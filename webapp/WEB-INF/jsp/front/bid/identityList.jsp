<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.UserAuthUtil" %>

<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:url var="act" value="/front/bid/identityList.do"/>
<c:url var="g_act" value='/front/guest/identityList.do'/>
<c:url var="detail_act" value="/front/bid/viewIdentity.do"/>
<c:url var="detail_g_act" value='/front/guest/viewIdentity.do'/>

<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
		<c:set var="action" value="${act}"/>
		<c:set var="detail_action" value="${detail_act}"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
		<c:set var="detail_action" value="${detail_g_act}"/>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<c:set var="title" value="${idenMap.meta_m_nm}"/>
<front:FrontDynamicHead title="${title}">
<%-- 		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/perm_input.js'/>"></script> --%>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${title}</span>
			</div>
			
		<form name="frm" id="frm" method="post" action="${action}">
		    <c:if test="${not empty childMap}">
		    	<c:set var="child_id_meta_m" value="${childMap.child_id_meta_m}"/>
		    </c:if>
			<input type="hidden" name="id_identity_m" id="id_identity_m" value="${param.id_identity_m}" />
			<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}" />
			<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
			<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
			<input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}"/>
			<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
			<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
			<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
            <c:if test="${idenMap.detl_type eq 'F'}">
             	<c:forEach var="col" items="${headerList}" varStatus="j">
             		<c:if test="${col.detail_param_yn eq 'Y'}">
			<input type="hidden" name="key_${col.meta_cd}" id="key_${col.meta_cd}"/>
             		</c:if>
         		</c:forEach>
            </c:if>
			<div class="page-content-wrap">
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
				              		<div class="tit_cont">검색조건</div>
						          	<select class="form-control mh-form-control" name="search_col" id="search_col">
					          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
						          	<c:forEach var="map" items="${headerList}" varStatus="i">
						                <option value="${map.id_meta}" ${(param.search_col eq map.id_meta? 'selected':'')} data-typ="${map.typ}">${map.meta_nm}</option>
						          	</c:forEach>
						       		</select>
			              			<div class="tit_cont">검색어</div>
	<%-- 		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${(empty param.search_keyword ?'':param.search_keyword)}"> --%>
									<input type="text" class="form-control mh-form-control" name="search_keyword" id="search_keyword" value="${paramMap.search_keyword}">
			              			<div class="tit_cont">검색기간</div>
			              			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
			              			-
			              			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
				        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
						<c:if test="${!is_guest}">
							<button type="button" class="btn h34 fl" onClick="javascript:openRegContreg();"><span class="btn_add"></span>수동입력</button>
						</c:if>
						</div>					
					</div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty idenMap.grid_width}">
				    <c:set var="grid_width" value="style='width:${idenMap.grid_width}%;'" />
				</c:if>
		            <div class="grid-area" ${grid_area_style}>
	                    <!-- 그리드 해더영역 -->
	                    <div class="grid-header">
	                        <c:forEach var="map" items="${headerList}" varStatus="i">
	                        <c:if test="${map.list_show_yn eq 'Y'}">
		                       	<c:if test="${not empty map.show_width}">
		                       	<c:set var="show_width" value="style='width:${map.show_width}%;'"/>
		                       	</c:if>
	                        <input type="hidden" id="h_${map.meta_cd}" value="${map.meta_cd}">
	                        <div class="header-unit" ${(empty map.show_width?'':show_width)}>
	                        <ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}" span_val="${map.meta_nm}"></ctrl:orderMark>
	                        </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
	                        </c:if>
	                        </c:forEach>
	                       <c:if test="${idenMap.detl_type eq 'F'}">
	                       	<div class="header-unit"  style="width:15%;">
	                       	<span>상세</span>
	                       	</div>
	                       </c:if>
	                    </div>
	                    <!-- //그리드 해더영역 -->
		                <!-- 그리드 컨텐츠영역 -->
	                    <div class="grid-contents">
	                    <c:forEach var="row" items="${list}" varStatus="i">
                			<c:set var="param_list" value=''/>
		                  	<c:forEach var="col" items="${headerList}" varStatus="j">
			                  	<c:if test="${col.detail_param_yn eq 'Y'}">
			                  		<c:set var="meta_cd" value="${col.meta_cd}"/>
			                  		<c:choose>
			                  			<c:when test="${typ eq GlobalConst.DT_NUM}">
			                  				<c:set var="param_list" value="${param_list}${row[meta_cd]},"/>
			                  			</c:when>
			                  			<c:otherwise>
			                  				<c:set var="param_list" value="${param_list}'${row[meta_cd]}',"/>
			                  			</c:otherwise>
			                  		</c:choose>
			                  	</c:if>
			              	</c:forEach>
			              	<c:if test="${not empty param_list}">
			              		<c:set var="param_list" value="${fn:substring(param_list, 0, fn:length(param_list) - 1)}"/>
			              	</c:if>
	                  		<c:choose>
	                  			<c:when test="${idenMap.detl_type eq 'F'}">
	                        <div class="grid-loof-cont" id="row_${i.count}" ondblclick="javascript:openDetail(${param_list});">
	                  			</c:when>
	                  			<c:otherwise>
	                        <div class="grid-loof-cont" id="row_${i.count}">
	                  			</c:otherwise>
	                  		</c:choose>
	                        <c:forEach var="col" items="${headerList}" varStatus="j">
	<%--                         col=[${col}]<br> --%>
	<%--                         row=[${row}] --%>
	                        <c:if test="${col.list_show_yn eq 'Y'}">
	                        	<c:set var="meta_cd" value="${col.meta_cd}"/>
	                        	<c:if test="${not empty col.show_width}">
	                        	<c:set var="show_width" value="style='width:${col.show_width}%;'"/>
	                        	</c:if>
	                         <div class="cont-unit" ${(empty col.show_width?'':show_width)}>
	                         <c:choose>
	                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
	                         	${row[meta_cd]}
	                         	</c:when>
	                         	<c:otherwise>
	                         	<a href="${row[meta_nm]}" target="blank">${row[meta_nm]}</a>
	                         	</c:otherwise>
	                         </c:choose>
	           				 </div>
	           				 </c:if>
	                    	</c:forEach>
	                       <c:if test="${idenMap.detl_type eq 'F'}">
		                       	<div class="cont-unit has-btn" style="width:15%;">
		                       	<button type="button" class="btn h34 fl" onClick="javascript:openDetail(${param_list});"><span class="btn_add">상세</span></button>
		                       	</div>
	                       </c:if>
	                       </div>
	                    </c:forEach>
	                    </div>
		                        <!-- //그리드 컨텐츠영역 -->
		            </div>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>

			</div>
    	</form>
		</div>
	</div>
</front:FrontDynamicBody>
<script>
$(function () {
// 	var act_type = "${param.act_type}";
// 	var ret = "${param.ret}";
// 	if(act_type =='D'){
// 		if(ret==0){
// 			infoOpen('성공하였습니다.');
// 		}else if(ret==1){
//     		alertOpen('실패하였습니다.');
// 		}
// 	}
});
<c:if test="${idenMap.detl_type eq 'F'}">
	<c:set var="param_list" value=''/>
    <c:forEach var="col" items="${headerList}" varStatus="j">
    	<c:if test="${col.detail_param_yn eq 'Y'}">
    		<c:set var="meta_cd" value="${col.meta_cd}"/>
			<c:set var="param_list" value="${param_list}${meta_cd},"/>
    	</c:if>
   	</c:forEach>
   	<c:if test="${not empty param_list}">
   		<c:set var="param_list" value="${fn:substring(param_list, 0, fn:length(param_list) - 1)}"/>
   	</c:if>
 	function openDetail(${param_list}){
        <c:forEach var="col" items="${headerList}" varStatus="j">
    	<c:if test="${col.detail_param_yn eq 'Y'}">
    		<c:set var="meta_cd" value="${col.meta_cd}"/>
        $("#key_${col.meta_cd}").val(${meta_cd});
    	</c:if>
	</c:forEach>
	frm.action="${detail_action}";
	frm.submit();
 	}
</c:if>
function initSearchForm(){
	$("#search_keyword").val('');
}
function search(){
	doList(1);
}
function doList(page){
	processing();
	var frm =document.frm;
	$("#page").val(page);
   	$("#frm").submit();
}
</script>
</html>
