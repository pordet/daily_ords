package kr.co.daily.usr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.usr.dao.GuestDAO;

@Service
public class GuestService {
	@Autowired GuestDAO dao;
	
	public void insertUsr(CommandMap map) {
		dao.insertUsr(map.getMap());
	}

	public void deleteUsr(CommandMap map) {
		dao.deleteUsr(map.getMap());
	}
	public void insertUsrTran(CommandMap map) {
		dao.insertUsr(map.getMap());
		CommandMap map1 = new CommandMap();
		map1.put("id","test1");
		map1.put("pwd", "1");
		dao.insertUsr(map1.getMap());
	}
	
	public CustomMap getUsr(CommandMap map) {
		CustomMap ret = dao.getUsr(map.getMap());
		return ret;
	}
	
	public CustomMap getcrsRequestAdm(CommandMap map) {
		CustomMap ret = dao.getcrsRequestAdm(map.getMap());
		return ret;
	}

	public PageList<CustomMap> selectUsrList(CommandMap map) throws Exception {
		return dao.selectUsrList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public int checkTmpUsrByEmail(CommandMap map) {
		return dao.checkTmpUsrByEmail(map.getMap());
	}

	public CustomMap confirmTmpUsr(CommandMap map) {
		return dao.confirmTmpUsr(map.getMap());
	}

	public void insertConfirmInfo(CommandMap map) {
		dao.insertConfirmInfo(map.getMap());
	}

	public void updateConfirmInfo(CommandMap map) {
		dao.updateConfirmInfo(map.getMap());
	}

	public CustomMap loginAction(CommandMap map) {
		return dao.loginAction(map.getMap());
	}

	public CustomMap guestLoginAction(CommandMap map) {
		return dao.guestLoginAction(map.getMap());
	}

	public void insertApplicantDetail(CommandMap map) {
		dao.insertApplicantDetail(map.getMap());
	}

	public void insertPersonNation(CommandMap map) {
		dao.insertPersonNation(map.getMap());
	}

	public void insertPersonProfession(CommandMap map) {
		dao.insertPersonProfession(map.getMap());
	}

	public void insertPersonOccupation(CommandMap map) {
		dao.insertPersonOccupation(map.getMap());
	}

	public void insertApplicant(CommandMap map) {
		dao.insertApplicant(map.getMap());
		dao.insertApplicantDetail(map.getMap());
	}

	public CustomMap getGuestPassword(CommandMap map) {
		return dao.getGuestPassword(map.getMap());
	}

	public void deleteConfirmInfo(CommandMap map) {
		dao.deleteConfirmInfo(map.getMap());
	}

	public CustomMap insertGuestReset(CommandMap map) {
		dao.deleteGuestReset(map.getMap());
		dao.insertGuestReset(map.getMap());
		dao.updatePersonalPassword(map.getMap());
		return dao.getGuestReset(map.getMap());
	}

	public void updatePwd(CommandMap map) {
		dao.updatePwd(map.getMap());
	}

	public CustomMap getGuestReset(CommandMap commandMap) {
		return null;
	}

	public CustomMap getCheckApplicatByNidNumber(CommandMap map) {
		return dao.getCheckApplicatByNidNumber(map.getMap());
	}

	public void updateApplicant(CommandMap map) {
		dao.updateApplicant(map.getMap());
		dao.updateApplicantDetail(map.getMap());
	}

	public void insertApplicantByPassport(CommandMap map) {
		dao.insertApplicant(map.getMap());
		dao.insertApplicantByPassport(map.getMap());
	}

	public void updateApplicantByPassport(CommandMap map) {
		dao.updateApplicant(map.getMap());
		dao.updateApplicantDetailByPassport(map.getMap());
	}

	public int getApplicantCntByEmail(CommandMap map) {
		return dao.getApplicantCntByEmail(map.getMap());
	}

	public int getApplicantCntByNid(CommandMap map) {
		return dao.getApplicantCntByNid(map.getMap());
	}

	public CustomMap getPersonNation(CommandMap map) {
		return dao.getPersonNation(map.getMap());
	}

	public CustomMap getPersonProfession(CommandMap map) {
		return dao.getPersonProfession(map.getMap());
	}

	public CustomMap getPersonOccupation(CommandMap map) {
		return dao.getPersonOccupation(map.getMap());
	}

}
