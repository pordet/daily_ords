<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="search_col" required="false" %>
<%@ attribute name="search_colList" required="true" %>
<%@ attribute name="search_keyword" required="false" %>
<c:forEach var="col" items="${search_colList}">
	<div class="search-group-double">
		<div class="text-column">
	        <p>텍스트</p>
	    </div>
	    <div class="input-column">
	        <input type="text" placeholder="입력란">
	    </div>
	</div>
</c:forEach>
							<div class="search-group-double">
								<div class="search-group-single-input">
									<select name="search_col">
									<c:forEach var="col" items="${search_colList}">
	                                    <option value="${col.meta_cd}" ${(search_col eq col.meta_cd? 'selected':'')}>${col.meta_nm}"</option>
									</c:forEach>
									
									</select>
									<input type="text" class="" name="search_keyword" id="search_keyword"  value="${search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>

