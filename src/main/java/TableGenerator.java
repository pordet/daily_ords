import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableGenerator {
    public static void main(String[] args) {
        // 테이블 헤더 정보를 가지는 List<Map> 생성
        List<Map<String, Integer>> data = new ArrayList<>();
        data.add(createRow(1, 0, 0));
        data.add(createRow(2, 1, 1));
        data.add(createRow(4, 1, 1));
        data.add(createRow(3, 2, 2));
        data.add(createRow(5, 2, 2));

        // 테이블 헤더 출력
        generateTable(data);
    }

    public static Map<String, Integer> createRow(int id, int parentId, int depth) {
        Map<String, Integer> row = new HashMap<>();
        row.put("id", id);
        row.put("parentId", parentId);
        row.put("depth", depth);
        return row;
    }

    public static void generateTable(List<Map<String, Integer>> data) {
        System.out.println("<table border='1'>");

        int maxDepth = getMaxDepth(data);

        for (int depth = 0; depth <= maxDepth; depth++) {
            System.out.println("<tr>");

            for (Map<String, Integer> row : data) {
                if (row.get("depth") == depth) {
                    int rowspan = calculateRowspan(data, row);
                    int colspan = calculateColspan(data, row);
                    String headerText = "ID: " + row.get("id") + " (Depth: " + row.get("depth") + ")";
                    
                    System.out.println("<th rowspan='" + rowspan + "' colspan='" + colspan + "'>" + headerText + "</th>");
                }
            }

            System.out.println("</tr>");
        }

        System.out.println("</table>");
    }

    public static int calculateRowspan(List<Map<String, Integer>> data, Map<String, Integer> currentRow) {
        int depth = currentRow.get("depth");
        int rowspan = 1;

        for (Map<String, Integer> row : data) {
            if (row.get("depth") > depth && row.get("parentId") == currentRow.get("id")) {
                rowspan++;
            }
        }

        return rowspan;
    }

    public static int calculateColspan(List<Map<String, Integer>> data, Map<String, Integer> currentRow) {
        int id = currentRow.get("id");
        int colspan = 0;

        for (Map<String, Integer> row : data) {
            if (row.get("parentId") == id) {
                colspan++;
            }
        }

        return colspan;
    }

    public static int getMaxDepth(List<Map<String, Integer>> data) {
        int maxDepth = 0;
        for (Map<String, Integer> row : data) {
            int depth = row.get("depth");
            if (depth > maxDepth) {
                maxDepth = depth;
            }
        }
        return maxDepth;
    }
}
