<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="isAdmin" value="N"/>
<!DOCTYPE html>
<front:FrontHead title="">
	<script src="<c:url value='/resource/plugins/rwdImageMap/jquery.rwdImageMaps.js'/>"></script>
	<script src="<c:url value='/resource/plugins/echarts/echarts.min.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/content_change.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/zone/file_list_register.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/zone/file_list_input.js'/>"></script>
	<style>
 	#netImg{
 		max-width:100%; 
 		width:100%;
/*  		height: auto !important;  */
 	} 
	</style>
</front:FrontHead>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
                 <form id="frm" name="frm" method="post">
                 <input type="hidden" name="type" value="3">
                    <div class="row" style="margin-top:-80px;">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색시작년도
<%-- 	                                  [${param.st_year }] --%>
	                                  </div>
										<select class="form-control mh-form-control" title="검색 시작년도" name="st_year" id="st_year">
											<option value="">없음</option>
										<c:if test="${not empty yd}">
										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}">
											<option value="${st_year}" <c:if test="${(not empty param.st_year ? param.st_year : yd.min_year) eq st_year}"> selected="selected"</c:if>>${st_year}</option>
										</c:forEach>
										</c:if>
											</select>
<!-- 										<select title="검색 종료년도" name="ed_year" id="ed_year"> -->
<%-- 										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${ed_year}" <c:if test="${ed_year eq yd.max_year}"> selected="selected"</c:if>>${ed_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 										</select> -->
	                              </div>
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색종료년도
<%-- 	                                  [${param.ed_year }] --%>
	                                  </div>
										<select class="form-control mh-form-control" title="종료년도" name="ed_year" id="ed_year">
											<option value="">없음</option>
										<c:if test="${not empty yd}">
										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}">
											<option value="${ed_year}" <c:if test="${(not empty param.ed_year ? param.ed_year : yd.max_year) eq ed_year}"> selected="selected"</c:if>>${ed_year}</option>
										</c:forEach>
										</c:if>
										</select>
										
	                              </div>
	                                <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">카테고리명</div>
	                                  <select class="form-control mh-form-control" id="id_category" name="id_category" onchange="javascript:changeCategory(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
	                                    		</c:if>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">국가명</div>
	                                  <select class="form-control mh-form-control" id="id_nat_area" name="id_nat_area" onchange="javascript:changeNation(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_nat_area ? 'selected':'')}>선택</option>
	                                    		</c:if>
	                                    		<c:set var="cur_nat_area" value="${(empty param.id_nat_area?firstNat.id_area:param.id_nat_area)}"/>
				                            <c:forEach var="map" items="${natList}" varStatus="i">
	                                    		<option value="${map.id_area}" ${(cur_nat_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">도시명</div>
	                                  <select class="form-control mh-form-control" id="id_city_area" name="id_city_area" onchange="javascript:changeCity(this);">
	                                     		<option value="" ${(empty param.id_city_area ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${cityList}" varStatus="i">
                                   		<option value="${map.id_area}" ${(param.id_city_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              	<c:if test="${fn:length(itemList)>0}">
	                              <div class="form-group inline-group" id="id_meta_select_div" style="width: 160px">
	                                  <div class="tit_cont">세부항목</div>
	                                  <select class="form-control mh-form-control" id="id_meta" name="id_meta">
	                                     		<option value="" ${(empty param.id_meta ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${itemList}" varStatus="i">
                                   		<option value="${map.id_meta}" ${(param.id_meta eq map.id_meta? 'selected':'')}>${map.unit_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              	</c:if>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
	                              <button type="button" class="btn h34" onclick="javascript:goDataMap();">Data Map</button>
	                       		<div class="fr">
	                              <button type="button" class="btn h34" onclick="javascript:openAjaxInput();" style="margin-top:20px;">등록/수정</button>
	                            </div>
	                       </div>
	                      </div>
	                     </div>
<!-- 				<div class="searchBox"> -->
<!-- 					<div class="t"> -->
<!-- 						<table class="search"> -->
<%-- 							<colgroup> --%>
<%-- 								<col style="width:120px"> --%>
<%-- 								<col style="width:auto"> --%>
<%-- 							</colgroup> --%>
<!-- 							<tbody> -->
<!-- 								<tr> -->
<!-- 									<th scope="row">연도 선택</th> -->
<!-- 									<td> -->
<!-- 										<select title="검색 시작년도" name="st_year" id="st_year"> -->
<%-- 										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${st_year}" <c:if test="${st_year eq yd.min_year}"> selected="selected"</c:if>>${st_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 											</select> ~ -->
<!-- 										<select title="검색 종료년도" name="ed_year" id="ed_year"> -->
<%-- 										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${ed_year}" <c:if test="${ed_year eq yd.max_year}"> selected="selected"</c:if>>${ed_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 										</select> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 							</tbody> -->
<!-- 						</table> -->
<!-- 					</div> -->
<!-- 					<div class="btnBox"><button type="submit">선택조건검색</button></div> -->
<!-- 				</div> -->
	                    </form>
				<div class="dbBtnbox">
					<div class="l">
					<front:FrontNavi id_category="${param.id_category}" id_nat_area="${param.id_nat_area}" id_city_area="${param.id_city_area}" id_zone="${param.id_zone}"></front:FrontNavi>
					</div>
				</div>
				<div class="dbBtnbox">
					<div class="l" id="network_btn_list">
						<c:forEach var="map" items="${rowList}" varStatus="i">
						<a href="javascript:void(0);" class="midBtn btnEme" onclick="show_map(${i.count});return false">${map.file_desc}</a>
						<input type="hidden" id="hdn_img_${i.count}" value="${map.file}">
						</c:forEach>
					</div>					
				</div>
				<div class="boardBox" tabindex="0" id="network_img">
				<c:if test="${fn:length(rowList)>0}">
					<img src="${rowList[0].file}" id="netImg">
				</c:if>
				</div>
    <div class="modal fade mh-modal" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModal" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="data_input_pop_title">데이터입력</h3>
                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
               <form id="reg_frm" name="reg_frm">
                <input type="hidden" id="reg_id_category" name="id_category">
                <input type="hidden" id="reg_id_nat_area" name="id_nat_area">
                <input type="hidden" id="reg_id_city_area" name="id_city_area">
                <input type="hidden" id="reg_id_meta" name="id_meta">
                <div class="modal-body">
                    <div class="row record-wrap">
				    			<table width="100%" id="file_list_tab">
				    				<tr>
					    				<td width="8%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">순서</div>
					    			         	<div class="form-group inline-group">
	 												<a href="javascript:void(0);" onClick="javascript:down_order();"><img src="<c:url value='/resource/img/down_black.png'/>" alt="추가" class="ico_plus"/></a>
	 				    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="40%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 파일</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" disabled>
	                                                    <div class="attach-btn">
	                                                      <button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>
	                                                      <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
	                                                    </div>
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="26%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 설명</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control">
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="8%">
	                      					<div class="col-12" style="margin-left:13px;">
						    				  <div class="tit_cont" style="margin-top:2px;">추가</div>
						    			   	  <div class="form-group">
													<a href="javascript:void(0);" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_plus.png'/>" alt="추가" class="ico_plus"/></a>
						    			   		</div>
						    			   	</div>
					    				</td>
				    				</tr>
				    				<tr>
					    				<td width="8%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">순서</div>
					    			         	<div class="form-group inline-group">
	 												<img src="<c:url value='/resource/img/updown_black.png'/>" alt="추가" class="ico_plus" usemap="#trMap"/>
						                            <map name="trMap" id="trMap">
						                            	<area shape="rect" coords="0,0,25,50" onclick="javascript:up_order();" onmouseover="document.body.style.cursor='pointer';" onmouseout="document.body.style.cursor='default';"/>
						                            	<area shape="rect" coords="25,0,50,50" onclick="javascript:down_order();" onmouseover="document.body.style.cursor='pointer';" onmouseout="document.body.style.cursor='default';"/>
						                            </map>
	 				    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="40%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 파일</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" disabled>
	                                                    <div class="attach-btn">
	                                                      <button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>
	                                                      <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
	                                                    </div>
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="26%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 설명</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control">
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="8%">
	                      					<div class="col-12" style="margin-left:13px;">
						    				  <div class="tit_cont" style="margin-top:2px;">추가</div>
						    			   	  <div class="form-group">
													<a href="javascript:void(0);" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_plus.png'/>" alt="추가" class="ico_plus"/></a>
						    			   		</div>
						    			   	</div>
					    				</td>
				    				</tr>
				    				<tr>
					    				<td width="8%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">순서</div>
					    			         	<div class="form-group inline-group">
	 												<a href="javascript:void(0);" onClick="javascript:up_order();"><img src="<c:url value='/resource/img/up_black.png'/>" alt="추가" class="ico_plus"/></a>
	 				    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="40%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 파일</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" disabled>
	                                                    <div class="attach-btn">
	                                                      <button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>
	                                                      <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
	                                                    </div>
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="26%">
	                       					<div class="col-12">
					    				  		<div class="tit_cont">네트워크 설명</div>
					    			         	<div class="form-group inline-group">
	                                                    <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control">
					    			         	</div>
					    			         </div>
					    				</td>
					    				<td width="10%">
	                      					<div class="col-12">
						    				  <div class="tit_cont">다운로드</div>
						    			   	  <div class="form-group" style="margin-left:8px;">
						    			         <a href="" download="">
						    			         <img src="<c:url value="/resource/img/ico_download.png"/>" alt="다운로드" style="width:38px;height:20px;"/>
						    			         </a>  
						    			   	  </div>
						    			   	 </div>
					    				  </td>
					    				<td width="8%">
	                      					<div class="col-12">
						    				  <div class="tit_cont" style="margin-top:2px;">추가</div>
						    			   	  <div class="form-group">
													<a href="javascript:void(0);" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_plus.png'/>" alt="추가" class="ico_plus"/></a>
						    			   		</div>
						    			   	</div>
					    				  </td>
					    				<td width="8%">
	                      					<div class="col-12">
						    				  <div class="tit_cont">삭제</div>
						    			   	  <div class="form-group">
	                                             <a href="javascript:void(0);" onClick="javascript:delAttach(${file.file_num});"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete" class="ico_delete" /></a>
						    			   		</div>
						    			   		</div>
					    				  </td>
				    				  </tr>
				    			</table>
                   </div>
              </div>
                </form>
							<!--modal-body end -->
                <div class="modal-footer mt-20p" id="btnArea1" style="display:block;text-align:center;">
			                        <button type="button" class="btn cancel" onclick="javascript:delData();">삭제</button>
			                        <button type="button" class="btn" onclick="javascript:register(1);">저장</button>
                </div>
              </div>
    	</div>
    </div>
<script>
$(function() {
 	$('#zoneImg').rwdImageMaps();
//	$("#previewModal").modal('show');	
 	
});

function show_zone_detail(no){
	$("#zoneDetail_title").html('Zone '+no +' 정보');
	$("#zoneDetailModal").modal('show');
}

function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/content.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function delData(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('존, 도시, 국가를 선택하십시오.');
		return;
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}
	var params = jQuery("#reg_frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.year+"_"+d.id_meta_meta).remove();
	    		});
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
		    	var re_param = $("#frm").serialize();
		    	var re_url = contextPath+"/contentAjax.do";
		    	$.ajax({
		    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    	    type: "POST", // HTTP 요청 방식(GET, POST)
		    	    data : re_param,
		    	    dataType: "json" ,
		    	    success : function(e){
		    	    	var con = "";
                        var st_html='';
                        var ed_html='<option value="">없음</option>';
	    	    		if(nanToStr(e.yd)==''){
		    	    		con+='<tbody>';
		    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
		    	    		$("#st_year").html('<option value="">없음</option>');
		    	    		$("#ed_year").html('<option value="">없음</option>');
	    	    		}else{
		    	    			
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<tr>';
		    	    			$.each(e.rowList,function(i,d){
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'" dataType="'+f.data_type+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
			    	    				var isEmpty =false;
				    	    			$.each(d.child,function(j,f){
				    	    				if(year == f.year){
				    	    					isEmpty = true;
							    	    		con+='<td class="l">'+f.val+'</td>';
				    	    				}
				    	    			});
				    	    			if(isEmpty ==false){
						    	    		con+='<td></td>';
				    	    			}
			    	    			}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}
		    	    },
		    	    error : function(e){
		    	    	endProcessing();
		    	    	if(e.status=='403'){
		    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	    	}else if(e.status=='401'){
		    			    alertOpen('접근 권한이 없습니다.');
		    	    	}else{
		    		    	alertOpen('실패하였습니다.');
		    	    	}
		    	    }
		    	});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function show_map(i){
	$("#netImg").attr("src",$("#hdn_img_"+i).val());
}
</script>
</front:FrontBody>
</html>