package daily.common.security;

import java.util.Collection;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;

/**
 * 권한 인증을 처리하는 Voter
 * @author pordet
 *
 */
public class SecurityAccessDecisionVoter implements AccessDecisionVoter<FilterInvocation> {

	@Override
	public boolean supports(ConfigAttribute attribute) {
		return attribute instanceof SecurityConfig;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz != null && clazz.isAssignableFrom(FilterInvocation.class);
	}

	/**
	 * grantedAuthority.getAuthority() : 요청 url의 허용 권한
	 * 요청 url을 얻는 방법 : object.getRequestUrl()
	 */
	@Override
	public int vote(Authentication authentication, FilterInvocation object, Collection<ConfigAttribute> attributes) {
		SecurityConfig securityConfig = null; 
		boolean containAuthority = false; 
		for(final ConfigAttribute configAttribute : attributes) { 
			if(configAttribute instanceof SecurityConfig) {
				securityConfig = (SecurityConfig) configAttribute; 
				for(GrantedAuthority grantedAuthority : authentication.getAuthorities()) { 
					if(grantedAuthority.getAuthority().equals("ROLE_ANONYMOUS")|| grantedAuthority.getAuthority().equals("GUEST")) {
						containAuthority = true;
					}else {
						containAuthority = securityConfig.getAttribute().equals(grantedAuthority.getAuthority());
					}
					if(containAuthority) { 
						break; 
					} 
				} 
				if(containAuthority) {
						break; 
				} 
			} 
		} 
		return containAuthority ? ACCESS_GRANTED : ACCESS_DENIED;

	}

}
