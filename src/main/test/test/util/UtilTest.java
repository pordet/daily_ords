package test.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.jupiter.api.Test;

import daily.common.util.MetaUtil;
import daily.common.vo.CustomMap;

public class UtilTest {
	@Test
	public void main() {
        List<CustomMap> mapList =MapTestUtil.generateSampleData();
        
		List<List<CustomMap>> headerList = new ArrayList<>();
	    //Map<id_meta,List<col> 형태로 child 맵 구성
	    Map<Integer,CustomMap> colMap = new HashMap<>();
	    //Map<depth,Map<id_meta,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthMap = new HashMap<>();
	    //Map<depth,Map<ord,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthOrdMap = new HashMap<>();
	    //id_parent_meta,Map<id_meta,Col> 자식담은 맵
	    Map<Integer,Map<Integer,CustomMap>> childMap = new HashMap<>();
        // List<Map> 출력
	    List<CustomMap> topList = new ArrayList<>();
	    List<CustomMap> slaveList = new ArrayList<>();
        for (CustomMap map : mapList) {
        	if(map.getInt("meta_depth",0)==1 && "Y".equals(map.getString("use_yn"))) {
        		topList.add(map);
        	}else if(map.getInt("meta_depth",0)>1) {
        		slaveList.add(map);
        	}
        }
	    for(CustomMap sl:topList) {
	    	colMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    }
	    for(CustomMap sl:slaveList) {
	    	boolean isListShowCal = false;
	    	if(sl.getInt("id_meta",0)==3||sl.getInt("id_meta",0)==5||sl.getInt("id_meta",0)==7||sl.getInt("id_meta",0)==8) {
	    		System.out.println(sl.getInt("id_meta",0)+":id_meta 테스트 시작");
	    	}
    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null){
    			isListShowCal = true;
    		}
//	    	if(sl.getInt("meta_depth",0)==2) {
//	    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null) {
//	    			isListShowCal = true;
//	    		}
//	    	}else {
//	    		if(childMap.get(sl.getInt("id_parent_meta",0))==null){
//	    			isListShowCal = true;
//	    		}
//	    	}
	    	if(isListShowCal) {
		    	colMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerChildMap = childMap.computeIfAbsent(sl.getInt("id_parent_meta"), k -> new HashMap<>());
		    	innerChildMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    	}
	    }
	    int real_max_depth = MetaUtil.findMaxKey(depthOrdMap);

	    for (int depth = real_max_depth; depth >= 1; depth--) {
    		Map<Integer,CustomMap> depthColMap=depthMap.get(depth);
	    	if(depth==real_max_depth) {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		cellMap.put("rowspan", 1);
		    		cellMap.put("colspan", cellMap.getInt("colspan",1));
	    		}
	    	}else {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		Map<Integer, CustomMap> innerChildMap = childMap.get(cellMap.getInt("id_meta"));
		    		//없는 경우
		    		if(innerChildMap==null) {
			    		cellMap.put("rowspan", real_max_depth-depth+1);
			    		cellMap.put("colspan", 1);
		    		}else {
			    		cellMap.put("rowspan", 1);
			    		if(innerChildMap.size()==0) {
				    		cellMap.put("colspan", 1);
			    		}else {
				    		int colspan = 0;
				    		for(int inner_id_meta:innerChildMap.keySet()) {
				    			CustomMap innerChild = innerChildMap.get(inner_id_meta);
				    			colspan+=innerChild.getInt("colspan",1);
				    		}
				    		cellMap.put("colspan", colspan);
			    		}
		    		}
	    		}
	    	}
	    }
	    //1depth인 것 가져오서 정렬해서 List<CustomMap> 만들기
	    Map<Integer, CustomMap> levelOneMap = depthOrdMap.get(1);
        List<CustomMap> dataRow = new ArrayList<>();

        if (levelOneMap != null) {
            List<CustomMap> resultList = new ArrayList<>();
            // key를 정렬한 후 순서대로 CustomMap을 List에 추가
            SortedSet<Integer> sortedKeys = new TreeSet<>(levelOneMap.keySet());
            for (Integer key : sortedKeys) {
                resultList.add(levelOneMap.get(key));
            }
            // 결과 출력
            for (CustomMap cell : resultList) {
    	    	if(cell.getInt("id_meta",0)==78||cell.getInt("id_meta",0)==83) {
    	    		System.out.println(cell.getInt("id_meta",0)+":cell id_meta 테스트 시작");
    	    	}
    	    	
            	List<CustomMap> useColList = MetaUtil.generateRealDataRow(cell,childMap);
            	dataRow.addAll(useColList);
            }
        } else {
            System.out.println("조회하려는 컬럼 설정을 찾지 못했습니다. 관리자에게 문의바랍니다.");
        }

	    TreeMap<Integer, Map<Integer, CustomMap>> sortedMap = new TreeMap<>(depthOrdMap);
	    for (Map.Entry<Integer, Map<Integer, CustomMap>> entry : sortedMap.entrySet()) {
            List<CustomMap> innerList = new ArrayList<>();

            // Map<Integer, CustomMap>에서 정렬된 키로 CustomMap을 가져옴
            Map<Integer, CustomMap> innerMap = entry.getValue();
            TreeMap<Integer, CustomMap> sortedInnerMap = new TreeMap<>(innerMap);

            for (Map.Entry<Integer, CustomMap> innerEntry : sortedInnerMap.entrySet()) {
                innerList.add(innerEntry.getValue());
            }

            headerList.add(innerList);
        }
	    for(List<CustomMap> childList:headerList) {
	    	for(CustomMap child:childList) {
	    		System.out.println(child.toString());
	    	}
	    }
    }
	@Test
	public void testHeaderData() {
        List<CustomMap> mapList =MapTestUtil.generateSampleData();
        
        //2차원 데이터 출력 테스트 설정
        List<CustomMap> childList =MapTestUtil.generateChildData();
        List<CustomMap> fkList =MapTestUtil.generateFkData();
        
        Map<Integer,List<CustomMap>> fkColMap = new HashMap<>();
        fkColMap.put(1, fkList);
        
        
		List<List<CustomMap>> headerList = new ArrayList<>();
	    //Map<id_meta,List<col> 형태로 child 맵 구성
	    Map<Integer,CustomMap> colMap = new HashMap<>();
	    //Map<depth,Map<id_meta,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthMap = new HashMap<>();
	    //Map<depth,Map<ord,List<col>> 형태로 child 맵 구성
	    Map<Integer,Map<Integer,CustomMap>> depthOrdMap = new HashMap<>();
	    //id_parent_meta,Map<id_meta,Col> 자식담은 맵
	    Map<Integer,Map<Integer,CustomMap>> childMap = new HashMap<>();
        // List<Map> 출력
	    List<CustomMap> topList = new ArrayList<>();
	    List<CustomMap> slaveList = new ArrayList<>();
        for (CustomMap map : mapList) {
        	if(map.getInt("meta_depth",0)==1 && "Y".equals(map.getString("use_yn"))) {
        		topList.add(map);
        	}else if(map.getInt("meta_depth",0)>1) {
        		slaveList.add(map);
        	}
        }
	    for(CustomMap sl:topList) {
	    	colMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerMap.put(sl.getInt("id_meta"),sl);
	    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(1, k -> new HashMap<>());
	    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    }
	    for(CustomMap sl:slaveList) {
	    	boolean isListShowCal = false;
	    	if(sl.getInt("id_meta",0)==3||sl.getInt("id_meta",0)==5||sl.getInt("id_meta",0)==7||sl.getInt("id_meta",0)==8) {
	    		System.out.println(sl.getInt("id_meta",0)+":id_meta 테스트 시작");
	    	}
    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null){
    			isListShowCal = true;
    		}
//	    	if(sl.getInt("meta_depth",0)==2) {
//	    		if(colMap.get(sl.getInt("id_parent_meta",0))!=null) {
//	    			isListShowCal = true;
//	    		}
//	    	}else {
//	    		if(childMap.get(sl.getInt("id_parent_meta",0))==null){
//	    			isListShowCal = true;
//	    		}
//	    	}
	    	if(isListShowCal) {
		    	colMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerChildMap = childMap.computeIfAbsent(sl.getInt("id_parent_meta"), k -> new HashMap<>());
		    	innerChildMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerMap = depthMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerMap.put(sl.getInt("id_meta"),sl);
		    	Map<Integer, CustomMap> innerOrdMap = depthOrdMap.computeIfAbsent(sl.getInt("meta_depth"), k -> new HashMap<>());
		    	innerOrdMap.put(sl.getInt("row_num"),sl);
	    	}
	    }
	    int real_max_depth = MetaUtil.findMaxKey(depthOrdMap);

	    for (int depth = real_max_depth; depth >= 1; depth--) {
    		Map<Integer,CustomMap> depthColMap=depthMap.get(depth);
	    	if(depth==real_max_depth) {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		cellMap.put("rowspan", 1);
		    		cellMap.put("colspan", cellMap.getInt("colspan",1));
	    		}
	    	}else {
	    		for(int id_meta:depthColMap.keySet()) {
		    		CustomMap cellMap = depthColMap.get(id_meta);
		    		Map<Integer, CustomMap> innerChildMap = childMap.get(cellMap.getInt("id_meta"));
		    		//없는 경우
		    		if(innerChildMap==null) {
			    		cellMap.put("rowspan", real_max_depth-depth+1);
			    		cellMap.put("colspan", 1);
		    		}else {
			    		cellMap.put("rowspan", 1);
			    		if(innerChildMap.size()==0) {
				    		cellMap.put("colspan", 1);
			    		}else {
				    		int colspan = 0;
				    		for(int inner_id_meta:innerChildMap.keySet()) {
				    			CustomMap innerChild = innerChildMap.get(inner_id_meta);
				    			colspan+=innerChild.getInt("colspan",1);
				    		}
				    		cellMap.put("colspan", colspan);
			    		}
		    		}
	    		}
	    	}
	    }
	    //1depth인 것 가져오서 정렬해서 List<CustomMap> 만들기
	    Map<Integer, CustomMap> levelOneMap = depthOrdMap.get(1);
        List<CustomMap> dataRow = new ArrayList<>();

        if (levelOneMap != null) {
            List<CustomMap> resultList = new ArrayList<>();
            // key를 정렬한 후 순서대로 CustomMap을 List에 추가
            SortedSet<Integer> sortedKeys = new TreeSet<>(levelOneMap.keySet());
            for (Integer key : sortedKeys) {
                resultList.add(levelOneMap.get(key));
            }
            // 결과 출력
            for (CustomMap cell : resultList) {
    	    	if(cell.getInt("id_meta",0)==78||cell.getInt("id_meta",0)==83) {
    	    		System.out.println(cell.getInt("id_meta",0)+":cell id_meta 테스트 시작");
    	    	}
    	    	
            	List<CustomMap> useColList = MetaUtil.generateRealDataRow(cell,childMap);
            	dataRow.addAll(useColList);
            }
        } else {
            System.out.println("조회하려는 컬럼 설정을 찾지 못했습니다. 관리자에게 문의바랍니다.");
        }

	    TreeMap<Integer, Map<Integer, CustomMap>> sortedMap = new TreeMap<>(depthOrdMap);
	    for (Map.Entry<Integer, Map<Integer, CustomMap>> entry : sortedMap.entrySet()) {
            List<CustomMap> innerList = new ArrayList<>();

            // Map<Integer, CustomMap>에서 정렬된 키로 CustomMap을 가져옴
            Map<Integer, CustomMap> innerMap = entry.getValue();
            TreeMap<Integer, CustomMap> sortedInnerMap = new TreeMap<>(innerMap);

            for (Map.Entry<Integer, CustomMap> innerEntry : sortedInnerMap.entrySet()) {
                innerList.add(innerEntry.getValue());
            }

            headerList.add(innerList);
        }
	    for(List<CustomMap> testList:headerList) {
	    	for(CustomMap child:testList) {
	    		System.out.println(child.toString());
	    	}
	    }
    }
}
