package kr.co.daily.front.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface CalcFormulaDAO {

	CustomMap getFormulaByIdMeta(Map<String, Object> map);

}
