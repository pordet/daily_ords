<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${metaM.meta_m_nm}"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
				<span>${title} 조회</span>
			</div>
			<form name="frm" id="frm" method="post">
			<div class="page-content-wrap">
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="id_meta_m" name="id_meta_m" value="${param.id_meta_m}"/>
				<input type="hidden" id="trg_id_meta_m" name="trg_id_meta_m" value="${param.trg_id_meta_m}"/>
				<input type="hidden" id="reg_id_meta_m" name="reg_id_meta_m" value="${param.reg_id_meta_m}"/>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
							<div class="search-group-single-input">
			              		<div class="tit_cont">검색조건</div>
					          	<select class="form-control mh-form-control" name="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${aloneList}" varStatus="i">
					                <option value="${map.meta_cd}" ${(param.search_col eq map.meta_cd? 'selected':'')}>${map.meta_nm}</option>
					          	</c:forEach>
					       		</select>
		              			<div class="tit_cont">검색어</div>
		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${param.search_keyword}">
			          		<c:if test="${fn:length(dtSearchList) > 0}">
		              			<div class="tit_cont">검색기간</div>
					          	<select class="form-control mh-form-control" name="search_dt_col">
					          	<c:forEach var="map" items="${dtSearchList}" varStatus="i">
					                <option value="${map.meta_cd}" ${(param.search_dt_col eq map.meta_cd? 'selected':'')}>${map.meta_nm}</option>
					          	</c:forEach>
					          	</select>
		              			<input type="date" class="form-control mh-form-control date_input" id="search_dt_srt" name="search_dt_srt" value="${param.search_dt_srt}">
		              			-
		              			<input type="date" class="form-control mh-form-control date_input" id="search_dt_end" name="search_dt_end" value="${param.search_dt_end}">
							</c:if>
								<button type="button" class="btn-primary" onclick="javascript:search();">
									<span class="btn_search"></span>
								</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
						<button type="button" class="btn h34 fl" onClick="javascript:downExcelForm(${param.id_meta_m});"><span class="btn_add"></span>엑셀 양식 다운로드</button>
					</div>
					<div class="r">
                          <button type="button" class="btn attach1" onclick="document.getElementById('excel_file1').click();"><span class="btn_attach"></span>엑셀 업로드</button>
                          <input type="file" name="excel_file" id="excel_file1" class="file_input_hidden" onchange="javascript: uploadExcel(this,${param.id_meta_m})"/>
 						  <button type="button" class="btn h34 fl" onClick="javascript:openRegMeta(${param.trg_id_meta_m});"><span class="btn_add"></span>수동입력</button>
					</div>					
				</div>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty metaM.grid_width}">
				    <c:set var="grid_width" value="style='width:${metaM.grid_width}%;'" />
				</c:if>
				<div class="tbl-wrap">
		            <table class="list_tbl"  ${grid_width}>
		            	<colgroup >
	                    <c:forEach var="map" items="${aloneList}" varStatus="i">
	                    	<col width="30%"/>
	                    </c:forEach>
		            	</colgroup>
		            	<tbody>
	                        <c:forEach var="row" items="${headerList}" varStatus="i">
	 			            	<tr id="tr_he_${i.count}">
	                        		<c:forEach var="col" items="${row}" varStatus="j">
	                        			<th
	                        		<c:if test="${not empty col.rowspan}">
	                        				rowspan="${col.rowspan}"
	                        		</c:if>
	                        		<c:if test="${not empty col.colspan}">
	                        				colspan="${col.colspan}"
	                        		</c:if>
				                         >${col.meta_nm}
			           				   </th>
	                        		</c:forEach>
			                    </tr>
	                    	</c:forEach>
							<c:forEach var="row" items="${list}" varStatus="i">
							    <tr id="tr_li_${i.count}" onclick="openRegMeta(${row.id_meta_m},${row.id_meta_row});">
							        <c:forEach var="show_col" items="${aloneList}" varStatus="j">
							            <c:set var="meta_cd" value="${show_col.meta_cd}"/>
							            <c:set var="data" value="${row[meta_cd]}"/>
							            <td>${data}</td>
							        </c:forEach>
							    </tr>
							</c:forEach>
		            	</tbody>
		            </table>
		       </div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
                   <!-- //paging -->
	    	</div>
			</form>
<script>
function doList(page){
	$("#page").val(page);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/metaList.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
function search(){
	doList(1);
}
function initSearchForm(){
	$("select[name=search_col] option:eq(0)").attr("selected","selected");
	$("input[name=search_keyword]").val('');
	<c:if test="${fn:length(dtSearchList) > 0}">
	$("#search_dt_srt").val('');
	$("#search_dt_end").val('');
	</c:if>
	$("#search_keyword").val('');
}
function openRegMeta(id_meta_m,id_meta_row){
	if(id_meta_row){
		$("#id_meta_row").val(id_meta_row);
	}
	$("#reg_id_meta_m").val(id_meta_m);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/regMetaData.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
	
}
function downExcelForm(id_meta_m) {
	var params={id_meta_m:id_meta_m}
	var url = contextPath+"/front/meta/downloadAjax.do";
    // 서버에서 엑셀 파일을 다운로드 받습니다.
    $.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    method: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
        success: function (data) {
// 			var fileUrl = data.fileUrl;
            
//             // 파일 다운로드를 위해 새 창을 열어서 해당 URL로 이동
//             window.open(fileUrl, '_blank');
        	var binary = atob(data.content);
        	var array = [];
        	for (var i = 0; i < binary.length; i++) {
        		array.push(binary.charCodeAt(i));
        	}
        	var blob = new Blob([new Uint8Array(array)], {type: 'application/octet-stream'});

        	var linkElement = document.createElement('a');
        	linkElement.href = window.URL.createObjectURL(blob);
        	linkElement.download = data.fileNm;
        	
        	document.body.appendChild(linkElement);
        	linkElement.click();
        	document.body.removeChild(linkElement);        
        },
        error: function () {
            console.error("다운로드 에러:");
        }
    });
}
function uploadExcel(that,id_meta_m){
	var formData = new FormData();
	formData.append('id_meta_m', id_meta_m); // 추가 데이터

	var fileInput = document.getElementById('excel_file1'); // 파일 입력 필드
	var file = fileInput.files[0]; // 업로드할 파일
	formData.append('excel_file', file);
	var page = $("#page").val();
	if(page ==''){
		page=1;
	}
	formData.append('page', page);
	processing();

    // 서버에서 엑셀 파일을 다운로드 받습니다.
    $.ajax({
        url: contextPath+"/front/meta/uploadAjax.do",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function (e) {
        	$("#excel_file1").val('');
        	redraw(e);
    		draw_paging_by_ajax(e.paginator);
			endProcessing();
        },
        error: function (error) {
			endProcessing();
	       	$("#excel_file1").val('');
            console.error("다운로드 에러:", error);
        }
    });
	
}
function redraw(e){
	$('tr[id^="tr_li_"]').remove();
	var tableBody = '';
    $.each(e.todayList, function(i, row) {
        var tableRow = '<tr id="tr_li_"'+i+'" onclick="openRegMeta(' + row.id_meta_m + ',' + row.id_meta_row + ');">';
        $.each(e.aloneList, function(j, show_col) {
            var meta_cd = show_col.meta_cd;
            var data = row[meta_cd];
            tableRow += '<td>' + nanToStr(data) + '</td>';
        });
        tableRow += '</tr>';
        tableBody += tableRow;
    });
    var elements = $('tr[id^="tr_he_"]').toArray();

 // elements는 이제 페이지에 있는 모든 해당 tr 요소의 배열입니다.

 // 배열의 마지막 요소 (즉, 가장 하위 값)을 가져옵니다.
	var lastElement = elements[elements.length - 1];
    $('#'+lastElement.id).after(tableBody);
}
</script>
</front:FrontDynamicBody>
</html>