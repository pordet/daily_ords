<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<%@ attribute name="top_id_category" required="false" %>
<%@ attribute name="top_id_area" required="false" %>
<%@ attribute name="id_category" required="false" %>
<%@ attribute name="id_area" required="false" %>
<%@ attribute name="id_nat_area" required="false" %>
<%@ attribute name="id_city_area" required="false" %>
<%@ attribute name="data_type" required="false" %>
<%@ attribute name="top_style" required="false" %>

<commc:LeftBrowserTag type="${data_type}" top_id_category="${top_id_category}" top_id_area="${top_id_area}" id_category="${id_category}" id_nat_area="${id_nat_area}" id_city_area="${id_city_area}" locale="${currLocale}" listName="leftList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <aside class="mh-side-nav" style="${top_style}">
            <section class="mh-sn-bar">
                <!-- side bar -->
                <div class="mh-sn-menuwrap">
                    <nav class="mh-sn-menu"><!-- li 요소에 .mh-open 클래스를 넣으면 선택된 요소로 활용 -->
<!-- 							1단 -->
                        <ul class="mh-menu-list">
						<c:forEach var="item" items="${leftList}">
						<li class="mh-mlview ${((item.idCategory eq id_category and item.idNatArea eq id_nat_area)? 'mh-open':'' )}">
						    <c:if test="${not empty param.top_id_area}">
						    	<c:set var="top_area_param" value="&top_id_area=${param.top_id_area}"/>
						    </c:if>
						    <c:if test="${not empty param.top_id_category}">
						    	<c:set var="top_cate_param" value="&top_id_category=${param.top_id_category}"/>
						    </c:if>
							<a href="<c:url value='/data_list.do?type=${param.type}${top_area_param}${top_cate_param}&id_category=${item.idCategory}&id_nat_area=${item.idNatArea}'/>">${item.codeNm}</a>
							<c:if test="${fn:length(item.child)>0}">
<!-- 							2단 -->
							<ul class="mh-mlview-menu ${(item.idCategory eq id_category and item.idNatArea eq id_nat_area? 'mh-open':'' )}">
								<c:forEach var="child" items="${item.child}">
<%-- 								    <c:if test="${not empty param.id_area}"> --%>
<%-- 								    	<c:set var="area_param" value="&top_id_area=${param.top_id_area}"/> --%>
<%-- 								    </c:if> --%>
<%-- 								    <c:if test="${not empty param.id_category}"> --%>
<%-- 								    	<c:set var="cate_param" value="&top_id_category=${param.top_id_category}"/> --%>
<%-- 								    </c:if> --%>
									
								<li <c:if test="${child.idCategory eq id_category and child.idNatArea eq id_nat_area and child.idArea eq id_city_area}">class="mh-open"</c:if>>
								<a href="<c:url value='/data_list.do?type=${param.type}${top_area_param}${top_cate_param}&id_category=${child.idCategory}&id_nat_area=${child.idNatArea}&id_city_area=${child.idArea}'/>">${child.codeNm}</a>
									<c:if test="${fn:length(child.child)>0}">
									<ul class="mh-mlview-menu-child" ${(child.idCategory eq id_category and child.idNatArea eq id_nat_area? 'mh-open':'' )} ${(child.idCategory eq id_category and child.idNatArea eq id_nat_area? ' style=\"display:block;\"':'' )}>
										<c:forEach var="granChild" items="${child.child}">
										<li <c:if test="${item.idCategory eq id_category and item.idNatArea eq id_nat_area and item.idCityArea eq id_city_area}">class="mh-open"</c:if>><a href="<c:url value='/data_list.do?type=${param.type}${top_area_param}${top_cate_param}&id_category=${child.idCategory}&id_nat_area=${child.idNatArea}&id_city_area=${granChild.idArea}'/>">${granChild.codeNm}</a>
										</li>
										</c:forEach>
									</ul>
									</c:if>
								</li>
								</c:forEach>
							</ul>
							</c:if>
						</li>
						</c:forEach>
					</ul>
                    </nav>
				</div>                    
            </section>
        </aside>
<script>
// 통합검색 코드 삽입부
</script>