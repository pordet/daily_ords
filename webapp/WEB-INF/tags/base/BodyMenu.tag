<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<c:set var="msg" value="request" />
<commc:MenuTag user_seq="${id_user}" locale="${currLocale}" listName="menuList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
        <aside>
            <section class="la-sn-bar">
                <!-- side bar -->
                <div class="his-tree">
<!-- 							1단 -->
						<c:forEach var="item" items="${menuList}">
						<div class="facet-inactive block-facet--links block block-facets block-facet-blockcoll_${item.idMenu}" id="block-content${item.idMenu}">
<%-- 								<a data-toggle="collapse" href="#coll_${item.ordStr}" class="facet-toggle" aria-expanded="false" aria-controls="collapseExample"> --%>
								<a href="javascript:void(0);" class="facet-toggle" onclick="javascript:showDiv(this,'${item.idMenu}');">
									<span class="facet-item_parent" id="p_span_${item.idMenu}">${item.menu_nm}</span> 
								<c:if test="${not empty item.child_cnt}">
									<i class="fa fa-angle-down fa-pull-right fa_pos" id="angle_${item.idMenu}"></i>
								</c:if>
								</a>
								<c:if test="${not empty item.child_cnt}">
									<div class="collapse in facets-widget-links" id="coll_${item.idMenu}">
										<ul class="facet-inactive js-facets-links item-list__links js-facets-widget" id="ul_${item.idMenu}">
										<c:forEach var="subMenu" items="${item.child}" varStatus="i">
										    <li class="facet-item" id="treeli_${subMenu.idMenu}">
												<a href="<c:url value='${subMenu.menu_url}'/>?id_menu=${subMenu.idMenu}" id="sel_icon_${subMenu.idMenu}">
													<span class="facet-item_child" id="p_span_${subMenu.idMenu}">${subMenu.menu_nm}</span> 
												</a>
											</li>
										</c:forEach>
										</ul>
									</div>
								</c:if>							
						</div>
						</c:forEach>
                    </div>
	                <div class="footer-content">
	                    <span class="copyright-img">
	<%--                     <img src="<c:url value='/resource/img/icon_gray.png'/>" alt=""/> --%>
	                    <em>THE KOREA TRANSPORT INSTITUTE</em></span>
	                    <span class="copyright-footer">Copyright &copy; 2019 2018. <br>THE KOREA TRANSPORT INSTITUTE<br>All Rights Reserved.</span>
	                </div>
            </section>
        </aside>

        <!-- //레프트네비 -->
<script>
	$(function () {
	   $('.js-action-a').click(function (e) {
			e.preventDefault();
		});
	});
</script>