function register(){
	if($("#title").val()==''){
		alertOpen('제목은 필수입니다.');
		return;
	}
	if($("#comment").val()==''){
		alertOpen('내용은 필수입니다.');
		return;
	}
	if($("#keyword").val()==''){
		alertOpen('키워드는 필수입니다.');
		return;
	}
	if($("#title").val().length >500){
		alertOpen('제목은 250자까지 입니다.');
		return;
	}
	if($("#comment").val().length >5000){
		alertOpen('내용은 2500자까지 입니다.');
		return;
	}
	if($("#keyword").val().length >500){
		alertOpen('키워드는 250자까지 입니다.');
		return;
	}
	$("#page").val($("#page").val());
	var formData = new FormData($('#frm')[0]);
	
	var deleteFile = [];
	for(var i=0; i<newList.length; i++) {
		var item = newList[i];
		if(item.mode == 'delete') deleteFile.push(item.id_board_file);
	}
	formData.append("deleteItem", JSON.stringify(deleteFile));
	
	var regMode = 'modify';
	if(nanToStr($("#id_board").val())=='') {
		regMode = 'insert';
	}
	formData.append("regMode", regMode);
	var url = contextPath+"/data/registerDataBoardFileAjax.do";
	
	/*
	var url = '';
	var isInsert = false;
	if(nanToStr($("#id_board").val())==''){
		isInsert = true;
		url += contextPath+"/data/insertDataBoardFileAjax.do";
	}else{
		url += contextPath+"/data/modifyDataBoardFileAjax.do";
	}
	*/
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    enctype: 'multipart/form-data',
	    processData:false,
	    contentType:false,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
				console.log("저장결과");
				console.log(e)
				
				var resultData = e.map;
				var id_board_master = resultData.id_board_master;
				var id_board = resultData.id_board;
				$("#id_board_master").val(id_board_master);
				$("#id_board").val(id_board);
				
				infoOpen("저장하였습니다.", "viewDataDetail");
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function delData(){
	var params = $("#frm").serialize();
	var url = contextPath+"/data/deleteDataBoardAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
		    	infoOpen('삭제하였습니다.', 'doList');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else if(e.status=='408'){
			    alertOpen('업로드 할 수 있는 파일 크기는 500M 입니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

/*
function register(){
	if($("#title").val()==''){
		alertOpen('제목은 필수입니다.');
		return;
	}
	if($("#comment").val()==''){
		alertOpen('내용은 필수입니다.');
		return;
	}
	if($("#keyword").val()==''){
		alertOpen('키워드는 필수입니다.');
		return;
	}
	if($("#title").val().length >500){
		alertOpen('제목은 250자까지 입니다.');
		return;
	}
	if($("#comment").val().length >5000){
		alertOpen('내용은 2500자까지 입니다.');
		return;
	}
	if($("#keyword").val().length >500){
		alertOpen('키워드는 250자까지 입니다.');
		return;
	}
	$("#page").val($("#page").val());
	var formData = new FormData($('#frm')[0]);
	var url = '';
	var isInsert = false;
	if(nanToStr($("#id_board").val())==''){
		isInsert = true;
		url += contextPath+"/data/insertDataBoardFileAjax.do";
	}else{
		url += contextPath+"/data/modifyDataBoardFileAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    enctype: 'multipart/form-data',
	    processData:false,
	    contentType:false,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		drawUserInfo(e,isInsert);
	    		loadData(e,isInsert);
	    		$("#title").focus();
				
				var resultData = e.map;
				var id_board_master = resultData.id_board_master;
				var id_board = resultData.id_board;
				$("#id_board_master").val(id_board_master);
				$("#id_board").val(id_board);
				
				infoOpen("저장하였습니다.", "viewDataDetail");
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function delData(){
	var params = $("#frm").serialize();
	var url = contextPath+"/data/deleteDataBoardAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$("#id_board").val('');
	    		$("#title").val('');
	    		$("#comment").val('');
	    		$("#keyword").val('');
	    		$("[id^=attach_]").remove();
	    		$("#btnDelete").hide();
	    		$("#board_reg_type").html(nanToStr(e.board_path)+" 등록");
//	    		addKeyword();
	    		$('#modal_body').scrollTop(0);
	    		$("#title").focus();
				
		    	infoOpen('삭제하였습니다.', 'doList');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else if(e.status=='408'){
			    alertOpen('업로드 할 수 있는 파일 크기는 500M 입니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

var attach_idx=0;
function delAttach(idx){
	attach_idx=idx;
	confirmOpen('삭제시 복구할 수 없습니다.',delAttachFile1);
}

function delAttachFile1(){
	alertOpen("aa");
}
function delAttachFile(){
	$("#id_board_file").val(attach_idx);
	var params = $("#frm").serialize();
	$.ajax({
		url: contextPath+"/data/deleteDataBoardFileAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret == '0'){
	     	   $("#attach_"+id_board_file).remove();
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}
*/
