<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="id_master" required="false" %>
<%@ attribute name="id_board" required="false" %>
<%@ attribute name="sel_ord_str" required="false" %>
<%@ attribute name="data_type" required="false" %>
<%@ attribute name="keyword" required="false" %>
<%@ attribute name="top_style" required="false" %>

<commc:LeftAccordionTag  id_master="${id_master}" keyword="${keyword}" listName="leftList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <aside class="col-2">
            <section class="la-sn-bar">
                <!-- side bar -->
                <div class="his-tree">
<!-- 							1단 -->
						<c:forEach var="item" items="${leftList}">
						<div class="facet-inactive block-facet--links block block-facets block-facet-blockcoll_${item.ordStr}" id="block-content${item.ordStr}">
<%-- 								<a data-toggle="collapse" href="#coll_${item.ordStr}" class="facet-toggle" aria-expanded="false" aria-controls="collapseExample"> --%>
								<a href="javascript:void(0);" class="facet-toggle" onclick="javascript:showDiv(this,'${item.ordStr}');">
									<span class="facet-item_parent" id="p_span_${item.ordStr}">${item.boardNm}</span> 
									<span id="span_${item.ordStr}" class="facet-item_count">${item.boardCnt}</span>
									<i class="fa fa-angle-down fa-pull-right fa_pos" id="angle_${item.ordStr}"></i>
								</a>
								<c:if test="${fn:length(item.child)>0}">
									<div class="collapse in facets-widget-links" id="coll_${item.ordStr}">
										<ul class="facet-inactive js-facets-links item-list__links js-facets-widget" id="ul_${item.ordStr}">
										<c:forEach var="child" items="${item.child}" varStatus="i">
										    <li class="facet-item" id="treeli_${child.lv}_${child.ordStr}" ${(i.count>3?'style="display:none;"':'')}>
												<a href="javascript:void(0);" onclick="goByTree(this,${child.lv},'${child.ordStr}')" id="sel_icon_${child.ordStr}">
													<span class="facet-item_child" id="p_span_${child.ordStr}">${child.boardNm}</span> 
													<span id="span_${child.ordStr}" class="facet-item_child">(${child.boardCnt})</span>
												</a>
											</li>
										</c:forEach>
										</ul>
										<c:set var="show_more_yn" value="${fn:length(item.child)>3}"/>
										<c:if test="${show_more_yn}">
										<a href="javascript:void(0);" onclick="javascript:showMore('ul_${item.ordStr}',this);" class="facets-soft-limit-link">더 보기</a>
										</c:if>
								
									</div>
								</c:if>							
						</div>
						</c:forEach>
                    </div>
            </section>
        </aside>
<script>
function showDiv(id,ord_str){
	var div = $("#coll_"+ord_str);
	div.children(".fa-angle-down")
	if(div.hasClass("in")){
		div.removeClass("in");
		$("#angle_"+ord_str).addClass("fa-rotate-270");
		$("#modi_sel_ord_str").val(ord_str);
		doList(1,true,false);	
	}else{
		$(div).addClass("in");
		$("#angle_"+ord_str).removeClass("fa-rotate-270");
		$("#modi_sel_ord_str").val(ord_str);
		doList(1,true,false);	
	}
//	$(".facet-item").removeClass("seled");
	selInit()
}
function selInit(){
	$("[id^=sel_icon_]").removeClass("seled");
	$("[id^=p_span_]").removeClass("seled_text");
	$("[id^=span_]").removeClass("seled_text");
}
function showMore(id,that){
	if($(that).hasClass("open")){
		$(that).removeClass("open");
		$(that).html('더 보기');
		$("#"+id).children("li").each(function(i){
			if(i>2){
				$(this).hide();
			}
		})
	}else{
		$(that).addClass("open");
		$("#"+id).children("li").show();
		$(that).html('숨기기');
	}
}
function goByTree(that,lv,sel_ord_str){
	var parent = $(that);
	if(parent.hasClass("seled")){
//		parent.removeClass("seled");
	}else{
		$("[id^=sel_icon_]").removeClass("seled");
// 		$("facet-item_child")
//		$('.facet-item_child').removeClass("seled_text");
		parent.addClass("seled");
//		parent.children().addClass("seled_text")
	}
	$("[id^=p_span_]").removeClass("seled_text");
	$("[id^=span_]").removeClass("seled_text");
//	if(!$("#p_span_"+sel_ord_str).hasClass("seled_text")){
		$("#p_span_"+sel_ord_str).addClass("seled_text");
		$("#span_"+sel_ord_str).addClass("seled_text");
//	}
	$("#modi_sel_ord_str").val(sel_ord_str);
//	search();
 	doList(1,true,false);	
}
function goByTreeByBtn(that,lv,sel_ord_str){
	var parent = $(that).parent();
	if(parent.hasClass("tree-open")){
		parent.removeClass("tree-open");
		parent.children(".comm-treeview-list").hide();
	}else{
		parent.addClass("tree-open");
		parent.children(".comm-treeview-list").show();
	}
//	alert($(that).attr("id"));
//	location.href=contextPath+'/history.do?id_master='+id_master+'&id_board_master='+id_board_master+'&sel_ord_str='+sel_ord_str;
}</script>