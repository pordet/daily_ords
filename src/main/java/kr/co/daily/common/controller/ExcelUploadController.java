package kr.co.daily.common.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import daily.common.component.ExcelReader;
import daily.common.util.FileUtil;
import daily.common.util.ReflectionUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;

@Transactional
@Controller
public class ExcelUploadController {

	@Autowired private ExcelReader excelReader;
    @Autowired private ApplicationContext applicationContext;
	@Autowired SessionLocaleResolver localeResolver;

    @RequestMapping("/test/excel.do")
	public void readExcel(CommandMap map, HttpServletRequest request) throws IOException, InvalidFormatException {
		Locale locale = localeResolver.resolveLocale(request);
    	List<MultipartFile> fileList = FileUtil.getMultipartFileListByRequest(request);
		String sKey = "excel."+map.getString("keyNm")+".serviceNm";
		String serviceNm = SpringPropertiesUtil.getProperty(sKey);
		String fKey = "excel."+map.getString("keyNm")+".funcNm";
		String funcNm = SpringPropertiesUtil.getProperty(fKey);
		Object bean = applicationContext.getBean(serviceNm);
		Method method = ReflectionUtil.findMethod(bean,funcNm);
		MultipartFile file = fileList.get(0);
		if(file ==null) return;
		List<CommandMap> excelList = excelReader.readFileToList(file , CommandMap::from);
		String key = "excel."+map.getString("keyNm")+".column";
		String head = SpringPropertiesUtil.getProperty(key,locale);
		String[] colArr = head.split(";");
		try {
			for(CommandMap param : excelList) {
				CommandMap dat = new CommandMap();
				for(int i=0;i<colArr.length;i++) {
					if(param.getString(String.valueOf(i))!=null) {
						dat.put(colArr[i], param.getString(String.valueOf(i)));
					}
				}
				method.invoke(bean, dat);
			}
		}catch(IllegalAccessException e){ 
			e.printStackTrace(); 
		}catch(InvocationTargetException e){ 
			e.getTargetException().printStackTrace(); //getTargetException }
		}
		
	}
    @RequestMapping("/test/excel1.do")
	public String readExcel1(MultipartFile file){
		return "test/mailTest";
	}
}
