<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: BodyHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <!-- header -->
        <div class="login-user-header">
            <div class="login-usr-hd-logo">
                <img src="<c:url value='/resource/img/logo-login.png'/>" alt="NPPA CRS">
                <div class="login-title-txt">
                    <strong>NPPA<span>CRS</span></strong>
                    <span class="login-tit-notice"><spring:message code="login.usr.login.title1"/> <br><em><spring:message code="login.usr.login.title2"/></em></span>
                </div>
            </div>
        </div>
        <!-- // header -->
