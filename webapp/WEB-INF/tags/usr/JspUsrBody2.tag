<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="usr" tagdir="/WEB-INF/tags/usr" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
    <div class="mh-wrapper">
        <usr:JspUsrHead/>
        <!--컨텐츠영역-->
        <jsp:doBody/>
        <!--//컨텐츠영역-->
        <!-- // mh-footer -->
        <usr:JspUsrTail/>
    </div>
    <c:if test="${hasPop}">
        <usr:JspUsrDialog/>
    </c:if>
    <!-- modal type02  알림타입 -->
    <div class="modal fade mh-modal" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body">
                    <p>We sent the confirmation number.<br>Please check your e-mail.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- //modal type02  -->
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap/js/bootstrap.min.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <!-- nppa JS -->
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
    <script src="<c:url value='/resource/js/comm_ords.js'/>"></script>
    
</body>

