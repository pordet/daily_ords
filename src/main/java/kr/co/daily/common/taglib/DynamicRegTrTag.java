package kr.co.daily.common.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DynamicRegTrTag extends TagSupport  {
    private Integer paramRowNum;
    private Integer paramColNum;
    private CustomMap row;
    private Integer currRowNum;
    private Integer currColNum;
    private CustomMap col;
    private List<CustomMap> childList;

    public void setParamRowNum(Integer paramRowNum) {
        this.paramRowNum = paramRowNum;
    }

    public void setParamColNum(Integer paramColNum) {
        this.paramColNum = paramColNum;
    }

	public void setRow(Map row) {
		this.row = (CustomMap)row;
	}

	public Integer getCurrRowNum() {
		return currRowNum;
	}

	public Integer getCurrColNum() {
		return currColNum;
	}
	public CustomMap getCol() {
		return col;
	}

	public List<CustomMap> getChildList() {
		return childList;
	}

	@Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();

            currRowNum = paramRowNum;
            currColNum = paramColNum;
            col = row;

            if (paramColNum == 1) {
            	if(GlobalConst.DT_STR.equals(row.getString("typ", "")) && row.getInt("len_type",0)>1) {
	                out.println("<tr>");
	                out.println("    <th>"+row.getString("meta_nm","")+"</th>");
	                out.println("    <td colspan=\"3\">");	                
            	}else if(GlobalConst.DT_GRP_HEAD.equals(row.getString("typ", ""))) {
            		if(row.getInt("meta_depth", 0)==1) {
            			if(currColNum==1) {
//        	                out.println("</tr>");
        	                out.println("<tr>");
        	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
        	                out.println("</tr>");
            			}else {
        	                out.println("<tr>");
        	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
        	                out.println("</tr>");
            			}
            		}else {
    	                out.println("<tr>");
    	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
    	                out.println("</tr>");
            		}
            		childList = (List<CustomMap>) row.get("child_list");
                    pageContext.setAttribute("childList", childList);
            	}else {
	                out.println("<tr>");
	                out.println("    <th>"+row.getString("meta_nm","")+"</th>");
	                out.println("    <td>");
            	}
            } else {
            	if(GlobalConst.DT_STR.equals(row.getString("typ", "")) && row.getInt("len_type",0)>1) {
	                out.println("<tr>");
	                out.println("    <th>"+row.getString("meta_nm","")+"</th>");
	                out.println("    <td colspan=\"3\">");	                
            	}else if(GlobalConst.DT_GRP_HEAD.equals(row.getString("typ", ""))) {
            		if(row.getInt("meta_depth", 0)==1) {
            			if(currColNum==1) {
        	                out.println("</tr>");
        	                out.println("<tr>");
        	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
            			}else {
        	                out.println("<tr>");
        	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
        	                out.println("</tr>");
            			}
            		}else {
    	                out.println("<tr>");
    	                out.println("    <th colspan=\"4\" class=\"depth"+(row.getInt("meta_depth", 0)+1)+"\">"+row.getString("meta_nm","")+"</th>");            			
    	                out.println("</tr>");
            		}
            		childList = (List<CustomMap>) row.get("child_list");
                    pageContext.setAttribute("childList", childList);
            	}else {
	                out.println("    <th>"+row.getString("meta_nm","")+"</th>");
	                out.println("    <td>");
            	}
            }
            pageContext.setAttribute("col", col);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return EVAL_BODY_INCLUDE;
    }
	public int doEndTag() throws JspException {
        // 출력 변수를 사용하여 원하는 작업 수행
        // 이 예제에서는 출력 변수를 화면에 출력
        try {
            JspWriter out = pageContext.getOut();
            if (paramColNum == 1) {
            	if(GlobalConst.DT_STR.equals(row.getString("typ", "")) && row.getInt("len_type",0)>1) {
	                out.println("</tr>");
	                currColNum = 1;
	                currRowNum = currRowNum+1;
            	}else if(GlobalConst.DT_GRP_HEAD.equals(row.getString("typ", ""))) {
                    currRowNum = currRowNum+1;
                    currColNum = 1;
            	}else {
	                out.println("    </td>");
	                currColNum = 2;
            	}
            } else {
            	if(GlobalConst.DT_STR.equals(row.getString("typ", "")) && row.getInt("len_type",0)>1) {
	                out.println("    </td>");
	                out.println("</tr>");
                    currRowNum = currRowNum+1;
	                currColNum = 1;
            	}else if(GlobalConst.DT_GRP_HEAD.equals(row.getString("typ", ""))) {
//                    pageContext.setAttribute("childList", childList);
                    currRowNum = currRowNum+1;
                    currColNum = 1;
            	}else {
	                out.println("    </td>");
	                out.println("</tr>");
                    currRowNum = currRowNum+1;
	                currColNum = 1;
            	}
            }

          pageContext.setAttribute("currRowNum", currRowNum);
          pageContext.setAttribute("currColNum", currColNum);
//            pageContext.getOut().write(currRowNum);
//            pageContext.getOut().write(currRowNum);
        } catch (Exception e) {
            throw new JspException(e);
        }

        // 결과로 EVAL_PAGE를 반환하여 페이지를 계속 진행
        return EVAL_PAGE;
    }
}

