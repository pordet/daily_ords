function openFilePicker(f_id) {
    document.getElementById('attach_file_'+f_id).click();
}

function displayFileName(f_id) {
    var fileInput = document.getElementById('attach_file_'+f_id);
    var fileNameDisplay = document.getElementById('fileName_'+f_id);
    
    if (fileInput.files && fileInput.files.length > 0) {
        fileNameDisplay.value = fileInput.files[0].name;
    } else {
        fileNameDisplay.value = "파일을 선택하세요.";
    }
}
var cur_file_num=1;
function addFile(f_id){
	var f_slt = f_id.split("_");
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
    var fileCnt = $("[id^=file_group_]").length;
	if(fileCnt==0){
		cur_file_num = 1;
		var t_html = getFileGroup(f_slt[0],cur_file_num);
		if($("#ul_group_"+f_slt[0]).length==0){
			$("#file_div_"+f_slt[0]).html(t_html);
		}else{
			$("#file_div_"+f_slt[0]).append(t_html);
		}
	}else{
		var lastElementId = $("[id^=file_group_]").last().attr("id");
    	var html = getFileGroup(f_slt[0],cur_file_num);
		$("#"+lastElementId).after(html);
	}
}
function getFileGroup(id_meta,cur_file_num){
    var html = 	'<div id="file_group_' + id_meta + '_' + cur_file_num + '">' +
			    '<div class="attach-btn1">' +
			    '<input type="text" id="fileName_' + id_meta + '_' + cur_file_num + '" data-add="' + cur_file_num + '" class="attach1" style="width:300px;" disabled>' +
			    '<button type="button" class="btn attach1" onclick="javascript:openFilePicker(\'' + id_meta + '_' + cur_file_num + '\');"><span class="btn_attach"></span>첨부파일</button>' +
			    '<input type="file" id="attach_file_' + id_meta + '_' + cur_file_num + '" name="attach_file_' + id_meta + '_' + cur_file_num + '" class="file_input_hidden" onchange="javascript: displayFileName(\'' + id_meta + '_' + cur_file_num + '\')"/>' +
			    '<a href="javascript:void(0);" onClick="javascript:delFile(\'' + id_meta + '_' + cur_file_num + '\');"><img src="'+contextPath+'/resource/img/ico_delete.png" class="file_delete" alt="delete"/></a>';
    if(cur_file_num ==1){
	   html+= '<button type="button" id="btn_add_file_'+id_meta+'" class="btn attach1" onClick="javascript:addFile(\'' + id_meta + '_' + cur_file_num + '\');"><span class="btn_attach"></span>파일 추가</button>';
    }
    html+='</div>' +
    '</div>';
	return html;		
}
var attach_idx=0;
function delFile(idx){
	var f_slt = idx.split("_");
    $("#file_group_"+idx).remove();
	var fileCnt = $("[id^=file_group_]").length;
	if(fileCnt==0){
		cur_file_num = 1;
		var t_html = getFileGroup(f_slt[0],cur_file_num)
		$("#"+"file_div_"+f_slt[0]).append(t_html);			
	}else{
		if ($("#btn_add_file_"+f_slt[0]).length == 0) {
			var html = '<button type="button" id="btn_add_file_'+f_slt[0]+'" class="btn attach1" onClick="javascript:addFile(\'' + f_slt[0] + '_' + f_slt[1] + '\');"><span class="btn_attach"></span>파일 추가</button>';
			$("[id^=file_group_]").first().children().first().append(html);
		}
	}
}
function delMeta(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/bbs/deleteBoardAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
 	    	}else if(e.ret==8){
     			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
     			return;
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}
function register(){
	var formData = new FormData($('#frm')[0]);
	var insert_yn = $("#insert_yn").val();
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/bbs/insertBoardAjax.do";
	}else{
		$("#sort_order").val($("#sort_order_dis").val());
		url = contextPath+"/adm/bbs/modifyBoardAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    enctype: 'multipart/form-data',
	    processData:false,
	    contentType:false,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		if(insert_yn =='N'){
					if(nanToStr(e.err_msg)!=''){
	    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
					}else{
	    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
					}
				}else{
					if(nanToStr(e.err_msg)!=''){
	    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
					}else{
	    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
					}
				}
	    	}else{
		    	$("#curr_title").html(e.bbs.bbs_nm+" 수정");
	    		if(insert_yn =='Y'){
		    		$("#insert_yn").val('N');
		    		$("#id_meta_row").val(e.id_meta_row);
		    		$.each(e.file_list,function(i,d){
		    			var ulHtml = '<li id="atch_li_'+d.id_meta+"_"+d.id_board_file+'">' +
								   '<div class="file_down_div">'+
								   '<a href="'+file_download_url+'?id_board_file='+d.id_board_file+'"><span class="in_padd">'+d.file_nm+'</span><img src="'+contextPath+'/resource/img/ico_download.png" class="file_delete" alt="delete"></a>'+
								   '<a href="javascript:delAttach(\''+d.id_board_file+'\',\''+d.id_meta+'\')"><img src="'+contextPath+'/resource/img/ico_delete.png" class="file_delete" alt="delete"></a>'+
								   '</div>'+
								   '</li>';
						var ulElement = $("#ul_group_"+d.id_meta);
						if($("#ul_group_"+d.id_meta).length>0){
							ulElement.append(ulHtml);
						}else{
							var newUl = '<ul id="ul_group_'+d.id_meta+'">'+ulHtml+'</ul>';
							$("#file_div_"+d.id_meta).append(newUl);
						}
		    		});
		    		$.each(e.attachColList,function(i,d){
					    var f_g_id = 'file_group_'+d.id_meta+'_';
						$("[id^="+f_g_id+"]").remove();
						addFile(d.id_meta+"_1");
		    		});
		    		$.each(e.insDataList,function(i,d){
		    			var html="";
		    			html+='<input type="hidden" name="i_id_meta_'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
		    			html+='<input type="hidden" name="i_id_data_'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
		    			if(d.typ==cal_str){
		    				$("#id_meta_"+d.id_meta).val(d.val);
		    			}
		    			$("#id_meta_"+d.id_meta).append(html);
		    		});
	    		}else{
		    		$.each(e.file_list,function(i,d){
		    			var ulHtml = '<li id="atch_li_'+d.id_meta+"_"+d.id_board_file+'">' +
								   '<div class="file_down_div">'+
								   '<a href="'+file_download_url+'?id_board_file='+d.id_board_file+'"><span class="in_padd">'+d.file_nm+'</span><img src="'+contextPath+'/resource/img/ico_download.png" class="file_delete" alt="delete"></a>'+
								   '<a href="javascript:delAttach(\''+d.id_board_file+'\',\''+d.id_meta+'\')"><img src="'+contextPath+'/resource/img/ico_delete.png" class="file_delete" alt="delete"></a>'+
								   '</div>'+
								   '</li>';
						var ulElement = $("#ul_group_"+d.id_meta);
						if($("#ul_group_"+d.id_meta).length>0){
							ulElement.append(ulHtml);
						}else{
							var newUl = '<ul id="ul_group_'+d.id_meta+'">';
							$("#file_div_"+d.id_meta).append(newUl).append(ulHtml);
						}
		    		});
		    		$.each(e.attachColList,function(i,d){
					    var f_g_id = 'file_group_'+d.id_meta+'_';
						$("[id^="+f_g_id+"]").remove();
						addFile(d.id_meta+"_1");
		    		});
		    		$.each(e.insList,function(i,d){
		    			var html="";
		    			$("#id_meta_"+d.id_meta).val(d.val);
		    			html+='<input type="hidden" name="i_id_meta_'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
		    			html+='<input type="hidden" name="i_id_data_"'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
		    			$("#id_meta_"+d.id_meta).append(html);
		    		});
		    		$.each(e.delList,function(i,d){
		    			$("#id_meta_"+d).val('');
		    			$("#i_id_meta_"+d).remove();
		    			$("#i_id_data_"+d).remove();
		    		});
		    		$.each(e.modList,function(i,d){
		    			var html="";
		    			if(d.typ ==dt_str && d.len_type >dlt_1){
			    			$("#id_meta_"+d.id_meta).val(d.val);
			    			$("#i_id_meta_"+d.id_meta).val(d.val);
			    			$("#i_id_data_"+d.id_meta).val(d.id_data);
		    			}else{
			    			$("#id_meta_"+d.id_meta).val(d.val);
			    			$("#i_id_meta_"+d.id_meta).val(d.val);
			    			$("#i_id_data_"+d.id_meta).val(d.id_data);
		    			}
		    		});
	    			
	    		}
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
