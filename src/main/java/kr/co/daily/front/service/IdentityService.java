package kr.co.daily.front.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.front.dao.IdentityDAO;

@Service
public class IdentityService {
	@Autowired private IdentityDAO dao;

	public List<CustomMap> selectBidIdentity(CommandMap map) {
		return dao.selectBidIdentity(map.getMap());
	}

	public PageList<CustomMap> selectBidIdentityListContents(CommandMap map, CommandMap spMap) {
		return dao.selectBidIdentityListContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}

	public List<CustomMap> selectBidNotiColForIdentity(CommandMap map) {
		return dao.selectBidNotiColForIdentity(map.getMap());
	}

	public CustomMap getIdentityMap(CommandMap map) {
		return dao.getIdentityMap(map.getMap());
	}

	public CustomMap getIdentityRel(CommandMap map) {
		return dao.getIdentityRel(map.getMap());
	}

	public List<CustomMap> selectBidNotiColByKeyList(CommandMap map) {
		return dao.selectBidNotiColByKeyList(map.getMap());
	}	

}
