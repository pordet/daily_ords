package kr.co.daily.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.CodeManageDAO;

@Service
public class CodeManageService {
	@Autowired CodeManageDAO dao;

	public PageList<CustomMap> selectCodeList(CommandMap map) {
		return dao.selectCodeList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public void insertCode(CommandMap map) {
		dao.insertCode(map.getMap());
	}

	public void insertCodeGroup(CommandMap map) {
		dao.insertCodeGroup(map.getMap());
	}

	public List<CustomMap> selectCode(CommandMap map) {
		return dao.selectCode(map.getMap());
	}

	public PageList<CustomMap> selectCodeGroupList(CommandMap map) {
		return dao.selectCodeGroupList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public CustomMap getCode(CommandMap map) {
		return dao.getCode(map.getMap());
	}

	public CustomMap getCodeGroup(CommandMap map) {
		return dao.getCodeGroup(map.getMap());
	}

	public void deleteCodeGroup(CommandMap map) {
		dao.deleteCodeByCodeGroup(map.getMap());
		dao.deleteCodeGroup(map.getMap());
	}

	public void updateCodeGroup(CommandMap map) {
		dao.updateCodeGroup(map.getMap());
	}

	public void deleteCode(CommandMap map) {
		dao.deleteCode(map.getMap());
	}

	public void updateCode(CommandMap map) {
		dao.updateCode(map.getMap());
	}

	public List<CustomMap> selectCodeGroup(CommandMap map) {
		return dao.selectCodeGroup(map.getMap());
	}

	public List<CustomMap> selectCodeComboByLocale(CommandMap map) {
		return dao.selectCodeComboByLocale(map.getMap());
	}

	public CustomMap getLocaleCode(CommandMap map) {
		return dao.getLocaleCode(map.getMap());
	}

	public CustomMap getCodeGroupByCodeGroup(CommandMap map) {
		return dao.getCodeGroupByCodeGroup(map.getMap());
	}

	public CustomMap getCodeByCode(CommandMap map) {
		return dao.getCodeByCode(map.getMap());
	}

	public CustomMap selectCodeByCodeName(CommandMap map) {
		return dao.selectCodeByCodeName(map.getMap());
	}

	public CustomMap getCodeByCodeId(CommandMap map) {
		return dao.getCodeByCodeId(map.getMap());
	}

	public CustomMap getMenuByMenuName(CommandMap map) {
		return dao.getMenuByMenuName(map.getMap());
	}

	public List<CustomMap> selectUnitTypeList(CommandMap map) {
		return dao.selectUnitTypeList(map.getMap());
	}

	public List<CustomMap> selectUsingCodeGroup(CommandMap  map) {
		return dao.selectUsingCodeGroup(map.getMap());
	}

}
