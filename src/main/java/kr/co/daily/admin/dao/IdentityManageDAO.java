package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface IdentityManageDAO {

	List<CustomMap> selectIdentityList(Map<String, Object> map);

	List<CustomMap> selectIdentityDetailParamList(Map<String, Object> map);

	CustomMap getIdentityM(Map<String, Object> map);

	List<CustomMap> selectIdentityRelList(Map<String, Object> map);

	PageList<CustomMap> selectIdentityMList(Map<String, Object> map, PageBounds pageBounds);

	PageList<CustomMap> selectIdentityList(Map<String, Object> map, PageBounds pageBounds);

	void insertIdentityM(Map<String, Object> map);

	void modifyIdentityM(Map<String, Object> map);

	List<CustomMap> selectSearchParamList(Map<String, Object> map);

	List<CustomMap> selectCdListForIdentity(Map<String, Object> map);

	List<CustomMap> selectIdentityListForHead(Map<String, Object> map);

	List<CustomMap> selectIdentityCdList(Map<String, Object> map);

	List<CustomMap> selectIdentity(Map<String, Object> map);

	List<CustomMap> selectIdentityByIdMetaM(Map<String, Object> map);

	CustomMap getIdentityByIdMetaMAndIdMeta(Map<String, Object> map);

	List<CustomMap> selectChildMetaListByIdParentMeta(Map<String, Object> map);

	int getNextOrdAtIdentityByIdMetaMAndMetaDepth(Map<String, Object> map);

	void insertIdentity(Map<String, Object> map);

	void modifyIdentity(Map<String, Object> map);

	List<String> selectIdentityListByInIdMeta(Map<String, Object> map);

	void deleteIdentityByList(Map<String, Object> map);

	List<CustomMap> selectIdentityCdListByIdMetaM(Map<String, Object> map);

	void deleteIdentityByIdMetaMList(Map<String, Object> map);

	void deleteIdentityMByIdMetaMList(Map<String, Object> map);

	PageList<CustomMap> selectIentityMetaList(Map<String, Object> map);

	PageList<CustomMap> selectTreeIdentityMetaDataContents(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectIdentityMetaDataForSearch(List<Integer> lowKey);

	CustomMap getIdentityMetaTabCd(Map<String, Object> map);

	CustomMap getIdentityMetaExcel(Map<String, Object> map);

}
