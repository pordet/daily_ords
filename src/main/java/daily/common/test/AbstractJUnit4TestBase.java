package daily.common.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import daily.common.base.AbstractCommonBase;

public class AbstractJUnit4TestBase extends AbstractCommonBase{
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Rule public TestName testName = new TestName();
	
	@Before
	public void beforenitTest() {
		logger.debug("===========================================================[before] " + testName.getMethodName());
	}

	@After
	public void afterUnitTest() {
		logger.debug("===========================================================[after] " + testName.getMethodName());
	}
}
