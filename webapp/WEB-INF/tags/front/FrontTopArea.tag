<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<%@ attribute name="top_id_category" required="false" %>
<%@ attribute name="top_id_area" required="false" %>
<%@ attribute name="id_category" required="false" %>
<%@ attribute name="id_area" required="false" %>
<commc:CategoryTag top_id_category="${top_id_category}" locale="${currLocale}" listName="categoryList"/>
<commc:NationTag top_id_area="${top_id_area}" locale="${currLocale}" listName="areaList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
		<div class="top-area-data pb-15p">
		  <div class="container text-white">
		    <div class="row mt-15p">
		      <div class="col-md-5">
		        <div class="p-15 bg-primary">
		          <div class="mb-15">Browse by</div>
		          <div class="row">
		            <div class="col-md-6 mb-15">
				      <div class="view-content">
					      <div class="dropdown">
						  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						      <c:forEach var="nat" items="${categoryList}">
						        <c:if test="${param.top_id_category eq nat.code}">
						      		<c:set var="cateNm" value="${nat.codeNm}"/>
						        </c:if>
						      </c:forEach>
						      ${(empty cateNm ?'분야별':cateNm)}
						  </button>
						  <ul class="dropdown-menu topics">
						      <c:forEach var="nat" items="${categoryList}">
						      <li class="col-sm-6">
						      <a href="<c:url value='/data_list.do?type=1&top_id_category=${nat.code}'/>">${nat.codeNm}</a>
						      </li>
						      </c:forEach>

						  </ul>
						</div>
			    	</div>
		            </div>
		           <div class="col-md-6 mb-15">
				      <div class="view-content">
		<!-- 		      <div class="view-content"> -->
					      <div class="dropdown">
						  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						      <c:forEach var="nat" items="${areaList}">
						        <c:if test="${param.top_id_area eq nat.idArea}">
						      		<c:set var="natNm" value="${nat.areaNm}"/>
						        </c:if>
						      </c:forEach>
						      ${(empty natNm ?'국가별':natNm)}
						  </button>
						  <ul class="dropdown-menu topics">
						      <c:forEach var="nat" items="${areaList}">
						      <li class="col-sm-5">
						      <a href="<c:url value='/data_list.do?type=2&top_id_area=${nat.idArea}'/>">${nat.areaNm}</a>
						      </li>
						      </c:forEach>
						  </ul>
						</div>
		    		</div>
		           </div>
		  
		          </div>
		          </div>
		        </div>

		
		      </div>
		   </div>
      </div>
<script>
// 통합검색 코드 삽입부
</script>