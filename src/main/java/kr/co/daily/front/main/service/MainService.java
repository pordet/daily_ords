package kr.co.daily.front.main.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.front.dao.BbsDAO;
import kr.co.daily.front.main.dao.MainDAO;

@Service
public class MainService {

//	private Logger log = LoggerFactory.getLogger(MainService.class);
	
	@Autowired MainDAO dao;
	@Autowired private BbsDAO bbsDao;

	public List<CustomMap> selectMainBoardList(CommandMap map) {
		try {
			List<CustomMap> rowList = dao.selectMainBoardRow(map.getMap());
			List<Integer> keyList = new ArrayList<>();
			for(CustomMap row:rowList) {
				int id_row = row.getInt("id_meta_row",0);
				if(id_row>0) {
					keyList.add(id_row);
				}
			}
			if(keyList.size()>0) {
				map.put("key_list", keyList);
				List<CustomMap> rowListCol = dao.selectBoardView(map.getMap());
				for(CustomMap cm : rowList) {
					int key = cm.getInt("id_meta_row",0);
					for(CustomMap rm:rowListCol) {
						if(rm.getInt("id_meta_row",0)==key) {
							if(rm.getInt("id_data",0)==3735) {
								System.out.println("debug rawList id_data:"+rm.getInt("id_data",0));
							}
							System.out.println("rawList id_data:"+rm.getInt("id_data",0));
							if(!"".equals(rm.getString("val",""))) {
								rm = MetaUtil.transNumberingByCustomMap(rm);
							}
							cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
						}
					}
				}
				
			}
			return rowList;
		}catch(Exception e) {
			return null;
		}
	}
}
