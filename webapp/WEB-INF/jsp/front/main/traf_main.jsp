<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<front:FrontHead title=""/>
<front:FrontMainBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
                    <!-- 컨텐츠영역 : 여기에 작성 -->
                <!--
             	<div class="cont_sec01">
             	<div class="row" style="justify-content:center;">
             	<c:forEach var="cate" items="${cateList}" varStatus="i">
             	    <div>
		            <a href="<c:url value='/content.do?id_category=${cate.id_category}'/>">
		            <img src="<c:url value='/resource/img/cate/${cate.id_category}.jpg'/>" class="cate_${cate.id_category}">
		            </a>
		            </div>
             	</c:forEach>
				</div>
             	<div class="row" style="justify-content:center;">
             	<c:forEach var="cate" items="${cateList}">
					<div class="main_icon">
						<a href="<c:url value='/content.do?id_category=${cate.id_category}'/>"><span>${cate.code_nm}</span></a>
					</div>
             	</c:forEach>
				</div>
             	</div>
             	-->
	<div class="front-main">
		<table class="cate-list">
			<colgroup>
				<c:set var="length" value="${fn:length(cateList) }"/>
				<c:forEach var="cate" items="${cateList }" varStatus="i">
					<col width="${(100 / length)}${'%'}">
				</c:forEach>
			</colgroup>
			<tbody>
				<tr>
					<c:forEach var="cate" items="${cateList }" varStatus="i">
						<td>
							<a href="<c:url value='/content.do?id_category=${cate.id_category}'/>">
								<img src="<c:url value='/resource/img/cate/${cate.id_category}.jpg'/>" class="cate_${cate.id_category}">
							</a>
						</td>
					</c:forEach>
				</tr>
				<tr>
					<c:forEach var="cate" items="${cateList }" varStatus="i">
						<td>
							<a href="<c:url value='/content.do?id_category=${cate.id_category}'/>">
								<span>${cate.code_nm}</span>
							</a>
						</td>
					</c:forEach>					
				</tr>
			</tbody>
		</table>
	</div>
</front:FrontMainBody>