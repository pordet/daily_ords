package kr.co.daily.batch.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface BatchDAO {

//	void insertBatchM(Map<String, Object> map);

	void insertDailyBatchM(Map<String, Object> map);

	void insertDailyBatchRet(Map<String, Object> map);

	CustomMap getLastDailyBatchMBySearchDate(Map<String, Object> map);

	int getDailyBatNumByIdBatch(Map<String, Object> map);

	void updateDailyBatNumByIdBatch(Map<String, Object> map);

	int getBatchTotPageAtDailyBatchMByIdBatch(Map<String, String> batInfoMap);

	int getSuccCountAtDailyBatchRetByIdBatch(Map<String, String> batInfoMap);

	void updateSuccYnAtBatchM(Map<String, String> batInfoMap);

	CustomMap getSuccYnAtDailyBatchMBySearchDate(Map<String, String> batInfoMap);

	CustomMap getTotCntAtDay(Map<String, Object> map);

}
