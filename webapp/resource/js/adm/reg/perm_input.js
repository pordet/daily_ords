function deleteList(){
	if($("input:checkbox[name=del_code]:checked").length==0){
		alertOpen('삭제할 항목을 선택하십시오.');
	}else{
		confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
	}
}
function deleteConfirm(){
	processing();
	if($("#confirm_yn").val()=='Y'){
	    $("#confirm_yn").val('N');
	    $("#list_search_col").val($("#search_col").val());
	    $("#list_search_keyword").val($("#search_keyword").val());
		var frm =document.listFrm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/auth/deletePermissionList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
	}
}
