$(function () {
	$("#btnDelete").click(function(){
		confirmOpen('삭제하시겠습니까?',"confirmDeleteData");
	});
	$("#btnInit").click(function(){
		initForm();
	});
});
function initForm(){
	$("#cur_title").html('화면 마스터 등록');
	$("#id_meta_row").val('');
	$("#insert_yn").val('Y');
	initInputs();
}

function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/front/delIdentityMetaDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
// 	    	}else if(e.ret==8){
//     			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
//     			return;
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}


