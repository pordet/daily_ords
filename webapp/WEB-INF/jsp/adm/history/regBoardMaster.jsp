<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/board_master_register.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
<!-- 
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;">
	                 <input type="hidden" name="id_board_master" id="id_board_master" value="${param.id_board_master}">
	                 <input type="hidden" name="id_parent_board" id="id_parent_board" value="${param.id_parent_board}">
	                 <input type="hidden" name="parent_ord_str" id="parent_ord_str" value="${param.ord_str}">
	                 <input type="hidden" name="ord_str" id="ord_str" value="${param.ord_str}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
                     <div class="row">
                         <div class="col-12">
                         <div class="tit_cont">자료유형</div>
                           <div class="form-group">
								<select class="form-control mh-form-control" title="자료유형" name="id_master" id="id_master">
				                	<c:forEach var="data" items="${dataList}" varStatus="i">
								<option value="${data.id_master}" ${(param.id_master eq data.id_master?'selected':'')}>${data.title}</option>
									</c:forEach>
								</select>
                           </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-12">
                         <div class="tit_cont">상위 자료실</div>
                           <div class="form-group">
                             <input type="text" class="form-control mh-form-control" value="${(not empty param.id_parent_board ? parent.board_nm : map.parent_board_nm)}" disabled>
                           </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-12">
                         <div class="tit_cont">자료실명</div>
                           <div class="form-group">
                             <input type="text" class="form-control mh-form-control" name="board_nm" id="board_nm" value="${map.board_nm}">
                           </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-12">
                         <div class="tit_cont">자료실 설명</div>
                           <div class="form-group">
                             <input type="text" class="form-control mh-form-control" name="comment" id="comment" value="${map.comment}">
                           </div>
                         </div>
                     </div>
                     <div class="row">
						<div class="col-8">
							<div class="tit_cont">사용여부</div>
							<div class="inline-group">
								<div class="mh-radio-wrapper">
									<input type="radio" class="mh-radio-tab" name="use_yn" value="Y" id="ra1" ${((empty map.use_yn || map.use_yn eq 'Y')?'checked':'')} />
									<label for="ra1">예</label>
									<input type="radio" class="mh-radio-tab" name="use_yn" value="N" id="ra2" ${(map.use_yn eq 'N'?'checked':'')} />
									<label for="ra2">아니요</label>
								</div>
							</div>
						</div>
					<div class="col-4">
					</div>
                 </div>
			</form>
            <div class="btn-area mb-0p">
                <button type="button" class="btn" id="btnDelete" onclick="delData();" style="display:${(not empty map.id_board_master ?'block':'none')};">삭제</button>
                <button type="button" class="btn" id="btnSave" onclick="register();">저장</button>
                <button type="button" class="btn cancel" id="btnList" onclick="doList();">리스트</button>
            </div>	
-->
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>자료실 등록/수정</span>
			</div>
			
			<div class="page-content-wrap">
				
				<form id="listFrm" name="listFrm" method="post">
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" value="${param.page}">
					<input type="hidden" name="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" value="${param.sord}"/>
				</form>
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="id_board_master" id="id_board_master" value="${param.id_board_master}">
					<input type="hidden" name="id_parent_board" id="id_parent_board" value="${param.id_parent_board}">
					<input type="hidden" name="parent_ord_str" id="parent_ord_str" value="${param.ord_str}">
					<input type="hidden" name="ord_str" id="ord_str" value="${param.ord_str}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>자료유형</td>
									<td>
										<select title="자료유형" name="id_master" id="id_master">
											<c:forEach var="data" items="${dataList}" varStatus="i">
												<option value="${data.id_master}" ${(param.id_master eq data.id_master?'selected':'')}>${data.title}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>상위자료실</td>
									<td>
										<input type="text" value="${(not empty param.id_parent_board ? parent.board_nm : map.parent_board_nm)}" disabled>
									</td>
								</tr>
								<tr>
									<td>자료실명</td>
									<td>
										<input type="text" name="board_nm" id="board_nm" value="${map.board_nm}">
									</td>
								</tr>
								<tr>
									<td>설명</td>
									<td>
										<input type="text" class="form-control mh-form-control" name="comment" id="comment" value="${map.comment}">
									</td>
								</tr>
								<tr>
									<td>첨부여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="attach_yn" value="Y" id="at1" ${((empty map.attach_yn || map.attach_yn eq 'Y')?'checked':'')} />
												<label for="at1">예</label>
											</div>
											<div>
												<input type="radio" name="attach_yn" value="N" id="at2" ${(map.attach_yn eq 'N'?'checked':'')} />
												<label for="at2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr id="tr_multi_yn">
									<td>멀티파일 첨부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="multi_attach_yn" value="Y" id="ma1" ${((empty map.multi_attach_yn || map.multi_attach_yn eq 'Y')?'checked':'')} />
												<label for="ma1">예</label>
											</div>
											<div>
												<input type="radio" name="multi_attach_yn" value="N" id="ma2" ${(map.multi_attach_yn eq 'N'?'checked':'')} />
												<label for="ma2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="ra1" ${((empty map.use_yn || map.use_yn eq 'Y')?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="ra2" ${(map.use_yn eq 'N'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" id="btnList" onclick="doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<c:if test="${(not empty map.id_board_master ?'block':'none')}">
								<button type="button" class="btn-warning" id="btnDelete" onclick="delData();">삭제</button>
							</c:if>
							<button type="button" class="btn-primary" id="btnSave" onclick="register();">저장</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
/*
function registerData(){
	$("#dataModal").modal('show');	
}
*/
function doList(){
	processing();
	var frm =document.listFrm;
		frm.action=contextPath+"/manageHistory.do";
//   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
	processing();
	frm.submit();
}
function search(){
	doList(1);
}
</script>
</html>