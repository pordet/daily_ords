package kr.co.daily.front.controller;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.util.StrUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.batch.service.BatchJobService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BidSearchService;
@Controller
public class BidNotiController extends AbstractControllerBase {
	@Autowired private BidSearchService service;
	@Autowired private BatchJobService jobService;
	@RequestMapping("/front/bid/bidNoti.do")
	public String bidNoti(CommandMap commandMap, Model model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		commandMap.put("search_keyword","AA");
		model.addAttribute("paramMap", commandMap.getMap());
		return "front/bid/bidNoti";
	}
	
	@RequestMapping("/front/bid/bidNotiSearchAjax.do")
	public String bidNotiSearchAjax(CommandMap commandMap, ModelMap model) throws Exception {

		try {
			String srt_dt = StringDateUtil.getNewFormatByFormat(commandMap.getString("search_srt_dt",""), "yyyy-MM-dd", "yyyyMMdd");
			String end_dt = StringDateUtil.getNewFormatByFormat(commandMap.getString("search_end_dt",""), "yyyy-MM-dd", "yyyyMMdd");
			jobService.manualCallOpenApi(GlobalConst.META_M_TYPE_BID,srt_dt, end_dt,"yyyyMMdd");
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return "jsonView";
	}
	@RequestMapping("/front/bid/bidNotiAjax.do")
	public String redrawContentAjax(CommandMap commandMap, ModelMap model) throws Exception {
		JSONParser parser = new JSONParser();

		try {
			FileReader reader = new FileReader("d:/현작업/jsonTest/pblanc.json");
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			
			reader.close();
			Map resMap = (Map)jsonObject.get("response");
			Map resHeadMap = (Map)resMap.get("header");
			Map resBodyMap = (Map)resMap.get("body");
			try {
				Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
				int totPage = totCnt / 10;
				int restRow = totCnt % 10;
				if(restRow!=0){
					totPage++;
				}
//				for(int i=0;i<totPage;i++) {
//					System.out.println("페이지 번호:"+(i+1));
//				}
				System.out.println("총 페이지:"+totPage);

				JSONArray arr = (JSONArray)resBodyMap.get("items");
				List<String> keywordList = new ArrayList<String>();
				List<CustomMap> kList = service.selectBidSearchKeywordList();
				for(CustomMap k:kList) {
					keywordList.add(k.getString("code_nm", ""));
				}
				keywordList.add("구입");
				keywordList.add("계약");
				commandMap.put("service_cd", GlobalConst.META_M_TYPE_BID);
//				List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
				//(TODO)테스트용 전체 컬럼 매칭 검색
				List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
				Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
				for(CustomMap m:mList) {
					String id_bid = m.getString("id_bid","");
					if(!"".equals(id_bid)) {
						CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
						String bid_col_cd = mCmap.getString("bid_col_cd","");
						if(!"".equals(bid_col_cd)) {
							bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
							System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
						}
					}else {
						System.out.println("나오면 안되는 라인");
					}
				}
				List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
				//(TODO)패치에서 넣어주어야 하는 코드
				CommandMap searchCon = new CommandMap();
				searchCon.put("search_date", "20230501");
				searchCon.put("rslt_pg_no", 1);
				for(Object ino : arr) {
					if(ino instanceof JSONObject) {
						JSONObject json = (JSONObject)ino;
						if(json.get("bsnsDivNm").toString().trim().equals("물품")) {
							//   (json에 담긴 값을 체크하기 위한 코드 시작
							System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
							
							String bidNtceNm = json.get("bidNtceNm").toString().trim();
							System.out.println("bidNtceNm:"+bidNtceNm);
							//   )
							
							//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
							List<CommandMap> inputMap = new ArrayList<CommandMap>();
							if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
								String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
								System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
								for(Object key:json.keySet()) {
									if(key instanceof String) {
										
										//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
										System.out.println("(=================================================");
										System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
										//(TODO)디버깅 용 코드
										if(key.equals("opengTm")) {
											System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
										}
										CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
										//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
										if(keyMap!=null) {
											System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
											CommandMap val = new CommandMap();
											//참고용 값
											val.put("bid_col_cd", key);
											val.put("id_bid",keyMap.getString("id_bid"));
											val.put("conts", json.get(key));
											inputMap.add(val);
										}else {
											System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
										}
										//   )
										System.out.println("=================================================)");
									}
								}
								if(inputMap.size()>0) {
									inputList.add(inputMap);
								}
							}
						}
					}else {
						System.out.print("ino Class명:"+ino.getClass());
					}
				}
				if(inputList.size()>0) {
//					service.insertBidNoti(inputList,searchCon);
				}else {
					System.out.print("inputList size = 0");
				}

			}catch(Exception e) {
				System.out.print("오류발생:"+e.getClass().getName());
				e.printStackTrace();
			}
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return "jsonView";
	}
	
	@RequestMapping(value = {"/front/bid/bidNotiList.do","/front/guest/bidNotiList.do"})
	public String bidNotiList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		commandMap.put("id_service", commandMap.get("sys_code"));
		CommandMap headerParam = new CommandMap();
		headerParam.put("id_service", commandMap.get("sys_code"));
		CustomMap idenMap = service.getIdentyMap(commandMap);
		model.addAttribute("idenMap", idenMap);
		List<CustomMap> headerList = service.selectBidNotiMForUsing(headerParam);
//		CommandMap specialMap = new CommandMap();
		if(!"".equals(commandMap.getString("sidx", ""))) {
			commandMap.put("order_yn", "Y");
		}
		PageList<CustomMap> todayList = null;
		try {
			todayList = service.selectBidNotiListContents(commandMap,null);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
		//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
		List<Integer> rawKey = new ArrayList<Integer>();
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_bid_noti",0);
			if(key!=0) {
				rawKey.add(key);
				valMap.put(key, cm);
			}
		}
		if(rawKey.size()>0) {
			List<CustomMap> rawList = service.selectBidNotiColForSearch(rawKey);
			for(CustomMap cm : todayList) {
				int key = cm.getInt("id_bid_noti",0);
				for(CustomMap rm:rawList) {
					if(rm.getInt("id_bid_noti",0)==key) {
						cm.putByString(rm.getString("bid_col_cd", ""), rm.getString("conts",""));
					}
				}
			}
		}
		putModelListPage(todayList, model);
		model.addAttribute("headerList", headerList);
		model.addAttribute("paramMap", commandMap.getMap());
		return "front/bid/bidNotiList";
	}
	@RequestMapping(value = {"/front/bid/bidNotiOriginList.do"})
	public String bidNotiOriginList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		commandMap.put("id_service", commandMap.get("sys_code"));
		CommandMap headerParam = new CommandMap();
		headerParam.put("id_service", commandMap.get("sys_code"));
		CustomMap idenMap = service.getIdentyMap(commandMap);
		model.addAttribute("idenMap", idenMap);
		List<CustomMap> headerList = service.selectBidNotiMForUsing(headerParam);
//		CommandMap specialMap = new CommandMap();
		if(!"".equals(commandMap.getString("sidx", ""))) {
			commandMap.put("order_yn", "Y");
		}
		PageList<CustomMap> todayList = null;
		try {
			todayList = service.selectBidNotiOriginListContents(commandMap,null);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
		//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
		List<Integer> rawKey = new ArrayList<Integer>();
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_bid_noti",0);
			if(key!=0) {
				rawKey.add(key);
				valMap.put(key, cm);
			}
		}
		if(rawKey.size()>0) {
			List<CustomMap> rawList = service.selectBidNotiColOriginForSearch(rawKey);
			for(CustomMap cm : todayList) {
				int key = cm.getInt("id_bid_noti",0);
				for(CustomMap rm:rawList) {
					if(rm.getInt("id_bid_noti",0)==key) {
						cm.putByString(rm.getString("bid_col_cd", ""), rm.getString("conts",""));
					}
				}
			}
		}
		putModelListPage(todayList, model);
		model.addAttribute("headerList", headerList);
		model.addAttribute("paramMap", commandMap.getMap());
		return "front/bid/bidNotiOriginList";
	}

}
