package crs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.math3.analysis.FunctionUtils;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.junit.Test;
import org.nfunk.jep.JEP;
import org.nfunk.jep.ParseException;

import daily.common.util.MetaUtil;
import daily.common.vo.CustomMap;

public class CalcFormulaTest {
	@Test
	public void isEqualIgnoreUpperTest() {
//		String origin="Abcde";
//		String target="aBcdf";
		String expr = "cntrctAmt-(cntrctAmt/1.1)";
		JEP jep = new JEP();
		jep.addVariable("cntrctAmt", 483630000);
		// 수식 파싱
		jep.parseExpression(expr);
//		double result = (double) jep.getValue();
		int scale=20;
		int precision = 2;
		BigDecimal decimalValue = new BigDecimal(jep.getValue())
                .setScale(precision, RoundingMode.HALF_UP);  // 소수점 이하 자릿수 설정
    //            .setScale(scale - precision);
		// 계산
	//	 System.out.println("Original double value: " + result);
	     System.out.println("Formatted decimal value: " + decimalValue);
	}

	@Test
	public void rateTest() {
//		String origin="Abcde";
//		String target="aBcdf";
		String expr = "cntrctAmt*advPayRate";
		JEP jep = new JEP();
		jep.addVariable("cntrctAmt", 483630000);
//		jep.addVariable("advPayRate", "30%");
		// 수식 파싱
		jep.parseExpression(expr);
//		double result = (double) jep.getValue();
		int scale=20;
		int precision = 2;
		BigDecimal decimalValue = new BigDecimal(jep.getValue())
                .setScale(precision, RoundingMode.HALF_UP);  // 소수점 이하 자릿수 설정
    //            .setScale(scale - precision);
		// 계산
	//	 System.out.println("Original double value: " + result);
	     System.out.println("Formatted decimal value: " + decimalValue);
	}

	@Test
	public void longNumTest() {
//		String origin="Abcde";
//		String target="aBcdf";
		String expr = "cntrctAmt*advPayRate";
		String longNum="1234234234234123";
		JEP jep = new JEP();
		double val = MetaUtil.getStringToDouble(longNum);
		System.out.println("double val:"+val);
		jep.addVariable("cntrctAmt", val);
//		jep.addVariable("advPayRate", "30%");
		// 수식 파싱
		jep.parseExpression(expr);
//		double result = (double) jep.getValue();
		int scale=20;
		int precision = 2;
		BigDecimal decimalValue = new BigDecimal(jep.getValue())
                .setScale(precision, RoundingMode.HALF_UP);  // 소수점 이하 자릿수 설정
    //            .setScale(scale - precision);
		// 계산
	//	 System.out.println("Original double value: " + result);
	     System.out.println("Formatted decimal value: " + decimalValue);
	}

	@Test
	public void rate11Test() {
	     JEP jep = new JEP();

	        // 변수에 퍼센트 값을 추가
	        String variableName = "percent";
	        double percentValue = 50.0; // 50%
	        double convertedValue = percentValue / 100.0; // 퍼센트 값을 소수로 변환하여 사용

	        jep.addVariable(variableName, convertedValue);

	        // 수식 문자열
	        String expression = "percent * 10";

	        jep.parseExpression(expression);

	        // 계산 결과
	        double result = jep.getValue();

	        System.out.println("Expression: " + expression);
	        System.out.println("Result: " + result);
	}
	@Test
	public void headTest() {
		List<CustomMap> headerList = new ArrayList<>();
		int max_meta_depth=3;
		
		CustomMap cus1 = new CustomMap();
		cus1.put("child_cnt", 1);
		cus1.put("meta_depth", 1);
		List<CustomMap> cusList1 = new ArrayList<>();
		CustomMap cus2 = new CustomMap();
		cus2.put("meta_depth", 2);
		cusList1.add(cus2);
		cus1.put("child_list",cusList1);
		headerList.add(cus1);
		
		CustomMap cus1WithChild = new CustomMap();
		cus1WithChild.put("child_cnt", 1);
		cus1WithChild.put("meta_depth", 1);

		List<CustomMap> cusList1WithChild = new ArrayList<>();
		CustomMap cus2WithChild = new CustomMap();
		cus2WithChild.put("meta_depth", 2);
		cusList1WithChild.add(cus2WithChild);

		cus1WithChild.put("child_list", cusList1WithChild);
		headerList.add(cus1WithChild);

		// CustomMap without child_list
		CustomMap cus1WithoutChild = new CustomMap();
		cus1WithoutChild.put("child_cnt", 0);
		cus1WithoutChild.put("meta_depth", 1);
		headerList.add(cus1WithoutChild);
		List<List<CustomMap>> headList = MetaUtil.buildHeaderList(headerList,max_meta_depth);
//		System.out.println(MetaUtil.generateTableHeader(headerList));
	}
}
