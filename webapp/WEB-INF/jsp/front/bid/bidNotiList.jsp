<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<c:set var="title" value="${idenMap.meta_m_nm}"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
<div class="page-wrap">
	<div class="page-layer">
		<div class="page-title-wrap">
			<span>${title}</span>
		</div>
		
		<form name="frm" id="frm" method="post">
			<div class="page-content-wrap">
				<input type="hidden" id="id_bid_noti" name="id_bid_noti"/>
				<input type="hidden" id="reg_yn" name="reg_yn"/>
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="id_meta_m" name="id_meta_m" value="${param.id_meta_m}"/>
				<input type="hidden" id="trg_id_meta_m" name="trg_id_meta_m" value="${param.trg_id_meta_m}"/>
				<input type="hidden" id="sys_code" name="sys_code" value="${param.sys_code}"/>
				<input type="hidden" id="search_typ" name="search_typ" value="${param.search_typ}"/>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
							<div class="search-group-single-input">
				              	<div class="tit_cont">검색조건</div>
					          	<select class="form-control mh-form-control" name="search_col" id="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${headerList}" varStatus="i">
					                <option value="${map.id_bid}" ${(param.search_col eq map.id_bid? 'selected':'')} data-typ="${map.typ}">${map.bid_col_short_nm}</option>
					          	</c:forEach>
					       		</select>
		              			<div class="tit_cont">검색어</div>
<%-- 		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${(empty param.search_keyword ?'':param.search_keyword)}"> --%>
								<input type="text" class="form-control mh-form-control" name="search_keyword" id="search_keyword" value="${paramMap.search_keyword}">
		              			<div class="tit_cont">검색기간</div>
		              			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
		              			-
		              			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
								<button type="button" class="btn-primary" onclick="javascript:search();">
									<span class="btn_search"></span>
									검색
								</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
					</div>
					<div class="r">
						<c:if test="${!is_guest}">
						<button type="button" class="btn h34 fl" onClick="javascript:openRegContreg();"><span class="btn_add"></span>수동입력</button>
						</c:if>
					</div>					
				</div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty idenMap.grid_width}">
				    <c:set var="grid_width" value="style='width:${idenMap.grid_width}%;'" />
				</c:if>
	            <div class="grid-area" ${grid_area_style}>
	                   <!-- 그리드 해더영역 -->
	                   <div class="grid-header" ${grid_width}>
	                       <c:forEach var="map" items="${headerList}" varStatus="i">
	                       <div class="header-unit" id="headerDiv${map.id_bid}">
	                       <input type="hidden" id="h_${map.bid_col_cd}" value="${map.id_bid}">
	                       <span id="head_span_${map.id_bid}">${map.bid_col_short_nm}</span><ctrl:orderMark val="${map.id_bid}" sidx="${param.sidx}" sord="${param.sord}"/>
	                       </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
	                       </c:forEach>
	                   </div>
	                   <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
	                   <div class="grid-contents" ${grid_width}>
	                   <c:forEach var="row" items="${list}" varStatus="i">
	                       <div class="grid-loof-cont ${(not empty row.id_meta_row?'editable':'')}" id="row_${i.count}" ondblclick="javascript:openRegContreg('${row.id_service}','${row.id_bid_noti}','${row.reg_yn}','${row.id_meta_row}')">
	                       <c:forEach var="col" items="${headerList}" varStatus="j">
	                       	<c:set var="col_nm" value="${col.bid_col_cd}"/>
	                        <c:set var="cont" value="${row[col_nm]}"/>
	                        <div class="cont-unit">
	                        <c:choose>
	                        	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
	                        	${cont}
	                        	</c:when>
	                        	<c:otherwise>
	                        	<a href="${cont}" target="blank">${cont}</a>
	                        	</c:otherwise>
	                        </c:choose>
	          				 </div>
	                   	</c:forEach>
	                       </div>
	                   </c:forEach>
	                   </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
	                   <!--//그리드영역-->
	                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
		  </div>
                   <!-- //paging -->
    	</form>
	</div>
</div>
</front:FrontDynamicBody>
<script>
function doList(page){
	$("#page").val(page);
	var selectedOption = $("#search_col option:selected");
	var dataType = selectedOption.data('typ');
	if($.trim(dataType)=='4'){
		$("#search_typ").val('4');
	}else{
		$("#search_typ").val('');
	}
	var frm =document.frm;
		frm.action=contextPath+"/front/bid/bidNotiList.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
function search(){
	$("#sidx").val('');
	doList(1);
}
function initSearchForm(){
	$("#search_col option:eq(0)").attr("selected","selected");
	$("#search_keyword").val('');
}
function openRegContreg(id_service,id_bid_noti,reg_yn,id_meta_row){
	$("#id_service").val(id_service);
	$("#id_bid_noti").val(id_bid_noti);
	$("#reg_yn").val(reg_yn);
	$("#id_meta_row").val(id_meta_row);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/regMetaData.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
</script>
</html>