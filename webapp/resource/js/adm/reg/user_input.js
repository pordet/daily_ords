//21.04.11 : /adm/aliasCheckAjax.do 에 해당하는 컨트롤러 없음.
function checkDouble(){		
	var insert_yn = $("#insert_yn").val();
	if($("#id_user").val()==""){
		alertOpen('사용자 ID를 입력하십시오.');
	}
	if(insert_yn =="Y"){
		$("#doubleYn").val("Y");
		var id_user = '3';
		id_user = $("#id_user").val();
		processing();
		$.ajax({
			url: contextPath+"/adm/user/aliasCheckAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: {id_user : id_user},                // HTTP 요청과 함께 서버로 보낼 데이터
	//	    data: {DocumentNumber : docNum , KeyPhrase : 'JDZytH@qDQJ^B!5wNWThYV7vkS&t*gyzHP8@s#4t^6AeM+&f_!V!&hRWySLUC-_HdrE8WDvhVm6HR&FWTyGk9#yHQP4L86yMF*eW3w*NLCbFP%wQyAv+m8q&?Ge*x@A$' },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "POST",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.map){
		    		alertOpen('기존에 사용중인 ID입니다.');
		    	}else{
		    		alertOpen('사용 가능한 ID입니다.');
		    		$("#doubleYn").val('N');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
	
}
function register(){
	var insert_yn = $("#insert_yn").val();
	if(insert_yn =='Y'){
		if($("#doubleYn").val()!='N'){
			alertOpen('중복체크를 하십시오.');
			return;
		}
		if($.trim($("#user_pwd").val())==''){
			alertOpen('암호를 입력하십시오.');
			return;
		}
		if($.trim($("#user_pwd1").val())==''){
			alertOpen('암호확인에 암호를 입력하십시오.');
			return;
		}
		if($("#user_pwd1").val()!=$("#user_pwd").val()){
			alertOpen('암호와 암호확인의 암호가 다릅니다.');
			$("#user_pwd1").val('');
			return;
		}
		if($.trim($("#cell_phone_num").val())==''){
			alertOpen('휴대폰 번호를 입력하십시오.');
			return;
		}
	}else{
		if(nanToStr($("#user_pwd1").val())=='' || $.trim($("#user_pwd1").val())==''){
			if($.trim($("#user_pwd").val())!=''){
				alertOpen('암호를 변경하려면 암호확인란에 암호를 입력하십시오.');
				return;
			}
		}else{
			if($.trim($("#user_pwd").val())!='' && $.trim($("#user_pwd1").val())=='' ){
				alertOpen('암호를 변경하려면 신규 암호에 암호확인의 암호를 입력하십시오.');
				return;
			}
			if(($.trim($("#user_pwd").val())!='' && $.trim($("#user_pwd1").val())!='') && $("#user_pwd1").val()!=$("#user_pwd").val()){
				alertOpen('암호와 암호확인의 암호가 다릅니다.');
				$("#user_pwd1").val('');
				return;
			}
		}
	}
    var email = $("#email").val();
    var exptext = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
    if(exptext.test(email)==false){
		//이메일 형식이 알파벳+숫자@알파벳+숫자.알파벳+숫자 형식이 아닐경우			
		alertOpen('이메일 형식에 맞지 않습니다.');
		$("#email").focus();
		return false;
	}
	var formData = new FormData($('#frm')[0]);
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/user/insertUsrAjax.do";
	}else{
		url = contextPath+"/adm/user/modifyUsrAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else if(e.ret==8){
    			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else{
	    		if(insert_yn =='Y'){
		    		$("#cur_title").html('사용자 수정');
		    		$("#user_seq").val(e.map.user_seq);
		    		$("#checkBtn").attr("disabled",true);
		    		$("#insert_yn").val('N');
	    		}
	    		$("#user_pwd").val('');
	    		$("#user_pwd1").val('');
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}
function initForm(){
	$("#cur_title").html('사용자 등록');
	$("#user_seq").val('');
	$("#id_user").val('');
	$("#insert_yn").val('Y');
	$("#user_pwd").attr('placeholder',"");
	$("#user_pwd1").attr('placeholder',"");
	$("#doubleYn").val('Y');
    $("#checkBtn").attr("disabled",false);
    $("#id_group option:eq(0)").attr("selected","selected");
    $("#user_nm").val('');
    $("#email").val('');
    $("#cell_phone_num").val('');
    $("#dept_nm").val('');
    $("#posi_nm").val('');
    $("#ra1").attr("checked",false);
    $("#ra2").attr("checked",true);
}
function delData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/user/deleteUsrAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}
function doList(){
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/user/userList.do";
	if(page!="")
		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
}
