package daily.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.core.runtime.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Component
public class FileUtil {

	Logger logger = LoggerFactory.getLogger(this.getClass());

    private static String FILE_UPLOAD_PATH;
//  @Value("$global['mybatis.pagination.count.per.page']")
	@Value("#{global['file.upload.path']}")
	public void setFilePath(String filePath) {
		FILE_UPLOAD_PATH = filePath+Path.SEPARATOR;
		logger.debug("TTTT:"+FILE_UPLOAD_PATH);
	}
    public static File convertMultipartFileToFile(MultipartFile file) throws IOException
    {   
    	File convFile = new File( file.getOriginalFilename());
    	file.transferTo(convFile);
        return convFile;      
    }

    public static File convertMultipartFileToTempFile(MultipartFile file) throws IOException
    {   
    	byte[] fileBytes = file.getBytes(); // MultipartFile을 byte 배열로 읽어옵니다.
        File convFile = File.createTempFile("temp", null); // 임시 파일을 생성합니다. 파일 이름은 "temp"로 시작합니다.

        try (FileOutputStream fos = new FileOutputStream(convFile)) {
            fos.write(fileBytes); // byte 배열을 파일에 씁니다.
        }

        return convFile;     
    }

	public static List<MultipartFile> getMultipartFileListByRequest(HttpServletRequest request) {
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames(); 
		MultipartFile multipartFile = null; 
		List<MultipartFile> attach = new ArrayList<MultipartFile>();
		while(iterator.hasNext()){ 
			multipartFile = multipartHttpServletRequest.getFile(iterator.next());
			if(multipartFile.isEmpty() == false){ 
				attach.add(multipartFile);
			} 
		}
		return attach;
	}
	public static String genSaveFilePath(String path) {
		Calendar calendar = Calendar.getInstance();
		path += File.separator+calendar.get(Calendar.YEAR);
		if(calendar.get(Calendar.MONTH)<10) 
			path += "0"+calendar.get(Calendar.MONTH);
		else
			path += calendar.get(Calendar.MONTH);
		if(calendar.get(Calendar.DATE)<10) 
			path += "0"+calendar.get(Calendar.DATE);
		else
			path += calendar.get(Calendar.DATE);
		return path;
	}
	// ���� ��媛��� 湲곗��쇰� ���� �대� ����
	public static String genSaveFileName(String fileName,String extName) {
		
		Calendar calendar = Calendar.getInstance();
		fileName += "_"+calendar.get(Calendar.YEAR);
		if(calendar.get(Calendar.MONTH+1)<10) 
			fileName += "0"+calendar.get(Calendar.MONTH+1);
		else
			fileName += calendar.get(Calendar.MONTH+1);
		if(calendar.get(Calendar.DATE)<10) 
			fileName += "0"+calendar.get(Calendar.DATE);
		else
			fileName += calendar.get(Calendar.DATE);
		fileName +="_";
		if(calendar.get(Calendar.HOUR_OF_DAY)<10) 
			fileName += "0"+calendar.get(Calendar.HOUR_OF_DAY);
		else
			fileName += calendar.get(Calendar.HOUR_OF_DAY);
		
		if(calendar.get(Calendar.MINUTE)<10) 
			fileName += "0"+calendar.get(Calendar.MINUTE);
		else
			fileName += calendar.get(Calendar.MINUTE);
		
		if(calendar.get(Calendar.SECOND)<10) 
			fileName += "0"+calendar.get(Calendar.SECOND);
		else
			fileName += calendar.get(Calendar.SECOND);
		
		if(calendar.get(Calendar.MILLISECOND)<10) 
			fileName += "0"+calendar.get(Calendar.MILLISECOND);
		else
			fileName += calendar.get(Calendar.MILLISECOND);
		fileName += extName;
		
		return fileName;
	}
	public static boolean saveFilePath(String path,File file) {
		boolean ret = false;
		File f = new File(path);
		if(!f.exists())  
		   f.mkdirs();
		try {
			if(file.createNewFile()){    
				ret = true;
			}else {
				ret = false;
			}
	    } catch (IOException e) {
			ret = false;
	    }	
		return ret;
	}
	
	public static boolean writeFile(MultipartFile multipartFile, String saveFilePath,String saveFileName)
								throws IOException{
		boolean result = true;

//		byte[] data = multipartFile.getBytes();
		File f = new File(saveFilePath);
		if(!f.isFile()) {
			f.mkdirs();
		}
		multipartFile.transferTo(new File(saveFilePath + File.separator + saveFileName));
		
		return result;
	}
	
	public static File convert(MultipartFile multipartFile) throws IOException {
        File file = new File(multipartFile.getOriginalFilename());
        
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            throw new IOException("Failed to convert MultipartFile to File", e);
        }
        
        return file;
    }
	public static void deleteDirectory(File directory) {
        if (directory.isDirectory()) {
            // 디렉토리 내의 파일과 디렉토리를 모두 가져옴
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    // 파일 또는 하위 디렉토리를 삭제
                    deleteDirectory(file);
                }
            }
        }
        // 디렉토리 삭제
        if (!directory.delete()) {
            System.out.println("Failed to delete directory: " + directory.getAbsolutePath());
        } else {
            System.out.println("Directory deleted: " + directory.getAbsolutePath());
        }
    }
}
