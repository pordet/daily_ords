
function addKeyword(){
	var bef_keyword_num = cur_keyword_num;
    cur_keyword_num=~~cur_keyword_num + 1;
    var keyword_num = cur_keyword_num;
	var html = '<div class="col-8 line" id="addKeyword_in_div_'+keyword_num+'">';
		html+= '	<div class="form-group">';
		html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+keyword_num+'" value="">';
		html+= '	</div>';
		html+= '</div>';
		html+= '<div class="col-4 line" id="delKeyword_in_div_'+keyword_num+'" style="text-align:right;">';
		html+= '	<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delKeyword('+keyword_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
		html+= '</div>';
	var fileCnt = $("[id^=addKeyword_in_div_]").length;
	if(fileCnt==0){
		$("#key_btn_group").after(html);
	}else{
		$("#delKeyword_in_div_"+bef_keyword_num).after(html);
	}
}

function viewData(id_board_master,id_board){
	$("#modi_id_board_master").val(id_board_master);
	$("#modi_id_board").val(id_board);
	$("[id^=addKeyword_in_div_]").remove();
	$("[id^=delKeyword_in_div_]").remove();
	$("[id^=keyword_]").remove();
	$("[id^=attach_]").remove();
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/getBoardAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
		    	alertOpen('키워드 목록 조회시 오류가 발생했습니다.');
	    	}else{
    			var html ='';
	    		$.each(e.pList,function(i,d){
	    			var id_parent_board = '';
	    			if(nanToStr(e.has_id_parent)==''){
	    				id_parent_board = e.master.id_parent_board;
	    			}else{
//	    				id_parent_board = e.master.id_parent_board;
	    				id_parent_board = e.master.id_board_master;
	    			}
	    			if(id_parent_board ==d.id_board_master){
		    			html+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_board_master").html(html);
    			var html1 ='<option value="">선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			if(nanToStr(e.master.id_board_master ==d.id_board_master)){
		    			html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    	   $("#modi_sel_child_board").html(html1);
	     	   loadData(e,true,false);
	     	   $("#modi_title").val(nanToStr(e.map.title));
	     	   $("#modi_comment").val(nanToStr(e.map.comment));
	     	   $("#modi_keyword").val(nanToStr(e.map.keyword));
	     	   $("#dataModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}

function search(){
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#modi_sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	selInit();
	doList(1,false);
	if($.trim($("#search_keyword").val())!=''){
		$("#search_init").show();
	}
}
function search_keyword_init(){
	$("#modi_sel_ord_str").val('');
	$("#search_keyword").val('');
	$("#search_init").hide();
}
function doList(page,isTreeClicked){
	$("#page").val(page);
	$("#modi_order_key").val($("#order_key").val());
	$("#modi_idx").val($("#idx").val());
	$("#modi_page").val(page);
	if($("#is_search").val()=='N'){
		$("#search_keyword").val('');
	}
	var params = $("#modi_frm").serialize();
		var url = contextPath+"/historyAjax.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		list_redraw(e);
		    		draw_paging_by_ajax(e.paginator);
		    		if(nanToStr(isTreeClicked)!='' && !isTreeClicked){
	   			    	infoOpen('성공하였습니다.');
		    		}
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}
var cur_file_num=1;
function addFile(){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
	var html ='<div class="col-8 line" id="addFile_in_div_'+file_num+'">';
	html+='		<div class="form-group inline-group">';
	html+='		   <input type="text" id="fileName'+file_num+'" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>';
	html+='		   <div class="attach-btn">';
	html+='			<button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>';
	html+='		    <input type="file" name="file_'+file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+file_num+'\').value = this.value"/>';
	html+='		   </div>';
	html+='		</div>';
	html+='		</div>';
	html+='		   <div class="col-4 line" id="delFile_in_div_'+file_num+'" style="text-align:right;">';
	html+='			<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile('+file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png'+'" alt="delete"/></a>';
	html+='		  </div>';
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==0){
		$("#file_btn_group").after(html);
	}else{
		$("#delFile_in_div_"+bef_file_num).after(html);
	}
}

function loadData(e,isTreeClicked,isInsert){
	if(nanToStr(e.map)!=""){
		$("#modi_id_board").val(e.map.id_board);
	}
	var html = '';
	var html = '';
	$("[id^=addFile_in_div_]").remove();
	if(nanToStr(e.attachList)!=''){
		if(e.attachList.length>0){
			var html ='';
			var rl = e.attachList.length;
			if(rl%2==1){
				rl=rl+1;
			}
			for(var i=1;i<=rl;i++){
				if(i>e.attachList.length){
					html+='	<div class="col-6 line" id="attach_'+d.id_board_file+'">';
					html+='	<div class="form-group inline-group">';
					html+='	</div>';
					html+='</div>';
					html+='</div>';
				}else{
					var k = i-1;
					var d;
					$.each(e.attachList,function(i,h){
						if(k==i){
							d = h;
						}
					});
					if(i%2==1){
						html+='<div class="row">';
					}
					html+='<div class="col-6 line" id="attach_'+d.id_board_file+'">';
					html+='<div class="form-group inline-group">';
					html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'" class="ico_download" style="color:#fff;"><img src="'+contextPath+'/resource/img/history/ico_download.png" alt="다운로드"/></a> ';
					html+='	   <span class="span_cut">'+d.file_nm+'</span>';
					html+='</div>';
					html+='</div>';
					if(i%2==0){
						html+='</div>';
						html+='</div>';
						html+='</div>';
					}
				}
			}
			$("#cle_file_list_div").after(html);
			$("#file_list_div").show();
		}else{
			$("#file_list_div").hide();
		}
	}else{
		$("#file_list_div").hide();
	}
	$("#board_reg_type").html(e.board_path+" 조회");
}
function list_redraw(e){
	if(e.list.length==0){
		$("#result_no").show();
		$("#result_header").hide();
		$("#result_list").hide();
		
	}else{
		$("#result_no").hide();
		$("#result_header").show();
		$("#result_list").show();
	}
	if(e.list.length>0){
		var summary = '검색결과  '+e.paginator.totalCount+' 개 중 '+e.list[0].rownum+' ~ '+e.list[e.list.length-1].rownum;
		$("#result_summary").html(summary);
	}
	$(".listboard-contents1").empty();
			var html='<ul class="list-line">';
		$.each(e.list,function(i,d){
			html+='<li>';
			html+='<div class="meta">'+d.board_nm+', '+d.reg_date+'</div>';
			html+='<div class="search-result-title"><a href="javascript:void(0);" onclick="javascript:viewData('+nanToStr(d.id_board_master)+','+d.id_board+');">'+d.title+'</a>';
			html+='	<div class="file-format inline-group">';
	   		$.each(d.filelist,function(j,f){
	   			html+='	<a href="'+contextPath+'/file/data_download.do?id_board_file='+f.id_board_file+'"><span class="'+f.file_sh_ext+'">'+f.file_ext+'</span></a>';
	   		});
			html+='	</div>';
			html+='</div>';
			html+='<div class="search-result-text txt_post">'+nanToStr(d.comment)+'</div>';
			html+='</li>';
		});
		html+='</ul>';
		$.each(e.countList,function(i,d){
			$("#span_"+d.ord_str).html("("+d.board_cnt+")");
		});
		html+='</ul>';
		
		$(".listboard-contents1").html(html);
}