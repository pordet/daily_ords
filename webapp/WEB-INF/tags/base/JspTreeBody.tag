<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="hasNotCommonPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="currLocale" required="true" %>
<%@ attribute name="hasHeader" required="false" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
    <div class="mh-wrapper">
        <base:BodyHead currLocale="${currLocale}" hasHeader="${hasHeader}"/>
        <base:BodyMenu currLocale="${currLocale}"/>     
        <div class="dimmed"></div>
        <!--  mh-container -->
        <div class="mh-container">
	<jsp:doBody/>
        </div>
        <!-- // mh-footer -->
    </div>
    <!-- // mh-wrapper -->
    
	<div class="spinner" id="spinnerDiv" style="display:none;">
        <div class="in"><img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/></div>
    </div>
    <!-- //spinner  -->
    <c:if test="${hasPop}">
   <!-- modal alert -->
    <div class="modal fade mh-modal" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body red-type" id="alertMsg">
                    <p id="alertMsg">기존 사용자가 존재합니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal info -->
    <div class="modal fade mh-modal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body" id="alertMsg">
                    <p id="infoMsg">기존 사용자가 존재합니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal confirm-->
    <div class="modal fade mh-modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body blue-type" id="alertMsg">
                    <p id="confirmMsg">기존 사용자가 존재합니다.</p>
                </div>
                <input type="hidden" id="confirm_yn" value="N">
                <input type="hidden" id="confirm_func_nm">
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:okModel('Y');">확인</button>
                    <button type="button" class="btn cancel" data-dismiss="modal" onclick="javascript:okModel('N');">취소</button>
                </div>
            </div>
        </div>
    </div>
    </c:if>
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap/js/bootstrap.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker - locale ko js -->
    <script src="<c:url value='/resource/plugins/moment/locale/ko.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <script src="<c:url value='/resource/js/datetime.locale.js'/>"></script>
    <!-- Data Tables JS -->
    <script src="<c:url value='/resource/plugins/datatables/js/jquery.dataTables.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/full_numbers_no_ellipses.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/dataTables.responsive.min.js'/>"></script>
    <!-- nppa JS -->
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
    <script src="<c:url value='/resource/js/comm_ords.js'/>"></script>

</body>
<script>
$(function () {
	$(".header-unit").click(function(e){
		e.preventDefault();
		var idx = $(".header-unit").index(this)+1;
		$("#sidx").val(idx);
	 	if($(this).find("span").hasClass("sorting_asc")){
			$("#sord").val("DESC");
		}else{
			$("#sord").val("ASC");
		}
	 	doList($("#page").val());
	});
  });
function processing(){
	$("#spinnerDiv").show();
}
function endProcessing(){
	$("#spinnerDiv").hide();
}
function alertOpen(msg){
  	$("#alertMsg").html(msg);
  	$('#alertModal').modal('show');
}
function infoOpen(msg){
  	$("#infoMsg").html(msg);
  	$('#infoModal').modal('show');
}
function confirmOpen(msg,func){
  	$("#confirmMsg").html(msg);
  	$('#confirmModal').modal('show');
  	$("#confirm_func_nm").val(func);
}
function closeModel(){
	if($('.doc-modal:visible').length>0){
		$('body').addClass('modal-open');
	}
}
function okModel(key){
	if(key=='Y'){
		$("#confirm_yn").val('Y');
		var funcNm = $("#confirm_func_nm").val();
		eval(funcNm+"();");
	}
	else{
		$("#confirm_yn").val('N');
	}
	if($('.doc-modal:visible').length>0){
		$('body').addClass('modal-open');
	}
}
</script>
