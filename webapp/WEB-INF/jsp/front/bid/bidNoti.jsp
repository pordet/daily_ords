<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<front:FrontDynamicHead title="서비스 관리"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
	<form id="frm" name="frm">
		<div class="page-content-wrap">
			<div class="single-row">
       			<div class="tit_cont">검색기간</div>
       			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
       			-
       			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
				<button type="button" class="btn-primary" onclick="javascript:search();">
					<span class="btn_search"></span>
					입찰자료 등록
				</button>
			</div>
			<div class="single-row">
       			<div class="tit_cont">검색기간</div>
       			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
       			-
       			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
				<button type="button" class="btn-primary" onclick="javascript:search();">
					<span class="btn_search"></span>
					입찰자료 등록
				</button>
			</div>
		</div>
	</form>
<script>
function onStart(){		
	$.ajax({
		url: contextPath+"/front/bid/bidNotiAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.cnt>0){
	    		alertOpen('기존에 사용중인 Url 패턴이 있습니다.');
	    	}else{
	    		alertOpen('사용 가능한 Url 패턴입니다.');
	    		$("#doubleYn").val('N');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function search(){
	var params = $("#frm").serialize();
	$.ajax({
		url: contextPath+"/front/bid/bidNotiSearchAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
		data:params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.cnt>0){
	    		alertOpen(cnt+'개수 만큼 등록할 자료가 있습니다.');
	    	}else{
	    		alertOpen('등록할 자료가 있습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

</script>
</front:FrontDynamicBody>
</html>