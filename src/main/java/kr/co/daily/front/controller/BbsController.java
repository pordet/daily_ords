package kr.co.daily.front.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.BbsManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BbsService;

@Controller
public class BbsController extends AbstractControllerBase {
	
	private Logger log = LoggerFactory.getLogger(BbsController.class);
	@Autowired private BbsService service;
	@Autowired private BbsManageService manageService;
	@RequestMapping(value = {"/front/bbs/boardList.do","/adm/bbs/boardList.do"})
	public String boardList(CommandMap commandMap, ModelMap model) throws Exception {
		log.info("commandMap.toString()");
		log.info(commandMap.toString());
		CustomMap bbs = service.getBbsM(commandMap);
		model.addAttribute("bbs", bbs);
		List<CustomMap> headerList = new ArrayList<>();
		List<CustomMap> makeOrdList = new ArrayList<>();
		List<CustomMap> makeList = new ArrayList<>();
		CustomMap boardNoMap = new CustomMap();
		boardNoMap.put("meta_nm", "번호");
		boardNoMap.put("meta_depth", "1");
		boardNoMap.put("meta_cd", "row_num");
		boardNoMap.put("ord", "0");
		boardNoMap.put("typ", GlobalConst.DT_NUM);
		boardNoMap.put("decimal_len", "10");
		boardNoMap.put("precision_len", "0");
		headerList.add(boardNoMap);
		List<CustomMap> searchHeaderList = service.selectBbsHeaderList(commandMap);
		headerList.addAll(searchHeaderList);
		
		CustomMap updViewCntMap = new CustomMap();
		updViewCntMap.put("meta_nm", "조회수");
		updViewCntMap.put("meta_depth", "1");
		updViewCntMap.put("meta_cd", "view_cnt");
		updViewCntMap.put("ord", "100");
		updViewCntMap.put("typ", GlobalConst.DT_NUM);
		updViewCntMap.put("decimal_len", "10");
		updViewCntMap.put("precision_len", "0");
		CustomMap updUsrNmMap = new CustomMap();
		updUsrNmMap.put("meta_nm", "최종수정자");
		updUsrNmMap.put("meta_depth", "1");
		updUsrNmMap.put("meta_cd", "mod_user_nm");
		updUsrNmMap.put("ord", "101");
		updUsrNmMap.put("typ", GlobalConst.DT_STR);
		CustomMap updUsrDateMap = new CustomMap();
		updUsrDateMap.put("meta_nm", "최종수정일");
		updUsrDateMap.put("meta_depth", "1");
		updUsrDateMap.put("meta_cd", "mod_date");
		updUsrDateMap.put("ord", "102");
		updUsrDateMap.put("typ", GlobalConst.DT_DATE);
		
		makeList.add(boardNoMap);
		makeList.add(updViewCntMap);
		makeList.add(updUsrNmMap);
		makeList.add(updUsrDateMap);
		
		makeOrdList.add(updViewCntMap);
		makeOrdList.add(updUsrNmMap);
		makeOrdList.add(updUsrDateMap);
		
		headerList.addAll(makeOrdList);
		
		model.addAttribute("headerList", headerList);
		PageList<CustomMap> list = service.selectBbsList(commandMap,makeList);
		model.addAttribute("list", list);
		return "front/bbs/boardList";
	}
	@RequestMapping(value = {"/front/bbs/boardDetail.do","/adm/bbs/boardDetail.do"})
	public String getBoard(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap bbsM = service.getBbsM(commandMap);
		model.addAttribute("bbs", bbsM);
		CustomMap meta_row = service.getBoardRow(commandMap);
		if(meta_row!=null) {
			List<CustomMap> addFileList = service.selectBoardAttachFile(commandMap);
			List<CustomMap> dataList = service.selectBoardData(commandMap);
			model.addAttribute("meta_row", meta_row);
			model.addAttribute("dataList", dataList);
			model.addAttribute("addFileList", addFileList);
 			if("Y".equals(commandMap.getString("is_guest","N"))) {
				service.updateViewCnt(commandMap);
			}
		}else {
			List<CustomMap> dataList = service.selectBoardShowYnMeta(commandMap);
			model.addAttribute("dataList", dataList);
		}
		
//		PageList<CustomMap> dataList = service.selectDataList(commandMap);
		return "front/bbs/getBoard";
	}
	@RequestMapping(value = {"/front/bbs/file_down.do","/adm/bbs/file_down.do"})
	public ModelAndView historyDownload(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res) throws Exception {

//		param.put("fileId", fileId);
		CustomMap map = service.getBoardFile(param);
	    String fileFullPath=map.getString("file_real_nm");
		File srcFile = new File(fileFullPath);
		log.debug("srcFile:"+fileFullPath+",file exists : "+srcFile.exists());
	    return new ModelAndView("fileDownloadView", "downloadFile",srcFile);

	}
	@RequestMapping("/adm/bbs/insertBoardAjax.do")
	public String insertBoardAjax(CommandMap commandMap, HttpServletRequest req, ModelMap model) throws Exception {
//		networkService.del
		commandMap = this.addParamByLoginInfo(commandMap);
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)req; 
			model = service.insertBoard(commandMap,model,multipartHttpServletRequest);
		}catch(Exception e) {
			e.printStackTrace();
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		CustomMap bbsM = service.getBbsM(commandMap);
		model.addAttribute("bbs", bbsM);
		return "jsonView";
	}
	@RequestMapping("/adm/bbs/deleteBoardAjax.do")
	public String deleteKeywordAjax(CommandMap commandMap, ModelMap model) throws Exception {
//		dataService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
//	        commandMap = addParamByLoginInfo(commandMap);
			model = service.deleteBoard(commandMap,model);
		}catch(Exception e) {
			e.printStackTrace();
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		CustomMap bbsM = service.getBbsM(commandMap);
		model.addAttribute("bbs", bbsM);
		return "jsonView";
	}

	@RequestMapping("/adm/bbs/modifyBoardAjax.do")
	public String modifyHistoryFileAjax(CommandMap commandMap, ModelMap model,MultipartHttpServletRequest req) throws Exception {
//		networkService.del
		commandMap = this.addParamByLoginInfo(commandMap);
		int ret = GlobalConst.ACT_SUCCESS;
		try { 
			model = service.modifyBoard(commandMap,model,req);
		}catch(Exception e) {
			e.printStackTrace();
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		CustomMap bbsM = service.getBbsM(commandMap);
		model.addAttribute("bbs", bbsM);
		return "jsonView";
	}
	

}
