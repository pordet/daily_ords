package kr.co.daily.domain;

import java.util.List;

public class TabVO {
	// input value
	private String id_user;
	private String locale;
	private int depth;
	
	// output value
	private String menuName;
	private int menuId;
	private int parentId;
	private int menuOrder;
	private String menuClass;
	private String url;
	private int childCnt;
	private List<TabVO> child;
	private boolean selected;
	private boolean childSelected;
	
	public String getId_user() {
		return id_user;
	}
	public void setId_user(String id_user) {
		this.id_user = id_user;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public int getMenuOrder() {
		return menuOrder;
	}
	public void setMenuOrder(int menuOrder) {
		this.menuOrder = menuOrder;
	}
	public String getMenuClass() {
		return menuClass;
	}
	public void setMenuClass(String menuClass) {
		this.menuClass = menuClass;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public int getChildCnt() {
		return childCnt;
	}
	public void setChildCnt(int childCnt) {
		this.childCnt = childCnt;
	}
	public List<TabVO> getChild() {
		return child;
	}
	public void setChild(List<TabVO> child) {
		this.child = child;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isChildSelected() {
		return childSelected;
	}
	public void setChildSelected(boolean childSelected) {
		this.childSelected = childSelected;
	}
	
	

}
