<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="typ" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="val" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="dec_len" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="prec_len" required="false" %>							<%-- 리턴 값 받을 변수명 --%>
<c:choose>
	<c:when test="${fn:contains(val,',')}">
		${val}
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${typ eq '1'}">
				<fmt:formatNumber value='${val}' pattern='#,##0'/>
		    </c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${empty prec_len || prec_len==0}">
						<c:set var="patn" value="#,##0"/>
					</c:when>
					<c:otherwise>
						<c:set var="zero" value=""/>
					    <c:forEach var="cnt" begin="1" end="${prec_len}" step="1">
					    	<c:set var="zero" value="${zero}0"/>
					    </c:forEach>
					    <c:set var="patn" value="#,##0.${zero}"/>
					</c:otherwise>
				</c:choose>
				<fmt:formatNumber value="${val}" pattern="${patn}"/>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
