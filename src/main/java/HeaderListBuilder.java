import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daily.common.vo.CustomMap;

public class HeaderListBuilder {
    public List<List<CustomMap>> buildHeaderList(List<CustomMap> lowList, int max_meta_depth) {
        List<List<CustomMap>> headerListByDepth = new ArrayList<>();
        Map<Integer, Integer> depthToChildCnt = new HashMap<>();

        for (int i = 1; i <= max_meta_depth; i++) {
            List<CustomMap> headerList = new ArrayList<>();

            for (CustomMap cellMap : lowList) {
                int childCnt = cellMap.getInt("child_cnt", 0);

                if (cellMap.getInt("meta_depth", 0) == i && childCnt > 0) {
                    int rowspan = max_meta_depth - i + 1;
                    int colspan = childCnt;

                    if (!depthToChildCnt.containsKey(i)) {
                        depthToChildCnt.put(i, childCnt);
                    } else {
                        colspan += depthToChildCnt.get(i);
                        depthToChildCnt.put(i, colspan);
                    }

                    // Create a header map with rowspan and colspan values
                    CustomMap headerMap = new CustomMap();
                    headerMap.put("rowspan", rowspan);
                    headerMap.put("colspan", colspan);
                    headerList.add(headerMap);
                }
            }

            headerListByDepth.add(headerList);
        }

        return headerListByDepth;
    }

    public static void main(String[] args) {
        // Replace this with your actual data retrieval logic
//        List<CustomMap> lowList = retrieveDataFromDatabase();
//        int max_meta_depth = calculateMaxMetaDepth(lowList);

        HeaderListBuilder builder = new HeaderListBuilder();
        List<CustomMap> lowList = new ArrayList<>();
		int max_meta_depth=2;
		List<List<CustomMap>> headerListByDepth = builder.buildHeaderList(lowList, max_meta_depth);

        // The 'headerListByDepth' now contains lists of header maps by depth level
    }

    // Implement 'retrieveDataFromDatabase' and 'calculateMaxMetaDepth' methods as needed
}
