<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="코드그룹 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_code_group" id="id_code_group"/>
					<input type="hidden" name="code_group" id="code_group" value="${param.code_group}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
									<select name="search_col">
										<option value="ug" ${(param.search_col eq 'ug'? 'selected':'')}>코드그룹 코드</option>
										<option value="nm" ${(param.search_col eq 'nm'? 'selected':'')}>그룹명</option>
										<option value="id" ${(param.search_col eq 'de'? 'selected':'')}>그룹설명</option>
									</select>
									<input type="text" class="" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
				</form>
					
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button class="btn-primary" onclick="javascript:openRegCodeGroup();return false">코드그룹 추가</button>
							<button class="btn-danger" onclick="javascript:deleteList();return false">코드그룹 삭제</button>
						</div>					
					</div>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit">
								<a href="#">
									코드그룹 코드
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									코드그룹명
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit" >
								<a href="#">
									코드그룹 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<form id="listFrm" name="listFrm">
							<input type="hidden" name="search_col" id="list_search_col" value="${param.search_col}"/>
							<input type="hidden" name="search_keyword" id="list_search_keyword" value="${param.search_keyword}"/>
							<input type="hidden" name="id_menu" id="list_id_menu" value="${param.id_menu}"/>
							<input type="hidden" name="sidx" id="list_sidx" value="${param.sidx}"/>
							<input type="hidden" name="sord" id="list_sord" value="${param.sord}"/>
							<input type="hidden" name="page" id="list_page" value="${param.page}"/>
							<c:choose>
								<c:when test="${fn:length(list) == 0 }">
								</c:when>
								<c:otherwise>
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openRegCodeGroup('${map.id_code_group}','${map.code_group}');">
									<div class="check-unit">
										<input type="checkbox" value="${map.id_code_group}" name="del_code" id="del_code${i.count}">
					                    <input type="hidden" id="codeGroupId${i.count}" value="${map.id_code_group}">
									</div>
									<div class="cont-unit">${map.code_group}</div>
									<div class="cont-unit">${(not empty map.code_group_nm? map.code_group_nm:'&nbsp;')}</div>
									<div class="cont-unit desc-unit">${(not empty map.code_group_desc? map.code_group_desc:'&nbsp;')}</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
								</c:otherwise>
							</c:choose>
							</form>
						</div>
					</div>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	$("#default1-tab").click(function(){
		location.href="<c:url value='/adm/code/codeList.do'/>?id_menu=${param.id_menu}";
	});
	$("#default2-tab").click(function(){
		location.href="<c:url value='/adm/code/codeGroupList.do'/>?id_menu=${param.id_menu}";
	});
});
function openRegCodeGroup(id_code_group,code_group){
	var page = $("#page").val();
	if(id_code_group){
		$("#id_code_group").val(id_code_group);
	}
	if(code_group){
		$("#code_group").val(code_group);
	}
	var frm =document.frm;
	frm.action=contextPath+"/adm/code/regCodeGroup.do";
	$("#page").val(page);
   	$("#frm").submit();
}
function initSearchForm(){
	$("#search_keyword").val('');
}
function doList(page){
	var frm =document.frm;
	frm.action=contextPath+"/adm/code/codeGroupList.do";
	if(page!=''){
		$("#page").val(page);
	}else{
		$("#page").val(1);
	}
    $("#frm").submit();
}
function search(){
	doList(1);
}
function deleteList(){
	if($("input:checkbox[name=del_code]:checked").length==0){
		alertOpen('삭제할 항목을 선택하십시오.');
	}else{
		confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
	}
}
function deleteConfirm(){
	if($("#confirm_yn").val()=='Y'){
	    $("#confirm_yn").val('N');
		var frm =document.listFrm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/code/deleteCodeGroupList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
	}
}
</script>
</html>
