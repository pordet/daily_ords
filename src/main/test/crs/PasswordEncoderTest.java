package crs;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class PasswordEncoderTest {

	private PasswordEncoder  encoder;
	
	@Before
	public void setUp() throws Exception {
		encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
	
	@Test
	public void encode() {
		String password="1";
		
		String encPassword = encoder.encode(password);
		System.out.println(encPassword); 
		assertEquals(password, encPassword); 
//		assertThat(encPassword,CoreMatchers.is("{by}"));
	}

	@Test(expected = IllegalArgumentException.class) 
	public void test_pass() { 
		String password = "password"; 
		String encPassword1 = "7a07c208fc2a407fb89cc3b6effb1b759da575a85f65dda9cd426f1ad14b56e6afaeeea6f9269569";
		// pbkdf2 
		String encPassword2 = "$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq"; 
		// bcrypt 
		String encPassword3 = "fcef9e3f82af42d9059e74a95c633fe99b7aba1c4bfb9ac1cae31dd1b67060da933776fee8baec8f"; 
		// sha256 
		System.out.println(encoder.matches(password, encPassword1));
		System.out.println(encoder.matches(password, encPassword2));
		System.out.println(encoder.matches(password, encPassword3));
		assertTrue(encoder.matches(password, encPassword1)); 
		assertTrue(encoder.matches(password, encPassword2)); 
		assertTrue(encoder.matches(password, encPassword3)); 
	} 
	@Test public void compare_pass() { 
		String password = "password"; 
		String encPassword = "$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq"; 
		// bcrypt 
		DelegatingPasswordEncoder delegatingPasswordEncoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder(); 
		delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder()); 
		assertTrue(encoder.matches(password, encPassword)); 
	} 
	@Test public void customDelegatingPasswordEncoder() { 
		Map<String, PasswordEncoder> encoders = new HashMap<>(); 
		String idForEncode = "bcrypt"; 
		encoders.put(idForEncode, new BCryptPasswordEncoder()); 
		encoders.put("pbkdf2", new Pbkdf2PasswordEncoder()); 
		encoder = new DelegatingPasswordEncoder(idForEncode, encoders); 
		String password = "password"; String encPassword = encoder.encode(password); 
		System.out.println(encPassword); 
		assertEquals(password, encPassword); 
		String encPassword1 = "{pbkdf2}7a07c208fc2a407fb89cc3b6effb1b759da575a85f65dda9cd426f1ad14b56e6afaeeea6f9269569"; 
		String encPassword2 = "{bcrypt}$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq"; 
		String encPassword3 = "{sha256}fcef9e3f82af42d9059e74a95c633fe99b7aba1c4bfb9ac1cae31dd1b67060da933776fee8baec8f"; 
		assertEquals(password, encPassword1); 
		assertEquals(password, encPassword2); 
		assertEquals(password, encPassword3); 
	}

	@Test
	public void testBCryptPasswordEncoder() {
		String password = "crspassword";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);

		boolean result = passwordEncoder.matches(password, hashedPassword);
		System.out.println("result:"+result);
		System.out.println(hashedPassword);
	}
	
}
