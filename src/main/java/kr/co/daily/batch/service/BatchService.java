package kr.co.daily.batch.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.CollectionUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.util.StrUtil;
import daily.common.util.StringDateUtil;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.batch.dao.BatchDAO;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BidSearchService;

@Service
public class BatchService {

	@Autowired private BidSearchService bidSearchService;
	@Autowired private BatchDAO dao;
	private static Logger log = LoggerFactory.getLogger(BatchService.class);
	
	public void insertDailyBatchM(CommandMap map) {
		dao.insertDailyBatchM(map.getMap());
	}

	public void insertDailyBatchRet(CommandMap map) {
		dao.insertDailyBatchRet(map.getMap());
	}

	public CustomMap getLastDailyBatchMBySearchDate(CommandMap map) {
		return dao.getLastDailyBatchMBySearchDate(map.getMap());
	}

	public int getDailyBatNumByIdBatch(CommandMap map) {
		return dao.getDailyBatNumByIdBatch(map.getMap());
	}

	public void updateDailyBatNumByIdBatch(CommandMap map) {
		dao.updateDailyBatNumByIdBatch(map.getMap());
	}

	public int getBatchTotPageAtDailyBatchMByIdBatch(Map<String, String> batInfoMap) {
		return dao.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);	
	}

	public int getSuccCountAtDailyBatchRetByIdBatch(Map<String, String> batInfoMap) {
		return dao.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
	}

	public void updateSuccYnAtBatchM(Map<String, String> batInfoMap) {
		dao.updateSuccYnAtBatchM(batInfoMap);
	}

	public CustomMap getSuccYnAtDailyBatchMBySearchDate(Map<String, String> batInfoMap) {
		return dao.getSuccYnAtDailyBatchMBySearchDate(batInfoMap);
	}

	public CustomMap getTotCntAtDay(CommandMap map) {
		return dao.getTotCntAtDay(map.getMap());
	}

	/**첫번째 성공시도 만 저장하는 메서드
     * restTemplate.getForObject 사용
     * @param search_date
     * @param batInfoMap 
     * @return
     * @throws ParseException
     */
    public Map<String,Object> getRestfulInfoByCurl(String search_date, int curr_pg, int num_of_rows, boolean is_checked,Map<String, String> batInfoMap) throws Exception{
    	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    String st_dt = null;
	    if(search_date==null) {
	    	st_dt = StringDateUtil.getToday("yyyyMMdd");
	    }else {
			st_dt = search_date;
	    }
//		params.add(bidNtceBgnDt, st_dt+"0000");
//		params.add(bidNtceEndDt, ed_dt+"2359");
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
                .queryParam(numOfRows, num_of_rows)
                .queryParam(pageNo, curr_pg)
                .queryParam(data_type, "json")
                .queryParam(bidNtceBgnDt, st_dt+"0000")
                .queryParam(bidNtceEndDt, st_dt+"2359");
//                .queryParam(serviceKey, test_key);
//
//        String url = builder.toUriString();
//        System.out.println(url);
        String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
        System.out.println(url);
        
    	ProcessBuilder processBuilder = new ProcessBuilder("curl", url);

        boolean sucYn=false;
        try {
            Process process = processBuilder.start();

            // 응답 데이터를 읽어오기 위한 작업
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder resp = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
            	resp.append(line);
            }

            reader.close();

            JSONParser parser = new JSONParser();
            try {
            	JSONObject respJson = (JSONObject)parser.parse(resp.toString());
            	CommandMap searchBatParam = new CommandMap();
            	searchBatParam.put("search_date", search_date);
            	CustomMap dailyBatM = this.getLastDailyBatchMBySearchDate(searchBatParam);
            	if(dailyBatM==null) {
            		
                	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,curr_pg,num_of_rows,batInfoMap);
                    if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
            	        result.put("sucYn", true);
            	        result.put("id_batch", ins_ret.get("id_batch"));
                    }else {
            	        result.put("sucYn", sucYn);
                    }
        	        result.put("tot_pg", ins_ret.get("tot_pg"));
            	}else {
            		//(TODO)이 경우를 찾아서 조치 필요
            		if(!is_checked) {
                    	boolean isIns = this.checkDateUpdated(respJson, batInfoMap);
                    	result.put("is_ins", isIns);
                    	if(isIns) {
                        	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,curr_pg,num_of_rows,batInfoMap);
                            if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
                    	        if(ins_ret.get("id_batch")==null) {
                    	        	if(batInfoMap.get("id_batch")!=null) {
                               	        result.put("id_batch", ins_ret.get("id_batch"));
                        	        }else {
                        	        	throw new Exception("id_batch가 존재하지 않는다.");
                        	        }
                    	        }else {
                        	        result.put("id_batch", ins_ret.get("id_batch"));
                    	        }
                    	        result.put("sucYn", true);
                    	        result.put("tot_pg", ins_ret.get("tot_pg"));
                            }else {
                    	        result.put("sucYn", sucYn);
                            }
                    		return result;
                    	}	
            		}else {
                    	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,curr_pg,num_of_rows,batInfoMap);
                        if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
                	        if(ins_ret.get("id_batch")==null) {
                	        	if(batInfoMap.get("id_batch")!=null) {
                           	        result.put("id_batch", ins_ret.get("id_batch"));
                    	        }else {
                    	        	throw new Exception("id_batch가 존재하지 않는다.");
                    	        }
                	        }else {
                    	        result.put("id_batch", ins_ret.get("id_batch"));
                	        }
                	        result.put("sucYn", true);
                	        result.put("tot_pg", ins_ret.get("tot_pg"));
                        }else {
                	        result.put("sucYn", sucYn);
                        }
                		return result;
            		}
            		
            	}
            }catch(Exception e) {
    	        result.put("sucYn", sucYn);
            }
        } catch (IOException e) {
	        result.put("sucYn", sucYn);
//            e.printStackTrace();
//            System.out.println("Error executing Curl command");
        }
//        String resp = restTemplate.getForObject(url, String.class);
//        System.out.println("resp: " + resp);
		return result;
    }

	/**첫번째 성공시도 만 저장하는 메서드
    * restTemplate.getForObject 사용
    * @param search_date
    * @param batInfoMap tring search_date, int curr_pg, int num_of_rows, 
    * @return
    * @throws ParseException
    */
    private Map<String,Object> insertRestfulInfoByCurl(Map<String, String> batInfoMap) throws ParseException{
    	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    String search_date = batInfoMap.get("search_date");
//	    String ed_date = batInfoMap.get("ed_date");
	    
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	               .queryParam(numOfRows, batInfoMap.get("lines_per_pg"))
	               .queryParam(pageNo, batInfoMap.get("curr_pg"))
	               .queryParam(data_type, "json")
	               .queryParam(bidNtceBgnDt, search_date+"0000")
	               .queryParam(bidNtceEndDt, search_date+"2359");
//		               .queryParam(serviceKey, test_key);
	//
//		       String url = builder.toUriString();
//		       System.out.println(url);
	    String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
	    System.out.println(url);
       
   		ProcessBuilder processBuilder = new ProcessBuilder("curl", url);

   		boolean sucYn=false;
   		try {
   			Process process = processBuilder.start();

           // 응답 데이터를 읽어오기 위한 작업
   			InputStream inputStream = process.getInputStream();
   			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
   			StringBuilder resp = new StringBuilder();
           	String line;

           	while ((line = reader.readLine()) != null) {
           		resp.append(line);
           	}

           	reader.close();

           	JSONParser parser = new JSONParser();
           	try {
           		JSONObject respJson = (JSONObject)parser.parse(resp.toString());
           		CommandMap searchBatParam = new CommandMap();
           		searchBatParam.put("search_date", search_date);
               	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,batInfoMap);
               	if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
               		result.put("sucYn", true);
       	        	result.put("id_batch", ins_ret.get("id_batch"));
               	}else {
               		result.put("sucYn", sucYn);
               	}
       	        result.put("tot_pg", ins_ret.get("tot_pg"));
           	}catch(Exception e) {
           		result.put("sucYn", sucYn);
           	}
   		} catch (IOException e) {
	        result.put("sucYn", sucYn);
//		           e.printStackTrace();
//		           System.out.println("Error executing Curl command");
   		}
		return result;
    }

	/**첫번째 성공시도 만 저장하는 메서드
    * restTemplate.getForObject 사용
    * @param search_date
    * @param batInfoMap tring search_date, int curr_pg, int num_of_rows, 
    * @return
    * @throws ParseException
    */
    private Map<String,Object> updateRestfulInfoByCurl(Map<String, String> batInfoMap) throws ParseException{
    	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    String search_date = batInfoMap.get("search_date");
//	    String ed_date = batInfoMap.get("ed_date");
	    
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	               .queryParam(numOfRows, batInfoMap.get("lines_per_pg"))
	               .queryParam(pageNo, batInfoMap.get("curr_pg"))
	               .queryParam(data_type, "json")
	               .queryParam(bidNtceBgnDt, search_date+"0000")
	               .queryParam(bidNtceEndDt, search_date+"2359");
//		               .queryParam(serviceKey, test_key);
	//
//		       String url = builder.toUriString();
//		       System.out.println(url);
	    String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
	    System.out.println(url);
       
   		ProcessBuilder processBuilder = new ProcessBuilder("curl", url);

   		boolean sucYn=false;
   		try {
   			Process process = processBuilder.start();

           // 응답 데이터를 읽어오기 위한 작업
   			InputStream inputStream = process.getInputStream();
   			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
   			StringBuilder resp = new StringBuilder();
           	String line;

           	while ((line = reader.readLine()) != null) {
           		resp.append(line);
           	}

           	reader.close();

           	JSONParser parser = new JSONParser();
           	try {
           		JSONObject respJson = (JSONObject)parser.parse(resp.toString());
           		CommandMap searchBatParam = new CommandMap();
           		searchBatParam.put("search_date", search_date);
               	Map<String,Object> ins_ret = this.updateOpenApiResult(respJson,batInfoMap);
               	if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
               		result.put("sucYn", true);
       	        	result.put("id_batch", ins_ret.get("id_batch"));
               	}else {
               		result.put("sucYn", sucYn);
               	}
       	        result.put("tot_pg", ins_ret.get("tot_pg"));
           	}catch(Exception e) {
           		result.put("sucYn", sucYn);
           	}
   		} catch (IOException e) {
	        result.put("sucYn", sucYn);
//		           e.printStackTrace();
//		           System.out.println("Error executing Curl command");
   		}
		return result;
    }

	Map<String,Object> updateTodayOrds(Map<String, String> batInfoMap) throws ParseException {
		Map<String,Object> result = new HashMap<String,Object>();
//    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		if(batInfoMap.get("try_api_cnt")==null) {
	        result.put("sucYn", false);
	        return result;
		}
    	Map<String,Object> batRet = this.insertRestfulInfoByCurl(batInfoMap);
    	int try_cnt_num = Integer.parseInt(String.valueOf(batInfoMap.get("try_api_cnt")));
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_cnt_num<3) {
//    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
    		    	Map<String,Object> batTryRet = this.insertRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    	        	        result.put("sucYn", true);
    	        	        result.put("try_api_cnt", batInfoMap);
    	        	        sucYn = true;
    	        	        break;
    		    		}
    		    	}
    		    	try_cnt_num++;
    			}
    		}
    	}
    	else {
	        result.put("sucYn", false);
	        result.put("try_api_cnt", batInfoMap);
			CommandMap retMap = new CommandMap();
			retMap.put("num_of_try", batInfoMap.get("try_"));
			retMap.put("succ_yn", "N");
			retMap.put("curr_pg_no", curr_pg);
			retMap.put("succ_yn","N");
			this.insertDailyBatchRet(retMap);
    	}
    	return result;
    }

	Map<String,Object>  insertTodayOrds(String search_date,int num_of_rows, int curr_pg,Map<String, String> batInfoMap) throws Exception {
		Map<String,Object> result = new HashMap<String,Object>();
//    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
		if(batInfoMap.get("try_api_cnt")==null) {
	        result.put("sucYn", false);
	        return result;
		}
    	Map<String,Object> batRet = this.getRestfulInfoByCurl(search_date,curr_pg,num_of_rows,true, batInfoMap);
    	int try_cnt_num = Integer.parseInt(String.valueOf(batInfoMap.get("try_api_cnt")));
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_cnt_num<3) {
//    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
    		    	Map<String,Object> batTryRet = this.getRestfulInfoByCurl(search_date,curr_pg,num_of_rows,true,batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    	        	        result.put("sucYn", true);
    	        	        result.put("try_api_cnt", batInfoMap);
    	        	        sucYn = true;
    	        	        break;
    		    		}
    		    	}
    		    	try_cnt_num++;
    			}
    		}
    	}
    	else {
	        result.put("sucYn", false);
	        result.put("try_api_cnt", batInfoMap);
			CommandMap retMap = new CommandMap();
			retMap.put("num_of_try", batInfoMap.get("try_"));
			retMap.put("succ_yn", "N");
			retMap.put("curr_pg_no", curr_pg);
			retMap.put("succ_yn","N");
			this.insertDailyBatchRet(retMap);
    	}
    	return result;
    }

	private Map<String,Object> insertTodayOrds(Map<String, String> batInfoMap) throws ParseException {
		Map<String,Object> result = new HashMap<String,Object>();
//    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		if(batInfoMap.get("try_api_cnt")==null) {
	        result.put("sucYn", false);
	        return result;
		}
    	Map<String,Object> batRet = this.insertRestfulInfoByCurl(batInfoMap);
    	int try_cnt_num = Integer.parseInt(String.valueOf(batInfoMap.get("try_api_cnt")));
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_cnt_num<3) {
//    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
    		    	Map<String,Object> batTryRet = this.insertRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    	        	        result.put("sucYn", true);
    	        	        result.put("try_api_cnt", batInfoMap);
    	        	        sucYn = true;
    	        	        break;
    		    		}
    		    	}
    		    	try_cnt_num++;
    			}
    		}
    	}
    	else {
	        result.put("sucYn", false);
	        result.put("try_api_cnt", batInfoMap);
			CommandMap retMap = new CommandMap();
			retMap.put("num_of_try", batInfoMap.get("try_"));
			retMap.put("succ_yn", "N");
			retMap.put("curr_pg_no", curr_pg);
			retMap.put("succ_yn","N");
			this.insertDailyBatchRet(retMap);
    	}
    	return result;
    }

	/**
	     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
	     * @param jsonObject
	     * @return
	     */
	//    private Map<String,Object> insertBatchM(JSONObject jsonObject,CommandMap param) {
	//    	Map<String,Object> result = new HashMap<String,Object>();
	//		Map resMap = (Map)jsonObject.get("response");
	//		Map resHeadMap = (Map)resMap.get("header");
	//		Map resBodyMap = (Map)resMap.get("body");
	//		try {
	//			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
	//			int totPage = totCnt / param.getInt("num_of_rows");
	//			int restRow = totCnt % param.getInt("num_of_rows");
	//			if(restRow!=0){
	//				totPage++;
	//			}
	//			System.out.println("총 페이지:"+totPage);
	//			result.put("tot_pg", totCnt);
	//			CommandMap commandMap = new CommandMap();
	//			commandMap.put("tot_pg", totPage);
	//			commandMap.put("search_date", param.getString("search_date"));
	//			commandMap.put("tot_row_cnt", totCnt);
	//			commandMap.put("lines_per_pg", param.getInt("num_of_rows"));
	//			commandMap.put("succ_yn", "Y");
	////			commandMap.put("tot_row_cnt", param.getString("search_date"));
	////			this.insertBatchM(commandMap);
	//
	//			return result;
	//		}catch(Exception e) {
	//			System.out.print("오류발생:"+e.getClass().getName());
	//			e.printStackTrace();
	//			result.put("sucYn", "N");
	//			return result;
	//		}
	//    	
	//    }
    /**
     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
     * @param jsonObject
     * @param num_of_rows 
     * @param curr_pg 
     * @param batInfoMap 
     * @return
     */
    private Map<String,Object> insertOpenApiResult(JSONObject jsonObject, int curr_pg, int num_of_rows, Map<String, String> batInfoMap) {
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
			List<String> keywordList = new ArrayList<String>();
			List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
			for(CustomMap k:kList) {
				keywordList.add(k.getString("code_nm", ""));
			}
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("id_service", GlobalConst.META_M_TYPE_BID);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)매칭이 안된 값 저장 
			List<List<CommandMap>> notMatchInputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date",batInfoMap.get("search_date"));
			searchCon.put("rslt_pg_no", 1);
			searchCon.put("id_service", GlobalConst.META_M_TYPE_BID);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
							System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										val.put("matched_keyword", matchingKeyword);
										inputMap.add(val);
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}else {
								result.put("sucYn", "N");
								throw new Exception("있어야 하는 값이 없음");
							}

						}
						//(TODO)키워드 검색 실패시 들어가야 하는 부분 처리
						else if(keywordList.size()>0 && !StrUtil.containsMidString(keywordList,bidNtceNm)) {
							List<CommandMap> notMatchInputMap = new ArrayList<CommandMap>();
							System.out.println("매칭이 안된 경우의 입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										notMatchInputMap.add(val);
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(notMatchInputMap.size()>0) {
								notMatchInputList.add(notMatchInputMap);
							}else {
								result.put("sucYn", "N");
								throw new Exception("있어야 하는 값이 없음");
							}
							
						}else if(keywordList.size()==0) {
							//키워드 검색 실패시 중요 알람
						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			//(not match 입력)
			if(notMatchInputList.size()>0) {
				bidSearchService.insertBidNotiOrigin(notMatchInputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				this.insertDailyBatchM(batM);
				int dailyBatNum = this.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				this.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
    	
    }

private boolean checkDateUpdated(JSONObject jsonObject, Map<String, String> batInfoMap) {
    	boolean ret = false;
		String search_date = batInfoMap.get("search_date");
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
//		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			CommandMap dayMap = new CommandMap();
			dayMap.put("id_service", batInfoMap.get("id_service"));
			dayMap.put("search_date", search_date);
			CustomMap currTot = this.getTotCntAtDay(dayMap);
			if(currTot==null) {
				return false;
			}else {
				if(totCnt == currTot.getInt("tot_row_cnt",0)) {
					return false;
				}else {
					return true;
				}
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			return false;
		}
    }

/**
     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
     * @param jsonObject
     * @param num_of_rows 
     * @param curr_pg 
     * @param batInfoMap 
     * @return
     */
    private Map<String,Object> insertOpenApiResult(JSONObject jsonObject, Map<String, String> batInfoMap) {
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		String search_date = batInfoMap.get("search_date");
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
//		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			Integer num_of_rows = Integer.parseInt(batInfoMap.get("lines_per_pg"));
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
			List<String> keywordList = new ArrayList<String>();
			List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
			for(CustomMap k:kList) {
				keywordList.add(k.getString("code_nm", ""));
			}
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("id_service", batInfoMap.get("id_service"));
			commandMap.put("search_date", search_date);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			
			// 키값을 가져오는 부분
			List<CustomMap> pkList = bidSearchService.selectBidPkInfoBySysCode(commandMap);
			List<String> pkStrList = new ArrayList<String>();
			List<String> pkBidColCdList = new ArrayList<String>();
			for(CustomMap pkCm:pkList) {
				String pkIdBid = pkCm.getString("id_bid","");
				if(!"".equals(pkIdBid)) {
					pkStrList.add(pkIdBid);
					pkBidColCdList.add(pkCm.getString("bid_col_cd", ""));
				}
			}
			commandMap.put("pkStrList", pkStrList);
			
			//기본키 정보에 해당되는 값을 가져와서 notiKeyMap에 여러개 행으로 된 notiVal값을 Map에 추가하는 부분
			List<CustomMap> notiKeyList = bidSearchService.selectBidNotiKeyList(commandMap);
			List<CustomMap> notMatchNotiKeyList = bidSearchService.selectBidNotiOriginKeyList(commandMap);
			Map<String,Map<String,String>> notiKeyMap = new HashMap<String,Map<String,String>>();
			Map<String,Map<String,String>> notMatchNotiKeyMap = new HashMap<String,Map<String,String>>();
			for(CustomMap pkCm:notiKeyList) {
				String idNoti = pkCm.getString("id_bid_noti","");
				String bidColCd = pkCm.getString("bid_col_cd","");
				String bidCdConts = pkCm.getString("conts","");
				if(notiKeyMap.get(idNoti)==null || "".equals(notiKeyMap.get(idNoti))) {
					Map<String,String> notiValMap = new HashMap<String,String>();
					notiValMap.put(bidColCd, bidCdConts);
					notiKeyMap.put(idNoti, notiValMap);
				}else {
					Map<String,String> notiValMap = notiKeyMap.get(idNoti);
					notiValMap.put(bidColCd, bidCdConts);
				}
			}
			for(CustomMap pkCm:notMatchNotiKeyList) {
				String idNoti = pkCm.getString("id_bid_noti","");
				String bidColCd = pkCm.getString("bid_col_cd","");
				String bidCdConts = pkCm.getString("conts","");
				if(notMatchNotiKeyMap.get(idNoti)==null || "".equals(notMatchNotiKeyMap.get(idNoti))) {
					Map<String,String> notiValMap = new HashMap<String,String>();
					notiValMap.put(bidColCd, bidCdConts);
					notMatchNotiKeyMap.put(idNoti, notiValMap);
				}else {
					Map<String,String> notiValMap = notMatchNotiKeyMap.get(idNoti);
					notiValMap.put(bidColCd, bidCdConts);
				}
			}
			Map<String,String> realPkMap = new HashMap<String,String>();
			for(String idNoti:notiKeyMap.keySet()) {
				Map<String,String> notiValMap = notiKeyMap.get(idNoti);
				StringBuffer notiValSb = new StringBuffer();
				for(String pkBidColCd:pkBidColCdList) {
					notiValSb.append(notiValMap.get(pkBidColCd)+"_");
				}
				String notiValStr = notiValSb.substring(0,notiValSb.toString().lastIndexOf("_"));
				realPkMap.put(notiValStr,"E");
			}
			
			Map<String,String> notMatchRealPkMap = new HashMap<String,String>();
			for(String idNoti:notMatchNotiKeyMap.keySet()) {
				Map<String,String> notiValMap = notMatchNotiKeyMap.get(idNoti);
				StringBuffer notiValSb = new StringBuffer();
				for(String pkBidColCd:pkBidColCdList) {
					notiValSb.append(notiValMap.get(pkBidColCd)+"_");
				}
				String notiValStr = notiValSb.substring(0,notiValSb.toString().lastIndexOf("_"));
				notMatchRealPkMap.put(notiValStr,"E");
			}
			
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)매칭이 안된 값 저장 
			List<List<CommandMap>> notMatchInputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date", search_date);
			searchCon.put("id_service", batInfoMap.get("id_service"));
			searchCon.put("rslt_pg_no", curr_pg);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
//					if(true) {
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
							System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							StringBuffer existChkSb = new StringBuffer();
							for(String pkBidColCd:pkBidColCdList) {
								existChkSb.append(json.get(pkBidColCd)+"_");
							}
							String existChkSbStr = existChkSb.substring(0,existChkSb.toString().lastIndexOf("_"));
							String existKeyStr = realPkMap.get(existChkSbStr);
							if(existKeyStr!=null && !"".equals(existKeyStr)){
								System.out.println("기존에 존재하는 데이터 키 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
							}else {
								System.out.println("없는 값 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
								for(Object key:json.keySet()) {
									if(key instanceof String) {
										
										//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
										System.out.println("(=================================================");
										System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
										//(TODO)디버깅 용 코드
										if(key.equals("opengTm")) {
											System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
										}
										CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
										//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
										if(keyMap!=null) {
											System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
											CommandMap val = new CommandMap();
											//참고용 값
											val.put("bid_col_cd", key);
											val.put("id_bid",keyMap.getString("id_bid"));
											val.put("matched_keyword", matchingKeyword);
											val.put("conts", json.get(key));
											inputMap.add(val);
										}else {
											System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
										}
										//   )
										System.out.println("=================================================)");
									}
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}
						}
						//(TODO)키워드 검색 실패시 들어가야 하는 부분 처리
						else if(keywordList.size()>0 && !StrUtil.containsMidString(keywordList,bidNtceNm)) {
							StringBuffer existChkSb = new StringBuffer();
							for(String pkBidColCd:pkBidColCdList) {
								existChkSb.append(json.get(pkBidColCd)+"_");
							}
							String existChkSbStr = existChkSb.substring(0,existChkSb.toString().lastIndexOf("_"));
							String existKeyStr = notMatchRealPkMap.get(existChkSbStr);
							if(existKeyStr!=null && !"".equals(existKeyStr)){
								System.out.println("기존에 존재하는 데이터 키 :"+existChkSbStr);
								continue;
							}
							List<CommandMap> notMatchInputMap = new ArrayList<CommandMap>();
							System.out.println("매칭이 안된 경우의 입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										notMatchInputMap.add(val);
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(notMatchInputMap.size()>0) {
								notMatchInputList.add(notMatchInputMap);
							}
						}else if(keywordList.size()==0) {
							result.put("sucYn", "N");
							throw new Exception("있어야 하는 값이 없음");
						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			//(not match 입력)
			if(!GlobalConst.GLOBAL_MODE_SERVER.equals(SpringPropertiesUtil.getProperty("Globals.server.mode"))) {
				if(notMatchInputList.size()>0) {
					bidSearchService.insertBidNotiOrigin(notMatchInputList,searchCon);
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
				}else {
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
					System.out.print("inputList size = 0");
				}
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				this.insertDailyBatchM(batM);
				int dailyBatNum = this.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				this.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
    }

    /**
     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
     * @param jsonObject
     * @param num_of_rows 
     * @param curr_pg 
     * @param batInfoMap 
     * @return
     */
    private Map<String,Object> updateOpenApiResult(JSONObject jsonObject, Map<String, String> batInfoMap) {
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		String search_date = batInfoMap.get("search_date");
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
//		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			Integer num_of_rows = Integer.parseInt(batInfoMap.get("lines_per_pg"));
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
			List<String> keywordList = new ArrayList<String>();
			List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
			for(CustomMap k:kList) {
				keywordList.add(k.getString("code_nm", ""));
			}
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("id_service", batInfoMap.get("id_service"));
			commandMap.put("search_date", search_date);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			
			// 키값을 가져오는 부분
			List<CustomMap> pkList = bidSearchService.selectBidPkInfoBySysCode(commandMap);
			List<String> pkStrList = new ArrayList<String>();
			List<String> pkBidColCdList = new ArrayList<String>();
			for(CustomMap pkCm:pkList) {
				String pkIdBid = pkCm.getString("id_bid","");
				if(!"".equals(pkIdBid)) {
					pkStrList.add(pkIdBid);
					pkBidColCdList.add(pkCm.getString("bid_col_cd", ""));
				}
			}
			commandMap.put("pkStrList", pkStrList);
			
			//기본키 정보에 해당되는 값을 가져와서 notiKeyMap에 여러개 행으로 된 notiVal값을 Map에 추가하는 부분
			List<CustomMap> notiKeyList = bidSearchService.selectBidNotiKeyList(commandMap);
			List<CustomMap> notMatchNotiKeyList = bidSearchService.selectBidNotiOriginKeyList(commandMap);
			Map<String,Map<String,String>> notiKeyMap = new HashMap<String,Map<String,String>>();
			Map<String,Map<String,String>> notMatchNotiKeyMap = new HashMap<String,Map<String,String>>();
			for(CustomMap pkCm:notiKeyList) {
				String idNoti = pkCm.getString("id_bid_noti","");
				String bidColCd = pkCm.getString("bid_col_cd","");
				String bidCdConts = pkCm.getString("conts","");
				if(notiKeyMap.get(idNoti)==null || "".equals(notiKeyMap.get(idNoti))) {
					Map<String,String> notiValMap = new HashMap<String,String>();
					notiValMap.put(bidColCd, bidCdConts);
					notiKeyMap.put(idNoti, notiValMap);
				}else {
					Map<String,String> notiValMap = notiKeyMap.get(idNoti);
					notiValMap.put(bidColCd, bidCdConts);
				}
			}
			for(CustomMap pkCm:notMatchNotiKeyList) {
				String idNoti = pkCm.getString("id_bid_noti","");
				String bidColCd = pkCm.getString("bid_col_cd","");
				String bidCdConts = pkCm.getString("conts","");
				if(notMatchNotiKeyMap.get(idNoti)==null || "".equals(notMatchNotiKeyMap.get(idNoti))) {
					Map<String,String> notiValMap = new HashMap<String,String>();
					notiValMap.put(bidColCd, bidCdConts);
					notMatchNotiKeyMap.put(idNoti, notiValMap);
				}else {
					Map<String,String> notiValMap = notMatchNotiKeyMap.get(idNoti);
					notiValMap.put(bidColCd, bidCdConts);
				}
			}
			Map<String,String> realPkMap = new HashMap<String,String>();
			for(String idNoti:notiKeyMap.keySet()) {
				Map<String,String> notiValMap = notiKeyMap.get(idNoti);
				StringBuffer notiValSb = new StringBuffer();
				for(String pkBidColCd:pkBidColCdList) {
					notiValSb.append(notiValMap.get(pkBidColCd)+"_");
				}
				String notiValStr = notiValSb.substring(0,notiValSb.toString().lastIndexOf("_"));
				realPkMap.put(notiValStr,"E");
			}
			
			Map<String,String> notMatchRealPkMap = new HashMap<String,String>();
			for(String idNoti:notMatchNotiKeyMap.keySet()) {
				Map<String,String> notiValMap = notMatchNotiKeyMap.get(idNoti);
				StringBuffer notiValSb = new StringBuffer();
				for(String pkBidColCd:pkBidColCdList) {
					notiValSb.append(notiValMap.get(pkBidColCd)+"_");
				}
				String notiValStr = notiValSb.substring(0,notiValSb.toString().lastIndexOf("_"));
				notMatchRealPkMap.put(notiValStr,"E");
			}
			
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)매칭이 안된 값 저장 
			List<List<CommandMap>> notMatchInputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date", search_date);
			searchCon.put("id_service", batInfoMap.get("id_service"));
			searchCon.put("rslt_pg_no", curr_pg);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
							StringBuffer existChkSb = new StringBuffer();
							for(String pkBidColCd:pkBidColCdList) {
								existChkSb.append(json.get(pkBidColCd)+"_");
							}
							String existChkSbStr = existChkSb.substring(0,existChkSb.toString().lastIndexOf("_"));
							String existKeyStr = realPkMap.get(existChkSbStr);
							if(existKeyStr!=null && !"".equals(existKeyStr)){
								System.out.println("기존에 존재하는 데이터 키 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
							}else {
								System.out.println("없는 값 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
								for(Object key:json.keySet()) {
									if(key instanceof String) {
										
										//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
										System.out.println("(=================================================");
										System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
										//(TODO)디버깅 용 코드
										if(key.equals("opengTm")) {
											System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
										}
										CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
										//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
										if(keyMap!=null) {
											System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
											CommandMap val = new CommandMap();
											//참고용 값
											val.put("bid_col_cd", key);
											val.put("id_bid",keyMap.getString("id_bid"));
											val.put("matched_keyword", matchingKeyword);
											val.put("conts", json.get(key));
											inputMap.add(val);
										}else {
											System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
										}
										//   )
										System.out.println("=================================================)");
									}
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}
						}
						//(TODO)키워드 검색 실패시 들어가야 하는 부분 처리
						else if(keywordList.size()>0 && !StrUtil.containsMidString(keywordList,bidNtceNm)) {
							StringBuffer existChkSb = new StringBuffer();
							for(String pkBidColCd:pkBidColCdList) {
								existChkSb.append(json.get(pkBidColCd)+"_");
							}
							String existChkSbStr = existChkSb.substring(0,existChkSb.toString().lastIndexOf("_"));
							String existKeyStr = notMatchRealPkMap.get(existChkSbStr);
							if(existKeyStr!=null && !"".equals(existKeyStr)){
								System.out.println("기존에 존재하는 데이터 키 :"+existChkSbStr);
								continue;
							}
							List<CommandMap> notMatchInputMap = new ArrayList<CommandMap>();
							System.out.println("매칭이 안된 경우의 입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										notMatchInputMap.add(val);
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(notMatchInputMap.size()>0) {
								notMatchInputList.add(notMatchInputMap);
							}
						}else if(keywordList.size()==0) {
							result.put("sucYn", "N");
							throw new Exception("있어야 하는 값이 없음");
						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			//(not match 입력)
			if(!GlobalConst.GLOBAL_MODE_SERVER.equals(SpringPropertiesUtil.getProperty("Globals.server.mode"))) {
				if(notMatchInputList.size()>0) {
					bidSearchService.insertBidNotiOrigin(notMatchInputList,searchCon);
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
				}else {
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
					System.out.print("inputList size = 0");
				}
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				this.insertDailyBatchM(batM);
				int dailyBatNum = this.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				this.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
    }

    public void insertOriginBatchJob(Map<String,String> batInfoMap) throws Exception{
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,Object> batRet = this.insertOriginRestfulInfoByCurl(batInfoMap);
    	boolean rowSuc=false;
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_api_cnt<3) {
    		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    		    	Map<String,Object> batTryRet = this.insertOriginRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
    		        		total_pg = (int)batRet.get("tot_pg");
    		        		rowSuc=true;
    		    			curr_pg++;
    		    	        break;
    		    		}
    		    	}
    		    	try_api_cnt++;
    			}
    	    	log.debug("Batch job start search fail");
    		}else {
            	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
    			rowSuc=true;
    			curr_pg++;
        		total_pg = (int)batRet.get("tot_pg");
    	    	log.debug("Batch job start search start");
    		}
    	}else {
    		throw new Exception("Batch job Fail");
    	}
    	if(rowSuc) {
        	for(int i=curr_pg;i<=total_pg;i++) {
        		System.out.println("현재 "+i+"페이지 검색 시작");
        		batInfoMap.put("curr_pg", String.valueOf(i));
        		this.insertTodayOrds(batInfoMap);
//        		Map<String,Object> insRet=this.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
        	}
        	int totPgCnt = this.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
        	int retCnt = this.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
        	if(totPgCnt==retCnt) {
        		batInfoMap.put("succ_yn", "Y");
        		this.updateSuccYnAtBatchM(batInfoMap);
        	}
    	}else {
    		CommandMap failMap = new CommandMap();
    		failMap.put("search_date",batInfoMap.get("search_date"));
    		failMap.put("id_service", batInfoMap.get("id_service"));
    		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
			failMap.put("succ_yn", "F");
    		this.insertDailyBatchM(failMap);
    	}
    	
    }

	public void updateOriginBatchJob(Map<String,String> batInfoMap) throws Exception {
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,Object> batRet = this.updateOriginRestfulInfoByCurl(batInfoMap);
    	boolean rowSuc=false;
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_api_cnt<3) {
    		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    		    	Map<String,Object> batTryRet = this.updateOriginRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
    		        		total_pg = (int)batRet.get("tot_pg");
    		        		rowSuc=true;
    		    			curr_pg++;
    		    	        break;
    		    		}
    		    	}
    		    	try_api_cnt++;
    			}
    	    	log.debug("Batch job start search fail");
    		}else {
            	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
    			rowSuc=true;
    			curr_pg++;
        		total_pg = (int)batRet.get("tot_pg");
    	    	log.debug("Batch job start search start");
    		}
    	}else {
    		throw new Exception("Batch job Fail");
    	}
    	if(rowSuc) {
        	for(int i=curr_pg;i<=total_pg;i++) {
        		System.out.println("현재 "+i+"페이지 검색 시작");
        		batInfoMap.put("curr_pg", String.valueOf(i));
        		this.updateTodayOrds(batInfoMap);
//        		Map<String,Object> insRet=this.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
        	}
        	int totPgCnt = this.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
        	int retCnt = this.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
        	if(totPgCnt==retCnt) {
        		batInfoMap.put("succ_yn", "Y");
        		this.updateSuccYnAtBatchM(batInfoMap);
        	}
    	}else {
    		CommandMap failMap = new CommandMap();
    		failMap.put("search_date",batInfoMap.get("search_date"));
    		failMap.put("id_service", batInfoMap.get("id_service"));
    		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
			failMap.put("succ_yn", "F");
    		this.insertDailyBatchM(failMap);
    	}
    	
    }

	/**첫번째 성공시도 만 저장하는 메서드
    * restTemplate.getForObject 사용
    * @param search_date
    * @param batInfoMap tring search_date, int curr_pg, int num_of_rows, 
    * @return
    * @throws ParseException
    */
    private Map<String,Object> insertOriginRestfulInfoByCurl(Map<String, String> batInfoMap) throws ParseException{
    	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    String search_date = batInfoMap.get("search_date");
//	    String ed_date = batInfoMap.get("ed_date");
	    
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	               .queryParam(numOfRows, batInfoMap.get("lines_per_pg"))
	               .queryParam(pageNo, batInfoMap.get("curr_pg"))
	               .queryParam(data_type, "json")
	               .queryParam(bidNtceBgnDt, search_date+"0000")
	               .queryParam(bidNtceEndDt, search_date+"2359");
//		               .queryParam(serviceKey, test_key);
	//
//		       String url = builder.toUriString();
//		       System.out.println(url);
	    String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
	    System.out.println(url);
       
   		ProcessBuilder processBuilder = new ProcessBuilder("curl", url);

   		boolean sucYn=false;
   		try {
   			Process process = processBuilder.start();

           // 응답 데이터를 읽어오기 위한 작업
   			InputStream inputStream = process.getInputStream();
   			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
   			StringBuilder resp = new StringBuilder();
           	String line;

           	while ((line = reader.readLine()) != null) {
           		resp.append(line);
           	}

           	reader.close();

           	JSONParser parser = new JSONParser();
           	try {
           		JSONObject respJson = (JSONObject)parser.parse(resp.toString());
           		CommandMap searchBatParam = new CommandMap();
           		searchBatParam.put("search_date", search_date);
               	Map<String,Object> ins_ret = this.insertOriginOpenApiResult(respJson,batInfoMap);
               	if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
               		result.put("sucYn", true);
       	        	result.put("id_batch", ins_ret.get("id_batch"));
               	}else {
               		result.put("sucYn", sucYn);
               	}
       	        result.put("tot_pg", ins_ret.get("tot_pg"));
           	}catch(Exception e) {
           		result.put("sucYn", sucYn);
           	}
   		} catch (IOException e) {
	        result.put("sucYn", sucYn);
//		           e.printStackTrace();
//		           System.out.println("Error executing Curl command");
   		}
		return result;
    }

    /**
     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
     * @param jsonObject
     * @param num_of_rows 
     * @param curr_pg 
     * @param batInfoMap 
     * @return
     */
    private Map<String,Object> insertOriginOpenApiResult(JSONObject jsonObject, Map<String, String> batInfoMap) {
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		String search_date = batInfoMap.get("search_date");
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
//		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			Integer num_of_rows = Integer.parseInt(batInfoMap.get("lines_per_pg"));
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("id_service", batInfoMap.get("id_service"));
			commandMap.put("search_date", search_date);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date", search_date);
			searchCon.put("id_service", batInfoMap.get("id_service"));
			searchCon.put("rslt_pg_no", curr_pg);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
//						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
//							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
//							System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										inputMap.add(val);
									}else {
										System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}else {
								result.put("sucYn", "N");
								throw new Exception("있어야 하는 값이 없음");
							}

//						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				this.insertDailyBatchM(batM);
				int dailyBatNum = this.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				this.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
    }

    /**첫번째 성공시도 만 저장하는 메서드
    * restTemplate.getForObject 사용
    * @param search_date
    * @param batInfoMap tring search_date, int curr_pg, int num_of_rows, 
    * @return
    * @throws ParseException
    */
    private Map<String,Object> updateOriginRestfulInfoByCurl(Map<String, String> batInfoMap) throws ParseException{
    	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    String search_date = batInfoMap.get("search_date");
//	    String ed_date = batInfoMap.get("ed_date");
	    
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	               .queryParam(numOfRows, batInfoMap.get("lines_per_pg"))
	               .queryParam(pageNo, batInfoMap.get("curr_pg"))
	               .queryParam(data_type, "json")
	               .queryParam(bidNtceBgnDt, search_date+"0000")
	               .queryParam(bidNtceEndDt, search_date+"2359");
//		               .queryParam(serviceKey, test_key);
	//
//		       String url = builder.toUriString();
//		       System.out.println(url);
	    String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
	    System.out.println(url);
       
   		ProcessBuilder processBuilder = new ProcessBuilder("curl", url);

   		boolean sucYn=false;
   		try {
   			Process process = processBuilder.start();

           // 응답 데이터를 읽어오기 위한 작업
   			InputStream inputStream = process.getInputStream();
   			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
   			StringBuilder resp = new StringBuilder();
           	String line;

           	while ((line = reader.readLine()) != null) {
           		resp.append(line);
           	}

           	reader.close();

           	JSONParser parser = new JSONParser();
           	try {
           		JSONObject respJson = (JSONObject)parser.parse(resp.toString());
           		CommandMap searchBatParam = new CommandMap();
           		searchBatParam.put("search_date", search_date);
               	Map<String,Object> ins_ret = this.updateOriginOpenApiResult(respJson,batInfoMap);
               	if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
               		result.put("sucYn", true);
       	        	result.put("id_batch", ins_ret.get("id_batch"));
               	}else {
               		result.put("sucYn", sucYn);
               	}
       	        result.put("tot_pg", ins_ret.get("tot_pg"));
           	}catch(Exception e) {
           		result.put("sucYn", sucYn);
           	}
   		} catch (IOException e) {
	        result.put("sucYn", sucYn);
//		           e.printStackTrace();
//		           System.out.println("Error executing Curl command");
   		}
		return result;
    }

    /**
     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
     * @param jsonObject
     * @param num_of_rows 
     * @param curr_pg 
     * @param batInfoMap 
     * @return
     */
    private Map<String,Object> updateOriginOpenApiResult(JSONObject jsonObject, Map<String, String> batInfoMap) {
		Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
		String search_date = batInfoMap.get("search_date");
    	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
//		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			Integer num_of_rows = Integer.parseInt(batInfoMap.get("lines_per_pg"));
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
			List<String> keywordList = new ArrayList<String>();
			List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
			for(CustomMap k:kList) {
				keywordList.add(k.getString("code_nm", ""));
			}
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("id_service", batInfoMap.get("id_service"));
			commandMap.put("search_date", search_date);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			
			// 키값을 가져오는 부분
			List<CustomMap> pkList = bidSearchService.selectBidPkInfoBySysCode(commandMap);
			List<String> pkStrList = new ArrayList<String>();
			List<String> pkBidColCdList = new ArrayList<String>();
			for(CustomMap pkCm:pkList) {
				String pkIdBid = pkCm.getString("id_bid","");
				if(!"".equals(pkIdBid)) {
					pkStrList.add(pkIdBid);
					pkBidColCdList.add(pkCm.getString("bid_col_cd", ""));
				}
			}
			commandMap.put("pkStrList", pkStrList);
			
			//기본키 정보에 해당되는 값을 가져와서 notiKeyMap에 여러개 행으로 된 notiVal값을 Map에 추가하는 부분
			List<CustomMap> notiKeyList = bidSearchService.selectBidNotiKeyList(commandMap);
			Map<String,Map<String,String>> notiKeyMap = new HashMap<String,Map<String,String>>();
			for(CustomMap pkCm:notiKeyList) {
				String idNoti = pkCm.getString("id_bid_noti","");
				String bidColCd = pkCm.getString("bid_col_cd","");
				String bidCdConts = pkCm.getString("conts","");
				if(notiKeyMap.get(idNoti)==null || "".equals(notiKeyMap.get(idNoti))) {
					Map<String,String> notiValMap = new HashMap<String,String>();
					notiValMap.put(bidColCd, bidCdConts);
					notiKeyMap.put(idNoti, notiValMap);
				}else {
					Map<String,String> notiValMap = notiKeyMap.get(idNoti);
					notiValMap.put(bidColCd, bidCdConts);
				}
			}
			Map<String,String> realPkMap = new HashMap<String,String>();
			for(String idNoti:notiKeyMap.keySet()) {
				Map<String,String> notiValMap = notiKeyMap.get(idNoti);
				StringBuffer notiValSb = new StringBuffer();
				for(String pkBidColCd:pkBidColCdList) {
					notiValSb.append(notiValMap.get(pkBidColCd)+"_");
				}
				String notiValStr = notiValSb.substring(0,notiValSb.toString().lastIndexOf("_"));
				realPkMap.put(notiValStr,"E");
			}
			
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date", search_date);
			searchCon.put("id_service", batInfoMap.get("id_service"));
			searchCon.put("rslt_pg_no", curr_pg);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
							StringBuffer existChkSb = new StringBuffer();
							for(String pkBidColCd:pkBidColCdList) {
								existChkSb.append(json.get(pkBidColCd)+"_");
							}
							String existChkSbStr = existChkSb.substring(0,existChkSb.toString().lastIndexOf("_"));
							String existKeyStr = realPkMap.get(existChkSbStr);
							if(existKeyStr!=null && !"".equals(existKeyStr)){
								System.out.println("기존에 존재하는 데이터 키 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
								continue;
							}
							System.out.println("없는 값 :"+existChkSbStr+",매칭된 keyword :"+matchingKeyword);
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										inputMap.add(val);
									}else {
										System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}else {
								result.put("sucYn", "N");
								throw new Exception("있어야 하는 값이 없음");
							}

						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				this.insertDailyBatchM(batM);
				int dailyBatNum = this.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				this.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				this.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
    }

    public void insertBatchJob(Map<String,String> batInfoMap) throws Exception{
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,Object> batRet = this.insertRestfulInfoByCurl(batInfoMap);
    	boolean rowSuc=false;
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_api_cnt<3) {
    		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    		    	Map<String,Object> batTryRet = this.insertRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
    		        		total_pg = (int)batRet.get("tot_pg");
    		        		rowSuc=true;
    		    			curr_pg++;
    		    	        break;
    		    		}
    		    	}
    		    	try_api_cnt++;
    			}
    	    	log.debug("Batch job start search fail");
    		}else {
            	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
    			rowSuc=true;
    			curr_pg++;
        		total_pg = (int)batRet.get("tot_pg");
    	    	log.debug("Batch job start search start");
    		}
    	}else {
    		throw new Exception("Batch job Fail");
    	}
    	if(rowSuc) {
        	for(int i=curr_pg;i<=total_pg;i++) {
        		System.out.println("현재 "+i+"페이지 검색 시작");
        		batInfoMap.put("curr_pg", String.valueOf(i));
        		this.insertTodayOrds(batInfoMap);
//        		Map<String,Object> insRet=this.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
        	}
        	int totPgCnt = this.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
        	int retCnt = this.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
        	if(totPgCnt==retCnt) {
        		batInfoMap.put("succ_yn", "Y");
        		this.updateSuccYnAtBatchM(batInfoMap);
        	}
    	}else {
    		CommandMap failMap = new CommandMap();
    		failMap.put("search_date",batInfoMap.get("search_date"));
    		failMap.put("id_service", batInfoMap.get("id_service"));
    		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
			failMap.put("succ_yn", "F");
    		this.insertDailyBatchM(failMap);
    	}
    	
    }

	public void updateBatchJob(Map<String,String> batInfoMap) throws Exception {
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,Object> batRet = this.updateRestfulInfoByCurl(batInfoMap);
    	boolean rowSuc=false;
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_api_cnt<3) {
    		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    		    	Map<String,Object> batTryRet = this.updateRestfulInfoByCurl(batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
    		        		total_pg = (int)batRet.get("tot_pg");
    		        		rowSuc=true;
    		    			curr_pg++;
    		    	        break;
    		    		}
    		    	}
    		    	try_api_cnt++;
    			}
    	    	log.debug("Batch job start search fail");
    		}else {
            	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
    			rowSuc=true;
    			curr_pg++;
        		total_pg = (int)batRet.get("tot_pg");
    	    	log.debug("Batch job start search start");
    		}
    	}else {
    		throw new Exception("Batch job Fail");
    	}
    	if(rowSuc) {
        	for(int i=curr_pg;i<=total_pg;i++) {
        		System.out.println("현재 "+i+"페이지 검색 시작");
        		batInfoMap.put("curr_pg", String.valueOf(i));
        		this.updateTodayOrds(batInfoMap);
//        		Map<String,Object> insRet=this.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
        	}
        	int totPgCnt = this.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
        	int retCnt = this.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
        	if(totPgCnt==retCnt) {
        		batInfoMap.put("succ_yn", "Y");
        		this.updateSuccYnAtBatchM(batInfoMap);
        	}
    	}else {
    		CommandMap failMap = new CommandMap();
    		failMap.put("search_date",batInfoMap.get("search_date"));
    		failMap.put("id_service", batInfoMap.get("id_service"));
    		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
			failMap.put("succ_yn", "F");
    		this.insertDailyBatchM(failMap);
    	}
    	
    }

}
