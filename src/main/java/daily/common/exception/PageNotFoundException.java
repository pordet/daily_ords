package daily.common.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class PageNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5882425299040052590L;

    @Autowired
    private MessageSource messageSource;
    
    private String message = messageSource.getMessage("error.404", null, LocaleContextHolder.getLocale());

    public PageNotFoundException() {
        super();
    }

    public PageNotFoundException(final String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
