package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface IdentityDAO {

	List<CustomMap> selectBidIdentity(Map<String, Object> map);

	PageList<CustomMap> selectBidIdentityListContents(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectBidNotiColForIdentity(Map<String, Object> map);

	CustomMap getIdentityMap(Map<String, Object> map);

	CustomMap getIdentityRel(Map<String, Object> map);

	List<CustomMap> selectBidNotiColByKeyList(Map<String, Object> map);
}
