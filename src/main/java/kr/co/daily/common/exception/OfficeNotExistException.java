package kr.co.daily.common.exception;

public class OfficeNotExistException extends RuntimeException {

	private static final long serialVersionUID = 2046361217375547416L;

	public OfficeNotExistException(String msg){
        super(msg);
    }       
}
