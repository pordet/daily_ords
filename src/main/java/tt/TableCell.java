package tt;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableCell {
    private int id;
    private int parent_id;
    private int depth;
    private int ord;

    public TableCell(int id, int parent_id, int depth, int ord) {
        this.id = id;
        this.parent_id = parent_id;
        this.depth = depth;
        this.ord = ord;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return parent_id;
    }

    public int getDepth() {
        return depth;
    }

    public int getOrd() {
        return ord;
    }

    public static void main(String[] args) {
        int max_depth = 4;
        List<TableCell> cellList = new ArrayList<>();
        
        cellList.add(new TableCell(1, 0, 1, 1));
        cellList.add(new TableCell(4, 1, 2, 1));
        cellList.add(new TableCell(5, 1, 2, 2));
        cellList.add(new TableCell(6, 4, 3, 1));
        cellList.add(new TableCell(7, 4, 3, 2));
        cellList.add(new TableCell(8, 4, 3, 3));
        cellList.add(new TableCell(9, 6, 4, 1));
        cellList.add(new TableCell(10, 6, 4, 2));
        cellList.add(new TableCell(11, 7, 4, 3));
        cellList.add(new TableCell(12, 7, 4, 4));
        cellList.add(new TableCell(13, 8, 4, 1));

        Map<Integer, List<TableCell>> childMap = new HashMap<>();
        List<TableCell> aloneList = new ArrayList<>();

        for (TableCell cell : cellList) {
            if (!childMap.containsKey(cell.getParentId())) {
                childMap.put(cell.getParentId(), new ArrayList<>());
            }
            childMap.get(cell.getParentId()).add(cell);
        }

        // ID를 기준으로 정렬
 //       cellList.sort(Comparator.comparingInt(TableCell::getId));

        for (TableCell cell : cellList) {
            if (cell.getDepth() == 1) {
                buildAloneListRecursive(cell, childMap, max_depth, aloneList);
            }
        }

        // 결과 출력
        System.out.println("자식을 가지지 않는 셀:");
        for (TableCell cell : aloneList) {
            System.out.println("ID: " + cell.getId() + ", Parent_ID: " + cell.getParentId() + ", Depth: " + cell.getDepth());
        }
    }

    private static void buildAloneListRecursive(TableCell cell, Map<Integer, List<TableCell>> childMap, int maxDepth, List<TableCell> aloneList) {
        if (!hasChildren(cell, childMap, maxDepth)) {
            aloneList.add(cell);
        } else {
            if (childMap.containsKey(cell.getId())) {
                for (TableCell child : childMap.get(cell.getId())) {
                    buildAloneListRecursive(child, childMap, maxDepth, aloneList);
                }
            }
        }
    }

    private static boolean hasChildren(TableCell cell, Map<Integer, List<TableCell>> childMap, int maxDepth) {
        if (childMap.containsKey(cell.getId())) {
            for (TableCell child : childMap.get(cell.getId())) {
                if (child.getDepth() <= maxDepth) {
                    return true;
                }
            }
        }
        return false;
    }
}
