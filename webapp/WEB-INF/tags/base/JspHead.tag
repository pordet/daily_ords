<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="title" required="false" %>
<%@ attribute name="titleCd" required="false" %>
<base:JspConfig />

<c:if test="${empty title and not empty titleCd}">
	<spring:message var="title" code="${titleCd}"/>
</c:if>
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<base href="<c:url value='/'/>"/>
	<link rel="shortcut icon" href="<c:url value='/resource/icon/favicon.ico'/>">
	<sec:authentication  var="user" property="principal" />
<%-- (KGC)
	<sec:authorize access="isAnonymous()"><!-- [${user}]  --></sec:authorize>
	<sec:authorize access="isAuthenticated()"><!-- [[${user.username}]]  --></sec:authorize>
--%>
    <%-- IE9에서 base태그가 작동이 안되어 아래와 같은 보안 처리가 필요하다. --%>
    <!--[if IE 9]>
   		<script src="<c:url value='/asset/js/ie/placeholders.min.js'/>"></script>
		<script>
			(function() { 
				var baseTag = document.getElementsByTagName('base')[0];
				baseTag.href = baseTag.href;
			})();
		</script>
    <![endif]-->	
    <%
//       out.println("aa");
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication(); 
 //      User user = (User) authentication.getPrincipal();
    %>

	<!-- css 부분 -->
    <!-- 일반 공통 CSS 
	-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/bootstrap/css/bootstrap.min.css'/>">
    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/css/bootstrap-datetimepicker.min.css'/>">
    <!-- Data Tables CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/datatables/css/jquery.dataTables.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css'/>">
    <!-- font-awesome CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/font-awesome/css/all.css'/>">
    <!-- traf Icon CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/css/traf_icon.css'/>">
    <!-- traf CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/css/ords.css'/>">
    <!-- traf SVG -->
    <link rel="stylesheet" href="<c:url value='/resource/css/traf_svg.css'/>">
	
<%-- 	<c:if test="${not empty isMain and isMain eq true}"> --%>
<%-- 		<link rel="stylesheet" href="<c:url value='/asset/css/main.css'/>"> --%>
<%-- 	</c:if> --%>
	
	<script src="<c:url value='/resource/plugins/jquery/jquery-1.12.4.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js'/>"></script>

	<!-- 	JS 분분 -->
	<%-- 일반 공통 JS --%>
	<!-- jquery 
	<script src="<c:url value='/resource/js/alert-1.0.0.js'/>"></script>
	<script src="<c:url value='/resource/js/bpopup/0.11.0/jquery.bpopup.min.js'/>"></script>
	-->
	
	<script>
 		var contextPath = "${pageContext.request.contextPath}";
 		var aa = "${user}";
	</script>
	
<%-- 	<script src="<c:url value='/resource/js/pagination/jquery.simplePagination.js'/>" ></script> --%>
<%--     <script src="<c:url value='/resource/js/bootstrap-3.2.0/bootstrap.js'/>"></script> --%>
	
	<jsp:doBody/>
	
</head>
