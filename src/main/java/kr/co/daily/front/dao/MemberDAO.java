package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface MemberDAO {

	CustomMap getCntUserByIdUser(Map<String, Object> map);

	void insertMember(Map<String, Object> map);

	CustomMap getUser(Map<String, Object> map);

	void updatePwd(Map<String, Object> map);

	List<CustomMap> selectUserByNmAndEmail(Map<String, Object> map);	

}
