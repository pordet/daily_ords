package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Repository
public interface BbsDAO {

	public CustomMap getBoardRow(Map<String, Object> map);

	public List<CustomMap> selectBoardData(Map<String, Object> map);

	public PageList<CustomMap> selectBoardRow(Map<String, Object> map, PageBounds pageBounds);

	public CustomMap getBbsM(Map<String, Object> map);

	public List<CustomMap> selectBbsHeaderList(Map<String, Object> map);

	public List<CustomMap> selectBoardAttachFile(Map<String, Object> map);

	public void insertBoardRow(Map<String, Object> map);

	public void insertBoardFile(Map<String, Object> map);

	public void deleteBoardFile(Map<String, Object> map);

	public void deleteBoardData(Map<String, Object> map);

	public void deleteBoardRow(Map<String, Object> map);

	public void insertMetaDataByGroup(List<Map> inInputList);

	public List<CustomMap> selectMetaDataByIdRow(int int1);

	public List<CustomMap> selectBoardMeta(Map<String, Object> map);

	public List<CustomMap> selectBoardMetaAndData(Map<String, Object> map);

	public void modifyMetaRow(Map<String, Object> map);

	public void updateMetaData(Map<String, Object> map);

	public void deleteMetaDataByList(Map<String, Object> map);

	public int getBoardMaxFileNo(Map<String, Object> map);

	public List<CustomMap> selectAttachMetaList(Map<String, Object> map);

	public CustomMap getBoardFile(Map<String, Object> map);

	public List<CustomMap> selectBoardAttachFileIn(Map<String, Object> map);

	public void deleteBoardFileInIdBoardFile(Map<String, Object> map);

	public List<CustomMap> selectBoardShowYnMeta(Map<String, Object> map);

	public void updateViewCnt(Map<String, Object> map);

}
