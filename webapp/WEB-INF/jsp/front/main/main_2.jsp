<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<front:FrontHead title=""/>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
                    <!-- 컨텐츠영역 : 여기에 작성 -->
             	<div class="cont_sec01">
             	<div class="row" style="margin-left:80px;margin-top:120px;">
             	<c:forEach var="cate" items="${cateList}">
		            <a href="<c:url value='/content.do?id_category=${cate.id_category}'/>">
<%-- 		            <div class="cate_${cate.id_category}"> --%>
<!-- 		            </div> -->
		            <img src="<c:url value='/resource/img/cate/${cate.id_category}.jpg'/>" class="cate_${cate.id_category}">
<!-- 		            </div> -->
		            </a>
             	</c:forEach>
				</div>
             	<div class="row" style="margin-left:80px;">
             	<c:forEach var="cate" items="${cateList}">
		            <a href="<c:url value='/content.do?id_category=${cate.id_category}'/>">
<%-- 		            <div class="cate_${cate.id_category}"> --%>
<!-- 		            </div> -->
					<div style="width:200px;">
						            <span style="width:200px;padding-left:65px;">${cate.code_nm}</span>
					</div>
<!-- 		            </div> -->
		            </a>
             	</c:forEach>
				</div>
             	</div>
</front:FrontBody>