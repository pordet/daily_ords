package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import daily.common.vo.CustomMap;

public interface ResourceDAO {
	
	CustomMap getResource(Map<String, Object> map);

	CustomMap getChildResource(Map<String, Object> map);

	int getResourcePattern(Map<String, Object> map);

	CustomMap getResourceIdByIdMenu(Map<String, Object> map);

	CustomMap getResourceIdByParentResource(Map<String, Object> map);
	
	void insertResource(Map<String, Object> map);

	void modifyResource(Map<String, Object> map);

	void modifyResourceByMenuUrl(Map<String, Object> map);
	
	void deleteResource(Map<String, Object> map);

	void deleteResourceByIdRole(Map<String, Object> map);

	List<CustomMap> selectAuthorizedResourceUrlByloginedId(Map<String, Object> map);
	
}
