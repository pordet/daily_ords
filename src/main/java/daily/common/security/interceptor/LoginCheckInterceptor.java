package daily.common.security.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginCheckInterceptor extends HandlerInterceptorAdapter {
	public boolean preHandle(HttpServletRequest req,HttpServletResponse resp,Object handler) throws Exception{
		boolean result = false;
		String webRoot = req.getContextPath();
		String url = req.getRequestURI();
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth == null) {
				if(isAjaxRequest(req)) {
					resp.sendError(400);
					return false;
				}else {
					if(isClientReq(url)) {
						resp.sendRedirect(webRoot+"/loginPage.do");
					}else {
						resp.sendRedirect(webRoot+"/loginPage.do");
					}
				}
			}
		}catch(Exception e) {
			return false;
		}
		return result;
	}

	private boolean isAjaxRequest(HttpServletRequest req) {
		String header = req.getHeader("AJAX");
		if("true".equals(header)) {
			return true;
		}else {
			return false;
		}
	}
	
	private boolean isClientReq(String url) {
		if(url.contains("//applicant") || url.contains("//crsApplicant")) {
			return true;
		}else {
			return false;
		}
	}
}
