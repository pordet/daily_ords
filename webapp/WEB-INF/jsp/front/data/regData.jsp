<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="data" tagdir="/WEB-INF/tags/data" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<data:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
	<c:choose>
		<c:when test="${!is_guest}">
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_draw.js'/>"></script>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_register.js'/>"></script>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_input.js'/>"></script>
		</c:when>
		<c:otherwise>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/guest_data_input.js'/>"></script>
		</c:otherwise>
	</c:choose>
</data:DataHead>
<data:DataAccordionBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:if test='${param.id_master eq 1}'>
					<span>공지사항</span>
				</c:if>
				<c:if test='${param.id_master eq 2}'>
					<span>글로벌 교통 연구보고서</span>
				</c:if>
			</div>

			<form id="frm" name="frm" method="get" onsubmit="return false;" enctype="multipart/form-data">
				<input type="hidden" name="attach_yn" value="${master.attach_yn}">
				<input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">

				<input type="hidden" name="id_master" value="${master.id_master}">
				<input type="hidden" name="id_board_master" id="id_board_master" value="${master.id_board_master}">
				<input type="hidden" name="id_parent_board" id="id_parent_board">
				<input type="hidden" name="id_board" id="id_board" value="${param.id_board}">
				<input type="hidden" name="is_insert" id="is_insert" value="${param.is_insert}">
				<input type="hidden" name="id_kind" id="id_kind" value="${param.id_kind}">
				<input type="hidden" name="is_more" id="is_more" value="${param.is_more}">
				<input type="hidden" name="more_page_rows" id="more_page_rows" value="${param.more_page_rows}">
				<input type="hidden" name="id_board_file" id="id_board_file">
				<input type="hidden" name="page" id="page" value="${param.page}">
				<input type="hidden" name="rows" id="rows" value="${param.rows}">
				<input type="hidden" name="ord" id="ord" value="${param.ord}">
				<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
				<c:if test="${!is_guest}">
					<div class="board-content">

						<div class="board-content-list-group">
							<table>
								<colgroup>
									<col width="200px">
									<col>
								</colgroup>
								<tbody>
									<c:if test="${not empty param.id_board}">
										<tr>
											<td>작성자</td>
											<td>${map.reg_user_nm }</td>
										</tr>
										<tr>
											<td>작성일</td>
											<td>${map.reg_date}</td>
										</tr>
									</c:if>
									<tr>
										<td>제목</td>
										<td class="board-input-row">
											<input type="text" name="title" id="title" value="${map.title}">
										</td>
									</tr>
									<tr>
										<td>게시판</td>
										<td class="board-input-row">
											<div class="board-select-wrap">
												<select title="상위게시판" name="parent_ord_str" id="sel_parent_board" onchange="changeRegData(this);">
													<c:forEach var="code" items="${rootList}">
														<option value="${code.id_board_master}" ${(id_parent_board eq code.id_board_master? ' selected':'')}>${code.board_nm}</option>
													</c:forEach>
												</select>
												<span> > </span>
												<select title="하위게시판" name="child_ord_str" id="sel_child_board" onchange="changeRegChildData(this);">
													<c:forEach var="child" items="${childList}">
														<option value="${child.id_board_master}" ${(id_parent_board eq child.id_board_master? ' selected':'')}>${child.board_nm}</option>
													</c:forEach>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>키워드</td>
										<td class="board-input-row">
											<input type="text" name="keyword" id="keyword" value="${map.keyword}">
										</td>
									</tr>
									<tr>
										<td>첨부파일</td>
										<td class="board-input-row">
				                        	<div class="file-input-group">
												<input type="text" disabled>
												<button type="button" class="btn-primary" onclick="javascript:fileAttachBtn();">
													<span class="glyphicon glyphicon-paperclip"></span>
													첨부
												</button>
												<input type="file" class="file-input-hidden" onchange="javascript:addNewFile(this);"/>
											</div>
					                        <div class="board-content-list-fileset">
					                        	<div id="attachedFiles"></div>
					                        	<div id="newFiles"></div>
					                        </div>
										</td>
									</tr>
									<tr>
										<td>동영상 첨부</td>
										<td class="board-input-row">
				                        	<div class="videoInputMaster file-input-group">
				                        		<div style="display: flex;">
													<input class="videoWidthVal" type="number" placeholder="Width">
													<input class="videoHeightVal" type="number" placeholder="Height">
				                        		</div>
												<input class="videoUrlString" type="text" placeholder="Please input a video url. (Youtube only)">
												<button type="button" class="btn-primary" onclick="javascript:addVideoStringBtn();">
													<span class="glyphicon glyphicon-plus"></span>
													추가
												</button>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="board-content-comment-area">
							<textarea name="comment" id="comment" rows="8" placeholder="내용을 입력하세요.">${map.comment}</textarea>
						</div>
					</div>
					<!-- button -->
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<c:if test="${not empty param.id_board}">
								<button type="button" class="btn-danger" onclick="confirmOpen('삭제하시겠습니까?','delData');">삭제</button>
							</c:if>
							<button type="button" class="btn-primary" onclick="confirmOpen('저장하시겠습니까?','register');">저장</button>
						</div>
					</div>
				</c:if>	
				<c:if test="${is_guest}">
					<!--
					<div class="wrong-approach-wrap">
						<div class="wrong-approach-content">
							<div>잘못된 접근입니다.</div>
							<div class="wrong-approach-btn">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록으로</button>
							</div>
						</div>
					</div>
					-->
				</c:if>
			</form>
			
		</div>
	</div>
</data:DataAccordionBody>
<c:if test="${!is_guest}">
	<script>
		var fList = JSON.parse('${addFileList}');
		var newList = [];
		createNewList(fList);
		displayFileList();
	</script>
</c:if>
<script>



</script>
</html>