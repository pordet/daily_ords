package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Repository
public interface BidSearchDAO {

	List<CustomMap> selectBidNotiM(Map<String, Object> map);

	List<CustomMap> selectBidNotiMForUsing(Map<String, Object> map);	

	List<CustomMap> selectBidNotiCol(Map<String, Object> map);

	List<CustomMap> selectBidNotiList(Map<String, Object> map);

	void insertBidNoti(Map<String, Object> map);

	void insertBidNotiColByGroup(List<Map> inInputList);

	List<CustomMap> selectBidSearchKeywordList(Map<String, Object> map);

	PageList<CustomMap> selectBidNotiListContents(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectBidNotiColForSearch(List<Integer> rawKey);

	List<CustomMap> selectBidNotiMUsingBySysCd(Map<String, Object> map);

	void updateBidServiceInputState(Map<String, Object> map);

	List<CustomMap> selectBidPkInfoBySysCode(Map<String, Object> map);

	List<CustomMap> selectBidNotiKeyList(Map<String, Object> map);

	void deleteBidNotiCol(Map<String, Object> map);

	void deleteBidNoti(Map<String, Object> map);

	void insertBidNotiColOriginByGroup(List<Map> inInputList);

	void insertBidNotiOrigin(Map<String, Object> map);

	List<CustomMap> selectBidNotiOriginKeyList(Map<String, Object> map);

	PageList<CustomMap> selectBidNotiOriginListContents(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectBidNotiColOriginForSearch(List<Integer> rawKey);

}
