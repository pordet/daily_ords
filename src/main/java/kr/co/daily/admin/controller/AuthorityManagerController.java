package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.security.ReloadableFilterInvocationSecurityMetadataSource;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.AuthorityManageService;
import kr.co.daily.admin.service.MenuManageService;
import kr.co.daily.admin.service.UserManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class AuthorityManagerController extends AbstractControllerBase {

	private Logger log = LoggerFactory.getLogger(AuthorityManagerController.class);
	
	@Autowired private AuthorityManageService service;
	@Autowired private MenuManageService mnuService;
	
	@Resource(name="reloadableFilterInvocationSecurityMetadataSource") 
	ReloadableFilterInvocationSecurityMetadataSource securitySrc;
	
	@RequestMapping("/adm/auth/doubleCheckUrlAjax.do")
	public String doubleCheckUrlAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			int cnt = service.getResourcePattern(commandMap);
			model.addAttribute("cnt", cnt);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/auth/resourceCheckAjax.do")
	public String resourceCheckAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			int cnt = service.getResourcePattern(commandMap);
			model.addAttribute("cnt",cnt);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/auth/permissionList.do")
	public String permissionList(CommandMap commandMap, ModelMap model) throws Exception {
	    commandMap = this.addParamByLoginInfo(commandMap);
		PageList<CustomMap> resultList = service.selectResourceAndRole(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/permissionList";
	}

	@RequestMapping("/adm/auth/deletePermissionList.do")
		public String deletePermissionList(CommandMap commandMap, ModelMap model) throws Exception {
			int ret = GlobalConst.ACT_SUCCESS;
			try {
				List<String> codelist = new ArrayList<String>();
				if(commandMap.get("del_code") instanceof String) {
					codelist.add(commandMap.getString("del_code",""));
					commandMap.put("del_code_list", codelist);
				}else {
					String[] delList = (String[])commandMap.get("del_code");
					if(delList!=null) {
						for(Object obj:delList) {
							codelist.add(String.valueOf(obj));
						}
						commandMap.put("del_code_list", codelist);
					}
				}
				service.deleteResourceRole(commandMap);
			}catch(Exception e) {
				ret = GlobalConst.ACT_FAIL;
			}
	//		commandMap.put("code_group", GlobalConst.CG_LANGU);
			PageList<CustomMap> resultList = service.selectResourceAndRole(commandMap);
			putModelListPage(resultList,model);
			model.addAttribute("act_type","D");
			model.addAttribute("ret",ret);
			return "adm/permissionList";
		}

	@RequestMapping("/adm/auth/regResourceRole.do")
	public String regResourceRole(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = this.addParamByLoginInfo(commandMap);
			CustomMap map = service.getResourceRole(commandMap);
			List<CustomMap> roleList = service.selectRoleList(commandMap);
	//		if(commandMap.getString("", intVal)
	//		CustomMap map = service.getResourceRole(commandMap);
			if(map==null) {
				List<CustomMap> menuList = mnuService.selectUnregisterMenuList(commandMap);
				model.addAttribute("menuList",menuList);
			}else {
					List<CustomMap> menuList = new ArrayList<CustomMap>();
					if(!"url".equals(map.getString("resource_type"))) {
						CommandMap pmap = CollectionUtil.getCommandMapByCustomMap(map);
						CustomMap menu = mnuService.getMenuByResourcePattern(pmap);
						menuList.add(menu);
					}
					List<CustomMap> unmenuList = mnuService.selectUnregisterMenuList(commandMap);
					menuList.addAll(unmenuList);
					model.addAttribute("menuList",menuList);
			}
			model.addAttribute("map",map);
			model.addAttribute("roleList",roleList);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "adm/reg/regResourceRole";
	}

	@RequestMapping("/adm/auth/selectUnregisterMenuAjax.do")
	public String selectUnregisterMenuAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<CustomMap> menuList = mnuService.selectUnregisterMenuList(commandMap);
			model.addAttribute("menuList",menuList);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/insertResourceRoleAjax.do")
	public String insertResourceRole(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			CommandMap newKey = service.insertResourceRole(commandMap);
			model.addAttribute("id_resource",newKey.getString("id_resource",""));
			model.addAttribute("id_resource_role",newKey.getString("id_resource_role",""));
			securitySrc.reload();
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/modifyResourceRoleAjax.do")
	public String modifyResourceRole(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			service.modifyResourceRole(commandMap);
			securitySrc.reload();
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/deleteResourceRoleAjax.do")
	public String deleteResourceRoleAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			List<String> codelist = new ArrayList<String>();
			codelist.add(commandMap.getString("id_resource",""));
			commandMap.put("del_code_list", codelist);
			service.deleteResourceRole(commandMap);
			securitySrc.reload();
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/roleList.do")
	public String roleList(CommandMap commandMap, ModelMap model) throws Exception {
		PageList<CustomMap> resultList = service.selectUserRoleList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/roleList";
	}

	@RequestMapping("/adm/auth/regRole.do")
	public String regRole(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = service.getUserRole(commandMap);
		model.addAttribute("map",map);
		return "/adm/reg/regRole";
	}

	@RequestMapping("/adm/auth/insertRoleAjax.do")
	public String insertRole(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			service.insertRole(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/modifyRoleAjax.do")
	public String modifyRole(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
	    commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				service.modifyRole(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

	@RequestMapping("/adm/auth/deleteRoleList.do")
		public String deleteRoleList(CommandMap commandMap, ModelMap model) throws Exception {
			log.info("commandMap");
			log.info(commandMap.toString());
			int ret = GlobalConst.ACT_SUCCESS;
			try {
				List<String> codelist = new ArrayList<String>();
				if(commandMap.get("cb_id_role") instanceof String) {
					codelist.add(commandMap.getString("cb_id_role",""));
					commandMap.put("del_code_list", codelist);
				}else {
					String[] delList = (String[])commandMap.get("cb_id_role");
					if(delList!=null) {
						for(Object obj:delList) {
							codelist.add(String.valueOf(obj));
						}
						commandMap.put("del_code_list", codelist);
					}
				}
				service.deleteRoleList(commandMap);
			}catch(Exception e) {
				ret = GlobalConst.ACT_FAIL;
			}
	//		commandMap.put("code_group", GlobalConst.CG_LANGU);
	//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
	//		putModelListPage(resultList,model);
			model.addAttribute("ret",ret);
			model.addAttribute("act_type","D");
			model.addAttribute("id_menu",commandMap.getString("id_menu",""));
			return "redirect:/adm/auth/roleList.do";
		}

	@RequestMapping("/adm/auth/deleteRoleAjax.do")
		public String deleteRoleAjax(CommandMap commandMap, ModelMap model) throws Exception {
			log.info("commandMap");
			log.info(commandMap.toString());
			int ret = GlobalConst.ACT_SUCCESS;
	        commandMap = addParamByLoginInfo(commandMap);
			try {
		        if(commandMap.getInt("login_usr_seq",0)==0) {
					ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
		        }else {
					List<String> codelist = new ArrayList<String>();
					codelist.add(commandMap.getString("id_role",""));
					commandMap.put("del_code_list", codelist);
					if("menu".equals(commandMap.getString("resource_type", ""))) {
					}
					service.deleteRoleList(commandMap);
		        }
			}catch(Exception e) {
				ret = GlobalConst.ACT_FAIL;
			}
	//		commandMap.put("code_group", GlobalConst.CG_LANGU);
	//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
	//		putModelListPage(resultList,model);
			model.addAttribute("ret",ret);
			model.addAttribute("act_type","D");
			model.addAttribute("id_menu",commandMap.getString("id_menu",""));
			return "redirect:/adm/auth/roleList.do";
		}

}
