<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ attribute name="currLocale" required="true" %>
<%@ attribute name="title" required="false" %>
<commc:MenuTag user_seq="${id_user}" locale="${currLocale}" listName="menuList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

	<div class="header-group">
		<div class="header-group-inner">
			<div class="main-logo-area">
		<sec:authorize access="isAuthenticated()"> <!-- 인증된 사용자만 역할 목록 출력 -->
			<sec:authentication property="principal.authorities" var="userAuthorities" />
		    	<div class="_logo">
					<a href="<c:url value='/main.do' />" >
						<img src="<c:url value='/resource/img/logo.png'/>" >
					</a>
				</div>
				<div class="_logo_sm">
					<a href="<c:url value='/main.do' />" >
						<img src="<c:url value='/resource/img/logo_sm.png'/>" >
					</a>
				</div>
		</sec:authorize>
		<sec:authorize access="!isAuthenticated()"> <!-- 인증된 사용자만 역할 목록 출력 -->
		    	<div class="_logo">
					<a href="<c:url value='/' />" >
						<img src="<c:url value='/resource/img/logo.png'/>" >
					</a>
				</div>
				<div class="_logo_sm">
					<a href="<c:url value='/' />" >
						<img src="<c:url value='/resource/img/logo_sm.png'/>" >
					</a>
				</div>
		</sec:authorize>
			</div>
			<div class="header-search-area">
				<div>
					<input id="search" type="text" class="main-search-input" value="${(not empty paramMap.search_keyword ?paramMap.search_keyword:'')}">
					<button id="searchBtn" type="button" class="btn-blank main-search-btn" title="검색">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</div>
			</div>
			<div class="header-user-area">
				<div class="main-user-area-inner">
					<c:choose>
						<c:when test="${not empty userinfo}">
							<div id="login_div">
								<div class="top-log">
									<span>${userinfo.name} 님</span>
									<button type="button" class="btn-default" onclick="javascript:goAdmin('${userinfo.id}');">관리</button>
									<button type="button" class="btn-info" onclick="javascript:logout();">
										<span class="glyphicon glyphicon-off"></span>
									</button>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div id="login_div">
								<ul class="util">
									<li><a href="<c:url value='/join.do'/>">회원가입</a></li>
									<li><a href="<c:url value='/loginPage.do'/>">로그인</a></li>
								</ul>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-group">
		<div class="menu-inner">
			<div class="menu-left">
				<ul class="tree-menu-ul">
				<c:set var="substringToCheck" value="?"/>
				<c:forEach var="item" items="${menuList}" varStatus="status">
					<li>
						<c:set var="menu_url" value="${item.menu_url}"/>
						<c:choose>
							<c:when test="${fn:contains(menu_url, substringToCheck)}">
								<c:set var="menu_url" value="${item.menu_url}&id_menu=${item.id_menu}"/>
							</c:when>
							<c:otherwise>
								<c:set var="menu_url" value="${item.menu_url}?id_menu=${item.id_menu}"/>
							</c:otherwise>
						</c:choose>
						<a href="<c:url value='${menu_url}'/>">
							${item.menu_nm}</a>
						<c:if test="${not empty item.child_cnt}">
<%-- 							<div class="collapse" id="coll_${item.id_menu}" style="list-style-type : none;position: absolute;height : max-content;width : max-content;background-color: black;"> --%>
							<ul class="sub-menu">
								<c:forEach var="subMenu" items="${item.child}" varStatus="i">
									<c:set var="sub_menu_url" value="${subMenu.menu_url}"/>
									<c:choose>
										<c:when test="${fn:contains(sub_menu_url, substringToCheck)}">
											<c:set var="sub_menu_url" value="${subMenu.menu_url}&id_menu=${subMenu.id_menu}"/>
										</c:when>
										<c:otherwise>
											<c:set var="sub_menu_url" value="${subMenu.menu_url}?id_menu=${subMenu.id_menu}"/>
										</c:otherwise>
									</c:choose>
									<li>
										<a href="<c:url value='${sub_menu_url}'/>"
										>
<%-- 											class="${(param.id_menu eq subMenu.id_menu ?'active':'')}"> --%>
<%-- 												<span>${subMenu.menu_nm}</span>  --%>
											${subMenu.menu_nm}
										</a>
									</li>
								</c:forEach>
							</ul>
<!-- 							</div> -->
						</c:if>
					</li>
				</c:forEach>
				</ul>
			</div>
			<div class="menu-right">
				<ul>
					<li>
						<a href="<c:url value='http://211.229.231.45:9090/evproject'/>" target="new">
							<div>EV Project</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-background-group">
		<img style="position:absolute;pointer-events:none;" src="<c:url value='/resource/img/global_back.png'/>" >
		<div class="main-background-border">
			<div class="main-background-inner">
				<div class="main-background-text-area">
					<div class="main-upper-text">미래 모빌리티 전환을 선도하다</div>
					<div>
						<div class="main-lower-text">Global Transportation Database Integrated</div>
						<div class="main-lower-text">Management System</div>
					</div>
				</div>
			</div>
			<div class="main-broad-line"></div>
		</div>
		
	</div>
	
<script>
document.querySelector("#search").addEventListener("keyup", function(event){
	var inputWord = event.target.value;
	var inputElement = document.querySelector("#search");
	var reg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
	if(reg.test(inputWord)) {
		inputWord = inputWord.replace(reg,"");
	}
	inputElement.value = inputWord;
	if(window.event.keyCode == 13){searchAction(inputElement)};
})
document.querySelector("#searchBtn").addEventListener("click", function(event) {
	searchAction(document.querySelector("#search"));
})
// 통합검색 코드 삽입부
function goAdmin(id_user){
	location.href=contextPath+"/adm/goAdminMainPage.do?id_user="+id_user;
}
function goLogin(){
	location.href=contextPath+"/loginPage.do";
}
function logout(){
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "Post");  //Post 방식
    form.setAttribute("action", "<c:url value='/logout.do'/>"); //요청 보낼 주소
    document.body.appendChild(form);
    form.submit();
}
function searchAction(inputElement) {
	var searchWord = inputElement.value;
	console.log(searchWord.length);
	searchWord = searchWord.trim();
	searchWord = searchWord.slice(0,15);
	inputElement.value = searchWord;
	
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "Post");  //Post 방식
    form.setAttribute("action", "<c:url value='/search.do?search_keyword="+searchWord+"'/>"); //요청 보낼 주소
    document.body.appendChild(form);
    form.submit();
}
$(".tree-menu-ul > li").mouseenter(function(){
    $(this).find(".sub-menu").show();
});
// $('.tree-menu>li').hover(
//      function() {
//        $(this).find('.sub-menu').css('display', 'block');
//      },
//      function() {
//        //$(this).find('.sub-menu').css('display', 'none');
//      }
// );
$(".tree-menu-ul > li, .sub-menu").mouseleave(function(){
//     $(this).find(".sub-menu").stop().slideUp();
 	$(this).closest('.tree-menu-ul').find('.sub-menu').hide();
//$(this).hide();
});
</script>