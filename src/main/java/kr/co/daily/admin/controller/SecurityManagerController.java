package kr.co.daily.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.util.UrlPathHelper;

import daily.common.base.AbstractControllerBase;
import daily.common.util.SpringApplicationContext;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.dao.CommonDAOImpl;
import kr.co.daily.domain.MenuVO;

@Controller
public class SecurityManagerController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;

	@RequestMapping("/user/initPath.do")
	public void loginPage(HttpServletRequest req,
			HttpServletResponse resp,CommandMap param, Model model) throws Exception {
		UrlPathHelper urlPathHelper = new UrlPathHelper(); 
		String url = urlPathHelper.getOriginatingRequestUri(req);
		String context = urlPathHelper.getOriginatingContextPath(req);
	    if(!context.equals("")) {
	    	url = url.substring(context.length());
	    }
//		List<MenuVO> list = null;
		CustomMap map = getLoginInfo();
		CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
		MenuVO vo = new MenuVO();
		vo.setId_user(map.getString("id"));
		MenuVO menu = dao.getMainMenuByGroupId(vo);
		String URL = menu.getMenu_url()+"?id_menu="+menu.getId_menu();
		resp.sendRedirect(URL);
	}
}
