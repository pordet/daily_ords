<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="title" required="false" %>
<%@ attribute name="titleCd" required="false" %>
<front:FrontConfig />

<c:if test="${empty title and not empty titleCd}">
	<spring:message var="title" code="${titleCd}"/>
</c:if>
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<base href="<c:url value='/'/>" />
	<link rel="shortcut icon" href="<c:url value='/resource/icon/favicon.ico'/>">
	<sec:authentication  var="user" property="principal" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css'/>">
    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/css/bootstrap-datetimepicker.min.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css'/>">
    <!-- font-awesome CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/plugins/font-awesome/css/all.css'/>">
    
    <link rel="stylesheet" href="<c:url value='/resource/css/ords.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/css/modal.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/css/table.css'/>">
	
	<script src="<c:url value='/resource/plugins/jquery-3.3.1.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js'/>"></script>
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"></script> -->
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.4.0/backbone-min.js"></script> -->
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/jointjs/3.4.1/joint.min.js"></script> -->
    
	<script>
 		var contextPath = "${pageContext.request.contextPath}";
	</script>
	
	<jsp:doBody/>
</head>
