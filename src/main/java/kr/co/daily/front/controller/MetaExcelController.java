package kr.co.daily.front.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.excel.exception.ExcelDataException;
import daily.common.util.CollectionUtil;
import daily.common.util.ExcelCellUtil;
import daily.common.util.FileUtil;
import daily.common.util.MetaUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.MetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.MetaExcelService;
import kr.co.daily.front.service.MetaService;
import tt.TableCell;

@Controller
public class MetaExcelController extends AbstractControllerBase {
	
	@Autowired ServletContext context; 
	@Autowired
	private MetaExcelService service;
	@Autowired
	private MetaService metaService;
	@Autowired
	private MetaManageService metaManageService;
	
	private static String FILE_DOWNLOAD_PATH;
	private static String FILE_UPLOAD_PATH;
    @Value("#{global['meta.excel.down']}")
	public void setFileDownloadPath(String fileUploadPath) {
    	FILE_DOWNLOAD_PATH = fileUploadPath;
	}
	
    @Value("#{global['meta.excel.upload']}")
	public void setFileUploadPath(String fileUploadPath) {
    	FILE_UPLOAD_PATH = fileUploadPath;
	}
	
	@RequestMapping(value = "/front/meta/downloadAjax.do")
	public String downloadAjax(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res, ModelMap model) throws Exception {

	    try {
			param.put("meta_m_type", GlobalConst.META_M_TYPE_META);
			CustomMap map = service.getMetaExcel(param);
			String filePath = FILE_DOWNLOAD_PATH; // 파일 경로 설정

		    if (!"".equals(map.getString("excel_path", "").trim())) {
		        filePath += map.getString("excel_path", "").trim();
		    }

		    // 파일 객체 생성
		    File file = new File(filePath, map.getString("excel_file_nm", "") + ".xlsx");
		    String downloadFileName = map.getString("excel_file_nm", "") + "_" + StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS") + ".xlsx";

		 // 엑셀파일 읽기 및 Base64 인코딩
	        FileInputStream fis = new FileInputStream(file);
	        byte[] fileBytes = IOUtils.toByteArray(fis);
	        String base64EncodedFileContent = Base64.getEncoder().encodeToString(fileBytes);

		    fis.close();
		    System.out.println("원본 파일명: " + file.getName());
		    System.out.println("다운로드 파일명: " + downloadFileName);
		    model.addAttribute("fileNm", downloadFileName);
		    model.addAttribute("content", base64EncodedFileContent);
	    }catch(Exception e) {
	    	// 다운로드 중 오류가 발생하면 오류 메시지를 출력하고 예외를 처리합니다.
	        e.printStackTrace();
	        res.setContentType("text/plain");
	        res.getWriter().write("파일 다운로드 중 오류가 발생했습니다.");
	    }
	    return "jsonView";
	}
	@RequestMapping(value = "/front/meta/downloadAjax1.do")
	public String downloadAjax1(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res, ModelMap model) throws Exception {

	    try {
			param.put("meta_m_type", GlobalConst.META_M_TYPE_META);
			CustomMap map = service.getMetaExcel(param);
			String filePath = FILE_DOWNLOAD_PATH; // 파일 경로 설정

		    if (!"".equals(map.getString("excel_path", "").trim())) {
		        filePath += map.getString("excel_path", "").trim();
		    }

		    // 파일 객체 생성
		    File file = new File(filePath, map.getString("excel_file_nm", "") + ".xlsx");

		    String downloadFileName = map.getString("excel_file_nm", "") + "_" + StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS") + ".xlsx";
		    // 파일 다운로드 설정
		    res.setContentLength((int) file.length());

		    // 파일 스트림 전송
		    FileInputStream fis = new FileInputStream(file);
		    Workbook workbook = new XSSFWorkbook(fis);
		    OutputStream os = res.getOutputStream();
		    res.setContentType("application/octet-stream");
		    res.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);
		    workbook.write(os);
		    os.flush();
		    os.close();
		    workbook.close();
		    fis.close();
		    System.out.println("원본 파일명: " + file.getName());
		    System.out.println("다운로드 파일명: " + downloadFileName);
		    model.addAttribute("fileUrl", file.getAbsolutePath());
	    }catch(Exception e) {
	    	// 다운로드 중 오류가 발생하면 오류 메시지를 출력하고 예외를 처리합니다.
	        e.printStackTrace();
	        res.setContentType("text/plain");
	        res.getWriter().write("파일 다운로드 중 오류가 발생했습니다.");
	    }
	    return "jsonView";
	}
	@RequestMapping(value = "/front/meta/download.do")
	public ModelAndView callDownload(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res) throws Exception {

//		param.put("fileId", fileId);
//		CustomMap map = historyService.getData(param);
		param.put("meta_m_type", GlobalConst.META_M_TYPE_META);
		CustomMap map = service.getMetaExcel(param);
		String filePath = FILE_DOWNLOAD_PATH; // 파일 경로 설정
	    if (!"".equals(map.getString("excel_path", "").trim())) {
	        filePath += map.getString("excel_path", "").trim();
	    }


	    String downloadFileName = map.getString("excel_file_nm", "") + "_" + StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS") + ".xlsx";
		File downloadFile = new File(filePath, downloadFileName);
	    // 파일 객체 생성
	    File srcFile = new File(filePath, map.getString("excel_file_nm", "") + ".xlsx");
//	    File downloadFile = new File(filePath, downloadFileName);
	    System.out.println("원본 파일명: " + srcFile.getName());
	    System.out.println("다운로드 파일명: " + downloadFileName);
	       try {
	            // 파일 복사 수행
	    	   FileCopyUtils.copy(srcFile,downloadFile);

	            System.out.println("파일 복사가 완료되었습니다.");
	        } catch (IOException e) {
	            System.err.println("파일 복사 중 오류가 발생했습니다.");
		        throw new IOException("File can't read(File not found)");
	        }		
	    if(!downloadFile.canRead()){
	        throw new Exception("File can't read(File not found)");
	    }
	    return new ModelAndView("fileDownloadView", "downloadFile",downloadFile);

	}
	@SuppressWarnings("resource")
	@RequestMapping(value = "/front/meta/uploadAjax.do")
	public String uploadExcel(CommandMap param,@RequestParam("excel_file") MultipartFile m_file,HttpServletRequest req, 
            HttpServletResponse res, ModelMap model) throws Exception {
            // 업로드된 Excel 파일을 읽기
		int ret = GlobalConst.ACT_SUCCESS;
		try{ 
			File file = FileUtil.convertMultipartFileToTempFile(m_file);
	         // 파일 이름 가져오기
            String fileName = file.getName();

            // 파일 크기 가져오기 (바이트 단위)
            long fileSize = file.length();

            // 파일 이름과 크기 출력
            System.out.println("파일 이름: " + fileName);
            System.out.println("파일 크기 (바이트): " + fileSize);
            FileInputStream fis = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(fis);
            // depth별로 시작하는 각각의 id_meta_m 기준으로 id_meta 값이니 사용불가
			Map<Integer,Integer> startMetaAtDepthMap = new HashMap<>();
         // 첫 번째 시트를 선택 (0부터 시작)
            Sheet sheet = workbook.getSheetAt(0);
//            param.put("id_meta_m", 7);
    		param.put("meta_m_type", GlobalConst.META_M_TYPE_META);
    		CustomMap metaM = metaManageService.getMetaM(param);
    		int excel_data_cell_st_num = metaM.getInt("excel_data_cell_st_num", 0);
    		int max_depth = service.getMaxDepthMetaByKey(param);
    		List<CustomMap> ordList = service.selectMetaOrderListForExcel(param);
    		List<Integer> calMetaIdList = new ArrayList<>();
    		Map<Integer,CustomMap> calColMap = new HashMap<>();
    		List<CustomMap> aloneList = MetaUtil.buildAloneList(ordList, max_depth,"id_parent_meta","id_meta","meta_depth");
    		List<String> showList = new ArrayList<>();
    		showList.add("id_meta");
    		showList.add("meta_cd");
    		showList.add("meta_nm");
    		MetaUtil.printCustomList(aloneList,showList);
    		//뎁스별 aloneList를 저장하는 맵
			Map<Integer,List<CustomMap>> aloneListMap = new HashMap<>();
			//DEPTH,START_CELL_INDEX 값을 저장
			Map<Integer,Integer> startCellIndexAtDepthMap = new HashMap<>();
			Map<Integer,Integer> idMetaMAtDepth = new HashMap<>();
    		if(aloneList ==null || aloneList.size()==0) {
    			throw new Exception("엑셀 헤더 생성 실패");
    		}
    		//(TODO)추후에 1번째 값을 DB에서 가져오게 변경 필요
    		startCellIndexAtDepthMap.put(1, 1);
    		startMetaAtDepthMap.put(1, aloneList.get(0).getInt("id_meta",0));
    		aloneListMap.put(1, aloneList);
    		idMetaMAtDepth.put(1, param.getInt("id_meta_m"));
    		//열을 저장하는 map
    		Map<Integer,CustomMap> ordColMap = new HashMap<>();
    		//aloneList 결과 테스트
    		int ord_col_cnt=1;
    		for(CustomMap o:aloneList) {
    			int al_meta_m = o.getInt("id_meta", 0);
    			if(al_meta_m>0) {
    				String al_typ = o.getString("typ","").trim();
    				if(GlobalConst.DT_CALC.equals(al_typ)) {
    					calMetaIdList.add(al_meta_m);
    				}
    			}
    			ordColMap.put(ord_col_cnt, o);
    			ord_col_cnt++;
    			System.out.println("id_meta:"+o.getInt("id_meta")+",id_parent_meta:"+o.getInt("id_parent_meta")+",depth:"+o.getInt("meta_depth")+",meta_nm:"+o.getString("meta_nm","")+",meta_doc:"+o.getString("meta_doc",""));
    		}
    		CommandMap metaKeyParam = new CommandMap();
    		metaKeyParam.put("id_meta_m",param.getInt("id_meta_m"));
//    		metaKeyParam.put("key_using_type", GlobalConst.KEY_USE_TYPE_EXCEL_UPLOAD);
    		//rel > key_m > key 컬럼순으로 사용
			List<CustomMap> childList = metaManageService.selectMetaRelByParentIdMetaM(metaKeyParam);
			Map<Integer,List<CustomMap>> fkListMap = new HashMap<>();
			//depth를 키로하고, 값은 depth별 aloneListMap을 저장
//			Map<Integer,Map<Integer,CustomMap>> childAloneListMap = new HashMap<>();
			for(CustomMap rel:childList) {
				if(rel.getInt("id_meta_rel",0)>0) {
		    		idMetaMAtDepth.put(rel.getInt("child_depth"), rel.getInt("child_id_meta_m"));
					CommandMap relParam = new CommandMap();
					relParam.put("id_meta_rel", rel.getInt("id_meta_rel"));
					relParam.put("key_use_ty", GlobalConst.KEY_USE_TYPE_EXCEL_UPLOAD);
					List<CustomMap> keyMList = metaManageService.selectMetaKeyMByIdMetaRel(relParam);
					for(CustomMap keyM:keyMList) {
						CommandMap keyParam = new CommandMap();
						keyParam.put("id_key", keyM.getInt("id_key"));
						List<CustomMap> keyList = metaManageService.selectMetaKeyListByIdKey(keyParam );
						fkListMap.put(rel.getInt("child_depth"),keyList);
					}
		    		CommandMap childParam=new CommandMap();
		    		childParam.put("id_meta_m",rel.getInt("child_id_meta_m"));
		    		//child의 max_depth
		    		int child_max_depth = service.getMaxDepthMetaByKey(childParam);
					List<CustomMap> childOrdList = service.selectMetaOrderListForExcel(childParam);
		    		List<CustomMap> childAloneList = MetaUtil.buildAloneList(childOrdList, child_max_depth, "id_parent_meta","id_meta","meta_depth");
		    		startMetaAtDepthMap.put(rel.getInt("child_depth"), childAloneList.get(0).getInt("id_meta",0));
	    			System.out.println("=============childAloneList 출력시작");
	        		int child_ord_col_cnt=0;
	        		Map<Integer,CustomMap> childOrdColMap = new HashMap<>();
		    		for(CustomMap o:childAloneList) {
		    			int al_meta_m = o.getInt("id_meta", 0);
		    			if(al_meta_m>0) {
		    				String al_typ = o.getString("typ","").trim();
		    				if(GlobalConst.DT_CALC.equals(al_typ)) {
		    					calMetaIdList.add(al_meta_m);
		    				}
		    			}
		    			childOrdColMap.put(child_ord_col_cnt, o);
		    			child_ord_col_cnt++;
		    			//여기서 meta_depth는 의미가 없음
		    			System.out.println("id_meta:"+o.getInt("id_meta")+",id_parent_meta:"+o.getInt("id_parent_meta")+",depth:"+o.getInt("meta_depth")+",meta_nm:"+o.getString("meta_nm","")+",meta_doc:"+o.getString("meta_doc",""));
		    		}
		    		aloneListMap.put(rel.getInt("child_depth"), childAloneList);
				}
			}
			int row_max_depth = childList.size()+1;
            int rowIndex = max_depth;
            int lastRowNum = sheet.getLastRowNum();
            //뎁스별 헤더 리스트를 담는 맵
            Map<Integer,List<CustomMap>> childHeaderMap = new HashMap<>();
            List<List<CommandMap>> inDataList = new ArrayList<>();
            List<List<CommandMap>> inChildDataList = new ArrayList<>();
            List<Map<CommandMap,List<CommandMap>>> realDataList = new ArrayList<>();
            CommandMap calParam = new CommandMap();
            calParam.put("cal_meta_m_list", calMetaIdList);
            calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
            List<CustomMap> calColInfoList = metaService.selectCalcuListByParam(calParam);
//            int depthOfChildRow = -1;
            int realRowNum=1;
            int curr_depth=1;
            // 각 cell을 순회하면서 값을 읽음
            boolean isCompleteSetStartCellIndexAtDepth = false;
            while (rowIndex <= lastRowNum) {
            	boolean isMainMeta = true;
                Row row = sheet.getRow(rowIndex);
                List<CommandMap> rowData = new ArrayList<>();
        		int nanValIdx=0;
        		boolean childMatch = false;
                if (row != null) {
                    //isFinishFirstCellInRow 가 true가 되면 행의 첫번째 데이터가 처리되었다는 의미 
                    boolean isFinishFirstCellInRow = false;
                    // 현재 행의 셀 수
                    int lastCellNum = row.getLastCellNum();
                    // 사용가능하고, 값을 처음 가져올 때 증가하는 변수.
                    int use_cell_idx=0;
                    if(rowIndex==7) {
                    	System.out.println("rowIndex:"+rowIndex);
                    }
                    for (int cellIndex = 0; cellIndex <= lastCellNum; cellIndex++) {
                        Cell cell = row.getCell(cellIndex);
                        //(TODO)디버깅코드
                        if(rowIndex>0 && cellIndex==10) {
                        	System.out.println("row end");
                        }
                        if(cellIndex==0 && cell==null) {
                        	break;
                        }
                        if (cell != null) {
                            // 현재 셀의 값을 읽어옴 (문자열로 변환)
                            boolean isUse = false;
                        	FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
                            String cellValue = ExcelCellUtil.getCellValueAsString(cell,formulaEvaluator);
                            
                            /**
                             * part1 : cell을 사용해서 1뎁스인지 아닌지 판단하는 부분
                             * isMainMeta가 false인 조건 : 첫번째 cell(cellIndex=0)일 때 머지된 cell이면서, 실
                             */
                            if(ExcelCellUtil.isMergedCell(sheet, rowIndex, cellIndex)) {
                            	if(ExcelCellUtil.isFirstMergedCell(sheet,rowIndex,cellIndex)) {
                            		isUse=true;
                            		isMainMeta = true;
                                    System.out.println("병합행 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                                	System.out.println("rowspan:"+ExcelCellUtil.getRowspan(sheet, rowIndex, cellIndex)+",colspan:"+ExcelCellUtil.getColspan(sheet, rowIndex, cellIndex));
                            	}else {
                            		if(cellIndex==0) {
                            			isMainMeta = false;
                            		}
                                    System.out.println("병합행되었지만 사용하지 않는 셀 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                                }
                            }else {
                        		isUse=true;
                        		System.out.println("일반 행 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                            }
                            /**
                             * part2 : isUse가 true일 때,  isMainMeta 불린값에 의해 분기, 
                             */
                            if(isUse) {
                            	if(rowIndex==3) {
                            		System.out.println("빠져나가아하는 rowIndex:"+rowIndex);
                            	}
                            	if(!isMainMeta) {
                            		int ordDepth = 0;
                            		if(!isFinishFirstCellInRow && "".equals(cellValue))
                            			continue;
                            		//curr_depth에 값이 없으면 처음 도착했다는 듯
                            		if(!isFinishFirstCellInRow) {
                            			if(!isCompleteSetStartCellIndexAtDepth) {
        	                                int next_depth = curr_depth+1;
        	                                if(startCellIndexAtDepthMap.get(cellIndex)==null) {
        	                                	startCellIndexAtDepthMap.put(cellIndex,next_depth);
        	                                	isFinishFirstCellInRow = true;
        	                                	curr_depth = next_depth;
        	                                	if(next_depth==row_max_depth) {
        	                                		isCompleteSetStartCellIndexAtDepth=true;
        	                                	}
        	                                }else {
        	                                	curr_depth = next_depth;
        	                                	isFinishFirstCellInRow=true;
        	                                }
                            			}else {
                            				if(startCellIndexAtDepthMap.get(cellIndex)==null) {
                            					throw new ExcelDataException("등록된 매핑 데이터가 없습니다.");
                            				}else {
                            					curr_depth = startCellIndexAtDepthMap.get(cellIndex);
                            					isFinishFirstCellInRow=true;
                            				}
                            			}
                            		}
                               //     CustomMap col = ordColMap.get(cellIndex);
                                    List<CustomMap> childAloneList = aloneListMap.get(curr_depth);
                                	System.out.println("rowIndex:"+rowIndex+"cellIndex:"+cellIndex);
//                                    int logicCellSpan = col.getInt("colspan", 0);
                                    CommandMap insCol = CollectionUtil.getCommandMapByCustomMap(childAloneList.get(nanValIdx));
    	                            insCol.put("val", cellValue);
                                	System.out.println("nanValIdx:"+nanValIdx+",meta_cd:"+insCol.getString("meta_cd",""));
    	                            rowData.add(insCol);
    	                            childMatch=true;
                            		nanValIdx++;
                            	}
                            	//1뎁스 실 데이터 처리
                            	else {
                            		if(cellIndex==0) {
                            			if(cellValue!=null && !"".equals(cellValue)) {
    	                                    CommandMap insCol = new CommandMap();
    	    	                            insCol.put("val", cellValue);
    	    	                            insCol.put("cell_index", cellIndex);
    	    	                            rowData.add(insCol);
                            			}else {
                            				continue;
                            			}
                            		}else {
	                                    List<CustomMap> childAloneList = aloneList;
//	                                	System.out.println("rowIndex:"+rowIndex+"cellIndex:"+cellIndex);
	//                                    int logicCellSpan = col.getInt("colspan", 0);
//	                                	if(childAloneList.size()>cellIndex) {
	                            			if(cellValue!=null && !"".equals(cellValue)) {
			                                    CommandMap insCol = CollectionUtil.getCommandMapByCustomMap(childAloneList.get(use_cell_idx));
			                                    System.out.println("====use_cell_idx:"+use_cell_idx+",cellValue:"+cellValue+",id_meta:"+insCol.getInt("id_meta",0));
			    	                            insCol.put("val", cellValue);
			    	                            rowData.add(insCol);
	                            			}
//	                                	}
                            			use_cell_idx++;
                            		}
                            	}
                            }
                            // 다음 셀로 이동
                            cellIndex += ExcelCellUtil.getColspan(sheet, rowIndex, cellIndex) - 1;
                        }else {
                        	if(isMainMeta) {
                        		use_cell_idx++;
                        	}else {
                        		if(childMatch) {
                            		nanValIdx++;                        			
                        		}
                        	}
                        	continue;
                        }
                    }
                    /**
                     * part3 : 행은 무조건 1뎁스부터 시작한다. isMainMeta에 따라 realDataList에 행값을 저장하는 부분
                     */
                	if(isMainMeta) {
                		if(rowData.size()==0) {
                			break;
                		}
                        inDataList.add(rowData);
                        Map<CommandMap,List<CommandMap>> ordRow = new HashMap<>();
                        CommandMap rowInfo = new CommandMap();
                        rowInfo.put("id", realRowNum);
                        rowInfo.put("depth", 1);
                        ordRow.put(rowInfo, rowData);
                        realDataList.add(ordRow);
                        realRowNum++;
                	}else {
                		if(realDataList.size()>0) {
                			Map.Entry<CommandMap, List<CommandMap>> parentRowEntry = this.getParentRow(realDataList,curr_depth-1);
                			if (parentRowEntry != null) {
                				CommandMap parentRowKey = parentRowEntry.getKey();
                    			CommandMap childRowInfo = new CommandMap();
                    			if(parentRowKey.getInt("id",0)>0) {
                        			childRowInfo.put("id_parent",parentRowKey.getInt("id",0));
                        			childRowInfo.put("id", realRowNum);
                        			childRowInfo.put("depth", curr_depth);
                                    Map<CommandMap,List<CommandMap>> ordRow = new HashMap<>();
                                    ordRow.put(childRowInfo, rowData);
                                    realDataList.add(ordRow);
                                    realRowNum++;
                    			}

                	            // parentRowEntry.getKey()를 사용하여 키(CommandMap)를 가져오고,
                	            // parentRowEntry.getValue()를 사용하여 값(List<CommandMap>)을 가져올 수 있습니다.
                	        } else {
                	            // fail 처리 필요
                	        }
                		}else {
                			throw new ExcelDataException("첫번째 데이터 행이 처리되지 못하였음.");
                		}
//                		CommandMap childInfo = rowData.get(rowData.size()-1);
//                		String meta_depth = childInfo.getString("meta_depth","");
//                		if(curr_depth>-1) {
//                			List<CommandMap> valList = new ArrayList<>();
//                			List<CustomMap> childHeader = childHeaderMap.get(curr_depth);
//                			int cell_step=1;
//                			for(CommandMap cell:rowData) {
//                				CustomMap child = childHeader.get(cell_step);
//                				child.put("val", cell.getString("val", ""));
//                				CommandMap cellCmd = CollectionUtil.getCommandMapByCustomMap(child);
//                				valList.add(cellCmd);
//                			}
//                			inChildDataList.add(valList);
//                		}
                	}
                }

                rowIndex++;
            }
            List<Map<CommandMap, List<CommandMap>>> calRealDataList = MetaUtil.calculateRealDataList(realDataList,calColInfoList);
 //           this.printCommandMapList(inDataList,"inDataList");
 //           this.printCommandMapList(inChildDataList,"inChildDataList");
            this.printRealDataList(calRealDataList,"excel row");
            service.insertMetaExcelData(calRealDataList,param,idMetaMAtDepth);
//            CollectionUtil.printDataBasedOnType(inDataList,"inDataList");  
//            CollectionUtil.printDataBasedOnType(inChildDataList, "inChildDataList");
//            CollectionUtil.printDataBasedOnType(realDataList, "excel row");
            // 사용이 끝난 후 Workbook 및 스트림을 닫아주는 것이 좋습니다.
            workbook.close();
            fis.close();
            file.delete();
            model = metaService.selectMataList(param,model);
			PageList<CustomMap> dataList = (PageList<CustomMap>) model.getAttribute("todayList");
    		putModelListPage(dataList,model);
            // 엑셀 파일 처리 로직 추가

            // 업로드 성공 메시지 반환
        } catch (IOException e) {
        	ret = GlobalConst.ACT_FAIL;
            e.printStackTrace();
			model.addAttribute("err_msg","File upload failed!");
        } catch(Exception e) {
            e.printStackTrace();
            ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg","알 수 없는 오류 발생");
            
        }
		model.addAttribute("ret",ret);
		return "jsonView";
	}
    // 특정 id의 자식 셀을 최대 뎁스까지 검색하여 추출하는 함수
    private List<CustomMap> findChildren(int parentId, List<CustomMap> cellList, int startDepth, int maxDepth) {
        List<CustomMap> children = new ArrayList<>();
        for (CustomMap cell : cellList) {
           int id_parent_meta = cell.getInt("id_parent_meta", 0);
           if (id_parent_meta == parentId && cell.getInt("meta_depth",0) >= startDepth && cell.getInt("meta_depth",0) <= maxDepth) {
                children.add(cell);
            }
        }
        return children;
    }
    
 // realDataList와 targetDepth를 파라미터로 받는 함수
//    public Map<CommandMap, List<CommandMap>> getParentRow(List<Map<CommandMap, List<CommandMap>>> realDataList, int targetDepth) {
//        // realDataList를 가장 최근 데이터부터 검색
//        for (int i = realDataList.size() - 1; i >= 0; i--) {
//            Map<CommandMap, List<CommandMap>> parentRow = realDataList.get(i);
//
//            // parentRow에서 해당 depth의 데이터 찾기
//            for (CommandMap key : parentRow.keySet()) {
//                if (key.getInt("depth",0) == targetDepth) {
//                    return parentRow; // 원하는 데이터를 찾았으므로 반환
//                }
//            }
//        }
//
//        return null; // 원하는 데이터가 없을 경우 null 반환
//    }
 // realDataList와 targetDepth를 파라미터로 받는 함수
    public Map.Entry<CommandMap, List<CommandMap>> getParentRow(List<Map<CommandMap, List<CommandMap>>> realDataList, int targetDepth) {
        // realDataList를 가장 최근 데이터부터 검색
        for (int i = realDataList.size() - 1; i >= 0; i--) {
            Map<CommandMap, List<CommandMap>> parentRow = realDataList.get(i);

            // parentRow에서 해당 depth의 데이터 찾기
            for (Map.Entry<CommandMap, List<CommandMap>> entry : parentRow.entrySet()) {
                if (entry.getKey().getInt("depth",0) == targetDepth) {
                    return entry; // 원하는 데이터를 찾았으므로 키와 값의 엔트리를 반환
                }
            }
        }

        return null; // 원하는 데이터가 없을 경우 null 반환
    }
    /**테스트 출력 함수
     * 
     * @param realDataList
     */
    public void printRealDataList(List<Map<CommandMap, List<CommandMap>>> realDataList,String listTitle) {
        System.out.println("--------------------" + listTitle + " 출력");
        for (Map<CommandMap, List<CommandMap>> data : realDataList) {
            System.out.println("--------------------1뎁스 행 출력");
            for (Map.Entry<CommandMap, List<CommandMap>> entry : data.entrySet()) {
                System.out.println("--------------------1뎁스 행의 entry 출력 시작");
                CommandMap key = entry.getKey();
                List<CommandMap> value = entry.getValue();
                System.out.println("--------------------1뎁스 행의 entry 키 출력 시작");
            	for (Entry<String, Object> en : key.entrySet()) {
                    String enkey = en.getKey();
                    String enVal = String.valueOf(en.getValue());
                    System.out.print("Key:" + enkey + ",Value:" + enVal + " , ");
                }
                System.out.println("--------------------1뎁스 행의 entry 키 출력 완료");
                System.out.println("--------------------1뎁스 행의 entry Value 출력 완료");
                 for(CommandMap map:value) {
                	for (Entry<String, Object> en : map.entrySet()) {
                        String enkey = en.getKey();
                        String enVal = String.valueOf(en.getValue());
                        System.out.print("Key:" + enkey + ",Value:" + enVal + " , ");
                    }
                    System.out.println("===");
                }
                System.out.println("--------------------1뎁스 행의 entry Value 출력 종료");
            }
        }
        System.out.println();
        System.out.println("--------------------" + listTitle + " 출력 종료");
    }
    public void printCommandMapList(List<List<CommandMap>> list,String listTitle) {
        System.out.println("--------------------" + listTitle + " 출력");
    	for (List<CommandMap> commandMapList : list) {
            System.out.println("--------------------1뎁스 행 출력시작");
            for (CommandMap commandMap : commandMapList) {
                System.out.println("--------------------2뎁스 행 출력시작");
                for (Entry<String, Object> entry : commandMap.entrySet()) {
                    String key = entry.getKey();
                    String value = String.valueOf(entry.getValue());
                    System.out.print("Key: " + key + ", Value: " + value + " , ");
                }
                System.out.println("===2뎁스 행 출력완료");
            }
            System.out.println("--------------------2뎁스 행 출력완료");
        }    
    }
}

