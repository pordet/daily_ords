<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>

<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: BodyHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

                <div class="usr-request-area">
                    <div class="top-statue2"><spring:message code="tag.body.head1user"/></div>
              <div class="top-area-data2">
                  <ul class="top-data">
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'D'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'P'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'R'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'N'}">
                        <li><span class="double"><spring:message code="status.not.approve.lbl1"/><br><spring:message code="status.not.approve.lbl2"/></span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'A'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'I'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    <c:forEach var="map" items="${statusList}">
                    <c:if test="${fn:replace(map.code,' ', '') eq 'C'}">
                        <li><span>${map.status}</span><strong>${map.cnt}</strong></li>
                    </c:if>
                    </c:forEach>
                    </ul>
                </div>
