package kr.co.daily.common.taglib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UrlPathHelper;

import daily.common.util.SimpleListTagSupport;
import daily.common.util.SpringApplicationContext;
import kr.co.daily.common.dao.CommonDAOImpl;
import kr.co.daily.domain.MenuVO;
import kr.co.daily.domain.NaviVO;

/**
 * 메뉴 선택시 메뉴 네비게이션 호출
 * @author pordet
 *
 */
public class NaviTag extends SimpleListTagSupport<MenuVO> {
	private String id_menu;
	private String locale;

	public String getId_menu() {
		return id_menu;
	}

	public void setId_menu(String id_menu) {
		this.id_menu = id_menu;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public List<MenuVO> getList() {

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		UrlPathHelper urlPathHelper = new UrlPathHelper(); 
		String url = urlPathHelper.getOriginatingRequestUri(req);
		String context = urlPathHelper.getOriginatingContextPath(req);
	    if(!context.equals("")) {
	    	url = url.substring(context.length());
	    }
		String id_menu = req.getParameter("id_menu");
		if(id_menu==null) id_menu="";
//		String url = req.getRequestURI();  - 
		List<MenuVO> list = null;
		CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
		NaviVO vo = new NaviVO();
		vo.setId_menu(id_menu);
		vo.setLocale(locale);
		try {
			List<MenuVO> allMenuList = dao.selectMenuListForNavigationBar(vo);
			Map<Integer,MenuVO> mnuMap = new HashMap<Integer,MenuVO>();
			for(MenuVO mnu : allMenuList) {
				mnuMap.put(mnu.getId_menu(), mnu);
			}
			
			Map<Integer,MenuVO> depthMnuMap = new HashMap<Integer,MenuVO>();
			MenuVO mnu = mnuMap.get(id_menu);
			int id_parent_menu = mnu.getId_parent_menu();
			depthMnuMap.put(mnu.getMenu_depth(),mnu);
			while(mnu.getMenu_depth()>0) {
				MenuVO parent_mnu =mnuMap.get(id_parent_menu);
				System.out.println(parent_mnu.toString());
				id_parent_menu = parent_mnu.getId_parent_menu();
				depthMnuMap.put(parent_mnu.getMenu_depth(),parent_mnu);
			}
			List<Integer> keySet = new ArrayList<>(depthMnuMap.keySet());
			Collections.sort(keySet);
			List<MenuVO> naviList = new ArrayList<MenuVO>();
			for(int depth:keySet) {
				MenuVO curMnu = depthMnuMap.get(depth);
				naviList.add(curMnu);
				System.out.println("menu depth:"+depth+",toString:"+curMnu.toString());
			}
			list = naviList;
		}catch(Exception e) {
			list = null;
		}
		return list;
	}

}
