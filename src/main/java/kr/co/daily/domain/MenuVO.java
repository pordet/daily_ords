package kr.co.daily.domain;

import java.util.List;

public class MenuVO {
	// input value
	private String id_user;
	private int user_seq;
	private String locale;
	private int menu_depth;
	private int id_group;
	
	// output value
	private int id_menu;
	private int id_parent_menu;
	private int sort_ord;
	private String menu_type;
	private String menu_nm;
	private String menu_desc;
	private String menu_url;
	private int child_cnt;
	private List<MenuVO> child;
	private boolean selected;
	private boolean childSelected;
	public String getId_user() {
		return id_user;
	}
	public void setId_user(String id_user) {
		this.id_user = id_user;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public int getUser_seq() {
		return user_seq;
	}
	public void setUser_seq(int user_seq) {
		this.user_seq = user_seq;
	}
	public int getMenu_depth() {
		return menu_depth;
	}
	public void setMenu_depth(int menu_depth) {
		this.menu_depth = menu_depth;
	}	
	
	public int getId_group() {
		return id_group;
	}
	public void setId_group(int id_group) {
		this.id_group = id_group;
	}
	public int getId_menu() {
		return id_menu;
	}
	public void setId_menu(int id_menu) {
		this.id_menu = id_menu;
	}
	public int getId_parent_menu() {
		return id_parent_menu;
	}
	public void setId_parent_menu(int id_parent_menu) {
		this.id_parent_menu = id_parent_menu;
	}
	public int getSort_ord() {
		return sort_ord;
	}
	public void setSort_ord(int sort_ord) {
		this.sort_ord = sort_ord;
	}
	public String getMenu_type() {
		return menu_type;
	}
	public void setMenu_type(String menu_type) {
		this.menu_type = menu_type;
	}
	public String getMenu_nm() {
		return menu_nm;
	}
	public void setMenu_nm(String menu_nm) {
		this.menu_nm = menu_nm;
	}
	public String getMenu_desc() {
		return menu_desc;
	}
	public void setMenu_desc(String menu_desc) {
		this.menu_desc = menu_desc;
	}
	public String getMenu_url() {
		return menu_url;
	}
	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}
	public int getChild_cnt() {
		return child_cnt;
	}
	public void setChild_cnt(int child_cnt) {
		this.child_cnt = child_cnt;
	}
	public List<MenuVO> getChild() {
		return child;
	}
	public void setChild(List<MenuVO> child) {
		this.child = child;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isChildSelected() {
		return childSelected;
	}
	public void setChildSelected(boolean childSelected) {
		this.childSelected = childSelected;
	}
	@Override
	public String toString() {
		return "MenuVO [id_user=" + id_user + ", locale=" + locale + ", menu_depth=" + menu_depth + ", id_menu="
				+ id_menu + ", id_parent_menu=" + id_parent_menu + ", sort_ord=" + sort_ord + ", menu_type=" + menu_type
				+ ", menu_nm=" + menu_nm + ", menu_desc=" + menu_desc + ", menu_url=" + menu_url + ", child_cnt="
				+ child_cnt + ", child=" + child + ", selected=" + selected + ", childSelected=" + childSelected + "]";
	}
	
}
