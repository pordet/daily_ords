import java.util.HashMap;
import java.util.Map;

public class MapValueSearch {
    public static void main(String[] args) {
        // 예시 문장과 맵
        String sentence = "cntrctAmt/advPayRate";
        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("cntrctAmt", "42");
        variableMap.put("intrmPayRate", "4");
        variableMap.put("advPayRate", null);
        // 맵에 값이 존재하지 않는 것을 탐색
        boolean hasMissingValues = havingAllVariableCal(variableMap, sentence);
        
        if (hasMissingValues) {
            System.out.println("문장에 맵에 없는 값이 존재합니다."+hasMissingValues);
        } else {
            System.out.println("문장에 모든 맵 값이 존재합니다."+hasMissingValues);
        }
    }
    
    public static boolean hasMissingValues(Map<String, String> map, String sentence) {
        for (String key : map.keySet()) {
            if (!sentence.contains(key)) {
                return true; // 문장에 해당 키가 없으면 true를 반환
            }
        }
        
        return false; // 모든 키가 존재하면 false를 반환
    }
    private static boolean hasKeyAtMapInSentence(Map<String, String> map, String sentence) {
    	int size = map.size();
    	int quiCnt=0;
        for (String key : map.keySet()) {
            if (sentence.contains(key)) {
            	System.out.println("matching Key:"+key);
            	quiCnt++;
            }
        }
        if(size>0 && quiCnt==size) {
        	return true;
        }else {
        	return false; // 모든 키가 존재하면 false를 반환
        }
    }
    public static boolean havingAllVariableCal(Map<String, String> map, String sentence) {
    	int varNmMatchingCnt=0;
    	Map<String,String> mMap = new HashMap<>();
        for (String key : map.keySet()) {
            if (sentence.contains(key)) {
            	System.out.println("matching Key:"+key);
            	mMap.put(key, map.get(key));
            	varNmMatchingCnt++;
            }
        }
        int quiCnt=0;
        for (String key : map.keySet()) {
        	System.out.println("varNmMatching Map:"+key);
        	String val = mMap.get(key);
        	if(val!=null) {
        		quiCnt++;
        	}
        }
        if(varNmMatchingCnt>0 && varNmMatchingCnt==quiCnt) {
        	return true;
        }else {
        	return false; // 모든 키가 존재하면 false를 반환
        }
    }
}
