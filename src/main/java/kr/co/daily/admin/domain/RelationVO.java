package kr.co.daily.admin.domain;

import java.util.Map;

public class RelationVO {
	private Map<String,TableDataVO> tableData;
	private Map<String,SvgVO> svgData;
	private Map<String,LineVO> lineData;
	private Map<String,RelColVO> relColData;
	public Map<String, TableDataVO> getTableData() {
		return tableData;
	}
	public void setTableData(Map<String, TableDataVO> tableData) {
		this.tableData = tableData;
	}
	public Map<String, SvgVO> getSvgData() {
		return svgData;
	}
	public void setSvgData(Map<String, SvgVO> svgData) {
		this.svgData = svgData;
	}
	public Map<String, LineVO> getLineData() {
		return lineData;
	}
	public void setLineData(Map<String, LineVO> lineData) {
		this.lineData = lineData;
	}
	public Map<String, RelColVO> getRelColData() {
		return relColData;
	}
	public void setRelColData(Map<String, RelColVO> relColData) {
		this.relColData = relColData;
	}
}
