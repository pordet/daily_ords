package daily.mybatis.util.interceptor;

import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.MappedStatement.Builder;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Intercepts({@Signature(type = Executor.class,
method = "query",
args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class MybatisForPagingInterceptor implements Interceptor {
	
	private static final Logger logger = LoggerFactory.getLogger(MybatisForPagingInterceptor.class);

//	private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
//	private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
//	private static final ReflectorFactory DEFAULT_REFLECTOR_FACTORY = new DefaultReflectorFactory();

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		Object[] args = invocation.getArgs();  
		MappedStatement ms = (MappedStatement)args[0];
		Object param = args[1];
		BoundSql boundSql = ms.getBoundSql(param);
		//invocation.getArgs(); invocation.getArgs(
		String originalSql = boundSql.getSql();
		logger.debug("originalSql = {}", originalSql);
		RowBounds rb = (RowBounds) args[2];

		logger.debug("RowBounds = {}", rb);

		if (rb == null || rb == RowBounds.DEFAULT) {
			// RowBounds가 없으면 그냥 실행
			return invocation.proceed();
		}

		// RowBounds가 있다!
		// 원래 쿼리에 limit 문을 붙여준다.
		StringBuffer sb = new StringBuffer();
		sb.append(originalSql);
		sb.append(" limit ");
		sb.append(rb.getOffset());
		sb.append(", ");
		sb.append(rb.getLimit());

		logger.debug("sql = {}", sb.toString());
		String sql = sb.toString();
		
		BoundSql newBoundSql = new BoundSql(ms.getConfiguration(),
                sql, boundSql.getParameterMappings(), boundSql
                        .getParameterObject());

		MappedStatement newMs = copyFromMappedStatement(ms,
                new BoundSqlSqlSource(newBoundSql));
		args[0] = newMs;
		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}

    // see: MapperBuilderAssistant
    private MappedStatement copyFromMappedStatement(MappedStatement ms,
            SqlSource newSqlSource) {
        Builder builder = new MappedStatement.Builder(ms
                .getConfiguration(), ms.getId(), newSqlSource, ms
                .getSqlCommandType());

        builder.resource(ms.getResource());
        builder.fetchSize(ms.getFetchSize());
        builder.statementType(ms.getStatementType());
        builder.keyGenerator(ms.getKeyGenerator());

        // setStatementTimeout()
        builder.timeout(ms.getTimeout());

        // setStatementResultMap()
        builder.parameterMap(ms.getParameterMap());

        // setStatementResultMap()
        builder.resultMaps(ms.getResultMaps());
        builder.resultSetType(ms.getResultSetType());

        // setStatementCache()
        builder.cache(ms.getCache());
        builder.flushCacheRequired(ms.isFlushCacheRequired());
        builder.useCache(ms.isUseCache());

        return builder.build();
    }
    
    public static class BoundSqlSqlSource implements  SqlSource {
        BoundSql boundSql;

        public BoundSqlSqlSource(BoundSql boundSql) {
            this.boundSql = boundSql;
        }

        public BoundSql getBoundSql(Object parameterObject) {
            return boundSql;
        }
    }
}
