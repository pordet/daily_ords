package kr.co.daily.admin.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.MetaManageDAO;
import kr.co.daily.common.GlobalConst;

@Service
public class MetaManageService {
	@Autowired MetaManageDAO dao;

	public PageList<CustomMap> selectMetaMList(CommandMap map) {
		return dao.selectMetaMList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public CustomMap getMetaM(CommandMap map) {
		return dao.getMetaM(map.getMap());
	}

	public void insertMetaM(CommandMap map) {
		dao.insertMetaM(map.getMap());
	}

	public void modifyMetaM(CommandMap map) {
		dao.modifyMetaM(map.getMap());
	}

	public CustomMap selectMeta(CommandMap map) {
		return dao.selectMeta(map.getMap());
	}

	public List<CustomMap> selectMetaByIdMetaM(CommandMap map) {
		return dao.selectMetaByIdMetaM(map.getMap());
	}
	public List<CustomMap> selectMetaRelByParentIdMetaM(CommandMap map) {
		return dao.selectMetaRelByParentIdMetaM(map.getMap());
	}
	public List<CustomMap> selectMetaKeyMByIdMetaRel(CommandMap map) {
		return dao.selectMetaKeyMByIdMetaRel(map.getMap());
	}

	public List<CustomMap> selectMetaKeyListByIdKey(CommandMap map) {
		return dao.selectMetaKeyListByIdKey(map.getMap());
	}

	public CustomMap getMetaByIdMetaMAndIdMeta(CommandMap map) {
		return dao.getMetaByIdMetaMAndIdMeta(map.getMap());
	}

	public List<CustomMap> selectChildMetaListByIdParentMeta(CommandMap map) {
		return dao.selectChildMetaListByIdParentMeta(map.getMap());
	}

	public Map<String, Object> insertMeta(CommandMap map) {
		switch(map.getString("typ", GlobalConst.DT_NUM)) {
			case GlobalConst.DT_NUM :
				map.put("precision_len", 0);
				map.put("len_type", 1);
				break;
			case GlobalConst.DT_DATE :
				map.put("decimal_len", 10);
				map.put("precision_len", 0);
				map.put("len_type", 1);
				break;
			case GlobalConst.DT_DATETIME :
				map.put("decimal_len", 19);
				map.put("precision_len", 0);
				map.put("len_type", 1);
				break;
		}
		int next_ord = dao.getNextOrdByIdMetaMAndMetaDepth(map.getMap());
		map.put("ord", next_ord);
		dao.insertMeta(map.getMap());
		return map.getMap();
	}

	public CustomMap modifyMeta(CommandMap map) {
		switch(map.getString("typ", GlobalConst.DT_NUM)) {
		case GlobalConst.DT_NUM :
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATE :
			map.put("decimal_len", 10);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATETIME :
			map.put("decimal_len", 19);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		}
		dao.modifyMeta(map.getMap());
		return dao.getMetaByIdMetaMAndIdMeta(map.getMap());
	}

	public List<String> deleteMetaByList(CommandMap map) {
		List<String> del_code_list = dao.selectMetaListByInIdMeta(map.getMap());
		CommandMap delMap = new CommandMap();
		delMap.put("del_code_list", del_code_list);
		dao.deleteMetaByList(delMap.getMap());
		return del_code_list;
	}

	public void deleteMetaM(List<String> list) {
		CommandMap map = new CommandMap();
		map.put("del_key_list", list);
		dao.deleteMetaByIdMetaMList(map.getMap());
		dao.deleteMetaMByIdMetaMList(map.getMap());
	}



}
