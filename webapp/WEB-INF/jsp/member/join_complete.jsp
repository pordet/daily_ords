<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.SpringPropertiesUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<!DOCTYPE html>
<html>
<front:FrontHead title="">
		<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/member/member_register.js'/>"></script>
</front:FrontHead>
<front:FrontMainBody currLocale="${locale}" hasPop="true" clazz="" hasImgTop="true">
<!--
<div class="join-wrap">
	
		<div class="page-title-area">
	        <h2 class="tit just-pc">승인 신청</h2>
	        <input type="hidden" id="none"/>
	    </div>
		<div class="top-text v2">
			<span><strong>승인 신청이 완료되었습니다.</strong></span>
			<p>
				승인 결과는 <em>등록하신 이메일로 결과가 통지됩니다.</em> 
			</p>
		</div>					

		<p class="text-area">가입정보</p>
		
		<div class="join-info box-border">
			<ul class="dot-list">
				<li><strong>아이디</strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${param.id_user}</li>
				<li><strong>이름</strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${param.user_nm}</li>
				<li><strong>이메일</strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${param.email}</li>
			</ul>
		</div>

		<div class="button-group a-c">
			<a href="<c:url value='/'/>" class="button navy">홈</a>
			<a href="<c:url value='/loginPage.do'/>" class="button blue">로그인</a>
		</div>
			
	</div>
-->
	<div class="page-wrap">
		<div class="page-layer w70">
			<div class="page-title-wrap">
				<span>회원 가입</span>
			</div>
			<div class="join-result-wrap">
				<div class="join-result-header">
					<div>회원가입 신청이 완료되었습니다.</div>
					<div>승인결과는 등록하신 이메일로 전달됩니다.</div>
				</div>
				
				<div class="join-result-body">
					
					<table>
						<tr>
							<td>아이디</td>
							<td>${param.id_user}</td>
						</tr>
						<tr>
							<td>이름</td>
							<td>${param.user_nm}</td>
						</tr>
						<tr>
							<td>이메일</td>
							<td>${param.email}</td>
						</tr>
					</table>
				</div>
				
				<div class="join-result-bottom">
					<button type="button" class="btn-default" onclick="goToUrl()">메인으로 이동</button>
					<button type="button" class="btn-primary" onclick="goToUrl('/loginPage.do')">로그인</button>
				</div>
			</div>
		</div>
	</div>
</front:FrontMainBody>
<script>
function goToUrl(target) {
	var url = contextPath;
	if(target != null) {
		url += target
	}
	window.location.href = url;
}
/*
function joinAction(){
	if($("#id_user").val()){
		alertOpen("")
	}
	if($("#pwd").val()){
		
	}
	$("frm").sumbit();
}
*/
</script>
</html>
