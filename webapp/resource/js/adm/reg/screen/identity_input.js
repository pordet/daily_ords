$(function () {
	$("#btnSave").click(function(){
		var params = $("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/adm/meta/insertIdentityAjax.do";
		}else{
			url = contextPath+"/adm/meta/modifyIdentityAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else if(e.ret==8){
	    			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    			return;
		    	}else{
		    		if(insert_yn =='Y'){
		    			$("#cur_title").html('메타 마스터 수정');
			    		$("#insert_yn").val('N');
						var d = e.data; // e.data에 데이터가 있다고 가정합니다.
						
						var html = '<tr class="grid-loof-cont" ondblclick="javascript:regMeta(\'' + d.id_meta_m + '\',\'' + d.id_meta + '\')" id="metaInfo' + d.id_meta + '">';
						html += '<td class="check-unit"><input type="checkbox" name="cb_id_meta_m" value="' + d.id_meta_m + '"></td>';
						html += '<td class="cont-unit">' + d.meta_depth + '</td>';
						html += '<td class="cont-unit">' + d.ord + '</td>';
						html += '<td class="cont-unit">' + d.meta_cd + '</td>';
						html += '<td class="cont-unit">' + d.typ_nm + '</td>';
						html += '<td class="cont-unit desc-unit">' + d.meta_nm + '</td>';
						html += '</tr>';	
						$("#meta_list").append(html);
		    		}else{
						var d = e.data; // e.data에 데이터가 있다고 가정합니다.
						
						html += '<td class="check-unit"><input type="checkbox" name="cb_id_meta_m" value="' + d.id_meta_m + '"></td>';
						html += '<td class="cont-unit">' + d.meta_depth + '</td>';
						html += '<td class="cont-unit">' + d.ord + '</td>';
						html += '<td class="cont-unit">' + d.meta_cd + '</td>';
						html += '<td class="cont-unit">' + d.typ_nm + '</td>';
						html += '<td class="cont-unit desc-unit">' + d.meta_nm + '</td>';
						$("#metaInfo"+d.id_meta).html(html);
					}
				   	$("#btnInit").prop("disabled", false);
				   	$("#btnAddEquiLvlSub").prop("disabled", false);
				    $("#btnAddSub").prop("disabled", false);
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	});
	$("#btnDelete").click(function(){
		confirmOpen('해당 메타를 삭제하시면 관련 하위 메타들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteData");
	});
	$("#btnAddSub").click(function(){
		if($.trim($("#id_parent_meta").val())==''){
			if($("#id_meta").val()==''){
				alertOpen('부모 메타 항목을 선택하지 않았습니다.');			
			}else{
				$("#id_parent_meta").val($("#id_meta").val());
			}
		}else{
			$("#id_parent_meta").val($("#id_meta").val());
			initForm(eval($("#meta_depth").val())+1);
			$("[id^='metaInfo']").removeClass("sel-tr");
			$("#metaInfo"+$("#id_parent_meta").val()).addClass("sel-tr");
        	$("#btnAddEquiLvlSub").prop("disabled", true);
			var cur_txt = $("#metaInfo"+$("#id_parent_meta").val()+" td:eq(5)").html()+" 하위 ";
			 	cur_txt += eval($("#metaInfo"+$("#id_parent_meta").val()+" td:eq(1)").html())+1;
		     	cur_txt +="뎁스 메타 추가";
			$("#cur_title").html(cur_txt);
		}
	});
	$("#btnAddEquiLvlSub").click(function(){
		if($.trim($("#id_parent_meta").val())==''){
			alertOpen('부모 메타 항목을 선택하지 않았습니다.');			
		}else{
			initForm($("#meta_depth").val());
			$("[id^='metaInfo']").removeClass("sel-tr");
			$("#metaInfo"+$("#id_parent_meta").val()).addClass("sel-tr");
			var cur_txt = $("#metaInfo"+$("#id_parent_meta").val()+" td:eq(5)").html()+" 하위 ";
			 	cur_txt += eval($("#metaInfo"+$("#id_parent_meta").val()+" td:eq(1)").html())+1;
		     	cur_txt +="뎁스 메타 추가";

       		$("#btnAddSub").prop("disabled", true);
			$("#cur_title").html(cur_txt);
		}
	});
	$("#btnInit").click(function(){
			initForm(1);
			$("#id_parent_meta").val('');
       		$("#btnAddSub").prop("disabled", true);
			$("[id^='metaInfo']").removeClass("sel-tr");
 	});
	$("#btnList,#toList").click(function(){
		var page = $("#page").val();
		var frm =document.frm;
			frm.action=contextPath+"/adm/meta/identityMList.do";
		if(page!="")
			frm.page.value=page;
		frm.submit();
	});
});
function initForm(meta_depth){
	$("#cur_title").html('신규 메타 추가');
	$("#id_meta").val('');
	$("#insert_yn").val('Y');
	$("#meta_nm").val('');
    // 메타 코드 입력란 채우기
    $("#meta_cd").val('');
	$("#meta_depth").val(meta_depth);
    
    // 메타 뎁스 입력란 채우기
    
    // 메타 설명 입력란 채우기
    $("#meta_doc").val('');
    // 데이터 타입 선택란 채우기
    $("#extern_yn2").prop("checked", true);
	// 하위여부 라디오 버튼 채우기
    $("#list_yn2").prop("checked", true);
	// 사용여부 라디오 버튼 채우기
    $("#use2").prop("checked", true);
   	$("#btnAddEquiLvlSub").prop("disabled", true);
    $("#btnAddSub").prop("disabled", true);
    $("#btnSave").text("저장");
}

function confirmDeleteData(){
	var formData = new FormData($('#checkFrm')[0]);
	var url = contextPath+"/adm/meta/deleteIdentityAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
// 	    	}else if(e.ret==8){
//     			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
//     			return;
	    	}else{
	    		$.each(e.del_code_list,function(i,d){
					$("#metaInfo"+d).remove();
				});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}

function doList(){
	var frm =document.listFrm;
	frm.action=contextPath+"/adm/meta/identityList.do";
	frm.submit();	
}
function allCheck(){
	var checkboxes = document.getElementsByName('chk_id_meta'); // 모든 체크박스 요소를 가져옵니다.
    var allCheckElement = document.getElementById('allCheck'); // 전체 선택 체크박스 요소를 가져옵니다.
    // 전체 선택 체크박스의 체크 여부에 따라 모든 체크박스를 선택하거나 선택 해제합니다.
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = allCheckElement.checked;
    }
}
