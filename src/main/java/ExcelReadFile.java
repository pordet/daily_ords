import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import daily.common.util.CollectionUtil;
import daily.common.util.ExcelCellUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelReadFile {
    public static void main(String[] args) {
        try (Workbook workbook = new XSSFWorkbook(new FileInputStream("d:\\meta\\upload\\계약.xlsx"))) {
            Sheet sheet = workbook.getSheetAt(0); // 첫 번째 시트를 읽음

            // 첫 번째 행을 헤더로 처리하여 헤더 정보를 추출
            Row headerRow = sheet.getRow(0);
            Map<Integer, CustomMap> headerMap = new HashMap<>();
            for (int colIndex = 0; colIndex < headerRow.getLastCellNum(); colIndex++) {
                Cell headerCell = headerRow.getCell(colIndex);
//                headerMap.computeIfAbsent(colIndex, k -> new HashMap<>());
                CustomMap header = new CustomMap();
                header.put("val", headerCell.getStringCellValue());
                headerMap.put(colIndex, header);
            }

            // 데이터를 저장할 데이터 맵 초기화
            Map<Integer, Map<Integer, Map<Integer, String>>> dataMap = new HashMap<>();
            List<List<CommandMap>> inDataList = new ArrayList<>();

            int rowIndex = 1; // 두 번째 행부터 데이터
            while (rowIndex <= sheet.getLastRowNum()) {
                Row row = sheet.getRow(rowIndex);
                List<CommandMap> rowData = new ArrayList<>();
                if (row != null) {
                    // 현재 행의 셀 수
                    int lastCellNum = row.getLastCellNum();

                    for (int cellIndex = 0; cellIndex < lastCellNum; cellIndex++) {
                        Cell cell = row.getCell(cellIndex);
                        if(cellIndex==0) {
                        	
                        }
                        CustomMap col = headerMap.get(cellIndex);
                  //  	System.out.println("rowIndex:"+rowIndex+",cellIndex:"+cellIndex);
                        int logicCellSpan = col.getInt("colspan", 0);
                        CommandMap insCol = CollectionUtil.getCommandMapByCustomMap(col);
                        //(TODO)디버깅코드
                        if(rowIndex>8 && cellIndex==1) {
                        	System.out.println("row end");
                        }
                        if (cell != null) {
                            // 현재 셀의 값을 읽어옴 (문자열로 변환)
                        	FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
                            String cellValue = ExcelCellUtil.getCellValueAsString(cell,formulaEvaluator);
                            //                            if(cellIndex==0&& "".equals(cellValue.trim())) {
//                            	break;
//                            }
                            if(ExcelCellUtil.isMergedCell(sheet, rowIndex, cellIndex)) {
                            	if(ExcelCellUtil.isFirstMergedCell(sheet,rowIndex,cellIndex)) {
                                    System.out.println("병합행 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                                	System.out.println("rowspan:"+ExcelCellUtil.getRowspan(sheet, rowIndex, cellIndex)+",colspan:"+ExcelCellUtil.getColspan(sheet, rowIndex, cellIndex));
                            	}else {
                                    System.out.println("병합행되었지만 사용하지 않는 셀 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                                                                	}
                            }else {
                                System.out.println("일반 행 " + rowIndex + ", 열 " + cellIndex + ",cell값: " + cellValue);
                            	
                            }
                            // 수평 병합된 셀 정보 확인  146

                            insCol.put("val", cellValue);
                            rowData.add(insCol);
                            // 다음 셀로 이동
                            cellIndex += ExcelCellUtil.getColspan(sheet, rowIndex, cellIndex)-1;
                            System.out.println("==========다음 셀의 cellIndex 값: " + cellIndex);
                        }
                    }
                    inDataList.add(rowData);
                }
                rowIndex++;
            }

            // 결과 출력
//            for (int dataIndex = 1; dataIndex <= 2; dataIndex++) {
//                System.out.println("Data Index: " + dataIndex);
//                Map<Integer, Map<Integer, String>> rowDataMap = dataMap.get(dataIndex);
//                for (int rowDataIndex : rowDataMap.keySet()) {
//                    System.out.println("Row Index: " + rowDataIndex);
//                    Map<Integer, String> cellData = rowDataMap.get(rowDataIndex);
//                    for (int colIndex : cellData.keySet()) {
//                        System.out.println("Header Index: " + colIndex);
//                        String cellValue = cellData.get(colIndex);
//                        System.out.println("Cell Value: " + cellValue);
//                    }
//                }
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
