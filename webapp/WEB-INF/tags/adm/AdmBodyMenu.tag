<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<c:set var="msg" value="request" />
<commc:MenuTag user_seq="${id_user}" locale="${currLocale}" listName="menuList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
	<div class="admin-nav-area">
		<div class="admin-nav-wrap">
			<div class="nav-main-title">
				<span>Admin Menu</span>
			</div>
			<div class="nav-menu-list">
				<c:forEach var="item" items="${menuList}">
					<div>
						<div class="side-nav-main-btn">
							<a href="javascript:void(0);" onclick="javascript:showDiv(this,'${item.id_menu}');">
								<span>${item.menu_nm}</span> 
								<c:if test="${not empty item.child_cnt}">
									<div>
										<i class="fa fa-angle-down fa-pull-right fa_pos" id="angle_${item.id_menu}"></i>
									</div>
								</c:if>
							</a>
						</div>
						<c:if test="${not empty item.child_cnt}">
							<div class="collapse in" id="coll_${item.id_menu}">
								<ul>
									<c:forEach var="subMenu" items="${item.child}" varStatus="i">
										<li class="side-nav-sub-btn">
											<c:choose>
												<c:when test="${fn:contains(subMenu.menu_url, '?')}">
													<a href="<c:url value='${subMenu.menu_url}'/>&id_menu=${subMenu.id_menu}"	class="${(param.id_menu eq subMenu.id_menu ?'active':'')}">
														<span>${subMenu.menu_nm}</span> 
													</a>
												</c:when>
												<c:otherwise>
													<a href="<c:url value='${subMenu.menu_url}'/>?id_menu=${subMenu.id_menu}"	class="${(param.id_menu eq subMenu.id_menu ?'active':'')}">
														<span>${subMenu.menu_nm}</span> 
													</a>
												</c:otherwise>
											</c:choose>
										</li>
									</c:forEach>
								</ul>
							</div>
						</c:if>							
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

<script>
/*
	$(function () {
	   $('.js-action-a').click(function (e) {
			e.preventDefault();
		});
	});

	function goByTree(that,idMenu){
		var parent = $(that).parent().parent();
		if(parent.hasClass("tree-open")){
			parent.removeClass("tree-open");
			parent.children(".comm-treeview-list").hide();
		}else{
			parent.addClass("tree-open");
			parent.children(".comm-treeview-list").show();
		}
		$("[id^=sel_icon_]").removeClass("seled");
		$("#sel_icon_"+idMenu).addClass("seled");
		$("#sel_ord_str").val(idMenu);
//		doList(1,true,false);	
	}
	function showDiv(id,idMenu){
		var div = $("#coll_"+idMenu);
		div.children(".fa-angle-down")
		if(div.hasClass("in")){
			div.removeClass("in");
			$("#angle_"+idMenu).addClass("fa-rotate-270");
//			doList(1);	
		}else{
			$(div).addClass("in");
			$("#angle_"+idMenu).removeClass("fa-rotate-270");
//			doList(1);	
		}
//		$(".facet-item").removeClass("seled");
		selInit()
	}
	function selInit(){
		$("[id^=sel_icon_]").removeClass("seled");
		$("[id^=p_span_]").removeClass("seled_text");
		$("[id^=span_]").removeClass("seled_text");
	}
*/
function showDiv(id,idMenu){
	var div = $("#coll_"+idMenu);
	div.children(".fa-angle-down")
	if(div.hasClass("in")){
		div.removeClass("in");
		$("#angle_"+idMenu).addClass("fa-rotate-270");
	}else{
		$(div).addClass("in");
		$("#angle_"+idMenu).removeClass("fa-rotate-270");
	}
}
</script>