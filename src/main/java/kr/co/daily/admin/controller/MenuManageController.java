package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.MenuManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class MenuManageController extends AbstractControllerBase {

	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private MenuManageService service;
	
	@RequestMapping("/adm/menu/menuList.do")
	public String menuList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		map.put("id_parent_menu", 0);
		List<CustomMap> topMenuList = service.selectMenuCodeByParentId(map);
		PageList<CustomMap> resultList = service.selectMenuList(commandMap);
		model.addAttribute("codeList", topMenuList);
	    this.putModelListPage(resultList, model);
		return "/adm/menuList";
	}
	
	@RequestMapping("/adm/menu/regMenu.do")
	public String regMenu(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		if("".equals(commandMap.getString("parent_id",""))) {
			commandMap.put("depth", 1);
		}
		Locale locale = localeResolver.resolveLocale(req);
		commandMap.put("locale", locale.toString());
		CustomMap map = service.getMenu(commandMap);
		CommandMap pmap = new CommandMap();
		pmap.put("locale", locale.toString());
		pmap.put("id_parent_menu", 0);
		List<CustomMap> parentMenuList = service.selectMenuCodeByParentId(pmap);
		model.addAttribute("parentMenuList", parentMenuList);
		if(map!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("map", map);
		}
		return "/adm/reg/regMenu";
	}
	@RequestMapping("/adm/menu/changeMenuAjax.do")
	public String changeMenu(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		if("".equals(commandMap.getString("parent_id",""))) {
			commandMap.put("depth", 1);
		}else {
			
		}
		Locale locale = localeResolver.resolveLocale(req);
		commandMap.put("locale", locale.toString());
		List<CustomMap> map = service.selectMenuByParentId(commandMap);
		model.addAttribute("map", map);
		return "jsonView";
	}
	@RequestMapping("/adm/menu/insertMenuAjax.do")
	public String insertMenu(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			int maxOrder = service.getMaxOrder(commandMap);
			commandMap.put("menu_order",maxOrder+1);
	        String parent_id = commandMap.getString("id_parent_menu","");
	        if("".equals(parent_id)) {
	        	commandMap.put("depth",1);
	        	commandMap.put("id_parent_menu",0);
	        }else {
	        	CommandMap parMap = new CommandMap();
	        	parMap.put("trg_menu_id", parent_id);
	        	CustomMap parInfoMap = service.getMenu(parMap);
	        	int parDepth=0;
	        	if(parInfoMap!=null) {
	        		parDepth = parInfoMap.getInt("menu_depth",0);
	        	}
	        	commandMap.put("menu_depth", parDepth+1);
	        }
	        commandMap = addParamByLoginInfo(commandMap);
			service.insertMenu(commandMap);
	        String trg_id_menu = commandMap.getString("trg_id_menu");
	        model.addAttribute("trg_id_menu", trg_id_menu);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/menu/deleteMenuList.do")
	public String deleteMenu(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			service.deleteMenu(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		List<CustomMap> topMenuList = service.selectMenuCodeByParentId(map);
		PageList<CustomMap> resultList = service.selectMenuList(commandMap);
	    this.putModelListPage(resultList, model);
		model.addAttribute("codeList", topMenuList);
		model.addAttribute("act_type","D");
		model.addAttribute("ret",ret);
		return "/adm/menuList";
	}
	@RequestMapping("/adm/menu/deleteMenuAjax.do")
	public String deleteMenuAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("trg_id_menu",""));
				commandMap.put("del_code_list", codelist);
				service.deleteMenu(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/menu/modifyMenuAjax.do")
	public String modifyMenu(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("id_group",""));
				commandMap.put("del_code_list", codelist);
				service.modifyMenu(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/menu/selectMenu.do")
	public String selectMenu(CommandMap commandMap, Model model) throws Exception {
		List<CustomMap> resultList = service.selectMenu(commandMap);
		model.addAttribute("list", resultList);
		return "/adm/userGroupList";
	}
	
	
	@RequestMapping("/adm/menu/setMenuByGroup.do")
	public String setMenuByGroup(CommandMap commandMap, Model model) throws Exception {
		List<CustomMap> resultList = service.selectMenu(commandMap);
		model.addAttribute("list", resultList);
		return "/adm/userGroupList";
	}
	
}
