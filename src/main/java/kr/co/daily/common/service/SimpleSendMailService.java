package kr.co.daily.common.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import daily.common.util.FileUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Service
public class SimpleSendMailService {
	
	private Logger log = LoggerFactory.getLogger(SimpleSendMailService.class);

	@Autowired private JavaMailSender mailSender;
	
	public void setMailSender(JavaMailSender sender) {
		this.mailSender = sender;
	}
	public int sendMail(CommandMap map) {
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("mail Send Success");
		return ret;

	}
	public int sendMailMessage(CommandMap map) {
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("mail Send Success");
		return ret;

	}
	public int sendMailFromHtml(CommandMap map,Map<String, String> input) {
//		String serverUrl = map.getString("serverUrl","");
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMultipart multipart = new MimeMultipart();
	        BodyPart messageBodyPart = new MimeBodyPart();
	        
	        //HTML mail content
	        String htmlText = readEmailFromHtml(req.getSession().getServletContext().getRealPath("/resource/html/confirm-email.html"),input);
	        messageBodyPart.setContent(htmlText, "text/html");
	        multipart.addBodyPart(messageBodyPart); 
	        
	        messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-logo.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID","<image>");

            multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();
            DataSource fds1 = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-pattern.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds1));
            messageBodyPart.setHeader("Content-ID","<mailPattern>");

            multipart.addBodyPart(messageBodyPart);
            
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
//			String htmlContent = map.getString("content");
			message.setContent(multipart);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;

	}
	public int sendMailFromResetHtml(CommandMap map,Map<String, String> input) {
//		String serverUrl = map.getString("serverUrl","");
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMultipart multipart = new MimeMultipart();
	        BodyPart messageBodyPart = new MimeBodyPart();
	        
	        //HTML mail content
	        String htmlText = readEmailFromHtml(req.getSession().getServletContext().getRealPath("/resource/html/reset-pw-email.html"),input);
	        messageBodyPart.setContent(htmlText, "text/html");
	        multipart.addBodyPart(messageBodyPart); 
	        
	        messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/logo.gif"));  
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID","<image>");

            multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();
            DataSource fds1 = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-pattern.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds1));
            messageBodyPart.setHeader("Content-ID","<mailPattern>");

            multipart.addBodyPart(messageBodyPart);
            
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"euc-kr");
			messHelper.setSubject(map.getString("subject"));
//			String htmlContent = map.getString("content");
			message.setContent(multipart);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "euc-kr"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;

	}
	public int sendMailFromReqHtml(CommandMap map,Map<String, String> input) {
//		String serverUrl = map.getString("serverUrl","");
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMultipart multipart = new MimeMultipart();
	        BodyPart messageBodyPart = new MimeBodyPart();
	        
	        //HTML mail content
	        String htmlText = readEmailFromHtml(req.getSession().getServletContext().getRealPath("/resource/html/crs-user_register.html"),input);
	        messageBodyPart.setContent(htmlText, "text/html");
	        multipart.addBodyPart(messageBodyPart); 
	        
	        messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-logo.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID","<image>");

            multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();
            DataSource fds1 = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-pattern.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds1));
            messageBodyPart.setHeader("Content-ID","<mailPattern>");

            multipart.addBodyPart(messageBodyPart);
            
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
//			String htmlContent = map.getString("content");
			message.setContent(multipart);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;

	}
	public int sendMailFromAlarmHtml(CommandMap map,Map<String, String> input) {
//		String serverUrl = map.getString("serverUrl","");
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMultipart multipart = new MimeMultipart();
	        BodyPart messageBodyPart = new MimeBodyPart();
	        
	        //HTML mail content
	        String htmlText = readEmailFromHtml(req.getSession().getServletContext().getRealPath("/resource/html/alarm-email.html"),input);
	        messageBodyPart.setContent(htmlText, "text/html");
	        multipart.addBodyPart(messageBodyPart); 
	        
	        messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-logo.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID","<image>");

            multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();
            DataSource fds1 = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-pattern.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds1));
            messageBodyPart.setHeader("Content-ID","<mailPattern>");

            multipart.addBodyPart(messageBodyPart);
            
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
//			String htmlContent = map.getString("content");
			message.setContent(multipart);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;

	}
	public int sendMail(CommandMap map, List<MultipartFile> attach) {
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			for(MultipartFile file : attach) {
				File attachFile = FileUtil.convertMultipartFileToFile(file);
			    messHelper.addAttachment(MimeUtility.encodeText(file.getOriginalFilename(), "UTF-8", "B"), attachFile);
			}
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;
	}

	public int sendMail(CommandMap map, File attach) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			messHelper.addAttachment(MimeUtility.encodeText(attach.getName(), "UTF-8", "B"), attach);
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		log.debug("mail and attach send success");
		return 0;
	}
	
	public int sendMail(CommandMap map, InputStream is) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			messHelper.addAttachment(MimeUtility.encodeText("noti.pdf", "UTF-8", "B"), new ByteArrayResource(IOUtils.toByteArray(is), "UTF-8"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return 0;
	}
	public int sendMail(CommandMap map, ByteArrayOutputStream bos) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
			String htmlContent = map.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			final InputStreamSource attachment = new ByteArrayResource(bos.toByteArray());
			messHelper.addAttachment(MimeUtility.encodeText("noti.pdf", "UTF-8", "B"), attachment);
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return 0;
	}
	
	//Method to replace the values for keys
	protected String readEmailFromHtml(String filePath, Map<String, String> input)
	{
	    String msg = readContentFromFile(filePath);
	    try
	    {
		    Set<Entry<String, String>> entries = input.entrySet();
		    for(Map.Entry<String, String> entry : entries) {
		        msg = msg.replace(entry.getKey().trim(), entry.getValue().trim());
		    }
	    }
	    catch(Exception exception)
	    {
	        exception.printStackTrace();
	    }
	    return msg;
	}
	//Method to read HTML file as a String 
	private String readContentFromFile(String fileName)
	{
		StringBuffer contents = new StringBuffer();
	    
	    try {
	      //use buffering, reading one line at a time
	    	BufferedReader reader =  new BufferedReader(new FileReader(fileName));
	    	try {
		        String line = null; 
		        while (( line = reader.readLine()) != null){
		          contents.append(line);
		          contents.append(System.getProperty("line.separator"));
		        }
	    	}
	    	finally {
	          reader.close();
	    	}
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    return contents.toString();
	}
	public int sendPdfMail(CustomMap emailMap, ByteArrayOutputStream bos,Locale locale) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(SpringPropertiesUtil.getProperty("cert1.mail.title",locale)+"("+SpringPropertiesUtil.getProperty("usr-01-03.sub1.lbl2",locale)+" : "+emailMap.getString("transaction_id")+")");
			String htmlContent = emailMap.getString("content");
			messHelper.setText(htmlContent,true);
			messHelper.setFrom(emailMap.getString("sendermail"), "NPPA");
			messHelper.setTo(new InternetAddress(emailMap.getString("response_email"), SpringPropertiesUtil.getProperty("adm-08-01.table.col2",locale), "UTF-8"));
			final InputStreamSource attachment = new ByteArrayResource(bos.toByteArray());
			messHelper.addAttachment(MimeUtility.encodeText("noti.pdf", "UTF-8", "B"), attachment);
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return 0;
	}
	public int sendMailFromAlarmHtml(CommandMap map, Map<String, String> input, HttpServletRequest req) {
//		String serverUrl = map.getString("serverUrl","");
		int ret=0;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMultipart multipart = new MimeMultipart();
	        BodyPart messageBodyPart = new MimeBodyPart();
	        
	        //HTML mail content
	        String htmlText = readEmailFromHtml(req.getSession().getServletContext().getRealPath("/resource/html/alarm-email.html"),input);
	        messageBodyPart.setContent(htmlText, "text/html; charset=KSC5601");
	        multipart.addBodyPart(messageBodyPart); 
	        
	        messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-logo.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID","<image>");

            multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();
            DataSource fds1 = new FileDataSource(req.getSession().getServletContext().getRealPath("/resource/img/mail/mail-pattern.png"));  
            messageBodyPart.setDataHandler(new DataHandler(fds1));
            messageBodyPart.setHeader("Content-ID","<mailPattern>");

            multipart.addBodyPart(messageBodyPart);
            
			MimeMessageHelper messHelper = new MimeMessageHelper(message,true,"UTF-8");
			messHelper.setSubject(map.getString("subject"));
//			String htmlContent = map.getString("content");
			message.setContent(multipart);
			messHelper.setFrom(map.getString("senderMail"), map.getString("senderNm"));
			messHelper.setTo(new InternetAddress(map.getString("receivorMail"), map.getString("receivorNm"), "UTF-8"));
			log.debug("MailSender started");
		    mailSender.send(message);
		}catch(MailException e) {
			log.debug("MailException occurred");
			return 1;
		}catch(Throwable e) {
			log.debug("Throwable occurred");
			return 2;
		}
		log.debug("MailSender end");
		return ret;

	}
	
}
