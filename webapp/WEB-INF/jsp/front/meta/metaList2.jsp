<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${metaM.meta_m_nm}"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
				<span>${title} 조회</span>
			</div>
			
			<div class="page-content-wrap">
			<form name="frm" id="frm" method="post">
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="id_meta_m" name="id_meta_m" value="${param.id_meta_m}"/>
				<input type="hidden" id="trg_id_meta_m" name="trg_id_meta_m" value="${param.trg_id_meta_m}"/>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
			              	<div class="tit_cont">검색조건</div>
			              	<div>
					          	<select class="form-control mh-form-control" name="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${aloneList}" varStatus="i">
					                <option value="${map.meta_nm}" ${(param.search_col eq map.meta_nm? 'selected':'')}>${map.meta_nm}</option>
					          	</c:forEach>
					       		</select>
			          		</div>
							<div class="search-group-single-input">
		              			<div class="tit_cont">검색어</div>
		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${param.search_keyword}">
										<button type="button" class="btn-primary" onclick="javascript:search();">
											<span class="btn_search"></span>
											검색
										</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
						<button type="button" class="btn h34 fl" onClick="javascript:downExcelForm(${param.id_meta_m});"><span class="btn_add"></span>엑셀 양식 다운로드</button>
					</div>
					<div class="r">
                          <button type="button" class="btn attach1" onclick="document.getElementById('excel_file1').click();"><span class="btn_attach"></span>엑셀 업로드</button>
                          <input type="file" name="excel_file" id="excel_file1" class="file_input_hidden" onchange="javascript: uploadExcel(this,${param.id_meta_m})"/>
 						  <button type="button" class="btn h34 fl" onClick="javascript:openRegMeta();"><span class="btn_add"></span>수동입력</button>
					</div>					
				</div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty idenMap.grid_width}">
				    <c:set var="grid_width" value="style='width:${idenMap.grid_width}%;'" />
				</c:if>
	            <div class="grid-area" ${grid_area_style}>
                    <!-- 그리드 해더영역 -->
                    <div class="grid-header" ${grid_width}>
                        <c:forEach var="row" items="${headerList}" varStatus="i">
                        	<c:forEach var="map" items="${row}" varStatus="j">
                        <div class="header-unit">
                        <input type="hidden" id="h_${map.meta_cd}" value="${map.meta_cd}">
                        ${map.meta_nm}<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
                        </div>각 div별로 style: 사이즈 잡아줍니다. 
                        	</c:forEach>
                        </c:forEach>
                    </div>
                    <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
                    <div class="grid-contents" ${grid_width}>
                    <c:forEach var="low" items="${list}" varStatus="i">
                        <div class="grid-loof-cont" id="row_${i.count}" ondblclick="javascript:openRegMeta('${low.id_meta_row}')">
                        <c:forEach var="col" items="${headerList}" varStatus="j">
<%--                         col=[${col}]<br> --%>
<%--                         low=[${low}] --%>
                        	<c:set var="meta_cd" value="${col.meta_cd}"/>
                         <div class="cont-unit">
                         <c:choose>
                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
                         	${low[meta_cd]}
                         	</c:when>
                         	<c:otherwise>
                         	<a href="${low[meta_nm]}" target="blank">${low[meta_nm]}</a>
                         	</c:otherwise>
                         </c:choose>
           				 </div>
                    	</c:forEach>
                        </div>
                    </c:forEach>
                    </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
	            <table>
	            	<colgroup >
                    <c:forEach var="map" items="${aloneList}" varStatus="i">
                    	<col width="30%"/>
                    </c:forEach>
	            	</colgroup>
	            	<tbody>
                        <c:forEach var="row" items="${headerList}" varStatus="i">
                        	${row}
			            	<tr>
		                    </tr>
                    	</c:forEach>
	            	</tbody>
	            </table>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
                   <!-- //paging -->
			</form>
	    	</div>
<script>
function doList(page){
	$("#page").val(page);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/metaList.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
function search(){
	doList(1);
}
function initSearchForm(){
	$("select[name=search_col] option:eq(0)").attr("selected","selected");
	$("input[name=search_keyword]").val('')
	$("#search_keyword").val('');
}
function openRegMeta(id_meta_row){
	if(id_meta_row){
		$("#id_meta_row").val(id_meta_row);
	}
	$("#page").val(page);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/regMetaData.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
	
}
function downExcelForm(id_meta_m) {
	var params={id_meta_m:id_meta_m}
    // 서버에서 엑셀 파일을 다운로드 받습니다.
    $.ajax({
        url: contextPath+"/front/meta/downloadAjax.do",
        data: params,
        method: "GET",
        responseType: 'blob', // 응답 데이터 타입을 'blob'으로 설정
        success: function (data) {
        	alert('성공');
        },
        error: function (error) {
            console.error("다운로드 에러:", error);
        }
    });
// 	if(id_meta_m){
// 		$("#id_meta_m").val(id_meta_m);
// 	}
// 	var frm =document.frm;
// 		frm.action=contextPath+"/front/meta/download.do";
// 		frm.submit();
}
function uploadExcel(that,id_meta_m){
	var formData = new FormData();
	formData.append('id_meta_m', id_meta_m); // 추가 데이터

	var fileInput = document.getElementById('excel_file1'); // 파일 입력 필드
	var file = fileInput.files[0]; // 업로드할 파일
	formData.append('excel_file', file);
    // 서버에서 엑셀 파일을 다운로드 받습니다.
    $.ajax({
        url: contextPath+"/front/meta/uploadAjax.do",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function (data) {
        	$("#excel_file1").val('');
        	alert('성공');
        },
        error: function (error) {
        	$("#excel_file1").val('');
            console.error("다운로드 에러:", error);
        }
    });
	
}
</script>
</front:FrontDynamicBody>
</html>