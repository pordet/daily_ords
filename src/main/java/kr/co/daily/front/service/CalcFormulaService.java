package kr.co.daily.front.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.front.dao.CalcFormulaDAO;

@Service
public class CalcFormulaService {
	@Autowired private CalcFormulaDAO dao;

	public CustomMap getFormulaByIdMeta(CommandMap map) {
		return dao.getFormulaByIdMeta(map.getMap());
	}

}
