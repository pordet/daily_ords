<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="utf-8" src="<c:url value='/resource/js/adm/reg/resource_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:choose>
					<c:when test="${empty param.id_resource_role}">
						<span id="cur_title">권한 등록</span>
					</c:when>
					<c:otherwise>
						<span id="cur_title">권한 수정</span>
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="page-content-wrap">
			
			<form id="frm" name="frm" method="post">
				<input type="hidden" name="id_menu" value="${param.id_menu}">
				<input type="hidden" name="page" id="page" value="${param.page}">
				<input type="hidden" id="doubleYn" value="Y">
				<input type="hidden" id="resource_type_url" value="${((map.resource_type eq 'url' || map.resource_type eq null) ?'Y':'N')}">
				<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
				<input type="hidden" name="id_resource_role" id="id_resource_role" value="${param.id_resource_role}">
				<input type="hidden" name="id_resource" id="id_resource" value="${param.id_resource}">
			  	<input type="hidden" name="search_col" id="search_col" value="${param.search_col}">
			  	<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
				
				<div class="adm-register-form">
					<table>
						<colgroup>
							<col width="150px;">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<td>리소스 유형</td>
								<td>
								<c:choose>
									<c:when test="${empty param.id_resource_role}">
										<select name="resource_type" id="resource_type" onchange="javascript:changeType(this);">
											<option value="url" ${((map.resource_type eq 'url' || map.resource_type eq null) ?'selected':'')}>url</option>
											<option value="menu" ${((map.resource_type ne null && map.resource_type ne 'url')?'selected':'')}>menu</option>
										</select>
									</c:when>
									<c:otherwise>
										<input type="hidden" name="resource_type" value="${map.resource_type}"/>
										<select id="resource_type" onchange="javascript:changeType(this);" disabled>
											<option value="url" ${((map.resource_type eq 'url' || map.resource_type eq null) ?'selected':'')}>url</option>
											<option value="menu" ${((map.resource_type ne null && map.resource_type ne 'url')?'selected':'')}>menu</option>
										</select>
									</c:otherwise>
								</c:choose>
								</td>
							</tr>
							<tr>
								<td>Role</td>
								<td>
									<select name="id_role" id="id_role">
										<c:forEach var="cg" items="${roleList}">
											<c:choose>
												<c:when test="${map.id_role eq cg.id_role}">
													<option value="${cg.id_role}" selected>${cg.role_nm}</option>
												</c:when>
												<c:otherwise>
													<option value="${cg.id_role}">${cg.role_nm}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td id="pattern_title">
									${((map.resource_type eq 'url' || map.resource_type eq null) ?'Url 패턴':'메뉴 Depth / 메뉴명')}
								</td>
								<td>
									<div id="menu_pattern" style="display:${((map.resource_type eq 'url' || map.resource_type eq null) ?'none':'block')}">
										<select name="code_id_menu" id="code_id_menu">
											<c:forEach var="menu" items="${menuList}">
												<option value="${menu.id_menu}" ${(!(map.resource_type eq 'url' || map.resource_type eq null) && map.resource_pattern eq menu.id_menu ?'selected':'')}>
													메뉴 Depth : ${menu.menu_depth} / 메뉴명 : ${menu.menu_nm}
												</option>
											</c:forEach>
											<c:if test="${fn.length(menuList) eq '0'}">
												<option value="" selected>등록할 메뉴가 없습니다.</option>
											</c:if>
										</select>
									</div>
									<div id="url_pattern" style="display:${((map.resource_type eq 'url' || map.resource_type eq null) ?'block':'none')}">
										<div class="single-row">
											<input type="text" id="resource_pattern" name="resource_pattern" value="${map.resource_pattern}">
											<button type="button" id="check_title" class="btn-primary" onclick="javascript:checkDouble();"
												<c:if test="${param.insert_yn eq 'N'}"> disabled</c:if>>
												중복체크
											</button>
										</div>
									</div>
								</td>
							</tr>
							<tr id="tr_menu_desc" style="display:${(map.resource_type eq 'menu'?'':'none')}">
								<td>메뉴 설명</td>
								<td>
										<input type="text" id="menu_desc" value="${map.menu_desc}" disabled>
								</td>
							</tr>
							<tr>
								<td>권한 가중치</td>
								<td>
									<input type="hidden" name="sort_order"  id="sort_order" value="1">
									<div class="single-row" id="div_menu_sort">
										<input type="text" id="sort_order_dis" value="${map.sort_order}" placeholder="1:특정 url, 3:일반, 5:공통">
										<div class="row-sub-description">
											<span>(* 1: 특정url, 3: 일반, 5: 공통 )</span>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="board-bottom-group">
					<c:if test="${!is_guest}">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" id="btnInit">초기화</button>
							<button type="button" class="btn-danger" id="btnDelete">삭제</button>
							<button type="button" class="btn-primary" onclick="javascript:register();">저장</button>
						</div>
					</c:if>
					<c:if test="${is_guest}">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
						</div>
					</c:if>
				</div>
			</form>
			
			</div>
		</div>
	</div>
	
</adm:AdmBody>
<script>
var test = "${menuList}";
console.log("test")
console.log(test)

</script>
</html>
