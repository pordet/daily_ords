package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Repository
public interface MetaManageDAO {

	PageList<CustomMap> selectMetaMList(Map<String, Object> map, PageBounds pageBounds);

	CustomMap getMetaM(Map<String, Object> map);

	void insertMetaM(Map<String, Object> map);

	void modifyMetaM(Map<String, Object> map);

	CustomMap selectMeta(Map<String, Object> map);

	List<CustomMap> selectMetaByIdMetaM(Map<String, Object> map);

	List<CustomMap> selectMetaRelByParentIdMetaM(Map<String, Object> map);

	List<CustomMap> selectMetaKeyMByIdMetaRel(Map<String, Object> map);

	List<CustomMap> selectMetaKeyListByIdKey(Map<String, Object> map);

	List<CustomMap> selectSubMetaColByKeyList(List<Integer> subKeyList);

	List<CustomMap> selectMetaFKBySlaveMeta(List<Integer> subKeyList);

	CustomMap getMetaByIdMetaMAndIdMeta(Map<String, Object> map);

	List<CustomMap> selectChildMetaListByIdParentMeta(Map<String, Object> map);

	void insertMeta(Map<String, Object> map);

	void modifyMeta(Map<String, Object> map);

	int getNextOrdByIdMetaMAndMetaDepth(Map<String, Object> map);

	List<String> selectMetaListByInIdMeta(Map<String, Object> map);

	void deleteMetaByList(Map<String, Object> map);

	void deleteMetaByIdMetaMList(Map<String, Object> map);

	void deleteMetaMByIdMetaMList(Map<String, Object> map);

}
