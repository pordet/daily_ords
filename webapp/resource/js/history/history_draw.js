function loadData(e,isTreeClicked,isInsert){
	if(nanToStr(e.map)!=""){
		$("#modi_id_board").val(e.map.id_board);
	}
	var html = '';
	if(nanToStr(e.keywordList)!=''){
		$.each(e.keywordList,function(i,d){
			html+='<div class="col-8 line" id="keyword_'+d.id_board_keyword+'">';
			html+='<div class="form-group inline-group">';
			if(nanToStr(d.keyword)!=''){
				html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+d.id_board_keyword+'" value="'+nanToStr(d.keyword)+'" disabled>';
			}else{
				html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+d.id_board_keyword+'" value="">';
			}
			html+='</div>';
			html+='</div>';
			html+='<div class="col-4 line" id="keyword_'+d.id_board_keyword+'_add" style="text-align:right;">';
			html+='    <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delKeywordNum('+d.id_board_keyword+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
			html+='</div>';
		});
		$("[id^=addKeyword_in_div_]").remove();
		$("[id^=delKeyword_in_div_]").remove();
		$("#key_btn_group").after(html);
	}
	var html = '';
	if(nanToStr(e.attachList)!=''){
		$.each(e.attachList,function(i,d){
			html+='<div class="col-8 line" id="attach_'+d.id_board_file+'">';
			html+='<div class="form-group inline-group">';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'" class="ico_download" style="color:#fff;"><img src="'+contextPath+'/resource/img/history/ico_download.png" alt="다운로드"/>다운로드</a> ';
			html+='	   <span class="span_cut">'+d.file_nm+'</span>';
			html+='</div>';
			html+='</div>';
			html+='<div class="col-4 line" id="attach_'+d.id_board_file+'_add" style="text-align:right;">';
			html+='    <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttachFile('+d.id_board_file+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
			html+='</div>';
		});
	}
	$("[id^=addFile_in_div_]").remove();
	$("[id^=delFile_in_div_]").remove();
	$("#file_list_div").append(html);
	addFile();
	if(!isTreeClicked && isInsert){
		$("#btnDelete").show();
   		if(nanToStr(e.list)!=''){
	   		var html='';
	   		list_redraw(e);
			draw_paging_by_ajax(e.paginator);
   		}

		$("#board_reg_type").html(e.board_path+" 조회/수정");
	}	
	if(!isInsert){
		$("#btnDelete").show();
		$("#board_reg_type").html(e.board_path+" 조회/수정");
   		if(nanToStr(e.list)!=''){
	   		var html='';
	   		list_redraw(e);
			draw_paging_by_ajax(e.paginator);
   		}
	}	
	if(!isTreeClicked){
		infoOpen('성공하였습니다.');
	}	
}

function list_redraw(e){
		if(e.list.length==0){
			$("#result_no").show();
			$("#result_header").hide();
			$("#result_list").hide();
			
		}else{
			$("#result_no").hide();
			$("#result_header").show();
			$("#result_list").show();
		}
		if(e.list.length>0){
			var summary = '검색결과  '+e.paginator.totalCount+' 개 중 '+e.list[0].rownum+' ~ '+e.list[e.list.length-1].rownum;
			$("#result_summary").html(summary);
		}
		$(".listboard-contents1").empty();
   			var html='<ul class="list-line">';
   		$.each(e.list,function(i,d){
   			html+='<li>';
   			html+='<div class="meta">'+d.board_path+', '+d.reg_date+'</div>';
   			html+='<div class="search-result-title"><a href="javascript:void(0);" onclick="javascript:viewData('+nanToStr(d.id_board_master)+','+d.id_board+',\''+d.ord_str+'\');">'+d.title+'</a>';
   			html+='	<div class="file-format inline-group">';
   	   		$.each(d.filelist,function(j,f){
   	   			html+='	<a href="'+contextPath+'/file/data_download.do?id_board_file='+f.id_board_file+'"><span class="'+f.file_sh_ext+'">'+f.file_ext+'</span></a>';
   	   		});
   			html+='	</div>';
   			html+='</div>';
   			html+='<div class="search-result-text txt_post">'+nanToStr(d.comment)+'</div>';
   			html+='</li>';
   		});
   		html+='</ul>';
   		$.each(e.countList,function(i,d){
   			$("#span_"+d.ord_str).html("("+d.board_cnt+")");
   		});
   		html+='</ul>';
  		
   		$(".listboard-contents1").html(html);
 //  		selInit();
}