<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<his:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_register.js'/>"></script>
</his:DataHead>
<his:DataTreeBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" hasLeft="true" type="${(empty param.type?'3':param.type)}" title="${master.title} ${master.board_nm}">
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;">
<%--                  <input type="hidden" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"> --%>
	                 <input type="hidden" name="id_master" value="${master.id_master}">
	                 <input type="hidden" name="id_board_master" value="${master.id_board_master}">
	                 <input type="hidden" name="id_board" id="id_board">
	                 <input type="hidden" name="attach_yn" value="${master.attach_yn}">
	                 <input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
	                 <input type="hidden" name="ord_str" value="${master.ord_str}">
                    	<div class="row">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색종류
<%-- 	                                  [${param.st_year }] --%>
	                                  </div>
										<select class="form-control mh-form-control" title="검색종류" name="search_type" id="search_type">
											<option value="t" ${(param.search_type eq 't'?'selected':'')}>제목</option>
											<option value="f" ${(param.search_type eq 'f'?'selected':'')}>파일명</option>
											</select>
	                                 </div>
		                              <div class="form-group inline-group" style="width: 300px">
		                                  <div class="tit_cont">검색어</div>
		                                  <input type="text" class="form-control mh-form-control" name="search_keyword" value=" ${(not empty param.search_keyword ?param.search_keyword:'')}">
		                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
	                       		<div class="fr">
	                              <button type="button" class="btn h34" onclick="javascript:registerData();"  style="margin-top:20px;">등록</button>
	                            </div>
	                            </div>
	                    </div>
	                </div>
	                       <div class="row">
								<div class="col-12">
				                    <div class="grid-area">
				                        <!-- 그리드 해더영역 -->
				                        <div class="grid-header">
<!-- 											<div class="header-unit header-check" style="width:5%;"> -->
<!-- 												<a href=""> -->
<!-- 													<div class="form-check mh-form-checkbox"> -->
<!-- 														<input class="form-check-input" type="checkbox" value="" id="defaultCheck1"/> -->
<!-- 														<span class="mh-check"></span> -->
<!-- 														<label class="form-check-label" for="defaultCheck1" title=""></label> -->
<!-- 													</div> -->
<!-- 												</a> -->
<!-- 											</div>각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:10%;text-align: center;"><a href="">순서<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:50%;text-align: center;"><a href="">제목<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:20%;text-align: center;"><a href="">조회수<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
				                            <div class="header-unit1" style="width:20%;text-align: center;"><a href="">등록일<ctrl:orderMark val="5" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                        </div>
				                        <!-- //그리드 해더영역 -->
				                        <!-- 그리드 컨텐츠영역 -->
				                        <div class="grid-contents">
				                            <!--loof-->
				                            <c:forEach var="map" items="${list}" varStatus="i">
				                            <div class="grid-loof-cont" ondblclick="javascript:viewData(${map.id_board});" style="hover:#ff0000;">
				                                <div class="cont-unit" style="width:10%;text-align: center;">${map.rownum}</div>
				                                <div class="cont-unit" style="width:50%">${map.title}</div>
				                                <div class="cont-unit" style="width:20%;text-align: center;">
				                                ${map.view_cnt}
<%-- 													<c:choose> --%>
<%-- 														<c:when test="${map.file_type eq 'F' }"> --%>
<%-- 														<a href="<c:url value='/file/download.do'/>?id_data=${map.id_data}">${map.file_nm}</a> --%>
<%-- 														</c:when> --%>
<%-- 														<c:otherwise> --%>
<%-- 														<a href="${map.file}" download="${map.file_nm }">재생</a> --%>
<%-- 														</c:otherwise> --%>
<%-- 													</c:choose> --%>
				                                </div>
				                                <div class="cont-unit" style="width:20%;text-align: center;">
												${map.reg_date}
				                                </div>
				                            </div>
				                            </c:forEach>
				                            <!--//loof-->
				                        </div>
				                        <!-- //그리드 컨텐츠영역 -->
				                    </div>
				                    <!--//그리드영역-->
				                    </div>
				                    <!-- //그리드 컨텐츠영역 -->
				                </div>
				                    <!-- paging -->
									<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
				                    <!-- //paging -->
			</form>
  <div class="modal fade mh-modal type05" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModal">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">데이터 등록</h3>
				<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<form name="reg_frm" id="reg_frm">
			<input type="hidden" id="reg_id_master" name="id_master" value="${(not empty param.id_master?param.id_master:1)}"/>
			<input type="hidden" id="reg_id_category" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"/>
			<div class="modal-body">
				<div class="cont_tit2">제목</div>
				<div class="form-group inline-group">
					<input type="text" class="form-control mh-form-control" name="title" onfocusout="checkStringFormatAndNull(this,500);">
				</div>
				<div class="cont_tit2">내용</div>
				<div class="form-group inline-group">
					<textarea class="form-control mh-form-control" name="comment" id="vi_comment" rows="6" style="height:240px;" onfocusout="checkStringFormatAndNull(this,2500);"></textarea>
				</div>
				<c:choose>
					<c:when test="${master.attach_yn eq 'Y' and master.multi_attach_yn eq 'N'}">
						<div class="cont_tit2">파일</div>
		                 <div class="form-group inline-group">
		                     <input type="text" id="claim_file" class="form-control mh-form-control" readonly>
		                     <div class="attach-btn">
		                       <button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>
		                       <input type="file" name="claim_file" id="claim_file_hidden" class="file_input_hidden" onchange="javascript: document.getElementById('claim_file').value = this.value"/>
		                     </div>
		                 </div>
						<div class="btn_area">
							<button type="button" class="btn2" onclick="register();">등록</button>
							<button type="button" class="btn2" data-dismiss="modal">닫기</button>
						</div>
					</c:when>
					<c:when test="${master.attach_yn eq 'Y' and master.multi_attach_yn eq 'Y'}">
                        <div class="row mt-40p">
						<div class="cont_tit2">파일</div>
                                      <div class="col-8 line" id="addFile_in_div_1">
                                          <div class="form-group inline-group">
                                              <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" disabled>
                                              <div class="attach-btn">
                                                <button type="button" class="btn attach"><span class="btn_attach"></span><spring:message code="comm.btn2"/></button>
                                                <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
                                              </div>
                                          </div>
                                      </div>
								<div class="col-4 line" id="delFile_in_div_1">
			                                          <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile(1);"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
			                                      </div>
			                                      <c:forEach var="file" items="${attachList}" varStatus="i">
								<div class="col-8 line" id="attach_${file.file_num}">
			                                          <div class="form-group inline-group">
			                                              <a href="data:${file.content_type};base64,${file.file}" class="ico_download" download="${file.file_name}"><img src="<c:url value='/resource/img/ico_download.png'/>" alt="delete"/> <spring:message code="comm.btn5"/></a> 
										<span>${file.file_name}</span>
			                                          </div>
			                                      </div>
								<div class="col-4 line" id="attach_${file.file_num}_add">
			                                          <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttach(${file.file_num});"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
			                                      </div>
                                      </c:forEach>
                         </div>
					</c:when>
				</c:choose>
			</form>
		</div>
	</div>
  </div>
  <div class="modal fade mh-modal type05" id="dataViewModal" tabindex="-1" role="dialog" aria-labelledby="dataViewModal">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">조회/수정</h3>
				<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<form name="modi_frm" id="modi_frm">
			<input type="hidden" id="modi_id_master" name="id_master" value="${(not empty param.id_master?param.id_master:1)}"/>
			<input type="hidden" id="modi_id_category" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"/>
			<input type="hidden" id="modi_id_data" name="id_data"/>
			<div class="modal-body">
				<div class="cont_tit2">제목</div>
				<div class="form-group inline-group">
					<input type="text" class="form-control mh-form-control" name="title" id="modi_title" onfocusout="checkStringFormatAndNull(this,500);">
				</div>
				<div class="cont_tit2">내용</div>
				<div class="form-group inline-group">
					<textarea class="form-control mh-form-control" name="comment" rows="6" style="height:240px;" id="modi_comment" onfocusout="checkStringFormatAndNull(this,2500);"></textarea>
				</div>
				<c:choose>
					<c:when test="${master.attach_yn eq 'Y' and master.multi_attach_yn eq 'N'}">
						<div class="cont_tit2 mt-20p">파일</div>
						<div class="form-group inline-group" id="modi_file_div">
		                     <input type="text" id="modi_file" class="form-control mh-form-control" readonly>
		                     <div class="attach-btn">
		                       <button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>
		                       <input type="file" name="modi_file" id="modi_file_hidden" class="file_input_hidden" onchange="javascript: document.getElementById('modi_file').value = this.value"/>
		                     </div>
		                     <div id="view_down_div">
		                     </div> 
		                </div>
					</c:when>
					<c:when test="${master.attach_yn eq 'Y' and master.multi_attach_yn eq 'Y'}">
						<div class="cont_tit2">파일</div>
                                      <div class="col-8 line" id="addFile_in_div_1">
                                          <div class="form-group inline-group">
                                              <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" disabled>
                                              <div class="attach-btn">
                                                <button type="button" class="btn attach"><span class="btn_attach"></span><spring:message code="comm.btn2"/></button>
                                                <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
                                              </div>
                                          </div>
                                      </div>
								<div class="col-4 line" id="delFile_in_div_1">
			                                          <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile(1);"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
			                                      </div>
			                                      <c:forEach var="file" items="${attachList}" varStatus="i">
								<div class="col-8 line" id="attach_${file.file_num}">
			                                          <div class="form-group inline-group">
			                                              <a href="data:${file.content_type};base64,${file.file}" class="ico_download" download="${file.file_name}"><img src="<c:url value='/resource/img/ico_download.png'/>" alt="delete"/> <spring:message code="comm.btn5"/></a> 
										<span>${file.file_name}</span>
			                                          </div>
			                                      </div>
								<div class="col-4 line" id="attach_${file.file_num}_add">
			                                          <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttach(${file.file_num});"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
			                                      </div>
                                      </c:forEach>
					</c:when>
				</c:choose>
 				<div class="btn_area">
					<button type="button" class="btn3" onclick="delData();">삭제</button>
					<button type="button" class="btn2" onclick="modifyData();">수정</button>
					<button type="button" class="btn2" data-dismiss="modal">닫기</button>
				</div>
			</div>
			</form>
		</div>
	</div>
  </div>
  <div class="modal fade mh-modal type05" id="movieViewModal" tabindex="-1" role="dialog" aria-labelledby="movieViewModal">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">동영상보기</h3>
				<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<video controls><source id="mov_src" src="${map.file}"></video>
 				<div class="btn_area">
					<button type="button" class="btn3" onclick="delData();">삭제</button>
					<button type="button" class="btn2" onclick="modifyData();">수정</button>
					<button type="button" class="btn2"  data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
  </div>
			
</his:DataTreeBody>
<script>
function registerData(){
	var frm =document.frm;
	frm.action=contextPath+"/regHistory.do";
//	frm.action="/excelDownload.do";
	$("#frm").submit();
}

function viewData(id_board){
	$("#id_board").val(id_board);
	var frm =document.frm;
	frm.action=contextPath+"/regHistory.do";
//	frm.action="/excelDownload.do";
	$("#frm").submit();
}

function registerData_bak(){
	$("#dataModal").modal('show');	
}

function changeCategory(that){
	var new_val = $(that).val();
	search();
}
$('#dataModal').on('hidden.bs.modal', function () {
	refresh();
});
$('#dataViewModal').on('hidden.bs.modal', function () {
	refresh();
});
function refresh(){
	var frm =document.frm;
	frm.action=contextPath+"/history.do";
		$("#page").val(page);
//	frm.action="/excelDownload.do";
	$("#frm").submit();
	
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/history.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
</script>
</html>