package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.domain.LineVO;
import kr.co.daily.admin.domain.RelColVO;
import kr.co.daily.admin.domain.SvgVO;
import kr.co.daily.admin.domain.TableDataVO;

@Repository
public interface IdentityMetaManageDAO {

	PageList<CustomMap> selectMetaRelMList(Map<String, Object> map, PageBounds pageBounds);

	PageList<CustomMap> selectMetaMList(Map<String, Object> map, PageBounds pageBounds);

	CustomMap getMetaMByIdMetaM(Map<String, Object> map);

	List<CustomMap> selectMetaListByIdMetaM(Map<String, Object> map);

	void insertIdentityMetaRelM(Map<String, Object> map);

	void insertIdentityMetaRel(RelColVO relCol);

	void insertIdentityMetaFkSvg(SvgVO srcVO);

	void insertIdentityMetaFkLine(LineVO lineVO);

	void insertIdentityMetaFkPosi(TableDataVO tab);

	List<CustomMap> selectIdentityMetaFkPosi(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaFkSvg(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaFkLine(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaFkM(Map<String, Object> map);

	List<CustomMap> selectIdentityMetaList(Map<String, Object> map);

}
