package kr.co.daily.admin.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface UsrDAO {

	void insertUsr(Map<String, Object> map);

	void deleteUsr(Map<String, Object> map);

	CustomMap getUser(Map<String, Object> map);

	PageList<CustomMap> selectUsrList(Map<String, Object> map, PageBounds pageBounds);


}
