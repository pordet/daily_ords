package kr.co.daily.common.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import daily.common.base.AbstractServiceBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.domain.User;
import kr.co.daily.usr.dao.GuestDAO;

@Service
public class UserService extends AbstractServiceBase implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class); 

	@Autowired private GuestDAO usrDAO;
    
	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("username : " + username); 
		logger.info("여기?");

		CommandMap commandMap = new CommandMap();
		commandMap.put("id", username);
		CustomMap user = usrDAO.loginAction(commandMap .getMap());
		if(user == null ) {
			logger.info("아이디 없음");
			throw new UsernameNotFoundException(username);
		}
		List<GrantedAuthority> gas = new ArrayList<GrantedAuthority>();
		List<CustomMap> authList = usrDAO.getUserAuth(commandMap.getMap());
		for(CustomMap auth : authList) {
			gas.add(new SimpleGrantedAuthority(auth.getString("role_nm").toString()));
		}
		return new User(user.get("id").toString(), user.get("pwd").toString(), user,gas);
	}

}
