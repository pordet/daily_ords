package daily.common.exception.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.ModelAndView;

import daily.common.exception.PageNotFoundException;
import daily.common.exception.PermissionDeniedException;
import daily.common.exception.ResourceFileNotFoundException;
import daily.common.exception.handler.ErrorResponse;

@SuppressWarnings("serial")
@ControllerAdvice
public class GlobalExceptionHandler extends RuntimeException {
	private static String ERR_GENERAL;			// default
    private static String ERR_SECURITY;
    @SuppressWarnings("unused")
	private static int MAX_ROW_COUNT;

//    @Value("$global['mybatis.pagination.count.per.page']")
    @Value("#{global['mybatis.pagination.count.per.page']}")
    public void setMaxRowCount(int maxRowCount) {
        MAX_ROW_COUNT = maxRowCount;
    }
	
	@Value("#{config['err.general']}")
    public void setErrorGen(String err) {
		ERR_GENERAL = err;
    }
	@Value("#{config['err.SecurityException']}")
    public void setErrorSecu(String err) {
		ERR_SECURITY = err;
    }
	@ExceptionHandler(SecurityException.class)
	public ModelAndView handlerSecurityException(HttpServletRequest req,HttpServletResponse resp,Exception e) throws Exception{
	    ModelAndView mnv = new ModelAndView(ERR_SECURITY);
        mnv.addObject("exception.message", e.getMessage());
        return mnv;
	}
	@ExceptionHandler(Exception.class)
	public ModelAndView handlerException(HttpServletRequest req,HttpServletResponse resp,Exception e) throws Exception{
	    ModelAndView mnv = new ModelAndView(ERR_GENERAL);
        mnv.addObject("message", e.getMessage());
        return mnv;
	}
	@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT)
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView handlerMaxUploadSizeExceededException(HttpServletRequest req,HttpServletResponse resp,Exception e,Model model) throws Exception{
		if(req.getHeader("AJAX")!=null && Boolean.valueOf(req.getHeader("AJAX"))) {
			ModelAndView mnv = new ModelAndView("jsonView");
	        mnv.addObject("errCode", "701");
		    return mnv;
		}else {
			ModelAndView mnv = new ModelAndView(ERR_GENERAL);
	        mnv.addObject("message", e.getMessage());
	        return mnv;
		}
 	}
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> handleRuntimeException(final RuntimeException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorResponse> handleAccessDeniedException(final AccessDeniedException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.FORBIDDEN, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(PermissionDeniedException.class)
    public ResponseEntity<ErrorResponse> handlePermissionDeniedException(final PermissionDeniedException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.FORBIDDEN, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(PageNotFoundException.class)
    public ResponseEntity<ErrorResponse> handlePageNotFoundException(final PageNotFoundException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceFileNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceFileNotFoundException(final ResourceFileNotFoundException ex) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
