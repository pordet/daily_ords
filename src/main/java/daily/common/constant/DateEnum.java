package daily.common.constant;

import java.util.HashMap;
import java.util.Map;

public enum DateEnum {
	DAY("1","1Day"), WEEK("2","1Week"),MONTH("3","1Month"), YEAR("4","1Year"), YEAR3("5","3Year");
	
	private String code;
	private String description;
	
	private static final Map<String,DateEnum> MAP;
	
	static {
		MAP = new HashMap<String,DateEnum>();
		
		for(DateEnum type : DateEnum.values()) {
			MAP.put(type.code, type);
		}
	}
	
	private DateEnum(String code,String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
