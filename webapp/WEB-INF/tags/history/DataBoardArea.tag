<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="id_master" required="false" %>
<%@ attribute name="id_board" required="false" %>
<%@ attribute name="sel_ord_str" required="false" %>
<%@ attribute name="data_type" required="false" %>
<%@ attribute name="keyword" required="false" %>
<%@ attribute name="top_style" required="false" %>

<commc:LeftBoardTag  id_master="${id_master}" keyword="${keyword}" listName="leftList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <aside class="mh-side-nav" style="${top_style}">
            <section class="mh-sn-bar">
                <!-- side bar -->
                <div class="main-tree">
					<div class="tree-content">
<!-- 							1단 -->
                        <ul class="comm-tree">
						<c:forEach var="item" items="${leftList}">
						<c:choose>
							<c:when test="${item.child_cnt eq 0}">
								<li><div style="margin-bottom:3px;"><a href="javascript:void(0);" onclick="goByTree(this,${item.lv},'${item.ordStr}')" id="sel_icon_${item.ordStr}"><i class="icon_keyboard"></i>${item.boardNm} (<span id="span_${item.ordStr}">${item.boardCnt}</span>)</a></div></li>
							</c:when>
							<c:otherwise>
						    <li class="comm-treeview" id="treeli_${item.lv}_${item.ordStr}">
						        <button type="button" class="comm-tree-expander" onclick="goByTreeByBtn(this,${item.lv},'${item.ordStr}')"></button>
								<div style="margin-bottom:3px;"><a href="javascript:void(0);" onclick="goByTree(this,${item.lv},'${item.ordStr}')" id="sel_icon_${item.ordStr}"><i class="icon_folder06"></i>${item.boardNm} (<span id="span_${item.ordStr}">${item.boardCnt}</span>)</a></div>
								<ul class="comm-treeview-list"  id="treels_${item.lv}_${item.ordStr}" style="display:none;">
								    <c:set var="nowNum" value="${fn:length(item.child)}"/>
									<c:forEach var="child" items="${item.child}">
									    <c:choose>
									    	<c:when test="${child.child_cnt eq 0}">
												<li><div style="margin-bottom:3px;"><a href="javascript:void(0);" onclick="goByTree(this,${child.lv},'${child.ordStr}')" id="sel_icon_${child.ordStr}"><i class="icon_keyboard"></i>${child.boardNm} (<span id="span_${child.ordStr}">${child.boardCnt}</span>)</a></div></li>
									    	</c:when>
									    	<c:otherwise>
											    <li class="comm-treeview" id="treeli_${child.lv}_${child.ordStr}"><button type="button" class="comm-tree-expander" onclick="goByTreeByBtn(this,${child.lv},'${child.ordStr}')"></button>
													<div style="margin-bottom:3px;"><a href="javascript:void(0);" onclick="goByTree(this,${child.lv},'${child.ordStr}')" id="sel_icon_${child.ordStr}"><i class="icon_folder06"></i>${child.boardNm} (<span id="span_${child.ordStr}">${child.boardCnt}</span>)</a></div>
													<ul class="comm-treeview-list" id="treels_${child.lv}_${child.ordStr}" style="display:none;">
													    <c:set var="nowNum" value="${fn:length(child.child)}"/>
														<c:forEach var="grand" items="${child.child}">
															<li><div style="margin-bottom:3px;"><a href="javascript:void(0);" onclick="goByTree(this,${grand.lv},'${grand.ordStr}')" id="sel_icon_${grand.ordStr}"><i class="icon_keyboard"></i>${grand.boardNm} (<span id="span_${grand.ordStr}">${grand.boardCnt}</span>)</a></div></li>
														</c:forEach>
													</ul>
												</li>
									    	</c:otherwise>
									    </c:choose>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
						</c:forEach>
						</ul>
                    </div>
				</div>                    
            </section>
        </aside>
<script>
function goByTree(that,lv,sel_ord_str){
	var parent = $(that).parent().parent();
	if(parent.hasClass("tree-open")){
		parent.removeClass("tree-open");
		parent.children(".comm-treeview-list").hide();
	}else{
		parent.addClass("tree-open");
		parent.children(".comm-treeview-list").show();
	}
	$("[id^=sel_icon_]").removeClass("seled");
	$("#sel_icon_"+sel_ord_str).addClass("seled");
	$("#sel_ord_str").val(sel_ord_str);
	doList(1,true,false);	
}
function goByTreeByBtn(that,lv,sel_ord_str){
	var parent = $(that).parent();
	if(parent.hasClass("tree-open")){
		parent.removeClass("tree-open");
		parent.children(".comm-treeview-list").hide();
	}else{
		parent.addClass("tree-open");
		parent.children(".comm-treeview-list").show();
	}
//	alert($(that).attr("id"));
//	location.href=contextPath+'/history.do?id_master='+id_master+'&id_board_master='+id_board_master+'&sel_ord_str='+sel_ord_str;
}

</script>