package crs;

public class GetOsInfo {

	public static void main(String[] args) {
        String osName = System.getProperty("os.name");
        String osVersion = System.getProperty("os.version");
        String osArch = System.getProperty("os.arch");

        // 출력
        System.out.println("운영 체제: " + osName);
        System.out.println("운영 체제 버전: " + osVersion);
        System.out.println("운영 체제 아키텍처: " + osArch);
    }

}
