<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메뉴 등록"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="utf-8" src="<c:url value='/resource/js/adm/reg/menu_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">

	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:choose>
					<c:when test="${empty param.trg_menu_id}">
						<span id="cur_title">메뉴 등록</span>
					</c:when>
					<c:otherwise>
						<span id="cur_title">메뉴 수정</span>
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm">
					<input type="hidden" id="doubleYn" value="Y">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="trg_id_menu" id="trg_id_menu" value="${map.id_menu}">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
				  	<input type="hidden" name="search_col" id="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
				  	<input type="hidden" name="search_parent_menu" id="search_parent_menu" value="${param.search_parent_menu}">
					<input type="hidden" name="page" id="page" value="${param.page}">
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>상위 Menu 이름</td>
									<td>
									<select name="id_parent_menu" id="id_parent_menu">
										<option value="0" ${(param.id_parent_menu eq '0' || empty param.id_parent_menu? 'selected':'')}>상위 메뉴없음</option>
										<c:forEach var="map" items="${parentMenuList}" varStatus="i">	
										<option value="${map.id_menu}" ${(param.id_parent_menu eq map.id_menu ? 'selected':'')}>${map.menu_nm}</option>
										</c:forEach>
									</select>

									</td>
								</tr>
								<tr>
									<td>Menu 이름</td>
									<td>
										<c:choose>
											<c:when test="${map eq null}">
												<div class="single-row">
													<input type="text" id="menu_nm" name="menu_nm" value="${map.menu_nm}">
												</div>
											</c:when>
											<c:otherwise>
												<input type="text"  id="menu_nm" name="menu_nm" value="${map.menu_nm}">
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td>메뉴 URL</td>
									<td>
										<input type="text" name="menu_url" id="menu_url" value="${map.menu_url}">
									</td>
								</tr>
								<tr>
									<td>메뉴 설명</td>
									<td>
										<input type="text" name="menu_desc" id="menu_desc" value="${map.menu_desc}">
									</td>
								</tr>
								<tr>
									<td>메뉴 순서</td>
									<td>
										<input type="text" name="sort_ord" id="sort_ord" value="${map.sort_ord}">
									</td>
								</tr>
								<tr>
									<td>메뉴 클래스</td>
									<td>
										 <input type="text" name="menu_style_class" id="menu_style_class" value="${map.menu_style_class}">
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="ra1" <c:if test="${empty map.use_yn or map.use_yn eq 'Y'}">checked</c:if> />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="ra2" <c:if test="${not empty map.use_yn and map.use_yn ne 'Y'}">checked</c:if>/>
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" id="btnList" onclick="doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" id="btnInit">초기화</button>
							<button type="button" class="btn-danger" id="btnDelete">삭제</button>
							<button type="button" class="btn-primary" id="btnSave">저장</button>
						</div>
					</div>
				</form>
	
			</div>
		</div>
		
	</div>
</adm:AdmBody>
</html>
