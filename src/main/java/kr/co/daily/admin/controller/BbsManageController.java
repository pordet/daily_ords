package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.BbsManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class BbsManageController  extends AbstractControllerBase{

	@Autowired private BbsManageService service;
	@RequestMapping("/adm/bbs/bbsList.do")
	public String main(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap.put("bbs_id_group", GlobalConst.CG_BBS_TYPE);
		PageList<CustomMap> dataList = service.selectBbsMaster(commandMap);
		putModelListPage(dataList,model);
		return "/adm/bbsList";
	}
	@RequestMapping("/adm/bbs/regBbsMaster.do")
	public String regBbsMaster(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = service.getBbsMaster(commandMap);
		model.addAttribute("map", map);
		return "/adm/reg/regBbsMaster";
	}
	@RequestMapping("/regChildBbsMaster.do")
	public String regChildBbsMaster(CommandMap commandMap, ModelMap model) throws Exception {
		List<CustomMap> dataList = service.selectDataMaster(commandMap);
		CustomMap map = service.getBbsMaster(commandMap);
		CustomMap parent = service.getParentBbsMaster(commandMap);
		
		model.addAttribute("dataList", dataList);
		model.addAttribute("map", map);
		model.addAttribute("parent", parent);
		return "adm/bbs/regBbsMaster";
	}
	
	@RequestMapping("/adm/bbs/insertBbsMasterAjax.do")
	public String insertBbsMasterAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        //if(!"".equals(commandMap.getString("id_parent_board",""))) {
	        //	commandMap.put("id_parent_board_master", commandMap.getString("id_parent_board",""));
	        //}
			service.insertBbsMaster(commandMap);
			model.addAttribute("map", commandMap.getMap());
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/bbs/modifyBbsMasterAjax.do")
	public String modifyBbsMasterAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			service.updateBbsMaster(commandMap);
			model.addAttribute("map", commandMap.getMap());
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/bbs/deleteBbsMaster.do")
	public String deleteBbsMaster(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_bbs_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_bbs_list", codelist);
				}
			}
	        commandMap = addParamByLoginInfo(commandMap);
			service.deleteBbsMaster(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		
		commandMap.put("bbs_id_group", GlobalConst.CG_BBS_TYPE);
		PageList<CustomMap> dataList = service.selectBbsMaster(commandMap);
		putModelListPage(dataList,model);
		return "/adm/bbsList";
	}
}
