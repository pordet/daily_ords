/*
function register(){
	$("[name^=file_desc_]")
	$.each($("[name^=file_desc_]"), function(i,item){
		if($("[name="+item.name+"]").val()==''){
			alertOpen('네트워크 설명에 값을 입력하십시오.');
		}
	});
		var formData = new FormData($('#reg_frm')[0]);
		console.log("formData");
		console.log(formData);
		
		var url = contextPath+"/adm/data/insertMultiFileAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
				console.log("받은목록");
				console.log(e)
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.rowList.length!=0){
		    			var btn_html="";
		    			var html="";
			    		$.each(e.rowList,function(i,d){
			    			if(i==0){
			    				html+='<img src="'+d.file+'" id="netImg">';
			    			}
			    			btn_html+='<a href="#none" class="midBtn btnEme" onclick="show_map('+d.ord+');return false">'+d.file_desc+'</a>';
			    		});
			    		$("#network_btn_list").html(btn_html);
			    		$("#network_img").html(html);
			    		infoOpen('성공하였습니다.');
		    		}else{
		    			
		    		}
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		
}
*/
/*
var attach_idx=0;
function delAttach(idx){
	attach_idx=idx;
	confirmOpen('삭제시 복구할 수 없습니다.', 'delAttachFile');
}
function delAttachFile(){
	var params = {id_data_file :attach_idx};
	$.ajax({
		url: contextPath+"/adm/data/deleteNetworkFileAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret == '0'){
			
	     	   $("#network_link_"+attach_idx).remove();
	    	   $("#addFile_in_div_"+attach_idx).remove();
			
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
	attach_idx = 0;
}
*/
function register(){
	var checker = true;
	var fileNameList = document.querySelectorAll(".fileName");
	for(var i=0; i<fileNameList.length; i++) {
		if(fileNameList[i].value == '') {
			alertOpen('파일을 등록하세요.');
			checker = false;
		}
	}
	if(!checker) return;

	var fileDescList = document.querySelectorAll(".fileDesc");
	for(var i=0; i<fileDescList.length; i++) {
		if(fileDescList[i].value == '') {
			alertOpen('설명을 입력하세요.');
			checker = false;
		}
	}
	if(!checker) return;
	
	if(checker) {
		var formData = new FormData($('#reg_frm')[0]);
		var delRow = [];
		var newRow = [];
		var modiRow = [];
		var lastOrder = 1;
		for(var i=0; i<newList.length; i++) {
			var item = newList[i];
			if(item.mode == 'create') { 
				if(item.ord > lastOrder) lastOrder = item.ord;
				var attachFile = document.querySelector("input[name=file_"+i+"]");
				item.attachFileName = attachFile.name;
				item.file_nm = attachFile.files[0].name;
				
			} else if (item.mode == "modify" && item.fileChange == "changed") {
				if(item.ord > lastOrder) lastOrder = item.ord;
				if(item.fileChange == "changed") {
					var attachFile = document.querySelector("input[name=file_"+i+"]");
					item.attachFileName = attachFile.name;
					item.file_nm = attachFile.files[0].name;
				}
			} else if((item.mode == "modify" && item.descChange == "changed")
				|| item.mode == "initial"
			) {
				if(item.ord > lastOrder) lastOrder = item.ord;
			}
		}
		for(var i=0; i<newList.length; i++) {
			var item = newList[i];
			if(item.mode == "delete") {
				delRow.push(item.id_data_file);
			} else if(item.mode =="create") {
				var newItem = {
					file_desc : item.newFileDesc,
					attachFileName : item.attachFileName,
					file_nm : item.file_nm,
				};
				//order, file여부 확인...
				newItem.ord = lastOrder + 1;
				lastOrder++;
				newRow.push(newItem);
			} else if(item.mode == "modify" 
				&& (item.fileChange == "changed" || item.descChange == "changed")
			) {
				var newItem = {
					id_data_file : item.id_data_file,
					fileChange : item.fileChange,
					descChange : item.descChange
				};
				if(item.fileChange == "changed") {
					newItem.attachFileName = item.attachFileName;
					newItem.file_nm = item.file_nm;
				}
				if(item.descChange == "changed") {
					newItem.file_desc = item.newFileDesc; 
				}
				//order, file여부 확인...
				newItem.ord = lastOrder + 1;
				lastOrder++;
				modiRow.push(newItem);
			}
		}
		
		if(delRow.length == 0 && newRow.length == 0 && modiRow.length == 0) {
			alertOpen('추가/변경된 내용이 없습니다.');
			return;
		}
		
		formData.append("deleteItem", JSON.stringify(delRow));
		formData.append("createItem", JSON.stringify(newRow));
		formData.append("modifyItem", JSON.stringify(modiRow));
		
		var url = contextPath+"/adm/data/insertMultiFileAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
					alertOpen('저장에 실패하였습니다.');
				}else{
	    			var btn_html="";
	    			var img_html="";
	
		    		if(e.rowList.length!=0){
			    		$.each(e.rowList,function(i,d){
			    			if(i==0){
			    				img_html += '<img src="'+d.file+'" id="netImg">';
								btn_html += '<div>'
				    			btn_html += '<button type="button" class="btn-info active" onclick="show_map('
											+i+');return false">' + d.file_desc + '</button>';
								btn_html += '<input type="hidden" id="hdn_img_'+ i +'" value="'+d.file+'">';
								btn_html += '</div>'
			    			} else {
								btn_html += '<div>'
			    				btn_html += '<button type="button" class="btn-info" onclick="show_map('
											+i+');return false">' + d.file_desc + '</button>';
								btn_html += '<input type="hidden" id="hdn_img_'+ i +'" value="'+d.file+'">';
								btn_html += '</div>'
							}
						});
		    		}else{
		    			//내용이 없는경우... 채울 더미??
		    		}
	
					$("#network_btn_list").html(btn_html);
			    	$("#network_img").html(img_html);
					
					newList = [];
					var list = e.rowList;
					if(list.length > 0) {
						list.sort(compare("ord"));
						for(var i=0; i<list.length; i++) {
							var item = list[i];
							var newItem = new Object;
							copyObjAttr(item, newItem);
							newItem.newFileName = "";
							newItem.newFileData = "";
							newItem.newFileDesc = "";
							newItem.fileChange = "initial";
							newItem.descChange = "initial";
							newItem.mode = "initial";
							newList.push(newItem);
						}
					} else {
						var newItem = {
							newFileName : '',
							newFileData : '',
							newFileDesc : '',
							fileChange : "initial",
							descChange : "initial",
							ord : 1, //임시...
							mode : 'create'
						}
						newList.push(newItem);
					}
					displayList(newList);
		    		infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		
	}
}