$(function () {
	$("#btnSave").click(function(){
		var params = $("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/adm/meta/insertIdentityMAjax.do";
		}else{
			url = contextPath+"/adm/meta/modifyIdentityMAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else if(e.ret==8){
	    			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    			return;
		    	}else if(e.ret==10){
	    			alertOpen('업데이트할 데이터가 없습니다.');
	    			return;
		    	}else{
		    		if(insert_yn =='Y'){
		    			$("#cur_title").html('메타 마스터 수정');
			    		$("#insert_yn").val('N');
		    		}
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	});
	$("#btnDelete").click(function(){
		confirmOpen('해당 메타 마스터를 삭제하시면 관련 권한들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteData");
	});
	$("#btnInit").click(function(){
		initForm();
	});
	$("#btnList,#toList").click(function(){
		var page = $("#page").val();
		var frm =document.frm;
			frm.action=contextPath+"/adm/meta/identityMList.do";
		if(page!="")
			frm.page.value=page;
		frm.submit();
	});
});
function initForm(){
	$("#cur_title").html('화면 마스터 등록');
	$("#trg_id_meta_m").val('');
	$("#insert_yn").val('Y');
	initInputs();
}

function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/meta/deleteIdentityMAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
// 	    	}else if(e.ret==8){
//     			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
//     			return;
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}


