package kr.co.daily.admin.service;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.CollectionUtil;
import daily.common.util.FileUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.BbsManageDAO;
import kr.co.daily.common.GlobalConst;

@Service
public class BbsManageService {

	@Autowired private BbsManageDAO dao;

	public List<CustomMap> selectDataMaster(CommandMap map) {
		return dao.selectDataMaster(map.getMap());
	}

	public PageList<CustomMap> selectBbsMaster(CommandMap map) {
		return dao.selectBbsMaster(map.getMap(),MybatisPagingUtil.pageBounds(map));
	}
	
	public void insertBbsMaster(CommandMap map) {
		dao.insertBbsMaster(map.getMap());
		List<CustomMap> metaList = dao.selectBoardMetaByStand(map.getMap());
		int ord = 1;
		for(CustomMap meta:metaList) {
			CommandMap mMap = CollectionUtil.getCommandMapByCustomMap(meta);
			mMap.put("id_meta_m", map.getInt("id_bbs_m"));
			if(!"".equals(map.getString("id_parent_bbs_m", ""))) {
				mMap.put("id_parent_meta_m", map.getInt("id_parent_bbs_m"));
			}
			mMap.put("login_usr_seq", map.getString("login_usr_seq",""));
        	if("reply_yn".equals(mMap.getString("meta_cd",""))) {
        		if("Y".equals(map.getString("reply_yn", "N"))) {
	    			mMap.put("use_yn", "Y");
	        	}else {
	    			mMap.put("use_yn", "N");
	        	}
	        }else if("attach_file".equals(mMap.getString("meta_cd",""))) {
    	        if("Y".equals(map.getString("attach_yn", "N"))) {
    		        if("Y".equals(map.getString("multi_attach_yn", "N"))) {
    		        	mMap.put("typ", GlobalConst.DT_MULTI_ATCH_FILE);
    		        }else {
    		        	mMap.put("typ", GlobalConst.DT_ATCH_FILE);
    		        }
	    			mMap.put("use_yn", "Y");
    	        }else {
	    			mMap.put("use_yn", "N");
    	        }
	        }
			dao.insertBoardMetaByStand(mMap.getMap());
			System.out.println(mMap.getString("meta_cd", "")+",ord:"+ord);
			ord++;
		}
		//if("".equals(map.getString("id_parent_board",""))) {
		//	dao.updateOrdStrAtFirstBoard(map.getMap());
		//}else {
		//	dao.updateOrdStrAfterInsertBbsMaster(map.getMap());
		//}
	}

	public void updateBbsMaster(CommandMap map) {
		if("Y".equals(map.getString("reply_yn", "N"))) {
			CommandMap s = new CommandMap();
	        s.put("id_meta_m", map.getString("id_bbs_m",""));
	        s.put("meta_cd", "reply_yn");
	        s.put("use_yn", "Y");
			s.put("login_usr_seq", map.getString("login_usr_seq",""));
			dao.updateBoardMetaByStand(s.getMap());
    	}else {
			CommandMap s = new CommandMap();
	        s.put("id_meta_m", map.getString("id_bbs_m",""));
	        s.put("meta_cd", "reply_yn");
	        s.put("use_yn", "N");
			s.put("login_usr_seq", map.getString("login_usr_seq",""));
			dao.updateBoardMetaByStand(s.getMap());
    	}
        if("Y".equals(map.getString("attach_yn", "N"))) {
			CommandMap s = new CommandMap();
	        s.put("id_meta_m", map.getString("id_bbs_m",""));
	        if("Y".equals(map.getString("multi_attach_yn", "N"))) {
	        	s.put("typ", GlobalConst.DT_MULTI_ATCH_FILE);
	        }else {
	        	s.put("typ", GlobalConst.DT_ATCH_FILE);
	        }
	        s.put("use_yn", "Y");
	        s.put("meta_cd", "attach_file");
			s.put("login_usr_seq", map.getString("login_usr_seq",""));
			dao.updateBoardMetaByStand(s.getMap());
        }else {
			CommandMap s = new CommandMap();
	        s.put("id_meta_m", map.getString("id_bbs_m",""));
	        s.put("meta_cd", "attach_file");
	        s.put("use_yn","N");
			s.put("login_usr_seq", map.getString("login_usr_seq",""));
			dao.updateBoardMetaByStand(s.getMap());
        }
		dao.updateBbsMaster(map.getMap());
 	}

	public void deleteBbsMaster(CommandMap map) {
		dao.deleteBbsData(map.getMap());
		dao.deleteBbsFile(map.getMap());
		dao.deleteBbsRow(map.getMap());
		dao.deleteBbsMeta(map.getMap());
		dao.deleteBbsMaster(map.getMap());
		String file_path = SpringPropertiesUtil.getProperty("board.file.upload.path")+File.separator+map.getString("id_meta_m","unknown");
		File bbsDir = new File(file_path);
		FileUtil.deleteDirectory(bbsDir);
	}

	public List<CustomMap> selectDataBbsMaster(CommandMap map) {
		return dao.selectDataBbsMaster(map.getMap());
	}

	public CustomMap getBbsMaster(CommandMap map) {
		return dao.getBbsMaster(map.getMap());
	}

	public CustomMap getParentBbsMaster(CommandMap map) {
		return dao.getParentBbsMaster(map.getMap());
	}

	public CustomMap getBbsM(CommandMap map) {
		return dao.getBbsM(map.getMap());
	}
		
}
