<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
        <div class="user-footer">
            <div class="user-footer-area">
                <span class="copyright-img"><img src="<c:url value='/resource/img/icon_gray.png'/>" alt="" /><em><spring:message code="usr-tail.01"/><br>
                <spring:message code="usr-tail.02"/></em></span>
                <span class="copyright-footer">Copyright &copy; <spring:message code="usr-tail.03"/></span>
            </div>
        </div>
