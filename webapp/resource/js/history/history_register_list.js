function register(){
	$("#modi_id_board_master").val($("#id_board_master").val());
//	$("#modi_sel_ord_str").val($("#sel_ord_str").val());
	$("#modi_page").val($("#page").val());
	$("#modi_search_keyword").val($("#search_keyword").val());
	var formData = new FormData($('#modi_frm')[0]);
	var url = '';
	var isInsert = false;
	if(nanToStr($("#modi_id_board").val())==''){
		if($("#modi_sel_ord_str").val('')==''){
			alertOpen('촤즉의 자료실 트리에서 자료실을 선택하십시오.');
			return;
		}
		isInsert = true;
		url += contextPath+"/history/insertHistoryFileAjax.do";
	}else{
		url += contextPath+"/history/modifyHistoryFileAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    enctype: 'multipart/form-data',
	    processData:false,
	    contentType:false,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		loadData(e,false,isInsert);
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function loadData(e,isTreeClicked,isInsert){
	if(nanToStr(e.map)!=""){
		$("#modi_id_board").val(e.map.id_board);
	}
	var html = '';
	if(nanToStr(e.keywordList)!=''){
		$.each(e.keywordList,function(i,d){
			html+='<div class="col-8 line" id="keyword_'+d.id_board_keyword+'">';
			html+='<div class="form-group inline-group">';
			if(nanToStr(d.keyword)!=''){
				html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+d.id_board_keyword+'" value="'+nanToStr(d.keyword)+'" disabled>';
			}else{
				html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+d.id_board_keyword+'" value="">';
			}
			html+='</div>';
			html+='</div>';
			html+='<div class="col-4 line" id="keyword_'+d.id_board_keyword+'_add" style="text-align:right;">';
			html+='    <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delKeywordNum('+d.id_board_keyword+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
			html+='</div>';
		});
		$("[id^=addKeyword_in_div_]").remove();
		$("[id^=delKeyword_in_div_]").remove();
		$("#key_btn_group").after(html);
	}
	var html = '';
	if(nanToStr(e.attachList)!=''){
		$.each(e.attachList,function(i,d){
			html+='<div class="col-8 line" id="attach_'+d.id_board_file+'">';
			html+='<div class="form-group inline-group">';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'" class="ico_download" style="color:#fff;"><img src="'+contextPath+'/resource/img/history/ico_download.png" alt="다운로드"/>다운로드</a> ';
			html+='	   <span>'+d.file_nm+'</span>';
			html+='</div>';
			html+='</div>';
			html+='<div class="col-4 line" id="attach_'+d.id_board_file+'_add" style="text-align:right;">';
			html+='    <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttachFile('+d.id_board_file+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
			html+='</div>';
		});
	}
	$("[id^=addFile_in_div_]").remove();
	$("[id^=delFile_in_div_]").remove();
	$("#file_list_div").append(html);
	addFile();
	if(!isTreeClicked && isInsert){
		$("#btnDelete").show();
   		$(".grid-contents1").empty();
   		var html='';
   		list_redraw(e);
		draw_paging_by_ajax(e.paginator);
		$("#board_reg_type").html("조회/수정");
	}	
	if(!isTreeClicked && !isInsert){
		$("#btnDelete").show();
		$("#board_reg_type").html("조회/수정");
   		$(".grid-contents1").empty();
   		var html='';
   		list_redraw(e);
		draw_paging_by_ajax(e.paginator);
	}	
	if(!isTreeClicked){
		infoOpen('성공하였습니다.');
	}	
}
function delData(){
	var params = $("#modi_frm").serialize();
	var url = contextPath+"/history/deleteHistoryAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$("#modi_id_board").val('');
	    		$("#modi_title").val('');
	    		$("#modi_comment").val('');
	    		$("[id^=attach_]").remove();
	    		$("[id^=keyword_]").remove();
	    		$("#btnDelete").hide();
	    		$("#board_reg_type").html("등록");
	    		addKeyword();
	       		list_redraw(e);
	    		draw_paging_by_ajax(e.paginator);
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function list_redraw(e){
		$(".grid-contents1").empty();
   		var html='';
   		$.each(e.list,function(i,d){
   			html+='<div class="grid-loof-cont">';
   			html+='<a href="javascript:void(0)" ondblclick="javascript:viewData('+d.id_board_master+','+d.id_board+');">';
   			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.rownum+'</div>';
   			html+='<div class="cont-unit" style="width:10%">'+nanToStr(d.board_nm)+'</div>';
   			html+='<div class="cont-unit" style="width:40%">'+nanToStr(d.title)+'</div>';
   			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.file_cnt+'</div>';
   			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.view_cnt+'</div>';
   			html+='<div class="cont-unit" style="width:20%;text-align: center;">'+nanToStr(d.reg_date)+'</div>';
   			html+='</a>';
   			html+='</div>';
   		});
   		$(".grid-contents1").html(html);
}