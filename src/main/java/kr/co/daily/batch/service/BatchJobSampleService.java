package kr.co.daily.batch.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import daily.common.util.CollectionUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.util.StrUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BidSearchService;
import reactor.core.publisher.Mono;

@Service
public class BatchJobSampleService {
	@Autowired private BidSearchService bidSearchService;
	@Autowired private BatchService batchService;
	private static Logger log = LoggerFactory.getLogger(BatchJobService.class);
	/**
	 * 주기적으로 실행 시키는 cron 속성
	 */
    //@Scheduled(cron = "00 06 23 * * *")		
    /**
     * 즉시실행시키는 fixedDelay 속성  , 1초가 1000
     * @throws ParseException 
     */
//    @Scheduled(initialDelay = 3000,fixedRate = 60000000)		
//    @Scheduled(cron = "00 12 25 * * *")		
    public void getMethodBatch() throws ParseException,Exception {
    	log.debug("Batch job start");
//    	String search_date = StringDateUtil.getMinusFromToday("yyyyMMdd",DateEnum.DAY,1);
    	String search_date="20230824";
//    	String search_date = StringDateUtil.getToday("yyyyMMdd");
		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
		int num_of_rows = Integer.parseInt(numOfRows);
		int curr_pg = 1;
    	int total_pg = 1;
    	int try_api_cnt=1;
    	Map<String,String> batInfoMap = new HashMap<String,String>();
    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    	batInfoMap.put("id_service", GlobalConst.META_M_TYPE_BID);
		batInfoMap.put("search_date", search_date);
		batInfoMap.put("lines_per_pg", numOfRows);
    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows,batInfoMap);
    	boolean rowSuc=false;
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_api_cnt<3) {
    		    	batInfoMap.put("try_api_cnt", String.valueOf(try_api_cnt));
    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows,batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    		            	batInfoMap.put("id_batch", String.valueOf(batTryRet.get("id_batch")));
    		        		total_pg = (int)batRet.get("tot_pg");
    		        		rowSuc=true;
    		    			curr_pg++;
    		    	        break;
    		    		}
    		    	}
    		    	try_api_cnt++;
    			}
    	    	log.debug("Batch job start search fail");
    		}else {
            	batInfoMap.put("id_batch", String.valueOf(batRet.get("id_batch")));
    			rowSuc=true;
    			curr_pg++;
        		total_pg = (int)batRet.get("tot_pg");
    	    	log.debug("Batch job start search start");
    		}
    	}else {
    		throw new Exception("Batch job Fail");
    	}
    	if(rowSuc) {
        	for(int i=curr_pg;i<=total_pg;i++) {
        		System.out.println("현재 "+i+"페이지 검색 시작");
        		Map<String,Object> insRet=this.insertTodayOrds(search_date,num_of_rows, i,batInfoMap);
        	}
        	int totPgCnt = batchService.getBatchTotPageAtDailyBatchMByIdBatch(batInfoMap);
        	int retCnt = batchService.getSuccCountAtDailyBatchRetByIdBatch(batInfoMap);
        	if(totPgCnt==retCnt) {
        		batInfoMap.put("succ_yn", "Y");
        		batchService.updateSuccYnAtBatchM(batInfoMap);
        	}
    	}else {
    		CommandMap failMap = new CommandMap();
    		failMap.put("search_date",batInfoMap.get("search_date"));
    		failMap.put("id_service", batInfoMap.get("id_service"));
    		failMap.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
			failMap.put("succ_yn", "F");
    		batchService.insertDailyBatchM(failMap);
    	}
    	log.debug("Batch job end");
    }

//  @Scheduled(initialDelay = 3000,fixedRate = 60000000)		
//@Scheduled(cron = "00 12 25 * * *")		
  public void testOpenApiBatch() throws ParseException {
  	log.debug("Batch job start");
  	String search_date = StringDateUtil.getToday("yyyyMMdd");
		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
		int num_of_rows = Integer.parseInt(numOfRows);
		int curr_pg = 1;
  	int total_pg = 1;
  	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
  	if(batRet.get("sucYn")!=null) {
  		boolean sucYn = (boolean)batRet.get("sucYn");
  		if(!sucYn) {
  	    	log.debug("Batch job start search fail");
  		}else {
      		total_pg = (int)batRet.get("tot_pg");
  	    	log.debug("Batch job start search start");
  		}
  	}else {
	    	log.debug("Batch job search fail");    		
  	}
  
  }
    /**첫번째 성공시도 만 저장하는 메서드
    * restTemplate.getForObject 사용
    * @param search_date
    * @return
    * @throws ParseException
    */
   private Map<String,Object> getRestfulInfoByString(String search_date, int curr_pg, int num_of_rows, Map<String, String> batInfoMap) throws ParseException{
   	Map<String,Object> result = new HashMap<String,Object>();
		String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");

		String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
		String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
		String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
		String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
		String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
		String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
		
		
		String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");

	    RestTemplate restTemplate = new RestTemplate();
	    String st_dt = null;
	    if(search_date==null) {
	    	st_dt = StringDateUtil.getToday("yyyyMMdd");
	    }else {
			st_dt = search_date;
	    }
//		params.add(bidNtceBgnDt, st_dt+"0000");
//		params.add(bidNtceEndDt, ed_dt+"2359");
	    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
               .queryParam(numOfRows, num_of_rows)
               .queryParam(pageNo, "1")
               .queryParam(data_type, "json")
               .queryParam(bidNtceBgnDt, st_dt+"0000")
               .queryParam(bidNtceEndDt, st_dt+"2359")
               .queryParam(serviceKey, test_key);

       String url = builder.toUriString();
       System.out.println(url);
       
       String resp = restTemplate.getForObject(url, String.class);
       System.out.println("resp: " + resp);
       JSONParser parser = new JSONParser();
       boolean sucYn=false;
       try {
	       JSONObject respJson = (JSONObject)parser.parse(resp);
	       CommandMap searchBatParam = new CommandMap();
	       searchBatParam.put("search_date", search_date);
	       CustomMap dailyBatM = batchService.getLastDailyBatchMBySearchDate(searchBatParam);
	       if(dailyBatM==null) {
		       	Map<String, String> re =new HashMap<String,String>();
					Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,1,100,batInfoMap);
		           if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
		   	        result.put("sucYn", true);
		   	        batInfoMap.put("id_batch", ins_ret.get("id_batch").toString());
		           }else {
		   	        result.put("sucYn", sucYn);
		           }
	       }else {
	    		//(TODO)이 경우를 찾아서 조치 필요
	        	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,curr_pg,num_of_rows,batInfoMap);
	            if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
	    	        result.put("sucYn", true);
	            }else {
	    	        result.put("sucYn", sucYn);
	            }
		        result.put("id_batch", batInfoMap.get("id_batch"));
		        result.put("tot_pg", ins_ret.get("tot_pg"));
	    		
	       }
       }catch(Exception e) {
	        result.put("sucYn", sucYn);
       }
   	return result;
   }
   /**
    * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
    * @param jsonObject
    * @param num_of_rows 
    * @param curr_pg 
    * @param batInfoMap 
    * @return
    */
   private Map<String,Object> insertOpenApiResult(JSONObject jsonObject, int curr_pg, int num_of_rows, Map<String, String> batInfoMap) {
   	Map<String,Object> result = new HashMap<String,Object>();
		Map resMap = (Map)jsonObject.get("response");
		Map resHeadMap = (Map)resMap.get("header");
		Map resBodyMap = (Map)resMap.get("body");
		try {
			Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
			int totPage = totCnt / num_of_rows;
			int restRow = totCnt % num_of_rows;
			if(restRow!=0){
				totPage++;
			}
//			for(int i=0;i<totPage;i++) {
//				System.out.println("페이지 번호:"+(i+1));
//			}
			System.out.println("총 페이지:"+totPage);
			result.put("tot_pg", totPage);

			JSONArray arr = (JSONArray)resBodyMap.get("items");
			List<String> keywordList = new ArrayList<String>();
			List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
			for(CustomMap k:kList) {
				keywordList.add(k.getString("code_nm", ""));
			}
//			keywordList.add("구입");
//			keywordList.add("계약");
			CommandMap commandMap = new CommandMap();
			commandMap.put("service_cd", GlobalConst.META_M_TYPE_BID);
//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
			//(TODO)테스트용 전체 컬럼 매칭 검색
			List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
			Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
			for(CustomMap m:mList) {
				String id_bid = m.getString("id_bid","");
				if(!"".equals(id_bid)) {
					CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
					String bid_col_cd = mCmap.getString("bid_col_cd","");
					if(!"".equals(bid_col_cd)) {
						bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
						System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
					}
				}else {
					System.out.println("나오면 안되는 라인");
				}
			}
			List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
			//(TODO)패치에서 넣어주어야 하는 코드
			CommandMap searchCon = new CommandMap();
			searchCon.put("search_date", batInfoMap.get("search_date"));
			searchCon.put("rslt_pg_no", 1);
			for(Object ino : arr) {
				if(ino instanceof JSONObject) {
					JSONObject json = (JSONObject)ino;
					if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
						//   (json에 담긴 값을 체크하기 위한 코드 시작
						System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
						
						String bidNtceNm = json.get("bidNtceNm").toString().trim();
						System.out.println("bidNtceNm:"+bidNtceNm);
						//   )
						
						//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
						List<CommandMap> inputMap = new ArrayList<CommandMap>();
						if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
							String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
							System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
							for(Object key:json.keySet()) {
								if(key instanceof String) {
									
									//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
									System.out.println("(=================================================");
									System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
									//(TODO)디버깅 용 코드
									if(key.equals("opengTm")) {
										System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
									}
									CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
									//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
									if(keyMap!=null) {
										System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
										CommandMap val = new CommandMap();
										//참고용 값
										val.put("bid_col_cd", key);
										val.put("id_bid",keyMap.getString("id_bid"));
										val.put("conts", json.get(key));
										inputMap.add(val);
									}else {
										System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
									}
									//   )
									System.out.println("=================================================)");
								}
							}
							if(inputMap.size()>0) {
								inputList.add(inputMap);
							}else {
								result.put("sucYn", "N");
								throw new Exception("있어야 하는 값이 없음");
							}

						}
					}
				}else {
					result.put("sucYn", "N");
					System.out.print("ino Class명:"+ino.getClass());
				}
			}
			if(inputList.size()>0) {
				bidSearchService.insertBidNoti(inputList,searchCon);
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
			}else {
				result.put("sucYn", "Y");
//				result.put("tot_pg",0);
				System.out.print("inputList size = 0");
			}
			if(curr_pg==1) {
				CommandMap batM = new CommandMap();
				batM.put("search_date",batInfoMap.get("search_date"));
				batM.put("id_service", batInfoMap.get("id_service"));
				batM.put("tot_pg", totPage);
				batM.put("tot_row_cnt", totCnt);
				batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
				batM.put("succ_yn", "P");
				batchService.insertDailyBatchM(batM);
				int dailyBatNum = batchService.getDailyBatNumByIdBatch(batM);
				batM.put("daily_bat_num", dailyBatNum);
				batchService.updateDailyBatNumByIdBatch(batM);
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batM.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				batchService.insertDailyBatchRet(batRet);
				result.put("id_batch", batM.get("id_batch"));
			}else {
				CommandMap batRet = new CommandMap();
				batRet.put("id_batch", batInfoMap.get("id_batch"));
				batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
				batRet.put("curr_pg_no", curr_pg);
				batRet.put("succ_yn", "Y");
				batchService.insertDailyBatchRet(batRet);
			}
		}catch(Exception e) {
			System.out.print("오류발생:"+e.getClass().getName());
			e.printStackTrace();
			result.put("sucYn", "N");
		}
		System.out.println("result tot_pg:"+result.get("tot_pg"));
		return result;
   	
   }
	private Map<String,Object>  insertTodayOrds(String search_date,int num_of_rows, int curr_pg,Map<String, String> batInfoMap) throws ParseException {
		Map<String,Object> result = new HashMap<String,Object>();
//    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
		if(batInfoMap.get("try_api_cnt")==null) {
	        result.put("sucYn", false);
	        return result;
		}
    	Map<String,Object> batRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows, batInfoMap);
    	int try_cnt_num = Integer.parseInt(String.valueOf(batInfoMap.get("try_api_cnt")));
    	if(batRet.get("sucYn")!=null) {
    		boolean sucYn = (boolean)batRet.get("sucYn");
    		if(!sucYn) {
    			while(try_cnt_num<3) {
//    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows);
    		    	Map<String,Object> batTryRet = this.getRestfulInfoByString(search_date,curr_pg,num_of_rows,batInfoMap);
    		    	if(batTryRet.get("sucYn")!=null) {
    		    		boolean trySucYn = (boolean)batTryRet.get("sucYn");
    		    		if(trySucYn) {
    	        	        result.put("sucYn", true);
    	        	        result.put("try_api_cnt", batInfoMap);
    	        	        sucYn = true;
    	        	        break;
    		    		}
    		    	}
    		    	try_cnt_num++;
    			}
    		}
    	}
    	else {
	        result.put("sucYn", false);
	        result.put("try_api_cnt", batInfoMap);
			CommandMap retMap = new CommandMap();
			retMap.put("num_of_try", batInfoMap.get("try_"));
			retMap.put("succ_yn", "N");
			retMap.put("curr_pg_no", curr_pg);
			retMap.put("succ_yn","N");
			batchService.insertDailyBatchRet(retMap);
    	}
    	return result;
    }

	/**첫번째 성공시도 만 저장하는 메서드
	    * restTemplate.getForObject 사용
	    * @param search_date
	    * @param batInfoMap tring search_date, int curr_pg, int num_of_rows, 
	    * @return
	    * @throws ParseException
	    */
	    private Map<String,Object> getRestfulInfoByCurl(Map<String, String> batInfoMap) throws ParseException{
	    	Map<String,Object> result = new HashMap<String,Object>();
			String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");
	
			String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
			String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
			String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
			String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
			String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
			String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
			
			
			String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");
	
		    String search_date = batInfoMap.get("search_date");
	//	    String ed_date = batInfoMap.get("ed_date");
		    
		    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
		               .queryParam(numOfRows, batInfoMap.get("lines_per_pg"))
		               .queryParam(pageNo, batInfoMap.get("curr_pg"))
		               .queryParam(data_type, "json")
		               .queryParam(bidNtceBgnDt, search_date+"0000")
		               .queryParam(bidNtceEndDt, search_date+"2359");
	//		               .queryParam(serviceKey, test_key);
		//
	//		       String url = builder.toUriString();
	//		       System.out.println(url);
		    String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
		    System.out.println(url);
	       
	   		ProcessBuilder processBuilder = new ProcessBuilder("curl", url);
	
	   		boolean sucYn=false;
	   		try {
	   			Process process = processBuilder.start();
	
	           // 응답 데이터를 읽어오기 위한 작업
	   			InputStream inputStream = process.getInputStream();
	   			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	   			StringBuilder resp = new StringBuilder();
	           	String line;
	
	           	while ((line = reader.readLine()) != null) {
	           		resp.append(line);
	           	}
	
	           	reader.close();
	
	           	JSONParser parser = new JSONParser();
	           	try {
	           		JSONObject respJson = (JSONObject)parser.parse(resp.toString());
	           		CommandMap searchBatParam = new CommandMap();
	           		searchBatParam.put("search_date", search_date);
	           		CustomMap dailyBatM = batchService.getLastDailyBatchMBySearchDate(searchBatParam);
	           		if(dailyBatM==null) {
	           		
		               	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,batInfoMap);
		                   if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
		           	        result.put("sucYn", true);
		           	        result.put("id_batch", ins_ret.get("id_batch"));
		                   }else {
		           	        result.put("sucYn", sucYn);
		                   }
		       	        result.put("tot_pg", ins_ret.get("tot_pg"));
	           		}else {
		           		//(TODO)search_date에 batch가 실행된 경우가 있을 경우의 조치 사항
	           			String today = StringDateUtil.getDateByFormat(batInfoMap.get("date_format"), Locale.KOREAN);
	       	     		CustomMap daySuccYn = batchService.getSuccYnAtDailyBatchMBySearchDate(batInfoMap);;
	          			if(today.equals(search_date)) {
	           				
	           			}else {
	           				int curr_date_daily_bat_num = daySuccYn.getInt("daily_bat_num",0);
	           				if(curr_date_daily_bat_num==0) {
	        	               	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,batInfoMap);
	        	               	if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
	        	               		result.put("sucYn", true);
	        	               	}else {
	        	               		result.put("sucYn", sucYn);
	        	               	}
		     	       	        result.put("id_batch", batInfoMap.get("id_batch"));
		     	       	        result.put("tot_pg", ins_ret.get("tot_pg"));
	           				}
	           			}
	           		}
	           	}catch(Exception e) {
	           		result.put("sucYn", sucYn);
	           	}
	   		} catch (IOException e) {
		        result.put("sucYn", sucYn);
	//		           e.printStackTrace();
	//		           System.out.println("Error executing Curl command");
	   		}
			return result;
	    }

	/**
	     * 응답 json을 보고 그 형식에 따라 db에 넣는 작업을 수행
	     * @param jsonObject
	     * @param num_of_rows 
	     * @param curr_pg 
	     * @param batInfoMap 
	     * @return
	     */
	    private Map<String,Object> insertOpenApiResult(JSONObject jsonObject, Map<String, String> batInfoMap) {
			Integer curr_pg = Integer.parseInt(batInfoMap.get("curr_pg"));
			String search_date = batInfoMap.get("search_date");
	    	Map<String,Object> result = new HashMap<String,Object>();
			Map resMap = (Map)jsonObject.get("response");
	//		Map resHeadMap = (Map)resMap.get("header");
			Map resBodyMap = (Map)resMap.get("body");
			try {
				Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
				Integer num_of_rows = Integer.parseInt(batInfoMap.get("lines_per_pg"));
				int totPage = totCnt / num_of_rows;
				int restRow = totCnt % num_of_rows;
				if(restRow!=0){
					totPage++;
				}
	//			for(int i=0;i<totPage;i++) {
	//				System.out.println("페이지 번호:"+(i+1));
	//			}
				System.out.println("총 페이지:"+totPage);
				result.put("tot_pg", totPage);
	
				JSONArray arr = (JSONArray)resBodyMap.get("items");
				List<String> keywordList = new ArrayList<String>();
				List<CustomMap> kList = bidSearchService.selectBidSearchKeywordList();
				for(CustomMap k:kList) {
					keywordList.add(k.getString("code_nm", ""));
				}
	//			keywordList.add("구입");
	//			keywordList.add("계약");
				CommandMap commandMap = new CommandMap();
				commandMap.put("service_cd", GlobalConst.META_M_TYPE_BID);
	//			List<CustomMap> mList = service.selectBidNotiMForUsing(commandMap);
				//(TODO)테스트용 전체 컬럼 매칭 검색
				List<CustomMap> mList = bidSearchService.selectBidNotiMForUsing(commandMap);
				Map<String,CommandMap> bidMasterMap = new HashMap<String,CommandMap>();
				for(CustomMap m:mList) {
					String id_bid = m.getString("id_bid","");
					if(!"".equals(id_bid)) {
						CommandMap mCmap = CollectionUtil.getCommandMapByCustomMap(m);
						String bid_col_cd = mCmap.getString("bid_col_cd","");
						if(!"".equals(bid_col_cd)) {
							bidMasterMap.put(mCmap.getString("bid_col_cd",""), mCmap);
							System.out.println("사용하는 컬럼코드:"+mCmap.getString("bid_col_cd","")+",컬럼명:"+mCmap.getString("bid_col_nm",""));
						}
					}else {
						System.out.println("나오면 안되는 라인");
					}
				}
				List<List<CommandMap>> inputList= new ArrayList<List<CommandMap>>();
				//(TODO)패치에서 넣어주어야 하는 코드
				CommandMap searchCon = new CommandMap();
				searchCon.put("search_date", search_date);
				searchCon.put("id_service", batInfoMap.get("id_service"));
				searchCon.put("rslt_pg_no", curr_pg);
				for(Object ino : arr) {
					if(ino instanceof JSONObject) {
						JSONObject json = (JSONObject)ino;
						if(json.get("bsnsDivNm").toString().trim().contains("용역")) {
							//   (json에 담긴 값을 체크하기 위한 코드 시작
							System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
							
							String bidNtceNm = json.get("bidNtceNm").toString().trim();
							System.out.println("bidNtceNm:"+bidNtceNm);
							//   )
							
							//(TODO) 미리 등록한 키워드를 조회해서 , 키워드와 매칭하는 jsonObject 만 db에 저장하는 로직 필요
							List<CommandMap> inputMap = new ArrayList<CommandMap>();
							if(keywordList.size()>0 && StrUtil.containsMidString(keywordList,bidNtceNm)) {
								String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,bidNtceNm);
								System.out.println("매칭된 키워드 :"+matchingKeyword+",입찰공고번호:"+json.get("bidNtceNo")+",입찰공고명:"+json.get("bidNtceNm"));
								for(Object key:json.keySet()) {
									if(key instanceof String) {
										
										//   (등록된 키워드와 매칭하는 cntrctNm 이 있는 경우의 처리 코드 시작
										System.out.println("(=================================================");
										System.out.println("찾아야 하는 key 값 : "+key+" 과 key의 값:"+json.get(key));
										//(TODO)디버깅 용 코드
										if(key.equals("opengTm")) {
											System.out.println("디버깅 코드 : "+key+" 과 key의 값:"+json.get(key));
										}
										CommandMap keyMap=bidMasterMap.get(String.valueOf(key));
										//미리 등록된 컬럼명에 해당되는 것만 db에 넣어야 함.
										if(keyMap!=null) {
											System.out.println("찾은 코드 명 : "+keyMap.getString("bid_col_cd","")+" 과 값:"+json.get(key));
											CommandMap val = new CommandMap();
											//참고용 값
											val.put("bid_col_cd", key);
											val.put("id_bid",keyMap.getString("id_bid"));
											val.put("conts", json.get(key));
											inputMap.add(val);
										}else {
											System.out.println("사용하지 않는 코드 : "+key+" 과 값:"+json.get(key));
										}
										//   )
										System.out.println("=================================================)");
									}
								}
								if(inputMap.size()>0) {
									inputList.add(inputMap);
								}else {
									result.put("sucYn", "N");
									throw new Exception("있어야 하는 값이 없음");
								}
	
							}
						}
					}else {
						result.put("sucYn", "N");
						System.out.print("ino Class명:"+ino.getClass());
					}
				}
				if(inputList.size()>0) {
					bidSearchService.insertBidNoti(inputList,searchCon);
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
				}else {
					result.put("sucYn", "Y");
	//				result.put("tot_pg",0);
					System.out.print("inputList size = 0");
				}
				if(curr_pg==1) {
					CommandMap batM = new CommandMap();
					batM.put("search_date",batInfoMap.get("search_date"));
					batM.put("id_service", batInfoMap.get("id_service"));
					batM.put("tot_pg", totPage);
					batM.put("tot_row_cnt", totCnt);
					batM.put("lines_per_pg", batInfoMap.get("lines_per_pg"));
					batM.put("succ_yn", "P");
					batchService.insertDailyBatchM(batM);
					int dailyBatNum = batchService.getDailyBatNumByIdBatch(batM);
					batM.put("daily_bat_num", dailyBatNum);
					batchService.updateDailyBatNumByIdBatch(batM);
					CommandMap batRet = new CommandMap();
					batRet.put("id_batch", batM.get("id_batch"));
					batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
					batRet.put("curr_pg_no", curr_pg);
					batRet.put("succ_yn", "Y");
					batchService.insertDailyBatchRet(batRet);
					result.put("id_batch", batM.get("id_batch"));
				}else {
					CommandMap batRet = new CommandMap();
					batRet.put("id_batch", batInfoMap.get("id_batch"));
					batRet.put("num_of_try", batInfoMap.get("try_api_cnt"));
					batRet.put("curr_pg_no", curr_pg);
					batRet.put("succ_yn", "Y");
					batchService.insertDailyBatchRet(batRet);
				}
			}catch(Exception e) {
				System.out.print("오류발생:"+e.getClass().getName());
				e.printStackTrace();
				result.put("sucYn", "N");
			}
			System.out.println("result tot_pg:"+result.get("tot_pg"));
			return result;
	    }

	/**첫번째 성공시도 만 저장하는 메서드
	     * restTemplate.getForObject 사용
	     * @param search_date
	     * @return
	     * @throws ParseException
	     */
	    private Map<String,Object> getRestfulInfoByString(String search_date, int curr_pg, int num_of_rows) throws ParseException{
	    	Map<String,Object> result = new HashMap<String,Object>();
			String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");
	
			String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
			String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
			String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
			String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
			String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
			String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
			
			
			String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");
	
		    RestTemplate restTemplate = new RestTemplate();
		    String st_dt = null;
		    if(search_date==null) {
		    	st_dt = StringDateUtil.getToday("yyyyMMdd");
		    }else {
				st_dt = search_date;
		    }
	//		params.add(bidNtceBgnDt, st_dt+"0000");
	//		params.add(bidNtceEndDt, ed_dt+"2359");
		    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	                .queryParam(numOfRows, num_of_rows)
	                .queryParam(pageNo, "1")
	                .queryParam(data_type, "json")
	                .queryParam(bidNtceBgnDt, st_dt+"0000")
	                .queryParam(bidNtceEndDt, st_dt+"2359")
	                .queryParam(serviceKey, test_key);
	
	        String url = builder.toUriString();
	        System.out.println(url);
	        
	        String resp = restTemplate.getForObject(url, String.class);
	        System.out.println("resp: " + resp);
	        JSONParser parser = new JSONParser();
	        boolean sucYn=false;
	        try {
	        	JSONObject respJson = (JSONObject)parser.parse(resp);
	        	Map<String, String> re =new HashMap<String,String>();
				Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,1,100,re );
	            if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
	    	        result.put("sucYn", true);
	            }else {
	    	        result.put("sucYn", sucYn);
	            }
	        }catch(Exception e) {
		        result.put("sucYn", sucYn);
	        }
	    	return result;
	    }

	public void useWebClient(String search_date) throws UnsupportedEncodingException {
	//    	WebClient client = WebClient.create();
	//    	String url = "https://apis.data.go.kr/1230000/PubDataOpnStdService/getDataSetOpnStdCntrctInfo?numOfRows%3D300%26pageNo%3D1%26type%3Djson%26bidNtceBgnDt%3D202308250000%26%3DbidNtceEndDt%3D202308252359%26ServiceKey%3DCfhUvbXmcnPYUz5r3vyAQjks6FG28KO0MtS56r9tPSOGPAP%252B4d4Vo9HlgfNx6w5Uck%252Fk9gJKM4NaSqSUzTplTA%253D%253D";
	//    	String responseBody = client.get()
	//                .uri(url)
	//                .retrieve()
	//                .bodyToMono(String.class)
	//                .block();
	// 
	//        System.out.println("Response Body: " + responseBody);
	        
	        // WebClient 생성
	        WebClient webClient = WebClient.create("https://apis.data.go.kr/1230000");
	
	        String encodedString = "CfhUvbXmcnPYUz5r3vyAQjks6FG28KO0MtS56r9tPSOGPAP%2B4d4Vo9HlgfNx6w5Uck%2Fk9gJKM4NaSqSUzTplTA%3D%3D";
	
	            String decodedString = URLDecoder.decode(encodedString, "UTF-8");
	            String reEncodedString = URLEncoder.encode(decodedString, "UTF-8");
	            System.out.println("Decoded String: " + reEncodedString);
	            String paramUrl ="/PubDataOpnStdService/getDataSetOpnStdCntrctInfo?numOfRows=10&pageNo=1&type=json&cntrctCnclsBgnDate=20230701&cntrctCnclsEndDate=20230731&ServiceKey="+decodedString;
	        // 비동기식으로 요청 보내고 응답을 Mono로 받아오기
	        Mono<String> responseBodyMono = webClient.get()
	                .uri(paramUrl)
	                .retrieve()
	                .bodyToMono(String.class);
	
	        // 비동기적으로 응답 처리
	        responseBodyMono.subscribe(
	            responseBody -> System.out.println("Response: " + responseBody),
	            error -> System.err.println("Request failed: " + error),
	            () -> System.out.println("Request completed")
	        );
	
	    }

	/**첫번째 성공시도 만 저장하는 메서드
	     * 
	     * @param search_date
	     * @return
	     * @throws ParseException
	     */
	    private Map<String,Object> getRestfulInfoByString(String search_date,int num_of_rows) throws ParseException{
	    	Map<String,Object> result = new HashMap<String,Object>();
			String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");
	
			String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
			String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
			String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
			String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
			String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
			String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
			
			
			String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");
	
		    RestTemplate restTemplate = new RestTemplate();
		    String st_dt = null;
		    if(search_date==null) {
		    	st_dt = StringDateUtil.getToday("yyyyMMdd");
		    }else {
				st_dt = search_date;
		    }
	//		params.add(bidNtceBgnDt, st_dt+"0000");
	//		params.add(bidNtceEndDt, ed_dt+"2359");
		    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	                .queryParam(numOfRows, num_of_rows)
	                .queryParam(pageNo, "1")
	                .queryParam(data_type, "json")
	                .queryParam(bidNtceBgnDt, st_dt+"0000")
	                .queryParam(bidNtceEndDt, st_dt+"2359");
	//                .queryParam(serviceKey, test_key);
	
	        String url = builder.toUriString()+"&"+serviceKey+"="+test_key;
	        System.out.println(url);
	        
	        String resp = restTemplate.getForObject(url, String.class);
	        System.out.println("resp: " + resp);
	        JSONParser parser = new JSONParser();
	        boolean sucYn=false;
	        try {
	        	JSONObject respJson = (JSONObject)parser.parse(resp);
	        	Map<String, String> re = new HashMap<String,String>();
				Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,1,100,re);
	            if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
	    	        result.put("sucYn", true);
	            }else {
	    	        result.put("sucYn", sucYn);
	            }
	        }catch(Exception e) {
		        result.put("sucYn", sucYn);
	        }
	    	return result;
	    }

	/**
	     * restTemplate.getForEntity 테스트 함수
	     * @param search_date
	     * @return
	     * @throws ParseException
	     */
	    private Map<String,Object> getRestfulInfo(String search_date) throws ParseException{
	    	Map<String,Object> result = new HashMap<String,Object>();
			String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");
	
			String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
			String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
			String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
			String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
			String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
			String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
			String real_row_num = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
			
			
			String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");
	
		    RestTemplate restTemplate = new RestTemplate();
		    String st_dt = null;
		    if(search_date==null) {
		    	st_dt = StringDateUtil.getToday("yyyyMMdd");
		    }else {
				st_dt = search_date;
		    }
	//		params.add(bidNtceBgnDt, st_dt+"0000");
	//		params.add(bidNtceEndDt, ed_dt+"2359");
		    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	                .queryParam(numOfRows, real_row_num)
	                .queryParam(pageNo, "1")
	                .queryParam(data_type, "json")
	                .queryParam(bidNtceBgnDt, st_dt+"0000")
	                .queryParam(bidNtceEndDt, st_dt+"2359")
	                .queryParam(serviceKey, test_key);
	
	        String url = builder.toUriString();
	        System.out.println(url);
	        
	//        String resp = restTemplate.getForObject(url, String.class);
	//        System.out.println("resp: " + resp);
		    ResponseEntity<String> responseEntity = restTemplate.getForEntity(openApiUrl, String.class);
		    HttpHeaders responseHeaders = responseEntity.getHeaders();
		    System.out.println("Headers: " + responseHeaders);
	//	    responseHeaders.getContentType().equals(com.google.common.net.HttpHeaders.CONTENT_TYPE)
		    HttpStatus res = responseEntity.getStatusCode();  
		    boolean sucYn = false;
		    if(responseHeaders.getContentType().getSubtype().toUpperCase().equals("XML")) {
		        result.put("sucYn", sucYn);
		        System.out.println("api fail : Server error");
		    }else if(responseHeaders.getContentType().getSubtype().toUpperCase().equals("JSON")) {
		        System.out.println("response : "+responseEntity.getBody());
		        String response = responseEntity.getBody();
		        JSONParser parser = new JSONParser();
		        JSONObject respJson = (JSONObject)parser.parse(response);
		        Map<String,String> api_ret = new HashMap<String,String>();
	        	Map<String,Object> ins_ret = this.insertOpenApiResult(respJson,1,100,api_ret);
	            if(ins_ret.get("sucYn")!=null && "Y".equals(String.valueOf(ins_ret.get("sucYn")))) {
	    	        result.put("sucYn", true);
	            }else {
	    	        result.put("sucYn", sucYn);
	            }
		    }else {
		        result.put("sucYn", sucYn);
		        System.out.println("api fail : 알수 없는 오류");
		    }
	    	return result;
	
	    }

	public void curlHttp(String search_date) {
	//    	try {
	//            Process process = Runtime.getRuntime().exec("https://apis.data.go.kr/1230000/PubDataOpnStdService/getDataSetOpnStdBidPblancInfo?numOfRows=500&pageNo=1&type=json&bidNtceBgnDt=202308220000&bidNtceEndDt=202308242359&ServiceKey=CfhUvbXmcnPYUz5r3vyAQjks6FG28KO0MtS56r9tPSOGPAP%2B4d4Vo9HlgfNx6w5Uck%2Fk9gJKM4NaSqSUzTplTA%3D%3D");
	//            
	//            InputStream inputStream = process.getInputStream();
	//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	//            
	//            String line;
	//            while ((line = reader.readLine()) != null) {
	//                System.out.println(line);
	//            }
	//            
	//            reader.close();
	//        } catch (IOException e) {
	//            e.printStackTrace();
	//        }
	    	ProcessBuilder processBuilder = new ProcessBuilder("curl", "https://apis.data.go.kr/1230000/PubDataOpnStdService/getDataSetOpnStdBidPblancInfo?numOfRows=500&pageNo=1&type=json&bidNtceBgnDt=202308220000&bidNtceEndDt=202308242359&ServiceKey=CfhUvbXmcnPYUz5r3vyAQjks6FG28KO0MtS56r9tPSOGPAP%2B4d4Vo9HlgfNx6w5Uck%2Fk9gJKM4NaSqSUzTplTA%3D%3D");
	
	        try {
	            Process process = processBuilder.start();
	
	            // 응답 데이터를 읽어오기 위한 작업
	            InputStream inputStream = process.getInputStream();
	            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	            StringBuilder response = new StringBuilder();
	            String line;
	
	            while ((line = reader.readLine()) != null) {
	                response.append(line);
	            }
	
	            reader.close();
	
	            System.out.println(response.toString());
	        } catch (IOException e) {
	            e.printStackTrace();
	            System.out.println("Error executing Curl command");
	        }
	
	    }

	/**
	     * restTemplate.getForEntity 테스트 함수
	     * @param search_date
	     * @return
	     * @throws ParseException
	     * @throws UnsupportedEncodingException 
	     */
	    private Map<String,Object> getTestRestfulInfo(String search_date) throws ParseException, UnsupportedEncodingException{
	    	Map<String,Object> result = new HashMap<String,Object>();
			String openApiUrl = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.url");
	
			String numOfRows = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.num.of.rows");
			String pageNo = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.page.no");
			String serviceKey = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.key");
			String data_type = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.type");
			String bidNtceBgnDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.bgn_dt");
			String bidNtceEndDt = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.end_dt");
			String real_row_num = SpringPropertiesUtil.getProperty("openapi.srv.pblanc.val.num.of.rows");
			
			
			String test_key = SpringPropertiesUtil.getProperty("openapi.srv.real.key");
	
		    int timeoutMillis = 5000; // 타임아웃 시간을 밀리초 단위로 설정 (예: 5초)
	        ClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
	        ((SimpleClientHttpRequestFactory) factory).setConnectTimeout(timeoutMillis);
	        ((SimpleClientHttpRequestFactory) factory).setReadTimeout(timeoutMillis);
		    RestTemplate restTemplate = new RestTemplate(factory);
		    String st_dt = null;
		    if(search_date==null) {
		    	st_dt = StringDateUtil.getToday("yyyyMMdd");
		    }else {
				st_dt = search_date;
		    }
	//		params.add(bidNtceBgnDt, st_dt+"0000");
	//		params.add(bidNtceEndDt, ed_dt+"2359");
		    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(openApiUrl)
	                .queryParam(numOfRows, real_row_num)
	                .queryParam(pageNo, "1")
	                .queryParam(data_type, "json")
	                .queryParam(bidNtceBgnDt, st_dt+"0000")
	                .queryParam(bidNtceEndDt, st_dt+"2359")
	                .queryParam(serviceKey, test_key);
	
	        String url = builder.toUriString();
	        System.out.println(url);
	        String paramValue=numOfRows+"="+real_row_num+"&"+pageNo+"="+1+"&"+data_type+"=json&"
	        		+bidNtceBgnDt+"="+st_dt+"0000&"+bidNtceEndDt+"="+st_dt+"2359&"+serviceKey+"="+test_key;
	        System.out.println("paramValue="+paramValue);
	    	String encodedParamValue = URLEncoder.encode(paramValue, "UTF-8");
	
	        // 인코딩된 값을 URL에 추가하여 완전한 URL 생성
	        String completeParamUrl = openApiUrl + "?" + paramValue;
	        System.out.println("[[[[completeParamUrl="+completeParamUrl);
	        
	        String paramResp1 = restTemplate.getForObject(completeParamUrl, String.class);       
	        System.out.println("----completeParamUrl resp: " + paramResp1);
	        
	        String completeUrl = openApiUrl + "?" + encodedParamValue;
	        System.out.println("{{{{completeUrl="+completeUrl);
	
	        // RestTemplate을 사용하여 HTTP 요청 수행
	        String resp = restTemplate.getForObject(completeUrl, String.class);
	        System.out.println("====completeUrl resp: " + resp);
	        
	    	return result;
	
	    }

	public void testTem() {
	    RestTemplate restTemplate = new RestTemplate();
	
	    String url = "https://apis.data.go.kr/1230000/PubDataOpnStdService/getDataSetOpnStdBidPblancInfo?numOfRows=300&pageNo=1&type=json&bidNtceBgnDt=202308250000&bidNtceEndDt=202308252359&ServiceKey=CfhUvbXmcnPYUz5r3vyAQjks6FG28KO0MtS56r9tPSOGPAP%2B4d4Vo9HlgfNx6w5Uck%2Fk9gJKM4NaSqSUzTplTA%3D%3D"; // 브라우저에서 보내는 URL
	
	    // RestTemplate을 사용하여 데이터를 가져옴
	    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
	
	    // 응답 본문 가져오기
	    String responseBody = response.getBody();
	    System.out.println("Response Body: " + responseBody);
	}


}
