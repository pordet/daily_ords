<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="data" tagdir="/WEB-INF/tags/data" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<data:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
	<c:choose>
		<c:when test="${!is_guest}">
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_draw.js'/>"></script>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_register.js'/>"></script>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/data_input.js'/>"></script>
		</c:when>
		<c:otherwise>
			<script charset="UTF-8" src="<c:url value='/resource/js/data/guest_data_input.js'/>"></script>
		</c:otherwise>
	</c:choose>
</data:DataHead>
<data:DataAccordionBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
	<c:set var="comment" value="${map.comment }" />
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:if test='${param.id_master eq 1}'>
					<span>공지사항</span>
				</c:if>
				<c:if test='${param.id_master eq 2}'>
					<span>글로벌 교통 연구보고서</span>
				</c:if>
			</div>
			<form id="frm" name="frm" method="get" onsubmit="return false;" enctype="multipart/form-data">
				<input type="hidden" name="attach_yn" value="${master.attach_yn}">
				<input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
				
				<input type="hidden" name="id_master" value="${param.id_master}">
				<input type="hidden" name="id_board_master" id="id_board_master" value="${param.id_board_master}">
				<input type="hidden" name="id_parent_board" id="id_parent_board">
				<input type="hidden" name="id_board" id="id_board" value="${param.id_board}">
				<input type="hidden" name="is_insert" id="is_insert" value="${param.is_insert}">
				<input type="hidden" name="id_kind" id="id_kind" value="${param.id_kind}">
				<input type="hidden" name="is_more" id="is_more" value="${param.is_more}">
				<input type="hidden" name="more_page_rows" id="more_page_rows" value="${param.more_page_rows}">
				<input type="hidden" name="id_board_file" id="id_board_file">
				<input type="hidden" name="page" id="page" value="${param.page}">
				<input type="hidden" name="rows" id="rows" value="${param.rows}">
				<input type="hidden" name="ord" id="ord" value="${param.ord}">
				<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
				<div class="board-content">
					<div class="board-content-header-group">
						<p class="board-content-title">${map.title }</p>
						<div class="board-content-sub-info">
							<span>작성일 : ${map.reg_date}</span>
							<span>작성자 : ${map.reg_user_nm }</span>
						</div>
					</div>
					<div class="board-content-list-group">
						<table>
							<colgroup>
								<col width="200px">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>게시판</td>
									<td>
										<c:forEach var="code" items="${rootList}">
											<c:if test="${map.id_parent_board eq code.id_board_master}">
												<span>${code.board_nm}</span>
											</c:if>
										</c:forEach>
										<span> > </span>
										<c:forEach var="child" items="${childList}">
											<c:if test="${map.id_board_master eq child.id_board_master}">
												<span>${child.board_nm}</span>
											</c:if>
										</c:forEach>
										
									</td>
								</tr>
								<tr>
									<td>키워드</td>
									<td>${map.keyword}</td>
								</tr>
								<tr>
									<td>첨부파일</td>
									<td>
										<div class="board-content-list-fileset">
											<div>
												<c:if test="${fn:length(addFileList)>0}">
													<c:forEach var="file" items="${addFileList}">
														<div class="file-set">
															<a class="file-icon" href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}">
																<span class="${file.file_sh_ext}">${file.file_ext}</span>
															</a>
															<a class="file-name" href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}">
																<span>${file.file_nm}</span>
															</a>
														</div>
													</c:forEach>
												</c:if>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-content-comment-area">
						<pre id="boardComment">${map.comment}</pre>
					</div>
				</div>
				<!-- button -->
				<div class="board-bottom-group">
					<div class="board-bottom-group-front">
						<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>
					</div>
					<div class="board-bottom-group-rear">
						<c:if test="${!is_guest}">
							<button type="button" class="btn-primary" onclick="javascript:viewDataBoard(${param.id_board_master},${param.id_board},'${param.ord_str}');">수정</button>						
						</c:if>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</data:DataAccordionBody>
<script>
var videoElementList = document.querySelectorAll("[name=player]");
var idAry = [], urlAry = [], objAry = [];
for(var i=0; i<videoElementList.length; i++) {
	var item = videoElementList[i];
	item.setAttribute("id", "videoPlayer_" + i);
	idAry.push("videoPlayer_" + i);
	var urlStrArr = item.innerHTML.split('/');
	var url = urlStrArr[urlStrArr.length -1];
	urlAry.push(url);
}

//youtube API 불러오는 부분
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
//플레이어 변수 설정
var done = false;
function onYouTubeIframeAPIReady() {
	for(var i=0; i<videoElementList.length; i++) {
		var item = videoElementList[i];
		var width = item.getAttribute("width");
		var height = item.getAttribute("height");
		var player;
		var playerId = idAry[i];
		player = new YT.Player(playerId, {
			videoId : urlAry[i],
			width : width,
			height : height,
			events: {
		      //'onReady': onPlayerReady,//로딩중에 이벤트 실행한다
		      'onStateChange': onPlayerStateChange//플레이어 상태 변화 시 이벤트를 실행한다.
		    }
		})
		//objAry.push(player);
	}
}
function onPlayerReady(event) {
	event.target.playVideo();
}
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING && !done) {
		done = true;
	}
}
</script>
</html>