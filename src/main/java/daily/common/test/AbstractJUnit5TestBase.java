package daily.common.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import daily.common.vo.CommandMap;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith({SpringExtension.class, TestNameExtension.class})
public class AbstractJUnit5TestBase {

    protected ApplicationContext applicationContext;

    @BeforeEach
    public void setUp(TestInfo testInfo) {
        applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        System.out.println("Test started: " + testInfo.getDisplayName());
    }

    @AfterEach
    public void tearDown(TestInfo testInfo) {
        System.out.println("Test finished: " + testInfo.getDisplayName());
        ((AnnotationConfigApplicationContext) applicationContext).close();
    }

    @Configuration
    static class AppConfig {

        @Bean
        public MessageSource messageSource() {
            ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
            messageSource.setBasename("classpath:messages");
            messageSource.setDefaultEncoding("UTF-8");
            return messageSource;
        }

        // 다른 빈 설정 및 구성도 가능
    }

    @Test
    public void testContext() {
        assertNotNull(applicationContext.getBean("testService"));
    }

    @Test
    public void testDAO() {
        CommandMap param = new CommandMap();
        assertNotNull(param);
    }

    @Test
    public void SpringPropertiesUtil() throws Exception {
        MessageSource messageSource = applicationContext.getBean(MessageSource.class);
        String aa = messageSource.getMessage("adm-02-01.sector1.title", null, new Locale("rw"));
        System.out.println("attr1:" + aa);
    }
}
