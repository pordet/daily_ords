package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface MenuManageDAO {

	PageList<CustomMap> selectMenuList(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectMenu(Map<String, Object> map);

	CustomMap getMenu(Map<String, Object> map);

	List<CustomMap> selectMenuByParentId(Map<String, Object> map);

	List<CustomMap> selectMenuCodeByParentId(Map<String, Object> map);

	CustomMap getMenuInfoByIdMenu(Map<String, Object> map);

	CustomMap getMenuByResourcePattern(Map<String, Object> map);
	
	int getMaxOrder(Map<String, Object> map);

	List<CustomMap> selectUnregisterMenuList(Map<String, Object> map);
	
	List<CustomMap> selectProgramList(Map<String, Object> map);

	void insertMenu(Map<String, Object> map);

	void modifyMenu(Map<String, Object> map);

	void deleteMenu(Map<String, Object> map);

	void deleteSubMenuByMenu(Map<String, Object> map);

	List<CustomMap> selectStartPageList(Map<String, Object> map);

}
