import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daily.common.vo.CommandMap;

public class Testmain {
    public static void main(String[] args) {
        // 예시 데이터 생성
        List<Map<CommandMap, List<CommandMap>>> realDataList = new ArrayList<>();
        List<CommandMap> colList = new ArrayList<>();
        for(int i=0;i<3;i++) {
            Map<CommandMap, List<CommandMap>> map1 = new HashMap<>();
            CommandMap m1 = new CommandMap();
            m1.put("val", i*10+1);
            CommandMap m2 = new CommandMap();
            m2.put("val", i*10+2);
            CommandMap m3 = new CommandMap();
            m3.put("val", i*10+3);

            List<CommandMap> list = new ArrayList<>();
            list.add(m2);
            list.add(m3);
            map1.put(m1,list);

            
            realDataList.add(map1);
        }
        for(int i=0;i<4;i++) {
            CommandMap m1 = new CommandMap();
            m1.put("val", String.valueOf(i));
            m1.put("id_meta", i);
            colList.add(m1);
        }
        // 데이터 추가
        List<CommandMap> commandMapList = new ArrayList<>();
        for (Map<CommandMap, List<CommandMap>> map : realDataList) {
            for (List<CommandMap> subList : map.values()) {
                commandMapList.addAll(subList);
            }
        }
        Map<Integer,String> calMap = new HashMap<>();
        // 추출된 데이터 확인
        for (CommandMap commandMap : commandMapList) {
            System.out.println(commandMap.get("val"));
        }
        for(CommandMap cal:colList) {
        	calMap.put(cal.getInt("id_meta"), cal.getString("val"));
        }
        System.out.println("end");
    }
}

