package kr.co.daily.front.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nfunk.jep.JEP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.exception.BizException;
import daily.common.util.CollectionUtil;
import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.MetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BidSearchService;
import kr.co.daily.front.service.CalcFormulaService;
import kr.co.daily.front.service.MetaExcelService;
import kr.co.daily.front.service.MetaService;

@Controller
public class MetaController extends AbstractControllerBase {
	@Autowired private MetaService service;
	@Autowired private BidSearchService bidService;
	@Autowired private CalcFormulaService calcService;
	@Autowired private MetaExcelService metaExcelservice;
	@Autowired private MetaManageService metaManageService;
	
	@RequestMapping("/front/meta/metaMList.do")
	public String metaMList(CommandMap commandMap, ModelMap model) throws Exception {
		PageList<CustomMap> list = service.selectMetaMList(commandMap);
		putModelListPage(list, model);
		return "front/meta/metaMList";
	}
	
	@RequestMapping("/front/meta/metaList.do")
	public String metaList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		model = this.selectMataList(commandMap, model);
		return "front/meta/metaList";
	}
	public ModelMap selectMataList(CommandMap commandMap, ModelMap model) {
		CustomMap metaM = null;
		try {
			metaM = service.getMetaM(commandMap);
			model.addAttribute("metaM", metaM);
			if(!"".equals(commandMap.getString("id_meta_m", ""))) {
				CommandMap dtParam = new CommandMap();
				dtParam.put("id_meta_m", commandMap.getString("id_meta_m", ""));
				List<String> dtTypList = new ArrayList<>();
				dtTypList.add(GlobalConst.DT_DATE);
				dtTypList.add(GlobalConst.DT_DATETIME);
				dtParam.put("typ", dtTypList);
				List<CustomMap> dtList = service.getDateTimeColByIdMetaM(dtParam);
				model.addAttribute("dtSearchList", dtList);
			}
	//		CommandMap metaKey = CollectionUtil.getCommandMapByCustomMap(metaM);
			int max_meta_depth = service.getMaxDepthAtMeta(commandMap);
			Map<String, Object> infoMap = service.selectMetaListForUsing(commandMap,max_meta_depth);
			List<List<CustomMap>> headerList = (List<List<CustomMap>>) infoMap.get("headerList");
//			Map<String,Object> subInfoMap = service.selectSubMetaListForUsing(commandMap,(List<CustomMap>) infoMap.get("aloneList"));
			model.addAttribute("max_meta_depth", max_meta_depth);
			model.addAttribute("headerList", headerList);
			CommandMap param = new CommandMap();
			param.put("id_meta_m", commandMap.get("id_meta_m"));
			List<CustomMap> aloneList = (List<CustomMap>) infoMap.get("aloneList");
			model.addAttribute("aloneList", aloneList);
    		CommandMap metaKeyParam = new CommandMap();
    		metaKeyParam.put("id_meta_m",param.getInt("id_meta_m"));
//    		metaKeyParam.put("key_using_type", GlobalConst.KEY_USE_TYPE_EXCEL_UPLOAD);
    		//rel > key_m > key 컬럼순으로 사용
			List<CustomMap> childList = metaManageService.selectMetaRelByParentIdMetaM(metaKeyParam);
			
			PageList<CustomMap> todayList = null;
			try {
				if(childList.size()==0) {
					todayList = service.selectMetaDataContents(commandMap,null);
				}else {
					List<Integer> row_list = new ArrayList<>();
					List<Integer> row_list1 = new ArrayList<>();
					if(!"".equals(commandMap.getString("search_keyword",""))) {
						CommandMap keyMap = new CommandMap();
						keyMap.put("id_meta_m", commandMap.get("id_meta_m"));
						keyMap.put("search_col", commandMap.get("search_col"));
						keyMap.put("search_keyword", commandMap.get("search_keyword"));
						List<CustomMap> searchList = service.selectMetaDataContents(keyMap,null);
						for(CustomMap ci:searchList) {
							row_list.add(ci.getInt("id_meta_row"));
						}
					}
					if(!"".equals(commandMap.getString("search_dt_srt",""))) {
						CommandMap keyMap = new CommandMap();
						keyMap.put("id_meta_m", commandMap.get("id_meta_m"));
						keyMap.put("search_dt_col", commandMap.get("search_dt_col"));
						keyMap.put("search_dt_srt", commandMap.get("search_dt_srt"));
						keyMap.put("search_dt_end", commandMap.get("search_dt_end"));
						List<CustomMap> searchList = service.selectMetaDataContents(keyMap,null);
						for(CustomMap ci:searchList) {
							row_list1.add(ci.getInt("id_meta_row"));
						}
					}
					if(row_list.size()>0 && row_list1.size()>0) {
						ArrayList<Integer> intersection = new ArrayList<>(row_list);  // 첫 번째 ArrayList 복사
				        intersection.retainAll(row_list1);
						commandMap.put("row_list", intersection);
					}else if(row_list.size()>0 && row_list1.size()==0) {
						commandMap.put("row_list", row_list);
					}else if(row_list.size()==0 && row_list1.size()>0) {
						commandMap.put("row_list", row_list1);
					}
					todayList = service.selectTreeMetaDataContents(commandMap,null);
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
			//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
			List<Integer> rawKey = new ArrayList<Integer>();
			for(CustomMap cm : todayList) {
				int key = cm.getInt("id_meta_row",0);
				if(key!=0) {
					rawKey.add(key);
					valMap.put(key, cm);
				}
			}
			if(valMap.size()>0) {
				List<CustomMap> rawList = service.selectMetaDataForSearch(rawKey);
				for(CustomMap cm : todayList) {
					if(childList.size()==0) {
						cm.put("id_meta_m", commandMap.get("id_meta_m"));
					}
					int key = cm.getInt("id_meta_row",0);
					for(CustomMap rm:rawList) {
						if(rm.getInt("id_meta_row",0)==key) {
							if(rm.getInt("id_data",0)==3735) {
								System.out.println("debug rawList id_data:"+rm.getInt("id_data",0));
							}
							System.out.println("rawList id_data:"+rm.getInt("id_data",0));
							if(!"".equals(rm.getString("val",""))) {
								rm = MetaUtil.transNumberingByCustomMap(rm);
							}
							cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
						}
					}
				}
			}
			putModelListPage(todayList, model);
			model.addAttribute("headerList", headerList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	/**
	 * 입찰 정보 조회에서 정보를 받아서 처리하는 부분과 직접 입력하는 부분을 처리하는 함수
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/front/meta/regMetaData.do")
	public String regMetaData(CommandMap commandMap, ModelMap model) throws Exception {
		Map<String,CustomMap> inputMap = new HashMap<String,CustomMap>();
		CommandMap pMap = new CommandMap();
		pMap.put("trg_id_meta_m", commandMap.getInt("trg_id_meta_m"));
		CustomMap map = service.getMetaM(pMap);
		model.addAttribute("map", map);
		Map<String,String> keyMap = CollectionUtil.getKeyListByCommandMap(commandMap, "key_");
    	Map<String,String> keyValMap = new HashMap<String,String>();
		for(String col_nm : keyMap.keySet()) {
			String val = keyMap.get(col_nm);
			keyValMap.put(col_nm, val);
		}
		if(!"".equals(commandMap.getString("sys_code","")) && "".equals(commandMap.getString("id_meta_row",""))) {
			if(keyValMap.size()==0) {
				throw new Exception("입력키가 없습니다.관리자에게 문의하십시오.");
			}
			commandMap.put("keyValMap", keyValMap);
			commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_BID);
			List<CustomMap> notiColList = bidService.selectBidNotiList(commandMap);
			for(CustomMap col:notiColList) {
				inputMap.put(col.getString("bid_col_cd"), col);
			}
		}else {
			commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_META);
//			commandMap.put("trg_id_meta_m", commandMap.getString("reg_id_meta_m"));
			CustomMap meta_row = service.getMetaRowByKey(commandMap);
			if(meta_row!=null) {
				CommandMap rowParam = CollectionUtil.getCommandMapByCustomMap(meta_row);
				List<CustomMap> valList = service.selectMetaDataByIdMetaRow(rowParam);
				System.out.println("+++++++++++");
				for(CustomMap col:valList) {
					inputMap.put(col.getString("meta_cd"), col);
					System.out.println("meta_cd:"+col.getString("meta_cd"));
				}
				System.out.println("+++++++++++");
			}
			model.addAttribute("meta_row", meta_row);
		}
		if("".equals(commandMap.getString("trg_id_meta_m",""))) {
			throw new BizException("입력 대상 코드가 없습니다.");
		}
		List<CustomMap> colList = service.selectMeta(pMap);
		for(CustomMap col:colList) {
//			CommandMap colMap = CollectionUtil.getCommandMapByCustomMap(col);
			String meta_cd = col.getString("meta_cd", "");
			
			if(!"".equals(meta_cd)) {
				CustomMap input = inputMap.get(meta_cd);
				if(meta_cd.equals("pmNm")) {
					System.out.println("체크 ="+meta_cd+";");
				}
				if(input!=null) {
					if(!"".equals(commandMap.getString("sys_code","")) && "".equals(commandMap.getString("id_meta_row",""))) {
						if(!"".equals(col.getString("typ","")) && (GlobalConst.DT_DATE.equals(col.getString("typ"))|| GlobalConst.DT_DATETIME.equals(col.getString("typ")))){
							col.put("val", input.getString("conts").replaceAll("-", "."));
						}else {
							col.put("val", input.getString("conts"));
						}
					}else {
						col.put("val", input.getString("val"));
						col.put("id_data", input.getString("id_data",""));
						if(col.getString("id_meta","").equals("90")) {
							System.out.println("id_meta :90");
						}
						try {
							col = MetaUtil.transNumberingByCustomMap(col);
						}catch(Exception e) {
							System.out.println("tran Error");
						}
					}
				}
			}
		}
		List<CustomMap> headerList = new ArrayList<>();
		//Map<id_parent_meta,List<child>
		Map<Integer,List<CustomMap>> childMap = new HashMap<>();
		Map<Integer,CustomMap> curMap = new HashMap<>();
		for(CustomMap col:colList) {
			if(col.getInt("meta_depth")==1) {
				headerList.add(col);
				curMap.put(col.getInt("id_meta"), col);
			}else {
				int id_parent_meta = col.getInt("id_parent_meta");
				List<CustomMap> childList = childMap.get(col.getInt("id_parent_meta"));
				if(childList==null) {
					childList = new ArrayList<>();
					childList.add(col);
					childMap.put(col.getInt("id_parent_meta"), childList);
				}else {
					childList.add(col);
				}
				curMap.get(id_parent_meta).put("child_list", childList);
			}
		}
		model.addAttribute("colList", headerList);
		return "front/meta/reg/regMetaData";
	}
	/**
	 * 입찰 정보 조회에서 정보를 받아서 처리하는 부분과 직접 입력하는 부분을 처리하는 함수
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/front/meta/deleteMetaData.do")
	public String deleteMetaData(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = service.getMetaM(commandMap);
		model.addAttribute("map", map);
		if("".equals(commandMap.getString("trg_id_meta_m",""))) {
			throw new RuntimeException("입력 대상 코드가 없습니다.");
		}
		if(!"".equals(commandMap.getString("id_bid_noti","")) && "".equals(commandMap.getString("id_meta_row",""))) {
			commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_BID);
			List<CustomMap> headerList = bidService.deleteBidNoti(commandMap);
			model.addAttribute("colList", headerList);
		}else {
			commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_META);
			List<CustomMap> headerList = service.deleteMetaData(commandMap);
			model.addAttribute("colList", headerList);
		}
		return "front/meta/reg/regMetaData";
	}
	@RequestMapping("/front/meta/insertMetaDataAjax.do")
	public String insertMetaData(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			model = service.insertMetaData(commandMap, model);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		model.addAttribute("id_meta_row", commandMap.getString("id_meta_row",""));
		return "jsonView";
	}

	@RequestMapping("/front/meta/modifyMetaDataAjax.do")
	public String modifyMetaData(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        service.modifyMetaRow(commandMap);
    		List<CustomMap> colList = service.selectMeta(commandMap);
    		Map<String,Object> insDataList = service.modifyMetaData(colList,commandMap);
			model.addAttribute("insList",(List<CustomMap>)insDataList.get("insList"));
			model.addAttribute("modList",(List<String>)insDataList.get("modList"));
			model.addAttribute("delList",(List<CustomMap>)insDataList.get("delList"));
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/front/meta/metaDataList.do")
	public String metaDataList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		CommandMap headerParam = new CommandMap();
		headerParam.put("meta_m_type", GlobalConst.META_M_TYPE_BID);
		headerParam.put("meta_m", commandMap.getInt("meta_m"));
		int max_meta_depth = service.getMaxDepthAtMeta(headerParam);
		Map<String, Object> infoMap = service.selectMetaListForUsing(commandMap,max_meta_depth);
		List<List<CustomMap>> headerList = (List<List<CustomMap>>) infoMap.get("headerList");
		List<CustomMap> aloneList = (List<CustomMap>) infoMap.get("aloneList");
		model.addAttribute("max_meta_depth", max_meta_depth);
		model.addAttribute("headerList", headerList);
		PageList<CustomMap> todayList = service.selectMetaDataListContents(commandMap);
		
		Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
		//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
		List<Integer> rawKey = new ArrayList<Integer>();
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_meta_m",0);
			if(key!=0) {
				rawKey.add(key);
				valMap.put(key, cm);
			}
		}
		List<CustomMap> rawList = service.selectMetaForSearch(rawKey);
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_meta_m",0);
			for(CustomMap rm:rawList) {
				if(rm.getInt("id_meta_m",0)==key) {
					cm.putByString(rm.getString("bid_col_cd", ""), rm.getString("conts",""));
				}
			}
		}
		putModelListPage(todayList, model);
		model.addAttribute("headerList", headerList);
		return "front/meta/metaList";
	}
	public String getValueByFormula(List<CustomMap> colList,CustomMap formu) throws Exception {
		String ret = "";
		String formula = formu.getString("formula","");
		if("".equals(formula)) {
			throw new Exception("계산식이 없읍니다.");
		}
		JEP jep = new JEP();
		for(CustomMap col:colList) {
			String typ = col.getString("typ","");
			if(!"".equals(typ)&& GlobalConst.DT_NUM.equals(typ)) {
				if(!"".equals(col.getString("val", ""))) {
					jep.addVariable(col.getString("meta_cd", ""), Double.parseDouble(col.getString("val")));
				}
			}
			if(!"".equals(typ)&& GlobalConst.DT_PERCENT.equals(typ)) {
				String valStr =col.getString("val", ""); 
				if(!"".equals(valStr)) {
					valStr=valStr.replaceAll("%", "");
					double percentVal = Double.parseDouble(valStr);
					double convertedVal = percentVal / 100.0;
					jep.addVariable(col.getString("meta_cd", ""), convertedVal);
				}
			}
		}
		jep.parseExpression(formula);
		BigDecimal decimalValue = new BigDecimal(jep.getValue())
                .setScale(formu.getInt("precision_len",0), RoundingMode.HALF_UP);  // 소수점 이하 자릿수 설정
	     System.out.println("Formatted decimal value: " + decimalValue);
		return decimalValue.toString();
	}
	
}
