package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.MetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.common.service.CodeService;

@Controller
public class MetaManageController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private MetaManageService service;
	@Autowired private CodeService codeService;

	@RequestMapping("/adm/meta/metaMList.do")
	public String menuList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		PageList<CustomMap> resultList = service.selectMetaMList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/metaMList";
	}
	@RequestMapping("/adm/meta/regMetaM.do")
	public String regMetaM(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = service.getMetaM(commandMap);
		if(map!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("map", map);
		}
		return "/adm/reg/regMetaM";
	}
	@RequestMapping("/adm/meta/insertMetaMAjax.do")
	public String insertMetaM(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	service.insertMetaM(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/modifyMetaMAjax.do")
	public String modifyMetaM(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
//				List<String> codelist = new ArrayList<String>();
//				codelist.add(commandMap.getString("cb_id_group",""));
//				commandMap.put("del_code_list", codelist);
				service.modifyMetaM(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteMetaMListAjax.do")
	public String deleteMetaMListAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codeList = CollectionUtil.getCheckedListByCommandMap(commandMap, "cb_id_meta_m_");
				if(codeList.size()>0) {
					service.deleteMetaM(codeList);
					model.addAttribute("del_key_list", codeList);
				}else {
					ret=GlobalConst.ACT_NO_UPD_TRGT;
				}
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteMetaMAjax.do")
	public String deleteMetaMAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codeList = new ArrayList<String>();
				if(!"".equals(commandMap.getString("id_meta_m",""))) {
					codeList.add(commandMap.getString("id_meta_m"));
					service.deleteMetaM(codeList);
				}
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/metaList.do")
	public String metaList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		PageList<CustomMap> resultList = service.selectMetaMList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/metaList";
	}
	@RequestMapping("/adm/meta/regMeta.do")
	public String regMeta(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		CustomMap map = service.selectMeta(commandMap);
		CustomMap map = service.getMetaM(commandMap);
		if(map!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("map", map);
		}
		CommandMap typParam =new CommandMap();
		typParam.put("id_code_group", GlobalConst.CG_META_DATA_TY);
		List<CustomMap> metaList = service.selectMetaByIdMetaM(commandMap);
		List<CustomMap> typCodeList = codeService.selectCodeByIdCodeGroup(typParam);
		model.addAttribute("typCodeList", typCodeList);
		CommandMap strTypParam =new CommandMap();
		strTypParam.put("id_code_group", GlobalConst.CG_STR_LEN_TY);
		List<CustomMap> lenTypeList = codeService.selectCodeByIdCodeGroup(strTypParam);
		model.addAttribute("lenTypeList", lenTypeList);
		if(metaList!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("metaList", metaList);
		}
		return "/adm/reg/regMeta";
	}
	@RequestMapping("/adm/meta/getMetaAjax.do")
	public String getMetaAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	    		CustomMap meta = service.getMetaByIdMetaMAndIdMeta(commandMap);
	    		CommandMap pMetaParam = CollectionUtil.getCommandMapFromCommandMap(commandMap);
	    		pMetaParam.put("id_parent_meta",meta.getInt("id_meta"));
	    		List<CustomMap> childMetaList = service.selectChildMetaListByIdParentMeta(pMetaParam);
	    		model.addAttribute("metaData", meta);
	    		model.addAttribute("childMetaList", childMetaList);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/insertMetaAjax.do")
	public String insertMeta(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	Map data = service.insertMeta(commandMap);
	    		model.addAttribute("data",data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/modifyMetaAjax.do")
	public String modifyMeta(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
//				List<String> codelist = new ArrayList<String>();
//				codelist.add(commandMap.getString("cb_id_group",""));
//				commandMap.put("del_code_list", codelist);
				CustomMap data = service.modifyMeta(commandMap);
	    		model.addAttribute("data",data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteMetaAjax.do")
	public String deleteMetaAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("chk_id_meta",""));
				commandMap.put("del_code_list", codelist);
				List<String> del_code_list = service.deleteMetaByList(commandMap);
				model.addAttribute("del_code_list",del_code_list);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
}
