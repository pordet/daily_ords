<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${map.meta_m_nm} 등록"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
			<c:choose>
				<c:when test="${empty meta_row}">
					<span id="curr_title">${title}</span>
				</c:when>
				<c:otherwise>
					<span id="curr_title">${map.meta_m_nm} 수정</span>
				</c:otherwise>
			</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="listFrm" name="listFrm" method="post">
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
                    <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}">
                    <input type="hidden" name="id_bid_noti" id="id_bid_noti" value="${param.id_bid_noti}">
                    <input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
				  	<input type="hidden" name="trg_meta_m" id="trg_meta_m" value="${param.trg_meta_m}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" value="${param.page}">
					<input type="hidden" name="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" value="${param.sord}"/>
				</form>
				<form id="frm" name="frm" method="post">
                    <input type="hidden" id="doubleYn" value="Y">
                    <input type="hidden" id="checkYn" value="N">
                    <input type="hidden" name="id_meta_row" id="id_meta_row" value="${meta_row.id_meta_row}">
                    <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}">
                    <input type="hidden" name="id_bid_noti" id="id_bid_noti" value="${param.id_bid_noti}">
                    <input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
				  	<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(meta_row eq null ? 'Y':'N')}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" id="page" value="${param.page}">
					<c:set var="curr_low_num" value="1"/>
					<c:set var="curr_col_num" value="1"/>
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="15%">
								<col width="30%">
								<col width="15%;">
								<col width="35%">
							</colgroup>
							<tbody>
                                <c:forEach var="row" items="${colList}" varStatus="i">
                                <c:choose>
									<c:when test="${curr_col_num eq 1}">
<%--   									[curr_col_num는 1:[${curr_low_num}]] --%>
		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td colspan="3">
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 3}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td colspan="3">
														<textarea data-meta-id="${row.id_meta}" style="width:100%" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" placeholder="${row.meta_nm}을 입력하세요.">${row.val}</textarea>
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<c:choose>
													<c:when test="curr_col_num==1">
												</tr>
													<tr>
														<th colspan="4">${row.meta_nm}</th>
													</tr>
													</c:when>
													<c:otherwise>
													<tr>
														<th colspan="4" class="depth2">${row.meta_nm}</th>
													</tr>
													</c:otherwise>
													</c:choose>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
												</c:if>
											</c:when>
											<c:otherwise>
												<tr>
													<th>${row.meta_nm}
<%-- 													[--][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
													<c:set var="curr_col_num" value="2"/>
											</c:otherwise>
		                                </c:choose>
									</c:when>
									<c:otherwise>
<%-- 								    [curr_col_num는 2:[${curr_low_num}]] --%>
		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
													<th>${row.meta_nm}
<%-- 													[//][${curr_low_num}] --%>
													</th>
													<td colspan="3">
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<c:choose>
														<c:when test="curr_col_num==1">
												</tr>
													<tr>
														<th colspan="${fn:length(row.child_list)}">${row.meta_nm}</th>
													</tr>
														</c:when>
														<c:otherwise>
													<tr>
														<th colspan="${fn:length(row.child_list)}" class="depth${row.meta_depth}">${row.meta_nm}</th>
													</tr>
														</c:otherwise>
													</c:choose>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
												</c:if>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:when>
											<c:otherwise>
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:otherwise>
		                                </c:choose>
 									</c:otherwise>
                                </c:choose>
                                </c:forEach>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<c:if test="${!is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" id="btnInit">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete" onclick="javascript:delMeta();">
									삭제
								</button>
								<button type="button" class="btn-primary" onclick="javascript:register();">
									저장
								</button>
							</div>
						</c:if>
						<c:if test="${is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
						</c:if>
					</div>
				</form>
			</div>
	<script>
	function doList(page){
		$("#page").val(page);
		var frm =document.frm;
		var sys_code = $("#sys_code").val();
		if(sys_code){
			frm.action=contextPath+"/front/bid/bidNotiList.do";
		}else{
			frm.action=contextPath+"/front/meta/metaList.do";
		}
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function openRegContreg(id_bid_noti){
		$("#id_bid_noti").val(id_bid_noti);
		var frm =document.frm;
			frm.action=contextPath+"/front/meta/regMetaData.do";
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function delMeta(){
		var frm =document.frm;
			frm.action=contextPath+"/front/meta/deleteMetaData.do";
	// 	if(page!="")
	// 		frm.page.value=page;
	//	frm.action="/excelDownload.do";
		processing();
		frm.submit();
	// 	var frm =document.frm;
	// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
	// 	var id_parent_menu = "${param.id_parent_menu}";
	// 	if(id_parent_menu){
	// 		url+="&id_parent_menu="+id_parent_menu;
	// 	}
	// 	location.href=url;
	}
	function register(){
		var params = $("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/front/meta/insertMetaDataAjax.do";
		}else{
			$("#sort_order").val($("#sort_order_dis").val());
			url = contextPath+"/front/meta/modifyMetaDataAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		if(insert_yn =='N'){
						if(nanToStr(e.err_msg)!=''){
		    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
						}else{
		    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
						}
					}else{
						if(nanToStr(e.err_msg)!=''){
		    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
						}else{
		    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
						}
					}
		    	}else{
		    		if(insert_yn =='Y'){
			    		$("#insert_yn").val('N');
			    		$("#curr_title").html('${map.meta_m_nm} 수정');
			    		$("#id_meta_row").val(e.id_meta_row);
			    		$.each(e.insDataList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_id_meta_"'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
			    			html+='<input type="hidden" name="i_id_data_"'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
			    			if(d.typ=='${GlobalConst.DT_CALC}'){
			    				$("#id_meta_"+d.id_meta).val(d.val);
			    			}
			    			$("#id_meta_"+d.id_meta).append(html);
			    		});
		    		}else{
			    		$.each(e.insList,function(i,d){
			    			var html="";
			    			$("#id_meta_"+d.id_meta).val(d.val);
			    			html+='<input type="hidden" name="i_id_meta_"'+d.id_meta+'" id="i_id_meta_'+d.id_meta+'" value="'+d.val+'"/>';
			    			html+='<input type="hidden" name="i_id_data_"'+d.id_meta+'" id="i_id_data_'+d.id_data+'" value="'+d.id_data+'"/>';
			    			$("#id_meta_"+d.id_meta).append(html);
			    		});
			    		$.each(e.delList,function(i,d){
			    			$("#id_meta_"+d).val('');
			    			$("#i_id_meta_"+d).remove();
			    			$("#i_id_data_"+d).remove();
			    		});
			    		$.each(e.modList,function(i,d){
			    			var html="";
			    			if(d.typ =='${GlobalConst.DT_STR}' && d.len_type >${GlobalConst.DLT_1}){
				    			$("#id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_data_"+d.id_meta).val(d.id_data);
			    			}else{
				    			$("#id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_meta_"+d.id_meta).val(d.val);
				    			$("#i_id_data_"+d.id_meta).val(d.id_data);
			    			}
			    		});
		    			
		    		}
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
	</script>

</front:FrontDynamicBody>
</html>