package kr.co.daily.common.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.daily.domain.BoardVO;
import kr.co.daily.domain.CodeVO;
import kr.co.daily.domain.MenuVO;
import kr.co.daily.domain.NaviVO;

@Repository("commonDAOImpl")
public class CommonDAOImpl implements CommonDAO {

    @Autowired private SqlSession sqlSession;
    
    private static final String Namespace = "kr.co.daily.common.dao.commonDAO";


	@Override
	public CodeVO getCodeInfo(CodeVO vo) throws Exception {
		return sqlSession.selectOne(Namespace+".getCodeInfo",vo);
	}

	@Override
	public List<BoardVO> selectCodeListByCodeGroup(BoardVO vo) throws Exception {
		return sqlSession.selectList(Namespace+".selectCodeListByCodeGroup",vo);
	}
	@Override
	public List<MenuVO> selectMenu(MenuVO vo) throws Exception {
		return sqlSession.selectList(Namespace+".selectMenu",vo);
	}

	@Override
	public List<MenuVO> selectTopMenu(MenuVO vo) throws Exception {
		return sqlSession.selectList(Namespace+".selectTopMenu",vo);
	}


	@Override
	public MenuVO getMainMenuByGroupId(MenuVO vo)  throws Exception{
		return sqlSession.selectOne(Namespace+".getMainMenuByGroupId",vo);
	}

	@Override
	public List<BoardVO> selectBoardMasterList(BoardVO vo) throws Exception{
		return sqlSession.selectList(Namespace+".selectBoardMasterList",vo);
	}

	@Override
	public List<BoardVO> selectBoardMasterListByKeyword(BoardVO vo) throws Exception{
		return sqlSession.selectList(Namespace+".selectBoardMasterListByKeyword",vo);
	}

	@Override
	public List<BoardVO> selectBoardMasterRootList(BoardVO vo) throws Exception{
		return sqlSession.selectList(Namespace+".selectBoardMasterRootList",vo);
	}

	@Override
	public List<BoardVO> selectBoardMasterChildList(BoardVO vo) throws Exception{
		return sqlSession.selectList(Namespace+".selectBoardMasterChildList",vo);
	}

	@Override
	public List<MenuVO> selectMenuListForNavigationBar(NaviVO vo) throws Exception{
		return sqlSession.selectList(Namespace+".selectMenuListForNavigationBar",vo);
	}

	@Override
	public List<MenuVO> selectTopMenuByIdGroup(MenuVO vo) throws Exception {
		return sqlSession.selectList(Namespace+".selectTopMenuByIdGroup",vo);
	}

	@Override
	public List<MenuVO> selectMenuByIdGroup(MenuVO vo)throws Exception {
		return sqlSession.selectList(Namespace+".selectMenuByIdGroup",vo);
	}


}
