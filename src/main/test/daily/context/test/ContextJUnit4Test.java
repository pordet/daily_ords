package daily.context.test;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;

import daily.common.base.AbstractJUnit4ContextTestBase;
import daily.common.vo.CommandMap;

public class ContextJUnit4Test extends AbstractJUnit4ContextTestBase {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired MessageSource messageSource;

	@Test
	public void testContext() {
		Assert.assertNotNull(applicationContext.getBean("testService"));
	}

	@Test
	public void testDAO() {
		CommandMap param = new CommandMap();
		Assert.assertNotNull(param);
	}

	@Test
	public void SpringPropertiesUtil() throws Exception {
		String aa = messageSource.getMessage("adm-02-01.sector1.title", null, new Locale("rw"));
//		String aa = SpringPropertiesUtil.getProperty("adm-02-01.sector1.title", "en");
		System.out.println("attr1:" + aa);
//    	System.out.println("attr2:"+new String(ppe.get("excel.usrList.head").getBytes("ISO-8859-1"),"UTF-8"));

	}
}
