package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.AuthorityManageService;
import kr.co.daily.admin.service.CodeManageService;
import kr.co.daily.admin.service.MenuManageService;
import kr.co.daily.admin.service.UserManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class UserManageController extends AbstractControllerBase {

	private PasswordEncoder  encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	@Autowired private UserManageService service;
	@Autowired private CodeManageService codeService;
	@Autowired private MenuManageService mnuService;
	@Autowired private AuthorityManageService authService;
	private Logger log = LoggerFactory.getLogger(UserManageController.class);
	
	@RequestMapping("/adm/user/userList.do")
	public String userList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap.put("code_group", GlobalConst.CG_LANGU);
		PageList<CustomMap> resultList = service.selectUserList(commandMap);
		log.info("resultList.toString()");
		log.info(resultList.toString());
	    this.putModelListPage(resultList, model);
		return "/adm/userList";
	}
	@RequestMapping("/adm/user/regUser.do")
	public String regUser(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByLoginInfo(commandMap);
		commandMap.put("code_group", GlobalConst.CG_LANGU);
		List<CustomMap> codeList = codeService.selectCodeComboByLocale(commandMap);
		model.put("langCodeList", codeList);
		
		
		CommandMap groupMap =new CommandMap();
		List<CustomMap> groupList = service.selectGroupList(groupMap);
		model.put("groupList", groupList);
		
		if(!"".equals(commandMap.getString("user_seq",""))) {
			CustomMap map = service.getUser(commandMap);
			CommandMap seParm = new CommandMap();
			seParm.put("id_user", commandMap.getString("user_id"));
			model.addAttribute("map",map);
		}
		return "/adm/reg/regUser";
	}
	@RequestMapping("/adm/user/aliasCheckAjax.do")
	public String aliasCheckAjax(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = service.getUserByAlias(commandMap);
		model.addAttribute("map",map);
		return "jsonView";
	}
	@RequestMapping("/adm/user/userGroupList.do")
	public String userGroupList(CommandMap commandMap, ModelMap model) throws Exception {
		PageList<CustomMap> resultList = service.selectUserGroupList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/userGroupList";
	}
	@RequestMapping("/adm/user/regUserGroup.do")
	public String regUserGroup(CommandMap commandMap, ModelMap model) throws Exception {
        commandMap = addParamByLoginInfo(commandMap);
		CustomMap map = service.getUserGroup(commandMap);
		model.addAttribute("map",map);
		List<CustomMap> roleList = authService.selectRoleList(commandMap);
		model.addAttribute("roleList",roleList);
		List<CustomMap> programList = mnuService.selectStartPageList(commandMap);
		model.addAttribute("programList",programList);
		List<CustomMap> groupRoleList = authService.selectRoleListByGroup(commandMap);
		model.addAttribute("groupRoleList",groupRoleList);
		return "/adm/reg/regUserGroup";
	}
	@RequestMapping("/adm/user/deleteUsrGroup.do")
	public String deleteUsrGroup(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			service.deleteUsrGroup(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("act_type","D");
		model.addAttribute("ret",ret);
		model.addAttribute("id_menu",commandMap.getString("id_menu",""));
		return "redirect:/adm/user/userGroupList.do";
	}
	@RequestMapping("/adm/user/insertUsrGroupAjax.do")
	public String insertUsrGroup(CommandMap commandMap, ModelMap model, HttpServletRequest req) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			List<String> rolelist = new ArrayList<String>();
			if(commandMap.get("hdn_id_role") instanceof String) {
				rolelist.add(commandMap.getString("hdn_id_role",""));
			}else {
				String[] delRole = (String[])commandMap.get("hdn_id_role");
				if(delRole!=null) {
					for(Object obj:delRole) {
						rolelist.add(String.valueOf(obj));
					}
				}
			}
			service.insertUsrGroup(commandMap,rolelist);
	        String id_group = commandMap.getString("id_group");
	        model.addAttribute("id_group", id_group);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/user/modifyUsrGroupAjax.do")
	public String modifyUsrGroup(CommandMap commandMap, ModelMap model, HttpServletRequest req) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			List<String> rolelist = new ArrayList<String>();
			if(commandMap.get("hdn_id_role") instanceof String) {
				rolelist.add(commandMap.getString("hdn_id_role",""));
			}else {
				String[] delRole = (String[])commandMap.get("hdn_id_role");
				if(delRole!=null) {
					for(Object obj:delRole) {
						rolelist.add(String.valueOf(obj));
					}
				}
			}
			List<String> delList = new ArrayList<String>();
			List<CustomMap> groupRoleList = authService.selectRoleListByGroup(commandMap);
	        for(CustomMap groupRole : groupRoleList) {
	        	boolean isExist = false;
	        	for(String id_role:rolelist) {
	        		if(groupRole.getString("id_role").equals(id_role)) {
	        			isExist = true;
	        			break;
	        		}
	        	}
	        	if(!isExist) {
	        		delList.add(groupRole.getString("id_group_role"));
	        	}
	        }
	        List<String> insList = new ArrayList<String>();
	        for(String id_role:rolelist) {
	        	boolean isExist = false;
	        	for(CustomMap groupRole : groupRoleList) {
	        		if(groupRole.getString("id_role").equals(id_role)) {
	        			isExist = true;
	        			break;
	        		}
	        	}
	        	if(!isExist) {
	        		insList.add(id_role);
	        	}
	        }
	        
			service.modifyUsrGroup(commandMap,delList,insList);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/user/deleteUsrGroupAjax.do")
	public String deleteUsrGroupAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("id_group",""));
				commandMap.put("del_code_list", codelist);
				service.deleteUsrGroup(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/user/deleteUserList.do")
	public String deleteUsr(CommandMap commandMap, ModelMap model) throws Exception {
		log.info("commandMap");
		log.info(commandMap.toString());
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			service.deleteUsr(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		commandMap.put("code_group", GlobalConst.CG_LANGU);
		PageList<CustomMap> resultList = service.selectUserList(commandMap);
		putModelListPage(resultList,model);
		model.addAttribute("act_type","D");
		model.addAttribute("ret",ret);
		return "adm/userList";
	}
	@RequestMapping("/adm/user/insertUsrAjax.do")
	public String insertUsrAjax(CommandMap commandMap, ModelMap model, HttpServletRequest req) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				String encPassword = encoder.encode(commandMap.getString("user_pwd", ""));
				commandMap.put("user_pwd", encPassword);
	        	service.insertUsr(commandMap);
	        	model.addAttribute("map", commandMap.getMap());
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/user/modifyUsrAjax.do")
	public String modifyUsr(CommandMap commandMap, ModelMap model, HttpServletRequest req) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	if(!"".equals(commandMap.getString("user_pwd", "").trim())) {
	        		String encPassword = encoder.encode(commandMap.getString("user_pwd", ""));
	        		commandMap.put("mod_user_pwd", encPassword);
	        	}
				service.modifyUsr(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/user/deleteUsrAjax.do")
	public String deleteUsrAjax(CommandMap commandMap, ModelMap model, HttpServletRequest req) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("user_seq",""));
				commandMap.put("del_code_list", codelist);
				service.deleteUsr(commandMap);;
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
}
