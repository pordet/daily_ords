package daily.common.excel.exception;

public class ExcelDataException extends Exception {

	public ExcelDataException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1577093367166060970L;
	
}
