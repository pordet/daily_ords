package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Repository
public interface MetaDAO {

	PageList<CustomMap> selectMetaMList(Map<String, Object> map, PageBounds pageBounds);

	CustomMap getMetaM(Map<String, Object> map);

	List<CustomMap> selectMeta(Map<String, Object> map);

	PageList<CustomMap> selectMetaRow(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectMetaDataForSearch(List<Integer> lowKey);

	List<CustomMap> selectMetaListForUsing(Map<String, Object> map);

	PageList<CustomMap> selectMetaDataListContents(Map<String, Object> map);

	List<CustomMap> selectMetaForSearch(List<Integer> rawKey);

	void insertMetaRow(Map<String, Object> map);

	void insertMetaDataByGroup(List<Map> inInputList);

	CustomMap getMetaRowByKey(Map<String, Object> map);

	void modifyMetaRow(Map<String, Object> map);

	void modifyMetaRowByGroup(List<Map> inInputList);

	List<CustomMap> selectMetaDataByIdRow(int int1);

	List<CustomMap> selectMetaForList(Map<String, Object> map);

	List<CustomMap> selectMetaKeyM(Map<String, Object> map);

	List<CustomMap> selectContsKeyList(Map<String, Object> map);

	int getMaxDepthAtMeta(Map<String, Object> map);

	List<CustomMap> selectTopDepthMetaListForHeader(Map<String, Object> map);

	List<CustomMap> selectDepthMetaListRemoveTopDepthForHeader(Map<String, Object> map);

	List<CustomMap> selectMetaOrderList(Map<String, Object> map);

	List<CustomMap> selectMetaDataByIdMetaRow(Map<String, Object> map);

	PageList<CustomMap> selectMetaDataContents(Map<String, Object> map, PageBounds pageBounds);

	void updateMetaData(Map<String, Object> map);

	void deleteMetaDataByList(List<Integer> delList);

	List<CustomMap> getDateTimeColByIdMetaM(Map<String, Object> map);

	List<CustomMap> selectCalcuListByParam(Map<String, Object> map);

	PageList<CustomMap> selectTreeMetaDataContents(Map<String, Object> map, PageBounds pageBounds);

	void deleteMetaData(Map<String, Object> map);

	void deleteMetaRow(Map<String, Object> map);

}
