<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tree" uri="/WEB-INF/tlds/tree.tld" %>
<tree:TreeCodeList id_justiciable="${id_justiciable}" code_group="NA" type="nat" locale="${locale}" listName="natTreeList"/>
<tree:TreeCodeList id_justiciable="${id_justiciable}" code_group="PF" type="prof" locale="${locale}" listName="profTreeList"/>
<tree:TreeCodeList id_justiciable="${id_justiciable}" code_group="OC" type="occu" locale="${locale}" listName="occuTreeList"/>
<%@ attribute name="openTreeId" required="true" %>
<%@ attribute name="id_justiciable" required="true" %>
<%@ attribute name="code_group_citizen_type" required="true" %>
<%@ attribute name="code_group_gender" required="true" %>
<%@ attribute name="code_group_civil_status" required="true" %>
<%@ attribute name="code_group_conviction_type" required="true" %>
<%@ attribute name="code_group_type_release" required="true" %>
<%@ attribute name="code_group_motifs" required="true" %>
<%@ attribute name="code_group_doc_type" required="true" %>
<%@ attribute name="locale" required="true" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
                <div class="main-tree">
                    <div class="tree-header">
                        <span>Input Item Information</span>
                        <button type="button" class="tree-close-btn">btn</button>
                    </div>
					<div class="tree-content">
						<div class="item-title"><i class="icon_item"></i>${appliInfo.firstName}<c:if test="${not empty appliInfo.middleName}">&nbsp;${appliInfo.middleName}</c:if>
						<c:if test="${not empty appliInfo.lastName}">&nbsp;${appliInfo.lastName}</c:if>
						</div>
						<ul class="comm-tree">
						    <li class="comm-treeview ${(openTreeId eq '1'?'tree-open':'')}" id="tree_1"><button type="button" class="comm-tree-expander"></button>
								<div><a href="#" id="t_ICN" class="js-action-a"><i class="icon_folder01"></i><spring:message code="adm-04.tab01"/></a></div>
								<ul class="comm-treeview-list" style="display: none;">
								   <li><div><a href="#" id="t_ICN_001" class="js-action-a" onclick="javascript:focusField('ICN_001');"><i class="icon_keyboard"></i><spring:message code="name.01"/> : ${appliInfo.firstName}</a></div></li>
								   <li><div><a href="#" id="t_ICN_002" class="js-action-a" onclick="javascript:focusField('ICN_002');"><i class="icon_keyboard"></i><spring:message code="name.02"/> : ${appliInfo.middleName}</a></div></li>
								   <li><div><a href="#" id="t_ICN_003" class="js-action-a" onclick="javascript:focusField('ICN_003');"><i class="icon_keyboard"></i><spring:message code="name.03"/> : ${appliInfo.lastName}</a></div></li>
								   <li><div><a href="#" id="t_ICN_004" class="js-action-a" onclick="javascript:focusField('ICN_004');"><i class="icon_keyboard"></i><spring:message code="Gender.nm"/> : ${appliInfo.gender}</a></div></li>
								   <li><div><a href="#" id="t_ICN_005" class="js-action-a" onclick="javascript:focusField('ICN_005');"><i class="icon_keyboard"></i><spring:message code="person.info.01"/> : ${appliInfo.birthday}</a></div></li>
								   <li><div><a href="#" id="t_ICN_006" class="js-action-a" onclick="javascript:focusField('ICN_006');"><i class="icon_keyboard"></i><spring:message code="Civil.Status"/> : ${appliInfo.civilStatus}</a></div></li>
								   <li><button type="button" class="comm-tree-expander"></button>
									   <div><a href="#" id="t_ICN_009" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="name.06"/></a></div>
									   <ul class="comm-treeview-list" style="display: block;">
											<li id="t_ICN_0091" onclick="javascript:focusField('ICN_0091');"><spring:message code="name.01"/> : ${appliInfo.fatherFirstName}</li>
										    <li id="t_ICN_0092" onclick="javascript:focusField('ICN_0092');"><spring:message code="name.03"/> : ${appliInfo.fatherLastName}</li>
									   </ul>
								   </li>
						    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_010" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="name.07"/></a></div>
										<ul class="comm-treeview-list" style="display: block;">
											<li id="t_ICN_0101" onclick="javascript:focusField('ICN_0101');"><spring:message code="name.01"/> : ${appliInfo.motherFirstName}</li>
											<li id="t_ICN_0102" onclick="javascript:focusField('ICN_0102');"><spring:message code="name.03"/> : ${appliInfo.motherLastName}</li>
										</ul>
									</li>
						    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_010" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="name.11"/></a></div>
										<ul class="comm-treeview-list" style="display: block;">
											<li id="t_ICN_0101" onclick="javascript:focusField('ICN_0101');"><spring:message code="name.01"/> : ${appliInfo.spouseFirstName}</li>
											<li id="t_ICN_0102" onclick="javascript:focusField('ICN_0102');"><spring:message code="name.03"/> : ${appliInfo.spouseLastName}</li>
										</ul>
									</li>
									<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_012" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="address.type.01"/></a></div>
										<ul class="comm-treeview-list" style="display: block;">
				                         <c:choose>
											<c:when test="${(appliInfo.citizenType eq 3 or appliInfo.citizenType eq 4 or appliInfo.citizenType eq 5) and appliInfo.socialNumType eq 'P'}">
													<c:choose>
														<c:when test="${appliInfo.citizenType eq 4}">
										   		<li id="t_ICN_0121" onclick="javascript:focusField('ICN_0121');"><spring:message code="address.01"/> : ${appliInfo.birthNation}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="address.type.07"/> : ${appliInfo.nidaPlaceOfBirth}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="date.type.01"/> : ${appliInfo.arrivalDate}</li>
														</c:when>
														<c:when test="${appliInfo.citizenType eq 5}">
										   		<li id="t_ICN_0121" onclick="javascript:focusField('ICN_0121');"><spring:message code="address.01"/> : ${appliInfo.birthNation}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="address.type.07"/> : ${appliInfo.nidaPlaceOfBirth}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="date.type.01"/> : ${appliInfo.arrivalDate}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="date.type.02"/> : ${appliInfo.depatureDate}</li>
														</c:when>
														<c:otherwise>
										   		<li id="t_ICN_0121" onclick="javascript:focusField('ICN_0121');"><spring:message code="address.01"/> : ${appliInfo.birthNation}</li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="address.type.07"/> : ${appliInfo.nidaPlaceOfBirth}</li>
														</c:otherwise>
													</c:choose>
											</c:when>
											<c:otherwise>
											   <li id="t_ICN_0131" onclick="javascript:focusField('ICN_0131');"><spring:message code="address.01"/> : ${appliInfo.birthNation}</li>
											   <li id="t_ICN_0132" onclick="javascript:focusField('ICN_0132');"><spring:message code="address.02"/> : ${appliInfo.birthProvince}</li>
											   <li id="t_ICN_0133" onclick="javascript:focusField('ICN_0133');"><spring:message code="address.03"/> : ${appliInfo.birthDistrict}</li>
											   <li id="t_ICN_0134" onclick="javascript:focusField('ICN_0134');"><spring:message code="address.04"/> : ${appliInfo.birthSector}</li>
											   <li id="t_ICN_0135" onclick="javascript:focusField('ICN_0135');"><spring:message code="address.05"/> : ${appliInfo.birthCell}</li>
											</c:otherwise>
										</c:choose>
										</ul>
									</li>
						    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_013" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="address.type.02"/></a></div>
										<ul class="comm-treeview-list" style="display: block;">
				                         <c:choose>
											<c:when test="${appliInfo.citizenType eq 1 and appliInfo.socialNumType eq 'P'}">
											   <li id="t_ICN_0131" onclick="javascript:focusField('ICN_0131');"><spring:message code="address.01"/> : ${appliInfo.residNation}</li>
											   <li id="t_ICN_0132" onclick="javascript:focusField('ICN_0132');"><spring:message code="address.02"/> : ${appliInfo.residProvince}</li>
											   <li id="t_ICN_0133" onclick="javascript:focusField('ICN_0133');"><spring:message code="address.03"/> : ${appliInfo.residDistrict}</li>
											   <li id="t_ICN_0134" onclick="javascript:focusField('ICN_0134');"><spring:message code="address.04"/> : ${appliInfo.residSector}</li>
											   <li id="t_ICN_0135" onclick="javascript:focusField('ICN_0135');"><spring:message code="address.05"/> : ${appliInfo.residCell}</li>
											</c:when>
											<c:when test="${(appliInfo.citizenType eq 3 or appliInfo.citizenType eq 4 or appliInfo.citizenType eq 5) and appliInfo.socialNumType eq 'P'}">
										   		<li id="t_ICN_0121" onclick="javascript:focusField('ICN_0121');"><spring:message code="address.01"/> : ${appliInfo.residNation} </li>
											    <li id="t_ICN_0122" onclick="javascript:focusField('ICN_0122');"><spring:message code="address.type.07"/> : ${appliInfo.nidaPlaceOfResid}</li>
											</c:when>
											<c:otherwise>
											   <li id="t_ICN_0131" onclick="javascript:focusField('ICN_0131');"><spring:message code="address.01"/> : ${appliInfo.residNation}</li>
											   <li id="t_ICN_0132" onclick="javascript:focusField('ICN_0132');"><spring:message code="address.02"/> : ${appliInfo.residProvince}</li>
											   <li id="t_ICN_0133" onclick="javascript:focusField('ICN_0133');"><spring:message code="address.03"/> : ${appliInfo.residDistrict}</li>
											   <li id="t_ICN_0134" onclick="javascript:focusField('ICN_0134');"><spring:message code="address.04"/> : ${appliInfo.residSector}</li>
											   <li id="t_ICN_0135" onclick="javascript:focusField('ICN_0135');"><spring:message code="address.05"/> : ${appliInfo.residCell}</li>
											</c:otherwise>
										</c:choose>
									   </ul>
									</li>
						    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_014" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="address.type.03"/></a></div>
										<ul class="comm-treeview-list" style="display: none;">
										   <li id="t_ICN_0131" onclick="javascript:focusField('ICN_0131');"><spring:message code="address.01"/> : ${appliInfo.regisNation}</li>
										   <li id="t_ICN_0132" onclick="javascript:focusField('ICN_0132');"><spring:message code="address.02"/> : ${appliInfo.regisProvince}</li>
										   <li id="t_ICN_0133" onclick="javascript:focusField('ICN_0133');"><spring:message code="address.03"/> : ${appliInfo.regisDistrict}</li>
										   <li id="t_ICN_0134" onclick="javascript:focusField('ICN_0134');"><spring:message code="address.04"/> : ${appliInfo.regisSector}</li>
										   <li id="t_ICN_0135" onclick="javascript:focusField('ICN_0135');"><spring:message code="address.05"/> : ${appliInfo.regisCell}</li>
									   </ul>
									</li>
									<c:set var="hasNation" value="${fn:length(natTreeList)!=0}"/>
									<c:set var="hasProf" value="${fn:length(profTreeList)!=0}"/>
									<c:set var="hasOccu" value="${fn:length(occuTreeList)!=0}"/>
									<li>
									  <c:if test="${hasNation eq true}">
									  <button type="button" class="comm-tree-expander"></button>
									  </c:if>
										<div>
											<a href="#" id="t_ICN_021" class="js-action-a" onclick="javascript:focusField('ICN_021');"><i class="icon_keyboard"></i><spring:message code="usr-01-09.sub1.lbl11"/></a>
										</div>
									  <c:if test="${hasNation eq true}">
										<ul class="comm-treeview-list" style="display: block;">
									  	<c:forEach var="code" items="${natTreeList}">
											<li>${code.codeName}</li>
									  	</c:forEach>
									    </ul>
									  </c:if>
									</li>
								
									<li>
									  <c:if test="${hasProf eq true}">
									  <button type="button" class="comm-tree-expander"></button>
									  </c:if>
										<div>
											<a href="#" id="t_ICN_022" class="js-action-a" onclick="javascript:focusField('ICN_022');"><i class="icon_keyboard"></i><spring:message code="usr-01-09.sub1.lbl12"/></a>
										</div>
									  <c:if test="${hasProf eq true}">
										<ul class="comm-treeview-list" style="display: block;">
									  	<c:forEach var="code" items="${profTreeList}">
											<li>${code.codeName}</li>
									  	</c:forEach>
									    </ul>
									  </c:if>
									</li>
								<li>
								  <c:if test="${hasOccu eq true}">
								  <button type="button" class="comm-tree-expander"></button>
								  </c:if>
									<div>
										<a href="#" id="t_ICN_023" class="js-action-a" onclick="javascript:focusField('ICN_023');"><i class="icon_keyboard"></i><spring:message code="usr-01-09.sub1.lbl13"/></a>
									</div>
								  <c:if test="${hasOccu eq true}">
									<ul class="comm-treeview-list" style="display: block;">
								  	<c:forEach var="code" items="${occuTreeList}">
										<li>${code.codeName}</li>
								  	</c:forEach>
								    </ul>
								  </c:if>
								</li>
							</ul>
						</li>
					    <li class="comm-treeview ${(openTreeId eq '2'?'tree-open':'')}" id="tree_2"><button type="button" class="comm-tree-expander"></button>
							<div><a href="#" id="t_OCN" class="js-action-a"><i class="icon_folder02"></i><spring:message code="adm-04.tab02"/></a></div>
							<ul class="comm-treeview-list" style="display: none;">
								<c:forEach var="offen" items="${offenseList}">
								<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_012" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col2"/> : ${offen.idDossier}</a></div>
									<ul class="comm-treeview-list" style="display: block;">
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('CCN_011');"><i class="icon_keyboard"></i><spring:message code="adm-04-02.sec2.col2"/> : ${offen.file}</a></div></li>
									   <li><div><a href="#" id="t_CCN_012" class="js-action-a" onclick="javascript:focusField('CCN_012');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${offen.date}</a></div></li>
									   <li><div><a href="#" id="t_CCN_013" class="js-action-a" onclick="javascript:focusField('CCN_013');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl4"/> : ${offen.detail}</a></div></li>
								   </ul>
								</li>
								</c:forEach>
							</ul>
						</li>
						<li class="comm-treeview ${(openTreeId eq '3'?'tree-open':'')}" id="tree_3"><button type="button" class="comm-tree-expander"></button>
							<div><a href="#" id="t_CCN" class="js-action-a"><i class="icon_folder03"></i><spring:message code="adm-04.tab03"/></a></div>
							<ul class="comm-treeview-list" style="display: none;">
								<c:forEach var="conv" items="${sentenceList}">
					    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_009" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl2"/> : ${conv.fileNo}</a></div>
									<ul class="comm-treeview-list" style="display: block;">
									   <li><div><a href="#" id="t_CCN_010" class="js-action-a" onclick="javascript:focusField('CCN_010');"><i class="icon_keyboard"></i><spring:message code="adm-04-03.his.col3"/> : ${conv.juridiction}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('CCN_011');"><i class="icon_keyboard"></i><spring:message code="adm-04-03.his.col2"/> : ${conv.codeName}</a></div></li>
									   <li><div><a href="#" id="t_CCN_012" class="js-action-a" onclick="javascript:focusField('CCN_012');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${conv.dateCondam}</a></div></li>
									   <li><div><a href="#" id="t_CCN_013" class="js-action-a" onclick="javascript:focusField('CCN_013');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl4"/> : ${conv.detail}</a></div></li>
								   </ul>
								</li>
								</c:forEach>
							</ul>
						</li>
						<li class="comm-treeview ${(openTreeId eq '4'?'tree-open':'')}" id="tree_4"><button type="button" class="comm-tree-expander"></button>
						<div><a href="#" id="t_RCN" class="js-action-a"><i class="icon_folder04"></i><spring:message code="adm-04.tab04"/></a></div>
							<ul class="comm-treeview-list" style="display: none;">
								<c:forEach var="conv" items="${releaseList}">
					    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_009" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col1"/> : ${conv.ressortName}</a></div>
									<ul class="comm-treeview-list" style="display: block;">
									   <li><div><a href="#" id="t_CCN_010" class="js-action-a" onclick="javascript:focusField('RCN_003');"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col2"/> : ${conv.fileNm}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('RCN_004');"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col3"/> : ${conv.codeName}</a></div></li>
									   <li><div><a href="#" id="t_CCN_012" class="js-action-a" onclick="javascript:focusField('RCN_005');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${conv.dateCondam}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('RCN_006');"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col5"/> : ${conv.libDate}</a></div></li>
									   <li><div><a href="#" id="t_CCN_013" class="js-action-a" onclick="javascript:focusField('RCN_007');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl4"/> : ${conv.detail}</a></div></li>
								   </ul>
								</li>
								</c:forEach>
						   	</ul>
						</li>
						<li class="comm-treeview ${(openTreeId eq '5'?'tree-open':'')}" id="tree_5"><button type="button" class="comm-tree-expander"></button>
							<div><a href="#" id="t_ECN" class="js-action-a"><i class="icon_folder05"></i><spring:message code="adm-04.tab05"/></a></div>
							<ul class="comm-treeview-list" style="display: none;">
								<c:forEach var="conv" items="${crsInfoList}">
					    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_009" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${conv.emisDate}</a></div>
									<ul class="comm-treeview-list" style="display: block;">
									   <li><div><a href="#" id="t_CCN_010" class="js-action-a" onclick="javascript:focusField('ECN_002');"><i class="icon_keyboard"></i><spring:message code="adm-04-05.sec2.lbl3"/> : ${conv.reasonName}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('ECN_003');"><i class="icon_keyboard"></i><spring:message code="adm-04-05.sec2.lbl4"/> : ${conv.quit}</a></div></li>
									   <li><div><a href="#" id="t_CCN_012" class="js-action-a" onclick="javascript:focusField('ECN_004');"><i class="icon_keyboard"></i><spring:message code="adm-04-05.sec2.lbl6"/> : ${conv.montant}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('ECN_005');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${conv.quitdate}</a></div></li>
									   <li><div><a href="#" id="t_CCN_013" class="js-action-a" onclick="javascript:focusField('ECN_006');"><i class="icon_keyboard"></i><spring:message code="adm-04-05.his.col2"/> : ${conv.ressortName}</a></div></li>
								   </ul>
								</li>
								</c:forEach>
							 </ul>
						</li>
						
						<li class="comm-treeview ${(openTreeId eq '6'?'tree-open':'')}" id="tree_6"><button type="button" class="comm-tree-expander"></button>
							<div><a href="#" id="t_DCN" class="js-action-a"><i class="icon_folder06"></i><spring:message code="adm-04.tab06"/></a></div>
							<ul class="comm-treeview-list" style="display: none;">
								<c:forEach var="conv" items="${documentList}">
					    		<li><button type="button" class="comm-tree-expander"></button><div><a href="#" id="t_ICN_009" class="js-action-a"><i class="icon_keyboard"></i><spring:message code="adm-04-04.his.col1"/> : ${conv.docTitle}</a></div>
									<ul class="comm-treeview-list" style="display: block;">
									   <li><div><a href="#" id="t_CCN_010" class="js-action-a" onclick="javascript:focusField('DCN_001');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl5"/> : ${conv.docTitle}</a></div></li>
									   <li><div><a href="#" id="t_CCN_011" class="js-action-a" onclick="javascript:focusField('DCN_002');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl1"/> : ${conv.codeName}</a></div></li>
									   <li><div><a href="#" id="t_CCN_012" class="js-action-a" onclick="javascript:focusField('DCN_003');"><i class="icon_keyboard"></i><spring:message code="adm.comm.lbl3"/> : ${conv.docDt}</a></div></li>
								   </ul>
								</li>
								</c:forEach>
							</ul>
						</li>
					</ul>
				</div>
			</div>
					<script>
function focusField(id){
	$("#id").focus();
}
function setLabel(id,nm,tgtId){
	if($.trim($("#"+tgtId).val())==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+$("#"+tgtId).val());
	}
}
function setDateLabel(id,nm,tgtId,dateText){
	if(dateText==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+dateText);
	}
}
function setComboLabel(id,nm,tgtId){
	if($.trim($("#"+tgtId+ " option:selected").val())==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+$("#"+tgtId+ " option:selected").text());
	}
}
$(function () {
	treeMenuInit();
	function treeMenuInit(){
		$('.comm-tree li').each(function(){
			if($(this).find($('.comm-treeview-list')).length>0){
				$('.comm-treeview-list',this).hide();
				$(this).prepend('<button type="button" class="comm-tree-expander"></button>')
			};
		});

		$(document).on('click', '.comm-tree .comm-tree-expander',function(){
			var thisBtn, thisItem;
			if($(this).hasClass('comm-tree-expander')){
				thisBtn = $(this);
			}else{
				thisBtn = $(this).closest('li').children('.comm-tree-expander');
			}
			 thisItem = thisBtn.parent();
			
			if (thisItem.is('.tree-open')) {
				thisBtn.add(thisItem.find('.tree-open').find('.comm-tree-expander'))
				thisItem.add(thisItem.find('.tree-open')).removeClass('tree-open').find('.comm-treeview-list').hide();
			} else {
				thisItem.children('.comm-treeview-list').stop().show();
				thisItem.addClass('tree-open');
			}
		});
	};
	$("#tree_${openTreeId}").children('.comm-treeview-list').stop().show();
	$("#tree_${openTreeId}").addClass('tree-open');

});

</script>
