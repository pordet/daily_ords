/*
function show_map1(){
	$("#zoneMapModal").modal('show');		
}
function show_map(){
		var params = $("#frm").serialize();
		var url=contextPath+'/getZoneSysAjax.do';
//		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
//		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.rowList.length==0){
		    			
		    		}else{
		    			$("#zone_sys_tab").empty();
		    			var html='';
		    			html+='<thead>';
		    			html+='<tr>';
		    			$.each(e.header,function(i,d){
		    				if(i==0){
		    					if(nanToStr(d)!=''){
					    			html+='<th class="first" >'+nanToStr(d.zone_sys_div_nm)+'</th>';
		    					}else{
					    			html+='<th class="first" ></th>';
		    					}
				    			
		    				}else{
		    					if(nanToStr(d)!=''){
					    			html+='<th scope="col">'+nanToStr(d.zone_sys_div_nm)+'</th>';
		    					}else{
					    			html+='<th scope="col"></th>';
		    					}
		    				}
		    			});
		    			html+='<th scope="col" colspan="2">Zone No</th>';
		    			html+='</tr>';
		    			html+='</thead>';
		    			html+='<tbody>';
			    		$.each(e.rowList,function(i,d){
			    			html+='<tr>';
			    			html+='<td rowspan="'+d.rowspan+'">'+d.zone_nm+'</td>';
			    			var c = d.child[0];
			    			html+='<td rowspan="'+c.rowspan+'">'+nanToStr(c.zone_nm)+'</td>';
			    			var gc = c.child[0];
			    			if(nanToStr(gc.sec_end_no)==''){
				    			html+='<td colspan="2">'+gc.sec_start_no+'</td>';
			    			}else{
				    			html+='<td>'+gc.sec_start_no+' 부터</td>';
				    			html+='<td>'+gc.sec_end_no+' 까지</td>';
			    			}
			    			html+='</tr>';
			    			for(var j=1;j<c.child.length;j++){
				    			var ic = c.child[j];
				    			html+='<tr>';
				    			if(nanToStr(ic.sec_end_no)==''){
					    			html+='<td colspan="2">'+ic.sec_start_no+'</td>';
				    			}else{
					    			html+='<td>'+ic.sec_start_no+' 부터</td>';
					    			html+='<td>'+ic.sec_end_no+' 까지</td>';
				    			}
				    			html+='</tr>';
			    			}
			    			for(var i=1;i<d.child.length;i++){
				    			var ic = d.child[i];
				    			html+='<tr>';
				    			html+='<td rowspan="'+ic.rowspan+'">'+ic.zone_nm+'</td>';
				    			var igc = ic.child[0];
				    			if(nanToStr(igc.sec_end_no)==''){
					    			html+='<td colspan="2">'+igc.sec_start_no+'</td>';
				    			}else{
					    			html+='<td>'+igc.sec_start_no+' 부터</td>';
					    			html+='<td>'+igc.sec_end_no+' 까지</td>';
				    			}
				    			html+='</tr>';
				    			for(var j=1;j<ic.child.length;j++){
				    				var igc = ic.child[j];
					    			html+='<tr>';
					    			if(nanToStr(igc.sec_end_no)==''){
						    			html+='<td colspan="2">'+igc.sec_start_no+'</td>';
					    			}else{
						    			html+='<td>'+igc.sec_start_no+' 부터</td>';
						    			html+='<td>'+igc.sec_end_no+' 까지</td>';
					    			}
					    			html+='</tr>';
				    			}
			    			}
			    		});
		    			html+='</tbody>';

		    			$("#zone_sys_tab").html(html);
			    		$("#zoneMapModal").modal('show');	
		    		}
		    	}
		    },
		    error : function(e){
//		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}
*/
/*
function show_zone_detail(no){
	//var title = $("#id_category option:selected").text()+' '+$("#id_nat_area option:selected").text()+' '+$("#id_city_area option:selected").text()+' '+'Zone '+no +' 정보';
	//var title = "Zone " +$("#id_category option:selected").text()+' '+ '정보';
	var title = "Zone " + iniParam.category + ' 정보';
	
	$("#docHeaderTitle").html(title);
	var url=contextPath+'/getZoneDetailAjax.do';
	var params = {zone_no:no,id_nat_area:$("#id_nat_area").val(),id_city_area:$("#id_city_area").val()} 
	
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.yearList.length==0){
	    		}else{
	    			$("#zoneDetailTable").empty();
	    			//var title = $("#id_category option:selected").text()+' '+$("#id_nat_area option:selected").text()+' '+$("#id_city_area option:selected").text();
	    			//var title = $("#id_nat_area option:selected").text()
					//		+ ' ' +$("#id_city_area option:selected").text() 
							//+ ' ' + $("#id_category option:selected").text();
	    			var title = iniParam.natArea + iniParam.cityArea;
	    			if(nanToStr(e.zoneDetail)!='' && nanToStr(e.zoneDetail.zone_district)!='')
	    				title+=' '+e.zoneDetail.zone_district;
	    			if(nanToStr(e.zoneDetail)!='' && nanToStr(e.zoneDetail.zone_name)!='')
	    				title+=' '+e.zoneDetail.zone_name;
	    			//title+=' '+'Zone '+no +' 정보';
	    			title+=' '+'Zone '+ no 
						//+ ' ' + $("#id_category option:selected").text();
	    			$("#docBodyTitle").html(title);
	    			var html='';
	    			html+='<thead>';
	    			html+='<tr>';
	    			html+='<th class="first">항목</th>';
	    			$.each(e.yearList,function(i,d){
    					if(nanToStr(d)!=''){
			    			html+='<th scope="col">'+nanToStr(d.year)+'년</th>';
    					}else{
			    			html+='<th scope="col"></th>';
    					}
	    			});
	    			html+='</tr>';
	    			html+='</thead>';
	    			html+='<tbody>';
					
		    		$.each(e.metaDataList,function(i,d){
		    			html+='<tr>';
		    			html+='<td>'+d.unit_nm+'</td>';
						
						if(e.zoneDataList.length > 0) {
							var check = false;
				    		$.each(e.zoneDataList,function(j,f){
				    			if(d.id_meta_unit==f.id_meta_unit) {
									check = true;
				    				html+='<td>'+f.val+'</td>';
								}
			    			});
							if(!check) {
								html += '<td> - </td>';
							}
						} else {
							for(var idx=0; idx<e.yearList.length; idx++) {
								html += '<td> - </td>';
							}
						}
		    			html+='</tr>';
		    		});
	    			html+='</tbody>';

	    			$("#zoneDetailTable").html(html);
	    		}
				
	    		if(e.trafDataList.length==0){
	    			var html='';
	    			$("#zoneTrafTable").html('<tr><td>검색결과 없음</td></tr>');
	    		}else{
	    			var secCnt = 4;
	    			$("#zoneTrafTable").empty();
	    			var html='';
	    			html+='<thead>';
	    			html+='<tr>';
	    			for(var i=0;i<secCnt;i++){
		    			html+='<th scope="col">교통수단</th>';
		    			html+='<th scope="col">통행량</th>';
	    			}
	    			html+='</tr>';
	    			html+='</thead>';
	    			html+='<tbody>';
					
					if(e.trafDataList.length % secCnt == 0) {
						//기존 : 4의 배수인 경우..
		    			$.each(e.trafDataList,function(i,d){
	    					if(i%secCnt==0){
	    		    			html+='<tr>';
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
	    					}else if(i%secCnt==(secCnt-1)){
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
				    			html+='</tr>';
	    					}else{
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
	    					}
		    			});
					} else {
						//그외... : 모자란 부분에 빈 셀 삽입..
						var oldList = e.trafDataList;
						var newList = [];
						var newRow = [];
						//4개씩 4줄로 분할..
						for(var idx=0; idx<oldList.length; idx++) {
							var item = oldList[idx];
							newRow.push(item);
							if((idx + 1) % 4 == 0) {
								newList.push(newRow);
								newRow = [];
							}
						}
						if(newRow.length > 0) {
							var emptyCellNum = secCnt - newRow.length;
							for(var idx=0; idx<emptyCellNum; idx++) {
								newRow.push("emptyCell");
							}
							newList.push(newRow);
						}
						console.log("oldList");
						console.log(oldList);
						console.log("newList");
						console.log(newList);
						for(var idx=0; idx<newList.length; idx++) {
							var thisRow = newList[idx];
							html += '<tr>';
							for(var k=0; k<thisRow.length; k++) {
								var rowItem = thisRow[k];
								if(rowItem == "emptyCell") {
									html += "<td> - </td><td> - </td>";
								} else {
									html+='<td>'+nanToStr(rowItem.trafic_nm)+'</td>';
									html+='<td>'+nanToStr(rowItem.val)+'</td>';
								}
							}
							html += "</tr>";
						}
					}
					
	    			html+='</tbody>';
	    			$("#zoneTrafTable").html(html);
	    		}
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
	//documentShow(null, $(".slide_doc_area"));
	documentShow(null, $(".slide-doc-layout"));
}
*/
function show_zone_detail(no){
	var title = "Zone " + iniParam.cateName + ' 데이터';
	$("#docHeaderTitle").html(title);
	
	$("#zoneSlideSpinner").css("display", "block");
	documentShow(null, $("#zoneDetailModal"));
	var url=contextPath+'/getZoneDetailAjax.do';
	var params = {
		zone_no : no,
		id_nat_area : iniParam.natArea,
		id_city_area : iniParam.cityArea
	} 
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.yearList.length==0){
	    		}else{
	    			$("#zoneDetailTable").empty();
	    			
					//var title = iniParam.natName + ' ' + iniParam.cityName;
					var title = iniParam.cityName;
	    			if(nanToStr(e.zoneDetail)!='' && nanToStr(e.zoneDetail.zone_district)!='') {
	    				title+=' '+e.zoneDetail.zone_district;
	    			}
					if(nanToStr(e.zoneDetail)!='' && nanToStr(e.zoneDetail.zone_name)!='') {
	    				title+=' '+e.zoneDetail.zone_name;
					}
	    			title+=' '+'Zone '+ no;
	    			$("#docBodyTitle").html(title);

	    			var html='';
	    			html+='<thead>';
	    			html+='<tr>';
	    			html+='<th class="">항목</th>';
	    			$.each(e.yearList,function(i,d){
    					if(nanToStr(d)!=''){
			    			html+='<th scope="col">'+nanToStr(d.year)+'년</th>';
    					}else{
			    			html+='<th scope="col"></th>';
    					}
	    			});
	    			html+='</tr>';
	    			html+='</thead>';
	    			html+='<tbody>';
					
		    		$.each(e.metaDataList,function(i,d){
		    			html+='<tr>';
		    			html+='<td>'+d.unit_nm+'</td>';
						
						if(e.zoneDataList.length > 0) {
							var check = false;
				    		$.each(e.zoneDataList,function(j,f){
				    			if(d.id_meta_unit==f.id_meta_unit) {
									check = true;
				    				html+='<td>'+f.val+'</td>';
								}
			    			});
							if(!check) {
								html += '<td> - </td>';
							}
						} else {
							for(var idx=0; idx<e.yearList.length; idx++) {
								html += '<td> - </td>';
							}
						}
		    			html+='</tr>';
		    		});
	    			html+='</tbody>';

	    			$("#zoneDetailTable").html(html);
	    		}
				
	    		if(e.trafDataList.length==0){
	    			var html='';
	    			$("#zoneTrafTable").html('<tr><td>검색결과 없음</td></tr>');
	    		}else{
	    			var secCnt = 4;
	    			$("#zoneTrafTable").empty();
	    			var html='';
	    			html+='<thead>';
	    			html+='<tr>';
	    			for(var i=0;i<secCnt;i++){
		    			html+='<th scope="col">교통수단</th>';
		    			html+='<th scope="col">통행량</th>';
	    			}
	    			html+='</tr>';
	    			html+='</thead>';
	    			html+='<tbody>';
					
					if(e.trafDataList.length % secCnt == 0) {
						//기존 : 4의 배수인 경우..
		    			$.each(e.trafDataList,function(i,d){
	    					if(i%secCnt==0){
	    		    			html+='<tr>';
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
	    					}else if(i%secCnt==(secCnt-1)){
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
				    			html+='</tr>';
	    					}else{
				    			html+='<td>'+nanToStr(d.trafic_nm)+'</td>';
				    			html+='<td>'+nanToStr(d.val)+'</td>';
	    					}
		    			});
					} else {
						//그외... : 모자란 부분에 빈 셀 삽입..
						var oldList = e.trafDataList;
						var newList = [];
						var newRow = [];
						//4개씩 4줄로 분할..
						for(var idx=0; idx<oldList.length; idx++) {
							var item = oldList[idx];
							newRow.push(item);
							if((idx + 1) % 4 == 0) {
								newList.push(newRow);
								newRow = [];
							}
						}
						if(newRow.length > 0) {
							var emptyCellNum = secCnt - newRow.length;
							for(var idx=0; idx<emptyCellNum; idx++) {
								newRow.push("emptyCell");
							}
							newList.push(newRow);
						}
						for(var idx=0; idx<newList.length; idx++) {
							var thisRow = newList[idx];
							html += '<tr>';
							for(var k=0; k<thisRow.length; k++) {
								var rowItem = thisRow[k];
								if(rowItem == "emptyCell") {
									html += "<td> - </td><td> - </td>";
								} else {
									html+='<td>'+nanToStr(rowItem.trafic_nm)+'</td>';
									html+='<td>'+nanToStr(rowItem.val)+'</td>';
								}
							}
							html += "</tr>";
						}
					}
					
	    			html+='</tbody>';
	    			$("#zoneTrafTable").html(html);
	    		}
				$("#zoneSlideSpinner").css("display", "none");
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
	
}
