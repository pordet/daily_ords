import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableCell {
    private int id;
    private int parent_id;
    private int depth;
    private int ord;
    private int rolIndex;
    private int cellIndex;
    private int colspan=1;
    private int rowspan=1;
    private String val ="";

    public TableCell(int id, int parent_id, int depth, int ord) {
        this.id = id;
        this.parent_id = parent_id;
        this.depth = depth;
        this.ord = ord;
    }

    public TableCell(int rownum, int colnum,int rowspan, int colspan,String val) {
        this.rolIndex = rolIndex;
        this.cellIndex = cellIndex;
        this.colspan = colspan;
        this.rowspan = rowspan;
        this.val = val;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return parent_id;
    }

    public int getDepth() {
        return depth;
    }

    public int getOrd() {
        return ord;
    }

    public static void main(String[] args) {
        int max_depth = 1;
        List<TableCell> headCellList = new ArrayList<>();
        headCellList.add(new TableCell(1, 0, 1, 1));
        headCellList.add(new TableCell(2, 0, 1, 2));
        headCellList.add(new TableCell(3, 0, 1, 3));
        headCellList.add(new TableCell(4, 0, 1, 4));
        headCellList.add(new TableCell(5, 0, 1, 5));
        headCellList.add(new TableCell(6, 0, 1, 6));
        
        List<TableCell> cellList = new ArrayList<>();
        
        cellList.add(new TableCell(1, 1, 2, 1, "a"));
        cellList.add(new TableCell(4, 1, 2, 1));
        cellList.add(new TableCell(5, 1, 2, 2));
        cellList.add(new TableCell(6, 4, 3, 1));
        cellList.add(new TableCell(7, 4, 3, 2));
        cellList.add(new TableCell(8, 4, 3, 3));
        cellList.add(new TableCell(9, 6, 4, 1));
//        cellList.add(new TableCell(10, 6, 4, 2));
//        cellList.add(new TableCell(11, 7, 4, 3));
//        cellList.add(new TableCell(12, 7, 4, 4));

        Map<Integer, List<TableCell>> childMap = new HashMap<>();
        List<TableCell> aloneList = new ArrayList<>();

        for (TableCell cell : cellList) {
            if (!childMap.containsKey(cell.getParentId())) {
                childMap.put(cell.getParentId(), new ArrayList<>());
            }
            childMap.get(cell.getParentId()).add(cell);
        }

        for (TableCell cell : cellList) {
            if (cell.getDepth() == 1) {
                if (hasChildren(cell, childMap, max_depth)) {
                    List<TableCell> children = findChildrenRecursive(cell.getId(), childMap, 1, max_depth, aloneList);
                    aloneList.addAll(children);
                }else {
                	aloneList.add(cell);
                }
            }
        }

        // 결과 출력
        System.out.println("자식을 가지지 않는 셀:");
        for (TableCell cell : aloneList) {
            System.out.println("ID: " + cell.getId() + ", Parent_ID: " + cell.getParentId() + ", Depth: " + cell.getDepth());
        }
    }

    private static List<TableCell> findChildrenRecursive(int parentId, Map<Integer, List<TableCell>> childMap, int startDepth, int maxDepth, List<TableCell> aloneList) {
        List<TableCell> children = new ArrayList<>();
        if (childMap.containsKey(parentId)) {
            for (TableCell cell : childMap.get(parentId)) {
            	if(hasChildren(cell,childMap,maxDepth)) {
                    if (cell.getDepth() >= startDepth && cell.getDepth() <= maxDepth) {
//                        children.add(cell);
                        if (cell.getDepth() < maxDepth) {
                            List<TableCell> childChildren = findChildrenRecursive(cell.getId(), childMap, cell.getDepth(), maxDepth, aloneList);
                            children.addAll(childChildren);
                        }
                    }
            	}
            }
        }
        return children;
    }

    private static boolean hasChildren(TableCell cell, Map<Integer, List<TableCell>> childMap, int maxDepth) {
        if (childMap.containsKey(cell.getId())) {
            for (TableCell child : childMap.get(cell.getId())) {
                if (child.getDepth() <= maxDepth) {
                    return true;
                }
            }
        }
        return false;
    }
}
