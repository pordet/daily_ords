package daily.common.util;

import java.util.ArrayList;
import java.util.List;

import daily.common.vo.CustomMap;

public class ChartUtil {

	public static List<CustomMap> getBarSeriesData(List<CustomMap> list,String legendNm,List<String> legend,String axisNm,List<String> yAxis){
		// ret 구조 : key - legend , value = List<int>
		List<CustomMap> ret = new ArrayList<CustomMap>();
		for(String lkey:legend) {
			CustomMap val = new CustomMap();
			List<Double> valList = new ArrayList<Double>();
			val.put("name",lkey);
			for(String ykey:yAxis) {
				double inVal =0;
				for(CustomMap map:list) {
					String inLegend = map.getString(legendNm,"");
					String inYkey = map.getString(axisNm,"");
					if(inLegend.equals(lkey) && inYkey.equals(ykey)) {
						double cnt = map.getDouble("cnt");
						inVal+=cnt;
					}
				}
				valList.add(inVal);
			}
			val.put("data",valList);
			ret.add(val);
		}
		return ret;
	}
}
