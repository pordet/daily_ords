function drawTable(id_meta_m,col_ver,tbl_key){
	var params = {id_meta_m:id_meta_m};
	var insert_yn = $("#insert_yn").val();
	var url = contextPath+"/adm/meta/getIdentityMetaMAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    	}else{
	    		var html ='';
	    		var m = e.metaM;
	    		html+='<tr><th><div class="th-split-container"><div id="h_meta_m_'+m.id_meta_m+'_'+col_ver+'" class="th-left-content">' +nanToStr(m.meta_m_nm)+'</div><button class="th-right-content" alt="삭제" onclick="eraseMetaM(this);"></buttion></div></th></tr>'; 
	    		$.each(e.list,function (i,d){
		    		html+='<tr><td><div id="d_meta_'+d.id_meta+'_'+col_ver+'">' +nanToStr(d.meta_nm)+'</div></td></tr>'; 
	    		});
	    		$("#t_id_meta_m_"+tbl_key).html(html);
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
	
}
//srcParentId 테이블의 오른쪽에서 trgParentId 테이블의 왼쪽에 붙는 화살표 그리기
function drawArrowFromRightToLeft(srcTable, trgTable) {
    // 시작 테이블의 위치와 크기
    var parentOffset = $('#canvas-content').offset();
    
    var srcTableOffset = srcTable.offset();
    var srcTableWidth = srcTable.outerWidth();
    var srcTableHeight = srcTable.outerHeight();

    // 끝 테이블의 위치와 크기
    var trgTableOffset = trgTable.offset();
    var trgTableWidth = trgTable.outerWidth();
    var trgTableHeight = trgTable.outerHeight();

    // SVG 요소의 너비와 높이를 계산합니다.
    var svgWidth = Math.abs(srcTableOffset.left - (trgTableOffset.left + trgTableWidth));
    var svgHeight = Math.abs(srcTableOffset.top - (trgTableOffset.top));

	var svgTop = 0;
	var lineArea ={};
	if (srcTableOffset.top > trgTableOffset.top) {
	    svgTop = srcTableOffset.top - parentOffset.top;
	    lineArea.x1 = svgWidth;
	    lineArea.y1 = 0;
	    lineArea.x2 = 0;
	    lineArea.y2 = svgHeight / 2;
	} else {
	    svgTop = trgTableOffset.top - parentOffset.top;
	    lineArea.x1 = svgWidth;
	    lineArea.x2 = 0;
	    lineArea.y1 = svgHeight / 2;
	    lineArea.y2 = 0;
	}
	if(svgHeight<14){
		svgHeight = 14;
	    lineArea.y1 = svgHeight / 2;
	    lineArea.y2 = svgHeight / 2;
	}
	var svgLeft = trgTableOffset.left - parentOffset.left + trgTableWidth;
	
    // 대각선의 길이와 각도를 계산합니다.
    var diagonalLength = Math.sqrt(svgWidth * svgWidth + svgHeight * svgHeight);
    var angle = Math.atan2(svgHeight, svgWidth) + Math.PI;

    // 화살표의 끝을 조정하기 위한 값입니다.
    var arrowheadSize = 10;

	var rel_id = calRelId(srcTable,trgTable);
    // SVG 요소를 만듭니다.
    var arrow = $("<svg id='svg_"+rel_id+"' width='" + svgWidth + "' height='" + svgHeight + "' style='position:absolute; left:" + svgLeft + "px; top:" + svgTop + "px;'>"+
                    "<defs>"+
                        "<marker id='arrowhead' markerWidth='10' markerHeight='7' refX='10' refY='3.5' orient='auto'>"+
                            "<polygon points='10 0, 0 3.5, 10 7' fill='black' transform='rotate(180, 5, 3.5)'/>"+
                        "</marker>"+
                    "</defs>"+
                    "<line id='ln_"+rel_id+"' x1='" + lineArea.x1 + "' y1='" + lineArea.y1 + "' x2='" + lineArea.x2 + "' y2='" + lineArea.y2 + "' stroke='black' stroke-width='2' marker-end='url(#arrowhead)' onclick='lineClicked(this)' onmouseover='lineOvered(this)' onmouseout='lineOutedd(this)' />"+
                 "</svg>");
    relationSave(srcTable,trgTable,rel_id,arrow);
}
// srcParentId 테이블의 왼쪽에서 trgParentId 테이블의 오른쪽에 붙는 화살표 그리기
function drawArrowFromLeftToRight(srcTable, trgTable) {
    // SVG를 포함하는 부모 요소의 위치를 가져옵니다.
    var parentOffset = $('#canvas-content').offset();

    // srcTable과 trgTable의 위치를 가져옵니다.
    var srcTableOffset = srcTable.offset();
    var trgTableOffset = trgTable.offset();
    var srcTableWidth = srcTable.outerWidth();
    var srcTableHeight = srcTable.outerHeight();
    var trgTableWidth = trgTable.outerWidth();
    var trgTableHeight = trgTable.outerHeight();

    // SVG 요소의 너비와 높이를 계산합니다.
    var svgWidth = Math.abs(trgTableOffset.left - (srcTableOffset.left + srcTableWidth));
    var svgHeight = Math.abs(trgTableOffset.top - (srcTableOffset.top));

	var svgTop = 0;
	var lineArea ={};
	// 소스가 아래에 있는 경우
	if(srcTableOffset.top>trgTableOffset.top){
		svgTop = trgTableOffset.top - parentOffset.top+(trgTableHeight/2);
	    lineArea.x1 = 0;
	    lineArea.x2 = svgWidth-20;
	    lineArea.y1 = srcTableHeight/2;
	    lineArea.y2 = 20;
	}else if(srcTableOffset.top==trgTableOffset.top){
		svgTop = srcTableOffset.top - parentOffset.top+(srcTableHeight/2);
	// 소스가 위에 있는 경우
	}else{
		svgTop = srcTableOffset.top - parentOffset.top+(srcTableHeight/2);
	    lineArea.x1 = 0;
	    lineArea.y1 = 0;
	    lineArea.x2 = svgWidth-20;
	    lineArea.y2 = svgHeight/2;
	}
	if(svgHeight<14){
		svgHeight = 14;
	    lineArea.y1 = svgHeight / 2;
	    lineArea.y2 = svgHeight / 2;
	}
	var svgLeft = srcTableOffset.left - parentOffset.left + srcTableWidth;
//     var svgHeight = Math.abs(trgTableOffset.top - (srcTableOffset.top + srcTableHeight));

 // 대각선의 길이와 각도를 계산합니다.
    var diagonalLength = Math.sqrt(svgWidth * svgWidth + svgHeight * svgHeight);
    var angle = Math.atan2(svgHeight, svgWidth);

    // 화살표의 끝을 조정하기 위한 값입니다.
    var arrowheadSize = 10;

    // SVG 요소를 만듭니다.
//     var arrow = $("<svg width='" + svgWidth + "' height='" + svgHeight + "' style='position:absolute; left:" + (srcTableOffset.left - parentOffset.left + srcTableWidth) + "px; top:" + (srcTableOffset.top - parentOffset.top + srcTableHeight / 2) + "px;'><line x1='0' y1='0' x2='" + svgWidth + "' y2='" + svgHeight + "' stroke='black' stroke-width='2' marker-end='url(#arrowhead)'/></svg>");
//     var defs = $("<defs><marker id='arrowhead' markerWidth='10' markerHeight='7' refX='0' refY='3.5' orient='auto'><polygon points='0 0, 10 3.5, 0 7' fill='black'/></marker></defs>");
//     $('#canvas-content').append(arrow);
//     arrow.append(defs);
	var rel_id = calRelId(srcTable,trgTable);
    // srcParentId의 하위에서 td_sel 클래스를 가진 모든 태그의 ID 정보 가져오기
    
    var arrow = $("<svg id='svg_"+rel_id+"' width='" + svgWidth + "' height='" + svgHeight + "' style='position:absolute; left:" + svgLeft + "px; top:" + svgTop + "px;'>"+
            "<defs>"+
                "<marker id='arrowhead' markerWidth='10' markerHeight='7' refX='0' refY='3.5' orient='auto'>"+
                    "<polygon points='0 0, 10 3.5, 0 7' fill='black'/>"+
                "</marker>"+
            "</defs>"+
            "<line id='ln_"+rel_id+"' x1='" + lineArea.x1 + "' y1='" + lineArea.y1 + "' x2='" + lineArea.x2 + "' y2='" + lineArea.y2 + "' stroke='black' stroke-width='2' marker-end='url(#arrowhead)' onclick='lineClicked(this)' onmouseover='lineOvered(this)' onmouseout='lineOutedd(this)' />"+
         "</svg>");
    relationSave(srcTable,trgTable,rel_id,arrow);
}

