package kr.co.daily.common.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import daily.common.vo.CommandMap;
import kr.co.daily.common.GlobalConst;
import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.NumbersAnswerProducer;

@Controller
public class CaptchaController {
    @RequestMapping("/crsLogin/captcha.do")
    public void crsCaptha(HttpServletResponse response, HttpSession session) {
         Captcha captcha = new Captcha.Builder(200, 60)
                 .addText() // default: 5개의 숫자+문자
                 .addNoise().addNoise()
                    .addNoise().addNoise().addNoise().addNoise() // 시야 방해 라인 3개
                    .addBackground() // 기본 하얀색 배경
                    .build();
      
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);
            response.setContentType("image/png");
            CaptchaServletUtil.writeImage(response, captcha.getImage()); // 이미지 그리기
            session.setAttribute("captcha", captcha.getAnswer()); // 값 저장
    }
    @RequestMapping(value="/crsLogin/isRightAjax.do")
    public String isRight(CommandMap map, HttpSession session,Model model) {
        String captcha = map.getString("captcha","");
        String answer = (String)session.getAttribute("captcha");
        int ret = GlobalConst.ACT_SUCCESS;
        //(TODO)캡차 부분 제외
        if(!captcha.equals(answer)) {
        	ret = GlobalConst.NOT_CAPTCHA_MATCH;
        }
		model.addAttribute("ret",ret);
		return "jsonView";
    }
    @RequestMapping("/captcha/index.do")
    public void index(HttpServletResponse response, HttpSession session) {
         Captcha captcha = new Captcha.Builder(200, 60)
                    .addText(new NumbersAnswerProducer(6)) // default: 5개의 숫자+문자
                    .addNoise().addNoise()
                    .addNoise().addNoise() // 시야 방해 라인 3개
                    .addBackground(new GradiatedBackgroundProducer()) // 기본 하얀색 배경
//                    .gimp(new DropShadowGimpyRenderer())
                    .build();
      
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);
            response.setContentType("image/png");
            CaptchaServletUtil.writeImage(response, captcha.getImage()); // 이미지 그리기
            session.setAttribute("captcha", captcha.getAnswer()); // 값 저장
 
 
    }
}