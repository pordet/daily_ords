<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<spring:message code="page.login.crs.title" var="title" scope="request" />
<front:FrontHead title="로그인"/>
<body class="login-page">
    <div class="login-top-admin">
        <div class="login-contents">
            <h1 class="login-title">
<%--                <img src="<c:url value='/resource/img/logo-login.png'/>" alt="NPPA CRS"/> --%>
               <div class="login-title-txt">
                    
					<strong><a href="<c:url value='/'/>"><i class="fas fa-home"></i></a>&nbsp;&nbsp;글로벌 교통 DB</strong>
                </div>
<!--                 <div class="login-tit-txt"><span class="login-admin"></span></div> -->
            </h1>
            <div class="login-area">
                <div class="login-form">
            	<form id="frm" method="post" action="<c:url value='/loginAction.do'/>">
            	    <input type="hidden" name="user_type" value="${GlobalConst.USER_TYPE_APPLICANT}">
                    <ul>
                        <li>
                            <label for=""><span class="icon-id"></span><spring:message code="login.crs.frm1.col1"/></label>
                            <input type="text" name="id" />
                        </li>
                        <li>
                            <label for=""><span class="icon-pass"></span><spring:message code="login.crs.frm1.col2"/></label>
                            <input type="password" name="pwd"/>
                        </li>
                    </ul>
            	</form>
                </div>
                
            </div>
            <button type="button" id="loginBtn" class="btn login"><spring:message code="login.crs.sub1.btn2"/></button>
        </div>
    </div>
    <div class="login-copyright">&copy; 2020 THE KOREA TRANSPORT INSTITUTE All Rights Reserved. Privacy and Terms</div>

    <!-- modal alert -->
    <div class="modal fade mh-modal" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body red-type" id="alertMsg">
                    <p id="alertMsg">기존에 사용중인 ID입니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
                </div>
            </div>
        </div>
    </div>
    
</body>
    <!-- jQuery JS -->
    <script src="<c:url value='/resource/plugins/jquery/jquery-1.12.4.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js'/>"></script>
    
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap/js/bootstrap.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker - locale ko js -->
    <script src="<c:url value='/resource/plugins/moment/locale/ko.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <!-- Data Tables JS -->
    <script src="<c:url value='/resource/plugins/datatables/js/jquery.dataTables.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/full_numbers_no_ellipses.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/dataTables.responsive.min.js'/>"></script>
    <!-- nppa JS -->
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
<script>
	var ret = "${param.ret}";
	$(function(){
		if(ret == "${GlobalConst.NOT_FOUND_USER}"){
			alertOpen("<spring:message code='common.alert.msg02'/>");
		}else if(ret == "${GlobalConst.NOT_PW_MATCH}"){
			alertOpen("<spring:message code='common.alert.msg03'/>");
		}else if(ret == "${GlobalConst.NOT_CAPTCHA_MATCH}"){
			alertOpen("<spring:message code='common.alert.msg04'/>");
		}
		$("#loginBtn").click(function(){
			$("#frm").submit();
		});
	});
    function changeLang(){
	   var lang = $("#multi_lang_sel option:selected").val();
	   var url = contextPath+"/guest/changeLanguage.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : {lang:lang},
		    dataType: "json" ,
		    success : function(e){
		    	location.reload();
		    },
		    error : function(e){
		    	alertOpen('실패하였습니다.');
		    }
		});
    }
	function alertOpen(msg){
	  	$("#alertMsg").html(msg);
	  	$('#alertModal').modal('show');
	}
</script>
</html>
