<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="data" tagdir="/WEB-INF/tags/data" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<data:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
<c:choose>
	<c:when test="${!is_guest}">
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_draw.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_register.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_input.js'/>"></script>
	</c:when>
	<c:otherwise>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/guest_data_input.js'/>"></script>
	</c:otherwise>
</c:choose>
</data:DataHead>
<data:DataAccordionBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${paramMap.id_master}" sel_ord_str="${master.ord_str}" type="${(empty paramMap.type?'3':paramMap.type)}" title="${master.title}" keyword="${paramMap.search_keyword}">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>통합검색</span>
			</div>
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="get" onsubmit="return false;">
					<div class="search-group-area">
						<div class="search-input-group">
							<div class="search-input-front">
								<input class="integratedSearch" type="text" name="search_keyword" value="${(not empty paramMap.search_keyword ?paramMap.search_keyword:'')}">
								<button class="integratedSearchBtn btn-primary" type="button" title="검색">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</div>
						</div>
					</div>
				
					<div class="search-state-area" id="result_header">
						<span>
							<c:choose>
								<c:when test="${not empty paramMap.search_keyword}">
									<span id="searchKeywordSpan">&quot;${paramMap.search_keyword}&quot;</span>에 대해 총 <strong class="result totalCntArea" id="mainTotalCnt">${total_cnt}</strong>건이 검색되었습니다.
								</c:when>
								<c:otherwise>
									전체 총 <strong class="totalCntArea" id="mainTotalCnt">${total_cnt}</strong>건이 검색되었습니다.
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				
					<div>
						<div class="tab-btn-area with-option tabBtnArea">
							<ul>
								<c:forEach var="map" items="${kindList}">
									<li ${(paramMap.id_kind eq map.id_kind ?' class="active"':'')}>
										<a class="tabBtn" href="#tab_layer_${map.id_kind}">
											<span id="main_${map.id_kind}_cnt">${map.kind_nm} (${map.kind_cnt})</span>
										</a>
									</li>
								</c:forEach>
							</ul>
						</div>
						
						<div class="search-option-area">
						</div>
					
						<div class="tab-content-area tabContentArea">
							<c:forEach var="map" items="${kindList}">
								<div id="tab_layer_${map.id_kind}" class="tab-content tabContent ${(paramMap.id_kind eq map.id_kind ?'active':'')}">
									<div class="list-wrap"  style="display:block;">
										<div class="list-title">
											<span class="">${map.kind_nm}(<fmt:formatNumber value="${map.kind_cnt}" pattern="#,###" />건)</span>
											<div class="list-btn-area">
												<button type="button" class="btn-blank" onclick="toBoard(${map.id_kind},'${paramMap.search_keyword}')">더보기 <i class="fa fa-angle-right"></i></button>
											</div>
										</div>
										<div class="list-line">
											<c:choose>
												<c:when test="${map.kind_cnt gt 0}">
													<ul id="ul_a_${map.id_kind}">
														<c:forEach var="item" items="${list}">
															<c:if test="${map.id_kind eq item.id_kind}">
																<li>
																	<div class="tag-area">
																		<div class="label-set brown">
																			<span>${item.parent_board_nm}</span>
																		</div>
																		<div class="label-set red">
																			<span>${item.board_nm}</span>
																		</div>
																	</div>
																	<dl class="list-content-area">
																		<dt> 
																			<c:forEach var="file" items="${item.filelist}">
																				<a class="file-icon" href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}">
																					<span class="${file.file_sh_ext}">${file.file_ext}</span>
																				</a>
																			</c:forEach>
																			<a href="javascript:void(0);" onclick="javascript:toDetailView('${item.id_kind}','${item.id_board_master}','${item.id_board}','${item.ord_str}');">
																				<span class="title">${item.title}</span>
																			</a>
																		</dt>
																		<dd>${item.comment}</dd>
																	</dl>
																	<div class="info-data">
																		<div>
																			<span>수정일 </span>
																			<span>${item.reg_date}</span>
																		</div>
																		<div>
																			<span>조회수 </span>
																			<span>${item.view_cnt}</span>
																		</div>
																	</div>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</c:when>
												<c:otherwise>
													<ul id="ul_a_${map.id_kind}">
														<li>
															<div class="no-list">
																<span class="txt">검색결과가 없습니다.</span>
															</div>
														</li>
													</ul>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</c:forEach>
							
						</div>
					
						
					</div>
				</form>
			
			</div>
		</div>
	</div>
</data:DataAccordionBody>
<script>
document.querySelector(".integratedSearch").addEventListener("keyup", function(event){
	var inputWord = event.target.value;
	var inputElement = document.querySelector(".integratedSearch");
	var reg = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
	if(reg.test(inputWord)) {
		inputWord = inputWord.replace(reg,"");
	}
	inputElement.value = inputWord;
	if(window.event.keyCode == 13){searchAction(inputElement)};
})
document.querySelector(".integratedSearchBtn").addEventListener("click", function(event) {
	searchAction(document.querySelector(".integratedSearch"));
})

function enterKey(){
	if(window.event.keyCode == 13){
		search();
	}
}
$(".tabBtn").on('click', function(e){
	console.log("click")
	console.log(e)
	e.preventDefault();
	var targetId = $(this).attr('href');
	var target = $("" + targetId);
	$(this).closest('li').addClass('active').siblings('li').removeClass('active');
	
	if(target.length > 0){
		$(""+targetId).closest('.tabContentArea').find('.tabContent').removeClass('active');
		$(""+targetId).addClass('active');
	}
});

function searchAction(inputElement) {
	var searchWord = inputElement.value;
	console.log(searchWord.length);
	searchWord = searchWord.trim();
	searchWord = searchWord.slice(0,15);
	inputElement.value = searchWord;
	
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "Post");  //Post 방식
    form.setAttribute("action", "<c:url value='/search.do?search_keyword="+searchWord+"'/>"); //요청 보낼 주소
    document.body.appendChild(form);
    form.submit();
}
</script>
</html>