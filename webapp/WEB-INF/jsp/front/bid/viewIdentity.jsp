<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.UserAuthUtil" %>

<c:url var="act" value="/front/bid/identityList.do"/>
<c:url var="g_act" value='/front/guest/identityList.do'/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
		<c:set var="action" value="${act}"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
		<c:set var="action" value="${g_act}"/>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<c:set var="title" value="${map.meta_m_nm} 등록"/>
<front:FrontDynamicHead title="${title}">
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
					<span id="curr_title">${title}</span>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
					<input type="hidden" id="reg_yn" name="reg_yn"/>
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
					<input type="hidden" id="search_typ" name="search_typ" value="${param.search_typ}"/>
				  	<input type="hidden" name="search_srt_dt" value="${param.search_srt_dt}">
				  	<input type="hidden" name="search_end_dt" value="${param.search_end_dt}">
				
					<input type="hidden" name="id_identity_m" id="id_identity_m" value="${param.id_identity_m}" />
					<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}" />
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}"/>
					<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
					<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
					<input type="hidden" name="page" id="page" value="${(empty param.page? 1 :param.page)}"/>
                 <c:forEach var="row" items="${colList}" varStatus="i">
					<c:if test="${row.detail_param_yn eq 'Y'}">
					<input type="hidden" name="key_${row.meta_cd}" id="key_${row.meta_cd}" value="${row.val}">
					</c:if>
				</c:forEach>
					<c:set var="curr_low_num" value="1"/>
					<c:set var="curr_col_num" value="1"/>
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="15%">
								<col width="30%">
								<col width="15%;">
								<col width="35%">
							</colgroup>
							<tbody>
                                <c:forEach var="row" items="${colList}" varStatus="i">
									<c:if test="${row.list_show_yn eq 'Y'}">
		                                <c:choose>
											<c:when test="${curr_col_num eq 1}">
				                                <c:choose>
												<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
													<tr>
														<th>${row.meta_nm}
														</th>
														<td colspan="3">
															<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
														</td> 
													</tr>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													<c:set var="curr_col_num" value="1"/>
												</c:when>
												<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 3}">
													<tr>
														<th>${row.meta_nm}
	<%-- 													[\\][${curr_low_num}] --%>
														</th>
														<td colspan="3">
															<textarea data-meta-id="${row.id_meta}" style="width:100%" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" disabled>${row.val}</textarea>
														</td> 
													</tr>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													<c:set var="curr_col_num" value="1"/>
												</c:when>
												<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
													<tr>
														<th>${row.meta_nm}
	<%-- 													[\\][${curr_low_num}] --%>
														</th>
														<td>
															<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
														</td> 
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													<c:set var="curr_col_num" value="1"/>
												</c:when>
												<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
													<tr>
														<th>${row.meta_nm}
	<%-- 													[\\][${curr_low_num}] --%>
														</th>
														<td>
															<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
														</td> 
													<c:set var="curr_col_num" value="2"/>
												</c:when>
												<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
													<c:if test="${row.meta_depth eq '1'}">
														<c:choose>
														<c:when test="curr_col_num==1">
													</tr>
														<tr>
															<th colspan="4">${row.meta_nm}</th>
														</tr>
														</c:when>
														<c:otherwise>
														<tr>
															<th colspan="4" class="depth2">${row.meta_nm}</th>
														</tr>
														</c:otherwise>
														</c:choose>
														<c:forEach var="child" items="${row.child_list}"  varStatus="i">
															<c:choose>
											                    <c:when test="${i.count % 2 == 1}">
											                        <tr>
											                        	<th>${child.meta_nm}</th>
											                        	<td>
																			<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요."${(child.auto_seq_yn eq 'Y'?' disabled':'')}>
																			<c:if test="${not empty meta_row and not empty row.val}">
																				<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																				<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																			</c:if>
											                        	</td>
											                    </c:when>
											                    <c:otherwise>
											                        	<th>${child.meta_nm}</th>
											                        	<td>
																			<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요."${(child.auto_seq_yn eq 'Y'?' disabled':'')}>
																			<c:if test="${not empty meta_row and not empty row.val}">
																				<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																				<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																			</c:if>
											                        	</td>
											                        </tr>
											                    </c:otherwise>
											                </c:choose>
														</c:forEach>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													<c:set var="curr_col_num" value="1"/>
													</c:if>
												</c:when>
												<c:when test="${row.typ eq GlobalConst.DT_CODE_GROUP || row.typ eq GlobalConst.DT_TAL_CODE_GROUP}">
													<tr>
														<th>${row.meta_nm}
	<%-- 													[\\][${curr_low_num}] --%>
														</th>
														<td>
															<c:set var="cdList" value="${cdMap[row.id_meta]}"/>
															<select  id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}">
																<option value="" ${(empty row.val ?'selected':'')}>선택</option>
															<c:forEach var="cd" items="${cdList}">
																<option value="${cd.code}" ${(row.val eq cd.code ?'selected':'')}>${cd.code_nm}</option>
															</c:forEach>
															</select>
															<c:if test="${not empty meta_row and not empty row.val}">
																<c:if test="${row.typ eq GlobalConst.DT_TAL_CODE_GROUP}">
																	<input type="hidden" data-meta-id="${row.refer_id_meta_m}" data-meta-val="" id="r_id_meta_m_${row.refer_id_meta_m}" name="r_id_meta_m_${row.refer_id_meta_m}" value="${row.refer_id_meta_m}">
																	<input type="hidden" data-meta-id="${row.refer_id_meta}" data-meta-val="" id="r_id_meta_${row.refer_id_meta}" name="r_id_meta_${row.refer_id_meta}" value="${row.refer_id_meta}">
																</c:if>
																<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
																<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
															</c:if>
														</td> 
													<c:set var="curr_col_num" value="2"/>
												</c:when>
												<c:otherwise>
													<tr>
														<th>${row.meta_nm}
	<%-- 													[--][${curr_low_num}] --%>
														</th>
														<td>
															<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
														</td> 
														<c:set var="curr_col_num" value="2"/>
												</c:otherwise>
			                                </c:choose>
											</c:when>
											<c:otherwise>
		<%-- 								    [curr_col_num는 2:[${curr_low_num}]] --%>
				                                <c:choose>
													<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
															<th>${row.meta_nm}
		<%-- 													[//][${curr_low_num}] --%>
															</th>
															<td colspan="3">
																<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
															</td> 
														</tr>
														<c:set var="curr_low_num" value="${curr_low_num+1}"/>
														<c:set var="curr_col_num" value="1"/>
													</c:when>
													<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 3}">
														</tr>
														<tr>
															<th>${row.meta_nm}
		<%-- 													[//][${curr_low_num}] --%>
															</th>
															<td colspan="3">
																<c:choose>
										                        	<c:when test="${empty row.link_yn || row.link_yn ne 'Y'}">
																<textarea data-meta-id="${row.id_meta}" style="width:100%" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" disabled>${row.val}</textarea>
										                        	</c:when>
										                        	<c:otherwise>
										                        	<a href="${row.val}" target="blank">${row.val}</a>
										                        	</c:otherwise>
										                        </c:choose>
															
															</td> 
														</tr>
														<c:set var="curr_low_num" value="${curr_low_num+1}"/>
														<c:set var="curr_col_num" value="1"/>
													</c:when>
													<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
														<c:if test="${row.meta_depth eq '1'}">
															<c:choose>
																<c:when test="curr_col_num==1">
														</tr>
															<tr>
																<th colspan="${fn:length(row.child_list)}">${row.meta_nm}</th>
															</tr>
																</c:when>
																<c:otherwise>
															<tr>
																<th colspan="${fn:length(row.child_list)}" class="depth${row.meta_depth}">${row.meta_nm}</th>
															</tr>
																</c:otherwise>
															</c:choose>
															<c:forEach var="child" items="${row.child_list}"  varStatus="i">
																<c:choose>
												                    <c:when test="${i.count % 2 == 1}">
												                        <tr>
												                        	<th>${child.meta_nm}</th>
												                        	<td>
																				<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" disabled>
												                        	</td>
												                    </c:when>
												                    <c:otherwise>
												                        	<th>${child.meta_nm}</th>
												                        	<td>
																				<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" disabled>
												                        	</td>
												                        </tr>
												                    </c:otherwise>
												                </c:choose>
															</c:forEach>
														<c:set var="curr_low_num" value="${curr_low_num+1}"/>
														<c:set var="curr_col_num" value="1"/>
														</c:if>
													</c:when>
													<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
															<th>${row.meta_nm}
		<%-- 													[==][${curr_low_num}] --%>
															</th>
															<td>
																<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
															</td> 
														</tr>
													<c:set var="curr_col_num" value="1"/>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													</c:when>
													<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
															<th>${row.meta_nm}
		<%-- 													[==][${curr_low_num}] --%>
															</th>
															<td>
																<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
															</td> 
														</tr>
													<c:set var="curr_col_num" value="1"/>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													</c:when>
													<c:when test="${row.typ eq GlobalConst.DT_CODE_GROUP || row.typ eq GlobalConst.DT_TAL_CODE_GROUP}">
															<th>${row.meta_nm}
		<%-- 													[\\][${curr_low_num}] --%>
															</th>
															<td>
																<c:set var="cdList" value="${cdMap[row.id_meta]}"/>
																<select  id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" disabled>
																	<option value="" ${(empty row.val ?'selected':'')}>선택</option>
																<c:forEach var="cd" items="${cdList}">
																	<option value="${cd.code}" ${(row.val eq cd.code ?'selected':'')}>${cd.code_nm}</option>
																</c:forEach>
																</select>
															</td> 
														</tr>
														<c:set var="curr_low_num" value="${curr_low_num+1}"/>
														<c:set var="curr_col_num" value="1"/>
													</c:when>
													<c:otherwise>
															<th>${row.meta_nm}
		<%-- 													[==][${curr_low_num}] --%>
															</th>
															<td>
																<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled>
															</td> 
														</tr>
													<c:set var="curr_col_num" value="1"/>
													<c:set var="curr_low_num" value="${curr_low_num+1}"/>
													</c:otherwise>
				                                </c:choose>
		 									</c:otherwise>
		                                </c:choose>
	                                </c:if>
                                </c:forEach>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
						</div>
						<div class="board-bottom-group-rear">
							<c:if test="${!is_guest}">
								<button type="button" class="btn-primary" onclick="javascript:register();">
									입찰 등록
								</button>
							</c:if>
						</div>
					</div>
				</form>
			</div>
	<script>
	var colArr = [];
	<c:forEach var="row" items="${colList}" varStatus="i">
	    var item = {
	        typ: "${row.typ}",
	        id_meta: "${row.id_meta}"
	    };
	    colArr.push(item);
	</c:forEach>
	function doList(page){
		$("#page").val(page);
		var frm =document.frm;
		frm.action="${action}";
		frm.submit();
	}
	function register(){
		$("#page").val(page);
		var frm =document.frm;
		frm.action=contextPath+"/front/meta/regMetaData.do";
		frm.submit();
	}
	</script>

</front:FrontDynamicBody>
</html>