/**
 *  입력항목 체크
 *  gubun : id/pw 찾기 구분코드 [id/pw]
 */
function fn_validation(gubun){
	 
	$(".input-warning").hide();
	 
	//이름
	var mberNm = "";
	if(gubun == "id"){
		mberNm = document.id_frm.user_nm;
	}else if(gubun == "pw"){
		mberNm = document.pwd_frm.user_nm;
	}
	
	if(fn_empty(mberNm.value)){
		//$("#mberNm_"+gubun).next(".input-util").find(".input-warning").show();
		console.log($("#mberNm_"+gubun).closest("tr").next());
		$("#mberNm_"+gubun).closest("tr").next().find(".input-util .input-warning").show();
		$("#mberNm_"+gubun).focus();
		return false;
	}
	
	if(gubun == "pw"){
		
		//사용자ID
		var mberId = document.pwd_frm.id_user;
		
		if(fn_empty(mberId.value)){
			//$("#mberId_pw").next(".input-util").find(".input-warning").show();
			$("#mberId_pw").closest("tr").next().find(".input-util .input-warning").show();
			$("#mberId_pw").focus();
			return false;
		}
	}
	
	//이메일 주소
	var email = "";
	if(gubun == "id"){
		email = document.id_frm.email;
	}else if(gubun == "pw"){
		email = document.pwd_frm.email;
	}
	if(fn_empty(email.value)){
		//$("#mberEmail_"+gubun).next(".input-util").find(".input-warning").show();
		$("#mberEmail_"+gubun).closest("tr").next().find(".input-util .input-warning").show();
		$("#mberEmail_"+gubun).focus();
		return false;
	
	//이메일 주소 형식 
	}else{
		var regExp = fn_regExp("email");
		if ( !regExp.test(email.value) ) {
			//$("#mberEmail_"+gubun).next(".input-util").find(".input-warning").show();
			$("#mberEmail_"+gubun).closest("tr").next().find(".input-util .input-warning").show();
			$("#mberEmail_"+gubun).focus();
			return false;
		}
	}
	
	return true;
}

/**
 * 사용자 정보 조회
 * gubun : id/pw 찾기 구분코드 [id/pw]
 */
function fn_search(gubun){
	
	if(fn_validation(gubun)){
		
		var data = $("#id_frm").serialize();
		if(gubun == "pw"){
			data = $("#pwd_frm").serialize();
		}else{
		}
		
		 //필수입력항목 미입력시 안내 메세지 숨김처리 
		$(".input-warning").hide();
	    
		$.ajax({
			type:"POST",
			url: contextPath+"/member/findInfoAjax.do",
			data : data,
			dataType : "json",
			success: function(e){
				
				$(".result-area").hide();
				
				if(e.ret==0){
					if(gubun == "id"){
						
						// 회원가입 정보 있을 경우 출력
						if(e.usr){
								$(".foundMberIdInfo").show();
								
								$(".foundMberId").empty();
								$(".foundMberId").text(e.usr.id_user);
								
								//아이디 찾기 후 비밀번호 찾기를 할 가능성이 높으므로 비밀번호찾기 입력폼에 자동 세팅
								document.pwd_frm.user_nm.value = document.id_frm.user_nm.value;
								document.pwd_frm.id_user.value = e.usr.id_user;
								document.pwd_frm.email.value = document.id_frm.email.value;
								// 회원가입 정보 없을 경우 출력	
						}else if(e.same){
							$(".doubleMberIdInfo").show();
						}else{
							
							$(".notFoundMberIdInfo").show();
							
							document.pwd_frm.user_nm.value = '';
							document.pwd_frm.id_user.value = '';
							document.pwd_frm.email.value = '';
						}
						
					}else{

						// 회원가입 정보 있을 경우 출력
						if(e.usr){
							$(".foundMberPwInfo2").show();;
						// 회원가입 정보 없을 경우 출력
						}else{
							$(".notFoundMberPwInfo").show();
						}
					}
				}else{
					alert("조회에 실패하였습니다.");
				}
			},
			error: function(xhr, status, error) {
				alert("조회에 실패하였습니다.");
				$(".result-area").hide();
			}	
		});
	}
}

/**
* 비밀번호 초기화
*/
function fn_resetMberPswrd(){
	
	$.ajax({
		type:"POST",
		url: "/uim/mis/updateMberPswrd.json",
		data : $("#pwd_frm").serialize(),
		dataType : "json",
		success: function(result){
			$(".result-area").hide();
			if(result.status){
				
				//비밀번호 초기화 성공			
				if(result.resetPswdResult){
					$(".foundMberPwInfo2").show();
					//alert("실 시스템 에서는 발송된 메일에서 초기화된 비밀번호 : "+result.resetPswd);
					
				//비밀번호 초기화 실패
				}else{
					alert("비밀번호 초기화 중 오류가 발생했습니다.");
					$(".result-area").hide();
				}
			}else{
				alert("비밀번호 초기화 중 오류가 발생했습니다.");
				$(".result-area").hide();
			}
		},
		error: function(xhr, status, error) {
			alert("비밀번호 초기화 중 오류가 발생했습니다.");
			$(".result-area").hide();
		}	
	});
}
