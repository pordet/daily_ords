package daily.common.security;
/**
 * loginAjax 처리를 위해 AuthenticationSuccessHandler를 사용자 정의한 핸들러
 * author : P.H.
 * date : 2018-04-24
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;

import daily.common.security.dao.SecuredObjectDAO;
import daily.common.security.service.SecuredObjectService;
import daily.common.security.util.CustomRequestMatcher;
import daily.common.service.SystemUserService;
import daily.common.util.StrUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.dao.MenuManageDAO;
import kr.co.daily.admin.dao.ResourceDAO;
import kr.co.daily.admin.dao.UserGroupDAO;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.domain.MenuVO;
import kr.co.daily.domain.User;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	private static Logger log = LoggerFactory.getLogger(LoginSuccessHandler.class);
	private RequestCache requestCache = new HttpSessionRequestCache();
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private String defaultSuccessUrl;
	private String targetUrlParameter;
	private boolean useReferer;
	@Autowired ResourceDAO rsrcDao;
	@Autowired UserGroupDAO userGroupDao;
	@Autowired MenuManageDAO menuDao;
	@Autowired private SystemUserService systemUserService;
	
	@Autowired SecuredObjectService securedService;
	
	@Autowired SecuredObjectDAO dao;
	
	private void useDefaultUrl(HttpServletRequest req, HttpServletResponse resp, Authentication auth) throws IOException {
		redirectStrategy.sendRedirect(req, resp, defaultSuccessUrl);
	}
	private void useRefererUrl(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String targetUrl = req.getHeader("REFERER");
		redirectStrategy.sendRedirect(req, resp, targetUrl);
	}
	private void useSessionUrl(HttpServletRequest req, HttpServletResponse resp, Authentication auth) throws IOException {
		SavedRequest savedRequest = requestCache.getRequest(req, resp);
		String targetUrl = savedRequest.getRedirectUrl();
		redirectStrategy.sendRedirect(req, resp, targetUrl);
	}
	/*
	private void useTargetUrl(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		SavedRequest savedRequest = requestCache.getRequest(req, resp);
		if(savedRequest !=null) {
			requestCache.removeRequest(req, resp);
		}
		String targetUrl = req.getParameter(targetUrlParameter);
		redirectStrategy.sendRedirect(req, resp, targetUrl);
	}
	*/
	/**
	 * 로그인 하기 전의 요청했던 URL을 알아낸다.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
//	private String getReturnUrl(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
//		SavedRequest savedRequest = requestCache.getRequest(request, response);
//		if (savedRequest == null) {
//			return request.getSession().getServletContext().getContextPath();
//		}
//		return savedRequest.getRedirectUrl();
//	}
	public String getDefaultSuccessUrl() {
		return defaultSuccessUrl;
	}
	public void setDefaultSuccessUrl(String defaultSuccessUrl) {
		this.defaultSuccessUrl = defaultSuccessUrl;
	}

	public String getTargetUrlParameter() {
		return targetUrlParameter;
	}
	public void setTargetUrlParameter(String targetUrlParameter) {
		this.targetUrlParameter = targetUrlParameter;
	}
	public boolean isUseReferer() {
		return useReferer;
	}
	public void setUseReferer(boolean useReferer) {
		this.useReferer = useReferer;
	}
	private void clearAuthenticationAttribute(HttpServletRequest req) {
		HttpSession session = req.getSession();
		if(session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
	/*
	private int decideRedirectStrategy(HttpServletRequest req, HttpServletResponse resp) {
		int result = 0;
		SavedRequest savedRequest = requestCache.getRequest(req, resp);
		if(!"".equals(targetUrlParameter)) {
			String targetUrl = req.getParameter(targetUrlParameter);
			if(StringUtils.hasText(targetUrl)) {
				result=1;
			}else {
				if(savedRequest !=null) {
					result = 2;
				}else {
					String refererUrl = req.getHeader("REFERER");
					if(useReferer && StringUtils.hasText(refererUrl)) {
						result =3;
					}else {
						result = 0;
					}
				}
			}
		}
		return result;
	}
	*/
	
//==============================================================================================//	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication auth)
			throws IOException, ServletException {
        User authUser = (User)auth.getPrincipal(); 
        HttpSession sess = req.getSession();
        sess.setAttribute("authUser", authUser);
        String sessionId = sess.getId();
        String loginedId = authUser.getUsername();
        
//        CustomMap userMap = authUser.getUserMap();
//        if(!"".equals(userMap.getString("begin_page",""))) {
//        	
//        }
        CommandMap loginInfo = new CommandMap();
        String userType = req.getParameter("user_type");
        loginInfo.put("userType", userType);
        loginInfo.put("loginedId", loginedId);
        loginInfo.put("loginedIp", req.getRemoteAddr());
        loginInfo.put("sessionId", sessionId);
    	LoginManager manager = LoginManager.getInstance();
        if(manager.isUsing(loginedId)) {
        	manager.removeSession(loginedId);
        	systemUserService.deleteLoginUserInfo(loginInfo);
        }else {
        	HttpSession session = req.getSession(true);
        	manager.setSession(session, loginedId);
        	systemUserService.deleteLoginUserInfo(loginInfo);
            systemUserService.insertLoginUserInfo(loginInfo);
        }

		String accept = req.getHeader("accept");
		
		String succ = GlobalConst.ACT_SUCCESS.toString();
		if( StrUtil.indexOf(accept, "html") > -1 ) { 
	        resp.setStatus(HttpServletResponse.SC_OK);
	        
	        this.clearAuthenticationAttribute(req);
	        
	        int intRedirectStrategy = this.decideRedirectStrategy(req, resp);
	        log.debug("LoginSuccessHandler redirectStrategy :"+intRedirectStrategy);
	        switch(intRedirectStrategy) {
	        case 1:
	        	useTargetUrl(req,resp,auth);
	        	break;
	        case 2:
	        	useSessionUrl(req,resp,auth);
	        	break;
	        case 3:
	        	useRefererUrl(req,resp,auth);
	        	break;
	        default:
	        	useDefaultUrl(req,resp,auth);	
	        }
//	        String successUrl = this.getReturnUrl(req, resp,auth);
////	        resp.sendRedirect(defaultSuccessUrl);		
//	        resp.sendRedirect(successUrl);		
		} else if( StrUtil.indexOf(accept, "xml") > -1 ) { 
			resp.setContentType("application/xml"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<ret>", succ , "</ret>" }); 
			PrintWriter out = resp.getWriter(); out.print(data); out.flush(); out.close(); 
		} else if( StrUtil.indexOf(accept, "json") > -1 ) { 
			resp.setContentType("application/json"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] {
					" { \"ret\" : ", succ, "} " }); 
			PrintWriter out = resp.getWriter(); 
			out.print(data); 
			out.flush(); 
			out.close();
		}
	}
	
	private void useRefererUrl(HttpServletRequest req, HttpServletResponse resp, Authentication auth) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * 이전 페이지 정보가 세션에 없을 경우가 디폴트인 경우
	 * @param req
	 * @param resp
	 * @return
	 */
	private int decideRedirectStrategy(HttpServletRequest req, HttpServletResponse resp) {
		int result = 0;
		SavedRequest savedRequest = requestCache.getRequest(req, resp);
		HttpSession session = req.getSession();
		String redirectUrl = (String) session.getAttribute("prevPage");
		log.info("성공 : redirectUrl");
		log.info(redirectUrl);
		//targetUrlParameter = redirectUrl;
		if(!"".equals(redirectUrl)) {
			//String targetUrl = req.getParameter(targetUrlParameter);
			String targetUrl = redirectUrl;
			if(StringUtils.hasText(targetUrl)) {
				result=1;
			}else {
				if(savedRequest !=null) {
					result = 2;
				}else {
					String refererUrl = req.getHeader("REFERER");
					if(useReferer && StringUtils.hasText(refererUrl)) {
						result =3;
					}else {
						result = 0;
					}
				}
			}
		}
		return result;
	}
	private void useTargetUrl(HttpServletRequest req, HttpServletResponse resp, Authentication auth) throws IOException {
		HttpSession sess = req.getSession();
		String redirectUrl = (String) sess.getAttribute("prevPage");
		String method =(String) sess.getAttribute("method");
		boolean hasParamMap = redirectUrl.contains("?");
		StringBuffer sb = new StringBuffer();
		if(method !=null && method.equalsIgnoreCase("POST")) {
			Map<String, Object> parameterMap = (Map) sess.getAttribute("postData");
			if(!hasParamMap) {
				sb.append("?");
			}
			if(!hasParamMap && parameterMap.size()>0) {
				hasParamMap=true;
				for(String param_nm:parameterMap.keySet()) {
					Object obj=parameterMap.get(param_nm);
					if(obj instanceof String) {
						String str = (String)obj;
						sb.append(StrUtil.stringToQueryString(param_nm,str));
					}else if(obj instanceof String[]) {
						String[] strArr = (String[])obj;
						sb.append(StrUtil.arrayToQueryString(param_nm, strArr));
					}
				}
			}
		}
		SavedRequest savedRequest = requestCache.getRequest(req, resp);
		if(savedRequest !=null) {
			requestCache.removeRequest(req, resp);
		}
        User authUser = (User)auth.getPrincipal(); 
        String loginedId = authUser.getUsername();
        CommandMap param = new CommandMap();
        param.put("login_id", loginedId);
		List<CustomMap> rsrcList = rsrcDao.selectAuthorizedResourceUrlByloginedId(param.getMap());
		boolean authorized = false;
		StringBuffer url = req.getRequestURL();
		// URI 가져오기
		String uri = req.getRequestURI();
		String contextPath = req.getContextPath(); // 컨텍스트 루트를 가져옴
		// URI를 URL에서 제거하여 나머지 부분 구하기
		String remainingPart = url.substring(0, url.indexOf(uri));
		if(redirectUrl!=null) {
			try {
				String uriToMatch = redirectUrl.substring(remainingPart.length());
		//
				// 컨텍스트 루트를 제외한 URI를 얻기 위해, 컨텍스트 루트 길이만큼 substr 사용
				String uriWithoutContextPath = uriToMatch.substring(contextPath.length());
				String[] parts = uriWithoutContextPath.split("\\?");
				// 첫 번째 요소는 '?' 이전의 부분, 두 번째 요소는 '?' 이후의 부분
				String uriPart = parts[0]; // '?' 이전의 부분
				for(CustomMap urlMap :rsrcList) {
//					key.
					String pattern_url = urlMap.getString("resource_pattern", "");
					CustomRequestMatcher matcher = new CustomRequestMatcher(pattern_url); 
					if(matcher.matches(uriPart)) {
						authorized = true;
						break;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(authorized) {
			if(hasParamMap) {
				if(!"".equals(sb.toString())) {
					String queryStr = sb.toString();
					String quryStr = queryStr.substring(0,queryStr.lastIndexOf("&"));
					System.out.println("queryStr:"+quryStr);
					redirectStrategy.sendRedirect(req, resp, redirectUrl+quryStr);
				}else {
					redirectStrategy.sendRedirect(req, resp, redirectUrl);
				}
			}else {
				redirectStrategy.sendRedirect(req, resp, redirectUrl);
			}
		}else {
	        CommandMap param1 = new CommandMap();
	        param1.put("id_user", loginedId);
			CustomMap usrMainMenu = userGroupDao.getUserDefaultMenuByIdUser(param1.getMap());
			StringBuffer sb1 = new StringBuffer();
			if(!"".equals(usrMainMenu.getString("id_main_menu",""))) {
		        param1.put("id_menu", usrMainMenu.getString("id_main_menu"));
				CustomMap menuItem = menuDao.getMenuInfoByIdMenu(param1.getMap());
				if(!"".equals(menuItem.getString("menu_url", ""))) {
					sb1.append(menuItem.getString("menu_url", ""));
					if(!"".equals(menuItem.getString("id_menu", ""))) {
						sb1.append("?id_menu="+menuItem.getString("id_menu", ""));
					}
				}
			}
			if(!"".equals(sb1.toString())) {
				String defaultDirectUrl=sb1.toString();
				System.out.println("defaultDirectUrl:"+defaultDirectUrl);
				redirectStrategy.sendRedirect(req, resp, defaultDirectUrl);
			}else {
				redirectStrategy.sendRedirect(req, resp, defaultSuccessUrl);
			}
		}
		//String targetUrl = req.getParameter(targetUrlParameter);
		//redirectStrategy.sendRedirect(req, resp, targetUrl);
	}
}
