package kr.co.daily.front.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.AddBatchUtil;
import daily.common.util.CollectionUtil;
import daily.common.util.MetaUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.controller.BbsController;
import kr.co.daily.front.dao.BbsDAO;

@Service
public class BbsService {
	@Value("#{global['openapi.addbatch.cnt']}")
	private int addBatchCnt;
	
	private Logger log = LoggerFactory.getLogger(BbsService.class);
	
	@Autowired private BbsDAO dao;

	public CustomMap getBoardRow(CommandMap map) {
		return dao.getBoardRow(map.getMap());
	}

	public List<CustomMap> selectBoardData(CommandMap map) {
		return dao.selectBoardData(map.getMap());
	}

	public List<CustomMap> selectBoardMeta(CommandMap map) {
		return dao.selectBoardMeta(map.getMap());
	}

	public PageList<CustomMap> selectBbsList(CommandMap map) {
		PageList<CustomMap> rowList = dao.selectBoardRow(map.getMap(),MybatisPagingUtil.pageBounds(map));
		List<Integer> keyList = new ArrayList<>();
		for(CustomMap row:rowList) {
			int id_row = row.getInt("id_meta_row",0);
			if(id_row>0) {
				keyList.add(id_row);
			}
		}
		if(keyList.size()>0) {
			CommandMap param = new CommandMap();
			param.put("key_list", keyList);
			List<CustomMap> rowListCol = dao.selectBoardMeta(param.getMap());
			for(CustomMap cm : rowList) {
				int key = cm.getInt("id_meta_row",0);
				for(CustomMap rm:rowListCol) {
					if(rm.getInt("id_meta_row",0)==key) {
						if(rm.getInt("id_data",0)==3735) {
							log.debug("debug rawList id_data:"+rm.getInt("id_data",0));
						}
						log.debug("rawList id_data:"+rm.getInt("id_data",0));
						if(!"".equals(rm.getString("val",""))) {
							rm = MetaUtil.transNumberingByCustomMap(rm);
						}
						cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
					}
				}
			}
			
		}
		return rowList;
	}

	public CustomMap getBbsM(CommandMap map) {
		return dao.getBbsM(map.getMap());
	}

	public List<CustomMap> selectBbsHeaderList(CommandMap map) {
		return dao.selectBbsHeaderList(map.getMap());
	}

	public List<CustomMap> selectBoardAttachFile(CommandMap map) {
		return dao.selectBoardAttachFile(map.getMap());
	}

	public ModelMap insertBoard(CommandMap map, ModelMap model, MultipartHttpServletRequest multipartHttpServletRequest) throws Exception {
		try {
	
			dao.insertBoardRow(map.getMap());
	        if(!"".equals(map.getString("id_meta_row", ""))) {
	    		List<CustomMap> colList = this.selectBoardMeta(map);
	    		if(colList==null || colList.size()==0) {
	    			throw new Exception("매칭되는 정보를 가져오지 못했습니다.");
	    		}
	        	List<CustomMap> insDataList = this.insertBoardData(colList,map);
				model.addAttribute("insDataList",insDataList);
	        }else {
				throw new Exception("행 삽입에 실패하였습니다.");
	        }
			model.addAttribute("id_meta_row",map.getString("id_meta_row",""));
			Iterator<String> iterator = multipartHttpServletRequest.getFileNames(); 
			MultipartFile multipartFile = null; 
			List<CustomMap> fileList = new ArrayList<>();
			int file_no=1;
			while(iterator.hasNext()){ 
				String fn = iterator.next();
				multipartFile = multipartHttpServletRequest.getFile(fn); 
				if(multipartFile.isEmpty() == false){
					log.debug("------------- file start -------------");
					log.debug("name : "+multipartFile.getName());
					log.debug("filename : "+multipartFile.getOriginalFilename());
					log.debug("size : "+multipartFile.getSize());
					log.debug("-------------- file end --------------\n");
					log.debug("name : "+multipartFile.getName());
					String fileNm = multipartFile.getOriginalFilename(); 
					String[] fieldNmArr = multipartFile.getName().split("_");
					if(fieldNmArr.length!=4) {
						throw new Exception("첨부파일 id가 잘못되었습니다.");
					}
					String file_path = SpringPropertiesUtil.getProperty("board.file.upload.path")+File.separator+map.getString("id_meta_m","unknown");
					String now = new SimpleDateFormat("yyyyMMddHmsS").format(new Date());  //현재시간
				    int i = -1;
		            i = fileNm.lastIndexOf("."); // 파일 확장자 위치
		            String fileName = fileNm.substring(0, i);
		            String fileExt = fileNm.substring(i);
		            String realFileName = fileName+"_"+now+fileExt; 
		            File saveDirectory = new File(file_path);
		            if (!saveDirectory.exists()) {
		                if (saveDirectory.mkdirs()) {
		                    log.debug("Directory created: " + saveDirectory.getAbsolutePath());
		                } else {
		                    log.error("Failed to create directory: " + saveDirectory.getAbsolutePath());
		                }
		            }
					File saveFile = new File(file_path, realFileName);
	//				String file_full_path=file_path+File.separator+multipartFile.getName();
					log.debug("saveFile : "+saveFile.getName());
					multipartFile.transferTo(saveFile);
					CommandMap addFile = new CommandMap();
					addFile.put("id_meta_row", map.getString("id_meta_row", ""));
					addFile.put("id_meta", fieldNmArr[2]);
					addFile.put("file_nm", fileNm);
					addFile.put("file_real_nm", file_path+File.separator+realFileName);
					addFile.put("file_no", file_no);
					dao.insertBoardFile(addFile.getMap());
					CustomMap addedFile = CollectionUtil.getCustomMapByCommandMap(addFile);
					fileList.add(addedFile);
					file_no++;
				} 
			}
			model.addAttribute("file_list",fileList);
			List<CustomMap> attachColList = dao.selectAttachMetaList(map.getMap());
			model.addAttribute("attachColList",attachColList);
		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception("알 수 없는 오류");
		}
		return model;
	}

	public List<CustomMap> insertBoardData(List<CustomMap> colList,CommandMap map) {
		List<CommandMap> inList = CollectionUtil.getCommandListByCommandMapAndPrefix(colList, "id_meta", map, "id_meta_","val");
		for(CommandMap inMap : inList) {
			inMap.put("id_meta_m", inMap.getString("id_meta_m"));
			inMap.put("id_meta", inMap.getString("id_meta"));
			inMap.put("id_meta_row", map.getInt("id_meta_row"));
		}
		List<CommandMap> calColList = MetaUtil.calculateRowData(colList,inList,null,map.getInt("id_meta_row"));
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, calColList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertMetaDataByGroup(inInputList);
		}
		List<CustomMap> dataList = dao.selectMetaDataByIdRow(map.getInt("id_meta_row"));
		return dataList;
	}

	public ModelMap deleteBoard(CommandMap map, ModelMap model) {
		List<CustomMap> fileList = dao.selectBoardAttachFile(map.getMap());
		for(CustomMap fileMap:fileList) {
			File file = new File(fileMap.getString("file_real_nm"));
			file.delete();
		}
		dao.deleteBoardFile(map.getMap());
		dao.deleteBoardData(map.getMap());
		dao.deleteBoardRow(map.getMap());
		return model;
	}

	public ModelMap modifyBoard(CommandMap map, ModelMap model, MultipartHttpServletRequest req) throws Exception {
//		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)req; 
		if(map.getStringList("atch_del_id")!=null) {
			List<String> delList = map.getStringList("atch_del_id");
			CommandMap param = new CommandMap();
			param.put("del_id_list", delList);
			List<CustomMap> fileList = dao.selectBoardAttachFileIn(param.getMap());
			for(CustomMap fileMap:fileList) {
				File file = new File(fileMap.getString("file_real_nm"));
				file.delete();
			}
			dao.deleteBoardFileInIdBoardFile(param.getMap());
			log.debug("------------- BoardFile delete complated -------------");
		}
		try {
			Iterator<String> iterator = req.getFileNames(); 
	//		req.getF
			MultipartFile multipartFile = null; 
			List<CustomMap> fileList = new ArrayList<>();
			int file_no=dao.getBoardMaxFileNo(map.getMap())+1;
			while(iterator.hasNext()){ 
				String fn = iterator.next();
				multipartFile = req.getFile(fn); 
				if(multipartFile.isEmpty() == false){
					log.debug("------------- file start -------------");
					log.debug("name : "+multipartFile.getName());
					log.debug("filename : "+multipartFile.getOriginalFilename());
					log.debug("size : "+multipartFile.getSize());
					log.debug("-------------- file end --------------\n");
					log.debug("name : "+multipartFile.getName());
					String fileNm = multipartFile.getOriginalFilename(); 
					String[] fieldNmArr = multipartFile.getName().split("_");
					if(fieldNmArr.length!=4) {
						throw new Exception("첨부파일 id가 잘못되었습니다.");
					}
					String file_path = SpringPropertiesUtil.getProperty("board.file.upload.path")+File.separator+map.getString("id_meta_m","unknown");
					log.debug("file_path : "+file_path);
					String now = new SimpleDateFormat("yyyyMMddHmsS").format(new Date());  //현재시간
				    int i = -1;
		            i = fileNm.lastIndexOf("."); // 파일 확장자 위치
		            String fileName = fileNm.substring(0, i);
		            String fileExt = fileNm.substring(i);
		            String realFileName = fileName+"_"+now+fileExt; 
		            File saveDirectory = new File(file_path);
		            if (!saveDirectory.exists()) {
		                if (saveDirectory.mkdirs()) {
		                    log.debug("Directory created: " + saveDirectory.getAbsolutePath());
		                } else {
		                    log.error("Failed to create directory: " + saveDirectory.getAbsolutePath());
		                }
		            }
	
					File saveFile = new File(file_path, realFileName);
//					String file_full_path=file_path+File.separator+multipartFile.getName();
					log.debug("saveFile : "+saveFile.getName());
					multipartFile.transferTo(saveFile);
					CommandMap addFile = new CommandMap();
					addFile.put("id_meta_row", map.getString("id_meta_row", ""));
					addFile.put("id_meta", fieldNmArr[2]);
					addFile.put("file_nm", fileNm);
					addFile.put("file_real_nm", file_path+File.separator+realFileName);
					addFile.put("file_no", file_no);
					dao.insertBoardFile(addFile.getMap());
					CustomMap addedFile = CollectionUtil.getCustomMapByCommandMap(addFile);
					fileList.add(addedFile);
					file_no++;
				} 
			}
			model.addAttribute("file_list",fileList);
	        dao.modifyMetaRow(map.getMap());
    		List<CustomMap> colList =  this.selectBoardMeta(map);
    		List<CustomMap> attachColList = dao.selectAttachMetaList(map.getMap());
			model.addAttribute("attachColList",attachColList);
    		Map<String,Object> insDataList = this.modifyMetaData(colList,map);
			model.addAttribute("insList",(List<CustomMap>)insDataList.get("insList"));
			model.addAttribute("modList",(List<String>)insDataList.get("modList"));
			model.addAttribute("delList",(List<CustomMap>)insDataList.get("delList"));

		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception("파일 생성 오류");
		}
		return model;
	}
	public Map<String,Object> modifyMetaData(List<CustomMap> colList, CommandMap map) {
		
		List<CustomMap> calColInfoMap = new ArrayList<>();
		List<CommandMap> inList = MetaUtil.getModifyCommandListByCommandMapAndPrefix(colList,calColInfoMap , "id_meta", map, "id_meta_","i_id_meta_","i_id_data_","id_data" ,"val");
		List<CommandMap> insDataList = new ArrayList<>();
		List<CommandMap> modDataList = new ArrayList<>();
		List<CustomMap> insList = new ArrayList<>();
		List<Integer> delList = new ArrayList<>();
		List<CustomMap> modList = new ArrayList<>();
		List<CommandMap> modValList = new ArrayList<>();
		Map<Integer,CommandMap> modColMap = new HashMap<>();
		for(CommandMap inMap : inList) {
			if(!"".equals(inMap.getString("modify_type",""))) {
				String modify_type = inMap.getString("modify_type","");
				if("D".equals(modify_type)) {
				    int id_data = inMap.getInt("id_data",0);
				    if(id_data>0) {
				    	delList.add(id_data);
				    }
				}else if("I".equals(modify_type)) {
					insDataList.add(inMap);
					insList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}else if("M".equals(modify_type)) {
					inMap.put("id_data", inMap.getInt("id_data",0));
					modDataList.add(inMap);
					modList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}
			}else {
				modValList.add(inMap);
				modColMap.put(inMap.getInt("id_meta"), inMap);
			}
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, insDataList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertMetaDataByGroup(inInputList);
		}
		if(modList.size()>0) {
			for(CommandMap inMap :modDataList) {
				dao.updateMetaData(inMap.getMap());
			}
		}
		if(delList.size()>0) {
			CommandMap delParam = new CommandMap();
			delParam.put("del_list", delList);
			dao.deleteMetaDataByList(delParam.getMap());
		}
		for(Integer m:modColMap.keySet()) {
			modColMap.get(m);
		}
		Map<String,Object> ret = new HashMap<>();
		ret.put("delList", delList);
		ret.put("modList", modList);
		ret.put("insList", insList);
		return ret;
	}

	public PageList<CustomMap> selectBbsList(CommandMap map, List<CustomMap> makeList) {
		PageList<CustomMap> rowList = dao.selectBoardRow(map.getMap(),MybatisPagingUtil.pageBounds(map));
		List<Integer> keyList = new ArrayList<>();
		for(CustomMap row:rowList) {
			int id_row = row.getInt("id_meta_row",0);
			if(id_row>0) {
				keyList.add(id_row);
			}
		}
		if(keyList.size()>0) {
			CommandMap param = new CommandMap();
			param.put("key_list", keyList);
			param.put("id_meta_m", map.getInt("id_meta_m"));
			List<CustomMap> rowListCol = dao.selectBoardMetaAndData(param.getMap());
			for(CustomMap cm : rowList) {
				int key = cm.getInt("id_meta_row",0);
				for(CustomMap rm:rowListCol) {
					if(rm.getInt("id_meta_row",0)==key) {
						if(rm.getInt("id_data",0)==3735) {
							log.debug("debug rawList id_data:"+rm.getInt("id_data",0));
						}
						log.debug("rawList id_data:"+rm.getInt("id_data",0));
						if(!"".equals(rm.getString("val",""))) {
							rm = MetaUtil.transNumberingByCustomMap(rm);
						}
						cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
					}
				}
				for(CustomMap mm:makeList) {
					if(cm.getInt("id_meta_row",0)==key) {
						log.debug("rawList id_data:"+cm.getInt("id_data",0));
						String meta_cd = cm.getString(mm.getString("meta_cd", ""),"");
						if(!"".equals(meta_cd)) {
							mm = MetaUtil.transNumberingByCustomMap(mm);
							cm.putByString(mm.getString("meta_cd", ""), cm.getString(mm.getString("meta_cd", ""),""));
						}
					}
				}
			}
			
		}
		return rowList;
	}

	public CustomMap getBoardFile(CommandMap map) {
		return dao.getBoardFile(map.getMap());
	}

	public List<CustomMap> selectBoardShowYnMeta(CommandMap map) {
		return dao.selectBoardShowYnMeta(map.getMap());
	}

	public void updateViewCnt(CommandMap map) {
		dao.updateViewCnt(map.getMap());
	}

}
