package daily.common.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class EnumContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String[] params = sce.getServletContext().getInitParameter("enumServletContextConfig").split(",");
		for(String className : params) {
			className = className.trim();
			this.loadEnum(className,sce.getServletContext());
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {	}
	
	@SuppressWarnings("rawtypes")
	private void loadEnum(String className,ServletContext context) {
		Map<String,Enum<?>> enumMap = null;
		List<Enum> enumList = null;
		
		try {
			@SuppressWarnings("unchecked")
			Class<Enum> clazz = (Class<Enum>)Class.forName(className);
			Enum[] enums = clazz.getEnumConstants();
			
			enumMap = new HashMap<String,Enum<?>>(enums.length);
			for(Enum e:enums) {
				enumMap.put(e.name(), e);
			}
			
			enumMap = Collections.unmodifiableMap(enumMap);
			enumList = Collections.unmodifiableList(Arrays.asList(enums));
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
			
		}
		context.setAttribute(this.getAlias(className), enumMap);
		context.setAttribute(this.getAlias(className)+"List", enumList);
	}
	
	private String getAlias(String className) {
		String[] token = className.split("\\.");
		String alias = token[token.length-1].trim();
		String initial = alias.substring(0,1);
		String suffix = alias.substring(1);
		
		initial = initial.toLowerCase();
		alias = initial + suffix;
		
		return alias;
	}

}
