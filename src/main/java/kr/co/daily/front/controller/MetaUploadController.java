package kr.co.daily.front.controller;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import daily.common.security.LoginSuccessHandler;
import daily.common.util.FileUtil;

@Controller
public class MetaUploadController {
	private static Logger log = LoggerFactory.getLogger(LoginSuccessHandler.class);
	private static String FILE_UPLOAD_PATH;
//	private static String FILE_PDF_UPLOAD_PATH;
//	
//	@Autowired private FileService fileService;
	
    @Value("#{global['file.upload.path']}")
	public void setFileUploadPath(String fileUploadPath) {
    	FILE_UPLOAD_PATH = fileUploadPath;
	}
//    @Value("#{global['file.pdf.upload.path']}")
//	public void setFilePdfUploadPath(String filePdfUploadPath) {
//    	FILE_PDF_UPLOAD_PATH = filePdfUploadPath;
//	}
	//Post 방식 파일 업로드
	@RequestMapping(value = "/front/meta/uploadForm.do", method = RequestMethod.POST)
	public String uploadFormPOST(MultipartFile file, Model model) throws Exception {
		
		log.info("uploadFormPost");
		
		if(file != null) {
			log.info("originalName:" + file.getOriginalFilename());
			log.info("size:" + file.getSize());
			log.info("ContentType:" + file.getContentType());
		}
		
		String savedName = uploadFile(file.getOriginalFilename(), file.getBytes());
		
		model.addAttribute("savedName", savedName);
		
		return "/sample/upload/uploadForm";
	}
	//업로드된 파일을 저장하는 함수
	private String uploadFile(String originFilename, byte[] fileData) throws IOException {
		
		String extName
		= originFilename.substring(originFilename.lastIndexOf("."), originFilename.length());
//		UUID uid = UUID.randomUUID();
		
		String savedName = FileUtil.genSaveFileName(originFilename, extName);
		String folderPath = FileUtil.genSaveFilePath(FILE_UPLOAD_PATH);
		File target = new File(folderPath, savedName);
		if(target.mkdirs()) {
			FileCopyUtils.copy(fileData, target);
		}
		//org.springframework.util 패키지의 FileCopyUtils는 파일 데이터를 파일로 처리하거나, 복사하는 등의 기능이 있다.
		
		return savedName;
		
	}

}
