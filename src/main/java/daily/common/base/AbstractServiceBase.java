package daily.common.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daily.common.vo.CustomMap;

public class AbstractServiceBase extends AbstractCommonBase
{
	protected List<CustomMap> arrayToListObject(CustomMap resultMap, String[] colNmArr) {
		Map<Integer,CustomMap> hash = new HashMap<Integer,CustomMap>();
		int listCnt = ((String[])resultMap.get(colNmArr[0])).length;
		for(int i=0;i<listCnt;i++){
			hash.put(i,new CustomMap());
		}
		for(String val : colNmArr){
		Object[] tranArr = (Object[])resultMap.get(val);
			for(int i=0;i<listCnt;i++){
				CustomMap el = hash.get(i);
				el.put(val, tranArr[i]);
			}
		}
		List<CustomMap> list = new ArrayList<CustomMap>();
		for( int i : hash.keySet()){
			list.add(hash.get(i));
		}
		return list;
	}

	/**
	 * 메일 템플릿 리턴
	 * @사용예 getMailTemplete("메일 제목", "메일 내용(html)");
	 * @설명 공통 메일 템플릿으로 리턴
	 * 
	 * @param strTitle - 메일 제목
	 * @param strContent - 메일 내용(html)
	 * 
	 */
	protected String getMailTemplete(String strTitle, String strContent){
		String body = "<html>" +
				"<head> " +
				"<title>"+strTitle+"</title>" +
				"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>" +
				"</head>" +
				"<body>" +
				"<table cellspacing='0' cellpadding='0' border='0' style='width:100%;max-width:700px;min-width:300px;'>" +
				"<tbody>" +
				"	<tr>" +
				"		<td>" +
				"			<div style='padding:15px 15px;background-color:#134b95;'>" +
				"				<div style='font-size:20px; font-weight:bold; color:#fff; height:25px; line-height:28px;'>test</div>" +
				"			</div>" +
				"			<div style='width:100%; padding:0 20px; margin:40px auto; font-family:\"Malgun Gothic\"; box-sizing:border-box;'>" +
				"				<div style='font-size:18px; font-weight:800; margin-top:35px; line-height:18px;'>"+strTitle+"</div>" +
				"				<div>" + strContent + "</div>" +	
				"           </div>" +
				"			<div style='background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAAAlCAYAAAG8MG76AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAsESURBVFhH7Vl5cFbVFed/1iSsMYQIKgx1qSMztdhSQAfzbSFFqIjQEFyoBqwpLSoaQSHAQMKXPSE7BJLAKIRNiTQgLVvEEmJAIEgABZFNxWoRId+v53feki8Jm7gM7Xxn5s5979577jn3vHPP9tr8PHCrIwHsu7uS0MlToM+EDu5F+tzNOd8eawVnAYQ559oL+M7+c7O/WaGdxzgcDxnkztXndp4i7St3nTbfjTU2WIdjX7zTZ0/ysCdu8gOb0GtYArpGL0ZHTy56OJNw14RydJCPHuJORRdnCvo6XkJ3x3x0c6ehiysdvV0zZTxL13Z3JaODu0j6JHR1yXpp5rZNECHaFBRVKJvmInzYa+g/rkie89HLMRO9nK+hX2Q8wiNf1826uVPQy52IYHeWzoc5Z6sG9nTOFILJusbc9vrhjsipqNh+4n/hYwTAH6haHd0F+tWpYkHufIS4MlUN74vNVxVk6yjjR9Goqkgt4VjPUUWyNguhgsv5My1vI+9ysDPDHuQ7F7INiM2xxyvePwfOhXm8zdaaj22KNx5DqJ+9U6DhoBHhBHU32JWDHq55qu8Dx6dqz3GerqEReuF6OhNV50NH5Bv6L2s4ftLXgnPrKDQ6lgHiGJ9PSeMJrHELOO8vgkPftNj0eoEcmo8BCMDPB30c0xAklz8+vw4vZFaKv0lWgxDsytXr9ZS3GsXvHNRrFuTJQifXQvVpvIZ3TViiV4vv1jivGK9cnfhpk0SbAaPnqXEJ9uQpHtfRr30iN2fIE4Z/45Xs6ClWmrzCB8XXm+hXhlsjp6Odu0QNCTcv2X6hGRL9HAOP3o+XY3pZPRJLa7XNKf0Auat2X5bAnWMylZlfPLVSexqzljHHie9oDsSZiyX85GLzuX5/SFO8ES+vvPoBGA5azAe5c1BRe8lGYKBD08y522JWIq6gHs/l12Ny4QHp92NW6Z7Lbn7f414lfnvMMmWQkm1psz6+IIZRTDq/CO2aOaxw96MSeQh+9Asrrs48nT8/4ZS8GjwzvxLdnV7dkCEMxyd6/4mUigPqYzhO/8LW2ZmGe2NLEBY5R5ngVwtxC57ghDrnYduxJob6jiRuqo3LRh9EtblvbIY5l6WNNOgVdsmJTPSfDhiHBzmzsaz6q5+eWAACEIAABOB7wWn4NJahYzoioX6Ea4Y4icxm8ccpWXPnyDnqgKzxTq48jWHe2Ad1Kta4geNFdHy+bfNXbPtMnRZjlmDxmlzD9Xc/MheV9Y2aZjOWahtlJFzEHzOt5No+g8zTqzHl6CAMGa483c7MujsNz9feVSwEcnSO3pA9iazeCw3CWGDg+lDXAoR4smWfIgx+MgNVBxv1kJYgdF8J8NjfEk26Bco49+I4vTgdHj22a7JRxLkiWMwzdmFj6mhOKbxTD2Wac9PKjyFx6R4NymYvlVZac9nNa77waUTKCJSMMREc8/q6Vmtv/b1X5yLjy5rNvSdhAb8SQwhz6PJwWvJdi/m2nhI8n/ZuM4RZZbXCQK4GbpaUrJ6HMpe1AkrSEgibOdwMKP22Mld/rnUMY+AbZbMrwmkJjJqYX4SerjnNENZ9SMkL87LRnwqOalQ5qWC/RpZ/zt932c0PfG3sSTwGa1SBuKTKVmvDh6cp3ZEvljeb2/OloQ1UN3Po8sBQNEQ+kSHNXCNEdS6QBEPyd8nZGd11daZreEo9D3PMQrhjBsJdsxDhmI639/gQEfmyXf8yLl+Orn8gNgVr5ULz8/NOac1A1oVHztI+bHiqHo60+RUiHKwjJIoxkDsjd+ShZ67BPGNsbhrqmo16SQjCBNm4kEbYSitxQu7F7SPm2+OdRSKc4wVcIReW+kkGKClr/KFJxTbhws0nlIZx2ZtC4rtHzsTyXUJfGDfGm/Ajn78Oa0NomSQQOHbG11R9tYDj/m2tyTwJMlPimLm0FVhzFq4O+gELKLyD5utPD1QbWpYwx7yfj+iPBRvrvtL7wWKyORSAAAQgAAEIQAACEIAfA6KfTcaDEosPmbQID8flIfXND+yA6/1PgRmSu45MWIWhk5dg6KRSbUOeK9P3YXFFmLtkp73+yHkgfe1hjEtcj2Hx5fb6wZONpu/cZ/JiPDa9AlUNgCsuQ3MBe17aoMlL5b1c2hJ4pr6B+MytWFf77VUDQf64KKj6FOPnbpAceZm9F5s//WGTCvDYlAw7RD/ZeAnlW7/A00lVGBy3GHeMTNOEjK3/6AxETlmOKdnVqNz73Y8XiLKQEOFIQFd3mpZXmNRMyD4Ex7S/a0WC7x3cCzVpYrbYrHn4TzsLY1P3YmrpMfRwzNHESTNLmWsn+7XCMVsnmev/xHIsl4yxp2O27sPx9q7CVmtZUWFGyaSN+c89oxdg5/GmvKagsgHhTqHtNFLolrxyT/72st6ZR902KhuFktAxKyXPRmKYr5ksU/e2rsWaaiuejFn0OztTMGBcOvb+0H8gFDx/rfHgJETGSJi9EhOGbnHNx0u521FZ+29UN1zAe9LYbz3iw4bDQP8xOcqU/8GGJ6zHoupL2Cjzm474taONePewT58pvFMXgF2HvsT7Df+x97b2r264iM37v0Xm6o9wz9hsFRB5onCZASe+dU4VhDU9CkfH3cnw/G0Zlm87ix0HL+peOw9/Z+5n7PkP4Wn4K5vM/5gsq+Vr7XDIs8VYtOk4th66gI/kOlC4m/efx4IV+0TzjR9bBn2RiShZ0aZPb1z4p3yNiHj4VVvwVmvrWaJjUVPKJDtunZFbkLL6sJYIeGgLl8Jn7S7YnanCMsoPRqPGGi0Z949P/V6MJ63cp/tZNChs9lQUjvVwL0CN30/gK8HW+vP64dq6CxWvi9uLtbu/uSbeq0U7TPqknY++o1JuXPAsc/SJTPATvHEIVtEouOhpb1xV8N6Kg/q/1hI8fzpTK6gdlqD569MWvJOF2lTVsIHfU/D8rUpc/XfstMyJQZP8st764enrEPyBr1Xwek53sSrBhr1X9x8E1mpD3EJfBV+A2x/x3rjgWfWjje/szlZG2NpHGfaNm9Ns9BqRjpnL6tU87D7bKA2oPQP864wPW+S53+OyzpNn47FiN9Zbq/ab89WfA9ul3y79ji+MnmM1XwLHG40rXSf7cU+2urM+bbuII+ZoYdVZDJhQrCbFqk7yls1YdQYPTn3bNAFG+b+bOwWPvrYBq+rElJ3yKa8Wv9yP/O847YPj1S1a2efHIy4/qOvFNVi5uxF1wtsxH/DxRTnjSYP+L8cXmvQNU0PHu7Dy4xsXPGvFTaZGBOjMREzGfgz9y2rVBK31yninqDx9DhFHS3vOD8IDk+GY1N2Iz9tnl1qpEdY/EK7TRhyzkRY1/97YEqzYKz5GPjxvgP8aNlb5ict92suBeWhe9f6j5mH7RxfsQ6e8WSManKi01bEKv1eiHWTy3HdUKnK2AKEer/LDvf3xWJXlepai6QvaR4mDVvp07kmoEY0xyd84DBuXgEEx8zAwJgmD/jgHyaVb7U15wFdyqhD1fAF+Mz4FD8Sm4YGYVFmbjvtjkwVvLmbmVdnrG84ByWU78OiLJfjdk6mKQ5Pi34j/2xgvRvx1CSr4b8gxVw/Lg1MIvEG/mpAtaw1z5IhbiElJa/DmtuNXPewn3wBZFXUYm1COh57OsGn502YbJOeMntj0M4IOvvCt/YiZXo5fj0tWRWANv7djBvq4pmPoxAw8N38NVld/9sOFfbMAy+Rhcm0tzQ4V21tWff7/54A3K6w3Ba9RkAifP2PKd5wLCD4AAQhAAAJwE0ObNv8FcX3ZUlfhTUIAAAAASUVORK5CYII=\") right no-repeat;border-top:2px solid #000;padding:0 100px 0 20px;font-size:14px;color:#000;line-height:22px;background-color:#eee' >" +
				"				<div style='margin-top:25px'>본 메일은 발신 전용 메일입니다.</div>" +
				"				<div>KEPCO Copyright @ 2018 KOREA Electric Power Corporation. All Right Reserved.</div>" +
				"				<div style='padding-bottom:30px;'>업무문의 : 기술기획처 기술표준부 Tel : 061-345-4926</div>" +
				"			</div>" +
				"		</td>" +
				"	</tr>" +
				"</tbody>" +
				"</table>" +
				"</body>" +
				"</html>";
		return body;
	}
}
