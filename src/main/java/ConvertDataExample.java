import java.util.ArrayList;
import java.util.List;

public class ConvertDataExample {
    public static void main(String[] args) {
        List<CommandMap> originalData = new ArrayList<>();
        originalData.add(new CommandMap(1, "부모1"));
        originalData.add(new CommandMap(2, "자식1"));
        originalData.add(new CommandMap(3, "손자1"));
        originalData.add(new CommandMap(3, "손자2"));
        originalData.add(new CommandMap(2, "자식2"));
        originalData.add(new CommandMap(1, "부모2"));
        originalData.add(new CommandMap(1, "부모3"));
        originalData.add(new CommandMap(1, "부모4"));
        originalData.add(new CommandMap(2, "자식4-1"));
        originalData.add(new CommandMap(2, "자식4-2"));

        List<CommandMap> convertedData = new ArrayList<>();

        int currentDepth = 1; // 시작 깊이 설정
        int currentMaxIndex = 0; // 현재 깊이에서의 최대 인덱스

        for (CommandMap data : originalData) {
            // 주어진 데이터의 깊이를 가져옴
            int dataDepth = data.getDepth();

            if (dataDepth == currentDepth) {
                // 같은 깊이 내에서의 증가
                currentMaxIndex++;
            } else {
                // 새로운 깊이로 넘어가면 현재 깊이와 최대 인덱스를 업데이트
                currentDepth = dataDepth;
                currentMaxIndex = 1;
            }

            // 변환된 데이터를 생성하고 리스트에 추가
            convertedData.add(new CommandMap(dataDepth, currentMaxIndex, data.getValue()));
        }

        // 결과 출력
        for (CommandMap data : convertedData) {
            System.out.println("Depth: " + data.getDepth() + ", Index: " + data.getIndex() + ", Value: " + data.getValue());
        }
    }
}

class CommandMap {
    private int depth;
    private int index;
    private String value;

    public CommandMap(int depth, String value) {
        this(depth, 0, value);
    }

    public CommandMap(int depth, int index, String value) {
        this.depth = depth;
        this.index = index;
        this.value = value;
    }

    public int getDepth() {
        return depth;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }
}
