package kr.co.daily.common;

public class GlobalConst {
	// 로그인 정보를 가져올 때 사용
	public final static String LOGIN_INFO = "loginInfo";
	// 성공했을 때 사용
	public final static Integer ACT_SUCCESS = 0;
	// 실패했을 때 사용
	public final static Integer ACT_FAIL = 1;
	// 세션 정보가 없을 때 사용
	public final static Integer NOT_LOGIN = 2;
	// 사용자가 존재하지 않을 때 사용
	public final static Integer NOT_FOUND_USER = 3;
	// 비밀번호가 일치하지 않을 때 사용
	public final static Integer NOT_PW_MATCH = 4;
	// 초기비밀번호값
	public final static Integer PW_ENCRYPT_ERR=5;
	// 권한없음
	public final static Integer NOT_ACCESS_AUTH = 6;
	
	public final static Integer NOT_CAPTCHA_MATCH = 7;
	
	public static final Integer NOT_FOUND_LOGIN_INFO = 8;
	// 사용자 승인 대기중
	public static final Integer NOT_ACCEPT_USER = 9;
	// 초기비밀번호값
	public final static String INIT_PW_VAL="1";
	// 초기비밀번호값
	public final static Integer ACT_NO_UPD_TRGT=10;

	
	
	
	// URL 매핑의 경우 숨기기 
//	public final static String GUEST_AUTH="GUEST";
	public static final int STAND_FIND_ROW = 5;
	public final static String USER_AUTH="LOGINER";
	public final static String GUEST_AUTH="ROLE_ANONYMOUS";
	public final static String DATA_INPUTER_AUTH="DATA_INPUTER";
	public final static String BBS_INPUTER_AUTH="BBS_INPUTER";
	
	public final static int ID_GROUP_GUEST=2;
	
	public final static String CG_CATEGORY="CT";
	public final static String CG_DATA_TYPE="DT";
	public static final String CG_LANGU = "UL";
	public static final String CG_UNIT_TYPE = "UN";
	public static final String CG_BBS_TYPE = "12";
	public static final String IMG_FILE_FILED_NM = "imgFile";
	public static final String CG_NATION_TOP="1";
	public static final int CG_ID_CONSTS_TYPE=7;
	
	public static final int CG_ID_KEY_USE_TYPE=13;
	public static final int KEY_USE_TYPE_EXCEL_UPLOAD=1;

	
//	public static final String BASIC_DATE_FORMAT = "dd/MM/yyyy";
	public static final String BASIC_DATE_FORMAT = "yyyy-MM-dd";
	public static final String BASIC_DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String LOCAL_DATE_FORMAT = "MMM dd, yyyy";
	// applicant type
	public final static String DT_NUM="1";
	public final static String DT_FLOAT="2";
	public final static String DT_STR="3";
	public final static String DT_DATE="4";
	public final static String DT_DATETIME="5";
	public final static String DT_PERCENT="6";
	public final static String DT_BOOL="7";
	public final static String DT_CALC="8";
	public final static String DT_GRP_HEAD="9";
	public final static String DT_ATCH_FILE="10";
	public final static String DT_MULTI_ATCH_FILE="11";
	// sys_code에서 가져오는 값
	public final static String DT_CODE_GROUP="12";
	// table의 하나의 컬럼 값을 가져오는 값
	public final static String DT_TAL_CODE_GROUP="13";
	// 여러 테이블에서 가져오는 값
	public final static String DT_TAL_COMPLEX ="14";
	
	public final static int DLT_1=1;
	public final static int DLT_2=2;
	public final static int DLT_3=3;
	
	
	public static final String RESOURCE_TYPE_URL="url";
	public static final String RESOURCE_TYPE_MENU="menu";
	// Open api 유형
	public static final String API_BID_SEARCH_KEYWORD_GROUP_CD="1";
	
	public static final int CG_META_DATA_TY = 5;
	public static final int CG_STR_LEN_TY = 6;
	
	public static final String DETL_TYPE_POP = "P";
	public static final String DETL_TYPE_SUBMIT = "S";
	
	public static final String META_M_TYPE_BID="1";
	public static final String META_M_TYPE_IDEN="2";
	public static final String META_M_TYPE_META = "3";

	public static final int KEY_TIME_PK = 1;
	public static final int KEY_TIME_FK = 2;
	public static final int KEY_TIME_UNIQUE = 3;
	
	public static final int IDEN_TY_LIST = 1;
	public static final int IDEN_TY_REG = 2;
	public static final int IDEN_TY_DETAIL = 3;

	public static final String GLOBAL_MODE_LOCAL= "local";
	public static final String GLOBAL_MODE_SERVER= "server";
}
