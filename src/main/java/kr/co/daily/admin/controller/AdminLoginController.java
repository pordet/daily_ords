package kr.co.daily.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import daily.common.base.AbstractControllerBase;
import daily.common.util.SpringApplicationContext;
import daily.common.vo.CommandMap;
import kr.co.daily.common.dao.CommonDAOImpl;
import kr.co.daily.domain.MenuVO;

@Controller
public class AdminLoginController extends AbstractControllerBase {
	private Logger log = LoggerFactory.getLogger(AdminLoginController.class);

//	@Autowired private UsrService service;
	
	@RequestMapping("/adm/goAdminMainPage.do")
	public String goAdminMainPage(HttpServletRequest req,HttpServletResponse resp,CommandMap param, Model model) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth==null || "anonymousUser".equals(auth.getPrincipal())) {
			return "front/main/loginPage";
		}else {
			CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
			MenuVO vo = new MenuVO();
	        String loginedId = this.getLoginUserId();
			vo.setId_user(String.valueOf(loginedId));
			MenuVO menu = dao.getMainMenuByGroupId(vo);
			String URL = menu.getMenu_url()+"?id_menu="+menu.getId_menu();
//			resp.sendRedirect(URL);
			return "redirect:"+URL;
		}
	}
	@RequestMapping("/adm/loginPage.do")
	public String loginPage(HttpServletRequest req,HttpServletResponse resp,CommandMap param, Model model) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth==null || "anonymousUser".equals(auth.getPrincipal())) {
			return "adm/loginPage";
		}else {
			CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
			MenuVO vo = new MenuVO();
	        String loginedId = this.getLoginUserId();
			vo.setId_user(String.valueOf(loginedId));
			MenuVO menu = dao.getMainMenuByGroupId(vo);
			String URL = menu.getMenu_url()+"?id_menu="+menu.getId_menu();
//			resp.sendRedirect(URL);
			return "redirect:"+URL;
		}
	}
	/*
	@RequestMapping("/loginPage.do")
	public String login(HttpServletRequest req,HttpServletResponse resp,CommandMap param, Model model) throws Exception {
		String referrer = req.getHeader("Referer");
	    req.getSession().setAttribute("prevPage", referrer);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		log.info("濡�洹몄��");
		if(auth==null || "anonymousUser".equals(auth.getPrincipal())) {
			return "front/main/loginPage";
		}else {
			CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
			MenuVO vo = new MenuVO();
	        String loginedId = this.getLoginUserId();
			vo.setId_user(String.valueOf(loginedId));
			MenuVO menu = dao.getMainMenuByGroupId(vo);
			String URL = menu.getUrl()+"?id_menu="+menu.getIdMenu();
//			resp.sendRedirect(URL);
			return "redirect:"+URL;
		}
	}
	*/
	
	
//============================================================================================================== //
	@RequestMapping("/loginPage.do")
	public String login(HttpServletRequest req,HttpServletResponse resp,CommandMap param, Model model) throws Exception {
		//�댁�����댁� url ����
		String referrer = req.getHeader("Referer");
		req.getSession().setAttribute("prevPage", referrer);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		log.info("濡�洹몄��");
		if(auth==null || "anonymousUser".equals(auth.getPrincipal())) {
			return "front/main/loginPage";
		}else {
			/*
			CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
			MenuVO vo = new MenuVO();
			String loginedId = this.getLoginUserId();
			vo.setId_user(String.valueOf(loginedId));
			MenuVO menu = dao.getMainMenuByGroupId(vo);
			String URL = menu.getUrl()+"?id_menu="+menu.getIdMenu();
			return "redirect:"+URL;
			*/
			if(referrer.contains("loginAction.do")){
				return "redirect:/";
			}else {
				
				return "redirect:" + referrer;
			}
		}
	}
}
