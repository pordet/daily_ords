package crs;

import org.junit.Test;

import daily.common.util.RandomUtil;

public class RandomUtilTest {

	@Test
	public void randomCharAndNumTest() {
		int length=5;
		
		String encPassword = RandomUtil.randomCharAndNum(length);
		System.out.println(encPassword); 
//		assertEquals(password, encPassword); 
//		assertThat(encPassword,CoreMatchers.is("{by}"));
	}
	@Test
	public void randomCharTest() {
		int length=5;
		
		String encPassword = RandomUtil.randomChar(length);
		System.out.println(encPassword); 
//		assertEquals(password, encPassword); 
//		assertThat(encPassword,CoreMatchers.is("{by}"));
	}
	@Test
	public void randomNumTest() {
		int length=5;
		
		String encPassword = RandomUtil.randomNum(length);
		System.out.println(encPassword); 
//		assertEquals(password, encPassword); 
//		assertThat(encPassword,CoreMatchers.is("{by}"));
	}
}
