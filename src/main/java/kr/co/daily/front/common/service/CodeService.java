package kr.co.daily.front.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.front.common.dao.CodeDAO;
import kr.co.daily.front.dao.BidSearchDAO;

@Service
public class CodeService {

	@Autowired private CodeDAO dao;
	public List<CustomMap> selectCodeByIdCodeGroup(CommandMap map) {
		return dao.selectCodeByIdCodeGroup(map.getMap());
	}

}
