package daily.common.base;

import java.net.URL;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import daily.common.constant.DateEnum;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.domain.User;


public class AbstractCommonBase
{
	/**
	 * �� Ŭ���� �������� ��� ���� �н��� ���� ���� ���� �н��� ��´�.
	 * �ش� ������ �о� ���� �� ����Ѵ�.
	 * @param path ���ϸ� �Ǵ� ��� ���� �н���
	 * @return 
	 */
	public final String getFileFullPath(java.lang.String path) {
		URL filrUrl = this.getClass().getResource(path);
		String getPath =  filrUrl.getPath();
		String fullPath = getPath.startsWith("/") ? getPath.substring(1) : getPath;
		return fullPath;
	}

	/**
	 * ������ �α��� ID�� ��´�.
	 * @return
	 */
	protected String getMyAccounId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    return auth == null ? "" : auth.getName();			//get logged in username
	}

	/**
	 * ���� �α������� ���������� ��´�.
	 * @return
	 */
	public CustomMap getLoginInfo(){
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		CustomMap login = null;
		HttpSession session = req.getSession();
		if(session.getAttribute("SPRING_SECURITY_CONTEXT")!=null){
			SecurityContext context = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
			User user = (User) context.getAuthentication().getPrincipal();
			login = user.getUserMap();
		}
		return login;
	}
	
	public CommandMap addParamByLoginInfo(CommandMap map) {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(req);
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
        CustomMap logInfo = this.getLoginInfo();
        if(logInfo!=null) {
	        map.put("login_usr_seq", logInfo.getString("user_seq",""));
        }
		return map;
	}
	
	public Map<String, Object> addMapByLoginInfo(Map<String, Object> map) {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(req);
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
        CustomMap logInfo = this.getLoginInfo();
        if(logInfo!=null) {
	        map.put("login_usr_seq", logInfo.getString("user_seq",""));
        }
		return map;
	}
	
	public CommandMap addParamByBetween(CommandMap map,String startFieldNm,String endFieldNm,int type) {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(req);
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
        CustomMap logInfo = this.getLoginInfo();
        if(logInfo!=null) {
	        map.put("login_usr_seq", logInfo.getString("user_seq"));
        }
		if(map.getString(startFieldNm, "").equals("")) {
			String stDate = "";
			if(type==1) {
			   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.WEEK, 1);
			}else if(type==2) {
				   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.MONTH, 1);
			}else if(type==3) {
				   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.MONTH, 3);
			}else if(type==4) {
				   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.MONTH, 6);
			}else if(type==5) {
				   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.YEAR, 1);
			}else {
				   stDate = StringDateUtil.getMinusFromToday(GlobalConst.BASIC_DATE_FORMAT, DateEnum.YEAR, 3);
			}
			map.put(startFieldNm, stDate);
		}
		if(map.getString(endFieldNm, "").equals("")) {
			String stDate = StringDateUtil.getToday(GlobalConst.BASIC_DATE_FORMAT);
			map.put(endFieldNm, stDate);
		}
		return map;
	}
	
	public CommandMap addMenuIdAtParam(CommandMap map) {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//		UrlPathHelper urlPathHelper = new UrlPathHelper(); 
//		String url = urlPathHelper.getOriginatingRequestUri(req);
//		String context = urlPathHelper.getOriginatingContextPath(req);
		@SuppressWarnings("rawtypes")
		Map pmap = req.getParameterMap();
	    String id_menu = pmap.get("id_menu").toString().trim();
	    map.put("id_menu",id_menu);
	    return map;
	}

	/**
	 * ���� �α������� ID�� ��´�
	 * @return �� �α��� ��� null
	 */
	public String getLoginUserId() {
		CustomMap user = getLoginInfo();
		return user == null ? null : user.get("id").toString();
	}
}
