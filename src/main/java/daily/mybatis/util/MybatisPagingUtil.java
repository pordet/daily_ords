package daily.mybatis.util;

import static java.time.LocalTime.MAX;
import static java.time.LocalTime.MIN;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.Order.Direction;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import daily.common.base.AbstractPageVoBase;
import daily.common.vo.CommandMap;

@Component
public class MybatisPagingUtil {

    private static Logger logger = LoggerFactory.getLogger(MybatisPagingUtil.class);
		private static int DEF_ROW_COUNT;			// default
	    private static int MAX_ROW_COUNT;

//	    @Value("$global['mybatis.pagination.count.per.page']")
	    @Value("#{global['mybatis.pagination.count.per.page']}")
	    public void setMaxRowCount(int maxRowCount) {
	        MAX_ROW_COUNT = maxRowCount;
	    }

	    @Value("#{global['mybatis.pagination.line.per.page']}")
	    public void setDefRowCount(int defRowCount) {
	    	DEF_ROW_COUNT = defRowCount;
	    }
	    /**
	     * 	(*1) Unique 하지 않는 키로 order by 시 첫페이지에서 조회된 결과 일부가 두번째 페이지에서도 조회되어
	     * 		중복 표시되는 일이 생긴다.
	     * 		일반적인 처리를 위해 첫번째 오는 칼럼을 추가로 order by 주기로 한다.
	     * 		대부분은 첫 컬럼이 PK인 경우가 대부분이기 때문이다.
	     * @param offset
	     * @param limit
	     * @param order
	     * @return
	     */
	    public static PageBounds pageBounds(Integer offset, Integer limit, Order order) {
	        if(offset == null || offset == 0) offset = 1;			// default
	        if(limit == null || limit == 0) limit = DEF_ROW_COUNT;	// default

	        if(limit <= 0) {
	            limit = MAX_ROW_COUNT;
	        }
			Order orderApd = new Order("1", Direction.ASC, null); //(*1)

	        return order != null ? new PageBounds(offset, limit, order, orderApd) : new PageBounds(offset, limit); 
	    }
	 
	    public static PageBounds pageBounds(Integer offset, Integer limit) {
	    	return pageBounds(offset, limit, null); 
	    }

	    public static PageBounds pageBounds(CommandMap param) {
			logger.debug("param = " + param.getMap().toString());
	    	Integer offset = param.getInt("page");
	    	Integer limit = param.getInt("rows");
	    	String sidx = param.getString("sidx");
	    	String sord = param.getString("sord");
	    	String order_key = param.getString("order_key");
	    	String idx = param.getString("idx");
	    	
	    	if(offset == 0 ) offset = param.getInt("offset");
	    	if(limit == 0 ) limit = DEF_ROW_COUNT;
	    	if(!sidx.isEmpty()) {
	    		Order order = new Order(sidx, sord.equals("DESC") ? Direction.DESC : Direction.ASC, null);
	    		return pageBounds(offset, limit, order);
	    	} else if(!idx.isEmpty()){
	    		Order order = new Order(order_key, idx.equals("DESC") ? Direction.DESC : Direction.ASC, null);
	    		return pageBounds(offset, limit, order);
	    	} else {
	    		return pageBounds(offset, limit);    		
	    	}
	    }

	    public static PageBounds pageBounds(CommandMap param,CommandMap special) {
			logger.debug("param = " + param.getMap().toString());
	    	Integer offset = param.getInt("page");
	    	Integer limit = param.getInt("rows");
	    	String sidx = param.getString("sidx");
//	    	St
	    	String sord = param.getString("sord");
	    	String order_key = param.getString("order_key");
	    	String idx = param.getString("idx");
	    	
	    	if(offset == 0 ) offset = param.getInt("offset");
	    	if(limit == 0 ) limit = DEF_ROW_COUNT;
	    	return pageBounds(offset, limit);    		
	    }

	    public static PageBounds pageBounds(AbstractPageVoBase param) {
	    	String sidx = param.getSidx();
	    	if(sidx != null && ! sidx.isEmpty()) {
	       		Order order = new Order(sidx, param.getSord().equals("DESC") ? Direction.DESC : Direction.ASC, null);
	    		return pageBounds(param.getOffset(), param.getLimit(), order);    		
	    	} else {
	    		return pageBounds(param.getOffset(), param.getLimit());    		
	    	}
	    }

	    public static LocalDateTime getStartDateTime(LocalDate date) {
	        return LocalDateTime.of(date, MIN);
	    }

	    public static LocalDateTime getEndDateTime(LocalDate date) {
	        return LocalDateTime.of(date, MAX);
	    }


}
