package daily.common.security.bean;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.util.matcher.RequestMatcher;

import daily.common.security.service.SecuredObjectService;

public class UrlResourceMapFactoryBean implements FactoryBean<LinkedHashMap<RequestMatcher,List<ConfigAttribute>>> {
	
	@Autowired SecuredObjectService service;
	
	private LinkedHashMap<RequestMatcher,List<ConfigAttribute>> requestMap;
	
	public void init() throws Exception{
		requestMap = service.getRolesAndUrl();
	}

	@Override
	public LinkedHashMap<RequestMatcher, List<ConfigAttribute>> getObject() throws Exception {
		if(requestMap == null) {
			requestMap = service.getRolesAndUrl();
		}
		return requestMap;
	}

	@Override
	public Class<?> getObjectType() {
		return LinkedHashMap.class;
	}
	
	public boolean isSingleton() {
		return true;
	}

}
