<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasMenu="true" hasPop="true" hasImgTop="true" hasLeft="true">
			<div class="tab-list tab-layer col-2">
                <ul id="usrTab"><!-- 타입 1 : 3단 -->
                    <li class="on">
                        <a id="tab_layer_1" href="javascript:void(0);">사용자 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_2" href="javascript:void(0);">사용자 그룹 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_3" href="javascript:void(0);">Role 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_4" href="javascript:void(0);">권한 관리</a>
                    </li>
                </ul>
            </div>
            <div class="tab-wrap">
				<div id="tab_layer_1" class="tab-cont on">
					<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
                       		<h3></h3>
                       <div class="adm-searchbox">
                              <div class="form-group inline-group" style="width: 140px">
                                <div class="tit_cont">검색 조건</div>
                                <select class="form-control mh-form-control" name="search_col">
                                  <option value="ug" ${(param.search_col eq 'ug'? 'selected':'')}>그룹 ID</option>
                                  <option value="nm" ${(param.search_col eq 'nm'? 'selected':'')}>그룹명</option>
                                  <option value="id" ${(param.search_col eq 'de'? 'selected':'')}>그룹설명</option>
                                </select>
                            </div>
                            <div class="form-group inline-group" style="width: 300px">
                                <div class="tit_cont">검색 내용</div>
                                <input type="text" class="form-control mh-form-control" name="search_keyword" value="${param.search_keyword}">
                            </div>
                            <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
                     </div>
                     <div class="code-manag-btn">
					<button type="button" class="btn h34 fl" onClick="javascript:openRegUser();"><span class="btn_add"></span>사용자 등록</button>
					<button type="button" class="btn h34 cancel fr" onClick="javascript:deleteList();">사용자 삭제</button>
                     </div>
                   <div class="grid-area">
                       <!-- 그리드 해더영역 -->
                       <div class="grid-header">
						<div class="header-unit header-check" style="width:5%;">
							<a href="">
								<div class="form-check mh-form-checkbox">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck1"/>
									<span class="mh-check"></span>
									<label class="form-check-label" for="defaultCheck1" title=""></label>
								</div>
							</a>
						</div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                           <div class="header-unit" style="width:25%"><a href="">사용자 ID<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
                           <div class="header-unit" style="width:25%"><a href="">사용자명<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                           <div class="header-unit" style="width:25%"><a href="">사용자 그룹명<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                           <div class="header-unit" style="width:10%"><a href="">승인여부<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
                           <div class="header-unit" style="width:10%"><a href="">사용여부<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
                       </div>
                       <!-- //그리드 해더영역 -->
                       <!-- 그리드 컨텐츠영역 -->
                       <div class="grid-contents">
                           <!--loof-->
                           <c:forEach var="map" items="${list}" varStatus="i">
                           <div class="grid-loof-cont" ondblclick="javascript:openRegUser('${map.user_seq}')">
							<div class="cont-unit cont-check" style="width:5%"><div class="form-check mh-form-checkbox">
									<input class="form-check-input" type="checkbox" value="${map.user_seq}" name="del_code" id="del_code${i.count}">
									<span class="mh-check"></span>
									<label class="form-check-label" for="defaultCheck2" title=""></label>
								</div>
							</div>
                               <div class="cont-unit" style="width:25%">${map.id}</div>
                               <div class="cont-unit" style="width:25%">${map.name}</div>
                               <div class="cont-unit" style="width:25%">${(not empty map.group_nm? map.group_nm:'&nbsp;')}</div>
                               <c:choose>
                               	<c:when test="${map.user_status eq 'Y'}">
                               <div class="cont-unit nocut" style="width:10%">승인 완료</div>
                               	</c:when>
                               	<c:otherwise>
                               <div class="cont-unit nocut" style="width:10%;text-align:center;">
                               <a href="javascript:void(0);" onclick="javascript:regChild(${map.id_master},${map.id_board_master},'${map.ord_str}');"><span class="btn_lbl_type02">승인</span></a>
                               </div>
                               	</c:otherwise>
                               </c:choose>
                               <div class="cont-unit" style="width:10%;text-align:center;">${map.use_yn}</div>
                           </div>
                           </c:forEach>
                           <!--//loof-->
                       </div>
                       <!-- //그리드 컨텐츠영역 -->
                   </div>
                   <!--//그리드영역-->
                   <!-- paging -->
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
	                   <!-- //paging -->
			</form>
            </div>
			<div id="tab_layer_2" class="tab-cont">
        	</div>
			<div id="tab_layer_3" class="tab-cont">
            </div>
			<div id="tab_layer_4" class="tab-cont">
            </div>
    </div>
</adm:AdmBody>
<script>
$(function () {
	$("#tab_layer_1").click(function(){
		location.href="<c:url value='/adm/user/userList.do'/>?id_menu=12";
	});
	$("#tab_layer_2").click(function(){
		location.href="<c:url value='/adm/user/userGroupList.do'/>?id_menu=9";
	});
	$("#tab_layer_3").click(function(){
		location.href="<c:url value='/adm/user/roleList.do'/>?id_menu=10";
	});
	$("#tab_layer_4").click(function(){
		location.href="<c:url value='/adm/user/permissionList.do'/>?id_menu=${param.id_menu}";
	});
});
function openRegUser(key){
	if(key){
		location.href="<c:url value='/adm/user/regUser.do'/>?id_menu=${param.id_menu}&user_id="+key;
	}else{
		location.href="<c:url value='/adm/user/regUser.do'/>?id_menu=${param.id_menu}";
	}
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/adm/user/userList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function deleteList(){
	confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
}
function deleteConfirm(){
	processing();
	if($("#confirm_yn").val()=='Y'){
	    $("#confirm_yn").val('N');
		var frm =document.frm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/user/deleteUserList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
	}
}
</script>
</html>
