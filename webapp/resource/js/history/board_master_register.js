function register(){
	var params = $("#frm").serialize();
		var url = '';
		var is_insert = true;
		if(nanToStr($("#id_board_master").val())==''){
			url += contextPath+"/history/insertBoardMasterAjax.do";
		}else{
			is_insert = false;
			url += contextPath+"/history/modifyBoardMasterAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}

function initForm(){
	$("$board_nm").val('');
	$("$comment").val('');
	$("#id_board_master").val('');
	$("#ord_str").val('');
}

function delData(){
	var params = $("#frm").serialize();
	var url = contextPath+"/history/deleteBoardMasterAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}