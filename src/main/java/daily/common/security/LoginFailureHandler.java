package daily.common.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.util.UrlPathHelper;

import daily.common.util.StrUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.UserManageService;
import kr.co.daily.common.GlobalConst;


/**
 * loginAjax 처리를 위해 AuthenticationSuccessHandler를 사용자 정의한 핸들러
 * author : P.H.
 * date : 2018-04-24
 */
public class LoginFailureHandler implements AuthenticationFailureHandler  {

	private static Logger log = LoggerFactory.getLogger(LoginFailureHandler.class);
	private String loginRedirect;
	private String loginRedirectNm;
	private String defaultFailureUrl;
	private String exceptMsgNm;
	@Autowired private UserManageService usrService;
	public String getLoginRedirect() {
		return loginRedirect;
	}
	public void setLoginRedirect(String loginRedirect) {
		this.loginRedirect = loginRedirect;
	}
	public String getLoginRedirectNm() {
		return loginRedirectNm;
	}
	public void setLoginRedirectNm(String loginRedirectNm) {
		this.loginRedirectNm = loginRedirectNm;
	}
	public String getDefaultFailureUrl() {
		return defaultFailureUrl;
	}
	public void setDefaultFailureUrl(String defaultFailureUrl) {
		this.defaultFailureUrl = defaultFailureUrl;
	}
	public String getExceptMsgNm() {
		return exceptMsgNm;
	}
	public void setExceptMsgNm(String exceptMsgNm) {
		this.exceptMsgNm = exceptMsgNm;
	}
	@Override
	public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse resp,
			AuthenticationException denied) throws IOException, ServletException {
		log.info("실패!");
		String accept = req.getHeader("accept");
		String userId = req.getParameter("id");
		CommandMap map = new CommandMap();
		map.put("id_user", userId);
		CustomMap loginInfo = usrService.getUser(map);
//		String loginRedirect = req.getParameter(loginRedirectNm);
		String error = "true"; 
		String message = "로그인실패하였습니다."; 
//		if(denied instanceof 
		String ret = GlobalConst.NOT_FOUND_USER.toString();
		if(loginInfo!=null) {
			String use_yn = loginInfo.getString("use_yn", "");
			String user_status = loginInfo.getString("user_status", "");
			if(use_yn.equals("N")) {
				ret = GlobalConst.ACT_FAIL.toString();
			}else if(user_status.equals("N")){
				ret = GlobalConst.NOT_ACCEPT_USER.toString();
			}
		}
        log.debug("LoginFailureHandler by Error Code : "+ret);
		if( StrUtil.indexOf(accept, "html") > -1 ) { 
			UrlPathHelper urlPathHelper = new UrlPathHelper(); 
			String url = urlPathHelper.getOriginatingRequestUri(req);
			String redirectUrl = null;
			String context = urlPathHelper.getOriginatingContextPath(req);
		    if(!context.equals("")) {  //(TODO) 주 후 일반 사용자 로그인 실패시 이 쪽으로 오면 수정해야 함
		    	url = url.substring(context.length());
		    }
            if(url.contains("")) {
            	
            }else {
            	redirectUrl = req.getParameter(this.defaultFailureUrl); 
            }
			req.setAttribute(exceptMsgNm, denied.getMessage());
			if (redirectUrl != null) { 
				req.getRequestDispatcher(redirectUrl).forward(req,resp);
			} else { 
				redirectUrl = this.defaultFailureUrl;
				req.setAttribute(loginRedirectNm, redirectUrl);
				req.setAttribute("ret", ret);
				req.getRequestDispatcher(redirectUrl).forward(req,resp);
			} 
		} else if( StrUtil.indexOf(accept, "xml") > -1 ) { 
			resp.setContentType("application/xml"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<resp>", "<error>" , error , "</error>", "<message>" , message , "</message>", "</resp>" }); 
			PrintWriter out = resp.getWriter(); out.print(data); out.flush(); out.close(); 
		} else if( StrUtil.indexOf(accept, "json") > -1 ) { 
			resp.setContentType("application/json"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] {
					"{ \"ret\" : ",  ret , "}" }); 
			PrintWriter out = resp.getWriter(); 
			out.print(data); 
			out.flush(); 
			out.close();
		}
		
	}

}
