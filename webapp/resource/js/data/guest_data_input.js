function viewDataDetail(id_board_master,id_board,ord_str){
	if(!(id_board_master == null || id_board_master == '' || id_board_master == undefined)){
		$("#id_board_master").val(id_board_master);
	}
	if(!(id_board == null || id_board == '' || id_board == undefined)){
		$("#id_board").val(id_board);
	}
	if(!(ord_str == null || ord_str == '' || ord_str == undefined)){
		$("#ord_str").val(ord_str);
	}
	
	$("#is_insert").val('N');
	var frm = document.frm;
	frm.action=contextPath+"/data/dataDetail.do";
	frm.submit();
}

function viewDataBoard(id_board_master,id_board,ord_str){
	$("#id_board_master").val(id_board_master);
	$("#id_board").val(id_board);
	$("#ord_str").val(ord_str);
	$("#is_insert").val('N');
	var frm = document.frm;
	frm.action=contextPath+"/data/regData.do";
	frm.submit();
}

function search(){
	$("#is_more").val('N');
	doList(1);
}
function searchByCombo(){
	doList(1);
}
function searchByComboPage(that){
	$("#more_page_rows").val($(that).val());
	doList(1);
}
function search_keyword_init(){
	$("#modi_sel_ord_str").val('');
	$("#search_keyword").val('');
	$("#search_init").hide();
}
function showMoreKind(id){
	$("#id_kind").val(id);
	$("#is_more").val('Y');
	var frm = document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}
function showList(id){
	$("#id_kind").val(id);
//	$("#is_more").val('N');
	if(id==''){
		$("#is_more").val('N');
	}
	$("#page").val(1);

	var frm = document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}

function doList(page,isTreeClicked){
	$("#page").val(page);
	var frm = document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}
var cur_file_num=1;

function loadData(e,isTreeClicked,isInsert){
	if(nanToStr(e.map)!=""){
		$("#modi_id_board").val(e.map.id_board);
	}
	var html = '';
	$("#file_list_div").empty();
	if(nanToStr(e.attachList)!=''){
		$.each(e.attachList,function(i,d){
			html+='<div class="file-format-attach inline-group mb5 mt0" id="attach_'+d.id_board_file+'">';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'"><span class="'+d.file_sh_ext+'">'+d.file_ext+'</span></a> ';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'"><span class="pad01 span_cut">'+d.file_nm+'</span></a> ';
			html+='</div>';
		});
		$("#file_list_div").append(html);
	}
	$("#board_reg_type").html(e.board_path+" 조회");
}

function toDetailView(id_master, id_board_master, id_board, ord_str){
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "get");  //Post 방식
    form.setAttribute("action", contextPath+"/data/dataDetail.do?");

	var idMaster = document.createElement("input");
	idMaster.setAttribute("name", "id_master");
	idMaster.setAttribute("value", id_master);
	form.appendChild(idMaster);
	
	var idBoardMaster = document.createElement("input");
	idBoardMaster.setAttribute("name", "id_board_master");
	idBoardMaster.setAttribute("value", id_board_master);
	form.appendChild(idBoardMaster);
	
	var idBoard = document.createElement("input");
	idBoard.setAttribute("name", "id_board");
	idBoard.setAttribute("value", id_board);
	form.appendChild(idBoard);
	
	var ordStr = document.createElement("input");
	ordStr.setAttribute("name", "ord_str");
	ordStr.setAttribute("value", ord_str);
	form.appendChild(ordStr);
	
    document.body.appendChild(form);
	form.submit();
}

function toBoard(id_master, search_keyword) {
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "get");  //Post 방식
    form.setAttribute("action", contextPath+"/data/data.do?");

	var idMaster = document.createElement("input");
	idMaster.setAttribute("name", "id_master");
	idMaster.setAttribute("value", id_master);
	form.appendChild(idMaster);
	
	var searchKeyword = document.createElement("input");
	searchKeyword.setAttribute("name", "search_keyword");
	searchKeyword.setAttribute("value", search_keyword);
	form.appendChild(searchKeyword);

    document.body.appendChild(form);
	form.submit();
}