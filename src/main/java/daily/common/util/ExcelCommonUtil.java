package daily.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import daily.common.vo.CustomMap;

public class ExcelCommonUtil {
	private Workbook workbook;
	private Map<String, Object> model;
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	public ExcelCommonUtil(Map<String, Object> model,
            Workbook workbook,
            HttpServletResponse response) {
		this.setModel(model);
		this.workbook=workbook;
		this.response=response;
	}
	public void createExcel(String fileNm,List<String> headList,List<String> colmnList,List<CustomMap> bodyList) {
		setFileName(response,fileNm);
		Sheet sheet = workbook.createSheet();
		createHead(sheet,mapToHeadList(headList));
		createBody(sheet,mapToBodyList(colmnList,bodyList));
	}
	
	private void createBody(Sheet sheet, List<List<Object>> bodyList) {
		int rowSize = bodyList.size();
		for(int i=0;i<rowSize;i++) {
			createBodyRow(sheet,bodyList.get(i),i+1);
		}
		
	}
	private void createHead(Sheet sheet, List<String> headList) {
	    Row row = sheet.createRow(0);
	    for (int i = 0; i < headList.size(); i++) {
	    	Object cell = headList.get(i);
	    	String val = String.valueOf(cell);
		    row.createCell(i).setCellValue(val);
	    }		
	}
	private void createBodyRow(Sheet sheet, List<Object> cellList, int rowNum) {
		int size = cellList.size();
	    Row row = sheet.createRow(rowNum);
	 
	    for (int i = 0; i < size; i++) {
	    	Object cell = cellList.get(i);
	    	if(cell instanceof String) {
		        row.createCell(i)
	            .setCellValue((String)cellList.get(i));
	    	}else if(cell instanceof Integer || cell instanceof Double || cell instanceof Float) {
	    		double doubleVal = Double.parseDouble(cell.toString());
		        row.createCell(i).setCellValue(doubleVal);
	    	}
	    }		
	}
	private List<List<Object>> mapToBodyList(List<String> colmnList, List<CustomMap> bodyList) {
		List<List<Object>> ret = new ArrayList<List<Object>>();
		for(CustomMap data:bodyList) {
			List<Object> row = new ArrayList<Object>();
			for(String colmnNm : colmnList) {
				Object colmn = data.get(colmnNm);
				row.add(colmn);
			}
			ret.add(row);
		}
		return ret;
	}
	private List<String> mapToHeadList(List<String> headList) {
		return headList ;
	}
	private void setFileName(HttpServletResponse resp,String fileNm) {
		resp.setHeader("Content-Disposition", "attachmentvalue; filename=\""+setFileExtenstion(fileNm)+"\"");
	}

	private String setFileExtenstion(String fileNm) {
		if(workbook instanceof XSSFWorkbook) {
			fileNm+=".xlsx";
		}else if(workbook instanceof SXSSFWorkbook) {
			fileNm+=".xlsx";
		}else if(workbook instanceof HSSFWorkbook) {
			fileNm+=".xls";
		}
		return fileNm;
	}
	public Map<String, Object> getModel() {
		return model;
	}
	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

}
