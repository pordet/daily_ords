<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="${map.meta_m_nm}" />
<!DOCTYPE html>
<html lang="ko">
<meta charset="utf-8">
<c:choose>
	<c:when test="${empty param.id_meta_m}">
		<c:set var="cur_title" value="메타 등록"/>
	</c:when>
	<c:otherwise>
		<c:set var="cur_title" value="${pageTitle} 수정"/>
	</c:otherwise>
</c:choose>
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/meta_input.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/meta_reg.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span id="cur_title">${cur_title}</span>
			</div>
			<form id="listFrm" name="listFrm" method="post">
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
				<input type="hidden" name="page" id="page" value="${param.page}">
			</form>
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
					<input type="hidden" name="id_meta" id="id_meta">
					<input type="hidden" name="id_parent_meta" id="id_parent_meta">
					<input type="hidden" name="insert_yn" id="insert_yn" value="Y">
					
					<div class="adm-meta-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>메타 마스터명</th>
									<td colspan="3">
										<input type="text" id="meta_m_nm" value="${map.meta_m_nm}" disabled>
<!-- 										<button type="button" id="btnInit">검색</button> -->
									</td>
								</tr>
								<tr>
									<th>메타 이름</th>
									<td>
										<input type="text" id="meta_nm" name="meta_nm">
									</td>
									<th>메타 코드</th>
									<td>
										<input type="text" id="meta_cd" name="meta_cd">
									</td>
								</tr>
								<tr>
									<th>메타 뎁스</th>
									<td>
										<input type="text" id="meta_depth" name="meta_depth" value="1">
									</td>
									<th>메타 설명</th>
									<td>
										<input type="text" id="meta_doc" name="meta_doc">
									</td>
								</tr>
								<tr id="typ_detail">
									<th>데이터 타입</th>
									<td>
										<select name="typ" onchange="javascript:changeTyp(this);">
										<c:set var="first_typ" value="0"/>
										<c:forEach var="c" items="${typCodeList}" varStatus="i">
											<c:choose>
												<c:when test="${i.count==1}">
													<c:set var="first_typ" value="1"/>
													<option value="${c.code}" selected>${c.code_nm}</option>
												</c:when>
												<c:otherwise>
													<option value="${c.code}">${c.code_nm}</option>
												</c:otherwise>	
											</c:choose>
										</c:forEach>
										</select>
									</td>
									<th>데이터 길이 유형</th>
									<td>
										<select name="len_type">
										<c:set var="first_typ" value="0"/>
										<c:forEach var="c" items="${lenTypeList}" varStatus="i">
											<c:choose>
												<c:when test="${i.count==1}">
													<c:set var="first_typ" value="1"/>
													<option value="${c.code}" selected>${c.code_nm}</option>
												</c:when>
												<c:otherwise>
													<option value="${c.code}">${c.code_nm}</option>
												</c:otherwise>	
											</c:choose>
										</c:forEach>
										</select>
									</td>
								</tr>
								<tr id="len_detail">
								  <th>숫자형 최대 길이</th>
								  <td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="숫자형 최대길이를 입력"></td>
								</tr>								
								<tr>
									<th>엑셀 업로드 여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="excel_up_yn" value="Y" id="excel_yn1"/>
												<label for="excel_yn1">예</label>
											</div>
											<div>
												<input type="radio" name="excel_up_yn" value="N" id="excel_yn2" checked/>
												<label for="excel_yn2">아니요</label>
											</div>
										</div>
									</td>
									<th>필수 입력 여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="need_yn" value="Y" id="need_yn1" />
												<label for="need_yn1">예</label>
											</div>
											<div>
												<input type="radio" name="need_yn" value="N" id="need_yn2" checked/>
												<label for="need_yn2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th>외부 소스 여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="extern_yn" value="Y" id="extern_yn1" <c:if test="${not empty map.extern_yn or map.extern_yn eq 'Y'}">checked</c:if> />
												<label for="extern_yn1">예</label>
											</div>
											<div>
												<input type="radio" name="extern_yn" value="N" id="extern_yn2" <c:if test="${empty map.extern_yn and map.extern_yn ne 'Y'}">checked</c:if>/>
												<label for="extern_yn2">아니요</label>
											</div>
										</div>
									</td>
									<th>화면 표시 여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="list_show_yn" value="Y" id="list_yn1" <c:if test="${empty map.list_yn or map.list_yn eq 'Y'}">checked</c:if> />
												<label for="list_yn1">예</label>
											</div>
											<div>
												<input type="radio" name="list_show_yn" value="N" id="list_yn2" <c:if test="${not empty map.list_yn and map.list_yn ne 'Y'}">checked</c:if>/>
												<label for="list_yn2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr id="child_yn_tr">
									<th>사용여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="use1" <c:if test="${empty map.use_yn or map.use_yn eq 'Y'}">checked</c:if> />
												<label for="use1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="use2" <c:if test="${not empty map.use_yn and map.use_yn ne 'Y'}">checked</c:if>/>
												<label for="use2">아니요</label>
											</div>
										</div>
									</td>
									<th>하위여부</th>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="child_yn" value="Y" id="child1" />
												<label for="child1">예</label>
											</div>
											<div>
												<input type="radio" name="child_yn" value="N" id="child2" checked/>
												<label for="child2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" id="btnInit">뎁스 1인 메타 추가</button>
							<button type="button" class="btn-primary" id="btnSave">저장</button>
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" id="btnAddEquiLvlSub">동일 뎁스 메타 추가</button>
							<button type="button" class="btn-primary" id="btnAddSub">하위 메타 추가</button>
						</div>
					</div>
				</form>
					<div class="grid-area">
						<div class="grid-header">
							<table class="grid-table">
							    <thead>
							        <tr class="grid-header">
							            <th class="check-unit check-all"><input type="checkbox" id="allCheck" onclick="allCheck()"></th>
							            <th class="header-unit"><a href="#">메타 뎁스<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/> </a></th>
							            <th class="header-unit"><a href="#">순서<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/> </a></th>
							            <th class="header-unit"><a href="#">메타 코드<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></th>
							            <th class="header-unit"><a href="#">데이터 타입<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></th>
							            <th class="header-unit desc-unit"><a href="#">메타 명<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></th>
							        </tr>
							    </thead>
							    <form id="checkFrm" name="checkFrm">
							    <tbody class="grid-contents" id="meta_list">
							        <c:forEach var="map" items="${metaList}" varStatus="i">
							            <tr class="grid-loof-cont" ondblclick="javascript:regMeta('${param.id_meta_m}','${map.id_meta}')" id="metaInfo${map.id_meta}">
							                <td class="check-unit"><input type="checkbox" name="chk_id_meta" value="${map.id_meta}"></td>
							                <td class="cont-unit">${map.meta_depth}</td>
							                <td class="cont-unit">${map.ord}</td>
							                <td class="cont-unit">${map.meta_cd}</td>
							                <td class="cont-unit">${map.typ_nm}</td>
							                <td class="cont-unit desc-unit">${map.meta_nm}</td>
							            </tr>
							        </c:forEach>
							    </tbody>
							    </form>
							</table>
						</div>
						<div class="board-bottom-group">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" class="btn-danger" id="btnDelete">삭제</button>
							</div>
						</div>
					</div>
					

			</div>
		</div>
	</div>

</adm:AdmBody>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function changeTyp(that){
	var html="";
	$("#len_detail").remove();
	if($(that).val()=="${GlobalConst.DT_FLOAT}" || $(that).val()=="${GlobalConst.DT_PERCENT}"){
		html+='<tr id="len_detail"><th>숫자 최대 길이</th><td><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="소수점 포함 숫자 최대길이를 입력"></td>';
		html+='<th>소수점 최대 길이</th><td><input type="text" id="precision_len" name="precision_len" value="" placeholder="소수점 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else if($(that).val()=="${GlobalConst.DT_STR}"){
		html+='<tr id="len_detail"><th>문자열 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="문자열 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else if($(that).val()=="${GlobalConst.DT_NUM}"){
		html+='<tr id="len_detail"><th>숫자형 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="숫자형 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else{
	}
}

</script>
</html>
