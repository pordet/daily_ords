function loadData(e,isTreeClicked,isInsert){
	if(nanToStr(e.map)!=""){
		$("#modi_id_board").val(e.map.id_board);
	}
	$("#file_list_div").empty();
	var html = '';
	$("#new_file_div").empty();
	var add_in  = '<div class="form-group inline-group" id="new_file_div_1">';
		add_in += '<input type="text" id="fileName1" class="form-control mh-form-control ht30 w100p" disabled="">';
		add_in += '<div class="attach-btn">';
		add_in += '<button type="button" class="btn attach1 ht30"><span class="btn_attach"></span>첨부</button>';
		add_in += '<input type="file" id="new_file_1" name="new_file_1" data-id="1" class="file_input_hidden" onchange="javascript:add_file(this);">';
		add_in += '</div>';
		add_in += '</div>';
	$("#new_file_div").append(add_in);

	if(nanToStr(e.attachList)!=''){
		$.each(e.attachList,function(i,d){
			html+='<div class="file-format-attach inline-group" id="attach_'+d.id_board_file+'">';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'"><span class="'+d.file_sh_ext+'">'+d.file_ext+'</span></a> ';
			html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'"><span class="pad10 span_cut">'+d.file_nm+'</span></a> ';
			html+='    <a href="javascript:delAttachFile('+d.id_board_file+');"><i class="fa fa-minus-circle del-file" aria-hidden="true"></i></a>';
			html+='</div>';
		});
		if(!$("#attach_head").hasClass("mt0")){
			$("#attach_head").addClass("mt0");
		}
		$("#file_list_div").append(html);
	}else{
		$("#attach_head").removeClass("mt0");
	}
//	$("[id^=addFile_in_div_]").remove();
//	$("[id^=delFile_in_div_]").remove();
	addFile();
	if(!isTreeClicked && isInsert){
		$("#btnDelete").show();
   		if(nanToStr(e.list)!=''){
	   		var html='';
	   		list_redraw(e);
			draw_paging_by_ajax(e.paginator);
   		}

		$("#board_reg_type").html(e.board_path+" 조회/수정");
	}	
	if(!isInsert){
		$("#btnDelete").show();
		$("#board_reg_type").html(e.board_path+" 조회/수정");
   		if(nanToStr(e.list)!=''){
	   		var html='';
	   		list_redraw(e);
			draw_paging_by_ajax(e.paginator);
   		}
	}	
	if(!isTreeClicked){
		infoOpen('성공하였습니다.');
	}	
}

function list_redraw(e){
		if(e.list.length==0){
			$("#result_no").show();
			$("#result_header").hide();
			$("#result_list").hide();
			
		}else{
			$("#result_no").hide();
			$("#result_header").show();
			$("#result_list").show();
		}
		if(e.list.length>0){
			var summary = '검색결과  '+e.paginator.totalCount+' 개 중 '+e.list[0].rownum+' ~ '+e.list[e.list.length-1].rownum;
			$("#result_summary").html(summary);
		}
		$(".listboard-contents1").empty();
   			var html='<ul class="list-line">';
   		$.each(e.list,function(i,d){
   			html+='<li>';
   			html+='<div class="meta">'+d.board_path+', '+d.reg_date+'</div>';
   			html+='<div class="search-result-title"><a href="javascript:void(0);" onclick="javascript:viewData('+nanToStr(d.id_board_master)+','+d.id_board+',\''+d.ord_str+'\');">'+d.title+'</a>';
   			html+='	<div class="file-format inline-group">';
   	   		$.each(d.filelist,function(j,f){
   	   			html+='	<a href="'+contextPath+'/file/data_download.do?id_board_file='+f.id_board_file+'"><span class="'+f.file_sh_ext+'">'+f.file_ext+'</span></a>';
   	   		});
   			html+='	</div>';
   			html+='</div>';
   			html+='<div class="search-result-text txt_post">'+nanToStr(d.comment)+'</div>';
   			html+='</li>';
   		});
   		html+='</ul>';
   		$.each(e.countList,function(i,d){
   			$("#span_"+d.ord_str).html("("+d.board_cnt+")");
   		});
   		html+='</ul>';
  		
   		$(".listboard-contents1").html(html);
 //  		selInit();
}