package kr.co.daily.admin.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface AuthManageDAO {
	// resource_role 관련
	PageList<CustomMap> selectResourceAndRole(Map<String, Object> map, PageBounds pageBounds);

	CustomMap getResourceRole(Map<String, Object> map);

	CustomMap getResourceRoleByChildResource(Map<String, Object> map);

	void insertResourceRole(Map<String, Object> map);

	void modifyResourceRole(Map<String, Object> map);

	void deleteResourceRole(Map<String, Object> map);

	void deleteResourceRoleByIdRole(Map<String, Object> map);
	
	void deleteResourceRoleByIdParentResource(Map<String, Object> map);

	void deleteResourceRoleByPk(Map<String, Object> map);

	void deleteGroupRoleByIdRole(Map<String, Object> map);

	

	//group_role 테이블 관련

	void insertGroupRole(Map<String, Object> map);

	void deleteGroupRole(Map<String, Object> map);


}
