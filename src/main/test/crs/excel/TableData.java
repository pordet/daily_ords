package crs.excel;

import java.util.ArrayList;
import java.util.List;

public class TableData {
	private List<TableRow> rows;

    public TableData() {
        this.rows = new ArrayList<>();
    }

    public void addRow(TableRow row) {
        rows.add(row);
    }

    public List<TableRow> getRows() {
        return rows;
    }
}
