package crs;

import java.io.File;
import java.util.List;
import java.util.Locale;

import org.junit.Test;

import daily.common.constant.DateEnum;
import daily.common.util.StrUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CustomMap;

public class StrUtilTest {

	@Test
	public void isEqualIgnoreUpperTest() {
		String origin="Abcde";
		String target="aBcdf";
		boolean encPassword = StrUtil.isEqualIgnoreUpper(origin,target);
		System.out.println(encPassword); 
//		assertEquals(password, encPassword); 
//		assertThat(encPassword,CoreMatchers.is("{by}"));
	}
	
	@Test
	public void pageTest() {
		int RANGE_SIZES = 7;
		int page = 15;
		int range = (page-1)/RANGE_SIZES;
		int firstPage = range*RANGE_SIZES+1;
		int lastPage = (range+1)*RANGE_SIZES;
		System.out.println("firstPage="+firstPage+",lastPage="+lastPage);
	}
	@Test
	public void getPlusTimeFromToday() throws Exception {
	    String today = StringDateUtil.getPlusTimeFromToday("HH:mm:ss",DateEnum.MONTH,6);
	    System.out.println(today);
	}
	@Test
	public void getTodayTime() throws Exception {
	    System.out.println(StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS"));
	    StringBuffer sb = new StringBuffer();
	    String FILE_DOWNLOAD_PATH ="d:\\upload";
    	sb.append(FILE_DOWNLOAD_PATH);
    	CustomMap map = new CustomMap();
    	map.put("excel_path", "\\aa");
    	map.put("excel_file_nm", "bb");
	    if(!"".equals(map.getString("excel_path", "").trim())){
	    	sb.append(map.getString("excel_path", "").trim());
	    }
	    if(!"".equals(map.getString("excel_file_nm", "").trim())){
	    	sb.append(File.separator+map.getString("excel_file_nm", "").trim()+"_"+StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS"));
	    }else {
	    	sb.append(File.separator+StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS"));
	    }
	    sb.append(".xlsx");
	    System.out.println("sb:"+sb.toString());
	}

	@Test
	public void wildUrlCheckTest() {
		boolean authorized = false;
		String redirectUrl = "http://localhost:8080/ords/adm/user/regUser.do?confirm_yn=&id_menu=5&sidx=&sord=&page=&user_seq=&search_col=nm&search_keyword=";
		String url = "/user/reg**";
		if(url.contains("*")) {
			String[] pathSplit = url.split("\\*");
			for(int i=0;i<pathSplit.length;i++) {
				if(pathSplit.length==0) {
					authorized = true;
				}
				if(redirectUrl.contains(pathSplit[i])) {
					authorized = true;
				}else {
					authorized = false;
				}
			}
		}else {
			if(redirectUrl.contains(url)) {
				authorized = true;
			}
		}
		System.out.println("authorized:"+authorized);
	}
	@Test
	public void urlCheckTest() {
		boolean authorized = false;
		String redirectUrl = "http://localhost:8080/ords/adm/user/regUser.do?confirm_yn=&id_menu=5&sidx=&sord=&page=&user_seq=&search_col=nm&search_keyword=";
		String url = "/user/regUser.do";
		if(url.contains("*")) {
			String[] pathSplit = url.split("\\*");
			for(int i=0;i<pathSplit.length;i++) {
				if(pathSplit.length==0) {
					authorized = true;
				}
				if(redirectUrl.contains(pathSplit[i])) {
					authorized = true;
				}else {
					authorized = false;
				}
			}
		}else {
			if(redirectUrl.contains(url)) {
				authorized = true;
			}
		}
		System.out.println("authorized:"+authorized);
	}
	@Test
	public void getPeriodDateListByFormat() {
		String st_date = "20230814";
		String ed_date="20230820";
		String format="yyyyMMdd";
		
		List<String> dateList = StringDateUtil.getPeriodDateListByFormat(st_date,ed_date,format);
		for(String date:dateList) {
			System.out.println("date:"+date);
		}
		
	}
	@Test
	public void getQueryStr() {
		String aa = "a=b&c=d&";
		String bb = aa.substring(0,aa.lastIndexOf("&"));
		System.out.println(bb);
	}
}
