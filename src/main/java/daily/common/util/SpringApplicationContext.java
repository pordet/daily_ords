package daily.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service("SpringApplicationContext")
public class SpringApplicationContext implements ApplicationContextAware 
{
	private static ApplicationContext context;
	private Logger log = LoggerFactory.getLogger(SpringApplicationContext.class);

	public SpringApplicationContext() {
		log.info("init SpringApplicationContext");
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		SpringApplicationContext.context = context;
	}

	public static Object getBean(String beanName) {
		return context.getBean(beanName);
	}

	public static <T> T getBean(String beanName, Class<T> requiredType) {
		return context.getBean(beanName, requiredType);
	}

}