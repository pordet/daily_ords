package daily.common.exception;

public class PermissionDeniedException extends RuntimeException {

    private static final long serialVersionUID = -7049316578732519153L;

    private String message = "Permission Denied"; 
    
    public PermissionDeniedException() {
        super();
    }

    public PermissionDeniedException(final String message) {
        super(message);
        this.message = message;
    } 

    @Override
    public String getMessage() {
        return message;
    }

}
