/* **************************************************************************************
 * 	김광천(KGC)
 */
;'use strict';

/* **************************************************************************************
 * 	일반 공통 라이브러리
 */
(function($) {

	String.prototype.startsWith = function(str){
		if (this.length < str.length) { return false; }
		return this.indexOf(str) == 0;
	}

	String.prototype.endsWith = function(str){
		if (this.length < str.length) { return false; }
		return this.lastIndexOf(str) + str.length == this.length;
	}

	/**
	 * 참조: https://codepen.io/Jvsierra/pen/BNbEjW
	 */
	bfcCom = {
		getUuid : function() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
		},
		
		htmlEncode : function(value) {
			  return $('<div/>').text(value).html();
		},
		htmlDecode : function(value) {
			return $('<div/>').html(value).text();
		},

		/**
		 * obj에 대해 주어진 타입과 동일한지 체크. typeof는 object에 대한 속성 체크가 안된다.
		 * 	Usage: bfcCom.isTypeOf(obj, objType)
		 * 	(참조) https://appletree.or.kr/blog/notes/javascript-objects%EC%9D%98-%EB%91%90-%EA%B0%80%EC%A7%80-%EC%9C%A0%ED%98%95-%ED%8C%90%EB%B3%84-%EB%B0%A9%EB%B2%95/
		 */
		isTypeOf : function(obj, objType) {
			return obj.constructor == objType;
		},

	};

	bfcStr = {
			
	};
	
	bfcDate = {
		getToday : function() {
			return common.getToday();
		},
		getDayBefore : function(days) {
			return common.getDate(days);
		}
	}

	bfcJqy = {
		/**
		 * that ID에 대한 suffix를 제외한 prefix를 "#" 붙여진 상태로 리턴
		 * Usage: bfcJqy.getPrefixSelectorId(this, "suffix");
		 */
		getPrefixId : function(that, suffix) {
			var pos = $(that).attr("id").indexOf(suffix);
			var id = $(that).attr("id").substr(0, pos);
			return id;
		},
	};

	/**
	 * Ajax 파라미터 전송용 모델 작성
	 */
	$.fn.serializeObject = function() {
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name]) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};

	/**
	 * Selector DOM이 화면상에 보이는지 여부를 판단.
	 *	[false,false]:invislble, [true,true]:fully visible, [false,true]:partly vislble
	 * @param inDiv 주어진면 DIV내의 판단이고, 주어지지 않으면 Window내 기준이다.
	 * @return [false, false] (fully, partly)
	 */
	$.fn.visible = function(inDiv) {
		var $t = $(this);
		var tt = $t.offset().top, th = $t.height(), tb = tt + th;
		if(inDiv) {
			var $d = $(inDiv);
			var vt = $d.offset().top, vh = $d.height(), vb = vt+vh;
		} else {
			var $w = $(window);
			var vt = $w.scrollTop(), vh = $w.height(), vb = vt+vh;
		}
		var onFully, onPartly;
		if(tb < vt || vb < tt) {
			onFully = false; onPartly = false;	//console.log('invislble');
		} else if(vt <= tt && vb >= tb || vt >= tt && vb <= tb) {
			onFully = true; onPartly = true;	//console.log('fully visible');
		} else {
			onFully = false; onPartly = true;	//console.log('partly vislble');
		}
		return {fully: onFully, partly: onPartly};
	}

	/**
	 * Selector DOM이 화면상에 나타나도록 필요한 경우 scroll이 되게 한다.
	 *		(*1) 부분만 보이는 경우에는 이동처리
	 *		(*2) 화면에 보이지 않는 경우에는 보이게 scroll 처리한다.
	 * @param inDiv 주어진면 DIV내의 판단이고, 주어지지 않으면 Window내 기준이다.
	 */
	$.fn.showCurrentScroll = function(inDiv) {
		var onVisible = $(this).visible(inDiv);
		if(! onVisible.fully && onVisible.partly) {
			$(this)[0].scrollIntoView(false); //(*1)
		} else if(! onVisible.fully) {
			$(this)[0].scrollIntoView(true); //(*2)        		
		}
	}

	/**
	 * prifix로 시작되는 클래스명를 삭제한다.
	 * 	(*1) 클래스명 뒤에 오는 blank를 마져 지어 준다. '[ ]?' 추가
	 * 	(*2) class가 존재하지 않으면 skip
	 * 	(*3) 삭제 후 class가 빈 경우는 class를 삭제한다.
	 * 	참조: https://stackoverflow.com/questions/2644299/jquery-removeclass-wildcard
	 * @param prefix 삭제 대상 클래스명의 prefix
	 */
	$.fn.removeClassPrefix = function(prefix) {
	    var c, regex = new RegExp("(^|\\s)" + prefix + "\\S+[ ]?", 'g'); //(*1)
	    return this.each(function() {
	        c = this.getAttribute('class');
	        if(! c) return; //(*2)
	        this.setAttribute('class', c.replace(regex, ''));
	        if(! this.getAttribute('class')) $(this).removeAttr("class"); //(*3)	        	
	    });
	};

	/**
	 * preifx로 시작하는 클래스명을 얻는다. 복수이면 첫 번째 것 리턴.
	 */
	$.fn.getClassPrefix = function(prefix) {
	    var regex = new RegExp("(^|\\s)" + prefix + "\\S+", "");
	    var c = $(this).attr('class');
	    if(! c) return "";
	    var m = c.match(regex);
	    //console.log("m=" + m);
	    return m.length > 0 ? m[0].trim() : "";	// 북수개면 첫번째 것으로
	};

	/**
	 * 영역 클릭 시 그 영역내 특정부분을 제외한 영역에 대한 이벤트 처리.
	 * 	즉, 특정영역 밖에만 이벤트를 거는 경우.
	 * 	(*1) 이벤트를 제외할 sub 영역
	 * 	예, $(".hwp_sect").onExcept(".Page", "click", function(evt) {};
	 */
	$.fn.onExcept = function(selExcept, evtNm, evtHandler) {
		$(this).on(evtNm, function(evt) {
			var area = $(this).find(selExcept); //(*1)
			if(area.has(evt.target).length === 0) {
				evtHandler(evt);
			}
		});		
	};
	
	String.prototype.replaceAll = function(org, des)
	{
		return this.replace(new RegExp(org,"g"), des);
	};

	String.prototype.removeAll = function(str)
	{
		return this.replaceAll(str, "");
	};

	/**
	 * Scroll To bottom
	 */
	$.fn.bfcScrollToBottom = function() {
		$(this).animate({scrollTop: $(this).prop("scrollHeight")});
	};		

	/**
	 * 해당 영역을 화면 중앙에 위치 시킨다.
	 */
	$.fn.bfcToCenter = function() {
		var top = Math.max(0, ($(window).height() - this.outerHeight()) / 2 + $(window).scrollTop()); 
		var left = Math.max(0, ($(window).width() - this.outerWidth()) / 2 + $(window).scrollLeft());
		//console.log("[bfcToCenter] window. height=" + $(window).height() + ", width=" + $(window).width() + ", top=" + $(window).scrollTop() + ", left=" + $(window).scrollLeft());
		//console.log("[bfcToCenter] top=" + top + ", left=" + left + ", h=" + this.outerHeight() + ", w=" + this.outerWidth());
		this.css("position", "absolute");
		this.css("top", top + "px");
		this.css("left", left + "px");
		return this;
	};

	numberWithCommas = function(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

}(jQuery));

/* **************************************************************************************
 * 	Common Component 라이브러리
*/
(function($) {

	// 
	/**
	 * select에서 option을 동적으로 적용하는 경우에 사용.
	 * @see FormSelectGen.tag
	 */
	$.fn.bfcSelectUpdate = function(items, selectTitle) {
		$(this).empty();
		
		if(items != null)
		{
			$(this).attr('disabled', false);
			var arr = tmp.split(',');
			$.each(arr, function(i, v) {
				// (TODO) ':' 가 있는 경우 처리 필요
				$(this).append('<option value="' + v + '">' + v + '</option>');
			});
		} else {
			$(this).attr('disabled', true);
			$(this).append('<option value="">' + selectTitle + '</option>');
		}
		return this;
	};

	/**
	 * File Dropdown 표준처리.
	 * @param mode 미지정이면 파일 제한 없음. ("hwp": HWP 파일만 허용)
	 * @see FormFileDropdown.tag
	 */
	$.fn.bfcFileDropDown = function(mode) {
		var id = "#" + $(this).attr("id");

		switch(mode) {
		case "hwp" :
			$(id).attr("accept", ".hwp");
			$(id+"_Handler").attr("data-mode", mode);
			break;
		}

		$(id).on("change", function(e) {
			var id = "#" + $(this).attr("id");
			$(id+"_FileName").val($(this).val());
		});
		
		//	(*1) drop된 file 정보를 input file에 assign 한다.
		//	(*2) HWP 파일만 허용한다.
		//	(*3) drag-drop에 따른 style 변화 처리
		//		(*3-1) 파일 drop 범위에 들어온 경우 on
		//		(*3-2) 파일 drop 범위를 벗어난 경우 off
		//		(*3-3) 파일이 drop된 경우 on
		//		(*3-4) 다른 파일을 다시 가져오는 경우 off
		//	(*4) Mac 경우 크롬인데도 type이 blank로 와서 비교가 안된다.
		// 참조 : http://hayageek.com/drag-and-drop-file-upload-jquery
		$(id+"_Handler").on('dragenter', function(e) {
		    e.stopPropagation();
		    e.preventDefault();
		    $(this).removeClass("drop"); //(*3-4)
		}).on('dragover', function(e) {
			e.stopPropagation();
			e.preventDefault();
		    $(this).addClass("over"); //(*3-2)
		}).on('dragleave', function(e) {
			e.stopPropagation();
			e.preventDefault();
		    $(this).removeClass("over"); //(*3-1)
		}).on('drop', function(e) {
			$(this).removeClass("over");
			e.preventDefault();
			var files = e.originalEvent.dataTransfer.files;

			var mode = $(this).attr("data-mode");
			var acceptOk = true, errMsg = "";
			switch(mode) {
			case "hwp" :
				acceptOk = files[0].name.indexOf(".hwp") != -1;	//(*4) files[0].type == "application/haansofthwp";
				errMsg = "HWP 파일만 허용합니다.";
				break; //(*2)
			}

			if(acceptOk) {
				$(this).addClass("drop");  //(*3-3)
				var id = "#" + bfcJqy.getPrefixId(this, "_Handler");
				$(id).prop("files", files); //(*1)
			} else {
				bfcAlert.alertWarning(errMsg);
			}
		});
		
		return this;
	}

	/**
	 * 표준 Date Picker Control Component를 처리한다.
	 * @param val 초기값(예, "2018-04-05").
	 * 		미 지정이면 오늘(today), n or -n 면 오늘 기준으로 after or before days. 
	 * @see FormDatePicker.tag
	 * 	(*1) 기존 설정 초기값이 있는 경우 우선한다. 값이 없는 경우 파라미터 val이 유효하다.
	 *	(*2) val이 없는 경우 오늘로 default 준다.
	 *	(*3) 버튼에 입력 값이 표시되는 것을 삭제시켜 준다.
	 *	(*4) input 다음에 datepicker가 온다고 가정한다.
	 *	(*5) val()이 잘 작동되는데 실재 태그에는 들어가 있지 않다. attr("value",)로 대체 사용한다. 할수없네??
	 */
	$.fn.bfcDatePicker = function(modeVal) {
		return this.each(function() {
			var that = this;
			var $datepicker = $(this).next(); //(*4)
			var val = $(this).val(); //(*1)
			if(! val) {
				var mode = $(this).attr("data-mode");
				if(! mode) mode = modeVal ? modeVal : "today";
				if(mode == "today") val = bfcDate.getToday(); //(*2)
				else val = common.getDate(mode).startDay;
				$(this).attr("value", val);		// $(this).val(val); (*5)
			}
			$datepicker.datepicker({
				onSelect : function() {
					$(that).val(this.value);
					this.value = ''; //(*3)
				},
				defaultDate: val,
			});
		});
	}

	/**
	 * Data Picker 초기화
	 */
	bfcDataPickerInit = function(divSel) {
		var datePickerSel = divSel ? divSel + " .apDatePicker" : ".apDatePicker";
		$(datePickerSel).each(function() {
			$(this).find("input[type=text]").bfcDatePicker();
		});
	}

	/*
	$.fn.bfcMark = function(options) {
		var settings = $.extend({
			color: "#556b2f",
			backgroundColor: "white"
		}, options); 
	
		this.css("color", shade);
		return this;
	};
	*/

	/** =================================================================================
	 * 웹에디터 표준 처리 공통 함수
	 */
	$.fn.editorSetContent = function(content) {
		$(this).html(content);
		CKEDITOR.instances[this[0].name].setData( content );
	};

	$.fn.editorSyncContent = function() {
		var content = CKEDITOR.instances[this[0].name].getData();
		// IE9 이하 경우를 위해 태그를 encoding 변환시켜 준다. 다른 브라우저는 문제 없다.
		content = jQuery('<div/>').text(content).html();
        $(this).html(content);
	};

	$.fn.editorGetContent = function() {
		$(this).editorSyncContent();
		return $(this).val();
	};
	
	$.fn.insertTextAt = function(myValue) {
	    myValue = myValue.trim();
	    CKEDITOR.instances[this[0].name].insertText(myValue);
	};

	$.fn.insertHtmlAt = function(myValue) {
	    myValue = myValue.trim();
	    CKEDITOR.instances[this[0].name].insertHtml(myValue);
	};

}(jQuery));

/* **************************************************************************************
 * 	Application Component 라이브러리
*/
(function($) {
	/** =================================================================================
	 * Ajax 호출 표준 처리
	 * Usage :
	 * 		bfcAjax.ajaxFileUpload(url, form, successCallback(result));
	 * 		bfcAjax.ajaxJsonSerialize(url, formSel, successCallback(result));
	 */
	bfcAjax = {
		/**
		 * 표준 ajax 처리
		 * 	(*1) "text" 타입인 경우 json으로 변환시키려 했지만 취소. "json"사용하고 고유의 "text" 유지.
		 * 	(*2) form - FormData 이든 formId 이든 타입 체크해서 처리해 준다. 
		 * 		(*2-1) 받은 form 파라미터가 FormData 타입인 경우는 그대로 준다.
		 * 		(*2-2) String 타입인 경우는 formId로 보고 FormData를 만들어 준다.
		 * 		(*2-3) Array 타입인 경우는 Serialize 된 것으로 보고 그대로 준다.
		 * 		그외는 오류
		 */
		ajaxCall : function(dataType, url, form, successCallback, options) {
			var formData = null;
			if(bfcCom.isTypeOf(form, FormData)) {
				formData = form; //(*2-1)
			} else if(bfcCom.isTypeOf(form, Array)) {
				formData = form; //(*2-3)
			} else if(bfcCom.isTypeOf(form, Object)) {
				formData = form; //(*2-3)
			} else if(bfcCom.isTypeOf(form, String)) {
				formData = new FormData(document.getElementById(form)); //(*2-2)
			} else {
				console.log("[ERROR](callAjaxSub) Invalid parameter type. form=" + form.constructor);
				return;
			}

			// default 공통 설정
			if(! options) options = {};
			var settings = $.extend({
				url: url,
	            type: "post",
	            dataType: dataType,
	            data: formData,
				beforeSend: function(xhr) { 
					if(dataType == "json"){
						xhr.setRequestHeader("Accept", "application/json"); 
					}
				},
	            success: function(result, textStatus, jqXHR) {
	            	//var result = dataType == "text" ? JSON.parse(data) : data; //(*1)
	            	// result.ret==6은 권한체크후에 권한있는 사람만 ajax 호출을 할 수 있게 변경함
	            	if(result.ret==6){
		            	bfcAlert.alertError("권한이 없습니다.");
	            	}else{
		            	successCallback(result);
	            	}
	            },
	            error: function(jqXHR, status, error) {
	            	bfcAlert.alertError("요청처리가 실패하였습니다.", "시스템에 문의바랍니다.<br>(" + status + " : " + error + ")");
	            	console.log("[Error](callAjaxSub) status=" + status + ", error=" + error + ", url=" + url);
	            },
	            complete: function() {
	            	bfcWait.hide();
	            },
			}, options);
			
			bfcWait.show();
	        $.ajax(settings);
	        
		},

		// File Upload 용
		ajaxFileUpload : function(url, form, successCallback) {
			bfcAjax.ajaxCall("json", url, form, successCallback, {
	            processData: false,
	            contentType: false,
			});
		},

		ajaxJsonSerialize : function(url, formSel, successCallback) {
			var form = $(formSel).serializeObject();
			bfcAjax.ajaxCall("json", url, form, successCallback);
		},

		ajaxJsonForm : function(url, form, successCallback, options) {
			bfcAjax.ajaxCall("json", url, form, successCallback, options);
		},

		/**
		 * 	(*3) jQuery에 데이터 처리 안함. 기본유형 "application/x-www-form-urlencoded" 아닌 경우 false.
		 * 		(참조) http://findfun.tistory.com/382, http://lng1982.tistory.com/225
		 */
		ajaxJsonFormData : function(url, formData, successCallback, options) {
			bfcAjax.ajaxCall("json", url, formData, successCallback, $.extend({
				processData: false,
				contentType: false,
			}, options));
		},

	}

	/** =================================================================================
	 * Alert / Confirm Layer Popup 표준 처리
	 */
	bfcAlert = {
		alertError : function(msg, detail, callback) {
			LayerPopup.alertError(msg, detail, callback);
		},
		alertWarning : function(msg, detail, callback) {
			LayerPopup.alertWarning(msg, detail, callback);
		},
		alertInfo : function(msg, detail, callback) {
			LayerPopup.alertInfo(msg, detail, callback);
		},
		alertSuccess : function(msg, detail, callback) {
			LayerPopup.alertSuccess(msg, detail, callback);
		},

		confirmWarning : function(msg, detail, callback, cancelCallback) {
			LayerPopup.confirmWarning(msg, detail, callback, cancelCallback);
		},
		confirmQuestion : function(msg, detail, callback) {
			LayerPopup.confirmQuestion(msg, detail, callback, cancelCallback);
		},
	}

	/** =================================================================================
	 * 레이어 팝업 표준 처리
	 * @param loadUrl 접속URL 
	 * @param options {rotate: false, buttonInsert: false}
	 * 	(참조) http://dinbror.dk/bpopup/  http://dinbror.dk/blog/bpopup/
	 * 	(*1) 모달창 바깥 클릭시 close 이벤트 disable
	 * 	(*2) 살짝 흐리게 준다.
	 *	(*3) OK처리 callback이 정의된 경우는 OK버튼 show 처리
	 *	(*4) OK처리 callback이 정의된 경우는 Custom Event Bind 처리
	 *	(*5) 사용자 화면 처리 callback
	 *	(*6) 팝업창이 여러번 하다보면 화면 중앙에 위치하지 않는 경우가 발생한다.
	 *	 	'fixed' 옵션 주면 항상 중앙에 위치한다.
	 */
	bfcPopup = {
			bpopup : null,
			opt : {
				divPopup : "#bfcLayerPopup",		// 레이어팝업 로드 위치 Selector
				onLoadCallback : null,				// Load가 완료된 후 작업
				onOkCallback: null,					// OK 버튼 클릭 시 작업
				loadData: false,
			},

			// URL 호출 페이지레이어 팝업창인 경우.
			openUrl : function(loadUrl, options) {
				this.openSub(loadUrl, null, options);
			},
			openUrlDiv : function(loadUrl, divPopup, options) {
				this.openSub(loadUrl, divPopup, options);
			},
			
			// 내장 레이어 팝업창인 경우. 바탕화면에 팝업 HTML이 내장되어 있는 경우에 사용
			openStatic : function(divPopup, options) {
				this.openSub(null, divPopup, options);
			},

			openSub : function(loadUrl, divPopup, options) {
				var self = this, opt = this.opt;
				$.extend(opt, options);
				if(! divPopup) divPopup = opt.divPopup;
				
		    	self.bpopup = $(divPopup).bPopup({
		   	        contentContainer: divPopup,
		   	        loadUrl: loadUrl,
					loadData: opt.loadData,
		   	        modalClose: false, //(*1)
		   	        opacity: 0.4, //(*2)
		   	        positionStyle: 'fixed', //(*6)
		   	        onOpen: function() {
						if(opt.onOpen) opt.onOpen();
		   	        },
		   	        loadCallback : function() {
		   	        	if(opt.onOkCallback) {
		   					self.showOk(); //(*3)
		   					$(self.getSel("button.apBtnOk")).on("click", function(evt) { //(*4)
								opt.onOkCallback(evt);
							});
		   	        	}
		  	        	if(opt.onLoadCallback) opt.onLoadCallback(); //(*5)
		   	        },
		   	        onClose: function() {
		   	        }
		    	}, function() {
		    		//var height = $(this).children().first().height();
		    		//$(this).height(height);
		    		//console.log("kgc. height=" + height);
		    	});
			},

			close : function() {
				var self = this, opt = this.opt;
				self.bpopup.close();
			},
			divClose : function(divPopup,options) {
				var self = this, opt = this.opt;
				$.extend(opt, options);
				if(! divPopup) 
					divPopup = opt.divPopup;
//				self.bpopup.close();
				$(divPopup).bPopup().close();
			},

			getSel : function(selector) {
				return this.opt.divPopup + " " + selector;
			},
			
			showOk : function() {
				$(bfcPopup.getSel("button.apBtnOk")).show();
			},

			openUrlData : function(loadUrl, loadData, options) {
				var options = options || {};
				options.loadData = loadData;
				bfcPopup.openUrl(loadUrl, options);
			},

			openUrlFormId : function(loadUrl, formSelId, options) {
				var loadData = $(formSelId).serializeObject();
				bfcPopup.openUrlData(loadUrl, loadData, options);
			},

//			// bPopup 레이어창이 화면 중앙에 위치하지 않는 경우가 발생된다. 특히IE.
//			//	팝업화면이 화면에 보일 시간에 강제로 center 시키다.
//			//	(*1) 팝업창이 깜박임을 방지하기 위해 hide()한후 show() 한다.
//			posCenter : function(selDiv) {
//				$(selDiv).css("visibility","visible");
//				$(selDiv).show();
//				var timerId = setInterval(function () {
//					var height = $(selDiv).outerHeight(); 
//					var width = $(selDiv).outerWidth();
//					console.log("[kgc] width=" + width + ", height=" + height)
//					if(height > 200 && width > 200) {
//						$(selDiv).bfcToCenter(); //(*1)
//						clearInterval(timerId);						
//					}
//				}, 100);
//			},
		};
		
	/** =================================================================================
	 * 그리드 표준처리 라이브러리 (jqGrid)
	 * 	usage
	 * 		- (필수) colModel, [colNames,]  
	 * 		- (준 필수) sortname, sortorder (초기 소팅 설정)
	 * 		- (주요 옵션) onSelectRow, loadComplete
	 * 		- (가급적 주지 말 옵션) height(자동 계산)
	 * 		- colModel에서 cmTemplate에 default 설정된 것은 주지 말것
	 * 	controller I/F
	 * 		- 목록은 model에 "list"로 저장
	 * 		- 페이징 정보는 model에 "paginator"로 저장
	 * 	(*1) colModel의 칼럼 property의 default 값이다.
	 * 		(*1-1) 소트를 불허한다. jqGrid default는 true. => 소트기능 제공으로 true로 원복.
	 * 	(*2) 페이징 대신 scroll로 해달라는 요청
	 * 	(*3) 페이징 단위은 default 15행으로 본다.
	 * 	(*4) height 주지 않으면 자동 계산. scroll 없는  페이지 분량으로 구성위해.
	 * 		default는 행수에 32ㄹ르 곱하면 칸수 만큼 표시된다.
	 * 	(*5) 로드 완료되면 기본적인 공통처리를 수행해 준다. 주로 배경색 이다.
	 * 	(*6) 좌우 scrollbar가 생긴다. 생기지 않도록 얼마간을 빼준다. 
	 */
	bfcGrid = {
		gridList : function(selGrid, selForm, url, options) {
			var rowNum = options.rowNum ? options.rowNum : 15; //(*3)
			var height = options.height ? options.height : rowNum * 32; //(*4)
			var loadComplete = function(e) {
				bfcGrid.gridLoaded(selGrid, this); // (*5)
				if(options.loadComplete) {
					options.loadComplete(e);
				}
			}

			// (KGC:FIX)(2018.08.01) 고정size로 설정. 단지 팝업창의 grid는 기존 autowidth: true를 사용하게 한다.
			if($(selGrid).parents(".popupLayer, .popup").length > 0) {
				if(! options["autowidth"]) options["autowidth"] = true;
			} else {
				if(! options["width"]) options["width"] = 1280;
			}

			// default 공통 설정
			var settings = $.extend({
				url: url,
				postData: $(selForm).serializeObject(),
				mtype: 	"post",
				datatype: "json",
				scroll : true, // (*2)
				rowNum: rowNum,
				height: height,
				//width: '1280',		// autowidth: true,
				footerrow: false,
				userDataOnFooter: true,
				//shrinkToFit: true,
				//viewrecords: true,
				//showpage:true,
				loadComplete : loadComplete,
				cmTemplate: { // (*1)
					//sortable: false,	//(*1-1)
				},
				formatter : {
					integer : {thousandsSeparator: ","},
					number : {thousandsSeparator: ",", decimalSeparator:".", decimalPlaces: 2},
				},
	    	    jsonReader : {
	    	          root: "list",
	    	          page: "paginator.page",
	    	          total: "paginator.totalPages",
	    	          records: "paginator.totalCount",
	    	          //repeatitems: false,
	    	          //cell: "cell",
	    	          //id: "seq"
	    	    },
				//sortname: 'SEQNO',		// sidx
				//sortorder: "DESC"			// sord
			}, options ); 
			$(selGrid).jqGrid(settings);
			bfcGrid.gridResize(selGrid); //(*6)
		},

		gridResize : function(selGrid) {
			setTimeout(function(){
				$(window).trigger("resize");
			}, 20);
		},

		gridListPopup : function(selGrid, selForm, url, options) {
			this.gridList(selGrid, selForm, url, options);
			
			// 팝업인 경우는 hide상태여서 그런지 srcoll이 발생이 안된다. 0.5초 후에 realod되도록 한다.
			setTimeout(function(){
				bfcGrid.gridReload(selGrid);
			}, 500);
		},

		gridReload : function(selGrid) {
			 $(selGrid).trigger("reloadGrid");
		},

		/**
		 * 그리드 검색 키 처리 이벤트
		 */
		gridSearch : function(selGrid, selForm) {
			$(selGrid).jqGrid("setGridParam", {
				postData: $(selForm).serializeObject(),
			}).trigger("reloadGrid");
		},

		/**
		 * 그리드가 loaded 된 후에 공통 처리 작업.
		 *	(*1) 홀수 행에 배경색 설정 (CSS : oddRow)
		 *	(*2) 마우스오버 시 배경색 설정 (CSS: cursorOver)
		 *	(*3) sortable: false 인 경우 mouse default 처리하여. 여기에 checkbox등을 주는 경우 이벤트 방해가 안되도록 한다.
		 */
		gridLoaded : function(selGrid, that) {
			$(".ui-th-column-header").removeAttr("style");
			$(that).find("tr.jqgrow:odd td").addClass("oddRow"); // (*1)
			$(that).on("mouseover", ".jqgrow", function(e) { // (*2)
				$(this).children("td").addClass("cursorOver");
			}).on("mouseleave", ".jqgrow", function(e) {
				$(this).children("td").removeClass("cursorOver");
			});

			// (*3) ui-jqgrid-sortable 를 제거해야 제어가 가능하다.
			var myGrid = $(selGrid);
			var cm = myGrid[0].p.colModel;
            $.each(myGrid[0].grid.headers, function(idx, val) {
    			//console.log("kgc complete, colName=" + cm[idx].name + ", sortable=" + cm[idx].sortable);
    			if(! cm[idx].sortable) {
    				//$('div.ui-jqgrid-sortable', val.el).css({cursor: "default"});
    				$('div.ui-jqgrid-sortable', val.el).removeClass('ui-jqgrid-sortable');
    			}
            });

		},
		
		fmtPercent : function(cellValue,option,rowdata,action) {
			return cellValue ? (cellValue * 100).toFixed(0) + '%' : '';
		}
	};

	/** =================================================================================
	 * 	MathRegex 공통 라이브러리 (for MathJax)
	 */
	bfcMath = {
		/**
		 * 수식에 대한 표현으로 대체된 마크업에 대해 원본으로 복구해 준다.
		 * 	that 내에 포함된 수식을 모두 원복시켜 준다. 
		 */
		removeMathRegex : function(that) {
			$(that).find(".math-tex").each(function() {
				var tex = $(this).find("script").html();
				$(this).html("\\(" + tex + "\\)")
			});		
		},

		/**
		 * 수식 표현화된 것에 대한 원복 시킨 마크업을 얻는다.
		 * 	removeMathRegex은 원본을 건들지만 이 함수는 원본은 건들지 않고 결과만 얻는다. 
		 */
		getOriginalMathRegex : function(that) {
			var orgHtml = $(that).html()
			var $html = $("<div>").html(orgHtml);
			bfcMath.removeMathRegex($html);
			return $html.html();
		},
			
		/**
		 * 전체에 대해 수식으로 표현화 해 준다.
		 */
		replayMathRegex : function() {
			MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
		},
			
		/**
		 * 아래는 removeMathRegex() 함수와 동일한 기능을 한다.
		 * 	다만 MathJax.Hub.getAllJax() 를 사용하는 방식이다.
		 * 	차후 이 같은 패턴을 사용할 것 같아 남겨 두기로 한다.
		 * 	(참조) https://groups.google.com/forum/#!topic/mathjax-users/x6XVkwp8i0o
		 */
		removeTypeset : function() {
			var HTML = MathJax.HTML,
				jax = MathJax.Hub.getAllJax();
			for(var i = 0, m = jax.length; i < m; i++) {
				var script = jax[i].SourceElement(), 
					tex = jax[i].originalText;
				if(script.type.match(/display/)) {
					tex = "\\["+tex+"\\]";		// (AS-IS) "\\\\["+tex+"\\\\]"
				} else {
					tex = "\\("+tex+"\\)";		// (AS-IS) "\\\\("+tex+"\\\\)"
				}
				$(script).parent().html(tex);
			}
		}
	}

	/** =================================================================================
	 * Search Mark 표준처리 (using jquery.mark.js)
	 *		참조: https://jsfiddle.net/Tom_Kim/4e956by1/2/
	 *		예, $(".hwp_search_op").bfcSearchMark(".hwp_sect" [,false])
	 * @param selContent search 대상 Content 영역
	 * @param options {rotate: false, buttonInsert: false} 
	 */
	$.fn.bfcSearchMark = function(selContent, options)
	{
		var self = this;
		var opt = $.extend({
			rotate: false,					// 맨처음/맨마지막인 경우 회전여부.
			buttonInsert: false,			// 버튼 영역에 버튼 삽입 여부.
			title: "Search",
			placeholder: "검색 입력",
			tooltip: "검색구분은 space로 구분합니다."
		}, options );
		
	    var $content = $(selContent),		// the context where to search
	    	$results,						// jQuery object to save <mark> elements
	    	currentClass = "current",		// the class that will be appended to the currentfocused element
	    	offsetTop = 50,					// top offset for the jump (the search bar)
	    	currentIndex = 0;				// the current index of the focused element
	    
		if(opt.buttonInsert) {
			var html = opt.title + '<span class="count"></span> : '
				+ '<input type="text" class="search" placeholder="' + opt.placeholder + '" title="' + opt.tooltip + '">'
				+ '<button class="next" title="다음">&darr;</button>'
				+ '<button class="prev" title="이전">&uarr;</button>'
				+ '<button class="clear" title="취소">X</button>';
			$(this).html(html);
		}
																																																																																																																																																																																																
		var $input = $(this).find("input.search");
			$clearBtn = $(this).find("button.clear"),
			$prevBtn = $(this).find("button.prev"),
			$nextBtn = $(this).find("button.next");

		// the input field - Searches for the entered keyword in the specified context on input
		$input.on("input", function() {
			var searchVal = this.value;
			$content.unmark({
				done : function() {
					$content.mark(searchVal, {
						separateWordSearch: true,
						done: function() {
							$results = $content.find("mark");
							currentIndex = 0;
							putCurrentIndex(currentIndex + 1);
							jumpToMatching();
						}
					});
				}
			});
		});
		
		// clear button - Clears the search
		$clearBtn.on("click", function() {
			$content.unmark();
			$input.val("").focus();
			clearCurrentIndex();
		});
		
		// prev button and next button - Next and previous search jump to
		$nextBtn.add($prevBtn).on("click", function() {
			if($results.length) {
				currentIndex += $(this).is($prevBtn) ? -1 : 1;
				if(currentIndex < 0) {
					currentIndex = opt.rotate ? $results.length - 1 : 0;
				} else if(currentIndex > $results.length - 1) {
					currentIndex = opt.rotate ? 0 : $results.length - 1;
				}
				putCurrentIndex(currentIndex + 1);
				jumpToMatching();
			}
		});

		// 현재 검색 번째를 표시한다. currIdx = 0 이면 표시를 삭제한다.
		function putCurrentIndex(currIdx) {
			if($results.length > 0) {
				$(self).find("span.count").text(' (' + currIdx + '/' + $results.length + ')');								
			} else {
				clearCurrentIndex();
			}
		}
		function clearCurrentIndex() {
			$(self).find("span.count").empty();
		}
		
		// Jumps to the element matching the currentIndex
		function jumpToMatching() {
			if($results.length) {
				var position,
					$current = $results.eq(currentIndex);
				$results.removeClass(currentClass);
				if($current.length) {
					$current.addClass(currentClass);
					// (AS-IS) position = $current.offset().top - offsetTop;  window.scrollTo(0, position); // 윈도우기준        	
					$current.showCurrentScroll(selContent);		// (TO-BE)
				}
			}
		}
	} // end of bfcSearchMark

	/** =================================================================================
	 * 	File Upload 공통 라이브러리
	 *  Usage :
	 * 		bfcUpload.ajaxFileUpload(formId, successCallback(fileVo));
	 * 		bfcUpload.ajaxImageUpload(formData, successCallback(fileVo));
	 */
	bfcUpload = {
		ajaxFileUpload : function(formId, successCallback) {
			bfcAjax.ajaxFileUpload("bfc/utl/wed/ajaxUploadFile.do", formId, function(result) {
		        if(! result.onSuccess) {
	            	bfcAlert.alertError("파일 업로드가 실패하였습니다.");
		        	return;
		        }
		        successCallback(result.fileVo);				
			});
		},
		ajaxImageUpload : function(formData, successCallback) {
			bfcAjax.ajaxFileUpload("bfc/utl/wed/uploadImageFile.do", formData, function(result) {
		        if(! result.onSuccess) {
	            	bfcAlert.alertError("이미지 파일 업로드가 실패하였습니다.");
		        	return;
		        }
		        successCallback(result.fileVo);
			});
		},
	};

	/** =================================================================================
	 * 	Please Wait Progress-bar 공통 라이브러리
	 * 	(참조) https://www.jqueryscript.net/loading/Easy-To-Customize-jQuery-Loading-Indicator-Plugin-babypaunch-spinner-js.html
	 */
	bfcWait = {
		init : function() {
			//$("#bfcSpin").spinner();	// default
			$("#bfcSpin").spinner({		// customize
				background: "rgba(0,0,0,0.6)",
				html: "<i class='fa fa-repeat' style='color: #fff;'></i>"
			});
		},

		show : function() {
			$("#bfcSpin").show();
		},

		hide : function() {
			$("#bfcSpin").hide()
		}
	};

	/** =================================================================================
	 * 	이미지 표준 처리
	 */
	bfcImg = {
			
		/**
		 * 이미지를 자신이 속한 figure 영역 크기내에 왜곡이 없이 표시되게 한다.
		 *	(*1) figure에 대해 figure height를 원복 시킨다.
		 *	(*2) no-image인 경우는 실재 width와  height가 0이 나오므로 기존크기 유지.
		 * usage : bfcImg.imageDivResize(that [, width, height]);
		 */
		imageDivResize : function(that) {
			if(that.src.indexOf("no_images") != -1) return; // (*2)
			$(that).css({"position": "relative", "width": "auto"});	// , "height": "auto"
			var $figure = $(that).parent();
			if(that.width > $figure.width()) {
				// 가로기준
				var figHeight = $figure.height();
				$(that).css({"height": "auto", "width": ""});
				$figure.css("height", figHeight); //(*1)
				var top = parseInt(($figure.height() - that.height)/2);
				$(that).css("top", top);
			} else {
				// 세로기준
				var left = parseInt(($figure.width() - that.width)/2);
				$(that).css("left", left);
			}
			//console.log("img.width=" + that.width + ", img.height=" + that.height + ", fig.width=" + $figure.width() + ", fig.height=" + $figure.height());	//  + ", this=", this
		},
/* 일단 보류	
		imageDivResizeFix : function(that, divWidth, divHeight) {
			console.log("img.src=" + that.src + ", img.width=" + that.width + ", img.height=" + that.height);
			//$(that).css({"position": "relative", "width": "auto"});	// , "height": "auto"
			var $figure = $(that).parent();
			if(that.width > divWidth) {
				// 가로기준
				var figHeight = $figure.height();
				$(that).css({"height": "auto", "width": ""});
				$figure.css("height", figHeight); //(*1)
				var top = parseInt((divHeight - that.height)/2);
				$(that).css("top", top);
			} else {
				// 세로기준
				var left = parseInt((divWidth - that.width)/2);
				$(that).css("left", left);
			}
			console.log("img.width=" + that.width + ", img.height=" + that.height + ", fig.width=" + $figure.width() + ", fig.height=" + $figure.height());	//  + ", this=", this
		}
*/
	}

	/** =================================================================================
	 * 	웹소켓 표준 처리 라이브러리
	 */
	bfcWs = {
		socket : null,
		open : function(url, callback) {
			var wsUrl = this.getWsUrl(url);
			//console.log("[bfcWs.open] wsUrl=" + wsUrl);
		    if('WebSocket' in window) {
		    	bfcWs.socket = new WebSocket(wsUrl);
		    } else if('MozWebSocket' in window) {
		    	bfcWs.socket = new MozWebSocket(wsUrl);
		    } else {
		    	alert('Error: WebSocket is not supported by this browser.');
		        return;
		    }
		 
		    // 서버에 접속이 되면 호출되는 콜백함수
		    bfcWs.socket.onopen = function() {
		    	console.log("[bfcWs.open] Conenct Ok. wsUrl=" + wsUrl);
		    };

		    // 연결이 끊어진 경우에 호출되는 콜백함수. 재연결한다.
		    bfcWs.socket.onclose = function () {
		    	//bfcWs.open(url, callback);
		    };

			// 서버로부터 메시지를 받은 경우에 호출되는 콜백함수
		    bfcWs.socket.onmessage = function(message) {
		    	var result = JSON.parse(message.data);
		    	//console.log("[bfcWs.onmessage] 메시지 수신 Ok. result=", result);
		    	var cmd = result["__cmd__"];
		    	if(callback) callback(cmd, result);
		    };
		
		    bfcWs.socket.onerror = function(e) {
		    	console.log('Error: WebSocket connect error.');
		    };
		},

		send : function(msg) {
			bfcWs.socket.send(msg);
		},

		getWsUrl : function(url) {
			var wsUrl = (window.location.protocol == "https:" ? "wss" : "ws") + "://"
					+ window.location.host 
					+ contextPath 
					+ url;
			return wsUrl;
		},
	}

	/** =================================================================================
	 * 	표준 인쇄 처리 라이브러리
	 */
	bfcPrint = {
		doPrint : function(selId) {
			var initBody = document.body.innerHTML;
			window.onbeforeprint = function () {
				document.body.innerHTML = document.getElementById(selId).innerHTML;
			}
			window.onafterprint = function () {
				document.body.innerHTML = initBody;
			}
			window.print();
		},

		doPrintIframe : function(selId) {
			var frm = document.getElementById(selId).contentWindow;
            frm.focus();	// focus on contentWindow is needed on some ie versions
            frm.print();
		}
	};

	/** =================================================================================
	 * 	표준 파일 Export 처리 라이브러리
	 */
	bfcExport = {

		/**
		 * MS-Office Word로 export 한다. (실재 다운로드 파일은 HTML 문서이고 mime 해더만 바꾼것이다.
		 * 	Usage: bfcExport.export2Doc('hwpPrintArea', 'sample.doc')
		 * 	(KGC)
		 * 		CSS 파일은 external link 하든지 internal include 하여야 한다. external link시는 full URL로 해야 한다.
		 * 			아래 소스는 테스트위해 internal include 예제를 넣은 것인데. 현제는 주석처리했다.
		 * 		이미지는 full URL로 변환시켜 주어야 한다. 전문에서 확인 결과 잘 안된다. 연구가 좀더 필요.
		 * @See https://www.codexworld.com/export-html-to-word-doc-docx-using-javascript/
		 */
		export2Doc : function(element, filename) {
			var cssHtml = (
					"<style>"
					//+ "@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}"
					//+ "div.WordSection1 {page: WordSection1;}"
					+ "table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}"
					+ "</style>"
			);
			var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>"
				 + "<head><meta charset='utf-8'>"
				 + "<title>Export HTML To Doc</title>"
				 //+ cssHtml
				 + "</head>"
				 + "<body>";
			var postHtml = "</body>"
				+ "\</html>";

			var html = preHtml + document.getElementById(element).innerHTML + postHtml;
			var blob = new Blob(['\ufeff', html], { type: 'application/msword' });
			var url = "data:application/vnd.ms-word;charset=utf-8," + encodeURIComponent(html);	// Specify link url
			filename = filename ? filename + '.doc' : 'document.doc'; // // Specify file name
			
			var downloadLink = document.createElement("a");	// Create download link element
			document.body.appendChild(downloadLink);
			
			if(navigator.msSaveOrOpenBlob ) {
				navigator.msSaveOrOpenBlob(blob, filename);
			} else {
				downloadLink.href = url;	// Create a link to the file
				downloadLink.download = filename;	// Setting the file name
				downloadLink.click();	//triggering the function
			}
			document.body.removeChild(downloadLink);
		}
	};

}(jQuery));