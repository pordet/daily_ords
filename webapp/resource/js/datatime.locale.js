$.fn.datepicker.Constructor = Datepicker;
    var dates = $.fn.datepicker.dates = {
        en: {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today"
        },
        rw:{
	        days: ["Ku cyumweru", "Kuwa mbere", "Kuwa kabiri", "Kuwa gatatu", "Kuwa kane", "Kuwa gatanu", "Kuwa gatandatu", "Ku cyumweru"],
	        daysShort: ["Cyu", "Mbe", "Kab", "Gtu", "Kan", "Gnu", "Gnd", "Cyu"],
		    daysMin: ["Cy", "Mb", "Ka", "Gt", "Kan", "Gnu", "Gnd", "Cy"],
		    months: ["Mutarama", "Gashyantare", "Werurwe", "Mata", "Gicurasi", "Kamena", "Nyakanga", "Kanama", "Nzeli", "Ukwakira", "Ugushyingo", "Ukuboza"],
		    monthsShort: ["Mut", "Gas", "Wer", "Mat", "Gic", "Kam", "Nya", "Kan", "Nze", "Ukw", "Ugu", "Uku"],
		    today: "uyu munsi"
        },
        fr:{
	        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
	        daysShort: ["Cyu", "Mbe", "Kab", "Gtu", "Kan", "Gnu", "Gnd", "Cyu"],
		    daysMin: ["Cy", "Mb", "Ka", "Gt", "Kan", "Gnu", "Gnd", "Cy"],
		    months: ["Janvier", "février", "March", "Avril", "Mai", "Juin", "Juillet", "août", "Septembre", "Octobre", "Novembre", "décembre"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today"
        }
	};