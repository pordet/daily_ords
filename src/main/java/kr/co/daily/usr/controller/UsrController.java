package kr.co.daily.usr.controller;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import daily.common.base.AbstractControllerBase;
import daily.common.util.RandomUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.domain.User;
import kr.co.daily.usr.service.GuestService;

@Controller
public class UsrController extends AbstractControllerBase {

//	private Logger log = LoggerFactory.getLogger(UsrController.class);
	@Autowired private GuestService guestService;
	@Autowired SessionLocaleResolver localeResolver;
	
	private PasswordEncoder  encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	
    private static String SENDER_MAIL;
    
	private static String NIDA_USER_NAME;			// default
    private static String NIDA_USER_PWD;
    private static String NIDA_GET_AUTH_URL;
    //    @Value("$global['mybatis.pagination.count.per.page']")
    @Value("#{global['nida.interface.id']}")
    public void setUserName(String userName) {
    	NIDA_USER_NAME = userName;
    }
    @Value("#{global['nida.interface.pwd']}")
    public void setPasswd(String passwd) {
    	NIDA_USER_PWD = passwd;
    }
    
    @Value("#{global['nida.getcitizen.keyPhrase']}")
    public void setKeyPhrase(String keyPhrase) {
    }
    
    @Value("#{global['email.smtp.account.id']}")
	public void setRangeSize(String rangeSize) {
    	SENDER_MAIL = rangeSize;
	}
   
    @Value("#{global['nida.url.auth']}")
    public void setAuthUrl(String url) {
    	NIDA_GET_AUTH_URL = url;
    }
    @Value("#{global['nida.url.citizen']}")
    public void setUserInfo(String url) {
    }
	
	@RequestMapping("/guest/sendResetPwAjax.do")
	public String sendResetPw(HttpServletRequest req,CommandMap commandMap, Model model) throws Exception {
		String pwd = RandomUtil.randomCharAndNum(8);
		String encPassword = encoder.encode(pwd);
		commandMap.put("password", encPassword);
		CustomMap map = guestService.insertGuestReset(commandMap);
		if(map!=null) {
			model.addAttribute("cnt",1);
		}else {
			model.addAttribute("cnt", 0);
		}
		CommandMap mailMap = new CommandMap();
		mailMap.put("subject","verify Person");
		mailMap.put("ConfirmString",pwd);
		mailMap.put("senderNm","ADMIN");
		mailMap.put("senderMail",SENDER_MAIL);
		mailMap.put("receivorMail",commandMap.getString("email"));
		mailMap.put("receivorNm","applicant");
        Map<String, String> input = new HashMap<String, String>();
        input.put("ConfirmString", pwd);
		Locale locale = localeResolver.resolveLocale(req);
        input.put("mailTitle", SpringPropertiesUtil.getProperty("mail.reset.title",locale));
        input.put("mailRow1", SpringPropertiesUtil.getProperty("mail.confirm.row1",locale));
        input.put("mailRow2Co1", SpringPropertiesUtil.getProperty("mail.reset.row2.co1",locale));
        input.put("mailRow2Co2", SpringPropertiesUtil.getProperty("mail.reset.row2.co2",locale));
        input.put("mailRow3", SpringPropertiesUtil.getProperty("mail.reset.row3",locale));
        input.put("mailRow4", SpringPropertiesUtil.getProperty("mail.confirm.row4",locale));
        input.put("resetLabel", SpringPropertiesUtil.getProperty("mail.reset.lbl1",locale));
        input.put("TimeOfIssue", StringDateUtil.getDateByFormat("dd/MM/yyyy, HH:mm z",locale));
//		mailService.sendMailFromResetHtml(mailMap,input);
		model.addAttribute("info",commandMap.getMap());
		return "jsonView";
	}
	
	@RequestMapping("/guest/checkEmailAjax.do")
	public String checkEmailAjax(HttpServletRequest req,CommandMap commandMap, Model model) throws Exception {
//		String locale = commandMap.getString("locale", "");
		int befCheck = guestService.getApplicantCntByEmail(commandMap);
		if(befCheck>0) {
			model.addAttribute("ret",GlobalConst.ACT_SUCCESS);
		}else {
			model.addAttribute("ret",GlobalConst.ACT_FAIL);
		}
		return "jsonView";
	}
	
	@RequestMapping("/guest/sendMailAjax.do")
	public String checkUsr(HttpServletRequest req,CommandMap commandMap, Model model) throws Exception {
//		String locale = commandMap.getString("locale", "");
		int befCheck = guestService.getApplicantCntByEmail(commandMap);
		model.addAttribute("befCheck",befCheck);
		if(befCheck>0) {
			return "jsonView";
		}
		int loginCnt = guestService.checkTmpUsrByEmail(commandMap);
		if(loginCnt>0) {
			guestService.deleteConfirmInfo(commandMap);
		}
		String encPassword = RandomUtil.randomCharAndNum(10);
		
		commandMap.put("confirm_str", encPassword);
        //Set key values
        Map<String, String> input = new HashMap<String, String>();
		Locale locale = localeResolver.resolveLocale(req);
        input.put("ConfirmString", encPassword);
        input.put("mailTitle", SpringPropertiesUtil.getProperty("mail.confirm.title",locale));
        input.put("mailRow1", SpringPropertiesUtil.getProperty("mail.confirm.row1",locale));
        input.put("mailRow2Co1", SpringPropertiesUtil.getProperty("mail.confirm.row2.co1",locale));
        input.put("mailRow2Co2", SpringPropertiesUtil.getProperty("mail.confirm.row2.co2",locale));
        input.put("mailRow3", SpringPropertiesUtil.getProperty("mail.confirm.row3",locale));
        input.put("mailRow4", SpringPropertiesUtil.getProperty("mail.confirm.row4",locale));
        input.put("TimeOfIssue", StringDateUtil.getDateByFormat("dd/MM/yyyy, HH:mm z",locale));
		guestService.insertConfirmInfo(commandMap);
		CommandMap mailMap = new CommandMap();
		mailMap.put("subject","verify Person");
		mailMap.put("ConfirmString",encPassword);
		mailMap.put("senderNm","ADMIN");
		mailMap.put("senderMail",SENDER_MAIL);
		mailMap.put("receivorMail",commandMap.getString("email"));
		mailMap.put("receivorNm","applicant");
//		mailService.sendMailFromHtml(mailMap,input);
		loginCnt ++;
		model.addAttribute("info",commandMap.getMap());
		model.addAttribute("cnt",loginCnt);
		return "jsonView";
	}
	
	@RequestMapping("/guest/resendMailAjax.do")
	public String resendMailAjax(HttpServletRequest req,CommandMap commandMap, Model model) throws Exception {
		Locale locale = localeResolver.resolveLocale(req);
		int loginCnt = guestService.checkTmpUsrByEmail(commandMap);
		if(loginCnt>0) {
			String encPassword = RandomUtil.randomCharAndNum(10);
			commandMap.put("confirm_str", encPassword);
			guestService.updateConfirmInfo(commandMap);
			
			CommandMap mailMap = new CommandMap();
			mailMap.put("subject","resend Verity Text");
			mailMap.put("ConfirmString",encPassword);
			mailMap.put("senderNm","ADMIN");
			mailMap.put("senderMail",SENDER_MAIL);
			mailMap.put("receivorMail",commandMap.getString("email"));
			mailMap.put("receivorNm","applicant");
	        Map<String, String> input = new HashMap<String, String>();
	        input.put("ConfirmString", encPassword);
	        input.put("mailTitle", SpringPropertiesUtil.getProperty("mail.confirm.title",locale));
	        input.put("mailRow1", SpringPropertiesUtil.getProperty("mail.confirm.row1",locale));
	        input.put("mailRow2Co1", SpringPropertiesUtil.getProperty("mail.confirm.row2.co1",locale));
	        input.put("mailRow2Co2", SpringPropertiesUtil.getProperty("mail.confirm.row2.co2",locale));
	        input.put("mailRow3", SpringPropertiesUtil.getProperty("mail.confirm.row3",locale));
	        input.put("mailRow4", SpringPropertiesUtil.getProperty("mail.confirm.row4",locale));
	        input.put("TimeOfIssue", StringDateUtil.getDateByFormat("dd/MM/yyyy, HH:mm z",locale));
//			mailService.sendMailFromHtml(mailMap,input);
		}
		model.addAttribute("cnt",loginCnt);
		return "jsonView";
	}
	
	@RequestMapping("/guest/goSignUp.do")
	public String goSignUp(CommandMap commandMap, Model model) throws Exception {
		return "guest/signUp";
	}
	
	@RequestMapping("/guest/confirmUser.do")
	public String confirmUser(CommandMap commandMap, Model model) throws Exception {
		return "guest/confirmUser";
	}
	
	@RequestMapping("/guest/confirmUserAjax.do")
	public String confirmUserAjax(CommandMap commandMap, Model model) throws Exception {
		CustomMap confirmInfo = guestService.confirmTmpUsr(commandMap);
	    model.addAttribute("confirmInfo",confirmInfo);
		return "jsonView";
	}
	
	@RequestMapping("/guest/usrCheckAjax.do")
	public String usrCheckAjax(CommandMap commandMap, Model model, HttpSession session) throws Exception {
		CustomMap existApplicant = guestService.getCheckApplicatByNidNumber(commandMap);
		if(existApplicant!=null) {
			model.addAttribute("exist","Y");
		}else {
			model.addAttribute("exist","N");
		}
		return "jsonView";
	}
	@RequestMapping("/guest/authenticateUsr.do")
	public String authenticateUsr(CommandMap commandMap, Model model) throws Exception {
//		String type = commandMap.getString("type");
//		if(GlobalConst.APPLICANT_TYPE_ABOARD.equals(type)) {
//			
//		}else if(GlobalConst.APPLICANT_TYPE_ABOARD.equals(type)){
//			
//		}else if(GlobalConst.APPLICANT_TYPE_ABOARD.equals(type)){
//			
//		}else {
//			
//		}
		model.addAttribute("confirmInfo",commandMap);
		return "guest/signUp";
	}
	
	@RequestMapping("/guest/loginAction.do")
	public String loginAction(CommandMap commandMap, Model model, HttpSession session) throws Exception {
//		String pwEn = encoder.encode(commandMap.getString("pwd"));
//		CustomMap
		CustomMap loginInfo = guestService.guestLoginAction(commandMap);
		if(loginInfo==null) {
			model.addAttribute("ret",GlobalConst.NOT_FOUND_USER);
			return "redirect:/loginPage.do";
		}else {
			CustomMap pw = guestService.getGuestPassword(commandMap);
			String pwEn =pw.getString("pwd","");
			if(!pwEn.equals("")) {
				boolean compare = encoder.matches(commandMap.getString("pwd"), pwEn);
				if(!compare) {
					model.addAttribute("ret",GlobalConst.NOT_PW_MATCH);
					return "redirect:/loginPage.do";
				}
			}else {
				model.addAttribute("ret",GlobalConst.NOT_PW_MATCH);
				return "redirect:/loginPage.do";
			}
			String captcha = commandMap.getString("captcha","");
	        String answer = (String)session.getAttribute("captcha");
	        if(!captcha.equals(answer)) {
				model.addAttribute("ret",GlobalConst.NOT_CAPTCHA_MATCH);
				return "redirect:/loginPage.do";
	        }
		}
		List<GrantedAuthority> gas = new ArrayList<GrantedAuthority>();
		gas.add(new SimpleGrantedAuthority(GlobalConst.USER_AUTH));
		User usr = new User(commandMap.getString("id"), commandMap.getString("pwd"), loginInfo, gas);
		Authentication authentication  = new UsernamePasswordAuthenticationToken(usr, encoder.encode(commandMap.getString("pwd")), gas);
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
		return "redirect:/crsApplicant/myRequestList.do";
	}

	@RequestMapping("/guest/joinComplete.do")
	public String joinComplete(CommandMap commandMap, Model model) throws Exception {
		return "guest/joinComplete";
	}

	@RequestMapping(value = "/guest/getAuthAjax.do", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.POST)
	public String getAuth(CommandMap param,Model model)
	{
	    RestTemplate restTemplate = new RestTemplate();
	    StringHttpMessageConverter smc = new StringHttpMessageConverter(StandardCharsets.UTF_8);
	    List<MediaType> supportedMediaTypes = Lists.newArrayList();
		supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		smc.setSupportedMediaTypes(supportedMediaTypes );
	    HttpMessageConverter<?> stringHttpMessageConverter = smc;
        List<HttpMessageConverter<?>> httpMessageConverter = Lists.newArrayList();
        httpMessageConverter.add(stringHttpMessageConverter);
        restTemplate.setMessageConverters(httpMessageConverter); 
        restTemplate.getMessageConverters();
        
	    final HttpHeaders headers = new HttpHeaders();
	    Charset utf8 = Charset.forName("UTF-8"); 
	    MediaType mediaType = new MediaType("application", "json", utf8); 
	    headers.setContentType(mediaType);

	    String valor ="{\"username\":\""+NIDA_USER_NAME+"\", \"password\":\""+NIDA_USER_PWD+"\"}";
	    HttpEntity<String> request = new HttpEntity<String>(valor, headers);
	    
	    ResponseEntity<String> responseEntity = restTemplate.exchange(NIDA_GET_AUTH_URL, HttpMethod.POST, request, String.class);
	    HttpHeaders reponseHeaders = responseEntity.getHeaders();
	    List<String> auth = reponseHeaders.get("Authorization");
	    if(!"".equals(auth.get(0))) {
		    model.addAttribute("result", auth.get(0));
	    }
	    return "jsonView";
	}
}
