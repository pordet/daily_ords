<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="type" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="val" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<c:choose>
	<c:when test="${type eq 'A'}">
		<span class="status_mark approved">${val}</span>
    </c:when>
	<c:when test="${type eq 'P'}">
		<span class="status_mark processing">${val}</span>
    </c:when>
	<c:when test="${type eq 'R'}">
		<span class="status_mark rejected">${val}</span>
    </c:when>
	<c:when test="${type eq 'I'}">
		<span class="status_mark issued">${val}</span>
    </c:when>
	<c:when test="${type eq 'N'}">
		<span class="status_mark notapproved">${val}</span>
    </c:when>
	<c:when test="${type eq 'C'}">
		<span class="status_mark claimed">${val}</span>
    </c:when>
	<c:otherwise>
		<span class="status_mark pending">${val}</span>
	</c:otherwise>
</c:choose>
