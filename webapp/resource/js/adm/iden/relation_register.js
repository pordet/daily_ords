function register(){
	var tableData = {};
	$("table[id^='t_id_meta_m_']").each(function() {
	    var id = $(this).attr("id").replace("t_id_meta_m_", "");
	    var left = parseFloat($(this).css("left"));
	    var top = parseFloat($(this).css("top"));
	    tableData[id] = {
	        id: $(this).attr("id"),
	        left: left,
	        top: top
	    };
	});

	// SVG 요소의 width, height, left, top 값을 가져와서 컬렉션에 저장
	var svgData = {};
	$("svg[id^='svg_']").each(function() {
	    var id = $(this).attr("id");
	    var width = parseFloat($(this).attr("width"));
	    var height = parseFloat($(this).attr("height"));
	    var left = parseFloat($(this).css("left"));
	    var top = parseFloat($(this).css("top"));
	    svgData[id] = {
	        id: id,
	        width: width,
	        height: height,
	        left: left,
	        top: top
	    };
	});

	// 각 line 요소의 x1, y1, x2, y2 값을 가져와서 컬렉션에 저장
	var lineData = {};
	$("svg[id^='svg_'] line").each(function() {
	    var id = $(this).parent().attr("id");
	    var x1 = parseFloat($(this).attr("x1"));
	    var y1 = parseFloat($(this).attr("y1"));
	    var x2 = parseFloat($(this).attr("x2"));
	    var y2 = parseFloat($(this).attr("y2"));
	    lineData[id] = {
	        x1: x1,
	        y1: y1,
	        x2: x2,
	        y2: y2
	    };
	});

	// 저장된 데이터 출력
	console.log("테이블 데이터:", tableData);
	console.log("SVG 데이터:", svgData);
	console.log("라인 데이터:", lineData);
	console.log("연결 컬럼 데이터:", relColMap);
	
	var insert_yn = $("#insert_yn").val();
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/meta/insertRelationAjax.do";
	}else{
		$("#sort_order").val($("#sort_order_dis").val());
		url = contextPath+"/adm/meta/modifyRelationAjax.do";
	}
	processing();
	var requestData = {
		    tableData: tableData,
		    svgData: svgData,
		    lineData: lineData,
		    relColData: relColMap
	};

		// AJAX 요청을 보냄
	$.ajax({
	    type: "POST", // 요청 방식
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: JSON.stringify(requestData), // 전송할 데이터(JSON 형식)
	    contentType: "application/json", // 전송하는 데이터의 타입
	    success: function(e) {
	    	endProcessing();
	    },
	    error: function(e) {
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});	
}
