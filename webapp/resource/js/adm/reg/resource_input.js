$(function () {
	$("#btnDelete").click(function(){
		if($("#id_resource").val()==''){
    		alertOpen('선택된 권한이 없습니다.');
    		return;
		}else{
			confirmOpen('해당 권한을 삭제하시겠습니까?',"confirmDeleteData");
		}
	});
	$("#btnInit").click(function(){
		initForm();
	});
});
function initForm(){
	$("#cur_title").html('권한 등록');
	$("#id_resource_role").val('');
	if($("#resource_type_url").val() =='Y'){
		$("#resource_pattern").val('');
		$("#sort_order").val('');
		$("#insert_yn").val('Y');
	    $("#id_resource").val('');
	}
	if($("#resource_type_url").val() =='N'){
		var params={};
		var url = contextPath+"/adm/auth/selectUnregisterMenuAjax.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		var html='';
		    		if(nanToStr(e.menuList)==''){
		    			html+='<option value="">등록할 메뉴가 없습니다.</option>';
						$("#menu_desc").val('');
		    		}else{
			    		$.each(e.menuList,function(i,d){
			    			html+='<option value="'+d.id_menu+'" data-val="'+nanToStr(d.menu_desc)+'">메뉴 Depth : '+d.menu_depth+' / 메뉴명 : '+nanToStr(d.menu_nm)+'</option>';
							if(i==0){
								$("#menu_desc").val(nanToStr(d.menu_desc));
							}
			    		});
		    		}
					$("#tr_menu_desc").show();
		    		$("#code_id_menu").html(html);
		    		$("#sort_order").val('1');
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		
	    $("#code_id_menu option:eq(0)").attr("selected","selected");
	    $("#resource_pattern").val('');
	    //확인필요
		$("#cur_title").html('권한 등록');
		$("#insert_yn").val('Y');
	    $("#id_resource").val('');
		$("#sort_order").val('');
	}
}
function register(){
	var params = $("#frm").serialize();
	var insert_yn = $("#insert_yn").val();
	$("#sort_order").val($("#sort_order_dis").val());
//	if($("#resource_type_url").val() =='Y' && $("#doubleYn").val()=='N'){
	if($("#resource_type_url").val() =='Y'){
		if($("#resource_pattern").val()==''){
	    	alertOpen('Url을 입력하십시오.');
			return;
		}
		if($("#sort_order").val()==''){
	    	alertOpen('권한 가중치를 입력하십시오.');
			return;
		}
	}
	if($("#resource_type_url").val() =='N'){
		if($("#code_id_menu").val()==''){
	    	alertOpen('미 등록된 메뉴가 없습니다.');
			return;
		}
		if($("#sort_order").val()==''){
	    	alertOpen('권한 가중치를 입력하십시오.');
			return;
		}
	}else{
		if($.trim($("#sort_order_dis").val())!=''){
			$("#sort_order").val($.trim($("#sort_order_dis").val()));
		}
	}
	if($("#resource_type_url").val() =='Y' && $("#doubleYn").val()=='Y'){
//	if(insert_yn =='Y' && $("#resource_type_url").val() =='Y' && $("#doubleYn").val()=='Y'){
		if($("#resource_pattern").val()==''){
	    	alertOpen('Url 패턴을 등록하십시오.');
			return;
		}else{
	    	alertOpen('Url 패턴 중복체크 하십시오.');
			return;
		}
	}
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/auth/insertResourceRoleAjax.do";
	}else{
		$("#sort_order").val($("#sort_order_dis").val());
		url = contextPath+"/adm/auth/modifyResourceRoleAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		if(insert_yn =='N'){
					if(nanToStr(e.err_msg)!=''){
	    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
					}else{
	    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
					}
				}
	    	}else{
	    		if(insert_yn =='Y'){
		    		$("#insert_yn").val('N');
		    		$("#cur_title").html('권한 수정');
		    		$("#id_resource").val(e.id_resource);
		    		$("#id_resource_role").val(e.id_resource_role);
	    		}
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function checkDouble(){		
	var insert_yn = $("#insert_yn").val();
	if($("#resource_pattern").val()==''){
    	alertOpen('Url 패턴을 등록하십시오.');
    	return;
	}
//	if(insert_yn =="Y"){
		$("#doubleYn").val("Y");
		var resource_pattern = $("#resource_pattern").val();
		processing();
		$.ajax({
			url: contextPath+"/adm/auth/doubleCheckUrlAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: {resource_pattern : resource_pattern},                // HTTP 요청과 함께 서버로 보낼 데이터
	//	    data: {DocumentNumber : docNum , KeyPhrase : 'JDZytH@qDQJ^B!5wNWThYV7vkS&t*gyzHP8@s#4t^6AeM+&f_!V!&hRWySLUC-_HdrE8WDvhVm6HR&FWTyGk9#yHQP4L86yMF*eW3w*NLCbFP%wQyAv+m8q&?Ge*x@A$' },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "POST",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.cnt>0){
		    		alertOpen('기존에 사용중인 Url 패턴이 있습니다.');
		    	}else{
		    		alertOpen('사용 가능한 Url 패턴입니다.');
		    		$("#doubleYn").val('N');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
//	}
	
}
function doList(){
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/auth/permissionList.do";
	if(page!="")
		frm.page.value=page;
//	frm.action="/excelDownload.do";
	frm.submit();
}
function closeModel(){
	if($('.doc-modal:visible').length>0){
		$('body').addClass('modal-open');
	}
}
function changeType(that){
	var type = $(that).val();
	if(type=='url'){
		$("#url_pattern").show();
		$("#pattern_title").html('Url 패턴');
		$("#resource_pattern").val('');
		$("#resource_type_url").val('Y');
		$("#menu_pattern").hide();
		$("#sort_order_dis").val('');
		$("#sort_order_dis").prop("disabled",false);
	}else{
		$("#url_pattern").hide();
		$("#menu_pattern").show();
		$("#pattern_title").html('메뉴 Depth / 메뉴명');
		var params={};
		var url = contextPath+"/adm/auth/selectUnregisterMenuAjax.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		var html='';
		    		if(nanToStr(e.menuList)==''){
		    			html+='<option value="">등록할 메뉴가 없습니다.</option>';
		    		}else{
			    		$.each(e.menuList,function(i,d){
			    			html+='<option value="'+d.id_menu+'">메뉴 Depth : '+d.menu_depth+' / 메뉴명 : '+nanToStr(d.menu_nm)+'</option>';
							if(i==0){
								$("#menu_desc").val(nanToStr(d.menu_desc));
							}
			    		});
		    		}
					$("#tr_menu_desc").show();
		    		$("#code_id_menu").html(html);
		    		$("#sort_order").val('1');
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
		$("#pattern_title").html('메뉴 Depth / 메뉴명');
		$("#sort_order_dis").val(1);
		$("#sort_order_dis").prop("disabled",true);
		$("#resource_type_url").val('N');
	}
}
function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/auth/deleteResourceRoleAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else if(e.ret==8){
    			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else{
	    		$("#insert_yn").val('Y');
	    		$("#id_resource").val('');
	    		$("#id_resource_role").val('');
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}

