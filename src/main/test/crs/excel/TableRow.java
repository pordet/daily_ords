package crs.excel;

import java.util.ArrayList;
import java.util.List;

public class TableRow {
	private List<TableCell> cells;

    public TableRow() {
        this.cells = new ArrayList<>();
    }

    public void addCell(TableCell cell) {
        cells.add(cell);
    }

    public List<TableCell> getCells() {
        return cells;
    }
}
