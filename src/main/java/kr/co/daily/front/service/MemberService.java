package kr.co.daily.front.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.front.dao.MemberDAO;

@Service
public class MemberService {
	@Autowired private MemberDAO dao;

	public CustomMap getCntUserByIdUser(CommandMap map) {
		return dao.getCntUserByIdUser(map.getMap());
	}

	public void insertMember(CommandMap map) {
		dao.insertMember(map.getMap());
	}

	public CustomMap getUser(CommandMap map) {
		return dao.getUser(map.getMap());
	}

	public void updatePwd(CommandMap map) {
		dao.updatePwd(map.getMap());
	}

	public List<CustomMap> selectUserByNmAndEmail(CommandMap map) {
		return dao.selectUserByNmAndEmail(map.getMap());
	}

}
