$(function () {
	$("#btnSave").click(function(){
		if($.trim($("#menu_nm").val())==''){
			alertOpen("Menu 명을 입력하십시오.");
			return;
		}
		if($.trim($("#menu_url").val())==''){
			alertOpen("menu_url을 입력하십시오.");
			return;
		}
		if($.trim($("#sort_ord").val())==''){
			alertOpen("메뉴 순서을 입력하십시오.");
			return;
		}
		var params = jQuery("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/adm/menu/insertMenuAjax.do";
		}else{
			url = contextPath+"/adm/menu/modifyMenuAjax.do";
		}
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(insert_yn =='Y'){
		    			$("#cur_title").html('메뉴 수정');
			    		$("#trg_id_menu").val(e.trg_id_menu);
			    		$("#insert_yn").val('N');
		    		}
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	});
	$("#btnDelete").click(function(){
		confirmOpen('해당 메뉴를 삭제하시면 매뉴 관련 정보 및 하위에 존재하는 메뉴들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteData");
	});
	$("#btnInit").click(function(){
		initForm();
	});

});
function initForm(){
	$("#cur_title").html('메뉴 등록');
	$("#trg_menu_id").val('');
	$("#menu_nm").val('');
	$("#menu_url").val('');
	$("#sort_ord").val('');
    $("#menu_style_class").val('');
    $("#insert_yn").val('Y');
    $("#ra1").attr("checked",false);
    $("#ra2").attr("checked",true);
}
function doList(){
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/menu/menuList.do";
	if(page!="")
		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
function checkDouble(){
	var insert_yn = $("#insert_yn").val();
	if(insert_yn =='Y'){
		$("#doubleYn").val('Y');
		var menu_nm = $("#menu_nm").val();
		var locale="${locale}";
		processing();
		$.ajax({
			url: contextPath+"/adm/menu/menuNmCheckAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: {menu_nm : menu_nm , locale : locale},                // HTTP 요청과 함께 서버로 보낼 데이터
	//	    data: {DocumentNumber : docNum , KeyPhrase : 'JDZytH@qDQJ^B!5wNWThYV7vkS&t*gyzHP8@s#4t^6AeM+&f_!V!&hRWySLUC-_HdrE8WDvhVm6HR&FWTyGk9#yHQP4L86yMF*eW3w*NLCbFP%wQyAv+m8q&?Ge*x@A$' },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "POST",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.map){
		    		alertOpen('기존에 사용중인 ID입니다.');
		    	}else{
		    		alertOpen('사용 가능한 ID입니다.');
		    		$("#doubleYn").val('N');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
	
}
function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/menu/deleteMenuAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}


