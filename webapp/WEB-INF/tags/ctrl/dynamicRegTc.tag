<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>


<%@ attribute name="col" required="true" type="java.util.Map" %>
<%@ attribute name="curr_row_num" required="true" type="java.lang.Integer" %>		
<%@ attribute name="curr_col_num" required="true" type="java.lang.Integer" %>		
					<%-- 리턴 값 받을 변수명 --%>
		                                <c:choose>
											<c:when test="${col.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
														<input type="text" data-meta-id="${col.id_meta}" data-meta-val="" id="id_meta_${col.id_meta}" name="id_meta_${col.id_meta}" value="${col.val}" placeholder="${col.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${col.id_meta}" data-meta-val="" id="i_id_meta_${col.id_meta}" name="i_id_meta_${col.id_meta}" value="${col.val}">
															<input type="hidden" data-meta-id="${col.id_data}" data-meta-val="" id="i_id_data_${col.id_meta}" name="i_id_data_${col.id_meta}" value="${col.id_data}">
														</c:if>
											</c:when>
											<c:when test="${col.typ eq GlobalConst.DT_STR && row.len_type eq 3}">
														<textarea data-meta-id="${col.id_meta}" style="width:100%" data-meta-val="" id="id_meta_${col.id_meta}" name="id_meta_${col.id_meta}" placeholder="${col.meta_nm}을 입력하세요.">${col.val}</textarea>
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${col.id_meta}" data-meta-val="" id="i_id_meta_${col.id_meta}" name="i_id_meta_${col.id_meta}" value="${col.val}">
															<input type="hidden" data-meta-id="${col.id_data}" data-meta-val="" id="i_id_data_${col.id_meta}" name="i_id_data_${col.id_meta}" value="${col.id_data}">
														</c:if>
											</c:when>
											<c:when test="${col.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
														<input type="text" data-meta-id="${col.id_meta}" data-meta-val="" id="id_meta_${col.id_meta}" name="id_meta_${col.id_meta}" value="${col.val}" disabled placeholder="${col.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${col.id_meta}" data-meta-val="" id="i_id_meta_${col.id_meta}" name="i_id_meta_${col.id_meta}" value="${col.val}">
															<input type="hidden" data-meta-id="${col.id_data}" data-meta-val="" id="i_id_data_${col.id_meta}" name="i_id_data_${col.id_meta}" value="${col.id_data}">
														</c:if>
											</c:when>
											<c:when test="${col.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
														<input type="text" data-meta-id="${col.id_meta}" data-meta-val="" id="id_meta_${col.id_meta}" name="id_meta_${col.id_meta}" value="${col.val}" onfocusout="checkNumberFormat(this,${col.decimal_len},${col.precision_len});" placeholder="${col.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${col.id_meta}" data-meta-val="" id="i_id_meta_${col.id_meta}" name="i_id_meta_${col.id_meta}" value="${col.val}">
															<input type="hidden" data-meta-id="${col.id_data}" data-meta-val="" id="i_id_data_${col.id_meta}" name="i_id_data_${col.id_meta}" value="${col.id_data}">
														</c:if>
											</c:when>
											<c:when test="${col.typ eq GlobalConst.DT_GRP_HEAD}">
											</c:when>
											<c:otherwise>
														<input type="text" data-meta-id="${col.id_meta}" data-meta-val="" id="id_meta_${col.id_meta}" name="id_meta_${col.id_meta}" value="${col.val}" placeholder="${col.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${col.id_meta}" data-meta-val="" id="i_id_meta_${col.id_meta}" name="i_id_meta_${col.id_meta}" value="${col.val}">
															<input type="hidden" data-meta-id="${col.id_data}" data-meta-val="" id="i_id_data_${col.id_meta}" name="i_id_data_${col.id_meta}" value="${col.id_data}">
														</c:if>
											</c:otherwise>
										</c:choose>
