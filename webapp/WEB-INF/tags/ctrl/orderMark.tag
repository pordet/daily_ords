<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="val" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="span_val" required="false" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="sidx" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="sord" required="true" %>							<%-- 포맷 타입 --%>
<c:choose>
	<c:when test="${sidx eq val}">
		<c:choose>
		   <c:when test="${sord eq 'DESC'}">
		      <span class="sorting_desc glyphicon glyphicon-arrow-down">${span_val}</span>
		   </c:when>
		   <c:when test="${sord eq 'ASC'}">
		      <span class="sorting_asc glyphicon glyphicon-arrow-up">${span_val}</span>
		   </c:when>
		   <c:otherwise>
			<span>${span_val}</span>
		   </c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<span>${span_val}</span>
	</c:otherwise>
</c:choose>

