<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메타 화면 구성"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle} 관리"/>
	
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle} 관리</span>
			</div>
			
			<div class="page-content-wrap">
				<div class="dbBtnbox">
					<div class="l"></div>
					<div class="r">
						<button type="button" class="btn-danger" onClick="javascript:deleteRole();">
							${pageTitle} 삭제
						</button>
					</div>
				</div>
				
				<form name="frm" id="frm" method="post">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit">
								<a href="#">
									${pageTitle} ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									${pageTitle} 이름
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									${pageTitle} 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_meta_m}')">
									<div class="check-unit">
										<input type="checkbox" name="cb_id_meta_m" value="${map.id_meta_m}">
									</div>
									<div class="cont-unit">${map.id_meta_m}</div>
									<div class="cont-unit">${map.meta_m_nm}</div>
									<div class="cont-unit desc-unit">${map.meta_m_doc}</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function openReg(id_role){
	var frm =document.frm;
	frm.action=contextPath+"/adm/meta/regMeta.do";
	if(id_role){
		$("#id_meta_m").val(id_role);
	}else{
		$("#id_meta_m").val('');
	}
	$("#frm").submit();
}
function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/meta/metaList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
</script>
</html>
