<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="isAdmin" value="N"/>
<!DOCTYPE html>
<front:FrontHead title="">
	<script src="<c:url value='/resource/plugins/echarts/echarts.min.js'/>"></script>
</front:FrontHead>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
                 <form id="frm" name="frm" method="post">
                 <input type="hidden" name="type" value="3">
                    <div class="row" style="margin-top:-80px;">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색시작년도
<%-- 	                                  [${param.st_year }] --%>
	                                  </div>
										<select class="form-control mh-form-control" title="검색 시작년도" name="st_year" id="st_year">
										<c:if test="${empty yd}">
											<option value="">없음</option>
										</c:if>
										<c:if test="${not empty yd}">
										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}">
											<option value="${st_year}" <c:if test="${(not empty param.st_year ? param.st_year : yd.min_year) eq st_year}"> selected="selected"</c:if>>${st_year}</option>
										</c:forEach>
										</c:if>
											</select>
<!-- 										<select title="검색 종료년도" name="ed_year" id="ed_year"> -->
<%-- 										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${ed_year}" <c:if test="${ed_year eq yd.max_year}"> selected="selected"</c:if>>${ed_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 										</select> -->
	                              </div>
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색종료년도
<%-- 	                                  [${param.ed_year }] --%>
	                                  </div>
										<select class="form-control mh-form-control" title="종료년도" name="ed_year" id="ed_year">
										<c:if test="${empty yd}">
											<option value="">없음</option>
										</c:if>
										<c:if test="${not empty yd}">
										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}">
											<option value="${ed_year}" <c:if test="${(not empty param.ed_year ? param.ed_year : yd.max_year) eq ed_year}"> selected="selected"</c:if>>${ed_year}</option>
										</c:forEach>
										</c:if>
										</select>
										
	                              </div>
	                                <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">카테고리명</div>
	                                  <select class="form-control mh-form-control" id="search_category" name="id_category" onchange="javascript:changeCategory(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
	                                    		</c:if>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">국가명</div>
	                                  <select class="form-control mh-form-control" id="id_nat_area" name="id_nat_area" onchange="javascript:changeNation(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_nat_area ? 'selected':'')}>선택</option>
	                                    		</c:if>
	                                    		<c:set var="cur_nat_area" value="${(empty param.id_nat_area?firstNat.id_area:param.id_nat_area)}"/>
				                            <c:forEach var="map" items="${natList}" varStatus="i">
	                                    		<option value="${map.id_area}" ${(cur_nat_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 120px">
	                                  <div class="tit_cont">도시명</div>
	                                  <select class="form-control mh-form-control" id="id_city_area" name="id_city_area" onchange="javascript:changeCity(this);">
	                                     		<option value="" ${(empty param.id_city_area ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${cityList}" varStatus="i">
                                   		<option value="${map.id_area}" ${(param.id_city_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              <c:choose>
	                              	<c:when test="${fn:length(zoneDivList)>0}">
	                              <div class="form-group inline-group" id="zone_select_div" style="width: 160px">
	                                  <div class="tit_cont">존 단위선택</div>
	                                  <select class="form-control mh-form-control" id="id_zone" name="id_zone"">
	                                     		<option value="" ${(empty param.id_zone ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${zoneDivList}" varStatus="i">
                                   		<option value="${map.val}" ${(param.id_zone eq map.val? 'selected':'')}>${map.unit_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              	</c:when>
	                              	<c:otherwise>
	                              <div class="form-group inline-group" id="zone_select_div" style="display:none;width: 160px">
	                                  <div class="tit_cont">존 단위선택</div>
	                                  <select class="form-control mh-form-control" id="id_zone" name="id_zone" onchange="javascript:changeZone(this);">
	                                     		<option value="" ${(empty param.id_zone ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${zoneDivList}" varStatus="i">
                                   		<option value="${map.val}" ${(param.id_zone eq map.val? 'selected':'')}>${map.unit_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              	</c:otherwise>
	                              </c:choose>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
	                       		<div class="fr">
	                                <div class="form-group inline-group" style="width: 80px">
	                                  <div class="tit_cont">년도</div>
	                                  <select class="form-control mh-form-control" id="search_year" name="year">
									<c:forEach var="i" begin="0" end="${2020-2007}">
									    <c:set var="yearOption" value="${2020-i}" />
									    <option value="${yearOption}">${yearOption}</option>
									</c:forEach>	                                  
									  </select>
	                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:openAjaxInput();">등록</button>
	                            </div>
	                       </div>
	                      </div>
	                     </div>
<!-- 				<div class="searchBox"> -->
<!-- 					<div class="t"> -->
<!-- 						<table class="search"> -->
<%-- 							<colgroup> --%>
<%-- 								<col style="width:120px"> --%>
<%-- 								<col style="width:auto"> --%>
<%-- 							</colgroup> --%>
<!-- 							<tbody> -->
<!-- 								<tr> -->
<!-- 									<th scope="row">연도 선택</th> -->
<!-- 									<td> -->
<!-- 										<select title="검색 시작년도" name="st_year" id="st_year"> -->
<%-- 										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${st_year}" <c:if test="${st_year eq yd.min_year}"> selected="selected"</c:if>>${st_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 											</select> ~ -->
<!-- 										<select title="검색 종료년도" name="ed_year" id="ed_year"> -->
<%-- 										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${ed_year}" <c:if test="${ed_year eq yd.max_year}"> selected="selected"</c:if>>${ed_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 										</select> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 							</tbody> -->
<!-- 						</table> -->
<!-- 					</div> -->
<!-- 					<div class="btnBox"><button type="submit">선택조건검색</button></div> -->
<!-- 				</div> -->
	                    </form>
				<div class="dbBtnbox">
					<div class="l">
					<front:FrontNavi id_category="${param.id_category}" id_nat_area="${param.id_nat_area}" id_city_area="${param.id_city_area}" id_zone="${param.id_zone}"></front:FrontNavi>
					
					</div>
<%-- 					[${fn:length(rowList)}][${rowList}] --%>
					<div class="r">
						<a href="#none" class="midBtn btnEme" onclick="show_chart();return false">분석</a>
					</div>					
				</div>
				<div class="boardBox" tabindex="0">
					<table class="tb03" id="result_table">
                                <c:if test="${fn:length(rowList) > 0}">
						<thead>
	      					<tr>	
	      						<th class="first" colspan="${maxLvl}">항목 </th>
										<c:forEach var="year" begin="${(empty param.st_year ? yd.min_year : param.st_year)}" end="${(empty param.ed_year ? yd.max_year : param.ed_year)}">
								<th scope="col">${year}년</th>	
										</c:forEach>
							</tr>
						</thead>
								</c:if>
						<tbody>		
                                 <c:if test="${fn:length(rowList) == 0}">
                                 <tr><td>검색결과없음</td></tr>
                                 </c:if>
                                <c:if test="${fn:length(rowList) > 0}">
							<c:forEach var="row" items="${rowList}" varStatus="i">	
							<tr>
								<c:forEach var="c" items="${row.child}" varStatus="j">	
								    <c:if test="${not empty c.head}">
								    	<c:choose>
								    		<c:when test="${(not empty c.colspan || not empty c.rowspan) && c.data_type ne '3'}">
										<td class="l" colspan="${(not empty c.colspan? c.colspan:1)}" rowspan="${(not empty c.rowspan? c.rowspan:1)}"  style="text-align:left;" >
                                            <input name="check" type="checkbox" value="${c.id_meta}" id="check_${c.id_meta}">
                                            <span class="mh-check"></span>
                                            <label class="form-check-label" for="check_${c.id_meta}"></label>
										${c.unit_nm}
										</td>	 
								    		</c:when>
								    		<c:otherwise>
										<td class="l" colspan="${(not empty c.colspan? c.colspan:1)}" rowspan="${(not empty c.rowspan? c.rowspan:1)}"  style="text-align:left;padding-left:35px;" >
										${c.unit_nm}
										</td>	 
								    		</c:otherwise>
								    	</c:choose>
								    </c:if>
						   		</c:forEach>
								<c:forEach var="year" begin="${(empty param.st_year ? yd.min_year : param.st_year)}" end="${(empty param.ed_year ? yd.max_year : param.ed_year)}">
									<c:set var="isEmpty" value="false"/>
									<c:forEach var="c" items="${row.child}" varStatus="j">	
									  	<c:if test="${year eq c.year}">
											<c:set var="isEmpty" value="true"/>
											<c:choose>
												<c:when test="${c.data_type eq '3'}">
													<td class="l">
                                            		<c:forEach var="file" items="${c.addfilelist}">
	                                                    <a href="${file.file}" download="${file.file_nm}">
	                                                    <img src="<c:url value='/resource/img/ico_download.png'/>" alt="delete" style="width:28px;height:16px;"/>
	                                                    </a>  
	                                                </c:forEach>
													</td>	 
												</c:when>
												<c:otherwise>
													<td class="l">${c.val}</td>	 
												</c:otherwise>
											</c:choose>
									  	</c:if>
									</c:forEach>
									<c:if test="${isEmpty eq false}">
										<td></td>
									</c:if>
								</c:forEach>
							</tr>
							</c:forEach>
								</c:if>
							</tbody>
						</table>
				</div>
    <div class="modal fade mh-modal" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">데이터입력</h3>
                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="reg_frm" name="reg_frm">
                <input type="hidden" id="reg_id_category" name="id_category">
                <input type="hidden" id="reg_id_nat_area" name="id_nat_area">
                <input type="hidden" id="reg_id_city_area" name="id_city_area">
                <input type="hidden" id="reg_id_zone_area" name="id_zone_area">
                <input type="hidden" id="reg_year" name="year">
                <div class="modal-body">
                    <div class="row record-wrap">
                    <div class="row" style="width:733px;">
                        <div class="col-12">
                            <div class="tab-content" id="input-content">
	                           </div>
	                       </div>
	                   </div>
                    </div>
                </div>
                </form>
							<!--modal-body end -->
                <div class="modal-footer mt-20p" id="btnArea1" style="display:block;text-align:center;">
			                        <button type="button" class="btn cancel" onclick="javascript:delData();">삭제</button>
			                        <button type="button" class="btn" onclick="javascript:register(1);">저장</button>
                </div>
              </div>
    	</div>
    </div>
    <div class="modal fade mh-modal" id="chartModal" tabindex="-1" role="dialog" aria-labelledby="chartModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="chart_frm" name="chart_frm">
                <input type="hidden" id="chart_id_category" name="id_category">
                <input type="hidden" id="chart_id_nat_area" name="id_nat_area">
                <input type="hidden" id="chart_id_city_area" name="id_city_area">
                <input type="hidden" id="chart_id_zone_area" name="id_zone_area">
                <input type="hidden" id="chart_ch_id_list" name="ch_id_list">
                <input type="hidden" id="chart_st_year" name="st_year">
                <input type="hidden" id="chart_ed_year" name="ed_year">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">데이터분석</h3>
                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row record-wrap">
                    <div class="row" style="width:733px;height:500px;">
                        <div class="col-12">
                            <div class="modal-chart-area"   id="chartCont1" style="width:720px;height:475px;" >
	                           </div>
	                       </div>
	                   </div>
                    </div>
                </div>
							<!--modal-body end -->
              </div>
              </form>
    	</div>
    </div>
<script>
function changeCategory(that){
	var new_val = $(that).val();	
	if(!(new_val=="2" || new_val=="3") && $("#id_city_area").val()!=''){
		showZoneSelect(new_val,$("#id_city_area").val());
	}else{
		$("#id_zone").val('');
		$("#zone_select_div").hide();
	}
}
function changeNation(that){
	var new_val = $(that).val();
	var params = {id_parent_area:new_val};
	var url = contextPath+"/adm/data/selectAreaDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.list.length!=0){
	    			var html='<option value="">선택</option>';
		    		$.each(e.list,function(i,d){
		    			html+='<option value="'+d.id_area+'">'+d.area_nm+'</option>';
		    			$("#id_city_area").html(html);
		    		});
	    		}
	    		$("#id_zone").val('');
	    		$("#zone_select_div").hide();
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});

// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val($("#id_nat_area").val());
// 	}
// 	if($("#search_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
// 	search();
	
}
function show_chart(){
	var ids =  $("input[name=check]:checked");
	var list = [];
	$.each(ids,function(){
		list.push(this.value)
	});
	if(nanToStr(list)=='' || list.length==0){
		alertOpen('차트에 사용할 항목을 선택하십시오.');
		return;
	}
	var ch_id_list ="";
	for(var i=0;i<list.length;i++){
		if(i==0){
			ch_id_list+=list[i];
		}else{
			ch_id_list+=","+list[i];
		}
	}
	$("#chart_ch_id_list").val(ch_id_list);
	if($("#id_city_area").val()!=''){
		$("#chart_id_city_area").val($("#id_city_area").val());
		$("#chart_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#chart_id_nat_area").val($("#id_nat_area").val());
	}
	$("#chart_st_year").val($("#st_year").val());
	$("#chart_ed_year").val($("#ed_year").val());
	
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#chart_id_category").val($("#search_category").val());
	}
	var params = $("#chart_frm").serialize();
	var url = contextPath+"/drawChartAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		var dom = document.getElementById("chartCont1");
	    		var myChart = echarts.init(dom);
		    	var seriesList = [];
		    	$.each(e.series,function(i,d){
		    		seriesList.push(
	                    {
	                    	name:d.name,
	                    	type:'line',
	                    	data:d.data
	                    }
	                )
		    	});
		    	myChart.setOption({
		    	    title: {
		    	        text: ' '
//		    	        ,subtext: '数据来自网络'
		    	    },
		    	    tooltip: {
		    	        trigger: 'axis',
		    	        axisPointer: {
		    	            type: 'shadow'
		    	        }
		    	    },
		    		legend: {
		                data: e.legend
		            },
		    	    grid: {
		    	        left: '3%',
		    	        right: '4%',
		    	        bottom: '3%',
		    	        containLabel: true
		    	    },
		    	    xAxis: {
		    	        type: 'category',
		    	        data: e.xAxis,
		    	        boundaryGap: false
//		    	        boundaryGap: [0, 0.01]
		    	    },
		    	    yAxis: {
		    	        type: 'value',
		    	    },
		            series: seriesList
		            
		        });
		    	$("#chartModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function changeCity(that){
	var id_category=$("#search_category").val();
	var new_val = $(that).val();	
	if(new_val==''){
		$("#id_zone").val('');
		$("#zone_select_div").hide();
	}else{
		showZoneSelect(id_category,new_val);
	}
}

function showZoneSelect(id_category,id_area){
	var id_nat_area = $("#id_nat_area").val();
	if(!(id_category=="2" || id_category=="3")){
		var params = {id_category:id_category,id_nat_area:id_nat_area,id_city_area:id_area,id_zone:''};
		var url = contextPath+"/zoneDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		$("#id_zone").val('');
		    		$("#zone_select_div").hide();
		    	}else{
		    		if(nanToStr(e.zoneDivList) !='' && e.zoneDivList.length!=0){
		    			var html='<option value="" selected>선택</option>';
			    		$.each(e.zoneDivList,function(i,d){
			    			html+='<option value="'+d.val+'">'+d.unit_nm+'</option>';
			    		});
		    			$("#id_zone").html(html);
		    			$("#zone_select_div").show();
		    		}
		    	}
		    },
		    error : function(e){
		    }
		});
		
	}
	
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/content.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function openInput(){
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#search_category").val());
	}
	$("#reg_year").val($("#search_year").val());
	$("#previewModal").modal('show');	
}
function openAjaxInput(){
	var id_zone = nanToStr($("#id_zone").val());
	var id_category = $("#search_category").val();
	if(id_zone!='' && id_category =='1'){
		zoneAjaxInput();
	}else{
	
		if($("#id_city_area").val()!=''){
			$("#reg_id_city_area").val($("#id_city_area").val());
			$("#reg_id_nat_area").val($("#id_nat_area").val());
			if($("#id_zone").val()!=''){
				$("#reg_id_zone_area").val($("#id_zone").val());
			}
			
		}else if($("#id_nat_area").val()==''){
			alertOpen('도시, 국가를 선택하십시오.');
			return;
		}else{
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}
		if($("#search_category").val()==''){
			alertOpen('카테고리를 선택하십시오.');
			return;
		}else{
			$("#reg_id_category").val($("#search_category").val());
		}
		$("#reg_year").val($("#search_year").val());
		var params = $("#reg_frm").serialize();
		var url = contextPath+"/getDataAjax.do";
		var sel_year=$("#search_year").val();
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.rowlist.length!=0){
		    			var html="";
			    		$.each(e.rowlist,function(i,d){
			    			if(d.row_type == 'c'){
			    				var row1 = d.row_1;
			    				var row2 = d.row_2;
				    			html+='<div class="row">';
				    			html+='	<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
				    			html+='         <div class="form-group">';
				    			if(nanToStr(row1.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
				    			}
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
				    			html+='</div>';
				    			html+='</div>';
				    			if(row2!=undefined){
					    			html+='	<div class="col-6">';
					    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
					    			html+='         <div class="form-group">';
					    			if(nanToStr(row2.val)!=""){
						    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
					    			}
					    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
					    			html+='</div>';
					    			html+='</div>';
				    			}
				    			html+='</div>';
			    			}else if(d.row_type == 'f'){
			    				var row1 = d.row_1;
				    			html+='<div class="row">';
				    			html+='	<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
				    			html+='         <div class="form-group inline-group">';
				    			if(nanToStr(row1.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
				    			}
				    			html+='           	<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
				    		//	html+='<input type="text" id="fileName" class="form-control mh-form-control" disabled>';

				    			html+='           	<div class="attach-btn">';
				    			html+='           		<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
				    			html+='           		<input type="file" name="f_'+sel_year+'_'+row1.id_meta+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'m_'+sel_year+'_'+row1.id_meta+'\').value = this.value"/>';
				    			html+='           	</div>';
				    			
				    			html+='         </div>';
				    			html+='	  </div>';
				    			html+='  </div>';
				    			html+='</div>';
			    				
			    			}else{
	// 		    				html='';
			    				var child = d.child;
				    			html+='<div class="row">';
				    			html+='	<div class="col-12">';
				    			html+='	  <div class="tit_cont">'+d.unit_nm+'</div>';
				    			html+='         <div class="form-group">';
				    			html+='<div class="child_table">';
			    				$.each(child,function(i,c){
				    				var row1 = c.row_1;
				    				var row2 = c.row_2;
					    			html+='<div class="row">';
					    			html+='<div class="col-6">';
					    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
					    			html+='<div class="form-group">';
					    			if(nanToStr(row1.val)!=""){
						    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
					    			}
					    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
					    			html+='</div>';
					    			html+='</div>';
					    			if(row2!=undefined){
						    			html+='	<div class="col-6">';
						    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
						    			html+='         <div class="form-group">';
						    			if(nanToStr(row2.val)!=""){
							    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
						    			}
						    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
						    			html+='</div>';
						    			html+='</div>';
					    			}
					    			html+='</div>';
			    				});
				    			html+='</div>';
				    			html+='</div>';
				    			html+='</div>';
				    			html+='</div>';
			    			}
			    		});
		    			$("#input-content").empty();
		    			$("#input-content").append(html);
		    			$("#previewModal").modal('show');	
		    		}
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
}

function zoneAjaxInput(){
	if($("#id_zone").val()==''){
		alertOpen('존을 선택하십시오.');
		return;
	}
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#search_category").val());
	}
	$("#reg_year").val($("#search_year").val());
	var arr = $("#id_zone").val().split("_");
	var st_zone_no = arr[0];
	var ed_zone_no = arr[1];
	var year = $("#search_year").val();
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getDataAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.rowlist.length!=0){
	    			var html="";
		    		$.each(e.rowlist,function(i,d){
// 	    				var row1 = d.row_1;
// 	    				var row2 = d.row_2;
// 		    			html+='<div class="row">';
// 		    			html+='	<div class="col-6">';
// 		    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
// 		    			html+='         <div class="form-group">';
// 		    			if(nanToStr(row1.val)!=""){
// 			    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
// 		    			}
// 		    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
// 		    			html+='</div>';
// 		    			html+='</div>';
// 		    			if(row2!=undefined){
// 			    			html+='	<div class="col-6">';
// 			    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
// 			    			html+='         <div class="form-group">';
// 			    			if(nanToStr(row2.val)!=""){
// 				    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
// 			    			}
// 			    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
// 			    			html+='</div>';
// 			    			html+='</div>';
// 		    			}
// 		    			html+='</div>';
		    		});
	    			$("#input-content").empty();
	    			$("#input-content").append(html);
	    			$("#previewModal").modal('show');	
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

function register(){
	var id_zone = nanToStr($("#id_zone").val());
	if(id_zone!=''){
		var formData = new FormData($('#reg_frm')[0]);
		var url = contextPath+"/adm/data/insertFileIncludeDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+d.val+'"/>';
			    			$("#m_"+d.year+"_"+d.id_meta).append(html);
			    		});
		    		}
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
		                            var st_html='';
		                            var ed_html='<option value="">없음</option>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:center;" >'+f.unit_nm+'</td>';
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
					    	    					if(f.data_type==3){
					    	    						if(nanToStr(f.addfilelist)!=''){
					    	    							con+='<td class="l">';
			                                                var img_url = "<c:url value='/resource/img/ico_download.png'/>";
						    	    						$.each(f.addfilelist,function(idx,data){
						    	    							con+='<a href="'+data.file+'" download="'+data.file_nm+'">';
						    	    							con+='<img src="'+img_url+'" alt="delete" style="width:28px;height:16px;"/>';
						    	    							con+='</a>';
						    	    							
						    	    						})
					    	    							con+='</td>';
					    	    						}
					    	    					}else{
									    	    		con+='<td class="l">'+f.val+'</td>';
					    	    					}
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td></td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
					    	infoOpen('성공하였습니다.');
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
		
	}else{
		if($("#id_city_area").val()!=''){
			$("#reg_id_city_area").val($("#id_city_area").val());
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}else if($("#id_nat_area").val()==''){
			alertOpen('도시, 국가를 선택하십시오.');
			return;
		}else{
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}
		if($("#search_category").val()==''){
			alertOpen('카테고리를 선택하십시오.');
			return;
		}else{
			$("#reg_id_category").val($("#search_category").val());
		}
		$("#reg_year").val($("#search_year").val());
		var params = jQuery("#reg_frm").serialize();
		var url = contextPath+"/adm/data/insertDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+d.val+'"/>';
			    			$("#m_"+d.year+"_"+d.id_meta).append(html);
			    		});
		    		}
			    	infoOpen('성공하였습니다.');
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
		                            var st_html='';
		                            var ed_html='<option value="">없음</option>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:center;" >'+f.unit_nm+'</td>';
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
								    	    		con+='<td class="l">'+f.val+'</td>';
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td></td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
	}
}
function delData(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('존, 도시, 국가를 선택하십시오.');
		return;
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}
	var params = jQuery("#reg_frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.year+"_"+d.id_meta_meta).remove();
	    		});
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
		    	var re_param = $("#frm").serialize();
		    	var re_url = contextPath+"/contentAjax.do";
		    	$.ajax({
		    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    	    type: "POST", // HTTP 요청 방식(GET, POST)
		    	    data : re_param,
		    	    dataType: "json" ,
		    	    success : function(e){
		    	    	var con = "";
                        var st_html='';
                        var ed_html='<option value="">없음</option>';
	    	    		if(nanToStr(e.yd)==''){
		    	    		con+='<tbody>';
		    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
		    	    		$("#st_year").html('<option value="">없음</option>');
		    	    		$("#ed_year").html('<option value="">없음</option>');
	    	    		}else{
		    	    			
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<tr>';
		    	    			$.each(e.rowList,function(i,d){
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:center;" >'+f.unit_nm+'</td>';
			    	    				}
			    	    			});
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
			    	    				var isEmpty =false;
				    	    			$.each(d.child,function(j,f){
				    	    				if(year == f.year){
				    	    					isEmpty = true;
							    	    		con+='<td class="l">'+f.val+'</td>';
				    	    				}
				    	    			});
				    	    			if(isEmpty ==false){
						    	    		con+='<td></td>';
				    	    			}
			    	    			}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}
		    	    },
		    	    error : function(e){
		    	    	endProcessing();
		    	    	if(e.status=='403'){
		    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	    	}else if(e.status=='401'){
		    			    alertOpen('접근 권한이 없습니다.');
		    	    	}else{
		    		    	alertOpen('실패하였습니다.');
		    	    	}
		    	    }
		    	});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
</script>
</front:FrontBody>
</html>