<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Search</title>
  <%@ include file="./include/_header.jspf" %>
  <link rel="stylesheet" href="<c:url value='/resource/navi_map/bootstrap-3.3.7-dist/css/bootstrap.min.css'/>">
   
  <!-- jstree style sheet -->
  <link rel="stylesheet" href="<c:url value='/resource/navi_map/jstree/default/style.min.css'/>">
  
  <link rel="stylesheet" href="<c:url value='/resource/navi_map/css/common_style_sheet.css'/>">
  <link rel="stylesheet" href="<c:url value='/resource/navi_map/css/map_comm.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/css/traf_front.css'/>">
    <!-- traf SVG -->
    <link rel="stylesheet" href="<c:url value='/resource/css/traf_svg.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/css/table.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/navi_map/css/map_slider.css'/>">

</head>
<body>
	<div class="content_container">
		<div class="navbar navbar-default search_bar_area">
			<div class="navbar_front">
				<div class="navbar_sign">
					<span class="navbar_sign_logo"></span>
					<!-- 
					<div class="navbar_sign_title">Data navigation</div>
					 -->
					<div class="navbar_sign_title">Data map</div>
				</div>
				<ul class="nav navbar-nav display_switcher_area">
		        	<li class="active">
		        		<a class="display_switcher network_display label_btn" href="javascript:void(0);" onclick="moveParent();">
		        			<span class="nav_map_icon"></span>
		        			글로벌 교통 DB 컨텐츠
		        		</a>
		        		<a class="display_switcher network_display nav_map_icon icon_btn"></a>
		        	</li>
		        	<!-- 해수부관련
		        	<li>
		        		<a class="display_switcher explorer_display label_btn">
		        			<span class="glyphicon glyphicon-list" aria-hidden="true" style="margin: 0 0.5rem 0 0;"></span>
		        			상세목록
		        		</a>
		        		<a class="display_switcher explorer_display nav_dep_icon icon_btn"></a>
		        	</li>
		        	-->
		        </ul>
					<div class="category_bar_area">
						<table>
							<tr>
								<td>
									<button id="category_btn_0" title="카테고리">
										카테고리
									</button>
								</td>
						<c:forEach var="cate" items="${cateList}">
								<td>
							<button id="category_btn_${cate.id_category}" title="${cate.code_nm}" onclick="startSearch(${cate.id_category});">
								${cate.code_nm}
							</button>
								</td>
						</c:forEach>
							</tr>
						</table>
					</div>
			</div>
		</div>
		
		<div class="display_area">
			<div class="network_layer">
				<div class="network_wrapper">
					<!-- 컨트롤바 영역 : 카테고리 영역 -->
					<!-- 컨트롤바 영역 : 영역에 맞춤/확대/축소 -->
					<div class="network_control_bar_area">
						<button id="network_fit_button" title="화면에 맞춤">
							<span class="resize_fit_icon"></span>
						</button>
						<button id="network_zoom_in" title="확대">
							<span class="resize_zoom_in_icon"></span>
						</button>
						<button id="network_zoom_out" title="축소">
							<span class="resize_zoom_out_icon"></span>
						</button>
					</div>
					
					<!-- graph영역 -->
					<div class="svg_container">
						<svg id="graph_area"></svg>
					</div>
					
				</div>
			</div>
			<!-- slide doc -->
			<div class="slide_doc_area" id="zoneDetailModal">
				<div class="slide_doc_layout">
					<div class="doc_button_area">
						<span id="doc_area_tile" class="doc_title"></span>
						<div class="doc_btn_group">
							<button class="hide_button hide_right"></button>
							<button class="close_button"></button>
						</div>
					</div>
					<div class="doc_controller">
	                 	<form id="frm" name="frm" method="post">
			                <input type="hidden" id="frm_id_area" name="id_area">
			                <input type="hidden" id="frm_id_category" name="id_category">
	                    <div class="row">
	                        <div class="col-12">
		                         	<div class="adm-searchbox">
		                                <div class="form-group inline-group" style="width: 100px">
		                                  <div class="tit_cont">검색시작년도
	<%-- 	                                  [${param.st_year }] --%>
		                                  </div>
											<select class="form-control mh-form-control" title="검색 시작년도" name="st_year" id="st_year">
											</select>
		                              	</div>
		                                <div class="form-group inline-group" style="width: 100px">
		                                  <div class="tit_cont">검색종료년도
		                                  </div>
											<select class="form-control mh-form-control" title="종료년도" name="ed_year" id="ed_year">
											</select>
											
		                              	</div>
			                              <button type="button" class="btn h34 btn_r" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
		                       		</div>
		                    </div>
		                </div>
		                </form>
						<div class="dbBtnbox">
							<div class="l">
							</div>
		<%-- 					[${fn:length(rowList)}][${rowList}] --%>
							<div class="r">
								<a href="#none" class="midBtn btnEme" onclick="show_chart();return false">분석</a>
							</div>					
						</div>
						<div class="naviboardBox" tabindex="0">
							<table class="tb03" id="result_table">
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="slide_chart_area" id="slideChartModal">
				<div class="slide_doc_layout">
					<div class="doc_button_area">
						<span id="doc_area_tile" class="doc_title">데이터 분석</span>
						<div class="doc_btn_group">
<!-- 							<button class="hide_button hide_right"></button> -->
							<button class="close_button"></button>
						</div>
					</div>
					<div class="doc_chart_controller">
	        			<form id="chart_frm" name="chart_frm">
			                <input type="hidden" id="chart_id_category" name="id_category">
			                <input type="hidden" id="chart_id_nat_area" name="id_nat_area">
			                <input type="hidden" id="chart_id_city_area" name="id_city_area">
			                <input type="hidden" id="chart_id_zone_area" name="id_zone_area">
			                <input type="hidden" id="chart_ch_id_list" name="ch_id_list">
			                <input type="hidden" id="chart_st_year" name="st_year">
			                <input type="hidden" id="chart_ed_year" name="ed_year">
		                </form>
						<div class="naviboardBox" tabindex="0" id="chartCon">
                            <div class="modal-chart-area"   id="chartCont1" style="width:720px;height:475px;" ></div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>		

	<!-- Modal -->
	<div class="modal fade" id="_alert_window" tabindex="-1" role="dialog" aria-labelledby="_alert_title" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" id="_alert_dialog"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="_alert_close" data-dismiss="modal" style="-moz-user-focus: normal">Close</button>
				</div>
			</div>
		</div>
	</div>
</body>

<script charset="UTF-8" src="<c:url value='/resource/navi_map/bootstrap-3.3.7-dist/js/bootstrap.min.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/pdfObject/pdfobject.min.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/d3/d3.min.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/jstree/jstree.js'/>"></script>

<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/common.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/explorer.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/document_loader2.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/area_info.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/plugins/echarts/echarts.min.js'/>"></script>
<script charset="UTF-8" src="<c:url value='/resource/js/comm_ords.js'/>"></script>

<!-- 
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/graph_drawer_add_usecase_dummy13.js'/>"></script>
-->
<!-- 회전 시 시간차로 인한 잔상제거, 코드 간략화
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/graph_drawer_add_usecase_dummy14.js'/>"></script>
-->
<!-- Drag : node 요소 g tag로 통합. -->
<script charset="UTF-8" src="<c:url value='/resource/navi_map/js/graph_drawer_1.js'/>"></script>
<script charset="UTF-8">
// 초기값 =====================================================================================

var _network_dummy1 = {
	"nodes":[
		{"id":"_root_key","title":"검색어","cate":"1","type":"keyword","group":"keyword","y":0,"x":0},
		/*
		{"id":"기관","title":"기관","cate":"2","type":"department","group":"depGroup","y":-150,"x":-150},
		{"id":"시스템","title":"시스템","cate":"2","source":"_root_key","type":"system","group":"sysGroup","y":0,"x":(150*1.5)},
		{"id":"개방데이터","title":"개방데이터","cate":"2","source":"_root_key","type":"dataset","group":"dataSetGroup","y":150,"x":-150},
		*/
		
		{"id":"기관","title":"기관","cate":"2","type":"department","group":"depGroup","y":-150,"x":-150},
		{"id":"시스템","title":"시스템","cate":"2","source":"_root_key","type":"system","group":"sysGroup","y":150,"x":150},
		{"id":"개방데이터","title":"개방데이터","cate":"2","source":"_root_key","type":"dataset","group":"dataSetGroup","y":150,"x":-150},
		{"id":"UseCase","title":"UseCase","cate":"2","source":"_root_key","type":"useCase","group":"useGroup","y":-150,"x":150}
		
	],
	"links":[
		{"source":"_root_key","target":"기관"},
		{"source":"_root_key","target":"시스템"},
		{"source":"_root_key","target":"개방데이터"},
		{"source":"_root_key","target":"UseCase"}
	]
}

var _network_dummy = {
		"nodes":[
			{"id":"1","title":"사회경제","cate":"1","type":"keyword","group":"keyword","y":0,"x":0},
			/*
			{"id":"기관","title":"기관","cate":"2","type":"department","group":"depGroup","y":-150,"x":-150},
			{"id":"시스템","title":"시스템","cate":"2","source":"_root_key","type":"system","group":"sysGroup","y":0,"x":(150*1.5)},
			{"id":"개방데이터","title":"개방데이터","cate":"2","source":"_root_key","type":"dataset","group":"dataSetGroup","y":150,"x":-150},
			{"id":"a_1","title":"미얀마","cate":"2","source":"1","type":"department","group":"depGroup","y":-150,"x":-150},
			{"id":"a_2","title":"베트남","cate":"2","source":"1","type":"system","group":"sysGroup","y":150,"x":150},
			{"id":"a_3","title":"만달레이","cate":"3","source":"a_1","type":"dataset","group":"dataSetGroup","y":150,"x":-150},
			{"id":"a_6","title":"다낭 ","cate":"3","source":"a_2","type":"useCase","group":"useGroup","y":-150,"x":150}
			*/
			
			{"id":"a_1","title":"미얀마","cate":"2","source":"1","type":"nat_area","group":"natGroup","y":-150,"x":-150},
			{"id":"a_2","title":"베트남","cate":"2","source":"1","type":"nat_area","group":"natGroup","y":150,"x":150},
			{"id":"a_3","title":"만달레이","cate":"3","source":"a_1","type":"city_area","group":"cityGroup","y":150,"x":-150},
			{"id":"a_6","title":"다낭 ","cate":"3","source":"a_2","type":"city_area","group":"cityGroup","y":-150,"x":150}
			
		],
		"links":[
			{"source":"1","target":"a_1"},
			{"source":"1","target":"a_2"},
			{"source":"a_1","target":"a_3"},
			{"source":"a_2","target":"a_6"}
		]
	}

// 현재 경로
var id_category = 1;
var nowPath = "${path}";
$(function() {
	startSearch(1);
// 	$(".text.cate2,.text.cate3").on("click",function(){
// 		displayArea($(this).attr("data-url"));
// 	});
});
function moveParent(){
	 
    //팝업창에서 부모창을 다른페이지로 이동합니다.
    window.opener.location.href="<c:url value='/content.do?id_category=1'/>";
}

// $(document).on("click",".text.cate2,.text.cate3",function(){
// 	displayArea();
// });

// network 초기화면 로드
//networkInitialLoader(_network_dummy);
// drawNetworkGraph(dataModifier(_fav_dummy));

//해수부 관련
//drawNetworkGraph(dataModifier(_sea_dummy));

// 공통기능 ====================================================================================
//1> 화면전환 이벤트
$(".display_switcher").on("click", displaySwitch);

</script>
</html>