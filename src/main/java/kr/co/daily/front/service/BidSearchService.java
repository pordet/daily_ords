package kr.co.daily.front.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import daily.mybatis.util.MybatisPagingUtil;

import daily.common.util.AddBatchUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.dao.IdentityManageDAO;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.dao.BidSearchDAO;
import kr.co.daily.front.dao.MetaDAO;

@Service
public class BidSearchService {
	
	@Value("#{global['openapi.addbatch.cnt']}")
	private int addBatchCnt;
	
	@Autowired private BidSearchDAO dao;
	@Autowired private MetaDAO metaDao;
	@Autowired private IdentityManageDAO idenDao;

	/**
	 * 실제 검색된 내용을 저장하는 테이블을 조회하는 테이블
	 * @param map
	 * @return
	 */
	public List<CustomMap> selectBidNotiM(CommandMap map) {
		return dao.selectBidNotiM(map.getMap());
	}
	
	public List<CustomMap> selectBidNotiCol(CommandMap map) {
		return dao.selectBidNotiCol(map.getMap());
	}
	
	public List<CustomMap> selectBidNotiList(CommandMap map) {
		return dao.selectBidNotiList(map.getMap());
	}
	
	public void insertBidNoti(List<List<CommandMap>> inputList, CommandMap searchCon) {
		Map<String,List<CommandMap>> inputMap = new HashMap<String,List<CommandMap>>();
		for(List<CommandMap> list:inputList) {
			CommandMap keyMap = new CommandMap();
			keyMap.put("search_date", searchCon.getString("search_date"));
			keyMap.put("rslt_pg_no", searchCon.getString("rslt_pg_no"));
			keyMap.put("id_service", searchCon.getString("id_service"));
			if(list.size()>0) {
				CommandMap first = list.get(0);
				keyMap.put("matched_keyword", first.getString("matched_keyword", ""));
			}
			dao.insertBidNoti(keyMap.getMap());
			for(CommandMap map:list) {
				map.put("id_bid_noti", keyMap.get("id_bid_noti"));
			}
			inputMap.put(keyMap.get("id_bid_noti").toString(), list);
		}
		List<CommandMap> inputRealList = new ArrayList<CommandMap>();
		for(String id_bid_noti:inputMap.keySet()) {
			List<CommandMap> inList = inputMap.get(id_bid_noti);
			inputRealList.addAll(inList);
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, inputRealList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertBidNotiColByGroup(inInputList);
		}
	}

	public List<CustomMap> selectBidSearchKeywordList() {
		CommandMap map = new CommandMap();
		map.put("id_code_group", GlobalConst.API_BID_SEARCH_KEYWORD_GROUP_CD);
		return dao.selectBidSearchKeywordList(map.getMap());
	}

	public List<CustomMap> selectBidNotiMForUsing(CommandMap map) {
		return dao.selectBidNotiMForUsing(map.getMap());
	}

	public PageList<CustomMap> selectBidNotiListContents(CommandMap map, CommandMap spMap) {
		return dao.selectBidNotiListContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}
	public PageList<CustomMap> selectBidNotiListContents(CommandMap map) {
		return dao.selectBidNotiListContents(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}


	public List<CustomMap> selectBidNotiColForSearch(List<Integer> rawKey) {
		return dao.selectBidNotiColForSearch(rawKey);
	}

	public List<CustomMap> selectBidNotiMUsingBySysCd(CommandMap map) {
		return dao.selectBidNotiMUsingBySysCd(map.getMap());
	}

	public void updateBidServiceInputState(CommandMap map) {
		dao.updateBidServiceInputState(map.getMap());
	}

	public List<CustomMap> selectBidPkInfoBySysCode(CommandMap map) {
		return dao.selectBidPkInfoBySysCode(map.getMap());
	}

	public List<CustomMap> selectBidNotiKeyList(CommandMap map) {
		return dao.selectBidNotiKeyList(map.getMap());
	}

	public CustomMap getIdentyMap(CommandMap map) {
		return idenDao.getIdentityM(map.getMap());
	}

	public List<CustomMap> deleteBidNoti(CommandMap map) {
		dao.deleteBidNotiCol(map.getMap());
		dao.deleteBidNoti(map.getMap());
		List<CustomMap> colList = metaDao.selectMeta(map.getMap());
		List<CustomMap> headerList = new ArrayList<>();
		//Map<id_parent_meta,List<child>
		Map<Integer,List<CustomMap>> childMap = new HashMap<>();
		Map<Integer,CustomMap> curMap = new HashMap<>();
		for(CustomMap col:colList) {
			if(col.getInt("meta_depth")==1) {
				headerList.add(col);
				curMap.put(col.getInt("id_meta"), col);
			}else {
				int id_parent_meta = col.getInt("id_parent_meta");
				List<CustomMap> childList = childMap.get(col.getInt("id_parent_meta"));
				if(childList==null) {
					childList = new ArrayList<>();
					childList.add(col);
					childMap.put(col.getInt("id_parent_meta"), childList);
				}else {
					childList.add(col);
				}
				curMap.get(id_parent_meta).put("child_list", childList);
			}
		}
		return headerList;
	}

	public void insertBidNotiOrigin(List<List<CommandMap>> notMatchInputList, CommandMap searchCon) {
		Map<String,List<CommandMap>> inputMap = new HashMap<String,List<CommandMap>>();
		for(List<CommandMap> list:notMatchInputList) {
			CommandMap keyMap = new CommandMap();
			keyMap.put("search_date", searchCon.getString("search_date"));
			keyMap.put("rslt_pg_no", searchCon.getString("rslt_pg_no"));
			keyMap.put("id_service", searchCon.getString("id_service"));
			dao.insertBidNotiOrigin(keyMap.getMap());
			for(CommandMap map:list) {
				map.put("id_bid_noti", keyMap.get("id_bid_noti"));
			}
			inputMap.put(keyMap.get("id_bid_noti").toString(), list);
		}
		List<CommandMap> inputRealList = new ArrayList<CommandMap>();
		for(String id_bid_noti:inputMap.keySet()) {
			List<CommandMap> inList = inputMap.get(id_bid_noti);
			inputRealList.addAll(inList);
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, inputRealList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertBidNotiColOriginByGroup(inInputList);
		}
	}

	public List<CustomMap> selectBidNotiOriginKeyList(CommandMap map) {
		return dao.selectBidNotiOriginKeyList(map.getMap());
	}

	public PageList<CustomMap> selectBidNotiOriginListContents(CommandMap map,CommandMap spMap) {
		return dao.selectBidNotiOriginListContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}

	public List<CustomMap> selectBidNotiColOriginForSearch(List<Integer> rawKey) {
		return dao.selectBidNotiColOriginForSearch(rawKey);
	}


}
