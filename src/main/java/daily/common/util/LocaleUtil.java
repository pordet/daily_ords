package daily.common.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

public class LocaleUtil {

	private static Locale getDefaultLocale() {
		return Locale.KOREAN;
	}
	
	public static Locale getLocale(HttpServletRequest req) {
		Locale locale = null;
		HttpSession session = req.getSession();
		locale = (Locale)session.getAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		
		if(locale == null) {
			locale=getDefaultLocale();
		}
		return locale;
	}
}
