package kr.co.daily.front.main.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface MainDAO {

	List<CustomMap> selectCate(Map<String, Object> map);

	List<CustomMap> selectNation(Map<String, Object> map);

	CustomMap getFirstNation(Map<String, Object> map);

	List<CustomMap> selectDataList(Map<String, Object> map);

	CustomMap getYearData(Map<String, Object> map);

	List<CustomMap> selectLvlList(Map<String, Object> map);

	List<CustomMap> selectUnit(Map<String, Object> map);

	List<CustomMap> selectRealDataList(Map<String, Object> map);

	List<CustomMap> selectChartData(Map<String, Object> map);

	List<CustomMap> selectZoneData(Map<String, Object> map);

	CustomMap getZoneInfo(Map<String, Object> map);

	List<CustomMap> selectIdMetaList(Map<String, Object> map);

	List<CustomMap> selectIndividualData(Map<String, Object> map);

	List<CustomMap> selectUnitDetail(Map<String, Object> map);

	Integer getUnitDetailCntByDataType(Map<String, Object> map);

	List<CustomMap> selectMetaUnit(Map<String, Object> map);

	CustomMap getYearDataByIdMeta(Map<String, Object> map);

	List<CustomMap> selectLvlListByDataType(Map<String, Object> map);

	List<CustomMap> selectUnitByIdMeta(Map<String, Object> map);

	List<CustomMap> selectSpecialChartData(Map<String, Object> map);
	
//	List<CustomMap> selectInitialData(int id_master);

	List<CustomMap> selectTotalListByIdParentUit(Map<String, Object> map);

	//List<CustomMap> integratedBoardSearch(List<Map<String, Object>> map);
	//List<CustomMap> integrateBoardCount(List<Map<String, Object>> map);
	//List<CustomMap> integratedFileSearch(List<CustomMap> map);
	List<CustomMap> integratedBoardSearch(Map<String, Object> map);
	List<CustomMap> integrateBoardCount(Map<String, Object> map);
	List<CustomMap> integratedFileSearch(List<CustomMap> map);

	List<CustomMap> selectMetaDataMultiFiles(Map<String, Object> map);

	CustomMap getMetaDesc(Map<String, Object> map);


	List<CustomMap> selectMainBoardRow(Map<String, Object> map);

	List<CustomMap> selectBoardView(Map<String, Object> map);

}
