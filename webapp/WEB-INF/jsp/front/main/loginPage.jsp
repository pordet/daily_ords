<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.SpringPropertiesUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>

<c:set var="title" value="Login"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
	<div class="login-form-area">
		<div class="login-form-header">
			<strong>${title}</strong>
		</div>
		<div class="login-form-body">
			<form name="frm" method="post" action="<c:url value='/loginAction.do'/>">
				<input type="hidden" name="returnUrl" value="">
				<input type="hidden" name="referer" value="">
				
				<div class="login-input-group">
					<div>
						<input type="text" id="userId" name="id" maxlength="60" placeholder="아이디">
					</div>
					<div>
						<input type="password" id="secretNo" name="pwd" maxlength="20" placeholder="비밀번호">
					</div>
					<div>
						<button class="btn-primary" id="login" name="login">로그인</button>
					</div>
				</div>
			</form>
		</div>
		<div class="login-form-bottom">
			<div>
				<a href="<c:url value='/member/find_user_info.do'/>">아이디/비밀번호 찾기</a>
			</div>
			<div>
				<a href="<c:url value='/join.do'/>">회원가입</a>
			</div>
		</div>
		<div class="login-copyright">© 2020 THE KOREA TRANSPORT INSTITUTE All Rights Reserved. Privacy and Terms</div>
	</div>
</front:FrontDynamicBody>
<script>
	var ret = "${ret}";
	var test = "${GlobalConst}"
	$(function(){
		if(ret == "${GlobalConst.NOT_FOUND_USER}"){
			alertOpen("등록되지 않은 사용자입니다.");
		}else if(ret == "${GlobalConst.NOT_PW_MATCH}"){
			alertOpen("비밀번호가 틀립니다.");
		}else if(ret == "${GlobalConst.NOT_ACCEPT_USER}"){
			alertOpen("관리자의 사용 승인이 되지 않았습니다.");
		}else if(ret == "${GlobalConst.ACT_FAIL}"){
			alertOpen("휴먼 계정입니다.");
		}
	});
</script>
</html>
