package daily.common.util;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public abstract class SimpleListTagSupport<M> extends SimpleTagSupport {

	private String listName;

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().setAttribute( listName, this.getList() );
	}

	public abstract List<M> getList();

	public void setListName( String listName ) {
		this.listName = listName;
	}

	public String getListName() {
		return listName;
	}
}
