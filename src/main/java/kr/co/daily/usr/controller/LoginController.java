package kr.co.daily.usr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import daily.common.base.AbstractControllerBase;
import daily.common.service.SystemUserService;
import daily.common.util.RandomUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Controller
public class LoginController extends AbstractControllerBase {

	@Autowired private SystemUserService systemUserService;
//	@Autowired private GuestService guestService;
//	@RequestMapping("/loginPage.do")
//	public String loginPage(CommandMap param, Model model) throws Exception {
//		
//		 
//		CustomMap crsRequest = guestService.getcrsRequestAdm(param);
//		model.addAttribute("crsRequest",crsRequest);
//		return "main/loginPage";
//	}
	
	@RequestMapping("/multiAccessFailure.do")
	public String multiAccessFailure(CommandMap param, Model model) throws Exception {
		return "common/error/sec/multiAccessFailure";
	}

	@RequestMapping("/accessDenied.do")
	public String accessDenied(CommandMap param, Model model) throws Exception {
		CustomMap map = getLoginInfo();
		if(map!=null) {
			model.addAttribute("logined", "Y");
		}
		return "common/error/sec/accessDenied";
	}

	@RequestMapping("/sessionExpired.do")
	public String sessionExpired(CommandMap param, Model model) throws Exception {
		return "common/error/sec/sessionExpired";
	}

	@RequestMapping("/login/main.do")
	public String loginMain(CommandMap param, Model model) throws Exception {
		String head = SpringPropertiesUtil.getProperty("excel.usrList.head");
		model.addAttribute("head", head);
		return "login/main";
	}
	@RequestMapping("/login/newLoginPage.do")
	public String newLoginPage(CommandMap param, Model model) throws Exception {
		model.addAttribute("recaptcha", RandomUtil.randomCharAndNum(5));
		return "login/newLoginPage";
	}
	@RequestMapping("/login/loginPage.do")
	public String loginLoginPage(CommandMap param, Model model) throws Exception {
		model.addAttribute("recaptcha", RandomUtil.randomCharAndNum(5));
		return "login/loginPage";
	}
	@RequestMapping("/login/regUser.do")
	public String regUser(CommandMap param, Model model) throws Exception {
		
		return "main/home";
	}
	@RequestMapping("/login/checkUsr.do")
	public String checkUsr(CommandMap param, Model model) throws Exception {
		CommandMap loginInfo = new CommandMap();
		loginInfo.put("loginedId", param.getString("id"));
		int loginCnt = systemUserService.countLoginCntByUserId(loginInfo );
		model.addAttribute("cnt",loginCnt);
		return "jsonView";
	}
	
}
