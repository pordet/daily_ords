package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface BatchManageDAO {
	PageList<CustomMap> selectBatchResultList(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectBatchDetailByIdBatch(Map<String, Object> map);


}
