function nanToStr(key){
	if(key){
		return key;
	}else{
		return "";
	}
}

function nanToNum(key){
	if(key){
		return key;
	}else{
		return 0;
	}
}

function nanTextToText(){
	var output = "";
    //for(var i of arguments) {
    for(var idx=0; idx<arguments.length; idx++) {
		var i = arguments[idx];
        if(i){
        	output+=i+" ";
        }
    }
    return output;
}

function optionalFormSubmit(action,formData){
	
	var form = document.createElement("form");

    form.setAttribute("method", "Post");  //Post 방식
    form.setAttribute("action", action); //요청 보낼 주소
    
    //for(var item of formData.entries()) {
	for(var i=0; i<formData.entries().length; i++) {
		var item = formData.entries()[i];
    	var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", item [0]);
        hiddenField.setAttribute("value", item [1]);
        form.appendChild(hiddenField);	
	}
    document.body.appendChild(form);
    form.submit();
}

function checkNumberFormat(that,decimal,scale){
	var isChecked=true;
	if($(that).val()==''){
		isChecked= false;
	}
	var special_pattern = /[`~!@#$%^&*|\\\'\";:\/?]/gi;

	if( special_pattern.test($(that).val()) == true ){
		alertOpen('특수문자는 사용할 수 없습니다.');
		isChecked= false;
	}
	if($(that).val().length>decimal){
		$(that).val('');
		alertOpen('입력 숫자 길이는 '+decimal+' 보다 클 수 없습니다.');
		isChecked= false;
	}
	var expr = $(that).val().replaceAll(',','').replaceAll('.','');
    if(isNaN(expr)){
		//이메일 형식이 알파벳+숫자@알파벳+숫자.알파벳+숫자 형식이 아닐경우			
		$(that).val('');
		alertOpen('숫자,콤마,소수점만 입력 가능합니다.');
		return;
		isChecked= false;
	}
    if(isChecked){
    	var val = $(that).val().replaceAll(',','');
    	var numArr = val.split(".");
    	
    	numArr[0] = numArr[0].replace(/\B(?=(\d{3})+(?!\d))/g,",");
    	if(numArr.length>1){
    		var scaleArr = Number("0."+numArr[1]).toFixed(scale).toString().split(".");
    		if(scale>0){
    			$(that).val(numArr[0]+"."+scaleArr[1]);
    		}else{
    			$(that).val(numArr[0]);
    		}
    	}else{
    		$(that).val(numArr[0]);
    	}
    }else{
		$(that).val('');
    	return false;
    }
    return;
}
function checkNonNumber(val){
	var expr = val.replaceAll(',','').replaceAll('.','');
    if(isNaN(expr)){
		//이메일 형식이 알파벳+숫자@알파벳+숫자.알파벳+숫자 형식이 아닐경우			
		return false;
	}else{
		return true;
	}
}

function checkStringFormat(that,decimal){
	if($(that).val().length>decimal){
		alertOpen('입력 글자수는 '+decimal+' 보다 클 수 없습니다.');
		$(that).val('');
		return false;
	}
	return false;
	
}
function checkStringFormatAndNull(that,decimal){
	if($(that).val().length>decimal){
		alertOpen('입력 글자수는 '+decimal+' 보다 클 수 없습니다.');
		$(that).val('');
		return false;
	}
	if($.trim($(that).val())==''){
		alertOpen('반드시 입력해야 합니다.');
		$(that).focus();
		return false;
	}
	
}