package kr.co.daily.common.taglib;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UrlPathHelper;

import daily.common.util.SimpleListTagSupport;
import daily.common.util.SpringApplicationContext;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.common.dao.CommonDAOImpl;
import kr.co.daily.domain.MenuVO;

public class MenuTag extends SimpleListTagSupport<MenuVO> {
	private int user_seq;
	private String locale;


	public int getUser_seq() {
		return user_seq;
	}

	public void setUser_seq(int user_seq) {
		this.user_seq = user_seq;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public List<MenuVO> getList() {

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		UrlPathHelper urlPathHelper = new UrlPathHelper(); 
		String url = urlPathHelper.getOriginatingRequestUri(req);
		String context = urlPathHelper.getOriginatingContextPath(req);
	    if(!context.equals("")) {
	    	url = url.substring(context.length());
	    }
		String id_menu = req.getParameter("id_menu");
		if(id_menu==null) id_menu="";
//		String url = req.getRequestURI();  - 
		List<MenuVO> list = null;
		CommonDAOImpl dao = SpringApplicationContext.getBean( "commonDAOImpl", CommonDAOImpl.class );
		MenuVO vo = new MenuVO();
		vo.setUser_seq(user_seq);
		vo.setLocale(locale);
		try {
			//게스트일 경우의 처리 로직
			if(user_seq==0) {
				vo.setMenu_depth(1);
				vo.setId_group(GlobalConst.ID_GROUP_GUEST);
				List<MenuVO> tlist = dao.selectTopMenuByIdGroup(vo);
				vo.setMenu_depth(2);
				List<MenuVO> slist = dao.selectMenuByIdGroup(vo);
				for(MenuVO topMenu : tlist) {
					List<MenuVO> childList = new ArrayList<MenuVO>();
					if(id_menu.equals(String.valueOf(topMenu.getId_menu()))) {
						topMenu.setSelected(true);
					}else {
						if(url.equals(topMenu.getMenu_url())) {
							topMenu.setSelected(true);
						}
					}
					for(MenuVO subMenu : slist) {
						if(subMenu.getId_parent_menu()==topMenu.getId_menu()) {
							if(id_menu.equals(String.valueOf(subMenu.getId_menu()))) {
								subMenu.setSelected(true);
				                topMenu.setSelected(true);
				                topMenu.setChildSelected(true);
							}
							childList.add(subMenu);
						}
					}
					topMenu.setChild(childList);
					topMenu.setChild_cnt(childList.size());
				}
				list = tlist;
			}else {
				vo.setMenu_depth(1);
				List<MenuVO> tlist = dao.selectTopMenu(vo);
				vo.setMenu_depth(2);
				List<MenuVO> slist = dao.selectMenu(vo);
				for(MenuVO topMenu : tlist) {
					List<MenuVO> childList = new ArrayList<MenuVO>();
					if(id_menu.equals(String.valueOf(topMenu.getId_menu()))) {
						topMenu.setSelected(true);
					}else {
						if(url.equals(topMenu.getMenu_url())) {
							topMenu.setSelected(true);
						}
					}
					for(MenuVO subMenu : slist) {
						if(subMenu.getId_parent_menu()==topMenu.getId_menu()) {
							if(id_menu.equals(String.valueOf(subMenu.getId_menu()))) {
								subMenu.setSelected(true);
				                topMenu.setSelected(true);
				                topMenu.setChildSelected(true);
							}
							childList.add(subMenu);
						}
					}
					topMenu.setChild(childList);
					topMenu.setChild_cnt(childList.size());
				}
				list = tlist;
			}
		}catch(Exception e) {
			list = null;
		}
		return list;
	}

}
