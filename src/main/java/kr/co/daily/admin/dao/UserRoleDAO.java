package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface UserRoleDAO {
	
	CustomMap getUserRole(Map<String, Object> map);

	List<CustomMap> selectRoleList(Map<String, Object> map);

	PageList<CustomMap> selectUserRoleList(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectRoleListByGroup(Map<String, Object> map);
	
	void insertRole(Map<String, Object> map);

	void modifyRole(Map<String, Object> map);

	void deleteRoleList(Map<String, Object> map);




}
