	var LayerPopup = {
		//alert_popup
		alertError : function(msg,detail,callback)
		{
			this.alert("error",msg,detail,callback);
		},
		alertWarning : function(msg,detail,callback)
		{
			this.alert("warning",msg,detail,callback);
		},
		alertInfo : function(msg,detail,callback)
		{
			this.alert("info",msg,detail,callback);
		},
		alertSuccess : function(msg,detail,callback)
		{
			this.alert("success",msg,detail,callback);
		},
		confirmQuestion : function(msg,detail,callback , cancelback)
		{
			this.confirm("question",msg, detail, callback , cancelback);
		},
		confirmWarning : function(msg,detail,callback , cancelback)
		{
			this.confirm("warning",msg, detail, callback , cancelback);
		},
		// type 값 : error , warning, info , success, question
		alert : function (type, msg, detail,callback)
		{

			var alertPic = contextPath+"/asset/images";
			var title = "";
			if(type == 'error'){
				alertPic += "/icon_error.png";
			}else if(type == 'warning'){
				alertPic += "/icon_warnning.png";
			}else if(type=='info'){
				alertPic += "/icon_info.png";
			}else if(type=='success'){
				alertPic += "/icon_confirm.png";
			}
            var param = {};
            param.content= msg,
            param.type = type,
            param.alertPicture= alertPic;
			if(typeof detail =='string') {
				param.detail = detail;
			}else if(typeof detail == 'function'){
				callback = detail;
				param.detail = "";
			}else{
				param.detail = "";
			}
 			$("#alertPopup").loadTemplate(contextPath+"/asset/template/alert.html", param, {
 				success: function(){
					if(typeof detail !='string'){
						$(".alertpop .detailAlert").hide();
					}
					$('#alertClose').on("click",function(e) {
						if(callback) 
							callback();
					});
				}
 			}).bPopup({
					modalClose: false,
					opacity: 0.4,
			});
		},
		
		//confirm_popup
		confirm : function( type, msg, detail, callbackYes , callbackNo )
		{
			var alertPic = contextPath+"/asset/images";
			if(type == 'warning'){
				alertPic += "/icon_warnning.png";
			}else if(type == 'question'){
				alertPic += "/icon_question.png";
			}else if(type=='info'){
				alertPic += "/icon_info.png";
			}else if(type=='success'){
				alertPic += "/icon_confirm.png";
			}
            var param = {};
            param.content= msg,
            param.type = type,
            param.alertPicture= alertPic;
			if(typeof detail =='string') {
				param.detail = detail;
			}
			if(typeof detail =='function') {
				param.detail = "";
				callbackNo = callbackYes;
				callbackYes = detail;
			}
			$("#confirmPopup").loadTemplate(contextPath+"/asset/template/confirm.html", param, {success: function(){
				if(typeof detail !='string'){
					$(".alertpop .detailConfirm").hide();
				}
				$('#confirmCancel').on("click",function(e){
					if(callbackNo) 
						callbackNo();
				});
				$('#confirmOk').on("click",function(e){
					if(callbackYes) 
						callbackYes();
				});
			}}).bPopup({
				modalClose: false,
				opacity: 0.4,
			});		
		}
	}
