function regMeta(id_meta_m,id_meta){
	var params={id_meta_m:id_meta_m,id_meta:id_meta};
	var url = contextPath+"/adm/meta/getMetaAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else if(e.ret==8){
    			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else{
				var m = e.metaData;
				var c = e.childMetaList;
				$("#meta_nm").val(m.meta_nm);
			    // 메타 코드 입력란 채우기
			    $("#meta_cd").val(m.meta_cd);
			    
			    // 메타 뎁스 입력란 채우기
			    $("#meta_depth").val(m.meta_depth);
			    
			    // 메타 설명 입력란 채우기
			    $("#meta_doc").val(m.meta_doc);
			    
			    // 데이터 타입 선택란 채우기
			    $("#typ").val(m.typ);
			    $("#decimal_len").val(m.decimal_len);
				$("#len_detail").remove();
				//정수형
				if(m.typ ==1){
					var html="";
					html+='<tr id="len_detail"><th>숫자형 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="'+m.decimal_len+'" placeholder="숫자형 최대길이를 입력"></td></tr>';
					$("#typ_detail").after(html);
					//실수형,퍼센트형
				}else if(m.typ==2 || m.type==6){
			    	$("#precision_len").val(m.precision_len);
					var html="";
					html+='<tr id="len_detail"><th>숫자 최대 길이</th><td><input type="text" id="decimal_len" name="decimal_len" value="'+m.decimal_len+'" placeholder="소수점 포함 숫자 최대길이를 입력"></td>';
					html+='<th>소수점 최대 길이</th><td><input type="text" id="precision_len" name="precision_len" value="'+m.precision_len+'" placeholder="소수점 최대길이를 입력"></td></tr>';
					$("#typ_detail").after(html);
					//문자열
				}else if(m.typ=3){
					var html="";
					html+='<tr id="len_detail"><th>문자열 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="'+m.decimal_len+'" placeholder="문자열 최대길이를 입력"></td></tr>';
					$("#typ_detail").after(html);
				}else if(m.typ=4){
					
				}else if(m.typ=5){
					
				}
			    
			    // 사용여부 라디오 버튼 채우기
			    if (m.extern_yn == 'Y') {
			        $("#extern_yn1").prop("checked", true);
			    } else {
			        $("#extern_yn2").prop("checked", true);
			    }
			    
			    // 하위여부 라디오 버튼 채우기
			    if (m.list_show_yn == 'Y') {
			        $("#list_yn1").prop("checked", true);
			    } else {
			        $("#list_yn2").prop("checked", true);
			    }
			    // 사용여부 라디오 버튼 채우기
			    if (m.use_yn == 'Y') {
			        $("#use1").prop("checked", true);
			    } else {
			        $("#use2").prop("checked", true);
			    }
				    
				if(c.length>0){
				    $("#child1").prop("checked", true);
					$("#child_list_tr").remove();
					var html = '<tr id="child_list_tr"><td colspan="4">\n';
					html += '<table><thead>\n';
					html += '<tr>\n';
					html += '<th>순서</th>\n';
					html += '<th>메타 코드</th>\n';
					html += '<th>메타 명</th>\n';
					html += '<th>데이터 타입</th>\n';
					html += '<th>하위추가</th>\n';
					html += '</tr>\n';
					html += '</thead>\n';
					html += '<tbody class="grid-contents">';
					$.each(c,function(i,d){
						html += '<tr>';
					    html += '<td>' + d.ord + '</td>';
					    html += '<td>' + d.meta_cd + '</td>';
					    html += '<td>' + d.meta_nm + '</td>';
					    html += '<td>' + d.typ_nm + '</td>';
					    html += '<td><button type="button">하위추가</button></td>';
					    html += '</tr>';
					});
					html += '</tbody></table>';
					html += '</td></tr>';
					var existingTr = $("#child_yn_tr").next("tr");
					if (existingTr.length === 0) {
					    // #child_yn_tr 다음에 이미 <tr>이 없는 경우에만 삽입
					    $("#child_yn_tr").after(html);
					}
				}else{
				    $("#child2").prop("checked", true);
					$("#child_list_tr").remove();
				}
				$("#btnSave").val('수정');
			}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}


