$(document).ready(function() {
	alert('a');
    $(".scroll-thumb").draggable({
        axis: "x",
        containment: "parent",
        drag: function(event, ui) {
            // Calculate the scroll position based on the handle's position
            var handlePosition = ui.position.left;
            var containerWidth = $(".content").width();
            var contentWidth = $(".grid-area").width();
            var scrollLeft = (handlePosition / (containerWidth - handlePosition)) * (contentWidth - containerWidth);
            
            // Set the scroll position of the grid_area
            $(".grid-area").scrollLeft(scrollLeft);
        }
    });
});