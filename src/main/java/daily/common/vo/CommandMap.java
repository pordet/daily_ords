package daily.common.vo;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
 
public class CommandMap implements Serializable {
	private static final long serialVersionUID = 6784305846477456650L;
	Map<String,Object> map = new java.util.HashMap<String,Object>();
     
    public Object get(String key) {
        return map.get(key);
    }

    /**
     * 臾몄���� �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ null 臾몄���대� 由ы��.
     */
    public String getString(String key) {
    	return getString(key, "");
    }

    /**
     * 臾몄���� �����쇰� 媛� �산린
     * @param key
     * @param intVal
     * @return 議댁�ы��吏� ��嫄곕�� null 臾몄���댁�대㈃ initVal 由ы��
     */
    public String getString(String key, String intVal) {
    	if(map.get(key) == null) return intVal;
    	String val = map.get(key).toString();
    	return val.isEmpty() ? intVal : val;
    }

    /**
     * ���� �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ 0 由ы��.
     */
    public int getInt(String key) {
    	return getInt(key, 0);
    }

    /**
     * ���� �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ initVal 由ы��.
     */
    public int getInt(String key, int intVal) {
    	if(map.get(key) == null) return intVal;
    	try {
    		return Integer.parseInt(map.get(key).toString()); 
    	} catch(Exception e) {
    		return intVal;
    	}
    }
 
    /**
     * �ㅼ�� �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ 0.0 由ы��.
     */
    public double getDouble(String key) {
    	return getDouble(key, 0.0);
    }

    /**
     * �ㅼ�� �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ initVal 由ы��.
     */
    public double getDouble(String key, double intVal) {
    	if(map.get(key) == null) return intVal;
    	try {
    		return Double.parseDouble(map.get(key).toString());
    	} catch(Exception e) {
    		return intVal;
    	}
    }

    /**
     * Boolean �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ initVal 由ы��.
     */
   public boolean getBoolean(String key) {
    	return getBoolean(key, false);
    }

    /**
     * Boolean �����쇰� 媛� �산린
     * @param key
     * @return 議댁�ы��吏� ���쇰㈃ intVal 由ы��.
     */
    public boolean getBoolean(String key, boolean intVal) {
    	if(map.get(key) == null) return intVal;
    	try {
    		return Boolean.parseBoolean(map.get(key).toString());
    	} catch(Exception e) {
    		return intVal;
    	}
    }
    
	/**
	 * checkbox name ���깆�쇰� �ㅼ�댁�ㅻ�� 媛��� CommandMap�� 異�媛�����.
	 * @param obj 臾몄���� ���� 臾몄���대같��
	 */
	public void setStringList(String key) {
    	List<String> list = new ArrayList<String>();
    	String listkey = key+"[]";
    	if(map.get(listkey)!=null){
    		String[] arr = (String[])map.get(listkey);
    		for(String str : arr){
    			list.add(str);
    		}
    	}else{
    		list.add(getString(key, ""));
    	}
		map.put(key, list);
	}
    
	public List<String> getStringList(String key) {
    	List<String> list = new ArrayList<String>();
    	if(map.get(key)==null) {
    		return null;
    	}
		if(map.get(key).getClass().isArray()){
			String[] valList = (String[])map.get(key);
			for(String val: valList) {
				list.add(val);
			}
		}else if(map.get(key) instanceof String) {
			list.add((String)map.get(key));
		}
		return list;
	}
    

    @Deprecated
    public float getFloat(String key) {
    	return (float)getDouble(key);
    }
    
    
    public void put(String key, Object value){
        map.put(key, value);
    }
     
    public Object remove(String key) {
        return map.remove(key);
    }
     
    public boolean containsKey(String key) {
        return map.containsKey(key);
    }
     
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }
     
    public void clear() {
        map.clear();
    }
     
    public Set<Entry<String, Object>> entrySet() {
        return map.entrySet();
    }
     
    public Set<String> keySet() {
        return map.keySet();
    }
     
    public boolean isEmpty() {
        return map.isEmpty();
    }
     
    public void putAll(Map<? extends String, ?extends Object> m) {
        map.putAll(m);
    }
     
    public Map<String,Object> getMap() {
        return map;
    }
    
    public String toString(){
    	return map.toString();
    }
    
    public static CommandMap from(Row row) {
    	CommandMap map = new CommandMap();
    	int len = row.getPhysicalNumberOfCells();
    	for(int i=0;i<len;i++) {
    		switch(row.getCell(i).getCellType()) {
	    		case STRING : 
	        		map.put(String.valueOf(i), row.getCell(i).getStringCellValue());
	        		break;
	    		case  NUMERIC :
	        		map.put(String.valueOf(i), String.valueOf(row.getCell(i).getNumericCellValue()));
	        		break;
	    		case  BOOLEAN :
	        		map.put(String.valueOf(i), row.getCell(i).getBooleanCellValue());
	        		break;
	    		case BLANK :
	        		map.put(String.valueOf(i), "");
	        		break;
	    		default :
	        		map.put(String.valueOf(i), row.getCell(i).getDateCellValue());
	        		break;
    		}
    	}
    	return map;
    }
}