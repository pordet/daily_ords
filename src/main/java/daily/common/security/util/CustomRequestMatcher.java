package daily.common.security.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.AntPathMatcher;

public class CustomRequestMatcher implements RequestMatcher {
	
    private String pattern;

    public CustomRequestMatcher(String pattern) {
        this.pattern = pattern;
    }

	@Override
	public boolean matches(HttpServletRequest request) {
		String requestURI = request.getRequestURI();
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        // 와일드카드 패턴을 이용하여 패턴 매칭 수행
        return antPathMatcher.match(pattern, requestURI);
	}
	
	public boolean matches(String url) {
	    String requestURI = url;
	    // Ant 스타일의 와일드카드 패턴을 사용하여 패턴 매칭 수행
	    AntPathMatcher antPathMatcher = new AntPathMatcher();
	    return antPathMatcher.match(pattern, requestURI);
	}
}
