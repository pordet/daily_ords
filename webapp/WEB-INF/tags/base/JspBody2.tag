<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="hasNotCommonPop" required="false" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<body <c:if test="${not empty clazz}">class="${clazz}"</c:if> style="width:1000px;height:800px;">  <%-- (KGC:FIX)(2018.08.01) 고정size. 중앙정렬, margin은 좌우만 적용되게 --%>

	<c:if test="${not empty title}">
		<div id="Stitle">
			<div class="title"><i class="fa ${not empty ttlIcon ? ttlIcon : 'fa-bars'}"></i> ${title}</div>
		</div>
	</c:if>

	<div id="wrap">
		<div id="container">
			<jsp:doBody/>
		</div>
	</div>
<%--
    <div id="alertPopup" style="display:none;width:auto;height:auto;"></div>    
    <div id="confirmPopup" style="display:none;width:auto;height:auto;"></div>    

	<div id="bfcLayerPopup" style="display:none;width:auto;height:auto;"></div>

 	<base:JspTail/> 
	
	<div id="bfcSpin" data-spinner-layer="" data-spinner-body="visible" style="display: none;">
		<div data-spinner-bar=""><i class="fa fa-repeat" style="color: fff;"></i></div>
	</div>
--%>

</body>
