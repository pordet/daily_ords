package daily.context.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import daily.common.test.AbstractJUnit5TestBase;
import daily.common.vo.CommandMap;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ContextJUnitTest extends AbstractJUnit5TestBase {

    @Autowired
    private MessageSource messageSource;

    @Test
    public void testContext() {
        assertNotNull(applicationContext.getBean("testService"));
    }

    @Test
    public void testDAO() {
        CommandMap param = new CommandMap();
        assertNotNull(param);
    }

    @Test
    public void SpringPropertiesUtil() throws Exception {
        String aa = messageSource.getMessage("adm-02-01.sector1.title", null, new Locale("rw"));
        System.out.println("attr1:" + aa);
    }
}
