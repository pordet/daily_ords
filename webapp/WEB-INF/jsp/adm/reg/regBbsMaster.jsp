<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<c:set var="title" value="자료실"/>
<adm:AdmHead title="${title}">
	<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/bbs_master_register.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
			<c:choose>
				<c:when test="${empty param.id_bbs_m}">
					<span id="curr_title">${title} 등록</span>
				</c:when>
				<c:otherwise>
					<span id="curr_title">${title} 수정</span>
				</c:otherwise>
			</c:choose>
			</div>
			
			<div class="page-content-wrap">
				
				<form id="listFrm" name="listFrm" method="post">
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" value="${param.page}">
					<input type="hidden" name="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" value="${param.sord}"/>
				</form>
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="id_bbs_m" id="id_bbs_m" value="${param.id_bbs_m}">
					<input type="hidden" name="id_parent_bbs_m" id="id_parent_bbs_m" value="${param.id_parent_bbs_m}">
					<input type="hidden" name="parent_ord_str" id="parent_ord_str" value="${param.ord_str}">
					<input type="hidden" name="ord_str" id="ord_str" value="${param.ord_str}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>자료실명</td>
									<td>
										<input type="text" name="bbs_nm" id="bbs_nm" value="${map.bbs_nm}">
									</td>
								</tr>
								<tr>
									<td>설명</td>
									<td>
										<input type="text" class="form-control mh-form-control" name="bbs_desc" id="bbs_desc" value="${map.bbs_desc}">
									</td>
								</tr>
								<tr>
									<td>첨부여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="attach_yn" value="Y" id="at1" ${((empty map.attach_yn || map.attach_yn eq 'Y')?'checked':'')} />
												<label for="at1">예</label>
											</div>
											<div>
												<input type="radio" name="attach_yn" value="N" id="at2" ${(map.attach_yn eq 'N'?'checked':'')} />
												<label for="at2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr id="tr_multi_yn">
									<td>멀티파일 첨부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="multi_attach_yn" value="Y" id="ma1" ${((empty map.multi_attach_yn || map.multi_attach_yn eq 'Y')?'checked':'')} />
												<label for="ma1">예</label>
											</div>
											<div>
												<input type="radio" name="multi_attach_yn" value="N" id="ma2" ${(map.multi_attach_yn eq 'N'?'checked':'')} />
												<label for="ma2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>답글여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="reply_yn" value="Y" id="reply_yn1" ${((empty map.reply_yn || map.reply_yn eq 'Y')?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="reply_yn" value="N" id="reply_yn2" ${(map.reply_yn eq 'N'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="use_yn1" ${((empty map.use_yn || map.use_yn eq 'Y')?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="use_yn2" ${(map.use_yn eq 'N'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" id="btnList" onclick="doList();">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<c:if test="${(not empty map.id_bbs_m ?'block':'none')}">
								<button type="button" class="btn-warning" id="btnDelete" onclick="delData();">삭제</button>
							</c:if>
							<button type="button" class="btn-primary" id="btnSave" onclick="register();">저장</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
/*
function registerData(){
	$("#dataModal").modal('show');	
}
*/
function register(){ // 저장 버튼
	processing();
	//var params = $("#frm").serialize();
	var params = jQuery("#frm").serialize();
	var insert_yn = $("#insert_yn").val();
	var url = '';
	if(insert_yn =='Y'){
		url =contextPath+"/adm/bbs/insertBbsMasterAjax.do";
	}else{
		url = contextPath+"/adm/bbs/modifyBbsMasterAjax.do";
	}
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(insert_yn=='Y'){
	    			$("#insert_yn").val('N');
	    			$("#id_bbs_m").val(e.map.id_bbs_m);
	    		}else{
	    		}
	    		alertOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function search(){ 
	doList(1);
}

function doList(){//목록 버튼
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/bbs/bbsList.do";
	if(page)
		frm.page.value=page;
	frm.submit();
}




</script>
</html>