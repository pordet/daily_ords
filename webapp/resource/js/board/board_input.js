var cur_keyword_num=1;
function registerData(){
	$("#modi_order_key").val($("modi_order_key").val());
	$("#modi_idx").val($("modi_idx").val());
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/getBoardPathAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
		    	alertOpen('게시판 정보 조회시 오류가 발생했습니다.');
	    	}else{
	    		if(nanToStr(e.child_ord_str)==''){
	    			$("#modi_sel_ord_str").val(e.parent_ord_str);
	    		}else{
	    			$("#modi_sel_ord_str").val(e.child_ord_str);
	    		}
    			var html ='';
	    		$.each(e.pList,function(i,d){
	    			if(e.parent_ord_str ==d.ord_str){
		    			html+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_board_master").html(html);
    			var html1 ='<option value="">선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			if(nanToStr(e.child_ord_str ==d.ord_str)){
		    			html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_child_board").html(html1);	
	    		$("#board_reg_type").html(e.board_path+" 등록");
	    		regBoardInit();
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
// 	var frm =document.frm;
// 	frm.action=contextPath+"/regHistory.do";
// //	frm.action="/excelDownload.do";
// 	$("#frm").submit();
}
function changeRegChildBoard(that){
	if($(that).val()==''){
		$("#modi_sel_ord_str").val($("#modi_sel_board_master option:selected").val());
	}else{
		$("#modi_sel_ord_str").val($(that).val());
	}
	changeRegTitle('Y');
}
function changeRegTitle(has_parent){
	var reg_title = '';
	if($("#modi_sel_board_master option:selected").text()!=''){
		reg_title=$("#modi_sel_board_master option:selected").text();
	}
	if(nanToStr(has_parent)!=''){
		if($("#modi_sel_child_board").val()!=''){
			reg_title+='>>'+$("#modi_sel_child_board option:selected").text();
		}
	}
	if($("#modi_id_board").val()!=''){
		reg_title+=' 조회/수정';
	}else{
		reg_title+=' 입력';
	}
	$("#board_reg_type").html(reg_title);
}
function changeRegBoard(that){
	var sel_ord_str = $("#modi_sel_board_master option:selected").val();
	$("#modi_sel_ord_str").val(sel_ord_str);
	$.ajax({
		url: contextPath+"/changeBoardRootAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: {sel_ord_str:$(that).val()},                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	changeRegTitle();
	    	if(e.ret==1){
		    	alertOpen('게시판 정보 조회시 오류가 발생했습니다.');
	    	}else{
    			var html1 ='<option value="" selected>선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    		});
	    		$("#modi_sel_child_board").html(html1);
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	});
	
}
function regBoardInit(){
	$("#modi_id_board").val('');
	$("#modi_title").val('');
	$("#modi_user_nm").val($("#hdn_modi_user_nm").val());
	$("#modi_date").val('');
	$("#modi_comment").val('');
	$("#modi_keyword").val('');
	$("#fileName1").val('');
	$("#file_list_div").empty();
	$("#btnDelete").hide();
	changeRegTitle('Y');
	$("#main_content1").hide();
	$("#subConcent").show();
	$("[id^=new_file_div_]").remove();
	var html1 ='<div class="form-group inline-group" id="new_file_div_'+cur_file_num+'">';
    html1+='	<input type="text" id="fileName'+cur_file_num+'" class="form-control mh-form-control ht30 w100p"  disabled>';  //file_input_hidden
	html1+='	<div class="attach-btn">';
	html1+='		<button type="button" class="btn attach1 ht30"><span class="btn_attach"></span>첨부</button>';
	html1+='		<input type="file" id="new_file_'+cur_file_num+'" name="new_file_'+cur_file_num+'" data-id="'+cur_file_num+'" class="file_input_hidden" onchange="javascript:add_file(this);"/>';
	html1+='	</div>';
	html1+='</div>';
	cur_file_num=1;
	$("#new_file_div").append(html1);
}

function viewData(id_board_master,id_board,ord_str){
	$("#modi_id_board_master").val(id_board_master);
	$("#modi_id_board").val(id_board);
	$("#modi_sel_ord_str").val(ord_str);
	$("[id^=attach_]").remove();
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/getBoardAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
		    	alertOpen('키워드 목록 조회시 오류가 발생했습니다.');
	    	}else{
    			var html ='';
	    		$.each(e.pList,function(i,d){
	    			var id_parent_board = '';
	    			if(nanToStr(e.has_id_parent)==''){
	    				id_parent_board = e.master.id_parent_board;
	    			}else{
//	    				id_parent_board = e.master.id_parent_board;
	    				id_parent_board = e.master.id_board_master;
	    			}
	    			if(id_parent_board ==d.id_board_master){
		    			html+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_board_master").html(html);
    			var html1 ='<option value="">선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			if(nanToStr(e.master.id_board_master ==d.id_board_master)){
		    			html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    	   $("#modi_sel_child_board").html(html1);
	     	   loadData(e,true,false);
	     	   $("#modi_title").val(nanToStr(e.map.title));
	     	   $("#modi_comment").html(nanToStr(e.map.comment));
	     	   $("#modi_user_nm").val(nanToStr(e.map.reg_user_nm));
	     	   $("#modi_date").val(nanToStr(e.map.reg_date));
	     	   $("#modi_keyword").val(nanToStr(e.map.keyword));
	     	   $("#btnDelete").show();
	     	   $("#main_content1").hide();
	     	   $("#subConcent").show();
	//     	   $("#dataModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	});
}
function fn_return(){
	$("#main_content1").show();
	$("#subConcent").hide();
}
function search(){
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#modi_sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	selInit();
	doList(1,false);
	if($.trim($("#search_keyword").val())!=''){
		$("#search_init").show();
	}
}
function search_keyword_init(){
	$("#modi_sel_ord_str").val('');
	$("#search_keyword").val('');
	$("#search_init").hide();
}
function doList(page,isTreeClicked){
	$("#main_content1").show();
	$("#subConcent").hide();
		$("#page").val(page);
		$("#modi_order_key").val($("#order_key").val());
		$("#modi_idx").val($("#idx").val());
		$("#modi_page").val(page);
		if($("#is_search").val()=='N'){
			$("#search_keyword").val('');
		}
		var params = $("#modi_frm").serialize();
		var url = contextPath+"/historyAjax.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		list_redraw(e);
		    		draw_paging_by_ajax(e.paginator);
		    		if(nanToStr(isTreeClicked)!='' && !isTreeClicked){
	   			    	infoOpen('성공하였습니다.');
		    		}
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}
function delFile(idx){
	   $("#new_file_div_"+idx).remove();
	   $("#attach_"+idx).remove();
	   if($.trim($("#file_list_div").html())==''){
		   
	   }
}

var cur_file_num=1;
var file_arr=[];
function add_file(that){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
    var full_file_nm = $(that).val();
	document.getElementById('fileName1').value = full_file_nm;
	var file_info = full_file_nm.split("\\");
	var file_nm = file_info[file_info.length-1];
	var fileVal = file_nm.split(".");
	var file_ext = fileVal[fileVal.length-1];
    var file_sh_ext = file_ext.substring(0,3).toLowerCase();
	var html = '';
	html+='<div class="file-format-attach inline-group" id="attach_'+cur_file_num+'">';
	html+='    <span class="'+file_sh_ext+'">'+file_ext+'</span>';
	html+='    <span class="pad10 span_cut">'+file_nm+'</span>';
	html+='    <a href="javascript:delFile('+cur_file_num+');"><i class="fa fa-minus-circle del-file" aria-hidden="true"></i></a>';
	html+='</div>';
	$("#file_list_div").append(html);
	$("#new_file_div_"+$(that).attr("data-id")).hide();
	var html1 ='<div class="form-group inline-group" id="new_file_div_'+cur_file_num+'">';
	    html1+='	<input type="text" id="fileName'+cur_file_num+'" class="form-control mh-form-control ht30 w100p"  disabled>';  //file_input_hidden
		html1+='	<div class="attach-btn">';
		html1+='		<button type="button" class="btn attach1 ht30"><span class="btn_attach"></span>첨부</button>';
		html1+='		<input type="file" id="new_file_'+cur_file_num+'" name="new_file_'+cur_file_num+'" data-id="'+cur_file_num+'" class="file_input_hidden" onchange="javascript:add_file(this);"/>';
		html1+='	</div>';
		html1+='</div>';
	$("#new_file_div").append(html1);
}

function addFile(){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
	var html ='<div class="col-8 line" id="addFile_in_div_'+file_num+'">';
	html+='		<div class="form-group inline-group">';
	html+='		   <input type="text" id="fileName'+file_num+'" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>';
	html+='		   <div class="attach-btn">';
	html+='			<button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>';
	html+='		    <input type="file" name="file_'+file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+file_num+'\').value = this.value"/>';
	html+='		   </div>';
	html+='		</div>';
	html+='		</div>';
	html+='		   <div class="col-4 line" id="delFile_in_div_'+file_num+'" style="text-align:right;">';
	html+='			<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile('+file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png'+'" alt="delete"/></a>';
	html+='		  </div>';
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==0){
		$("#file_btn_group").after(html);
	}else{
		$("#delFile_in_div_"+bef_file_num).after(html);
	}
}

