package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface CodeManageDAO {

	PageList<CustomMap> selectCodeList(Map<String, Object> map, PageBounds pageBounds);

	void insertCode(Map<String, Object> map);

	void insertCodeGroup(Map<String, Object> map);

	List<CustomMap> selectCode(Map<String, Object> map);

	PageList<CustomMap> selectCodeGroupList(Map<String, Object> map, PageBounds pageBounds);

	CustomMap getCodeGroup(Map<String, Object> map);

	CustomMap getCode(Map<String, Object> map);

	void deleteCodeByCodeGroup(Map<String, Object> map);

	void deleteCodeGroup(Map<String, Object> map);

	void updateCodeGroup(Map<String, Object> map);

	void deleteCode(Map<String, Object> map);

	void updateCode(Map<String, Object> map);

	List<CustomMap> selectCodeGroup(Map<String, Object> map);

	List<CustomMap> selectCodeComboByLocale(Map<String, Object> map);

	CustomMap getLocaleCode(Map<String, Object> map);

	CustomMap getCodeGroupByCodeGroup(Map<String, Object> map);

	CustomMap getCodeByCode(Map<String, Object> map);

	CustomMap selectCodeByCodeName(Map<String, Object> map);

	CustomMap getCodeByCodeId(Map<String, Object> map);

	CustomMap getMenuByMenuName(Map<String, Object> map);

	List<CustomMap> selectUnitTypeList(Map<String, Object> map);

	List<CustomMap> selectUsingCodeGroup(Map<String, Object> map);


}
