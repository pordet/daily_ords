package kr.co.daily.admin.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.dao.IdentityManageDAO;
import kr.co.daily.admin.service.BatchManageService;
import kr.co.daily.admin.service.IdentityManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class BatchManageController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private BatchManageService service;
	@Autowired private IdentityManageService idenService;
	@Autowired private IdentityManageDAO idenDao;
	
	@RequestMapping("/adm/batch/batchResultList.do")
	public String menuList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		this.addParamByBetween(commandMap, getMyAccounId(), getLoginUserId(), 0)
		try {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		CommandMap param = new CommandMap();
		param.put("id_meta_m", 1);
		CustomMap idenMap = idenService.getIdentityM(param);
		idenMap.put("master_yn", "Y");
//		idenMap.get("")
		List<CustomMap> relList = idenService.selectIdentityRelList(param);
		relList.add(idenMap);
		for(CustomMap rel : relList) {
			idenMap.put("master_yn", "N");
			CommandMap subParam = new CommandMap();
			subParam.put("id_meta_m", rel.getInt("id_meta_m",0));
			List<CustomMap> subHeaderList = idenService.selectIdentityList(subParam);
			rel.put("header_list", subHeaderList);
			List<CustomMap> subDetailParamList = idenService.selectIdentityDetailParamList(param);
			rel.put("detail_param_list", subDetailParamList);
		}
		model.addAttribute("relList", relList);
		List<CustomMap> headerList = idenService.selectIdentityList(param);
		model.addAttribute("headerList", headerList);
		List<CustomMap> detailParamList = idenService.selectIdentityDetailParamList(param);
		model.addAttribute("detailParamList", detailParamList);
		idenMap.put("header_list", headerList);
		idenMap.put("detail_param_list", detailParamList);
		model.addAttribute("idenMap", idenMap);
		commandMap.put("succ_yn_id_cg", 8);
		if("".equals(commandMap.getString("id_service",""))) {
			commandMap.put("id_service", GlobalConst.META_M_TYPE_BID);
		}
		}catch(Exception e) {
			System.out.println("aa");
		}
		PageList<CustomMap> resultList = service.selectBatchResultList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/batchResultList";
	}

	@RequestMapping("/adm/batch/batchDetailAjax.do")
	public String batchDetailAjax(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			CommandMap map = new CommandMap();
			Locale locale = localeResolver.resolveLocale(req);
			map.put("locale", locale.toString());
			CommandMap param = new CommandMap();
			param.put("id_meta_m", 2);
			CustomMap idenMap = idenService.getIdentityM(param);
			model.addAttribute("idenMap", idenMap);
			//(팝업에서도 이것이 있을 때 사용)
			List<CustomMap> relList = idenService.selectIdentityRelList(param);
			for(CustomMap rel : relList) {
				CommandMap subParam = new CommandMap();
				subParam.put("id_meta_m", rel.getInt("id_meta_m",0));
				List<CustomMap> subHeaderList = idenService.selectIdentityList(subParam);
				rel.put("header_list", subHeaderList);
				List<CustomMap> subDetailParamList = idenService.selectIdentityDetailParamList(param);
				rel.put("detail_param_list", subDetailParamList);
			}
			model.addAttribute("relList", relList);
			List<CustomMap> headerList = idenService.selectIdentityList(param);
			model.addAttribute("headerList", headerList);
			commandMap.put("succ_yn_id_cg", 8);
			List<CustomMap> detail = service.selectBatchDetailByIdBatch(commandMap);
			model.addAttribute("detail", detail);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}

}
