package daily.common.base;

public abstract class AbstractPageVoBase {
	private Integer limit;
	private Integer offset;
	private String	sidx;
	/**
	 * 정렬에 사용되는 클래스
	 */
	private String	sord;
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}

}
