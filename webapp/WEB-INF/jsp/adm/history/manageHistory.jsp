<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="자료실 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/manage_history_register.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
<!--
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;">
	                 <input type="hidden" name="id_master" id="id_master">
	                 <input type="hidden" name="id_board_master" id="id_board_master">
	                 <input type="hidden" name="id_parent_board" id="id_parent_board">
	                 <input type="hidden" name="ord_str" id="ord_str">
                    <div class="row">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 100px">
	                                  <div class="tit_cont">검색종류
	                                  </div>
										<select class="form-control mh-form-control" title="검색종류" name="search_type" id="search_type">
											<option value="t" ${(param.search_type eq 't'?'selected':'')}>자료실명</option>
											</select>
	                                 </div>
		                              <div class="form-group inline-group" style="width: 300px">
		                                  <div class="tit_cont">검색어</div>
		                                  <input type="text" class="form-control mh-form-control" name="search_keyword" value=" ${(not empty param.search_keyword ?param.search_keyword:'')}">
		                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
	                       		<div class="fr">
	                              <button type="button" class="btn h34" onclick="javascript:regNewBoardMaster();"  style="margin-top:20px;">등록</button>
	                            </div>
	                            </div>
	                    </div>
	                </div>
	                       <div class="row">
								<div class="col-12">
				                    <div class="grid-area">
				                        <div class="grid-header">
				                            <div class="header-nomarl-unit" style="width:15%;text-align: center;">자료유형</div>
				                            <div class="header-nomarl-unit" style="width:40%;text-align: center;">자료실명</div>
				                            <div class="header-nomarl-unit" style="width:17%;text-align: center;">관리</div>
				                            <div class="header-nomarl-unit" style="width:5%;text-align: center;">레벨</div>
				                            <div class="header-nomarl-unit" style="width:8%;text-align: center;">사용여부</div>
				                            <div class="header-nomarl-unit" style="width:15%;text-align: center;">수정일</div>
				                        </div>
				                        <div class="grid-contents">
				                            <c:forEach var="map" items="${list}" varStatus="i">
				                            <div class="grid-loof-cont" ondblclick="javascript:viewData(${map.id_master},${map.id_board_master},'${map.ord_str}');">
				                                <div class="cont-unit" style="width:15%;text-align: center;">${map.title}</div>
				                                <div class="cont-unit" style="width:40%;white-space:pre;"><c:out value="${map.board_nm}"/></div>
				                                <div class="cont-unit" style="width:17%;text-align: center;">
				                                <c:choose>
				                                	<c:when test="${map.lv lt 2}">
				                                <a href="javascript:void(0);" onclick="javascript:regChild(${map.id_master},${map.id_board_master},'${map.ord_str}');"><span class="label_type02">하위 추가</span></a>
				                                	</c:when>
				                                	<c:otherwise>
				                                		&nbsp;
				                                	</c:otherwise>
				                                </c:choose>
				                                </div>
				                                <div class="cont-unit" style="width:5%;text-align: center;">${map.lv}</div>
				                                <div class="cont-unit" style="width:8%;text-align: center;">${map.use_yn}</div>
				                                <div class="cont-unit" style="width:15%;text-align: center;">${map.reg_date}</div>
				                            </div>
				                            </c:forEach>
				                        </div>
				                    </div>
				                    </div>
				                </div>
									<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
			</form>
-->
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>자료실 관리</span>
			</div>
			
			<div class="page-content-wrap">
			
				<form id="frm" name="frm">
					<input type="hidden" name="id_master" id="id_master">
					<input type="hidden" name="id_board_master" id="id_board_master">
					<input type="hidden" name="id_parent_board" id="id_parent_board">
					<input type="hidden" name="ord_str" id="ord_str">
					
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div>
									<select title="검색종류" name="search_type" id="search_type">
										<option value="t" ${(param.search_type eq 't'?'selected':'')}>자료실명</option>
									</select>
								</div>
								<div class="search-group-single-input">
									<input type="text" name="search_keyword" value="${(not empty param.search_keyword ?param.search_keyword:'')}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
							</div>
						</div>
					</div>
					
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button type="button" class="btn-primary" onclick="javascript:regNewBoardMaster();">등록</button>
<!-- 							<button type="button" class="btn-danger">삭제</button> -->
						</div>					
					</div>
					
					
							<div class="boardBox list-set" tabindex="0">
						<table id="result_table">
							<colgroup>
<%-- 								<col width="3%">  --%>
								<col width="16%"> 
								<col width="16%"> 
								<col width="10%"> 
								<col width="8%"> 
								<col width="8%"> 
								<col width="14%"> 
							</colgroup>
							<thead>
								<tr>	
<!-- 									<th> -->
<%-- 										<input name="check" type="checkbox" value="${c.id_meta}" id="check_${c.id_meta}" > --%>
<!-- 									</th> -->
									<th scope="col">자료유형</th>	
									<th scope="col">자료실명</th>	
									<th scope="col">관리</th>	
									<th scope="col">레벨</th>	
									<th scope="col">사용여부</th>	
									<th scope="col">수정일</th>	
								</tr>
							</thead>
							<tbody>		
								<c:choose>
									<c:when test="${fn:length(list) == 0 }">
										<tr><td colspan="6">검색결과없음</td></tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="map" items="${list}" varStatus="i">	
											<tr id="row${i.count}" ondblclick="javascript:viewData(${map.id_master},${map.id_board_master},'${map.ord_str}');">
<!-- 												<td> -->
<%-- 													<input class="" type="checkbox" value="${map.id_board_master}" name="del_code" id="del_code${i.count}"> --%>
<!-- 												</td> -->
												<td>${map.title}</td>
												<td>${map.board_nm}</td>
												<td>
												<c:if test="${map.lv lt 2}">
													<a href="javascript:void(0);" onclick="javascript:regChild(${map.id_master},${map.id_board_master},'${map.ord_str}');">
														하위 추가
													</a>
												</c:if>
												</td>
												<td>${map.lv}</td>
												<td>${map.use_yn}</td>
												<td>${map.reg_date}</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
/*
function registerData(){
	$("#dataModal").modal('show');	
}

function changeCategory(that){
	var new_val = $(that).val();
	search();
}

$('#dataModal').on('hidden.bs.modal', function () {
	refresh();
});
$('#dataViewModal').on('hidden.bs.modal', function () {
	refresh();
});
*/
function refresh(){
	var frm =document.frm;
	frm.action=contextPath+"/manageHistory.do";
		$("#page").val(page);
//	frm.action="/excelDownload.do";
	$("#frm").submit();
	
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/manageHistory.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
</script>
</html>