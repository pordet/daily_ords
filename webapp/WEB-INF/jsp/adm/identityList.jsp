<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="화면 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/perm_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="newPage" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<c:if test="${fn:length(srchParamList)>0}">
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
									<select name="search_col" id="search_col">
				                        <c:forEach var="map" items="${srchParamList}" varStatus="i">
										<option value="${map.param_cd}" ${(map.param_cd eq param.search_col || i.count eq 1? 'selected':'')}>${map.param_nm}</option>
				                        </c:forEach>
									</select>
									<input type="text" class="" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
					</c:if>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m"/>
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button class="btn-danger" onClick="javascript:deleteList();">화면 삭제</button>
						</div>					
					</div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty idenMap.grid_width}">
				    <c:set var="grid_width" value="style='width:${idenMap.grid_width}%;'" />
				</c:if>
	            <div class="grid-area" ${grid_area_style}>
                    <!-- 그리드 해더영역 -->
                    <div class="grid-header" ${grid_width}>
                        <c:forEach var="map" items="${headerList}" varStatus="i">
                        <div class="header-unit">
                        <input type="hidden" id="h_${map.meta_cd}" value="${map.meta_cd}">
                        ${map.meta_nm}<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
                        </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                        </c:forEach>
                    </div>
                    <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
                    <div class="grid-contents" ${grid_width}>
                    <c:forEach var="low" items="${list}" varStatus="i">
                        <div class="grid-loof-cont" id="row_${i.count}" ondblclick="javascript:openReg('${low.id_meta_m}')">
                        <c:forEach var="col" items="${headerList}" varStatus="j">
<%--                         col=[${col}]<br> --%>
<%--                         low=[${low}] --%>
                        	<c:set var="meta_cd" value="${col.meta_cd}"/>
                         <div class="cont-unit">
                         <c:choose>
                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
                         	${low[meta_cd]}
                         	</c:when>
                         	<c:otherwise>
                         	<a href="${low[meta_nm]}" target="blank">${low[meta_nm]}</a>
                         	</c:otherwise>
                         </c:choose>
           				 </div>
                    	</c:forEach>
                        </div>
                    </c:forEach>
                    </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>

			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
// 	var act_type = "${param.act_type}";
// 	var ret = "${param.ret}";
// 	if(act_type =='D'){
// 		if(ret==0){
// 			infoOpen('성공하였습니다.');
// 		}else if(ret==1){
//     		alertOpen('실패하였습니다.');
// 		}
// 	}
});
function openReg(id_meta_m){
	var frm =document.frm;
	if(id_meta_m){
		$("#trg_id_meta_m").val(id_meta_m);
	}
	frm.action=contextPath+"/adm/meta/regIdentity.do";
	frm.submit();
}
function initSearchForm(){
	$("#search_keyword").val('');
}
function openIdentity(id_meta_m){
	var frm =document.frm;
//	$("#id_meta_m").val(id_meta_m);
	$("#trg_id_meta_m").val(id_meta_m);

	frm.action=contextPath+"/adm/meta/regIdentity.do";
	frm.submit();
}

function search(){
	doList(1);
}
function doList(page){
	processing();
	var frm =document.frm;
	frm.action=contextPath+"/adm/meta/identityList.do";
		$("#page").val(page);
    	$("#frm").submit();
}
</script>
</html>
