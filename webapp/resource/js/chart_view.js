/*
function show_chart(){
	$("#chart_ch_id_list").val('');
	if($("#st_year").val()==''){
		alertOpen('검색시작년도를 선택하십시오.');
		$("#st_year").focus();
		return;
	}
	if($("#ed_year").val()==''){
		alertOpen('검색종료년도를 선택하십시오.');
		return;
	}
	var ids =  $("input[name=check]:checked");
	var list = [];
//	var rowspan = 1;
	var grChkCnt = 0;
	$.each(ids,function(){
		list.push(this.value);
		var rows = nanToStr($("#check_"+this.value).parent().attr("rowspan"));
		if(rows>1){
			grChkCnt++;
		}
	});
	if(grChkCnt>1){
		alertOpen('하위항목을 가진 항목은 하나 이상 선택할 수 없습니다.');
		return;
	}
	if(nanToStr(list)=='' || list.length==0){
		alertOpen('차트에 사용할 항목을 선택하십시오.');
		return;
	}
	var ch_id_list ="";
	for(var i=0;i<list.length;i++){
		if(i==0){
			ch_id_list+=list[i];
		}else{
			ch_id_list+=","+list[i];
		}
	}
	if(grChkCnt==1 && list.length>1){
		alertOpen('하위항목을 가진 항목과 단일항목을 동시에 선택할 수 없습니다.');
		return;
	}
	$("#chart_ch_id_list").val(ch_id_list);
	if($("#id_city_area").val()!=''){
		$("#chart_id_city_area").val($("#id_city_area").val());
		$("#chart_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#chart_id_nat_area").val($("#id_nat_area").val());
	}
	$("#chart_st_year").val($("#st_year").val());
	$("#chart_ed_year").val($("#ed_year").val());
	
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#chart_id_category").val($("#id_category").val());
	}
	var params = $("#chart_frm").serialize();
	var url = contextPath+"/drawChartAjax.do";
	processing();
	$("#chartCon").empty();
	var html='<div class="modal-chart-area"   id="chartCont1" style="width:720px;height:475px;" ></div>';
	$("#chartCon").html(html);
	var myChart = echarts.init(document.getElementById("chartCont1"));
	var seriesList = [];
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
		    	$.each(e.series,function(i,d){
		    		seriesList.push(
	                    {
	                    	name:d.name,
	                    	type:'line',
	                    	data:d.data
	                    }
	                )
		    	});
		    	myChart._chartsViews = [];
		    	myChart._chartsMap =[];
		    	myChart.setOption({
		    	    title: {
		    	        text: ' '
		    	    },
		    	    tooltip: {
		    	        trigger: 'axis',
		    	        axisPointer: {
		    	            type: 'shadow'
		    	        }
		    	    },
		    		legend: {
		                data: e.legend
		            },
		    	    grid: {
		    	        left: '3%',
		    	        right: '4%',
		    	        bottom: '3%',
		    	        containLabel: true
		    	    },
		    	    xAxis: {
		    	        type: 'category',
		    	        data: e.xAxis,
		    	        boundaryGap: false
//		    	        boundaryGap: [0, 0.01]
		    	    },
		    	    yAxis: {
		    	        type: 'value',
		    	    },
		            series: seriesList
		            
		        });
		    	$("#chartModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
*/

function show_chart(){
	$("#chart_ch_id_list").val('');
	if($("#st_year").val()==''){
		alertOpen('검색시작년도를 선택하십시오.');
		$("#st_year").focus();
		return;
	}
	if($("#ed_year").val()==''){
		alertOpen('검색종료년도를 선택하십시오.');
		return;chart_ed_year
	}
	var ids = $("input[name=check]:checked");
	var list = [];
	var grChkCnt = 0;
	$.each(ids,function(){
		list.push(this.value);
		var dataType = nanToStr($("#check_"+this.value).attr("dataType"));
		if(dataType==2 || dataType==7){
			grChkCnt++;
		}
	});
	if(grChkCnt>1){
		alertOpen('하위항목을 가진 항목은 하나 이상 선택할 수 없습니다.');
		return;
	}
	if(nanToStr(list)=='' || list.length==0){
		alertOpen('차트에 사용할 항목을 선택하십시오.');
		return;
	}
	var ch_id_list ="";
	for(var i=0;i<list.length;i++){
		if(i==0){
			ch_id_list+=list[i];
		}else{
			ch_id_list+=","+list[i];
		}
	}
	if(grChkCnt==1 && list.length>1){
		alertOpen('하위항목을 가진 항목과 단일항목을 동시에 선택할 수 없습니다.');
		return;
	}
	$("#chart_ch_id_list").val(ch_id_list);
	if(iniParam.cityArea != ''){
		$("#chart_id_city_area").val(iniParam.cityArea);
		$("#chart_id_nat_area").val(iniParam.natArea);
	} else {
		$("#chart_id_nat_area").val(iniParam.natArea);
	}
	$("#chart_st_year").val(iniParam.startYear);
	$("#chart_ed_year").val(iniParam.endYear);
	$("#chart_id_category").val(iniParam.category);
	
	var params = $("#chart_frm").serialize();
	var url = contextPath+"/drawChartAjax.do";
	processing();
	$("#chartCon").empty();
	var html='<div class="modal-chart-area" id="chartCont1" style="width:720px;height:475px;" ></div>';
	$("#chartCon").html(html);
	var myChart = echarts.init(document.getElementById("chartCont1"));
	var seriesList = [];
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
		    	$.each(e.series,function(i,d){
		    		seriesList.push(
	                    {
	                    	name:d.name,
	                    	type:'line',
	                    	data:d.data
	                    }
	                )
		    	});
		    	myChart._chartsViews = [];
		    	myChart._chartsMap =[];
		    	myChart.setOption({
		    	    title: {
		    	        text: ' '
		    	    },
		    	    tooltip: {
		    	        trigger: 'axis',
		    	        axisPointer: {
		    	            type: 'shadow'
		    	        }
		    	    },
		    		legend: {
		                data: e.legend
		            },
		    	    grid: {
		    	        left: '3%',
		    	        right: '4%',
		    	        bottom: '3%',
		    	        containLabel: true
		    	    },
		    	    xAxis: {
		    	        type: 'category',
		    	        data: e.xAxis,
		    	        boundaryGap: false
		    	    },
		    	    yAxis: {
		    	        type: 'value',
		    	    },
		            series: seriesList
		            
		        });
		    	$("#chartModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}