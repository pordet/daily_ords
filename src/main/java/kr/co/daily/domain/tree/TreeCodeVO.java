package kr.co.daily.domain.tree;

public class TreeCodeVO {
	private String id_justiciable;
	private String locale;
	private String code_group;
	private String type;

	private String codeName;

	public String getId_justiciable() {
		return id_justiciable;
	}

	public void setId_justiciable(String id_justiciable) {
		this.id_justiciable = id_justiciable;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCode_group() {
		return code_group;
	}

	public void setCode_group(String code_group) {
		this.code_group = code_group;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
