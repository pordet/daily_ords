<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<c:set var="msg" value="request" />
<commc:MenuTag user_seq="${id_user}" locale="${currLocale}" listName="menuList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
        <!-- 레프트네비 -->
        <aside class="mh-side-nav">
            <section class="mh-sn-bar">
                <!-- side bar -->
                <div class="mh-sn-menuwrap">
                    <nav class="mh-sn-menu"><!-- li 요소에 .mh-open 클래스를 넣으면 선택된 요소로 활용 -->
                        <ul class="mh-menu-list">
                        <c:forEach var="menu" items="${menuList}">
                            <li class="${(menu.selected? 'mh-open':'mh-mlview' )}"><a 
                            <c:choose>
                               <c:when test="${menu.child_cnt eq 0}">
	                               <c:if test="${menu.menu_url!=''}">
	                            href="<c:url value='${menu.menu_url}'/>?id_menu=${menu.idMenu}"
	                               </c:if>
	                               <c:if test="${menu.menu_url eq ''}">
	                            href="#"  class="js-action-a"
	                               </c:if>
                               </c:when>
                               <c:otherwise>
                                    href="#" class="js-action-a"
                               </c:otherwise>
                            </c:choose>
                             data-toggle="tooltip" data-placement="right" title="${menu.menu_nm}"><span class="${menu.menuStyleClass}"></span>${menu.menu_nm}</a>
                            <c:choose>
                               <c:when test="${menu.child_cnt eq 0}"></c:when>
                               <c:otherwise>
                                <ul class="mh-mlview-menu" ${(menu.childSelected? 'style="display:block;"':'' )}">
                                   <c:forEach var="subMenu" items="${menu.child}">
                                     <li <c:if test="${subMenu.selected}">class="mh-open"</c:if>><a href="<c:url value='${subMenu.menu_url}'/>?id_menu=${subMenu.idMenu}">${subMenu.menu_nm}</a></li>
                                   </c:forEach>
                                </ul>
                               </c:otherwise>
                            </c:choose>
                             </li>
                        </c:forEach>
                        </ul>
                    </nav>
                </div>
                <div class="footer-content">
                    <span class="copyright-img">
<%--                     <img src="<c:url value='/resource/img/icon_gray.png'/>" alt=""/> --%>
                    <em>THE KOREA TRANSPORT INSTITUTE</em></span>
                    <span class="copyright-footer">Copyright &copy; 2019 2018. <br>THE KOREA TRANSPORT INSTITUTE<br>All Rights Reserved.</span>
                </div>
            </section>
        </aside>
        <!-- //레프트네비 -->
<script>
	$(function () {
	   $('.js-action-a').click(function (e) {
			e.preventDefault();
		});
	});
</script>