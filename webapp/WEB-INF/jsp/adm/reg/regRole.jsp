<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="역할관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/role_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:choose>
					<c:when test="${empty param.id_role}">
						<span id="cur_title">Role 등록</span>
					</c:when>
					<c:otherwise>
						<span id="cur_title">Role 수정</span>
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" id="page" value="${param.page}">
					<input type="hidden" name="id_role" id="id_role" value="${param.id_role}">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>Role 이름</td>
									<td>
										<input type="text" id="role_nm" name="role_nm" value="${map.role_nm}">
									</td>
								</tr>
								<tr>
									<td>설명</td>
									<td>
										<input type="text" id="role_desc" name="role_desc" value="${map.role_desc}">
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="ra1" <c:if test="${empty map.use_yn or map.use_yn eq 'Y'}">checked</c:if> />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="ra2" <c:if test="${not empty map.use_yn and map.use_yn ne 'Y'}">checked</c:if>/>
												<label for="ra2">아니요</label>
											</div>
										</div>
										
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" id="btnList">목록</button>
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" id="btnInit">초기화</button>
							<button type="button" class="btn-danger" id="btnDelete">삭제</button>
							<button type="button" class="btn-primary" id="btnSave">등록</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</adm:AdmBody>
<script>
</script>
</html>
