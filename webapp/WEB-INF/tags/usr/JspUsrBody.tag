<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="usr" tagdir="/WEB-INF/tags/usr" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>
<%@ attribute name="currLocale" required="true" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />



<script type="text/javascript" language="javascript">
 
$(document).ready(function(){
	
	   //$('#Progress_Loading').hide(); //첫 시작시 로딩바를 숨겨준다.
	   //infoOpen($('#Progress_Loading').html());
		$('#progressModal').modal('hide');
	})
	.ajaxStart(function(){
		//$('#Progress_Loading').show(); //ajax실행시 로딩바를 보여준다.
		//debugger;
		 //progressOpen($('#progressModal').html());
	})
	.ajaxStop(function(){
		//$('#Progress_Loading').hide(); //ajax종료시 로딩바를 숨겨준다.
		//$("#progressModal2").hide();
		//$('#progressModal').hide();
		
		
	  
	});
 
</script>
<style type = "text/css"> <!-- 로딩바스타일 -->

#Progress_Loading
{
 position: absolute;
 left: 50%;
 top: 50%;
 background: #ffffff;
}
</style>
<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
    <div class="mh-wrapper">[[]]
        <usr:JspUsrReqHead currLocale="${currLocale}"/>
        <jsp:doBody/>
      <!--//컨텐츠영역-->
        <usr:JspUsrTail/>
    </div>
    <c:if test="${hasPop}">
        <usr:JspUsrDialog/>
    </c:if>
	<div class="spinner" id="spinnerDiv" style="display:none;">
        <div class="in"><img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/></div>
    </div>
    <!-- //spinner  -->
    <!-- modal type02  알림타입 -->
    <div class="modal fade mh-modal" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body">
                    <p>We sent the confirmation number.<br>Please check your e-mail.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal alert -->
    <div class="modal fade mh-modal" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body red-type" id="alertMsg">
                    <p id="alertMsg">기존 사용자가 존재합니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal info -->
    <div class="modal fade mh-modal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body" id="alertMsg">
                    <p id="infoMsg">기존 사용자가 존재합니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade mh-modal" id="progressModal" name ="progressModal" tabindex="-1" role="dialog" aria-labelledby="progressModal">
        <div class="modal-dialog modal-dialog-centered" role="document" id="progressModal2" name ="progressModal2">
            <div class="modal-content alert-modal"  >
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body" id="progressMsg">
                    <p>Processing</p>
<!--                     <img src="/crs/resource/img/loadingbar.gif"/> -->
                </div>
<!--                 <div class="modal-footer"> -->
<!--                     <button type="button" class="btn" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button> -->
<!--                 </div> -->
            </div>
        </div>
    </div>
    <!-- modal confirm-->
    <div class="modal fade mh-modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content alert-modal">
                <!-- .alert-modal 붙여줌-->
                <div class="modal-body blue-type" id="alertMsg">
                    <p id="confirmMsg">기존 사용자가 존재합니다.</p>
                </div>
                <input type="hidden" id="confirm_yn" value="N">
                <input type="hidden" id="confirm_func_nm">
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" onclick="javascript:okModel('Y');">확인</button>
                    <button type="button" class="btn cancel" data-dismiss="modal" onclick="javascript:okModel('N');">취소</button>
                </div>
            </div>
        </div>
    </div>
    <!-- //modal type02  -->
    <!-- //modal type02  -->
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap/js/bootstrap.min.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <script src="<c:url value='/resource/js/datetime.locale.js'/>"></script>
    <!-- nppa JS -->
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
    <script src="<c:url value='/resource/js/comm_ords.js'/>"></script>
    <script>
	function processing(){
		$("#spinnerDiv").show();
	}
	function endProcessing(){
		$("#spinnerDiv").hide();
	}
    </script>
     <script>
	<c:if test="${hasPop}">
	function alertOpen(msg){
	  	$("#alertMsg").html(msg);
	  	$('#alertModal').modal('show');
	}
	function infoOpen(msg){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	}
	function infoOpen(msg,func){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function progressOpen(msg){
	  	$("#progressMsg").html(msg);
	  	$('#progressModal').modal('show');
	}
	function confirmOpen(msg,func){
	  	$("#confirmMsg").html(msg);
	  	$('#confirmModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function closeModel(){
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	function okModel(key){
		if(key=='Y'){
			$("#confirm_yn").val('Y');
			var funcNm = $("#confirm_func_nm").val();
			eval(funcNm+"();");
		}
		else{
			$("#confirm_yn").val('N');
		}
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	</c:if>
    </script>
</body>

