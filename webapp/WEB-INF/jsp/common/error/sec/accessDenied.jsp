<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Error</title>
    <!-- nppa CSS -->
    <link rel="stylesheet" href="<c:url value='/resource/css/nppa.css'/>">
    <!-- nppa SVG -->
    <link rel="stylesheet" href="<c:url value='/resource/css/nppa_svg.css'/>">
</head>
<body class="nopage">
    <div class="nopage-contents">
        <div class="nopage-area">
<c:choose>
	<c:when test="${logined eq 'Y'}">
	<h1>403</h1>
            <p><strong>접근권한이 없습니다2.</strong></p>
		<button type="button" class="btn" onclick="javascript:logout();">로그아웃</button>
	</c:when>
	<c:otherwise>
	<h1>403</h1>
    <p><strong>접근권한이 없습니다3.</strong></p>
	</c:otherwise>
	
</c:choose>
		</div>
	</div>
</body>
<script>
function logout(){
	   var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "Post");  //Post 방식
    form.setAttribute("action", "<c:url value='/logout.do'/>"); //요청 보낼 주소
    document.body.appendChild(form);
    form.submit();
}
</script>
</html>