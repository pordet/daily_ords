<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${idenMap.meta_m_nm}"/>
<front:FrontDynamicHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
			<div class="page-title-wrap">
				<span>${title}</span>
			</div>
			
			<div class="page-content-wrap">
			<form name="frm" id="frm" method="post">
				<input type="hidden" id="id_bid_noti" name="id_bid_noti"/>
				<input type="hidden" id="reg_yn" name="reg_yn"/>
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="trg_id_meta_m" name="trg_id_meta_m" value="${param.trg_id_meta_m}"/>
				<input type="hidden" id="sys_code" name="sys_code" value="${param.sys_code}"/>
          	<c:forEach var="map" items="${detailParamList}" varStatus="i">
                <input type="hidden" id="${map.meta_cd}" name="${map.meta_cd}"/>
          	</c:forEach>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
			              	<div class="tit_cont">검색조건</div>
			              	<div>
					          	<select class="form-control mh-form-control" name="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${headerList}" varStatus="i">
					                <option value="${map.id_meta}" ${(param.search_col eq map.id_meta? 'selected':'')}>${map.meta_nm}</option>
					          	</c:forEach>
					       		</select>
			          		</div>
							<div class="search-group-single-input">
		              			<div class="tit_cont">검색어</div>
		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${param.search_keyword}">
										<button type="button" class="btn-primary" onclick="javascript:search();">
											<span class="btn_search"></span>
											검색
										</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
					</div>
					<div class="r">
<!-- 						<button type="button" class="btn h34 fl" onClick="javascript:openRegContreg();"><span class="btn_add"></span>수동입력</button> -->
					</div>					
				</div>
				<c:set var="grid_area_style" value=""/>
	            <c:if test="${not empty idenMap.grid_width and idenMap.grid_width>100}">
				    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
				</c:if>
				<c:set var="grid_width" value=""/>
				<c:if test="${not empty idenMap.grid_width}">
				    <c:set var="grid_width" value="style='width:${idenMap.grid_width}%;'" />
				</c:if>
	            <div class="grid-area" ${grid_area_style}>
                    <!-- 그리드 해더영역 -->
                    <div class="grid-header" ${grid_width}>
                        <c:forEach var="map" items="${headerList}" varStatus="i">
                        <div class="header-unit">
                        <input type="hidden" id="h_${map.id_meta}" value="${map.id_meta}">
                        ${map.meta_nm}<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
                        </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                        </c:forEach>
                    </div>
                    <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
                    <div class="grid-contents" ${grid_width}>
                    <c:forEach var="row" items="${list}" varStatus="i">
                    	<c:choose>
                    		<c:when test="${idenMap.detl_yn eq 'Y'}">
                       			<div class="grid-loof-cont" id="row_${i.count}" ondblclick="javascript:open${idenMap.upr_detl_meta_m_cd}(${idenMap.detl_id_meta_m},'row_${i.count}')">
						          	<c:forEach var="map" items="${detailParamList}" varStatus="j">
						                <input type="hidden" id="row_${i.count}_${map.meta_cd}" value="${row[map.meta_cd]}"/>
						          	</c:forEach>
			                        <c:forEach var="col" items="${headerList}" varStatus="j">
			                        	<c:set var="col_cd" value="${col.meta_cd}"/>
				                        <div class="cont-unit">
				                        <c:choose>
				                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
				                         	${row[col_cd]}
				                         	</c:when>
				                         	<c:otherwise>
				                         	<a href="${row[meta_cd]}" target="blank">${row[col_cd]}</a>
				                         	</c:otherwise>
				                        </c:choose>
				           				</div>
                    				</c:forEach>
                    			</div>
                    		</c:when>
                    		<c:otherwise>
                    		<c:when test="${idenMap.detl_yn eq 'Y'}">
                       			<div class="grid-loof-cont" id="row_${i.count}">
			                        <c:forEach var="col" items="${headerList}" varStatus="j">
			                        	<c:set var="col_cd" value="${col.meta_cd}"/>
				                        <div class="cont-unit">
				                        <c:choose>
				                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
				                         	${row[col_cd]}
				                         	</c:when>
				                         	<c:otherwise>
				                         	<a href="${row[meta_cd]}" target="blank">${row[col_cd]}</a>
				                         	</c:otherwise>
				                        </c:choose>
				           				</div>
                    				</c:forEach>
                    			</div>
                    		</c:when>
                    		</c:otherwise>
                    	</c:choose>
                    </c:forEach>
                    </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
                   <!-- //paging -->
			</form>
	    </div>
<c:forEach var="rel" items="${relList}">
	<c:if test="${rel.detl_type eq GlobalConst.DETL_TYPE_POP}">
		<c:set var="subHeaderList" value="${rel.header_list}"/>
		<c:set var="subDetailParamList" value="${rel.detail_param_list}"/>
		<c:set var="subDetailList" value="${rel.detail_list}"/>
	    <div class="modal fade mh-modal" id="${rel.upr_meta_m_cd}" tabindex="-1" role="dialog" aria-labelledby="${rel.upr_meta_m_cd}">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h3 class="modal-title">${rel.meta_m_nm}</h3>
	                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
	                </div>
	                <div class="modal-body"<c:if test="${not empty rel.pop_width}"> style="width: ${rel.pop_width};"</c:if>>
						<c:set var="grid_style" value=""/>
						<c:if test="${not empty rel.grid_width}">
						    <c:set var="grid_style" value="style='width:${rel.grid_width}%;'" />
						</c:if>
						<c:set var="grid_area_style" value=""/>
			            <c:if test="${not empty rel.grid_width and rel.grid_width>100}">
						    <c:set var="grid_area_style" value="style='overflow-x: scroll;'"/>
						</c:if>
			            <div class="grid-area" ${grid_area_style}>
<!-- 		                    그리드 해더영역 -->
		                    <div class="grid-header" id="sub_header_${rel.meta_m_cd}" ${grid_style}>
		                        <c:forEach var="map" items="${subHeaderList}" varStatus="i">
		                        <div class="header-unit">
		                        <input type="hidden" id="h_${map.id_meta}" value="${map.id_meta}">
		                        ${map.meta_nm}<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
		                        </div>
		                        </c:forEach>
		                    </div>
<!-- 		                    //그리드 해더영역 -->
<!-- 			                그리드 컨텐츠영역 -->
		                    <div class="grid-contents" id="sub_conts_${rel.meta_m_cd}" ${grid_style}>
		                        <c:forEach var="map" items="${subDetailList}" varStatus="i">
			                        <div class="cont-unit">
			                        ${map.col_ }
			                        <c:choose>
			                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
			                         	${row[col_cd]}
			                         	</c:when>
			                         	<c:otherwise>
			                         	<a href="${row[meta_cd]}" target="blank">${row[col_cd]}</a>
			                         	</c:otherwise>
			                        </c:choose>
			           				</div>
					           	</c:forEach>
		                    </div>
<!-- 			                        //그리드 컨텐츠영역 -->
			            </div>
	                </div>
<!-- 								modal-body end -->
	                <div class="modal-footer mt-20p" id="btnArea1" style="display:block;">
		                    <button type="button" class="btn" data-dismiss="modal">닫기</button>
	               </div>
	              </div>
	    	</div>
	    </div>
	</c:if>
</c:forEach>

<script>
function doList(page){
	$("#page").val(page);
	var frm =document.frm;
		frm.action=contextPath+"/adm/batch/batchResultList.do";
	processing();
	frm.submit();
// 	$("#BatchResult").show();
}
function initSearchForm(){
	$("#search_col option:eq(0)").attr("selected","selected");
	$("#search_keyword").val('');
}
</script>
<c:forEach var="rel" items="${relList}">
	<c:if test="${rel.detl_type eq GlobalConst.DETL_TYPE_POP}">
    <c:set var="functionName" value="open${rel.upr_meta_m_cd}" />
    <c:set var="param1" value="${rel.id_meta_m}" />
    <c:set var="popId" value="${rel.meta_m_cd}" />
    <script>
        function ${functionName}(param1,id_row) {
        	$("[id^='"+id_row+"_']").each(function(){
        		var strLength = id_row.length;
        		var trgt_id = $(this).attr("id").substring(strLength+1);
        		$("#"+trgt_id).val($(this).val());
        	});
        	var params = $("#frm").serialize();
        	var url = contextPath+"/adm/batch/batchDetailAjax.do";
        	processing();
        	$.ajax({
        		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
        	    type: "POST", // HTTP 요청 방식(GET, POST)
        	    data : params,
        	    dataType: "json" ,
        	    success : function(e){
        	    	endProcessing();
        	    	if(e.ret==1){
        	    		if(insert_yn =='N'){
        					if(nanToStr(e.err_msg)!=''){
        	    				alertOpen(e.err_msg+'./n 다음과 같은 오류가 발생하였습니다.');
        					}else{
        	    				alertOpen('처리 과정중 알 수 없는 오류 때문에 등록에 실패하였습니다.');
        					}
        				}
        	    	}else{
           				const listContentContainer = $('#sub_conts_${rel.meta_m_cd}');
           				listContentContainer.html('');
           				
                        $.each(e.detail, function (rowIndex, row) {
                            const rowContainer = $('<div>').addClass('grid-loof-cont').attr('id', 'row_' + rowIndex);

                            if (e.idenMap.detl_yn === 'Y') {
                                rowContainer.on('dblclick', function () {
                                    openDetail('row_' + rowIndex);
                                });

                                $.each(e.detailParamList, function (j, map) {
                                    $('<input>')
                                        .attr('type', 'hidden')
                                        .attr('id', 'row_' + rowIndex + '_' + map.meta_cd)
                                        .val(row[map.meta_cd])
                                        .appendTo(rowContainer);
                                });
                            }

                            // 수정된 부분: e.headerList를 순회하며 해당 컬럼 키를 이용하여 값을 가져옴
                            $.each(e.headerList, function (j, col) {
                                const col_cd = col.meta_cd;
                                const colContainer = $('<div>').addClass('cont-unit');

                                const colValue = row[col_cd]; // 해당 컬럼 키를 이용하여 값 가져오기
                                colContainer.text(colValue);
                                colContainer.appendTo(rowContainer);
                            });

                            rowContainer.appendTo(listContentContainer);
                        });
                        $("#${rel.upr_meta_m_cd}").modal('show');
        	    	}
        	    },
        	    error : function(e){
        	    	endProcessing();
        	    	if(e.status=='403'){
        		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
        	    	}else if(e.status=='401'){
        			    alertOpen('접근 권한이 없습니다.');
        	    	}else{
        		    	alertOpen('실패하였습니다.');
        	    	}
        	    }
        	});
         }
    </script>
	</c:if>
</c:forEach>

</front:FrontDynamicBody>
</html>