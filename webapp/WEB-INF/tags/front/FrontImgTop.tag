<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

<div class="_content_title">
    <table style="width:100%;border=0">
    	<tr>
    		<td width="277px">
				<div class="_logo">
					<a href="<c:url value='/'/>"><img src="<c:url value='/resource/img/logo.gif'/>"></a>
				</div>
    		</td>
    		<td width="*" style="text-align:center;">
				<div class="_main_title" style="display:inline-block;">
					<h4>글로벌 교통 DB 컨텐츠 관리 시스템</h4>
				</div>
    		</td>    		
    	</tr>
    </table>
</div>		
<script>
// 통합검색 코드 삽입부
</script>