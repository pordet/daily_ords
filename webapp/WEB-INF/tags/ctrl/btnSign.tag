<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="type" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<%@ attribute name="val" required="true" %>							<%-- 리턴 값 받을 변수명 --%>
<c:choose>
	<c:when test="${type eq 'D'}">
		<span class="label_type01">${val}</span>
    </c:when>
	<c:when test="${type eq 'A'}">
		<span class="label_type02">${val}</span>
    </c:when>
	<c:when test="${type eq 'W'}">
		<span class="label_type03">${val}</span>
    </c:when>
	<c:when test="${type eq 'F'}">
		<span class="label_type04">${val}</span>
    </c:when>
	<c:when test="${type eq 'R'}">
		<span class="label_type05">${val}</span>
    </c:when>
	<c:when test="${type eq 'G'}">
		<span class="label_type07">${val}</span>
    </c:when>
	<c:otherwise>
		<span>${val}</span>
	</c:otherwise>
</c:choose>
