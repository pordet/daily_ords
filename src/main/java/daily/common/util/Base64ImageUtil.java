package daily.common.util;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

public class Base64ImageUtil {
	public static String fileToBase64String(final File target) throws IOException {
		Encoder encoder = Base64.getEncoder();
		BufferedInputStream bis = null;
		ByteArrayOutputStream baos = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(target));
			baos = new ByteArrayOutputStream();

			byte[] buffer = new byte[8192];
			int readSize = 0;
			while ((readSize = bis.read(buffer)) > -1) {
				baos.write(buffer, 0, readSize);
			}

			return encoder.encodeToString(baos.toByteArray());
		} finally {
			try {
				bis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				baos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void base64StringToFile(final String encoded, final File output) throws IOException {
		Decoder decoder = Base64.getDecoder();
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(output));
			bos.write(decoder.decode(encoded));
		} finally {
			try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public static String multipartToBase64(MultipartFile file) throws IOException {
		Encoder encoder = Base64.getEncoder();
		String str = encoder.encodeToString(file.getBytes());
		return str;
		
	}
	public static String BufferedImageToBase64(BufferedImage img,String type) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(img, type, os);
		byte[] imageBytes = os.toByteArray();
		Encoder encoder = Base64.getEncoder();
		String str = encoder.encodeToString(imageBytes);
		return str;
	}
}
