package daily.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import daily.common.dao.SystemUserDAO;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Service
public class SystemUserService {

	@Autowired private SystemUserDAO dao;
	public void insertLoginUserInfo(CommandMap loginInfo) {
		dao.insertLoginUserInfo(loginInfo.getMap());
	}

	public void deleteLoginUserInfo(CommandMap loginInfo) {
		dao.deleteLoginUserInfo(loginInfo.getMap());
	}
	
	public int countLoginCntByUserId(CommandMap loginInfo) {
		return dao.countLoginCntByUserId(loginInfo.getMap());
	}

	public CustomMap getLoginUserInfo(CommandMap loginInfo) {
		return dao.getLoginUserInfo(loginInfo.getMap());
	}
}
