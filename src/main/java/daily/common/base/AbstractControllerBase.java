package daily.common.base;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.ui.ModelMap;

import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;

import kr.co.daily.common.GlobalConst;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

public class AbstractControllerBase extends AbstractCommonBase {

	private Logger log = LoggerFactory.getLogger(AbstractControllerBase.class);
	private static final String PATH_EXCEL_TEMP = "/asset/exceltemplate";

	/**
	 * 페이징 목록 결과를 model에 표준 저장 처리.
	 * 		"list" : 조회 페이지 목록 데이터
	 * 		"paginator" : 페이징 정보
	 * @param list
	 * @param modelMap
	 * @return
	 */
	protected void putModelListPage(PageList<?> list, ModelMap model) {
        Paginator paginator = list.getPaginator();
    	model.put("paginator", paginator);
        model.put("list", list);
		//log.debug("list(" + list.size() + ") = " + list.toString());
		log.debug("list size = " + list.size() + ", paginator = " + paginator.toString());
	}

	/**
	 * 페이징 목록 결과를 JSON으로 response 표준 처리.
	 * 		"list" : 조회 페이지 목록 데이터
	 * 		"paginator" : 페이징 정보
	 * @param list
	 * @param modelMap
	 * @return
	 */
	protected String responseJsonList(PageList<?> list, ModelMap model) {
		putModelListPage(list, model);
		return "jsonView";
	}

	
	/**
	 * ajax로 엑셀 파일 다운로드
	 * @사용예 excelDownload(bean, "DownloadFileName", "RemoteFileName.xls", request, response);
	 * @설명 엑셀의 셀에 ${param1} ${param2}로 저장해 놓으면 맵으로 전달된 값이 셀에 보여지고 리스트는 ${list.id} ${list.name} 등으로 반복되어 보여진다.
	 * 
	 * @param bean - map으로 파라미터 전달
	 * @param fileName - 다운로드하는 파일명(확장자 제외. 확장자는 본 함수 내에 xls로 지정.)
	 * @param templateFile - 서버상에 위치한 파일명 (디렉토리 = PATH_EXCEL_TEMP = "/asset/exceltemplate")
	 * @param request
	 * @param response
	 * 
	 */
	protected void excelDownload(Map<String, Object> bean, String fileName, String templateFile, HttpServletRequest request, HttpServletResponse response){

		//String tempPath = request.getSession().getServletContext().getRealPath("/WEB-INF/templete");
		String tempPath = request.getSession().getServletContext().getRealPath(PATH_EXCEL_TEMP);
		
		InputStream is = null;
		ServletOutputStream os = null;
		try {
			is = new BufferedInputStream(new FileInputStream(tempPath + "/" + templateFile));
			XLSTransformer xls = new XLSTransformer();
			Workbook workbook = xls.transformXLS(is, bean);
			//HSSFWorkbook workbook = xls.transformXLS(is, bean);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xls\"");
			os = response.getOutputStream();
			workbook.write(os);
		} catch (FileNotFoundException e) {
			log.error("File Not Found" + e.getMessage());
		} catch (ParsePropertyException e) {
			log.error("Parse Property" + e.getMessage());
			//e.printStackTrace();
		} catch (InvalidFormatException e) {
			log.error("Invalid Format" + e.getMessage());
		} catch (IOException e) {
			log.error("IO Exception" + e.getMessage());
		} finally {
			try {
				is.close();
				os.close();
			} catch (IOException e) {}
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isApplicant(Authentication authentication) {
//		SecurityContext context = SecurityContextHolder.getContext(); 
		// 인증 객체를 얻습니다. 
		boolean isUser = false;
		if(authentication == null) {
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority(GlobalConst.GUEST_AUTH));
			return isUser;
		}else {
			Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
			Iterator<GrantedAuthority> it = authorities.iterator(); 
			while(it.hasNext()) {
				GrantedAuthority auth = it.next();
				if(auth.getAuthority().equals(GlobalConst.GUEST_AUTH)) {
					isUser=true;
				}
			}
			return isUser;
		}

	}
}
