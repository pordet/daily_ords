package kr.co.daily.domain;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import daily.common.vo.CustomMap;

@SuppressWarnings("serial")
public class User implements UserDetails {
	private String username; 
	private String password; // 추가적인 회원정보 항목을 추가한다. email 이나 연락처 등등... 
	private List<GrantedAuthority> authorities;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	private CustomMap userMap;

	public User(String username,String password,CustomMap userMap,List<GrantedAuthority> authList){
		this.username = username;
		this.password = password;
		this.userMap = userMap;
		this.authorities = authList;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public CustomMap getUserMap() {
		return this.userMap;
	}

}
