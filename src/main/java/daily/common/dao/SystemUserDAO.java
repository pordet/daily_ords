package daily.common.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface SystemUserDAO {

	void insertLoginUserInfo(Map<String, Object> map);

	void deleteLoginUserInfo(Map<String, Object> map);

	int countLoginCntByUserId(Map<String, Object> map);

	CustomMap getLoginUserInfo(Map<String, Object> map);

}
