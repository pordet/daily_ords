<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="Role 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/role_register.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
<!--			
			<div class="tab-list tab-layer col-2">
                <ul id="usrTab">
                    <li>
                        <a id="tab_layer_1" href="javascript:void(0);">사용자 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_2" href="javascript:void(0);">사용자 그룹 관리</a>
                    </li>
                    <li class="on">
                        <a id="tab_layer_3" href="javascript:void(0);">Role 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_4" href="javascript:void(0);">권한 관리</a>
                    </li>
                </ul>
            </div>
            <div class="tab-wrap">
				<div id="tab_layer_1" class="tab-cont">
                </div>
				<div id="tab_layer_2" class="tab-cont">
	        	</div>
				<div id="tab_layer_3" class="tab-cont on">
	                       <div class="code-manag-btn">
								<button type="button" class="btn h34 fl" onClick="javascript:openReg();"><span class="btn_add"></span>Role 등록</button>
	                       </div>
								<form name="frm" id="frm">
				                    <div class="grid-area">
				                        <div class="grid-header">
				                            <div class="header-unit" style="width:25%"><a href="#">Role ID<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:25%"><a href="#">Role 이름<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:35%"><a href="#">Role 설명<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:15%"><a href="#">사용 여부<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                        </div>
				                        <div class="grid-contents">
				                            <c:forEach var="map" items="${list}" varStatus="i">
				                            <div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_role}')">
				                                <div class="cont-unit" style="width:25%">${map.id_role}</div>
				                                <div class="cont-unit" style="width:25%">${map.role_nm}</div>
				                                <div class="cont-unit" style="width:35%">${map.role_desc}</div>
				                                <div class="cont-unit" style="width:15%">${map.use_yn}</div>
				                            </div>
				                            </c:forEach>
				                        </div>
				                    </div>
									<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
								</form>
	            </div>
				<div id="tab_layer_4" class="tab-cont">
	            </div>
    		</div>
-->
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<div class="dbBtnbox">
					<div class="l"></div>
					<div class="r">
						<button type="button" class="btn-primary" onClick="javascript:openReg();">
							Role 등록
						</button>
						<button type="button" class="btn-danger" onClick="javascript:deleteRole();">
							Role 삭제
						</button>
					</div>
				</div>
				
				<form name="frm" id="frm" method="post">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit">
								<a href="#">
									Role ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									Role 이름
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									Role 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_role}')">
									<div class="check-unit">
										<input type="checkbox" name="cb_id_role" value="${map.id_role}">
									</div>
									<div class="cont-unit">${map.id_role}</div>
									<div class="cont-unit">${map.role_nm}</div>
									<div class="cont-unit desc-unit">${map.role_desc}</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="id_role" id="id_role" value="${param.id_role}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function openReg(id_role){
	var frm =document.frm;
	frm.action=contextPath+"/adm/auth/regRole.do";
	if(id_role){
		$("#id_role").val(id_role);
	}else{
		$("#id_role").val('');
	}
	$("#frm").submit();
}
function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/auth/roleList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
</script>
</html>
