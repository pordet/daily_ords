<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.SpringPropertiesUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<!DOCTYPE html>
<html>
<front:FrontDynamicHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/member/member_register.js'/>"></script>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer w70">
			<div class="page-title-wrap">
				<span>회원 가입</span>
				<input type="hidden" id="none"/>
			</div>
			
			<form id="frm" name="frm" method="post" action="<c:url value='/joinAction.do'/>" onsubmit="joinAction();return false;">
				<div class="form-content-wrap">
					<table>
						<colgroup>
							<col>
							<col>
						</colgroup>
						<tbody>
							<tr class="titie-row">
								<td>
									<div class="inner-title">
										<span>기본정보</span>
									</div>
								</td>
								<td>
									<div class="title-sub-desc">
										<span>
											<strong class="imp-red">*</strong>
											표시는 필수 입력항목입니다.
										</span>
									</div>
								</td>
							</tr>
							<tr class="main-row top-border">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										아이디
									</span>
								</td>
								<td>
									<input id="id_user" name="id_user" title="아이디" style="ime-mode:active" type="text" value="" maxlength="20" 
										onkeypress="if(event.keyCode == 13) fn_duplChkMberId();" onkeyup="fn_inputKey('7', this);">
									<button type="button" class="btn-primary" onclick="fn_duplChkMberId()">중복확인</button>
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util">
									<div class="input-reference">4~20자 이내 영문 소문자, 숫자만 가능합니다.</div>
									<div class="input-warning id-input-warning" style="display: none;">아이디 입력해주세요</div>
									<div class="input-warning duplChkIdResult-warning" style="display: none;">아이디 중복확인을 해주세요.</div>
									<div class="input-success duplChkIdResult" style="display: none;">사용 가능한 아이디입니다.</div>
								</td>
							</tr>
							
							<tr class="main-row">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										비밀번호
									</span>
								</td>
								<td>
									<input id="pwd" name="user_pwd" title="비밀번호" onkeyup="fn_pswrdInputChk();" type="password" value="" maxlength="20">
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util">
									<div class="input-reference">숫자, 영문자, 특수문자(!, @, #, $, %, ^, &amp;, +, =, ~, *) 조합 8자이상 ~ 20자 이하 </div>
									<div class="input-warning" style="display: none;">비밀번호를 입력해주세요</div>
								</td>
							</tr>
	
							<tr class="main-row">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										비밀번호 확인
									</span>
								</td>
								<td>
									<input type="password" title="비밀번호 확인 입력" id="pwd_chk" onkeyup="fn_pswrdInputChk();" maxlength="20">
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util">
									<div class="input-warning pswrdChkAt" style="display: none;">비밀번호가 일치하지 않습니다</div>
								</td>
							</tr>
							
							<tr>
								<td colspan="2" class="empty-row">
								</td>
							</tr>
							
							<tr class="titie-row">
								<td>
									<div class="inner-title">
										<span>개인정보</span>
									</div>
								</td>
								<td>
									<div class="title-sub-desc">
										<span>
											<strong class="imp-red">*</strong>
											표시는 필수 입력항목입니다.
										</span>
									</div>
								</td>
							</tr>
							<tr class="main-row top-border">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										이름
									</span>
								</td>
								<td>
									<input id="user_nm" name="user_nm" title="이름 입력" type="text" value="" maxlength="20">
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util">
									<div class="input-warning" style="display: none;">이름을 입력해주세요</div>
								</td>
							</tr>
							
							<tr class="main-row">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										이메일
									</span>
								</td>
								<td>
									<input id="email" name="email" title="이메일 입력" style="ime-mode:disabled" placeholder="sample@sample.com" type="text" value="" maxlength="40">
									<input type="checkbox" title="이메일 수신 동의여부" id="emailRecptnAt" name="email_receive_yn" value="Y">
									<label for="emailRecptnAt">이메일 수신 동의</label>
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util" id="email_input_util">
									<div class="input-reference">비밀번호 초기화 메일 수신 등에 반드시 필요한 정보이므로 정확히 이메일 주소를 입력해주세요.</div>
									<div class="input-warning" style="display: none;">이메일 인증을 진행해주세요</div>
								</td>
							</tr>
							
							<tr class="main-row">
								<td>
									<span>
										<strong class="imp-red">*</strong>
										휴대전화번호
									</span>
								</td>
								<td>
									<input id="mbl_phone_num" name="cell_phone_num" onkeyup="fn_inputKey('1', this);"
										style="ime-mode:disabled" type="text" value="" maxlength="14" title="휴대전화번호 입력">
								</td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td class="input-util" id="phone_input_util">
									<div class="input-warning" style="display: none;">휴대전화번호를 입력해주세요.</div>
								</td>
							</tr>
							
							<tr class="main-row bottom-border">
								<td>
									<span>부서</span>
								</td>
								<td>
									<input id="dept_nm" name="dept_nm" title="부서입력" type="text" value="" maxlength="20">
								</td>
							</tr>
							<tr class="main-row bottom-border">
								<td>
									<span>직책</span>
								</td>
								<td>
									<input id="posi_nm" name="posi_nm" title="직책 입력" type="text" value="" maxlength="20">
								</td>
							</tr>
							
						</tbody>
					</table>
				</div>
				
				<div class="form-button-wrap">
					<button type="button" class="btn-primary" onclick="fn_save();">승인 신청</button>
					<button type="button" class="btn-default" onclick="fn_cancel();">취소</button>
				</div>
			</form>
		</div>
	</div>
</front:FrontDynamicBody>
<script>
//취소 시 메인화면으로 이동 : fn_cancel에서 confirm을 불러올때 사용.
function returnToMain() {
	window.location.href = "/traf/";
}
</script>
</html>
