<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<his:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_register.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_input.js'/>"></script>
</his:DataHead>
<his:DataTreeBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" hasLeft="true" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;" onsubmit="return false;">
<%--                  <input type="hidden" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"> --%>
	                 <input type="hidden" name="id_master" value="${master.id_master}">
	                 <input type="hidden" name="id_board_master" id="id_board_master" value="${master.id_board_master}">
	                 <input type="hidden" name="id_board" id="id_board">
	                 <input type="hidden" name="attach_yn" value="${master.attach_yn}">
	                 <input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
	                 <input type="hidden" name="ord_str" value="${master.ord_str}">
	                 <input type="hidden" name="sel_ord_str" id="sel_ord_str">
                   	 <div class="row">
                        <div class="col-12">
		                            <div class="form-group inline-group search">
		                                  <input type="text" class="form-control mh-form-control" style="width:100%" name="search_keyword" id="search_keyword" value="${(not empty param.search_keyword ?param.search_keyword:'')}" onkeyup="enterKey();"><input type="image" src="<c:url value='/resource/img/search_bg.gif'/>" alt="검색">
		                            </div>
	                    </div>
	                </div>
                   	 <div class="row">
                        <div class="col-12">
		                    <div class="form-group form-inline">
							<label class="control-label">정렬</label>
							<select class="form-control mh-form-control" style="width:100px;font-size:13px;height:30px;" title="정렬" name="order_key" id="order_key">
								<option value="reg_date">등록일</option>
								<option value="title">제목</option>
							</select>
<!-- 							</div> -->
<!-- 							<div class="form-group form-inline"> -->
							&nbsp;&nbsp;<label class="control-label">순서</label>
							<select class="form-control mh-form-control" style="width:100px;font-size:13px;height:30px;" title="순서" name="idx" id="idx">
								<option value="ASC">오름차순</option>
								<option value="DESC">내림차순</option>
							</select>
							</div>
	                    </div>
	                </div>
                   	 <div class="row">
                        <div class="col-4">
<!--                             <div class="form-group inline-group"> -->
	                              <button type="button" class="list-btn h34" onclick="javascript:search();"><img src="<c:url value='/resource/img/search_bg.png'/>" style="width:24px;">검색</button>
<!-- 	                              <button type="button" class="list-btn h34" id="search_init" onclick="search_keyword_init();">초기화</button>  -->
<!-- 	                              style="display:none;" -->
	                              
<!--                             </div> -->
	                    </div>
	                    <div class="col-8">
	                       		<div class="pos-fr">
	                              <button type="button" class="list-btn h34" onclick="javascript:registerData();">
<!-- 	                              <span class="write-icon-png"></span> -->
<img src="<c:url value='/resource/img/write_btn.png'/>" style="width:24px;">
	                              등록</button>
	                            </div>
	                    </div>
	                </div>
		                    <div class="row" id="result_header" style="margin-top:5px;" style="display:${(fn:length(list) gt 0?'block':'none')}">
		                        <div class="col-4">
		                            <div class="form-group inline-group">
		                            	<c:forEach var="item" items="${list}" varStatus="i">
		                            		<c:if test="${i.count eq 1}">
		                            			<c:set var="result_first" value="${item.rownum}"/>
		                             		</c:if>
		                            		<c:if test="${i.count eq fn:length(list)}">
		                            			<c:set var="result_last" value="${item.rownum}"/>
		                             		</c:if>
		                            	</c:forEach>
			                              <span style="font-size: 14px;" id="result_summary">검색결과  ${paginator.totalCount} 개 중 ${result_first} ~ ${result_last}</span>
		                            </div>
			                    </div>
			                </div>
	                       	<div class="row" id="result_list" style="display:${(fn:length(list) gt 0?'block':'none')}">
								<div class="col-12">
				                        <div class="listboard-contents1">
				                            <!--loof-->
				                            <ul class="list-line">
					                            <c:forEach var="map" items="${list}">
					                            <li>
					                            <div class="meta">${map.board_nm}, ${map.reg_date}</div>
												<div class="search-result-title"><a href="javascript:void(0);" onclick="javascript:viewData(${map.id_board_master},${map.id_board});">${map.title}</a>
													<div class="file-format inline-group"> 
													<c:forEach var="file" items="${map.filelist}">
														<a href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}"><span class="${file.file_sh_ext}">${file.file_ext}</span></a>
													</c:forEach>
													</div>
												</div>
												<div class="search-result-text txt_post">${map.comment}</div>
												</li>
					                            </c:forEach>
				                            </ul>
				                            <!--//loof-->
				                         </div>
				                    <!-- //그리드 컨텐츠영역 -->
					                    <!-- paging -->
										<pageList:ajax_page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
										<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
										<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
										<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
										<input type="hidden" name="page" id="page" value="${param.page}"/>
					                    <!-- //paging -->
				                    </div>
				                    <!--//그리드영역-->
				              </div>
	                	<div class="row" id="result_no" style="display:${(fn:length(list) eq 0?'block':'none')}">
	                		<div class="col-12">
	                		검색결과가 없습니다.
	                		</div>
	                	</div>
			</form>
  <div class="modal fade mh-modal type05" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModal" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="board_reg_type">등록</h3>
				<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<form name="modi_frm" id="modi_frm">
			<input type="hidden" id="modi_id_master" name="id_master" value="${(not empty param.id_master?param.id_master:1)}"/>
			<input type="hidden" id="modi_id_board" name="id_board" value=""/>
			<input type="hidden" id="modi_id_board_master" name="id_board_master"/>
			<input type="hidden" id="modi_id_board_file" name="id_board_file"/>
			<input type="hidden" id="modi_sel_ord_str" name="sel_ord_str"/>
			<input type="hidden" id="modi_page" name="page"/>
			<input type="hidden" id="modi_search_keyword" name="search_keyword"/>
			<div class="modal-body">
                      <div class="row">
                          <div class="col-12">
                          <div class="tit_cont">제목</div>
                            <div class="form-group">
                              <input type="text" class="form-control mh-form-control" name="title" id="modi_title" value="${map.title}">
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                          <div class="tit_cont">내용</div>
                            <div class="form-group">
								<textarea class="form-control mh-form-control" name="comment" id="modi_comment" rows="6" style="height:240px;">${map.comment}</textarea>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                          <div class="tit_cont">키워드</div>
                            <div class="form-group">
                              <input type="text" class="form-control mh-form-control" name="keyword" id="modi_keyword" value="${map.title}">
                            </div>
                          </div>
<!-- 						 <h3 class="h3_tit col-12" id="key_btn_group"> -->
<!-- 							키워드 목록 -->
<%-- 							<a href="javascript:void(0);" class="btn_add" onClick="javascript:addKeyword();"><img src="<c:url value='/resource/img/ico_add.png'/>" alt=""/>추가</a> --%>
<!-- 						 </h3> -->
                      </div>
                <div class="row" id="file_list_div">
					 <h3 class="h3_tit col-12" id="file_btn_group">
						첨부파일 리스트
						<a href="javascript:void(0);" class="btn_add" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_add.png'/>" alt=""/>추가</a>
					 </h3>
                    <div class="col-8 line" id="addFile_in_div_1">
                        <div class="form-group inline-group">
                            <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>
                            <div class="attach-btn">
                              <button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>
                              <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
                            </div>
                        </div>
                    </div>
					<div class="col-4 line" id="delFile_in_div_1" style="text-align:right;">
	                             <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile(1);"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
	                 </div>
                </div>
            <div class="btn-area mb-0p">
                <button type="button" class="btn back" id="btnDelete" onclick="delData();" style="display:${(not empty map.id_board?'block':'none')};">삭제</button>
                <button type="button" class="btn" id="btnSave" onclick="register();">저장</button>
                <button type="button" class="btn list" id="btnList" onclick="close_reg();">닫기</button>
            </div>	
			</div>
			</form>
		</div>
	</div>
  </div>
</his:DataTreeBody>
<script>
$('#dataModal').on('hidden.bs.modal', function () {
	close_reg();
});
function enterKey(){
	if(window.event.keyCode == 13){
		$("#sel_ord_str").val('');
		doList(1,false);
	}
}
function close_reg(){
	$("#dataModal").modal('hide');	
}

$(function() {
    <c:forEach var="map" items="${countList}">
    $("#span_${map.ord_str}").html("${map.board_cnt}");
    </c:forEach>
	$(".grid-loof-cont").mouseover(function(e){
 		$(this).find(".cont-unit").css("background-color","rgb(211,234,255)");
 		e.preventDefault();
 	});	
 	$(".grid-loof-cont").mouseout(function(e){
 		$(this).find(".cont-unit").css("background-color","white");
 		e.preventDefault();
 	});	
});

</script>
</html>