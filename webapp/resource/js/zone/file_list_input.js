/*
function openInput(){
	setInputTitle('I');	
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_year").val($("#search_year").val());
	$("#previewModal").modal('show');	
}
function setInputTitle(type){
	var title = "";
	
	if(nanToStr($("#id_category").val())!=''){
		title +=$("#id_category option:selected").text()+' ';
	}
	if(nanToStr($("#id_nat_area").val())!=''){
		title +=$("#id_nat_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_city_area").val())!=''){
		title +=$("#id_city_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_meta").val())!=''){
		title +=$("#id_meta option:selected").text()+' ';
	}
	if(type=='I'){
		title+='데이터 등록/수정';
	}else{
		title+='데이터 수정';
	}
	$("#data_input_pop_title").html(title);
    
}
function openAjaxInput(){
	setInputTitle('I');	
	var id_meta = nanToStr($("#id_meta").val());
	var id_category = $("#id_category").val();
	
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
		if($("#id_meta").val()!=''){
			$("#reg_id_meta").val($("#id_meta").val());
		}
		
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else if($("#id_meta").val()==''){
		alertOpen('세부항목를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_id_zone_area").val($("#id_zone").val());
	$("#reg_year").val($("#search_year").val());
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getNetworkAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
    			var html="";
	    		if(e.rowList.length!=0){
	    			var rsize = e.rowList.length;
	    			$.each(e.rowList,function(i,d){
		    			if(i==0){
		    				html+='<tr id="addFile_in_div_'+(i+1)+'">';
		    				html+='	   <td width="40%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" id="fileName'+d.ord+'" data-add="1" class="form-control mh-form-control" disabled value="'+d.file_nm+'">';
		    				html+='				  <div class="attach-btn">';
		    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
		    				html+='				  	<input type="file" name="file_'+d.ord+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+d.ord+'\').value = this.value"/>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="26%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" name="file_desc_'+d.ord+'" data-add="'+d.ord+'" class="form-control mh-form-control" disabled value="'+d.file_desc+'">';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="10%">';
	    				    html+='		  	   <div class="col-12">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">다운로드</div>';
		    				html+='       	      <div class="form-group" style="margin-left:8px;">';
		    				html+='       	      <a href="'+d.file+'" download="'+d.file_nm+'">';
		    				html+='				  	<img src="'+contextPath+'/resource/img/ico_download.png" alt="추가" style="width:38px;height:20px;"/></a>';
		    				html+='               </a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
	    				    html+='		  	   <div class="col-12" style="margin-left:13px;">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
		    				html+='       	      <div class="tit_cont">삭제</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete" class="ico_delete" /></a>';
		    				html+='               </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='</tr>';	    				
		    			}else{
		    				html+='<tr id="addFile_in_div_'+(i+1)+'">';
		    				html+='	   <td width="40%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" id="fileName_'+d.ord+'" data-add="1" class="form-control mh-form-control" disabled value="'+d.file_nm+'">';
		    				html+='				  <div class="attach-btn">';
		    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
		    				html+='				  	<input type="file" name="file_'+d.ord+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+d.ord+'\').value = this.value"/>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="26%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" name="file_desc_'+d.ord+'" data-add="'+d.ord+'" class="form-control mh-form-control" disabled value="'+d.file_desc+'">';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="10%">';
	    				    html+='		  	   <div class="col-12">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">다운로드</div>';
		    				html+='       	      <div class="form-group" style="margin-left:8px;">';
		    				html+='       	      <a href="'+d.file+'" download="'+d.file_nm+'">';
		    				html+='				  	<img src="'+contextPath+'/resource/img/ico_download.png" alt="추가" style="width:38px;height:20px;"/></a>';
		    				html+='               </a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
	    				    html+='		  	   <div class="col-12">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
		    				html+='       	      <div class="tit_cont">삭제</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete" class="ico_delete" /></a>';
		    				html+='               </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='</tr>';
				    	}
	    			});
	    			

	    			$("#file_list_tab").empty();
	    			$("#file_list_tab").html(html);
	    			$("#previewModal").modal('show');	
	    		}else{
	    			
    				html+='<tr id="addFile_in_div_1">';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
					html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="40%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" id="fileName_1" data-add="1" class="form-control mh-form-control" disabled>';
    				html+='				  <div class="attach-btn">';
    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
    				html+='				  	<input type="file" name="file_nm_1" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName_1\').value = this.value"/>';
    				html+='               </div>';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="26%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" name="file_desc_1" data-add="1" class="form-control mh-form-control">';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="10%">';
				    html+='		  	   <div class="col-12">';
    				html+='            </div>';
    				html+='	   </td>';
    				html+='	   <td width="8%">';
				    html+='		  <div class="col-12">';
				    html+='       	  <div class="tit_cont" style="margin-top:2px;">추가</div>';
    				html+='       	  <div class="form-group">';
    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile(1);"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
    				html+='           </div>';
    				html+='       </div>';
    				html+='	   </td>';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='</tr>';	    
    				
	    			$("#file_list_tab").empty();
	    			$("#file_list_tab").html(html);
	    			$("#previewModal").modal('show');	
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
let arr = [1];
function addFile(idx){
	var bef_file_num = idx;
    var cur_file_num = idx + 1;
    arr.push(cur_file_num);
	var html ='';
	html+='<tr id="addFile_in_div_'+cur_file_num+'">';
	html+='	   <td width="40%">';
	html+='		   <div class="col-12">';
    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
	html+='       	   <div class="form-group inline-group">';
	html+='				  <input type="text" id="fileName_'+cur_file_num+'" data-add="1" class="form-control mh-form-control" disabled>';
	html+='				  <div class="attach-btn">';
	html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
	html+='				  	<input type="file" name="file_nm_'+cur_file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName_'+cur_file_num+'\').value = this.value"/>';
	html+='               </div>';
	html+='            </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="26%">';
	html+='		   <div class="col-12">';
    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
	html+='       	   <div class="form-group inline-group">';
	html+='				  <input type="text" name="file_desc_'+cur_file_num+'" data-add="'+cur_file_num+'" class="form-control mh-form-control">';
	html+='            </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="10%">';
    html+='		  	   <div class="col-12">';
	html+='            </div>';
	html+='	   </td>';
	html+='	   <td width="8%">';
	html+='		   <div class="col-12">';
	html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
	html+='       	      <div class="form-group">';
	html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+cur_file_num+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
	html+='               </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="8%">';
	html+='		   <div class="col-12">';
	html+='       	      <div class="tit_cont">삭제</div>';
	html+='       	      <div class="form-group">';
	html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+cur_file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="삭제" class="ico_delete"/></a>';
	html+='               </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='</tr>';	    				
	var fileCnt = $("[id^=addFile_in_div_]").length;
	$("#addFile_in_div_"+bef_file_num).after(html);
}
function addFile(idx){
	console.log("arr add")
	console.log(arr)
	
	var bef_file_num = idx;
    var cur_file_num = idx + 1;
    arr.push(cur_file_num);
	var html = fileTableRowCreator(cur_file_num, null);	
	//var html = fileTableRowCreator(bef_file_num, null);	//내부에서 +1
	//var fileCnt = $("[id^=addFile_in_div_]").length;
	$("#addFile_in_div_"+bef_file_num).after(html);
	
}
function delFile(idx){
	var ix = arr.indexOf(idx);
	if (ix > -1) arr.splice(ix,1);
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==1){
		alertOpen('적어도 하나의 행은 있어야 합니다.')
	}
    //$("#addFile_in_div_"+idx).remove();
    $("#addFile_in_div_"+ix).remove();
}
*/


function setInputTitle(){
	var docTitle = "";
	var bodyTitle = "";
	
	if(nanToStr(iniParam.category) != ''){
		docTitle += iniParam.cateName + ' ';
	}
	docTitle += '데이터 등록/수정';
	
	if(nanToStr(iniParam.cityArea) != ''){
		bodyTitle += iniParam.cityName + ' ';
	} else {
		if(nanToStr(iniParam.natArea) != ''){
			bodyTitle += iniParam.natName + ' ';
		}
	}
//	if(nanToStr(iniParam.meta) != ''){
//		bodyTitle += iniParam.metaName + ' ';
//	}
	bodyTitle += '데이터 목록';
	
	$("#data_input_pop_title").html(docTitle);
	$("#modalBodyTitle").html(bodyTitle);
}
//객체 복사
function copyObjAttr(source, target) {
	var keys = Object.keys(source);
	for(var i=0; i<keys.length; i++) {
		target[""+keys[i]+""] = source[""+keys[i]+""];
	}
}
//배열 내 객체비교 함수 . 'ARRAY'.sort(compare('key'))
function compare(key) {
	return function(a,b) {
	    return a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0;
	}
}
var newList = [];
function displayList(list) {
	var html="";
	for(var i=0; i<list.length; i++) {
		var item = list[i];
		var newRow = fileTableRowCreator(i, item);
		html += newRow;
	}
	$("#file_list_tab").empty();
	$("#file_list_tab").html(html);
	
	//파일데이터 삽입...
	if(list.length > 0) {
		var rows = document.querySelectorAll("#file_list_tab tr");
		for(var i=0; i<list.length; i++) {
			var item = list[i];
			if(item.newFileData != "") {
				rows[i].querySelector("input[type=file]").files = item.newFileData;
			}			
		}
	}
}
function openAjaxInput(){
	setInputTitle();
	
	if(iniParam.cityArea != ''){
		$("#reg_id_city_area").val(iniParam.cityArea);
//		if(iniParam.meta != ''){
//			$("#reg_id_meta").val(iniParam.meta);
//		}
	}
	$("#reg_id_nat_area").val(iniParam.natArea);
	$("#reg_id_category").val(iniParam.category);
	$("#reg_id_zone_area").val(iniParam.zone);
	$("#reg_year").val($("#search_year").val());
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getNetworkAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
				newList = [];
				
				var list = e.rowList;
				if(list.length > 0) {
					list.sort(compare("ord"));
					for(var i=0; i<list.length; i++) {
						var item = list[i];
						var newItem = new Object;
						copyObjAttr(item, newItem);
						newItem.newFileName = "";
						newItem.newFileData = "";
						newItem.newFileDesc = "";
						newItem.fileChange = "initial";
						newItem.descChange = "initial";
						newItem.mode = "initial";
						newList.push(newItem);
					}
				} else {
					var newItem = {
						newFileName : '',
						newFileData : '',
						newFileDesc : '',
						fileChange : "initial",
						descChange : "initial",
						ord : 1, //임시...
						mode : 'create'
					}
					newList.push(newItem);
				}
				displayList(newList);
    			$("#previewModal").modal('show');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

//추가. fileInput click : 버튼의 다음에 있는 요소를 클릭한다. 버튼다음에 input을 배치할 것.
function fileAttachBtn() {
	var btnElement = event.target;
	if(btnElement.tagName != 'BUTTON') {
		btnElement = btnElement.parentNode;
		if(btnElement.tagName != 'BUTTON') return;
	}
	var fileElement = btnElement.nextElementSibling;
	fileElement.click();
}

function fileTableRowCreator(i, item) {
	var mode = item.mode;
	var fileChange = item.fileChange;
	var descChange = item.descChange;
	var html = '';
	if(mode == "initial") {
		html+=' <tr class="file-list-row-wrap">';
		html+='		<td>';
		html+='			<div class="file-upload-group">';
		html+='				<input class="fileName" type="text" disabled value="'+item.file_nm+'" >';
		html+='				<button class="btn-primary" type="button" disabled title="첨부">';
		html+='					<span class="glyphicon glyphicon-paperclip"></span>';
		html+='					첨부';
		html+='				</button>';
		html+='			</div>';
		html+='		</td>';
		html+='		<td>';
		html+='			<input class="fileDesc" type="text" disabled value="'+item.file_desc+'">';
		html+='		</td>';
		html+='		<td>';
		html+='			<div class="multi-btn-area">'
		html+='				<div class="multi-btn-wrap">'
		html+='					<div class="btn-wrap">'
		html+='						<a href="'+item.file+'" download="'+item.file_nm+'">';
		html+='							<span class="glyphicon glyphicon-download-alt"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:modifyRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-pencil" title="수정"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:delRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='				</div>';
		html+='			</div>';
		html+='		</td>';
		html+='	</tr>';
	} else if(mode == "delete") {
		html+=' <tr class="file-list-row-wrap deleteRow">';
		html+='		<td>';
		html+='			<div class="file-upload-group">';
		html+='				<input class="fileName" type="text" disabled value="'+item.file_nm+'" >';
		html+='				<button class="btn-primary" type="button" disabled title="첨부">';
		html+='					<span class="glyphicon glyphicon-paperclip"></span>';
		html+='					첨부';
		html+='				</button>';
		html+='			</div>';
		html+='		</td>';
		html+='		<td>';
		html+='			<input class="fileDesc" type="text" disabled value="'+item.file_desc+'">';
		html+='		</td>';
		html+='		<td>';
		html+='			<div class="multi-btn-area">'
		html+='				<div class="multi-btn-wrap">'
		html+='					<div class="btn-wrap">'
		html+='						<a href="'+item.file+'" download="'+item.file_nm+'">';
		html+='							<span class="glyphicon glyphicon-download-alt"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:delRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-repeat" title="삭제취소"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='				</div>';
		html+='			</div>';
		html+='		</td>';
		html+='	</tr>';
	} else if(mode == "create") {
		html+=' <tr class="file-list-row-wrap">';
		html+='		<td>';
		html+='			<div class="file-upload-group">';
		html+='				<input class="fileName" type="text" disabled value="'+item.newFileName+'" placeholder="파일을 등록하세요.">';
		html+='				<button type="button" class="btn-primary" onclick="javascript:fileAttachBtn();" title="첨부">';
		html+='					<span class="glyphicon glyphicon-paperclip"></span>';
		html+='					첨부';
		html+='				</button>';
		html+='				<input type="file" name="file_'+ i +'" class="file-input-hidden" onchange="javascript:fileChange('+i+')"/>';
		html+='			</div>';
		html+='		</td>';
		html+='		<td>';
		html+='			<input class="fileDesc" type="text" value="'+item.newFileDesc+'" onchange="javascript:descChange('+i+')" placeholder="설명을 입력하세요.">';
		html+='		</td>';
		html+='		<td>';
		html+='			<div class="multi-btn-area">'
		html+='				<div class="multi-btn-wrap">'
		html+='					<div class="btn-wrap">'
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:delRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='				</div>';
		html+='			</div>';
		html+='		</td>';
		html+='	</tr>';
	} else if(mode == "modify") {
		html+=' <tr class="file-list-row-wrap">';
		html+='		<td>';
		html+='			<div class="file-upload-group">';
		if(fileChange == "initial") {
			html+='			<input class="fileName" type="text" disabled value="'+item.file_nm+'" placeholder="파일을 등록하세요.">';
		} else if(fileChange == "changed") {
			html+='			<input class="fileName"type="text" disabled value="'+item.newFileName+'" placeholder="파일을 등록하세요.">';
		}
		html+='				<button type="button" class="btn-primary" onclick="javascript:fileAttachBtn();" title="첨부">';
		html+='					<span class="glyphicon glyphicon-paperclip"></span>';
		html+='					첨부';
		html+='				</button>';
		html+='				<input type="file" name="file_'+ i +'" class="file-input-hidden" onchange="javascript:fileChange('+i+')"/>';
		html+='			</div>';
		html+='		</td>';
		html+='		<td>';
		if(descChange == "initial") {
			html+='		<input class="fileDesc" type="text" value="'+item.file_desc+'" onchange="javascript:descChange('+i+')" placeholder="설명을 입력하세요.">';
		} else if(descChange == "changed") {
			html+='		<input class="fileDesc" type="text" value="'+item.newFileDesc+'" onchange="javascript:descChange('+i+')" placeholder="설명을 입력하세요.">';
		}
		html+='		</td>';
		html+='		<td>';
		html+='			<div class="multi-btn-area">'
		html+='				<div class="multi-btn-wrap">'
		html+='					<div class="btn-wrap">'
		if(fileChange == "initial") {
			html+='					<a href="'+item.file+'" download="'+item.file_nm+'">';
			html+='						<span class="glyphicon glyphicon-download-alt"></span>';
			html+='					</a>';
		}
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:modifyRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-repeat" title="수정취소"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='					<div class="btn-wrap">'
		html+='						<a href="javascript:void(0);" onClick="javascript:delRow('+ i +');">';
		html+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
		html+='						</a>';
		html+='					</div>';
		html+='				</div>';
		html+='			</div>';
		html+='		</td>';
		html+='	</tr>';
	}
	return html;
}
function fileChange(index) {	
	var target = event.target;
	var nowMode = newList[index].mode;
	var item = newList[index];
	if(nowMode == 'create') {
		item.newFileName = target.value;
		item.newFileData = target.files;
	} else if(nowMode == "modify") {
		if(target.files.length > 0) {
			item.newFileName = target.value;
			item.newFileData = target.files;
			item.fileChange = "changed";
		} else {
			item.newFileName = "";
			item.newFileData = "";
			item.fileChange = "initial";
		}
	}
	displayList(newList);
}
function descChange(index) {
	var target = event.target;
	var nowMode = newList[index].mode;
	newList[index].newFileDesc = target.value;
	if(target.value.replace(/ /gi, "") != "" && nowMode != 'create') {
		newList[index].mode = "modify";
		newList[index].descChange = "changed";
	} else {
		newList[index].descChange = "initial";
	}
	displayList(newList);
}
function addRow() {
	var newRow = {
		newFileName : '',
		newFileData : '',
		newFileDesc : '',
		fileChange : "initial",
		descChange : "initial",
		ord : 1, //임시...
		mode : 'create'
	}
	newList.push(newRow);
	displayList(newList);
}
function modifyRow(index) {
	var nowMode = newList[index].mode;
	if(nowMode == "modify") {
		newList[index].mode = 'initial';
	} else {
		newList[index].mode = 'modify';
	}
	displayList(newList);
}
function delRow(index) {
	var nowMode = newList[index].mode;
	if(nowMode == 'create') { //새로 추가된 행 : DB에 등록된 내용 없음... --> 데이터 삭제..
		newList.splice(index, 1);
	} else if(nowMode == 'delete') { //현재모드가 삭제 모드인 경우 --> initial로 변경...
		newList[index].mode = 'initial';
	} else { //삭제 모드로 변경..
		newList[index].mode = 'delete';
	}
	displayList(newList);
}