package kr.co.daily.front.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.AddBatchUtil;
import daily.common.util.CollectionUtil;
import daily.common.util.IdentityUtil;
import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.IdentityManageDAO;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.dao.IdentityMetaDAO;
import kr.co.daily.front.dao.MetaDAO;

@Service
public class IdentityMetaService {
	
	@Value("#{global['openapi.addbatch.cnt']}")
	private int addBatchCnt;
	@Autowired private IdentityMetaDAO dao;
	@Autowired private IdentityManageDAO idenDao;
	
	public PageList<CustomMap> selectIdentityMetaList(CommandMap map) {
		return dao.selectIdentityMetaList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}
	public int getMaxDepthAtIdentity(CommandMap map) {
		return dao.getMaxDepthAtIdentity(map.getMap());
	}
	/**
	 * 헤더 리스트를 가져오는 메서드
	 * @param map
	 * @param max_meta_depth
	 * @return subColList , aloneList ,headerList 값을 가지는 map
	 * @throws Exception
	 */
	public Map<String,Object> selectIdentityListForUsing(CommandMap map, int max_meta_depth) throws Exception {
	    List<CustomMap> topList = dao.selectTopDepthIdentityListForHeader(map.getMap());
	    List<CustomMap> slaveList = dao.selectDepthIdentityListRemoveTopDepthForHeader(map.getMap());
	    Map<String,Object> ret = null;
		List<CustomMap> subList = dao.selectIdentityMetaRelByMetaIdMetaM(map.getMap());
		List<Integer> subKeyList = new ArrayList<>();
		Map<Integer,CustomMap> subMap = new HashMap<>();
		for(CustomMap sub:subList) {
			int key = sub.getInt("child_id_meta_m",0);
			subKeyList.add(key);
			subMap.put(key, sub);
		}
		if(subKeyList.size()>0) {
			List<CustomMap> fkColList = dao.selectIdentityMetaFKBySlaveMeta(subKeyList);
			Map<Integer,List<CustomMap>> fkColMap = new HashMap<>();
			for(CustomMap sub:fkColList) {
				List<CustomMap> depthSubColList = fkColMap.computeIfAbsent(sub.getInt("id_meta_m"), k -> new ArrayList<>());
				depthSubColList.add(sub);
			}
		    ret = IdentityUtil.generateDataAndHeaderLists(topList,slaveList,fkColMap);
			List<CustomMap> subColList = dao.selectSubIdentityMetaColByKeyList(subKeyList);
			//(TODO)검증해야 하는 부분
			Map<Integer,List<CustomMap>> depthSubColMap = new HashMap<>();
			List<CustomMap> list = (List<CustomMap>) ret.get("aloneList");
			for(CustomMap alone:list) {
				for(CustomMap sub:subList) {
					if(alone.getStringRaw("meta_cd","").equals(sub.getStringRaw("meta_cd"))) {
						List<CustomMap> depthSubColList = depthSubColMap.computeIfAbsent(alone.getInt("id_meta_m"), k -> new ArrayList<>());
						depthSubColList.add(sub);
						depthSubColMap.put(sub.getInt("id_meta_m"), subColList);
					}
				}
			}
			ret.put("subColList", depthSubColMap);
		}else {
		    ret = IdentityUtil.generateDataAndHeaderLists(topList,slaveList);
		}
	    return ret;
	}
	public List<CustomMap> selectIdentityMetaRelList(CommandMap map) {
		return dao.selectIdentityMetaRelList(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaDetailParamList(CommandMap map) {
		return dao.selectIdentityMetaDetailParamList(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaRelByParentIdMetaM(CommandMap map) {
		return dao.selectIdentityMetaRelByMetaIdMetaM(map.getMap());
	}
	public PageList<CustomMap> selectIdentityMetaDataContents(CommandMap map, CommandMap spMap) {
		return dao.selectIdentityMetaDataContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}
	public PageList<CustomMap> selectTreeIdentityMetaDataContents(CommandMap map, CommandMap spMap) {
		return dao.selectTreeIdentityMetaDataContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}
	public List<CustomMap> selectIdentityMetaDataForSearch(List<Integer> lowKey) {
		return dao.selectIdentityMetaDataForSearch(lowKey);
	}
	public CustomMap getIdentityMetaRowByKey(CommandMap map) {
		return dao.getIdentityMetaRowByKey(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaDataByIdMetaRow(CommandMap map) {
		return dao.selectIdentityMetaDataByIdMetaRow(map.getMap());
	}
	public List<CustomMap> selectIdentityMeta(CommandMap map) {
		return dao.selectIdentityMeta(map.getMap());
	}
	public List<CustomMap> selectIdentity(CommandMap map) {
		return dao.selectIdentity(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaTabSearch(List<Map<String, Object>> params) {
		return dao.selectIdentityMetaTabSearch(params);
	}
	public List<CustomMap> selectIdentityMetaTabSearchList(CommandMap map) {
		return dao.selectIdentityMetaTabSearchList(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaRowForTabCode(CommandMap map) {
		return dao.selectIdentityMetaRowForTabCode(map.getMap());
	}
	public List<CustomMap> selectIdentityMetaCodeList(CommandMap map) {
		return dao.selectIdentityMetaCodeList(map.getMap());
	}
	public void modifyIdentityMetaRow(CommandMap map) {
		dao.modifyIdentityMetaRow(map.getMap());
	}
	public Map<String, Object> modifyIdentityMetaData(List<CustomMap> colList, CommandMap map) {
		List<Integer> calMetaIdList = new ArrayList<>();
		for(CustomMap o:colList) {
			int al_meta_m = o.getInt("id_meta", 0);
			if(al_meta_m>0) {
				String al_typ = o.getString("typ","").trim();
				if(GlobalConst.DT_CALC.equals(al_typ)) {
					calMetaIdList.add(al_meta_m);
				}
			}
		}
        CommandMap calParam = new CommandMap();
        calParam.put("cal_meta_m_list", calMetaIdList);
        calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
        List<CustomMap> calColInfoMap = this.selectCalcuListByParam(calParam);
		List<CommandMap> inList = IdentityUtil.getModifyCommandListByCommandMapAndPrefix(colList,calColInfoMap, "id_meta", map, "id_meta_","i_id_meta_","i_id_data_","id_data" ,"val");
		List<CommandMap> insDataList = new ArrayList<>();
		List<CommandMap> modDataList = new ArrayList<>();
		List<CustomMap> insList = new ArrayList<>();
		List<Integer> delList = new ArrayList<>();
		List<CustomMap> modList = new ArrayList<>();
		List<CommandMap> modValList = new ArrayList<>();
		Map<Integer,CommandMap> modColMap = new HashMap<>();
		for(CommandMap inMap : inList) {
			if(!"".equals(inMap.getString("modify_type",""))) {
				String modify_type = inMap.getString("modify_type","");
				if("D".equals(modify_type)) {
				    int id_data = inMap.getInt("id_data",0);
				    if(id_data>0) {
				    	delList.add(id_data);
				    }
				}else if("I".equals(modify_type)) {
					inMap.put("id_meta_m", inMap.getString("refer_id_meta_m"));
					inMap.put("id_meta", inMap.getString("refer_id_meta"));
					insDataList.add(inMap);
					insList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}else if("M".equals(modify_type)) {
					inMap.put("id_data", inMap.getInt("id_data",0));
					modDataList.add(inMap);
					modList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}
			}else {
				modValList.add(inMap);
				modColMap.put(inMap.getInt("id_meta"), inMap);
			}
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, insDataList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			for(Map in:inInputList) {
				
			}
			dao.insertMetaDataByGroup(inInputList);
		}
		if(modList.size()>0) {
			for(CommandMap inMap :modDataList) {
				dao.updateMetaData(inMap.getMap());
			}
		}
		if(delList.size()>0) {
			dao.deleteMetaDataByList(delList);
		}
		for(Integer m:modColMap.keySet()) {
			modColMap.get(m);
		}
		Map<String,Object> ret = new HashMap<>();
		ret.put("delList", delList);
		ret.put("modList", modList);
		ret.put("insList", insList);
		return ret;
	}
	public List<CustomMap> selectCalcuListByParam(CommandMap map) {
		return dao.selectCalcuListByParam(map.getMap());
	}
	public void insertIdentityMetaRow(CommandMap map) {
		dao.insertIdentityMetaRow(map.getMap());
	}
	
	public ModelMap insertIdentityMetaData(CommandMap map, ModelMap model) throws Exception {
		CustomMap mainMeta = dao.getIdentityMetaIdMetaM(map.getMap());
		map.put("id_meta_m", mainMeta.getString("id_meta_m"));
        this.insertIdentityMetaRow(map);
        if(!"".equals(map.getString("id_meta_row", ""))) {
    		List<CustomMap> colList = dao.selectIdentity(map.getMap());
    		if(colList==null || colList.size()==0) {
    			throw new Exception("매칭되는 정보를 가져오지 못했습니다.");
    		}
        	List<CustomMap> insDataList = this.insertIdentityMetaData(colList,map);
			model.addAttribute("insDataList",insDataList);
        }
		model.addAttribute("id_meta_row",map.getString("id_meta_row",""));
		return model;
	}

	public List<CustomMap> insertIdentityMetaData(List<CustomMap> colList, CommandMap map) throws Exception {
		List<Integer> calMetaIdList = new ArrayList<>();
		List<Integer> tabColList = new ArrayList<>();
		for(CustomMap o:colList) {
			int al_meta_m = o.getInt("id_meta", 0);
			if(al_meta_m>0) {
				String al_typ = o.getString("typ","").trim();
				if(GlobalConst.DT_CALC.equals(al_typ)) {
					calMetaIdList.add(al_meta_m);
				}else if(GlobalConst.DT_TAL_CODE_GROUP.equals(al_typ)) {
					tabColList.add(al_meta_m);
				}
			}
		}
        CommandMap calParam = new CommandMap();
        calParam.put("cal_meta_m_list", calMetaIdList);
        calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
        List<CustomMap> calColInfoList = this.selectCalcuListByParam(calParam);
		List<CommandMap> inList = this.getCommandListByCommandMapAndPrefix(colList, "id_meta", map, "id_meta_","val");
		for(CommandMap inMap : inList) {
			inMap.put("id_meta_m", inMap.getString("refer_id_meta_m"));
			inMap.put("id_meta", inMap.getString("refer_id_meta"));
			inMap.put("id_meta_row", map.getInt("id_meta_row"));
		}

		List<CommandMap> calColList = MetaUtil.calculateRowData(colList,inList,calColInfoList,map.getInt("id_meta_row"));
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, calColList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertMetaDataByGroup(inInputList);
		}
		List<CustomMap> dataList = dao.selectIdentityMetaDataByIdRow(map.getInt("id_meta_row"));
		return dataList;
	}
	public List<CommandMap> getCommandListByCommandMapAndPrefix(List<CustomMap> baselist,String key,CommandMap in,String prefix,String attrNm){
		List<CommandMap> ret = new ArrayList<CommandMap>();
		for(CustomMap map:baselist) {
			String idMata = map.getString(key,"");
			if(!"".equals(idMata)) {
				String val = in.getString(prefix+idMata,"");
				if(!"".equals(val)) {
					CommandMap cmap = CollectionUtil.getCommandMapByCustomMap(map);
					cmap.put(attrNm, val);
					ret.add(cmap);
				}
			}
		}
		return ret;
	}

	public void deleteMetaData(CommandMap map) {
		dao.deleteIdentityMetaData(map.getMap());
		dao.deleteIdentityMetaRow(map.getMap());
	}


}
