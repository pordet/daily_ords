<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메타 마스터"/>
<!DOCTYPE html>
<html lang="ko">
<front:FrontDynamicHead title="${pageTitle} 관리"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle} 관리</span>
			</div>
			
			<div class="page-content-wrap">
				<form name="frm" id="frm" method="post">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
			     	<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
				              	<div class="tit_cont">검색조건</div>
				              	<div>
						          	<select class="form-control mh-form-control" name="search_col">
					          			<option value="m" ${(param.search_col eq 'm'? 'selected':'')}>이름</option>
					          			<option value="d" ${(param.search_col eq 'd'? 'selected':'')}>설명</option>
						       		</select>
				          		</div>
								<div class="search-group-single-input">
			              			<div class="tit_cont">검색어</div>
			              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${param.search_keyword}">
											<button type="button" class="btn-primary" onclick="javascript:search();">
												<span class="btn_search"></span>
												검색
											</button>
				        		</div>
				   			</div>
				   		</div>
				    </div>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all" style="width:5%;">
								<input type="checkbox">
							</div>
							<div class="header-unit" style="width:15%;">
								<a href="#">
									${pageTitle} ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit" style="width:25%;">
								<a href="#">
									${pageTitle} 이름
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit" style="width:55%;">
								<a href="#">
									${pageTitle} 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_meta_m}')">
									<div class="check-unit" style="width:5%;">
										<input type="checkbox">
									</div>
									<div class="cont-unit" style="width:15%;">${map.id_meta_m}</div>
									<div class="cont-unit" style="width:25%;">${map.meta_m_nm}</div>
									<div class="cont-unit desc-unit" style="width:55%;">${map.meta_m_doc}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			</div>
		</div>
	</div>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function openReg(id_role){
	var frm =document.frm;
	frm.action=contextPath+"/adm/meta/regMetaM.do";
	if(id_role){
		$("#id_meta_m").val(id_role);
	}else{
		$("#id_meta_m").val('');
	}
	$("#frm").submit();
}
function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/meta/metaMList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
</script>
</front:FrontDynamicBody>
</html>
