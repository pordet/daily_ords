package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.IdentityManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.common.service.CodeService;
import kr.co.daily.front.service.IdentityMetaService;

@Controller
public class IdentityMakingController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private IdentityManageService service;
	@Autowired private IdentityMetaService identityMetaService;
	@Autowired private CodeService codeService;

	@RequestMapping("/front/identityView.do")
	public String identityView(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		this.addParamByBetween(commandMap, getMyAccounId(), getLoginUserId(), 0)
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		CommandMap param = new CommandMap();
		if(!"".equals(commandMap.getString("trg_id_meta_m",""))) {
			param.put("id_meta_m", commandMap.get("trg_id_meta_m"));
			CustomMap idenMap = service.getIdentityM(param);
			if(idenMap.getInt("iden_type",0)==GlobalConst.IDEN_TY_LIST) {
				model = this.selectIdentityMetaList(commandMap,idenMap, model);
				return "/front/iden/idenMetaList";
			}else if(idenMap.getInt("iden_type",0)==GlobalConst.IDEN_TY_REG) {
				return this.regIdenMetaData(req, commandMap, model);
			}else {
				return "/err/notFoundPage";
			}
		}else {
			model.addAttribute("ret", GlobalConst.ACT_NO_UPD_TRGT);
			return "/err/notFoundPage";
		}
	}

	@RequestMapping("/front/regIdenMetaData.do")
	public String regIdenMetaData(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		this.addParamByBetween(commandMap, getMyAccounId(), getLoginUserId(), 0)
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		CommandMap param = new CommandMap();
		if(!"".equals(commandMap.getString("trg_id_meta_m",""))) {
			param.put("id_meta_m", commandMap.get("trg_id_meta_m"));
			CustomMap idenMap = service.getIdentityM(param);
			model.addAttribute("map", idenMap);
			
			List<CustomMap> cdList = service.selectIdentityCdListByIdMetaM(param);
			Map<Integer,List<CustomMap>> cdMap = CollectionUtil.getKeyMapByList("id_meta",cdList);
			List<CustomMap> tabSearchList = identityMetaService.selectIdentityMetaTabSearchList(param);
			//(TODO)테이블 조회 콤보일 때 값을 어떻게 저장할지 추후 결정
			for(CustomMap srMap:tabSearchList) {
				CommandMap srchMap = CollectionUtil.getCommandMapByCustomMap(srMap);
				List<CustomMap> rList = identityMetaService.selectIdentityMetaRowForTabCode(srchMap);
				List<CustomMap> coCdList = new ArrayList<>();
				if(rList.size()>0) {
					for(CustomMap riMap : rList) {
						CommandMap sInM = CollectionUtil.getCommandMapByCustomMap(riMap);
						List<CustomMap> tabColList = identityMetaService.selectIdentityMetaCodeList(sInM);
						CustomMap tabMap = service.getIdentityMetaTabCd(srchMap);
						if(tabMap!=null) {
							CustomMap codeMap = new CustomMap();
							for(CustomMap rMap : tabColList) {
								if(rMap.getString("id_meta","").equals(tabMap.getString("parent_cd_id_meta",""))) {
									codeMap.put("parent_cd", rMap.getString("val",""));
									
								}
								if(rMap.getString("id_meta","").equals(tabMap.getString("cd_id_meta"))) {
										codeMap.put("code", rMap.getString("val",""));
								}
								if(rMap.getString("id_meta","").equals(tabMap.getString("nm_id_meta"))) {
									codeMap.put("code_nm", rMap.getString("val",""));
								}
							}
							coCdList.add(codeMap);
						}
					}
				}
				cdMap.put(srMap.getInt("id_meta"), coCdList);
			}
			model.addAttribute("cdMap", cdMap);
			Map<String,CustomMap> inputMap = new HashMap<String,CustomMap>();

			commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_META);
//				commandMap.put("trg_id_meta_m", commandMap.getString("reg_id_meta_m"));
			CustomMap meta_row = identityMetaService.getIdentityMetaRowByKey(commandMap);
			if(meta_row!=null) {
				CommandMap rowParam = CollectionUtil.getCommandMapByCustomMap(meta_row);
				List<CustomMap> valList = identityMetaService.selectIdentityMetaDataByIdMetaRow(rowParam);
				System.out.println("+++++++++++");
				for(CustomMap col:valList) {
					String input_key = col.getString("id_meta_m","")+"_"+col.getString("id_meta", "");
					inputMap.put(input_key, col);
					System.out.println("input_key:"+input_key);
				}
				System.out.println("+++++++++++");
			}
			model.addAttribute("meta_row", meta_row);
			List<CustomMap> colList = identityMetaService.selectIdentityMeta(commandMap);
			List<Map<String,Object>> tabSearchParamList = new ArrayList<>();
			for(CustomMap col:colList) {
//				CommandMap colMap = CollectionUtil.getCommandMapByCustomMap(col);
				String row_key = col.getString("refer_id_meta_m","")+"_"+col.getString("refer_id_meta", "");
				if(col.getInt("id_meta",0)==35) {
					System.out.println('a');
				}
				if(!"".equals(col.getString("refer_id_meta_m",""))&&!"".equals(col.getString("refer_id_meta",""))) {
					CustomMap input = inputMap.get(row_key);
					if(input!=null) {
						if(GlobalConst.DT_TAL_CODE_GROUP.equals(col.getString("type"))) {
							Map<String,Object> tabSearchParam = new HashMap<String,Object>();
							tabSearchParam.put("id_meta_m", col.getInt("id_meta_m"));
							tabSearchParam.put("id_meta", col.getInt("id_meta_m"));
							tabSearchParamList.add(tabSearchParam);
						}else {
							col.put("val", input.getString("val"));
							col.put("id_data", input.getString("id_data",""));
							try {
							col = MetaUtil.transNumberingByCustomMap(col);
							}catch(Exception e) {
								System.out.println("tran Error");
							}
						}
					}
				}
			}
			if(tabSearchParamList.size()>0) {
//				List<CustomMap> tabSearchRetList = identityMetaService.selectIdentityMetaTabSearch(tabSearchParamList);
			}

			List<CustomMap> headerList = new ArrayList<>();
			//Map<id_parent_meta,List<child>
			Map<Integer,List<CustomMap>> childMap = new HashMap<>();
			Map<Integer,CustomMap> curMap = new HashMap<>();
			for(CustomMap col:colList) {
				//(TODO)수정할지 삭제할지 추후 결정
				if(col.getInt("meta_depth",1)==1) {
					headerList.add(col);
					curMap.put(col.getInt("id_meta"), col);
				}else {
					int id_parent_meta = col.getInt("id_parent_meta");
					List<CustomMap> childList = childMap.get(col.getInt("id_parent_meta"));
					if(childList==null) {
						childList = new ArrayList<>();
						childList.add(col);
						childMap.put(col.getInt("id_parent_meta"), childList);
					}else {
						childList.add(col);
					}
					curMap.get(id_parent_meta).put("child_list", childList);
				}
			}
			model.addAttribute("colList", headerList);
			
			return "/front/iden/regIdenMetaData";
		}else if("".equals(commandMap.getString("trg_id_meta_m",""))) {
			throw new RuntimeException("입력 대상 코드가 없습니다.");
		}else {
			model.addAttribute("ret", GlobalConst.ACT_NO_UPD_TRGT);
			return "/err/notFoundPage";
		}
	}

	/**
	 * 
	 * @param commandMap : 화면 관리 정보
	 * @param idenMap : identityM 정보
	 * @param model
	 * @return
	 * @throws Exception
	 */
	private ModelMap selectIdentityMetaList(CommandMap commandMap, CustomMap idenMap, ModelMap model) throws Exception {
		idenMap.put("master_yn", "Y");
		CommandMap param = CollectionUtil.getCommandMapByCustomMap(idenMap);
		
		int max_meta_depth = identityMetaService.getMaxDepthAtIdentity(param);
		Map<String, Object> infoMap = identityMetaService.selectIdentityListForUsing(param,max_meta_depth);
		List<List<CustomMap>> headerList = (List<List<CustomMap>>) infoMap.get("headerList");
//		Map<String,Object> subInfoMap = service.selectSubMetaListForUsing(commandMap,(List<CustomMap>) infoMap.get("aloneList"));
		model.addAttribute("max_meta_depth", max_meta_depth);
		model.addAttribute("headerList", headerList);
		
		//실데이터 컬럼 리스트를 담는 그릇
		List<CustomMap> aloneList = (List<CustomMap>) infoMap.get("aloneList");
		model.addAttribute("aloneList", aloneList);

		commandMap.put("meta_id_meta_m",param.getInt("meta_id_meta_m"));
//		metaKeyParam.put("key_using_type", GlobalConst.KEY_USE_TYPE_EXCEL_UPLOAD);
		//rel > key_m > key 컬럼순으로 사용
		List<CustomMap> childList = identityMetaService.selectIdentityMetaRelByParentIdMetaM(commandMap);
		PageList<CustomMap> todayList = null;
//		try {
			if(childList.size()==0) {
				todayList = identityMetaService.selectIdentityMetaDataContents(commandMap,null);
			}else {
				List<Integer> row_list = new ArrayList<>();
				List<Integer> row_list1 = new ArrayList<>();
				if(!"".equals(commandMap.getString("search_keyword",""))) {
					List<CustomMap> searchList = identityMetaService.selectIdentityMetaDataContents(commandMap,null);
					for(CustomMap ci:searchList) {
						row_list.add(ci.getInt("id_meta_row"));
					}
				}
				if(!"".equals(commandMap.getString("search_dt_srt",""))) {
					List<CustomMap> searchList = identityMetaService.selectIdentityMetaDataContents(commandMap,null);
					for(CustomMap ci:searchList) {
						row_list1.add(ci.getInt("id_meta_row"));
					}
				}
				if(row_list.size()>0 && row_list1.size()>0) {
					ArrayList<Integer> intersection = new ArrayList<>(row_list);  // 첫 번째 ArrayList 복사
			        intersection.retainAll(row_list1);
					commandMap.put("row_list", intersection);
				}else if(row_list.size()>0 && row_list1.size()==0) {
					commandMap.put("row_list", row_list);
				}else if(row_list.size()==0 && row_list1.size()>0) {
					commandMap.put("row_list", row_list1);
				}
				todayList = identityMetaService.selectTreeIdentityMetaDataContents(commandMap,null);
			}
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
		
		Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
		//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
		List<Integer> rawKey = new ArrayList<Integer>();
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_meta_row",0);
			if(key!=0) {
				rawKey.add(key);
				valMap.put(key, cm);
			}
		}
		//(todo)tab_code 가져오는 코드 필요
		if(valMap.size()>0) {
			List<CustomMap> rawList = identityMetaService.selectIdentityMetaDataForSearch(rawKey);
			for(CustomMap cm : todayList) {
				if(childList.size()==0) {
					cm.put("id_meta_m", commandMap.get("id_meta_m"));
				}
				int key = cm.getInt("id_meta_row",0);
				for(CustomMap rm:rawList) {
					if(rm.getInt("id_meta_row",0)==key) {
						if(rm.getInt("id_data",0)==3735) {
							System.out.println("debug rawList id_data:"+rm.getInt("id_data",0));
						}
						System.out.println("rawList id_data:"+rm.getInt("id_data",0));
						if(!"".equals(rm.getString("val",""))) {
							rm = MetaUtil.transNumberingByCustomMap(rm);
						}
						cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
					}
				}
			}
		}
		putModelListPage(todayList, model);
//		idenMap.get("")
		
		//(TODO)하위 테이블 조회용 
		List<CustomMap> relList = identityMetaService.selectIdentityMetaRelList(param);
		relList.add(idenMap);
		for(CustomMap rel : relList) {
			idenMap.put("master_yn", "N");
			CommandMap subParam = new CommandMap();
			subParam.put("id_meta_m", rel.getInt("id_meta_m",0));
			List<CustomMap> subHeaderList = service.selectIdentityList(subParam);
			rel.put("header_list", subHeaderList);
			List<CustomMap> subDetailParamList = service.selectIdentityDetailParamList(param);
			rel.put("detail_param_list", subDetailParamList);
		}
		model.addAttribute("relList", relList);
//		List<CustomMap> detailParamList = identityMetaService.selectIdentityMetaDetailParamList(param);
//		model.addAttribute("detailParamList", detailParamList);
		idenMap.put("header_list", headerList);
//		idenMap.put("detail_param_list", detailParamList);
		model.addAttribute("idenMap", idenMap);
		commandMap.put("succ_yn_id_cg", 8);
		return model;
	}
	@RequestMapping("/front/modifyIdentityMetaDataAjax.do")
	public String modifyIdentityMetaDataAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
//	        service.modifyMetaRow(commandMap);
//    		List<CustomMap> colList = service.selectMeta(commandMap);
//    		Map<String,Object> insDataList = service.modifyMetaData(colList,commandMap);
	        identityMetaService.modifyIdentityMetaRow(commandMap);
    		List<CustomMap> colList = identityMetaService.selectIdentity(commandMap);
    		Map<String,Object> insDataList = identityMetaService.modifyIdentityMetaData(colList,commandMap);
			model.addAttribute("insList",(List<CustomMap>)insDataList.get("insList"));
			model.addAttribute("modList",(List<String>)insDataList.get("modList"));
			model.addAttribute("delList",(List<CustomMap>)insDataList.get("delList"));
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/front/insertIdentityMetaDataAjax.do")
	public String insertIdentityMetaDataAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			model = identityMetaService.insertIdentityMetaData(commandMap,model);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		model.addAttribute("id_meta_row", commandMap.getString("id_meta_row",""));
		return "jsonView";
	}
	@RequestMapping("/front/delIdentityMetaDataAjax.do")
	public String delIdentityMetaDataAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			identityMetaService.deleteMetaData(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
			model.addAttribute("err_msg",e.getMessage());
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/front/joint_test.do")
	public String joint_test(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		return "/front/iden/joint_test";
	}
}
