package daily.common.exception.handler;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

public class ErrorResponse {
    private LocalDateTime timestamp;

    private int status;

    private String reason;

    private String message;

    public ErrorResponse(final HttpStatus httpStatus) {
        this.timestamp = LocalDateTime.now();
        this.status = httpStatus.value();
        this.reason = httpStatus.getReasonPhrase();
    }

    public ErrorResponse(final HttpStatus httpStatus, final String message) {
        this.timestamp = LocalDateTime.now();
        this.status = httpStatus.value();
        this.reason = httpStatus.getReasonPhrase();
        this.message = message;
    }

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
