<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<front:FrontHead title=""/>
<front:FrontTreeBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" top_id_parent_area="0" top_code_group="${GlobalConst.CG_CATEGORY}" hasLeft="true" id_category="${param.id_category}" id_nat_area="${param.id_nat_area}" id_city_area="${param.id_city_area}" type="${(empty param.type?'3':param.type)}">
                 <form id="frm" name="frm" method="post">
                 <input type="hidden" name="type" value="3">
                 <input type="hidden" id="id_area" name="id_area" value="3">
                    <div class="row">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">카테고리명</div>
	                                  <select class="form-control mh-form-control" id="search_category" name="id_category" onchange="javascript:changeCategory(this);">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.code}" ${(param.id_category eq map.code? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">국가명</div>
	                                  <select class="form-control mh-form-control" id="id_nat_area" name="id_nat_area" onchange="javascript:changeNation(this);">
	                                    		<option value="" ${(empty param.id_nat_area ? 'selected':'')}>선택</option>
				                            <c:forEach var="map" items="${natList}" varStatus="i">
	                                    		<option value="${map.id_area}" ${(param.id_nat_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">도시명</div>
	                                  <select class="form-control mh-form-control" id="id_city_area" name="id_city_area" onchange="javascript:changeCity(this);">
	                                    		<option value="" ${(empty param.id_city_area ? 'selected':'')}>선택</option>
		                            <c:forEach var="map" items="${cityList}" varStatus="i">
                                   		<option value="${map.id_area}" ${(param.id_city_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
	                       		<div class="fr">
	                                <div class="form-group inline-group" style="width: 80px">
	                                  <div class="tit_cont">년도</div>
	                                  <select class="form-control mh-form-control" id="search_year" name="year">
	                                    	<option value="2020" ${(param.year eq '2020'? 'selected':'')}>2020</option>
	                                    	<option value="2019" ${(param.year eq '2019'? 'selected':'')}>2019</option>
	                                    	<option value="2018" ${(param.year eq '2018'? 'selected':'')}>2018</option>
	                                    	<option value="2017" ${(param.year eq '2017'? 'selected':'')}>2017</option>
	                                    	<option value="2016" ${(param.year eq '2016'? 'selected':'')}>2016</option>
	                                    	<option value="2015" ${(param.year eq '2015'? 'selected':'')}>2015</option>
	                                    	<option value="2014" ${(param.year eq '2014'? 'selected':'')}>2014</option>
	                                  </select>
	                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:openAjaxInput();">등록</button>
	                            </div>
	                       </div>
	                      </div>
	                     </div>
	                       <div class="row">
								<div class="col-12">
				                    <div class="grid-area">
				                        <!-- 그리드 해더영역 -->
				                        <div class="grid-header">
<!-- 											<div class="header-unit header-check" style="width:5%;"> -->
<!-- 												<a href=""> -->
<!-- 													<div class="form-check mh-form-checkbox"> -->
<!-- 														<input class="form-check-input" type="checkbox" value="" id="defaultCheck1"/> -->
<!-- 														<span class="mh-check"></span> -->
<!-- 														<label class="form-check-label" for="defaultCheck1" title=""></label> -->
<!-- 													</div> -->
<!-- 												</a> -->
<!-- 											</div>각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit" style="width:20%"><a href="">카테고리명<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit" style="width:20%"><a href="">국가명<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit" style="width:20%"><a href="">도시명<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
				                            <div class="header-unit" style="width:20%"><a href="">년도<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                            <div class="header-unit" style="width:20%"><a href="void(0);">조회</a></div>
				                        </div>
				                        <!-- //그리드 해더영역 -->
				                        <!-- 그리드 컨텐츠영역 -->
				                        <div class="grid-contents">
				                            <!--loof-->
				                            <c:forEach var="map" items="${list}" varStatus="i">
				                            <div class="grid-loof-cont">
<!-- 												<div class="cont-unit cont-check" style="width:5%"><div class="form-check mh-form-checkbox"> -->
<%-- 														<input class="form-check-input" type="checkbox" value="${map.user_seq}" name="del_code" id="del_code${i.count}"> --%>
<!-- 														<span class="mh-check"></span> -->
<!-- 														<label class="form-check-label" for="defaultCheck2" title=""></label> -->
<!-- 													</div> -->
<!-- 												</div> -->
				                                <div class="cont-unit" style="width:20%">${map.code_nm}</div>
				                                <div class="cont-unit" style="width:20%">${map.area_nat_nm}</div>
				                                <div class="cont-unit" style="width:20%">${map.area_city_nm}</div>
				                                <div class="cont-unit" style="width:20%">${map.year}</div>
				                                <div class="cont-unit" style="width:20%">
										<div class="btn_group">
											<a href="javascript:void(0);" onclick="javascript:openView('${map.year}','${map.id_category}','${map.id_nat_area}','${map.id_city_area}');"><span class="reassign disabled">조회</span></a>
										</div>
				                                </div>
				                            </div>
				                            </c:forEach>
				                            <!--//loof-->
				                        </div>
				                        <!-- //그리드 컨텐츠영역 -->
				                    </div>
				                    <!--//그리드영역-->
				                    </div>
				                    <!-- //그리드 컨텐츠영역 -->
				                </div>
				                    <!-- paging -->
									<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
				                    <!-- //paging -->
    <div class="modal fade mh-modal" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">데이터입력</h3>
                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row record-wrap">
                    <div class="row" style="width:733px;">
                        <div class="col-12">
                            <div class="tab-content" id="input-content">
                                  <c:forEach var="row" items="${rowlist}" varStatus="i">
	                                  <c:set var="newRow" value="${row.row_num}"/>
	                                    <c:choose>
	                                    	<c:when test="${row.row_type eq 'c'}">
	                                    		<c:set var="row1" value="${row.row_1}"/>
	                                    		<c:set var="row2" value="${row.row_2}"/>
                                    			<div class="row">
	                                      			<div class="col-6">
				                                        <div class="tit_cont">${row1.unit_nm}</div>
				                                        <div class="form-group">
				                                          <c:if test="${not empty map.val}">
				                                          	<input type="hidden" name="i_2020_${row1.id_meta_data}" id="i_2020_${row1.id_meta_data}" value="${row1.val}"/>
				                                          </c:if>
				                                          <input type="text"  name="m_2020_${row1.id_meta}" id="m_2020_${row1.id_meta}" value="${row1.val}" class="form-control mh-form-control">
				                                        </div>
	                                    			</div>
	                                    		<c:if test="${not empty row2}">
	                                    			<div class="col-6">
				                                        <div class="tit_cont">${row2.unit_nm}</div>
				                                        <div class="form-group">
				                                          <c:if test="${not empty map.val}">
				                                          	<input type="hidden" name="i_2020_${row2.id_meta_data}" id="i_2020_${row2.id_meta_data}" value="${row2.val}"/>
				                                          </c:if>
				                                          <input type="text"  name="m_2020_${row2.id_meta}" id="m_2020_${row2.id_meta}" value="${row2.val}" class="form-control mh-form-control">
				                                        </div>
	                                    			</div>
	                                    		</c:if>
	                                    		</div>	
	                                    	</c:when>
	                                    	<c:otherwise>
		                                  <div class="row">
		                                  	  <div class="col-12">
												<div class="tit_cont">${row.unit_nm}</div>	
													<div class="child_table">
					                                  <c:forEach var="child" items="${row.child}" varStatus="j">
			                                    		<c:set var="child_row1" value="${child.row_1}"/>
			                                    		<c:set var="child_row2" value="${child.row_2}"/>
		                                    			<div class="row">
			                                      			<div class="col-6">
						                                        <div class="tit_cont">${child_row1.unit_nm}</div>
						                                        <div class="form-group">
						                                          <c:if test="${not empty map.val}">
						                                          	<input type="hidden" name="i_2020_${child_row1.id_meta_data}" id="i_2020_${child_row1.id_meta_data}" value="${child_row1.val}"/>
						                                          </c:if>
						                                          <input type="text"  name="m_2020_${child_row1.id_meta}" id="m_2020_${child_row1.id_meta}" data_row="${j.count}" value="${child_row1.val}" class="form-control mh-form-control">
						                                        </div>
			                                    			</div>
			                                    		<c:if test="${not empty child_row2}">
			                                    			<div class="col-6">
						                                        <div class="tit_cont">${child_row2.unit_nm}</div>
						                                        <div class="form-group">
						                                          <c:if test="${not empty map.val}">
						                                          	<input type="hidden" name="i_2020_${child_row2.id_meta_data}" id="i_2020_${child_row2.id_meta_data}" value="${child_row2.val}"/>
						                                          </c:if>
						                                          <input type="text"  name="m_2020_${child_row2.id_meta}" id="m_2020_${child_row2.id_meta}" value="${child_row2.val}" class="form-control mh-form-control">
						                                        </div>
			                                    			</div>
			                                    		</c:if>
			                                    		</div>	
		                                        	   </c:forEach>
		                                        </div>
		                                      </div>
		                                   </div>	                                    	
		                                   </c:otherwise>
	                                    </c:choose>
	                               </c:forEach>
	                           </div>
	                       </div>
	                   </div>
                    </div>
                </div>
							<!--modal-body end -->
                <div class="modal-footer mt-20p" id="btnArea1" style="display:block;">
			                        <button type="button" class="btn cancel" onclick="javascript:delData();">삭제</button>
			                        <button type="button" class="btn" onclick="javascript:register(1);">저장</button>
                </div>
              </div>
    	</div>
    </div>
			</form>
    <div class="modal fade mh-modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">데이터조회</h3>
                    <button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row record-wrap">
                    <div class="row" style="width:733px;">
                        <div class="col-12">
                            <div class="tab-content" id="tab-content">
	                       </div>
	                   </div>
                    </div>
                	</div>
                </div>
							<!--modal-body end -->
                <div class="modal-footer mt-20p" id="btnArea1" style="display:block;">
	                    <button type="button" class="btn" data-dismiss="modal">닫기</button>
               </div>
              </div>
    	</div>
    </div>
			
</front:FrontTreeBody>
<script>
function openInput(){
	if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#id_area").val($("#id_nat_area").val());
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#id_category").val($("#search_category").val());
	}
	$("#previewModal").modal('show');	
}
function openAjaxInput(){
	if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#id_area").val($("#id_nat_area").val());
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#id_category").val($("#search_category").val());
	}
	var params = $("#frm").serialize();
	var url = contextPath+"/getDataAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.rowlist.length!=0){
	    			var html="";
		    		$.each(e.rowlist,function(i,d){
		    			if(d.row_type == 'c'){
		    				var row1 = d.row_1;
		    				var row2 = d.row_2;
			    			html+='<div class="row">';
			    			html+='	<div class="col-6">';
			    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
			    			html+='         <div class="form-group">';
			    			if(nanToStr(row1.val)!=""){
				    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
			    			}
			    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
			    			html+='</div>';
			    			html+='</div>';
			    			if(row2!=undefined){
				    			html+='	<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
				    			html+='         <div class="form-group">';
				    			if(nanToStr(row2.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
				    			}
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
				    			html+='</div>';
				    			html+='</div>';
			    			}
			    			html+='</div>';
		    			}else{
// 		    				html='';
		    				var child = d.child;
			    			html+='<div class="row">';
			    			html+='	<div class="col-12">';
			    			html+='	  <div class="tit_cont">'+d.unit_nm+'</div>';
			    			html+='         <div class="form-group">';
			    			html+='<div class="child_table">';
		    				$.each(child,function(i,c){
			    				var row1 = c.row_1;
			    				var row2 = c.row_2;
				    			html+='<div class="row">';
				    			html+='<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
				    			html+='<div class="form-group">';
				    			if(nanToStr(row1.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row1.id_meta_data+'" id="i_'+sel_year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
				    			}
				    			html+='<input type="text"  name="m_'+sel_year+'_'+row1.id_meta+'" id="m_'+sel_year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
				    			html+='</div>';
				    			html+='</div>';
				    			if(row2!=undefined){
					    			html+='	<div class="col-6">';
					    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
					    			html+='         <div class="form-group">';
					    			if(nanToStr(row2.val)!=""){
						    			html+='           	<input type="hidden" name="i_'+sel_year+'_'+row2.id_meta_data+'" id="i_'+sel_year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
					    			}
					    			html+='<input type="text"  name="m_'+sel_year+'_'+row2.id_meta+'" id="m_'+sel_year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
					    			html+='</div>';
					    			html+='</div>';
				    			}
				    			html+='</div>';
		    				});
			    			html+='</div>';
			    			html+='</div>';
			    			html+='</div>';
			    			html+='</div>';
		    			}
		    		});
	    			$("#input-content").empty();
	    			$("#input-content").append(html);
	    			$("#previewModal").modal('show');	
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

function openView(year,id_category,id_nat_area,id_city_area){
	var params = {year:year, id_category:id_category,id_nat_area:id_nat_area,id_city_area:id_city_area};
	var url = contextPath+"/getDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.rowlist.length!=0){
	    			var html="";
		    		$.each(e.rowlist,function(i,d){
		    			if(d.row_type == 'c'){
		    				var row1 = d.row_1;
		    				var row2 = d.row_2;
			    			html+='<div class="row">';
			    			html+='	<div class="col-6">';
			    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
			    			html+='         <div class="form-group">';
			    			if(nanToStr(row1.val)!=""){
				    			html+='           	<input type="hidden" name="i_'+row1.year+'_'+row1.id_meta_data+'" id="i_'+row1.year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
			    			}
			    			html+='<input type="text"  name="m_'+row1.year+'_'+row1.id_meta+'" id="m_'+row1.year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
			    			html+='</div>';
			    			html+='</div>';
			    			if(row2!=undefined){
				    			html+='	<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
				    			html+='         <div class="form-group">';
				    			if(nanToStr(row2.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+row2.year+'_'+row2.id_meta_data+'" id="i_'+row2.year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
				    			}
				    			html+='<input type="text"  name="m_'+row2.year+'_'+row2.id_meta+'" id="m_'+row2.year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
				    			html+='</div>';
				    			html+='</div>';
			    			}
			    			html+='</div>';
		    			}else{
// 		    				html='';
		    				var child = d.child;
			    			html+='<div class="row">';
			    			html+='	<div class="col-12">';
			    			html+='	  <div class="tit_cont">'+d.unit_nm+'</div>';
			    			html+='         <div class="form-group">';
			    			html+='<div class="child_table">';
		    				$.each(child,function(i,c){
			    				var row1 = c.row_1;
			    				var row2 = c.row_2;
				    			html+='<div class="row">';
				    			html+='<div class="col-6">';
				    			html+='	  <div class="tit_cont">'+row1.unit_nm+'</div>';
				    			html+='<div class="form-group">';
				    			if(nanToStr(row1.val)!=""){
					    			html+='           	<input type="hidden" name="i_'+row1.year+'_'+row1.id_meta_data+'" id="i_'+row1.year+'_'+row1.id_meta_data+'" value="'+nanToStr(row1.val)+'"/>';
				    			}
				    			html+='<input type="text"  name="m_'+row1.year+'_'+row1.id_meta+'" id="m_'+row1.year+'_'+row1.id_meta+'" value="'+nanToStr(row1.val)+'" class="form-control mh-form-control">';
				    			html+='</div>';
				    			html+='</div>';
				    			if(row2!=undefined){
					    			html+='	<div class="col-6">';
					    			html+='	  <div class="tit_cont">'+row2.unit_nm+'</div>';
					    			html+='         <div class="form-group">';
					    			if(nanToStr(row2.val)!=""){
						    			html+='           	<input type="hidden" name="i_'+row2.year+'_'+row2.id_meta_data+'" id="i_'+row2.year+'_'+row2.id_meta_data+'" value="'+nanToStr(row2.val)+'"/>';
					    			}
					    			html+='<input type="text"  name="m_'+row2.year+'_'+row2.id_meta+'" id="m_'+row2.year+'_'+row2.id_meta+'" value="'+nanToStr(row2.val)+'" class="form-control mh-form-control">';
					    			html+='</div>';
					    			html+='</div>';
				    			}
				    			html+='</div>';
		    				});
			    			html+='</div>';
			    			html+='</div>';
			    			html+='</div>';
			    			html+='</div>';
		    			}
		    		});
	    			$("#tab-content").empty();
	    			$("#tab-content").append(html);
	    			$("#viewModal").modal('show');	
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function changeCategory(that){
	var new_val = $(that).val();
	search();
}
function changeNation(that){
	var new_val = $(that).val();
// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val($("#id_nat_area").val());
// 	}
// 	if($("#search_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
	search();
	
}
function changeCity(that){
	var new_val = $(that).val();
// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val(new_val);
// 	}
// 	if($("#search_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
	search();	
}
function register(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#id_area").val($("#id_nat_area").val());
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#id_category").val($("#search_category").val());
	}
	var params = jQuery("#frm").serialize();
	var url = contextPath+"/adm/data/insertDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(e.upList.length!=0){
		    		$.each(e.upList,function(i,d){
		    			var html="";
		    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+d.val+'"/>';
		    			$("#m_"+d.year+"_"+d.id_meta).append(html);
		    		});
	    		}
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function delData(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('존, 도시, 국가를 선택하십시오.');
		return;
	}
	if($("#search_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}
	var params = jQuery("#frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.year+"_"+d.id_meta_meta).remove();
	    		});
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/tree_data_list.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function deleteList(){
	confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
}
function deleteConfirm(){
	processing();
	if($("#confirm_yn").val()=='Y'){
	    $("#confirm_yn").val('N');
		var frm =document.frm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/manage/deleteUserList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
	}
}
</script>
</html>