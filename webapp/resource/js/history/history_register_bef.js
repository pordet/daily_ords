//function viewData(id_data){
//	$("#modi_id_data").val(id_data);
//	var params = {id_data:id_data};
//	var url = contextPath+"/getDataFileAjax.do";
//	processing();
//	$.ajax({
//		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
//	    type: "POST", // HTTP 요청 방식(GET, POST)
//	    data : params,
//	    dataType: "json" ,
//	    success : function(e){
//	    	endProcessing();
//	    	if(e.ret==1){
//	    		alertOpen('실패하였습니다.');
//	    	}else{
//	    		var f = e.file;
//	    		$("#modi_title").val(f.title);
//	    		var html='';
//	    		if(f.file_type == 'F'){
//	    			$("#ra22").attr("checked",true);
//	    		}else{
//	    			$("#ra23").attr("checked",true);
//	    		}
//                var down_url = contextPath+"/file/download.do?id_data="+f.id_data;
//                var img_url = contextPath+"/resource/img/ico_download.png";
//                html+='<a href="'+down_url+'">';
//                html+='<img src="'+img_url+'" alt="delete" style="width:28px;height:16px;"/>';
//                html+='</a>';
//	    		$("#modi_file").val(f.file_nm);
//	    		$("#modi_comment").val(f.comment);
//	    		$("#view_down_div").html(html)
//	    		$("#dataViewModal").modal('show');	
//	    	}
//	    },
//	    error : function(e){
//	    	endProcessing();
//	    	if(e.status=='403'){
//		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
//	    	}else if(e.status=='401'){
//			    alertOpen('접근 권한이 없습니다.');
//	    	}else{
//		    	alertOpen('실패하였습니다.');
//	    	}
//	    }
//	});
//	
//}

function register(){
		var formData = new FormData($('#frm')[0]);
		var url = '';
		if(nanToStr($("#id_board").val())==''){
			url += contextPath+"/history/insertHistoryFileAjax.do";
		}else{
			url += contextPath+"/history/modifyHistoryFileAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(nanToStr(e.map)!=""){
		    			$("#id_board").val(e.map.id_board);
		    		}
		    		if(nanToStr(e.attachList)!=''){
			    		var html = '';
		    			$.each(e.attachList,function(i,d){
		    				html+='<div class="col-8 line" id="attach_'+d.id_board_file+'">';
		    				html+='<div class="form-group inline-group">';
		    				html+='    <a href='+contextPath+'/file/data_download.do?id_board_file='+d.id_board_file+'" class="ico_download" style="color:#fff;"><img src="'+contextPath+'/resource/img/history/ico_download.png" alt="다운로드"/>다운로드</a> ';
		    				html+='	   <span>'+d.file_nm+'</span>';
		    				html+='</div>';
		    				html+='</div>';
		    				html+='<div class="col-4 line" id="attach_'+d.id_board_file+'_add" style="text-align:right;">';
		    				html+='    <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delAttachFile('+d.id_board_file+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
		    				html+='</div>';
		    			});
		    		}
		    		$("[id^=addFile_in_div_]").remove();
		    		$("[id^=delFile_in_div_]").remove();
		    		$("#file_list_div").append(html);
		    		addFile();
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}

function delData(){
	var params = $("#frm").serialize();
	var url = contextPath+"/history/deleteHistoryAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$("#id_board").val('');
	    		$("#title").val('');
	    		$("#comment").val('');
	    		$("[id^=attach_]").remove();
	    		$("#btnDelete").hide();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}