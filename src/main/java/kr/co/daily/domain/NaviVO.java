package kr.co.daily.domain;

public class NaviVO {
	private String id_menu;
	private String locale;
	private String menu_nm;
	public String getId_menu() {
		return id_menu;
	}
	public void setId_menu(String id_menu) {
		this.id_menu = id_menu;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getMenu_nm() {
		return menu_nm;
	}
	public void setMenu_nm(String menu_nm) {
		this.menu_nm = menu_nm;
	}
	
}
