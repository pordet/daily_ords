package daily.common.util;

import java.lang.reflect.Method;

public class ReflectionUtil {

	public static Method findMethod(Object bean, String methodName){
		Method[] methods = bean.getClass().getMethods();
		Method method = null;
	    for(int i = 0 ; i < methods.length ; i++) {
	    	if( methods[i].getName().equals(methodName)) {
	    		method= methods[i];
	    	}
	     }
	    return method;
	}
}
