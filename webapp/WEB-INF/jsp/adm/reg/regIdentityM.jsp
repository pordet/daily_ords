<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="${map.meta_m_nm}" />
<!DOCTYPE html>
<html lang="ko">
<meta charset="utf-8">
<c:choose>
	<c:when test="${empty param.trg_id_meta_m}">
		<c:set var="cur_title" value="화면 등록"/>
	</c:when>
	<c:otherwise>
		<c:set var="cur_title" value="${pageTitle} 화면 수정"/>
	</c:otherwise>
</c:choose>
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/screen/identity_m_input.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/screen/identity_m_reg.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span id="cur_title">${cur_title}</span>
			</div>
			<form id="listFrm" name="listFrm" method="post">
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}">
				<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}">
				<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
			</form>
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="trg_id_meta_m" id="trg_id_meta_m" value="${param.trg_id_meta_m}">
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}">
					<input type="hidden" name="id_meta" id="id_meta">
					<input type="hidden" name="id_parent_meta" id="id_parent_meta">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(empty param.trg_id_meta_m?'Y':'N')}">
					
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="15%">
								<col width="30%">
								<col width="15%;">
								<col width="35%">
							</colgroup>
							<tbody>
							    <c:set var="curr_col_num" value="1"/>
                                <c:forEach var="row" items="${colList}" varStatus="i">
                                <c:choose>
									<c:when test="${curr_col_num eq 1}">
<%--   									[curr_col_num는 1:[${curr_low_num}]] --%>
		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td colspan="3">
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 3}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td colspan="3">
														<textarea data-meta-id="${row.id_meta}" style="width:100%" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" placeholder="${row.meta_nm}을 입력하세요.">${map[row.meta_cd]}</textarea>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" disabled placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_BOOL}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<c:set var="ynCheck" value="${(map[row.meta_cd] eq 'Y'?true:false)}"/>
														<div class="single-row">
															<div>
																<input type="radio" name="id_meta_${row.id_meta}" value="Y" id="id_meta_${row.id_meta}_1" ${(ynCheck eq true?'checked':'')}/>
																<label for="id_meta_${row.id_meta}_1">예</label>
															</div>
															<div>
																<input type="radio" name="id_meta_${row.id_meta}" value="N" id="id_meta_${row.id_meta}_2" ${(ynCheck ne true?'checked':'')}/>
																<label for="id_meta_${row.id_meta}_2">아니요</label>
															</div>
														</div>
													</td> 
												<c:if test="${not empty map and not empty map[row.meta_cd]}">
													<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
													<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
												</c:if>
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_NUM}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<c:choose>
													<c:when test="curr_col_num==1">
												</tr>
													<tr>
														<th colspan="4">${row.meta_nm}</th>
													</tr>
													</c:when>
													<c:otherwise>
													<tr>
														<th colspan="4" class="depth2">${row.meta_nm}</th>
													</tr>
													</c:otherwise>
													</c:choose>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty map and not empty map[row.meta_cd]}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty map and not empty map[row.meta_cd]}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
												</c:if>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CODE_GROUP}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<c:set var="cdList" value="${cdMap[row.id_meta]}"/>
														<select  id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}">
														<c:forEach var="cd" items="${cdList}">
															<option value="${cd.code}" ${(map[row.meta_cd] eq cd.code ?'selected':'')}>${cd.code_nm}</option>
														</c:forEach>
														</select>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												<c:set var="curr_col_num" value="2"/>
											</c:when>
											<c:otherwise>
												<tr>
													<th>${row.meta_nm}
<%-- 													[--][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
													<c:set var="curr_col_num" value="2"/>
											</c:otherwise>
		                                </c:choose>
									</c:when>
									<c:otherwise>
<%-- 								    [curr_col_num는 2:[${curr_low_num}]] --%>
		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 1}">
													<th>${row.meta_nm}
<%-- 													[//][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 2}">
													<th>${row.meta_nm}
<%-- 													[//][${curr_low_num}] --%>
													</th>
													<td colspan="3">
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<c:choose>
														<c:when test="curr_col_num==1">
												</tr>
													<tr>
														<th colspan="${fn:length(row.child_list)}">${row.meta_nm}</th>
													</tr>
														</c:when>
														<c:otherwise>
													<tr>
														<th colspan="${fn:length(row.child_list)}" class="depth${row.meta_depth}">${row.meta_nm}</th>
													</tr>
														</c:otherwise>
													</c:choose>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty map and not empty map[row.meta_cd]}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty map and not empty map[row.meta_cd]}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
												</c:if>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" disabled placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_BOOL}">
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<c:set var="ynCheck" value="${(map[row.meta_cd] eq 'Y'?true:false)}"/>
														<div class="single-row">
															<div>
																<input type="radio" name="id_meta_${row.id_meta}" value="Y" id="id_meta_${row.id_meta}_1" ${(ynCheck eq true?'checked':'')}/>
																<label for="id_meta_${row.id_meta}_1">예</label>
															</div>
															<div>
																<input type="radio" name="id_meta_${row.id_meta}" value="N" id="id_meta_${row.id_meta}_2" ${(ynCheck ne true?'checked':'')}/>
																<label for="id_meta_${row.id_meta}_2">아니요</label>
															</div>
														</div>
													</td> 
												</tr>
												<c:if test="${not empty map and not empty map[row.meta_cd]}">
													<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
													<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
												</c:if>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CODE_GROUP}">
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<c:set var="cdList" value="${cdMap[row.id_meta]}"/>
														<select  id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}">
														<c:forEach var="cd" items="${cdList}">
															<option value="${cd.code}" ${(map[row.meta_cd] eq cd.code ?'selected':'')}>${cd.code_nm}</option>
														</c:forEach>
														</select>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
												<c:set var="curr_low_num" value="${curr_low_num+1}"/>
												<c:set var="curr_col_num" value="1"/>
											</c:when>
											<c:otherwise>
													<th>${row.meta_nm}
<%-- 													[==][${curr_low_num}] --%>
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${map[row.meta_cd]}" placeholder="${row.meta_nm}을 입력하세요." ${(row.auto_seq_yn eq 'Y'?' disabled':'')}>
														<c:if test="${not empty map and not empty map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${map[row.meta_cd]}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											<c:set var="curr_col_num" value="1"/>
											<c:set var="curr_low_num" value="${curr_low_num+1}"/>
											</c:otherwise>
		                                </c:choose>
 									</c:otherwise>
                                </c:choose>
                                </c:forEach>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
						</div>
						<div class="board-bottom-group-rear">
								<button type="button" id="btnInit">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete">
									삭제
								</button>
								<button type="button" class="btn-primary" id="btnSave">
									저장
								</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</adm:AdmBody>
<script>
var colArr = [];
<c:forEach var="row" items="${colList}" varStatus="i">
    var item = {
        typ: "${row.typ}",
        id_meta: "${row.id_meta}"
    };
    colArr.push(item);
</c:forEach>

$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function initInputs(){
    for (var i = 0; i < colArr.length; i++) {
        var item = colArr[i];
        var typ = item.typ;
        var id_meta = item.id_meta;
        
        // typ에 따른 input 초기화
        switch (typ) {
            case "${GlobalConst.DT_CODE_GROUP}":
                // text 타입의 input 초기화
                $("#id_meta_" + id_meta + " option:first").prop("selected", true);
                break;
            case "${GlobalConst.DT_BOOL}":
            	$("input[name='id_meta_" + id_meta + "']:eq(1)").prop("checked", true); // 두 번째 라디오 버튼 선택
                $("input[name='id_meta_" + id_meta + "']:not(:eq(1))").prop("checked", false);                // number 타입의 input 초기화
                break;
            default:
                $("#id_meta_"+id_meta).val('');
                // checkbox 타입의 input 초기화
                break;
            // 여기에 다른 typ에 따른 input 초기화 로직을 추가할 수 있습니다.
        }
    	$("#i_id_meta_"+id_meta).remove();
    	$("#i_id_data_"+id_meta).remove();
    }
}
function doList(){
	processing();
	var frm =document.listFrm;
	frm.action=contextPath+"/adm/meta/identityMList.do";
//    	frm.action="/excelDownload.do";
    	frm.submit();
}
function changeTyp(that){
	var html="";
	$("#len_detail").remove();
	if($(that).val()=="${GlobalConst.DT_FLOAT}" || $(that).val()=="${GlobalConst.DT_PERCENT}"){
		html+='<tr id="len_detail"><th>숫자 최대 길이</th><td><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="소수점 포함 숫자 최대길이를 입력"></td>';
		html+='<th>소수점 최대 길이</th><td><input type="text" id="precision_len" name="precision_len" value="" placeholder="소수점 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else if($(that).val()=="${GlobalConst.DT_STR}"){
		html+='<tr id="len_detail"><th>문자열 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="문자열 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else if($(that).val()=="${GlobalConst.DT_NUM}"){
		html+='<tr id="len_detail"><th>숫자형 최대 길이</th><td colspan="3"><input type="text" id="decimal_len" name="decimal_len" value="" placeholder="숫자형 최대길이를 입력"></td></tr>';
		$("#typ_detail").after(html);
	}else{
	}
}

</script>
</html>
