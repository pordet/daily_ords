package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.domain.LineVO;
import kr.co.daily.admin.domain.RelColVO;
import kr.co.daily.admin.domain.RelationVO;
import kr.co.daily.admin.domain.SvgVO;
import kr.co.daily.admin.domain.TableDataVO;
import kr.co.daily.admin.service.IdenetityMetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.common.service.CodeService;

@Controller
public class IdentityMetaManageController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private IdenetityMetaManageService service;
	@Autowired private CodeService codeService;

	@RequestMapping("/adm/meta/relationList.do")
	public String relationList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		PageList<CustomMap> resultList = service.selectMetaRelMList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/iden/relationList";
	}
	
	@RequestMapping("/adm/meta/relationMeta.do")
	public String defineTable(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		commandMap.put("rows",40);
		PageList<CustomMap> resultList = service.selectMetaMList(commandMap);
	    this.putModelListPage(resultList, model);
	    List<CustomMap> tabList = service.selectIdentityMetaFkPosi(commandMap);
	    Set<String> metaMSet = new HashSet<>(); 
	    for(CustomMap tab :tabList) {
	    	if(!"".equals(tab.getString("id_posi",""))){
	    		String id_meta_m = tab.getString("id_posi").split("_")[0];
	    		metaMSet.add(id_meta_m);
	    	}
	    }
	    if(metaMSet.size()>0) {
	    	CommandMap param = new CommandMap();
	    	param.put("meta_m_list", metaMSet);
	    	Map<String,List<CustomMap>> tabColMap = service.selectIdentityMetaList(param);
	    	model.addAttribute("tabColMap", tabColMap);
	    }
	    List<CustomMap> svgList = service.selectIdentityMetaFkSvg(commandMap);
	    List<CustomMap> lineList = service.selectIdentityMetaFkLine(commandMap);
	    if(lineList.size()>0) {
	    	Map<String,CustomMap> svgMap = new HashMap<>();
	    	for(CustomMap line:lineList) {
	    		svgMap.put(line.getString("id_line"), line);
	    	}
	    	model.addAttribute("svgMap", svgMap);
	    }
	    List<CustomMap> relColList = service.selectIdentityMetaFkM(commandMap);
	    model.addAttribute("tabList", tabList);
	    model.addAttribute("svgList", svgList);
	    model.addAttribute("lineList", lineList);
	    model.addAttribute("relColList", relColList);
	    
	    
		return "/adm/iden/defineTable";
	}
	
	@RequestMapping("/adm/meta/getIdentityMetaMAjax.do")
	public String getIdentityMetaMAjax(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap metaM = service.getMetaMByIdMetaM(commandMap);
	    model.addAttribute("metaM", metaM);
	    List<CustomMap> list = service.selectMetaListByIdMetaM(commandMap);
	    model.addAttribute("list", list);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/insertRelationAjax.do")
	public String insertRelationAjax(@RequestBody RelationVO data, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		CommandMap commandMap = new CommandMap();
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	service.insertRelation(commandMap,data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/modifyRelationAjax.do")
	public String modifyRelationAjax(@RequestBody RelationVO data, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		CommandMap commandMap = new CommandMap();
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
//				List<String> codelist = new ArrayList<String>();
//				codelist.add(commandMap.getString("cb_id_group",""));
//				commandMap.put("del_code_list", codelist);
				service.modifyRelation(commandMap,data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteRelationAjax.do")
	public String deleteMetaMListAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codeList = CollectionUtil.getCheckedListByCommandMap(commandMap, "cb_id_meta_m_");
				if(codeList.size()>0) {
					service.deleteRelation(commandMap);
					model.addAttribute("del_key_list", codeList);
				}else {
					ret=GlobalConst.ACT_NO_UPD_TRGT;
				}
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
}
