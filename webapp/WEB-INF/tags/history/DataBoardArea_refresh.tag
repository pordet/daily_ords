<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="id_master" required="false" %>
<%@ attribute name="id_board" required="false" %>
<%@ attribute name="sel_ord_str" required="false" %>
<%@ attribute name="data_type" required="false" %>
<%@ attribute name="top_style" required="false" %>

<commc:LeftBoardTag  id_master="${id_master}" listName="leftList"/>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <aside class="mh-side-nav" style="${top_style}">
            <section class="mh-sn-bar">
                <!-- side bar -->
                <div class="main-tree">
					<div class="tree-content">
<!-- 							1단 -->
                        <ul class="comm-tree">
						<c:forEach var="item" items="${leftList}">
						<c:choose>
							<c:when test="${item.child_cnt eq 0}">
								<li><div style="margin-bottom:3px;"><a href="<c:url value='/history.do?id_master=${id_master}&id_board_master=${item.idBoardMaster}&sel_ord_str=${item.ordStr}'/>"><i class="icon_keyboard"></i>${item.boardNm} (${item.child_cnt})</a></div></li>
							</c:when>
							<c:otherwise>
						    <li class="comm-treeview" id="treeli_${item.lv}_${item.ordStr}">
						        <button type="button" class="comm-tree-expander" onclick="goByTree(${id_master},${item.idBoardMaster},'${item.ordStr}')"></button>
								<div style="margin-bottom:3px;"><a href="<c:url value='/history.do?id_master=${id_master}&id_board_master=${item.idBoardMaster}&sel_ord_str=${item.ordStr}'/>"><i class="icon_folder01"></i>${item.boardNm} (${item.child_cnt})</a></div>
								<ul class="comm-treeview-list"  id="treels_${item.lv}_${item.ordStr}" style="display:none;">
								    <c:set var="nowNum" value="${fn:length(item.child)}"/>
									<c:forEach var="child" items="${item.child}">
									    <c:choose>
									    	<c:when test="${child.child_cnt eq 0}">
												<li><div style="margin-bottom:3px;"><a href="<c:url value='/history.do?id_master=${id_master}&id_board_master=${child.idBoardMaster}&sel_ord_str=${child.ordStr}'/>"><i class="icon_keyboard"></i>${child.boardNm} (${item.child_cnt})</a></div></li>
									    	</c:when>
									    	<c:otherwise>
											    <li class="comm-treeview" id="treeli_${child.lv}_${child.ordStr}"><button type="button" class="comm-tree-expander" onclick="goByTree(${id_master},${child.idBoardMaster},'${child.ordStr}')"></button>
													<div style="margin-bottom:3px;"><a href="<c:url value='/history.do?id_master=${id_master}&id_board_master=${child.idBoardMaster}&sel_ord_str=${child.ordStr}'/>"><i class="icon_folder01"></i>${child.boardNm} (${child.child_cnt})</a></div>
													<ul class="comm-treeview-list" id="treels_${child.lv}_${child.ordStr}" style="display:none;">
													    <c:set var="nowNum" value="${fn:length(child.child)}"/>
														<c:forEach var="grand" items="${child.child}">
															<li><div style="margin-bottom:3px;"><a href="<c:url value='/history.do?id_master=${id_master}&id_board_master=${grand.idBoardMaster}&sel_ord_str=${grand.ordStr}'/>"><i class="icon_keyboard"></i>${grand.boardNm} (${grand.child_cnt})</a></div></li>
														</c:forEach>
													</ul>
												</li>
									    	</c:otherwise>
									    </c:choose>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
						</c:forEach>
						</ul>
                    </div>
				</div>                    
            </section>
        </aside>
<script>
// 통합검색 코드 삽입부
$(function() {
	var sel = "${sel_ord_str}";
	var arr = sel.split("_");
	if(arr.length==1){
		$("[id^=treeli_1_]").each(function(i,d){
			var id = $(d).attr("id");
			var ord_str = id.split("treeli_1_")[1];
			if(arr[0]==ord_str){
				if(!$("#"+id).hasClass("tree-open")){
					$("#"+id).addClass("tree-open");
				}
				$("#treels_1_"+ord_str).show();
			}else{
				if($("#"+id).hasClass("tree-open")){
//					$("#"+id).removeClass("tree-open");
				}
				
			}
		});
	}else if(arr.length==2){
		$("[id^=treeli_1_]").each(function(i,d){
			var id = $(d).attr("id");
			var ord_str = id.split("treeli_1_")[1];
			if(arr[0]==ord_str){
				if(!$("#"+id).hasClass("tree-open")){
					$("#"+id).addClass("tree-open");
				}
				$("#treels_1_"+ord_str).show();
			}else{
				if($("#"+id).hasClass("tree-open")){
//					$("#"+id).removeClass("tree-open");
				}
				
			}
		});
		$("[id^=treeli_2_]").each(function(i,d){
			var id = $(d).attr("id");
			var ord_str = id.split("treeli_2_")[1];
			if(sel==ord_str){
				if(!$("#"+id).hasClass("tree-open")){
					$("#"+id).addClass("tree-open");
				}
				$("#treels_2_"+ord_str).show();
			}else{
				if($("#"+id).hasClass("tree-open")){
//					$("#"+id).removeClass("tree-open");
				}
				
			}
		});
		
	}else if(arr.length==3){
		$("[id^=treeli_1_]").each(function(i,d){
			var id = $(d).attr("id");
			var ord_str = id.split("treeli_1_")[1];
			if(arr[0]==ord_str){
				if(!$("#"+id).hasClass("tree-open")){
					$("#"+id).addClass("tree-open");
				}
				$("#treels_1_"+ord_str).show();
			}else{
				if($("#"+id).hasClass("tree-open")){
//					$("#"+id).removeClass("tree-open");
				}
				
			}
		});
		$("[id^=treeli_2_]").each(function(i,d){
			var id = $(d).attr("id");
			var ord_str = id.split("treeli_2_")[1];
			var sel_str = arr[0]+"_"+arr[1];
			if(sel_str==ord_str){
				if(!$("#"+id).hasClass("tree-open")){
					$("#"+id).addClass("tree-open");
				}
				$("#treels_2_"+ord_str).show();
			}else{
				if($("#"+id).hasClass("tree-open")){
//					$("#"+id).removeClass("tree-open");
				}
				
			}
		});
	}
	
});
function goByTree(id_master,id_board_master,sel_ord_str){
	location.href=contextPath+'/history.do?id_master='+id_master+'&id_board_master='+id_board_master+'&sel_ord_str='+sel_ord_str;
}
</script>