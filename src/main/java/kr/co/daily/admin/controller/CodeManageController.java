package kr.co.daily.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.CodeManageService;
import kr.co.daily.common.GlobalConst;

@Controller
public class CodeManageController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private CodeManageService codeService;

	@RequestMapping("/adm/code/codeList.do")
	public String selectCodeList(CommandMap commandMap, ModelMap model) throws Exception {
		PageList<CustomMap> resultList = codeService.selectCodeList(commandMap);
		putModelListPage(resultList,model);
		return "adm/codeList";
	}
	
	@RequestMapping("/adm/code/regCode.do")
	public String regCode(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		Locale locale = localeResolver.resolveLocale(req);
		commandMap.put("locale", locale.toString());
		List<CustomMap> cglist = codeService.selectUsingCodeGroup(commandMap);
		
		CustomMap map = codeService.getCode(commandMap);
		model.addAttribute("cglist",cglist);
		model.addAttribute("map",map);
		return "adm/reg/regCode";
	}
	
	@RequestMapping("/adm/code/codeCheckAjax.do")
	public String codeCheckAjax(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = codeService.getCodeByCode(commandMap);
		model.addAttribute("map",map);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/insertCodeAjax.do")
	public String insertCodeAjax(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			CustomMap codeGr = codeService.getCodeGroup(commandMap);
			commandMap.put("code_group", codeGr.getString("code_group",""));
	        commandMap = addParamByLoginInfo(commandMap);
	        codeService.insertCode(commandMap);
	        String id_code = commandMap.getString("id_code");
	        model.addAttribute("id_code", id_code);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/deleteCodeList.do")
	public String deleteCode(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			codeService.deleteCode(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		PageList<CustomMap> resultList = codeService.selectCodeList(commandMap);
		putModelListPage(resultList,model);
		model.addAttribute("act_type","D");
		model.addAttribute("ret",ret);
		return "adm/codeList";
	}
	
	@RequestMapping("/adm/code/deleteCodeAjax.do")
	public String deleteCodeAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("id_code") instanceof String) {
				codelist.add(commandMap.getString("id_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("id_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			codeService.deleteCode(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	
	@RequestMapping("/adm/code/modifyCodeAjax.do")
	public String updateCode(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			CustomMap codeGr = codeService.getCodeGroup(commandMap);
			commandMap.put("code_group", codeGr.getString("code_group",""));
	        commandMap = addParamByLoginInfo(commandMap);
			codeService.updateCode(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/codeGroupCheckAjax.do")
	public String codeGroupCheckAjax(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap map = codeService.getCodeGroupByCodeGroup(commandMap);
		model.addAttribute("map",map);
		return "jsonView";
	}
	
//	@RequestMapping("/adm/menuNameCheckAjax.do")
//	public String menuNameCheckAjax(CommandMap commandMap, ModelMap model) throws Exception {
//		CustomMap map = codeService.getMenuByMenuName(commandMap);
//		model.addAttribute("map",map);
//		return "jsonView";
//	}
//	
	@RequestMapping("/adm/code/regCodeGroup.do")
	public String regCodeGroup(CommandMap commandMap, ModelMap model) throws Exception {
        commandMap = addParamByLoginInfo(commandMap);
		CustomMap map = codeService.getCodeGroup(commandMap);
		model.addAttribute("map",map);
		return "adm/reg/regCodeGroup";
	}
	
	@RequestMapping("/adm/code/insertCodeGroupAjax.do")
	public String insertCodeGroupAjax(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        codeService.insertCodeGroup(commandMap);
	        String code_group_id = commandMap.getString("code_group_id");
	        model.addAttribute("code_group_id", code_group_id);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/deleteCodeGroupList.do")
	public String deleteCodeGroup(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<String> codelist = new ArrayList<String>();
			if(commandMap.get("del_code") instanceof String) {
				codelist.add(commandMap.getString("del_code",""));
				commandMap.put("del_code_list", codelist);
			}else {
				String[] delList = (String[])commandMap.get("del_code");
				if(delList!=null) {
					for(Object obj:delList) {
						codelist.add(String.valueOf(obj));
					}
					commandMap.put("del_code_list", codelist);
				}
			}
			codeService.deleteCodeGroup(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		PageList<CustomMap> resultList = codeService.selectCodeGroupList(commandMap);
		putModelListPage(resultList,model);
		model.addAttribute("act_type","D");
		model.addAttribute("ret",ret);
		return "adm/codeGroupList";
	}
	
	@RequestMapping("/adm/code/deleteCodeGroupAjax.do")
	public String deleteCodeGroupAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("id_code_group",""));
				commandMap.put("del_code_list", codelist);
				codeService.deleteCodeGroup(commandMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/updateCodeGroupAjax.do")
	public String insertCodeGroup(CommandMap commandMap, ModelMap model) throws Exception {
//		codeService.del
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
			codeService.updateCodeGroup(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	
	@RequestMapping("/adm/code/codeGroupList.do")
	public String codeGroupList(CommandMap commandMap, ModelMap model) throws Exception {
		try {
		PageList<CustomMap> resultList = codeService.selectCodeGroupList(commandMap);
		putModelListPage(resultList,model);
		}catch(Exception e) {
			e.fillInStackTrace();
		}
		return "adm/codeGroupList";
	}
	
}
