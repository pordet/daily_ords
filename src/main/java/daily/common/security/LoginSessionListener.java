package daily.common.security;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class LoginSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {

//		PushWsLoginUsers pushWsLoginUsers = SpringApplicationContext.getBean("pushWsLoginUsers", PushWsLoginUsers.class );
//		HttpSession session = event.getSession();
//		log.debug(session.toString());
//		pushWsLoginUsers.pushMsgCountAtLogout();
   }
}

// 다른 방식의 구현
//public class LoginSessionListener extends HttpSessionEventPublisher {
//
//	   @Override
//	   public void sessionCreated(HttpSessionEvent event) {
//	      super.sessionCreated(event);
//	      log.debug("sessionCreated");
//	   }
//
//	   @Override
//	   public void sessionDestroyed(HttpSessionEvent event) {
//	      //do something
//	      super.sessionDestroyed(event);
//
//	      @SuppressWarnings("unchecked")
//	      Map<String, Object> userInfo = (Map<String, Object>)event.getSession().getAttribute("loginInfo");
//	      String id = userInfo.get("loginid").toString();
//	      log.debug("sessionDestroyed. id=" + id);
//	   }
//}