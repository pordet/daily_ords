<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="isDashboard" required="false" %>
<%@ attribute name="hasNotCommonPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="isConfirm" required="false" %>
<%@ attribute name="currLocale" required="false" %>
<%@ attribute name="hasBrowser" required="false" %>
<%@ attribute name="hasHeader" required="false" %>
<%@ attribute name="hasImgTop" required="false" %>
<%@ attribute name="hasMenu" required="false" %>
<%@ attribute name="hasLeft" required="false" %>

<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
    <div class="mh-wrapper">
        <c:if test="${not empty hasImgTop}">
        <front:FrontMainTop currLocale="${currLocale}"/>
        </c:if>
        <%--
        <c:if test="${not empty hasBrowser}">
        <front:FrontTopArea currLocale="${currLocale}" top_id_category="${top_id_category}" 
        	top_id_area="${top_id_area}"/>
        </c:if>
        <c:if test="${not empty hasLeft}">
        <front:FrontLeftArea currLocale="${currLocale}"  data_type="${type}"  top_id_category="${top_id_category}" 
        	top_id_area="${top_id_area}" id_category="${id_category}" id_nat_area="${id_nat_area}" id_city_area="${id_city_area}"/>
        </c:if>
        --%>
        <div class="dimmed"></div>
        <!--  mh-container -->
        
        <div class="mh-container">
			<div class="main-content">
				<jsp:doBody/>
			</div>
		</div>
       <!-- // mh-footer -->
    </div>
	<div class="spinner" id="spinnerDiv" style="display:none;">
		<div class="in"><img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/></div>
	</div>
    <!-- //spinner  -->

	<!-- modal alert -->
	<div class="modal fade msg-modal" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="alertMsg">
					<p id="alertMsg">기존 사용자가 존재합니다.</p>
				</div>
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal info -->
	<div class="modal fade msg-modal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="infoMsg">
					<p>기존 사용자가 존재합니다.</p>
				</div>
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
				</div>
			</div>
		</div>
	</div>
    
	<div class="modal fade msg-modal" id="progressModal" tabindex="-1" role="dialog" aria-labelledby="progressModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="processMsg">
					<p>Processing</p>
				</div>
			</div>
		</div>
	</div>
    
	<!-- modal confirm-->
	<div class="modal fade msg-modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="confirmMsg">
					<p>기존 사용자가 존재합니다.</p>
				</div>
				<input type="hidden" id="confirm_yn" value="N">
				<input type="hidden" id="confirm_func_nm">
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:okModel('Y');">확인</button>
					<button type="button" class="btn-default" data-dismiss="modal" onclick="javascript:okModel('N');">취소</button>
				</div>
			</div>
		</div>
	</div>
    <!-- //modal type02  -->
    <!-- // mh-wrapper -->
    <!-- jQuery JS -->
    
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap-3.3.7-dist/js/bootstrap.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker - locale ko js -->
    <script src="<c:url value='/resource/plugins/moment/locale/ko.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <script src="<c:url value='/resource/js/datetime.locale.js'/>"></script>
    <!-- Data Tables JS -->
    <script src="<c:url value='/resource/plugins/datatables/js/jquery.dataTables.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/full_numbers_no_ellipses.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/dataTables.responsive.min.js'/>"></script>
    <!-- nppa JS 
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
    -->
    <script charset="UTF-8" src="<c:url value='/resource/js/comm_ords.js'/>"></script>

</body>
<script>
$(function () {
	$(".header-unit").click(function(e){
		e.preventDefault();
		var targetId = "h_";
		var element = $(this).find("[id^='" + targetId + "']");
		alert($(element).val());
		$("#sidx").val($(element).val());
	 	if($(this).find("span").hasClass("sorting_asc")){
			$("#sord").val("DESC");
		}else{
			$("#sord").val("ASC");
		}
	 	doList($("#page").val());
	});
  });
	
	function alertOpen(msg){
	  	$("#alertMsg").html(msg);
	  	$('#alertModal').modal('show');
	}
	function infoOpen(msg){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	}
	function infoOpen(msg,func){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function progressOpen(msg){
	  	$("#progressMsg").html(msg);
	  	$('#progress').modal('show');
	}
	function confirmOpen(msg,func){
	  	$("#confirmMsg").html(msg);
	  	$('#confirmModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function callbackFunc(func){
	  	$("#confirm_func_nm").val(func);
	}
	function closeModel(){
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	function okModel(key){
		if(key=='Y'){
			$("#confirm_yn").val('Y');
			var funcNm = $("#confirm_func_nm").val();
			//eval(funcNm+"()");
			window[funcNm]();
		}
		else{
			$("#confirm_yn").val('N');
		}
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	
	function processing(){
		$("#spinnerDiv").show();
	}
	function endProcessing(){
		$("#spinnerDiv").hide();
	}
</script>
