<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<commc:MenuTag user_seq="${id_user}" locale="${currLocale}" listName="menuList"/>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>고정된 앞쪽 메뉴</title>
    <link rel="stylesheet" href="<c:url value='/resource/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css'/>">
    <link rel="stylesheet" href="<c:url value='/resource/plugins/font-awesome/css/all.css'/>">
	<link rel="stylesheet" href="<c:url value='/resource/css/new_ords.css'/>">
	<script>
 		var contextPath = "${pageContext.request.contextPath}";
	</script>
</head>
<body>
<div class="wrapper">
	<div class="header-wrap">
		<div class="header-group">
			<div class="header-group-inner">
				<div class="header-logo-area">
			    	<div class="_logo">
						<a href="<c:url value='/' />" >
							<img src="<c:url value='/resource/img/logo.png'/>" >
						</a>
					</div>
					<div class="_logo_sm">
						<a href="<c:url value='/' />" >
							<img src="<c:url value='/resource/img/logo_sm.png'/>" >
						</a>
					</div>
				</div>
				<div class="header-search-area">
					<div>
						<input id="search" type="text" class="header-search-input" value="${(not empty paramMap.search_keyword ?paramMap.search_keyword:'')}">
						<button id="searchBtn" type="button" class="btn-blank header-search-btn" title="검색">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</div>
				</div>
				<div class="header-user-area">
					<div class="user-area-inner">
						<c:choose>
							<c:when test="${not empty userinfo}">
								<div id="login_div">
									<div class="top-log">
										<span>${userinfo.name} 님</span>
										<button type="button" class="btn-default" onclick="javascript:goAdmin('${userinfo.id}');">관리</button>
										<button type="button" class="btn-info" onclick="javascript:logout();">
											<span class="glyphicon glyphicon-off"></span>
										</button>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div id="login_div">
									<ul class="util">
										<li><a href="<c:url value='/join.do'/>">회원가입</a></li>
										<li><a href="<c:url value='/loginPage.do'/>">로그인</a></li>
									</ul>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="menu">
		  <ul class="menu-level-1">
			<c:forEach var="item" items="${menuList}" varStatus="status">
			    <li>
			    	<a href="<c:url value='${item.menu_url}?id_menu=${item.id_menu}'/>">${item.menu_nm}</a>
				      <ul class="menu-level-2">
						<c:forEach var="subMenu" items="${item.child}" varStatus="i">
				        <li><a href="<c:url value='${subMenu.menu_url}'/>?id_menu=${subMenu.id_menu}">${subMenu.menu_nm}</a></li>
				        </c:forEach>
				      </ul>
			    </li>
		    </c:forEach>
		  </ul>
		</div>
	</div>
	<div class="contents">
	</div>
</div>
</body>
</html>