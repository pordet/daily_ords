var befMeta = '';
function registerData(){
//	befMeta = iniParam.meta;
//	iniParam.meta = 'content';
	$("input[type=text]").each(function(i,d){
		if($(this).attr("dataNumOnly")==1){
			if(!checkNonNumber($(this).val())){
				$(this).val('')
			}
		}
	})
	var multiFileCnt = $("input[name^=mf1_]").length;
	if(inputLow==0 && multiFileCnt==0 ){
		alertOpen("저장할 항목이 없습니다. 단위 관리 프로그램에서 항목을 등록하십시오.");
		return;
	}
	var varCnt =0;
	$.each($("[id^=m_"+$("#search_year").val()+"_]"),function(i,d){
		if($(this).val()!=''){
			varCnt++;
		}
	})
	$.each($("[id^=d_"+$("#search_year").val()+"_]"),function(i,d){
		if($(this).val()!=''){
			varCnt++;
		}
	})
	varCnt+=$("[id^=mf_]").length;
	varCnt+=$("[id^=fe_div_]").length;
	
//	$.each($("[id^=fi_"+$("#search_year").val()+"_]"),function(i,d){
//		if($(this).val()!=''){
//			varCnt++;
//		}
//	})
	if(varCnt==0){
		alertOpen("저장할 항목이 없습니다.");
		return;
	}
//	if(modifyList.size>0){
//		alertOpen("파일 수정 상태로는 저장할 수 없습니다.");
//		return;
//	}
	register();
}
function register(){
//	var id_meta = iniParam.meta;
//	var id_meta = 'a';
//	if(id_meta!=''){
		var formData = new FormData($('#reg_frm')[0]);
		var url = contextPath+"/adm/data/insertFileIncludeDataAjax.do";
//		if(formData){
//			alertOpen("저장할 항목이 없습니다.항목 값들을 등록하십시오.");
//			return;
//		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
				console.log("컨텐츠 화면 등록결과");
				console.log(e);
		    	endProcessing();
//		    	iniParam.meta=befMeta;
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(nanToStr(e.yd)==''){
		    			alertOpen("저장할 항목이 없습니다.항목 값들을 등록하십시오.");
		    			return;
		    		}else{
		    			openAjaxInput();
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
		    		redraw(e);
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
}
function register_bak(){
//	var id_meta = iniParam.meta;
//	var id_meta = 'a';
//	if(id_meta!=''){
		var formData = new FormData($('#reg_frm')[0]);
		var url = contextPath+"/adm/data/insertFileIncludeDataAjax.do";
//		if(formData){
//			alertOpen("저장할 항목이 없습니다.항목 값들을 등록하십시오.");
//			return;
//		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
				console.log("컨텐츠 화면 등록결과");
				console.log(e);
		    	endProcessing();
//		    	iniParam.meta=befMeta;
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(nanToStr(e.yd)==''){
		    			alertOpen("저장할 항목이 없습니다.항목 값들을 등록하십시오.");
		    			return;
		    		}else{
			    		if(e.upList.length!=0){
				    		$.each(e.upList,function(i,d){
				    			// 무슨 기능인지
				    			if(nanToStr(d.addfile)!=''){
				    				var file = d.addfile[0];
				    				var inHtml='';
					    				inHtml+='	<div id="fe_div_'+d.id_meta+'">';
					    				inHtml+='		<div class="select-label">첨부된 파일</div>';
					    				inHtml+='		<div class="form-group">';
					    				inHtml+='			<input type="hidden" name="fi_'+d.year+'_'+d.id_meta+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
					    				inHtml+='			<a href="'+contextPath+'/file/download_add.do?id_data_file='+file.id_data_file+'">';
					    				inHtml+='				<span>' + file.file_nm + '</span>';
					    				inHtml+='				<span class="glyphicon glyphicon-download-alt"></span>';
					    				inHtml+='			</a>';  
					    				inHtml+='		</div>';
					    				inHtml+='	</div>';
					    				$("#file_div_"+d.id_meta).after(inHtml);
				    			}
				    		});
				    		$.each(e.upList,function(i,d){
				    			if(d.data_type=='1' || d.data_type=='8'){
					    			var html="";
					    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta+'" id="i_'+d.year+"_"+d.id_meta+'" value="'+nanToStr(d.val)+'"/>';
					    			$("#m_"+d.year+"_"+d.id_meta).append(html);
				    			}
				    			if(d.data_type=='3'){
					    			$("#hm_"+d.year+"_"+d.id_meta).val('');
					    			$("#m_"+d.year+"_"+d.id_meta).val('');
					    			var html="";
					    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta+'" id="i_'+d.year+"_"+d.id_meta+'" value="'+nanToStr(d.val)+'"/>';
					    			$("#m_"+d.year+"_"+d.id_meta).after(html);
				    			}
				    		});
				    		$.each(e.upDetailList,function(i,d){
				    			var html="";
				    			if(nanToStr(d.id_meta_unit)!=''){
					    			html+='<input type="hidden" name="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" id="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" value="'+nanToStr(d.val)+'"/>';
					    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).append(html);
				    			}
				    		});
			    		}
			    		if(e.insertMultiFileList.length!=0){
				    		$.each(e.insertMultiFileList,function(i,f){
				    			var d = f.map;
			    				var inHtml='';
			    				inHtml+='		<td>';
			    				inHtml+='			<div class="file-upload-group">';
			    				inHtml+='         	<input type="hidden" name="fi_'+d.year+'_'+d.id_meta_data+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
			    				inHtml+='				<input class="fileName" type="text" disabled value="'+nanToStr(d.file_nm)+'" >';
			    				inHtml+='				<button class="btn-primary" type="button" title="첨부">';
			    				inHtml+='					<span class="glyphicon glyphicon-paperclip"></span>';
			    				inHtml+='					첨부';
			    				inHtml+='				</button>';
				    			inHtml+='           	<input type="file" name="f_'+d.year+'_'+d.id_meta+'" class="file-input-hidden" onchange="javascript: document.getElementById(\'m_'+d.year+'_'+d.id_meta+'\').value = this.value"/>';
			    				inHtml+='			</div>';
			    				inHtml+='		</td>';
			    				inHtml+='		<td>';
			    				inHtml+='			<input class="fileDesc" type="text" disabled value="'+nanToStr(d.file_desc)+'">';
			    				inHtml+='		</td>';
			    				inHtml+='		<td>';
			    				inHtml+='			<div class="multi-btn-area">'
			    				inHtml+='				<div class="multi-btn-wrap">'
			    				inHtml+='					<div class="btn-wrap">'
			    				inHtml+='						<a href="'+contextPath+'/file/multi_file_download.do?id_data_file='+nanToStr(d.id_data_file)+'" download="'+nanToStr(d.file_nm)+'">';
			    				inHtml+='							<span class="glyphicon glyphicon-download-alt"></span>';
			    				inHtml+='						</a>';
			    				inHtml+='					</div>';
			    				inHtml+='					<div class="btn-wrap">'
			    				inHtml+='						<a href="javascript:void(0);" onClick="javascript:modifyRow(\''+ d.id_meta+'_'+d.row_num +'\','+d.id_data_file+');">';
			    				inHtml+='							<span class="glyphicon glyphicon-pencil" title="수정"></span>';
			    				inHtml+='						</a>';
			    				inHtml+='					</div>';
			    				inHtml+='					<div class="btn-wrap">'
			    				inHtml+='						<a href="javascript:void(0);" onClick="javascript:delFileRow('+d.id_data_file+',\''+ d.id_meta+'_'+d.row_num +'\');">';
			    				inHtml+='							<span class="glyphicon glyphicon-remove" title="삭제"></span>';
			    				inHtml+='						</a>';
			    				inHtml+='					</div>';
			    				inHtml+='				</div>';
			    				inHtml+='			</div>';
			    				inHtml+='		</td>';
				    			$("#mf_"+d.id_meta+"_"+d.row_num).html(inHtml);
				    		});
			    		}
			    		if(e.updateFileList.length!=0){
				    		$.each(e.updateFileList,function(i,d){
				    			if(nanToStr(d.upfile)!=''){
				    				var file = d.upfile[0];
				    				$("#hm_"+$("#search_year").val()+"_"+d.id_meta).val('');
				    				$("#m_"+$("#search_year").val()+"_"+d.id_meta).val('');
//					    			var html="";
//					    			html+='<input type="hidden" name="i_'+$("#search_year").val()+"_"+d.id_meta+'" id="i_'+$("#search_year").val()+"_"+d.id_meta+'" value="'+nanToStr(d.val)+'"/>';
//					    			$("#m_"+$("#search_year").val()+"_"+d.id_meta).append(html);
				    				var inHtml='';
					    				inHtml+='	<div class="select-label">첨부된 파일</div>';
					    				inHtml+='   <div class="form-group">';
					    				inHtml+='         <input type="hidden" name="fi_'+$("#search_year").val()+'_'+file.id_meta_data+'" id="fi_'+$("#search_year").val()+'_'+file.id_meta_data+'" value="'+nanToStr(file.id_meta_data)+'"/>';
					    				inHtml+='         <a href="'+contextPath+'/file/download_add.do?id_data_file='+file.id_data_file+'" download="'+file.file_nm+'">';
					    				inHtml+='         	<span>' + file.file_nm + '</span>';
					    				inHtml+='         	<span class="glyphicon glyphicon-download-alt"></span>';
										inHtml+='         </a>';  
					    				inHtml+='   </div>';
					    				$("#fe_div_"+d.id_meta).html(inHtml);
				    			}
				    		});
			    		}
		    			
		    		}
		    		if(modifyList.size>0){
		    			modifyList.forEach(function(d,i,arr){
		    				var inHtml = modifyList.get(i);
		    				modifyList.delete(i);
		    				$("#"+i).html(inHtml);
		    			});
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
		    		redraw(e);
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
}
function redraw(e){
	var con = "";
	if(nanToStr(e.yd)==''){
		con+='<tbody>';
		con+='<tr><td>검색결과없음</td></tr>';
		con+='</tbody>';
		$("#result_table").html(con);
	}else{
			
		var st_year = e.searchBetween.st_year;
		var ed_year = e.searchBetween.ed_year;
    	if(e.rowList.length>0){
    		con+='<thead>';
    		con+='<tr>';
    		con+='<th class="" colspan="'+e.maxLvl+'">항목 </th>';
    			for(var year=Number(st_year);year<=Number(ed_year);year++){
    	    		con+='<th scope="col">'+year+'년</th>';
    			}
    		con+='</tr>';
    		con+='</thead>';
    	}
		con+='<tbody>';
		if(e.rowList.length==0){
    		con+='<tr><td>검색결과없음</td></tr>';
			inputLow=0;
 		}
		if(e.rowList.length>0){
    		con+='<tr>';
			$.each(e.rowList,function(i,d){
    			$.each(d.child,function(j,f){
    				var colspan = 1;
    				if(f.colspan !=''){
    					colspan=f.colspan;
    				}
    				var rowspan = 1;
    				if(nanToStr(f.rowspan) !=''){
    					rowspan=nanToStr(f.rowspan);
    				}
    				if(nanToStr(f.head)!=''){
    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && (f.data_type =='1' || f.data_type =='2' || f.data_type=='7')){
		    	    		con+='<td class="row-header" colspan="'+colspan+'" rowspan="'+rowspan+'">';
		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'" dataType="'+f.data_type+'">';
		    	    		con+='<span class=""></span>';
		    	    		con+='<label class="" for="check_'+f.id_meta+'"></label>';
		    	    		con+=' ' + f.unit_nm + '</td>';
    					}else{
		    	    		con+='<td class="row-header" colspan="'+colspan+'" rowspan="'+rowspan+'">'+f.unit_nm+'</td>';
    					}
    				}
    			});
    			for(var year=Number(st_year);year<=Number(ed_year);year++){
    				var isEmpty =false;
	    			$.each(d.child,function(j,f){
	    				if(year == f.year){
	    					isEmpty = true;
	    					if(f.data_type==3){
	    						if(nanToStr(f.addfilelist)!=''){
	    							con+='<td class="">';
    	    						$.each(f.addfilelist,function(idx,data){
    	    							con+='<a href="'+contextPath+'/file/download_add.do?id_data_file='+nanToStr(data.id_data_file)+'">';
    	    							con+='	<span class="glyphicon glyphicon-download-alt"></span>';
										con+='</a>';
    	    						})
	    							con+='</td>';
	    						}
	    					}else if(f.data_type==4){
    							con+='<td class="">';
    							con+='<a href="javascript:showMultiFile('+nanToStr(f.id_meta_data)+');">';
    							con+='	<span class="glyphicon glyphicon glyphicon-list"></span>';
								con+='</a>';
    							con+='</td>';
	    					}else{
			    	    		con+='<td class="">'+f.val+'</td>';
	    					}
	    				}
	    			});
	    			if(isEmpty ==false){
	    	    		con+='<td>-</td>';
	    			}
    			}
	    		con+='</tr>';
			});
			inputLow=e.rowList.length;
		}
		con+='</tbody>';
		$("#result_table").html(con);
	}
}
function delData(){
	var varCnt =0;
	$.each($("[id^=m_"+$("#search_year").val()+"_]"),function(i,d){
		if($(this).val()!=''){
			varCnt++;
		}
	})
	$.each($("[id^=d_"+$("#search_year").val()+"_]"),function(i,d){
		if($(this).val()!=''){
			varCnt++;
		}
	})
	varCnt+=$("[id^=mf_]").length;
	varCnt+=$("[id^=fe_div_]").length;
	
//	$.each($("[id^=fi_"+$("#search_year").val()+"_]"),function(i,d){
//		if($(this).val()!=''){
//			varCnt++;
//		}
//	})
	if(varCnt==0){
		alertOpen("삭제할 항목이 없습니다.");
		return;
	}
	deleteData();
}
function deleteData(){
	if(inputLow==0){
		alertOpen("삭제할 항목이 없습니다. 단위 관리 프로그램에서 항목을 등록하십시오.");
		return;
	}
//	m,d,fi,f
	if(iniParam.zone != ''){
		$("#id_area").val(iniParam.zone);
	}else if(iniParam.cityArea != ''){
		$("#id_area").val(iniParam.cityArea);
	}
	
	var params = jQuery("#reg_frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.year+"_"+d.id_meta).remove();
	    		});
	    		if(nanToStr(e.delFileList)!=''){
		    		$.each(e.delFileList,function(i,d){
		    			$("#m_"+d.year+"_"+d.id_meta).val('');
		    			$("#hm_"+d.year+"_"+d.id_meta).val('');
		    		});
	    		}
    			$("[id^=fe_div_]").remove();
    			$("[id^=file_list_tab_]").empty();
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
	    		$.each(e.delDetailList,function(i,d){
	    			$("#di_"+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit).remove();
	    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).val('');
	    		});
	    		if(nanToStr(e.searchBetween)!=''){
	    			changeBetween(e.searchBetween,e.yd);
	    		}else{
	    			nonBetween();
	    		}
	    		redraw(e);
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function changeBetween(b,yd){
	var st_html='<option value="">없음</option>';
	var ed_html='<option value="">없음</option>';
	for(var i = yd.min_year;i<=yd.max_year;i++){
		if(i==b.st_year){
			st_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			st_html+='<option value="'+i+'">'+i+'</option>';
		}
		if(i==b.ed_year){
			ed_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			ed_html+='<option value="'+i+'">'+i+'</option>';
		}
	}
	$("#st_year").html(st_html);
	$("#ed_year").html(ed_html);
	iniParam.startYear = yd.min_year;
	iniParam.endYear = yd.max_year;
}
function nonBetween(){
	var st_html='<option value="">없음</option>';
	var ed_html='<option value="">없음</option>';
	
	$("#st_year").html(st_html);
	$("#ed_year").html(ed_html);
	iniParam.startYear = "";
	iniParam.endYear = "";
}
