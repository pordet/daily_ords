<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메타 화면 구성"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle} 관리">
<style>
#canvas-content {
    overflow: auto; /* 내부 컨텐츠에 스크롤 적용 */
    width: 100%; /* 내부 컨텐츠 너비 100% 설정 */
    height: 100%; /* 내부 컨텐츠 높이 100% 설정 */
    position:absolute;
    box-sizing: border-box; /* 패딩이 컨텐츠 크기에 포함되도록 설정 */
}
.hidden-table { 
    width:auto; 
    display:none;
}
.td_sel {
	color : white;
	background-color : rgba(8, 143, 220, 1);
}
.hidden-table tr td {
	border: 1px solid red;
    width: 100px; /* 너비를 100px로 설정합니다. */
    height: 20px; /* 높이를 50px로 설정합니다. */
}
.hidden-table tr th {
	border: 1px solid blue;
    width: 100px; /* 너비를 100px로 설정합니다. */
    height: 20px; /* 높이를 50px로 설정합니다. */
}
</style>
</adm:AdmHead>
	
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle} 관리</span>
			</div>
			
			<div class="page-content-wrap">
				<div id="page-content-wrap-top">
					<c:set var="mListSize" value="${fn:length(list)}"/>
					<c:set var="mLowSize" value="${(mListSize/4)+1}"/>
					<c:set var="mLastCellIdx" value="${mListSize%4}"/>
					<c:set var="mCurrLowNum" value="1"/>
					<table>
						<colgroup>
							<col width="25%">
							<col width="25%">
							<col width="25%">
							<col width="25%">
						</colgroup>
						<tbody>
	                    <c:forEach var="row" items="${list}" varStatus="j">
			                <c:set var="i" value="${j.count}"/>
			                <c:choose>
			                    <c:when test="${i%4 eq 1}">
			                        <tr class="grid-loof-cont">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                    </c:when>
			                    <c:when test="${mCurrLowNum ne mLowSize && i%4 eq 0}">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                        </tr>
			                    </c:when>
			                    <c:when test="${mCurrLowNum eq mLowSize && i%4 eq 0}">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                        </tr>
			                        <c:set var="mCurrLowNum" value="${mCurrLowNum+1}"/>
			                    </c:when>
			                    <c:otherwise>
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                    </c:otherwise>
			                </c:choose>
			            </c:forEach>
						</tbody>
					</table>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
					<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
					<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
				</div>
		        <div id="page-content-wrap-bottom">
		            	<div id="canvas">
		            		<div id="canvas-content">
								<table class="hidden-table" id="hidden_table">
					                <tr><td>Row1</td></tr>
					                <tr><td>Row2</td></tr>
					                <tr><td>Row3</td></tr>
					            </table>
							</div>
		            	</div>
		        </div>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	 $(".draggable").draggable({
	        helper: 'clone'
    });
	    $("div.td_sel").draggable({
	        helper: 'clone',
	        drop: function(event, ui) {
                console.log("addID daadoes not start with 'drag_'");
	        	
	        }
	    });
//     드롭 영역 설정
    $("div.td_sel").droppable({
        helper: 'clone',
       drop: function(event, ui) {
            // 드롭된 요소의 좌표를 가져옴
            var dropX = ui.offset.left;
            var dropY = ui.offset.top;

            // 화살표 생성
            var arrow = $('<div class="arrow"></div>');
            arrow.appendTo("#canvas-content");

            // 화살표 위치 설정
            arrow.css({
                "position": "absolute",
                "left": dropX + "px",
                "top": dropY + "px"
            });

            // 드래그된 요소의 td_sel 클래스를 가진 td의 좌표를 가져옴
            var dragX = ui.helper.offset().left;
            var dragY = ui.helper.offset().top;

            // 화살표 길이 계산
            var length = Math.sqrt(Math.pow(dropX - dragX, 2) + Math.pow(dropY - dragY, 2));

            // 화살표 각도 계산
            var angle = Math.atan2(dropY - dragY, dropX - dragX) * 180 / Math.PI;

            // 화살표 회전 및 길이 설정
            arrow.css({
                "transform": "rotate(" + angle + "deg)",
                "width": length + "px"
            });
        }
    });
	    
    $( ".draggable" ).draggable({
        helper: 'clone',
        stop: function(event, ui) {
            var pos = $('#canvas').position();
            var canvasWidth = $('#canvas').width(); // 부모 요소인 canvas의 초기 너비를 저장합니다.
            var canvasContentPos = $('#canvas-content').position(); // canvas-content의 위치를 가져옵니다.
            var canvasContentWidth = $('#canvas-content').width(); // canvas-content의 초기 너비를 저장합니다.
            var drag_id = $(this).attr("id")
            var id_meta_m = '';
            if (drag_id.startsWith("h_id_meta_m_")) {
            	id_meta_m = drag_id.substring("h_id_meta_m_".length); // "drag_"의 길이만큼 제거
            	drawTable(id_meta_m);
                // remaining에는 "drag_"를 제외한 나머지 문자열이 포함됨
            } else {
                console.log("ID does not start with 'drag_'");
            }
            var newTable = $('#hidden_table').clone().attr('id', 't_id_meta_m_'+id_meta_m).css({
															                'display': 'block',
															                'position': 'absolute',
															                'left': ui.position.left,
														                    'top': ui.position.top - $('#canvas').position().top, // newTable의 top 위치를 ui가 놓여진 위치의 top 좌표에서 canvas-content의 top 좌표를 뺀 값으로 설정합니다.
															                'width': 'auto'
															            });
            if(ui.position.top < pos.top){
            	newTable.css('top',pos.top+10);
            }
            var newTableWidth = $('#hidden_table').width()+ui.position.left;
            if(newTableWidth > canvasWidth){
            	$('#canvas-content').css('width', newTableWidth + 50);
// 				$('#canvas-content').css('overflow', 'auto');
            }
            $('#canvas-content').append(newTable);
            newTable.draggable({
                containment: '#canvas-content'
            });
         // 드래그된 테이블 내의 td_sel 클래스를 가진 <td>도 드래그 가능하게 만듭니다.
            newTable.find("td.td_sel").draggable({
                helper: 'clone',
//                 containment: '#canvas-content'
				stop:function(event, ui) {
	                console.log("aaa");
				}
            });
        }
    });
});
$(document).on("drop", "[id^='d_meta_']", function(event, ui) {
    console.log("드롭 이벤트 발생:", this);
});

$(document).on("mouseover", "[id^='t_id_meta_m_']", function() {
	var parentId = $(this).attr("id"); // 클릭된 요소의 부모 테이블의 아이디를 가져옴
    var counter = $("#" + parentId + " .td_sel").length; // 부모 테이블 내 td_sel 클래스를 가진 요소들의 개수를 세어 카운터에 저장

    // 부모 테이블의 드래그 가능 여부를 설정
    if (counter > 0) {
        $(this).css("cursor", "pointer"); // 마우스 포인터를 변경
    } else {
        $(this).css("cursor", "arrow"); // 마우스 포인터를 변경
    }
});
$(document).on("click", "[id^='d_meta_']", function() {
    // 클릭된 요소에 class 속성을 추가합니다.
    if($(this).hasClass("td_sel")){
        $(this).removeClass("draggable").removeClass("td_sel");
    }else{
        $(this).addClass("draggable").addClass("td_sel").droppable({ // dropable 클래스 추가 및 드롭 가능하게 만듦
            helper: 'clone',
            drop: function(event, ui) {
                console.log("드롭 이벤트 발생:", this);
            }
        });;
    }

	var parentId = $(this).closest("table").attr("id"); // 클릭된 요소의 부모 테이블의 아이디를 가져옴
    var counter = $("#" + parentId + " .td_sel").length; // 부모 테이블 내 td_sel 클래스를 가진 요소들의 개수를 세어 카운터에 저장

    // 부모 테이블의 드래그 가능 여부를 설정
    if (counter > 0) {
        $("#" + parentId).draggable("disable"); // 드래그 불가능하게 만듦
    } else {
        $("#" + parentId).draggable("enable"); // 드래그 가능하게 만듦
    }
});
$(document).on("dblclick", "[id^='d_meta_']", function() {
    // 클릭된 요소에 class 속성을 추가합니다.
    if($(this).hasClass("td_sel")){
        $(this).removeClass("draggable").removeClass("td_sel");
    }else{
        $(this).addClass("draggable").addClass("td_sel");
    }
});

function drawTable(id_meta_m){
	var params = {id_meta_m:id_meta_m};
	var insert_yn = $("#insert_yn").val();
	var url = contextPath+"/adm/meta/getIdentityMetaMAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    	}else{
	    		var html ='';
	    		var m = e.metaM;
	    		html+='<tr><th><div id="h_meta_m_'+m.id_meta_m+'">' +nanToStr(m.meta_m_nm)+'</div></th></tr>'; 
	    		$.each(e.list,function (i,d){
		    		html+='<tr><td><div id="d_meta_'+d.id_meta+'">' +nanToStr(d.meta_nm)+'</div></td></tr>'; 
	    		});
	    		$("#t_id_meta_m_"+id_meta_m).html(html);
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
	
}
function openReg(id_role){
	var frm =document.frm;
	frm.action=contextPath+"/adm/meta/regMeta.do";
	if(id_role){
		$("#id_meta_m").val(id_role);
	}else{
		$("#id_meta_m").val('');
	}
	$("#frm").submit();
}
function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/meta/metaList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function aa(){
	   // 더블 클릭 이벤트 핸들러를 추가합니다.
 $(this).dblclick(function() {
     // 현재 요소의 내용을 input 태그로 교체합니다.
     var currentText = $(this).text();
     $(this).html("<input type='text' value='" + currentText + "'>");

     // input 태그에 포커스를 설정합니다.
     $(this).find("input").focus();

     // input 태그에서 포커스가 빠져나가면 값을 저장하고 다시 텍스트로 표시합니다.
     $(this).find("input").blur(function() {
         var newText = $(this).val();
         $(this).parent().text(newText);
     });
 });

 // 포인터를 올릴 때 텍스트로 바꾸는 이벤트 핸들러를 추가합니다.
 $(this).hover(function() {
     // 현재 요소의 내용을 input 태그로 교체합니다.
     var currentText = $(this).text();
     $(this).html("<input type='text' value='" + currentText + "'>");

     // input 태그에 포커스를 설정합니다.
     $(this).find("input").focus();

     // input 태그에서 포커스가 빠져나가면 값을 저장하고 다시 텍스트로 표시합니다.
     $(this).find("input").blur(function() {
         var newText = $(this).val();
         $(this).parent().text(newText);
     });
 }, function() {
     // 포인터가 요소를 벗어나면 텍스트로 변경합니다.
     var newText = $(this).find("input").val();
     $(this).text(newText);
 });
	
}
</script>
</html>
