package kr.co.daily.restful.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class RestfulController {
	@SuppressWarnings("unchecked")
	
    private static String TEST_SERVER_PATH;
    
    @Value("#{global['openapi.interface.url']}")
    public void setUserName(String userName) {
    	TEST_SERVER_PATH = userName;
    }
	@RequestMapping(value = "/test/getCitizen.do", produces = MediaType.APPLICATION_JSON_VALUE,  method = RequestMethod.POST)
	public String getAuth(@RequestParam HashMap<String,String> map,Model model)
	{
		final String uri = TEST_SERVER_PATH;
	    RestTemplate restTemplate = new RestTemplate();
	    
//	    CommandMap map = new CommandMap();
//	    map.put("username", NIDA_USER_NAME);
//	    map.put("password", NIDA_USER_PWD);
//	    CustomMap result = restTemplate.getForObject(uri, CustomMap.class);
	    Map<String, Object> result = restTemplate.postForObject(uri, map, Map.class);
	    model.addAttribute("result", result);
	    return "jsonView";
	}

}
