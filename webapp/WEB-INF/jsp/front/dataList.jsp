<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<html lang="ko">
<front:FrontHead title=""/>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" top_id_area="${param.top_id_area}" top_id_category="${param.top_id_category}" 
	hasLeft="true" type="${param.type}" id_category="${param.id_category}" id_nat_area="${param.id_nat_area}" id_city_area="${param.id_city_area}" id_area="${param.id_area}">
</front:FrontBody>
</html>