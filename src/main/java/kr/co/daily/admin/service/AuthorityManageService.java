package kr.co.daily.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.security.ReloadableFilterInvocationSecurityMetadataSource;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.AuthManageDAO;
import kr.co.daily.admin.dao.MenuManageDAO;
import kr.co.daily.admin.dao.ResourceDAO;
import kr.co.daily.admin.dao.UserGroupDAO;
import kr.co.daily.admin.dao.UserManageDAO;
import kr.co.daily.admin.dao.UserRoleDAO;
import kr.co.daily.common.GlobalConst;

import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
@Service
public class AuthorityManageService {

	@Autowired UserManageDAO dao;
	@Autowired UserGroupDAO userGroupDao;
	@Autowired UserRoleDAO roleDao;
	@Autowired AuthManageDAO authDao;
	@Autowired MenuManageDAO menuDao;
	@Autowired ResourceDAO rsrcDao;
	@Autowired SecurityMetadataSource securityMetadataSource;
	
	public CommandMap insertResourceRole(CommandMap map) throws Exception {
		CommandMap newKey = new CommandMap();
		if("url".equals(map.getString("resource_type"))) {
			map.put("reg_by_menu", 'N');
			rsrcDao.insertResource(map.getMap());
			authDao.insertResourceRole(map.getMap());
			newKey.put("id_resource", map.getInt("id_resource"));
			newKey.put("id_resource_role", map.getInt("id_resource_role"));
		}else {
			// 메뉴 등록 총 2 단계 중 1단계 : menu 타입 resource 생성 단계
			CommandMap menuTypeMap = new CommandMap();
			menuTypeMap.put("reg_by_menu", 'N');
			menuTypeMap.put("resource_type", "menu");
			menuTypeMap.put("resource_pattern", map.getString("code_id_menu"));
			menuTypeMap.put("sort_order", map.getString("sort_order","1"));
			menuTypeMap.put("id_role", map.getString("id_role"));
			menuTypeMap.put("login_usr_seq", map.getString("login_usr_seq"));
			menuTypeMap.put("id", map.getString("id"));
			rsrcDao.insertResource(menuTypeMap.getMap());
			authDao.insertResourceRole(menuTypeMap.getMap());
			newKey.put("id_resource", menuTypeMap.getInt("id_resource"));
			newKey.put("id_resource_role", menuTypeMap.getInt("id_resource_role"));
			// 메뉴 등록 총2 단계 중 2단계 : 메뉴에 등록된 url을 url 등록하는 단계,reg_by_menu 컬럼에 'Y' 와 메뉴 resource의 id를 id_parent_resource에 넣어야 한다.
			String id_menu = map.getString("code_id_menu","");
			CommandMap param = new CommandMap();
			param.put("id_menu", id_menu);
			CustomMap menuMap = menuDao.getMenuInfoByIdMenu(param.getMap());
			CommandMap menu = CollectionUtil.getCommandMapByCustomMap(menuMap);
			menu.put("id_parent_resource", menuTypeMap.getInt("id_resource"));
			menu.put("reg_by_menu", 'Y');
			menu.put("resource_type", "url");
			menu.put("resource_pattern", menu.getString("menu_url"));
			menu.put("sort_order", map.getString("sort_order"));
			menu.put("id_role", map.getString("id_role"));
			menu.put("login_usr_seq", map.getString("login_usr_seq"));
			menu.put("id", map.getString("id"));
			rsrcDao.insertResource(menu.getMap());
			authDao.insertResourceRole(menu.getMap());
		}
		this.reloadSecurityMetadata();
		return newKey;
	}

/**
	 * 메뉴가 2뎁스인 경우의 역할 변경
	 * @param map
	 */
	public void modifyResourceRole(CommandMap map) throws Exception {
		CustomMap res = rsrcDao.getResource(map.getMap());
		if(GlobalConst.RESOURCE_TYPE_MENU.equals(res.getString("resource_type",""))) {
			//(TODO) 수정전 resource_type이 menu 인 경우의 권한 수정
//				if(!res.getString("resource_pattern","").equals(map.getString("code_id_menu",""))){
				// 메뉴가 변경된 경우의 권한 수정
				map.put("resource_pattern", map.getString("code_id_menu"));
				rsrcDao.modifyResource(map.getMap());
				authDao.modifyResourceRole(map.getMap());
				// 메뉴명을 변경했을 경우에 menu에 종속된 url도 바꿔줘야 함.
				CommandMap paramForChild = new CommandMap();
				paramForChild.put("id_resource", res.getString("id_resource"));
				CustomMap child = rsrcDao.getChildResource(paramForChild.getMap());
				CommandMap childParam = CollectionUtil.getCommandMapByCustomMap(child);
				if(!"".equals(childParam.getString("id_resource",""))) {
					// 메뉴를 변경했을 때 1단계, 변경했을 경우에 menu에 종속된 url도 바꿔줘야 함.
					CommandMap param = new CommandMap();
					param.put("id_menu", map.getString("code_id_menu"));
					CustomMap menuMap = menuDao.getMenuInfoByIdMenu(param.getMap());
					CommandMap menu = CollectionUtil.getCommandMapByCustomMap(menuMap);
					menu.put("id_resource", childParam.getString("id_resource"));
					menu.put("id_parent_resource",res.getString("id_resource"));
					menu.put("reg_by_menu", 'Y');
					menu.put("resource_type", "url");
					menu.put("resource_pattern", menu.getString("menu_url"));
					menu.put("sort_order", map.getString("sort_order"));
					menu.put("id_role", map.getString("id_role"));
					menu.put("login_usr_seq", map.getString("login_usr_seq"));
					menu.put("id", map.getString("id"));
					rsrcDao.modifyResource(menu.getMap());
					
					/** 메뉴를 변경했을 때 2단계 : 변경된 menu의 url을 가져오기 위해 새로 변경된 메뉴 id(code_id_menu) 파라미터값으로 
					 *                       메뉴 정보에 종속된 url도 바꿔줘야 함.
					 */
//					CommandMap param1 = new CommandMap();
//					param1.put("id_resource", childParam.getString("id_resource"));
					CommandMap menuMap1 = new CommandMap();
					menuMap1.put("id_menu", map.getString("code_id_menu",""));
					CustomMap newMenu = menuDao.getMenuInfoByIdMenu(menuMap1.getMap());
					if(newMenu !=null) {
						String newResourcePattern = newMenu.getString("id_menu", "");
						CommandMap childResourceRoleParam = new CommandMap();
						childResourceRoleParam.put("id_resource", child.getString("id_resource"));
						CustomMap childResourceRoleMap = authDao.getResourceRoleByChildResource(childResourceRoleParam.getMap());
						CommandMap childResourceRole = new CommandMap();
						childResourceRole.put("resource", newResourcePattern);
						childResourceRole.put("id_resource_role", childResourceRoleMap.getString("id_resource_role"));
						childResourceRole.put("id_resource", childParam.getString("id_resource"));
						childResourceRole.put("id_role", map.getString("id_role"));
						childResourceRole.put("login_usr_seq", map.getString("login_usr_seq"));
						childResourceRole.put("id", map.getString("id"));
						authDao.modifyResourceRole(childResourceRole.getMap());
					}else {
						throw new Exception("권한 변경 오류");
					}
					
//					CustomMap parentResourceMap = authDao.getResourceRoleByIdParentResource(param1.getMap());
				}
//					else {
//						throw new Exception("menu 권한 변경시 menu 관련 url 권한 변경 오류");
//					}
//				}else {
//					// 메뉴명이 변경되지 않고, 권한만 변경된 경우의 처리
//					// 수정후 resource_type이 url로 바뀌는 경우의 권한 수정 - 현 시스템에서는 resource_type 변경 불가이기 때문에 이 로직으로는 안 옴
//					map.put("reg_by_menu", 'N');
//					rsrcDao.modifyResource(map.getMap());
//					authDao.modifyResourceRole(map.getMap());
				
//				CommandMap paramForParent = new CommandMap();
//				paramForParent.put("id_resource", map.getString("id_resource", ""));
//				CustomMap childMap = rsrcDao.getChildResource(paramForParent.getMap());
//				CommandMap paramForChild = new CommandMap();
//				paramForChild.put("id_resource", childMap.getString("id_resource", ""));
//				rsrcDao.deleteResource(paramForChild.getMap());
//				
//				// child resource_role ���� ����
////				CommandMap param1 = new CommandMap();
////				param1.put("id_resource", childMap.getString("id_resource", ""));
////				CustomMap childResourceMap = authDao.getResourceRoleByChildResource(param1.getMap());
//				CommandMap childResourceRole = new CommandMap();
//				childResourceRole.put("id_resource", childMap.getString("id_resource", ""));
//				authDao.deleteResourceRole(childResourceRole.getMap());
//				}
		}else {
			//(TODO) 수정전 resource_type이 menu이 아닌 url 경우의 권한 수정
			if(GlobalConst.RESOURCE_TYPE_MENU.equals(map.getString("resource_type",""))){
				// 수정후 resource_type이 menu로 바뀌는 경우의 권한 수정
				CommandMap menuTypeMap = new CommandMap();
				menuTypeMap.put("reg_by_menu", 'N');
				menuTypeMap.put("id_resource", map.getString("id_resource"));
				menuTypeMap.put("resource_type", "menu");
				menuTypeMap.put("resource_pattern", map.getString("code_id_menu"));
				menuTypeMap.put("sort_order", map.getString("sort_order","1"));
				menuTypeMap.put("id_role", map.getString("id_role"));
				menuTypeMap.put("login_usr_seq", map.getString("login_usr_seq"));
				menuTypeMap.put("id", map.getString("id"));
				rsrcDao.modifyResource(menuTypeMap.getMap());
				
				authDao.modifyResourceRole(map.getMap());
				
				CommandMap newKey = new CommandMap();
				newKey.put("id_resource", menuTypeMap.getInt("id_resource"));
				newKey.put("id_resource_role", menuTypeMap.getInt("id_resource_role"));
				
				String id_menu = map.getString("code_id_menu","");
				CommandMap param = new CommandMap();
				param.put("id_menu", id_menu);
				CustomMap menuMap = menuDao.getMenuInfoByIdMenu(param.getMap());
				CommandMap menu = CollectionUtil.getCommandMapByCustomMap(menuMap);
				menu.put("id_parent_resource", menuTypeMap.getInt("id_resource"));
				menu.put("reg_by_menu", 'Y');
				menu.put("resource_type", "url");
				menu.put("resource_pattern", menu.getString("menu_url"));
				menu.put("sort_order", map.getString("sort_order"));
				menu.put("id_role", map.getString("id_role"));
				menu.put("login_usr_seq", map.getString("login_usr_seq"));
				menu.put("id", map.getString("id"));
				rsrcDao.insertResource(menu.getMap());
				authDao.insertResourceRole(menu.getMap());
			}else {
				// 수정후에도 resource_type이 변경되지 않은 경우에 처리
				rsrcDao.modifyResource(map.getMap());
				authDao.modifyResourceRole(map.getMap());
			}
		}
		this.reloadSecurityMetadata();
	}

	public int getResourcePattern(CommandMap map) {
		return rsrcDao.getResourcePattern(map.getMap());
	}

	public void deleteResourceRole(CommandMap map) throws Exception {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>)map.getMap().get("del_code_list");
		for(String id_resource : list) {
			CommandMap param = new CommandMap();
			param.put("id_resource", id_resource);
			CustomMap resource = rsrcDao.getResource(param.getMap());
			if(resource == null) continue;
			if("url".equals(resource.getString("resource_type"))) {
				authDao.deleteResourceRole(param.getMap());
				rsrcDao.deleteResource(param.getMap());
			}else {
				CustomMap childResource = rsrcDao.getChildResource(param.getMap());
				if(childResource!=null) {
				CommandMap childParam = CollectionUtil.getCommandMapByCustomMap(childResource);
					authDao.deleteResourceRole(childParam.getMap());
					rsrcDao.deleteResource(childParam.getMap());
				}
				authDao.deleteResourceRole(param.getMap());
				rsrcDao.deleteResource(param.getMap());
			}
		}
		this.reloadSecurityMetadata();
	}

	public void deleteResourceRoleByIdParentResource(CommandMap map) {
		authDao.deleteResourceRoleByIdParentResource(map.getMap());
	}

	public PageList<CustomMap> selectResourceAndRole(CommandMap map) {
		return authDao.selectResourceAndRole(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public CustomMap getResourceRole(CommandMap map) {
		return authDao.getResourceRole(map.getMap());
	}

	public List<CustomMap> selectRoleList(CommandMap map) {
		return roleDao.selectRoleList(map.getMap());
	}

	public PageList<CustomMap> selectUserRoleList(CommandMap map) {
		return roleDao.selectUserRoleList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public CustomMap getUserRole(CommandMap map) {
		return roleDao.getUserRole(map.getMap());
	}

	public void insertRole(CommandMap map) {
		roleDao.insertRole(map.getMap());
	}

	public void modifyRole(CommandMap map) {
		roleDao.modifyRole(map.getMap());
	}

	public List<CustomMap> selectRoleListByGroup(CommandMap map) {
		return roleDao.selectRoleListByGroup(map.getMap());
	}

	public void deleteRoleList(CommandMap map) {
		roleDao.deleteRoleList(map.getMap());
		authDao.deleteGroupRoleByIdRole(map.getMap());
		rsrcDao.deleteResourceByIdRole(map.getMap());
		authDao.deleteResourceRoleByIdRole(map.getMap());
	}
	public void reloadSecurityMetadata() throws Exception {
	    if (securityMetadataSource instanceof FilterInvocationSecurityMetadataSource) {
	    	ReloadableFilterInvocationSecurityMetadataSource filterMetadataSource = 
	            (ReloadableFilterInvocationSecurityMetadataSource) securityMetadataSource;

	        filterMetadataSource.reload();
	    }
	}
}
