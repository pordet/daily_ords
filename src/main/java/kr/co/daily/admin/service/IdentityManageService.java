package kr.co.daily.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.IdentityManageDAO;
import kr.co.daily.common.GlobalConst;

@Service
public class IdentityManageService {

	@Autowired private IdentityManageDAO dao;
	public List<CustomMap> selectIdentityList(CommandMap  map) {
		return dao.selectIdentityList(map.getMap());
	}
	public List<CustomMap> selectIdentityDetailParamList(CommandMap map) {
		return dao.selectIdentityDetailParamList(map.getMap());
	}
	public CustomMap getIdentityM(CommandMap map) {
		return dao.getIdentityM(map.getMap());
	}
	/**
	 * identitiy_rel에서 child_id_meat_m에 해당하는 identity_m 정보를 가져오는 쿼리
	 * @param map : 부모 identity_m의 id_meta_id를 파라미터로 사용
	 * @return
	 */
	public List<CustomMap> selectIdentityRelList(CommandMap map) {
		return dao.selectIdentityRelList(map.getMap());
	}
	public PageList<CustomMap> selectIdentityMList(CommandMap map) {
		return dao.selectIdentityMList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}
	public PageList<CustomMap> selectIdentityPageList(CommandMap map) {
		return dao.selectIdentityList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}
	public void insertIdentityM(Map<String, Object> map) {
		dao.insertIdentityM(map);
	}
	public void modifyIdentityM(Map<String, Object> map) {
		dao.modifyIdentityM(map);
	}
	public List<CustomMap> selectSearchParamList(CommandMap map) {
		return dao.selectSearchParamList(map.getMap());
	}
	public List<CustomMap> selectCdListForIdentity(CommandMap map) {
		return dao.selectCdListForIdentity(map.getMap());
	}
	public List<CustomMap> selectIdentityListForHead(CommandMap map) {
		return dao.selectIdentityListForHead(map.getMap());
	}
	public List<CustomMap> selectIdentityCdList(CommandMap map) {
		return dao.selectIdentityCdList(map.getMap());
	}
	public List<CustomMap> selectIdentity(CommandMap map) {
		return dao.selectIdentity(map.getMap());
	}
	public List<CustomMap> selectIdentityByIdMetaM(CommandMap map) {
		return dao.selectIdentityByIdMetaM(map.getMap());
	}
	public CustomMap getIdentityByIdMetaMAndIdMeta(CommandMap map) {
		return dao.getIdentityByIdMetaMAndIdMeta(map.getMap());
	}
	public List<CustomMap> selectChildMetaListByIdParentMeta(CommandMap map) {
		return dao.selectChildMetaListByIdParentMeta(map.getMap());
	}
	public Map insertIdentity(CommandMap  map) {
		switch(map.getString("typ", GlobalConst.DT_NUM)) {
		case GlobalConst.DT_NUM :
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATE :
			map.put("decimal_len", 10);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATETIME :
			map.put("decimal_len", 19);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		}
		map.put("trg_id_meta_m", map.get("id_meta_m"));
		int next_ord = dao.getNextOrdAtIdentityByIdMetaMAndMetaDepth(map.getMap());
		map.put("ord", next_ord);
		dao.insertIdentity(map.getMap());
		CustomMap meta = dao.getIdentityByIdMetaMAndIdMeta(map.getMap());
		return meta;
	}
	public CustomMap modifyIdentity(CommandMap map) {
		switch(map.getString("typ", GlobalConst.DT_NUM)) {
		case GlobalConst.DT_NUM :
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATE :
			map.put("decimal_len", 10);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		case GlobalConst.DT_DATETIME :
			map.put("decimal_len", 19);
			map.put("precision_len", 0);
			map.put("len_type", 1);
			break;
		}
		dao.modifyIdentity(map.getMap());
		return dao.getIdentityByIdMetaMAndIdMeta(map.getMap());
	}
	public List<String> deleteIdentityByList(CommandMap map) {
		List<String> del_code_list = dao.selectIdentityListByInIdMeta(map.getMap());
		CommandMap delMap = new CommandMap();
		delMap.put("del_code_list", del_code_list);
		dao.deleteIdentityByList(delMap.getMap());
		return del_code_list;
	}
	public List<CustomMap> selectIdentityCdListByIdMetaM(CommandMap map) {
		return dao.selectIdentityCdListByIdMetaM(map.getMap());
	}
	public void deleteIdentityM(List<String> list) {
		CommandMap map = new CommandMap();
		map.put("del_key_list", list);
		dao.deleteIdentityByIdMetaMList(map.getMap());
		dao.deleteIdentityMByIdMetaMList(map.getMap());
	}
	public PageList<CustomMap> selectTreeIdentityMetaDataContents(CommandMap map, CommandMap spMap) {
		return dao.selectTreeIdentityMetaDataContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}
	public List<CustomMap> selectIdentityMetaDataForSearch(List<Integer> lowKey) {
		return dao.selectIdentityMetaDataForSearch(lowKey);
	}
	public CustomMap getIdentityMetaTabCd(CommandMap map) {
		return dao.getIdentityMetaTabCd(map.getMap());
	}
	public CustomMap getIdentityMetaExcel(CommandMap map) {
		return dao.getIdentityMetaExcel(map.getMap());
	}
}
