<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
			<div class="tab-list tab-layer col-2">
                <ul id="usrTab"><!-- 타입 1 : 3단 -->
                    <li class="on">
                        <a id="tab_layer_1" href="javascript:void(0);">사용자 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_2" href="javascript:void(0);">사용자 그룹 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_3" href="javascript:void(0);">Role 관리</a>
                    </li>
                    <li>
                        <a id="tab_layer_4" href="javascript:void(0);">권한 관리</a>
                    </li>
                </ul>
            </div>
            <div class="tab-wrap">
				<div id="tab_layer_1" class="tab-cont on">
                                  <form id="frm" name="frm" method="post">
                                  <input type="hidden" id="doubleYn" value="Y">
								  <input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
								  <input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
								  <input type="hidden" name="user_id" id="user_id" value="${param.user_id}">
								  <input type="hidden" name="page" id="page" value="${param.page}">
                                  <div class="row">
                                      <div class="col-6">
                                        <div class="tit_cont">사용자 명</div>
                                        <div class="form-group">
                                          <input type="text"  name="user_nm" id="user_nm" value="${map.user_nm}" class="form-control mh-form-control">
                                        </div>
                                      </div>
                                      <div class="col-6">
                                       </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-12">
                                        <div class="tit_cont">사용자 ID</div>
                                        <div class="inline-group form-group">
                                          <input type="text" id="id_user" name="id_user" value="${map.id}" class="form-control mh-form-control"><button type="button" class="btn h36" onclick="javascript:checkDouble();" id="checkBtn" <c:if test="${param.insert_yn eq 'N'}"> disabled</c:if>>중복체크</button>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-12">
                                        <div class="tit_cont">사용자 그룹</div>
                                        <div class="form-group">
	                                        <select class="form-control mh-form-control" name="id_group" id="id_group">
	                                           <c:forEach var="cg" items="${groupList}">
	                                           <c:choose>
	                                           	<c:when test="${map.id_group eq cg.id_group}">
		                                           	<option value="${cg.id_group}" selected>${cg.group_nm}</option>
	                                           	</c:when>
	                                           	<c:otherwise>
		                                           	<option value="${cg.id_group}">${cg.group_nm}</option>
	                                           	</c:otherwise>
	                                           </c:choose>
	                                           </c:forEach>
	                                         </select>
                                        </div>
                                      </div>
                                  </div>
                                <div class="row">
									<div class="col-4">
										<div class="tit_cont">사용여부</div>
										<div class="inline-group">
											<div class="mh-radio-wrapper">
												<input type="radio" class="mh-radio-tab" name="use_yn" value="Y" id="ra1" checked />
												<label for="ra1">예</label>
												<input type="radio" class="mh-radio-tab" name="use_yn" value="N" id="ra2" />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</div>
									<div class="col-8">
									</div>
								</div>
                                  </form>
	                     <div class="btn-area">
	                        <button type="button" class="btn cancel" onclick="javascript:doList();">목록</button>
	                        <button type="button" class="btn" onclick="javascript:register();">등록</button>
	
	                    </div>
                </div>
				<div id="tab_layer_2" class="tab-cont">
	        	</div>
				<div id="tab_layer_3" class="tab-cont">
	            </div>
				<div id="tab_layer_4" class="tab-cont">
	            </div>
    		</div>
</adm:AdmBody>
<script>
$(function () {
	$("#tab_layer_1").click(function(){
		location.href="<c:url value='/adm/user/userList.do'/>?id_menu=12";
	});
	$("#tab_layer_2").click(function(){
		location.href="<c:url value='/adm/user/userGroupList.do'/>?id_menu=9";
	});
	$("#tab_layer_3").click(function(){
		location.href="<c:url value='/adm/user/roleList.do'/>?id_menu=10";
	});
	$("#tab_layer_4").click(function(){
		location.href="<c:url value='/adm/user/permissionList.do'/>?id_menu=11";
	});
});
function checkDouble(){
	var insert_yn = $("#insert_yn").val();
	if(insert_yn =='Y'){
		$("#doubleYn").val('Y');
		var alias = $("#alias").val();
		$.ajax({
			url: contextPath+"/adm/aliasCheckAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: {alias : alias},                // HTTP 요청과 함께 서버로 보낼 데이터
	//	    data: {DocumentNumber : docNum , KeyPhrase : 'JDZytH@qDQJ^B!5wNWThYV7vkS&t*gyzHP8@s#4t^6AeM+&f_!V!&hRWySLUC-_HdrE8WDvhVm6HR&FWTyGk9#yHQP4L86yMF*eW3w*NLCbFP%wQyAv+m8q&?Ge*x@A$' },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "POST",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	if(e.map){
		    		alertOpen('기존에 사용중인 ID입니다.');
		    	}else{
		    		alertOpen('사용 가능한 ID입니다.');
		    		$("#doubleYn").val('N');
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	}
	
}
$("#img_file").change(function(e){
	var reader = new FileReader();

    reader.onload = function (e) {
        $("#img_photo").attr("src",e.target.result);
    };

    reader.readAsDataURL(this.files[0]);
});


function photo_preview(event){
//	document.getElementById('file_1').value = $("#file_1").val();
 //  alert(this.value);
	document.getElementById('fileName').value = $("#img_file").val();
}
function register(){
	var insert_yn = $("#insert_yn").val();
	if(insert_yn =='Y'){
		if($("#doubleYn").val()!='N'){
			alertOpen('중복체크를 하십시오.');
			return;
		}
	}
    var email = $("#email").val();
    var exptext = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
    if(exptext.test(email)==false){
		//이메일 형식이 알파벳+숫자@알파벳+숫자.알파벳+숫자 형식이 아닐경우			
		alertOpen('이메일 형식에 맞지 않습니다.');
		$("#email").focus();
		return false;
	}
	var formData = new FormData($('#frm')[0]);
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/user/insertUsrAjax.do";
	}else{
		url = contextPath+"/adm/user/modifyUsrAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    enctype: 'multipart/form-data',
	    processData:false,
	    contentType:false,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(insert_yn =='Y'){
		    		$("#id_user").val(e.map.id_user);
		    		$("#checkBtn").attr("disabled",true);
		    		$("#insert_yn").val('N');
	    		}
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function doList(){
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/user/userList.do";
	if(page!="")
		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
}
function closeModel(){
	if($('.doc-modal:visible').length>0){
		$('body').addClass('modal-open');
	}
}
</script>
</html>
