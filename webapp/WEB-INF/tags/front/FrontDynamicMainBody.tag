<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
<%@ attribute name="clazz" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ attribute name="ttlIcon" required="false" %>
<%@ attribute name="isDashboard" required="false" %>
<%@ attribute name="hasNotCommonPop" required="false" %>
<%@ attribute name="pageTitle" required="false" %>
<%@ attribute name="hasPop" required="false" %>
<%@ attribute name="isConfirm" required="false" %>
<%@ attribute name="currLocale" required="false" %>
<%@ attribute name="hasBrowser" required="false" %>
<%@ attribute name="hasHeader" required="false" %>
<%@ attribute name="hasImgTop" required="false" %>
<%@ attribute name="hasMenu" required="false" %>
<%@ attribute name="hasLeft" required="false" %>


<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<body <c:if test="${not empty clazz}">class="${clazz}"</c:if>>
    <div class="dimmed"></div>
	<div class="spinner" id="spinnerDiv" style="display:none;">
		<div class="in"><img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/></div>
	</div>
    <!-- //spinner  -->
    <div class="header-wrap">
        <c:if test="${not empty hasImgTop}">
        <front:FrontDynamicMainTop currLocale="${currLocale}"/>
        </c:if>
        <%--
        <c:if test="${not empty hasBrowser}">
        <front:FrontTopArea currLocale="${currLocale}" top_id_category="${top_id_category}" 
        	top_id_area="${top_id_area}"/>
        </c:if>
        <c:if test="${not empty hasLeft}">
        <front:FrontLeftArea currLocale="${currLocale}"  data_type="${type}"  top_id_category="${top_id_category}" 
        	top_id_area="${top_id_area}" id_category="${id_category}" id_nat_area="${id_nat_area}" id_city_area="${id_city_area}"/>
        </c:if>
        --%>
        <!--  mh-container -->
        
       <!-- // mh-footer -->
    </div>
	<jsp:doBody/>
	<div id="tooltip"></div>

	<!-- modal alert -->
	<div class="modal fade msg-modal" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="alertMsg">
					<p id="alertMsg">기존 사용자가 존재합니다.</p>
				</div>
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- modal info -->
	<div class="modal fade msg-modal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="infoMsg">
					<p>기존 사용자가 존재합니다.</p>
				</div>
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:closeModel();">닫기</button>
				</div>
			</div>
		</div>
	</div>
    
	<div class="modal fade msg-modal" id="progressModal" tabindex="-1" role="dialog" aria-labelledby="progressModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="processMsg">
					<p>Processing</p>
				</div>
			</div>
		</div>
	</div>
    
	<!-- modal confirm-->
	<div class="modal fade msg-modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content alert-modal">
				<div class="modal-body msg-modal-display-area" id="confirmMsg">
					<p>기존 사용자가 존재합니다.</p>
				</div>
				<input type="hidden" id="confirm_yn" value="N">
				<input type="hidden" id="confirm_func_nm">
				<div class="modal-footer msg-modal-btn-area">
					<button type="button" class="btn-primary" data-dismiss="modal" onclick="javascript:okModel('Y');">확인</button>
					<button type="button" class="btn-default" data-dismiss="modal" onclick="javascript:okModel('N');">취소</button>
				</div>
			</div>
		</div>
	</div>
    <!-- //modal type02  -->
    <!-- // mh-wrapper -->
    <!-- jQuery JS -->
    
    <!-- popper JS -->
    <script src="<c:url value='/resource/plugins/popper/popper.min.js'/>"></script>
    <!-- Bootstrap JS -->
    <script src="<c:url value='/resource/plugins/bootstrap-3.3.7-dist/js/bootstrap.js'/>"></script>
    <!-- Datetimepicker - Moment js -->
    <script src="<c:url value='/resource/plugins/moment/js/moment-with-locales.js'/>"></script>
    <!-- Datetimepicker - locale ko js -->
    <script src="<c:url value='/resource/plugins/moment/locale/ko.js'/>"></script>
    <!-- Datetimepicker JS -->
    <script src="<c:url value='/resource/plugins/bootstrap4-datetimepicker-master/js/bootstrap-datetimepicker.min.js'/>"></script>
    <script src="<c:url value='/resource/js/datetime.locale.js'/>"></script>
    <!-- Data Tables JS -->
    <script src="<c:url value='/resource/plugins/datatables/js/jquery.dataTables.min.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/full_numbers_no_ellipses.js'/>"></script>
    <script src="<c:url value='/resource/plugins/datatables/js/dataTables.responsive.min.js'/>"></script>
    <!-- nppa JS 
    <script src="<c:url value='/resource/js/ords.js'/>"></script>
    -->
    <script charset="UTF-8" src="<c:url value='/resource/js/comm_ords.js'/>"></script>

</body>
<script>
// $(document).ready(function() {
// 	var isDragging = false;
// 	var $customScrollbar = $(".custom-scrollbar");
// 	var $scrollThumb = $(".scroll-thumb");
// 	var $gridArea = $(".grid-area");
//     // Make .scroll-thumb draggable
//     $scrollThumb.draggable({
//         axis: "x",
//         containment: "parent",
//         drag: function(event, ui) {
//             isDragging = true; // Set the flag to indicate dragging
//             var handlePosition = ui.position.left;
//             var containerWidth = $customScrollbar.width();
//             var contentWidth = $gridArea.width();
//             var scrollLeft = (handlePosition / (containerWidth - $scrollThumb.width())) * (contentWidth - containerWidth);
//             $gridArea.scrollLeft(scrollLeft);
//         },
//         stop: function(event, ui) {
//             isDragging = false; // Reset the flag when dragging stops
//         }
//     });

//     // Handle click on .custom-scrollbar
//     $customScrollbar.on("mousedown", function(event) {
//         if (!isDragging) { // Check if not dragging
//         	if (event.target === this) { // Only if the user clicks on the scrollbar track, not the thumb
//         		var thumbPosition = event.clientX - $scrollThumb.offset().left;
//         	    updateGridAreaScroll(thumbPosition);
//         	}
//         }
//     });

//     function updateGridAreaScroll(scrollLeft) {
//         $gridArea.scrollLeft(scrollLeft);
//         var thumbPosition = (-scrollLeft / maxScroll) * (containerWidth - $scrollThumb.width());
//         $scrollThumb.css("left", thumbPosition);
//     }
// // 	$(".scroll-thumb").draggable({
// //         axis: "x",
// //         containment: "parent",
// //         drag: function(event, ui) {
// //             updateGridAreaScroll(ui.position.left);
// //         }
// //     });

//     // Function to update the grid_area's scroll position
//     function updateGridAreaScroll(thumbPosition) {
//         var containerWidth = $(".content").width();
//         var contentWidth = $(".grid-area")[0].scrollWidth; // Get the total scrollable width
//         var scrollLeft = (thumbPosition / ($(".custom-scrollbar").width() - $(".scroll-thumb").width())) * (contentWidth - containerWidth);
//         $gridArea.scrollLeft(scrollLeft);
//     }});
$(function () {
	$(".header-unit").click(function(e){
		e.preventDefault();
		var targetId = "h_";
		var element = $(this).find("[id^='" + targetId + "']");
		alert($(element).val());
		$("#sidx").val($(element).val());
	 	if($(this).find("span").hasClass("sorting_asc")){
			$("#sord").val("DESC");
		}else{
			$("#sord").val("ASC");
		}
	 	doList($("#page").val());
	});
    $(".grid-contents .cont-unit").mouseover(function(event) {
        // 말풍선 텍스트를 설정합니다.
//         var tooltipText = $(this).html().trim();
        // 말풍선의 위치를 마우스 위치로 설정합니다.
	 	var fullText = $(this).html().trim();
		console.log("tooltip:"+fullText);
		var elementWidth = $(this).width();
        // 요소 내용의 너비
        var contentWidth = $(this).prop("scrollWidth");

        // 너비 비교를 통해 줄임말 표시 여부 확인
        if (contentWidth > elementWidth) {              
             // 말풍선의 위치를 마우스 위치로 설정합니다.
             $("#tooltip").css({
                 top: event.clientY + 10, // 마우스 Y 좌표에 10을 더해 위로 조금 이동
                 left: event.clientX + 10 // 마우스 X 좌표에 10을 더해 오른쪽으로 조금 이동
             });

            // 말풍선에 텍스트를 설정하고 표시합니다.
             $("#tooltip").text(fullText).show();
        }
    });
    // .header-unit 요소에 마우스 아웃 이벤트를 추가합니다.
    $(".grid-contents .cont-unit").mouseout(function() {
        // 말풍선을 숨깁니다.
        $("#tooltip").hide();
    });
});
	function alertOpen(msg){
	  	$("#alertMsg").html(msg);
	  	$('#alertModal').modal('show');
	}
	function infoOpen(msg){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	}
	function infoOpen(msg,func){
	  	$("#infoMsg").html(msg);
	  	$('#infoModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function progressOpen(msg){
	  	$("#progressMsg").html(msg);
	  	$('#progress').modal('show');
	}
	function confirmOpen(msg,func){
	  	$("#confirmMsg").html(msg);
	  	$('#confirmModal').modal('show');
	  	$("#confirm_func_nm").val(func);
	}
	function callbackFunc(func){
	  	$("#confirm_func_nm").val(func);
	}
	function closeModel(){
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	function okModel(key){
		if(key=='Y'){
			$("#confirm_yn").val('Y');
			var funcNm = $("#confirm_func_nm").val();
			//eval(funcNm+"()");
			window[funcNm]();
		}
		else{
			$("#confirm_yn").val('N');
		}
		if($('.doc-modal:visible').length>0){
			$('body').addClass('modal-open');
		}
	}
	
	function processing(){
		$("#spinnerDiv").show();
	}
	function endProcessing(){
		$("#spinnerDiv").hide();
	}
</script>
