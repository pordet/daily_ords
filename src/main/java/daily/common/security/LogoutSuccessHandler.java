package daily.common.security;
/**
 * loginAjax 처리를 위해 AuthenticationSuccessHandler를 사용자 정의한 핸들러
 * author : P.H.
 * date : 2018-04-24
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import daily.common.security.util.RequestParamUtil;
import daily.common.util.StrUtil;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.domain.User;

public class LogoutSuccessHandler  extends SimpleUrlLogoutSuccessHandler {

//	private Logger log = LoggerFactory.getLogger(LogoutSuccessHandler.class);
//	@Autowired private SessionRegistry sessionRegistry;
	//@Autowired private PushWsLoginUsers pushWsLoginUsers;
	@Override
	public void onLogoutSuccess(HttpServletRequest req,
			HttpServletResponse resp, Authentication authentication)
			throws IOException, ServletException {
		String accept = req.getHeader("accept");
		HttpSession sess= req.getSession(false); 
		boolean isUser = false;
		String succ = GlobalConst.ACT_SUCCESS.toString(); 
//		Collection<? extends GrantedAuthority> aa = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		if(authentication != null){
			Collection<? extends GrantedAuthority> userAuths = authentication.getAuthorities();
			Iterator<? extends GrantedAuthority> it = userAuths.iterator();
			while(it.hasNext()) {
				GrantedAuthority auth = it.next();
				if(auth.getAuthority().equals(GlobalConst.USER_AUTH)) {
					isUser=true;
					break;
				}
			}
			System.out.println("#####:"+req.isUserInRole(GlobalConst.USER_AUTH));
			if( StrUtil.indexOf(accept, "html") > -1 ) { 
		        resp.setStatus(HttpServletResponse.SC_OK);
			} else if( StrUtil.indexOf(accept, "xml") > -1 ) { 
				resp.setContentType("application/xml"); 
				resp.setCharacterEncoding("utf-8"); 
				String data = StrUtil.join(new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<ret>", succ , "</ret>" }); 
				PrintWriter out = resp.getWriter(); out.print(data); out.flush(); out.close(); 
			} else if( StrUtil.indexOf(accept, "json") > -1 ) { 
				resp.setContentType("application/json"); 
				resp.setCharacterEncoding("utf-8"); 
				String data = StrUtil.join(new String[] {
						" { \"ret\" : ", succ, "} " }); 
				PrintWriter out = resp.getWriter(); 
				out.print(data); 
				out.flush(); 
				out.close();
			}
			
	//		if (authentication != null) {
	//			SecurityContextHolder.getContext().setAuthentication(null);
	//        }
		}
		if(isUser){
			String URL = req.getContextPath() + "/loginPage.do";
			resp.setStatus(HttpStatus.OK.value());
			resp.sendRedirect(URL);
		}else {
			String URL = req.getContextPath() + "/loginPage.do";
			resp.setStatus(HttpStatus.OK.value());
			resp.sendRedirect(URL);
		}
		
		if(authentication != null){
	        super.onLogoutSuccess(req, resp, authentication); 
	        LoginManager manager = LoginManager.getInstance();
	        User user = (User)authentication.getPrincipal();
	        String userId = user.getUsername();
	        if(manager.isUsing(userId)) {
	        	manager.removeSession(userId);
	        }
		}
//		if (sess != null && sess.getAttribute("method")!=null) {
//		    // 세션이 유효한 경우 속성을 가져옴
//		    Object attribute = sess.getAttribute("method");
//		    if (attribute != null) {
//				sess.setAttribute("method", req.getMethod());			
//		    } else {
//		        HttpSession ses = req.getSession(false);
//				ses.setAttribute("method", req.getMethod());			
//		    }
//		} else {
//			sess.setAttribute("method", req.getMethod());
//		}
//		if (sess != null && sess.getAttribute("postData")!=null) {
//		    // 세션이 유효한 경우 속성을 가져옴
//		    Object attribute = sess.getAttribute("postData");
//		    if (attribute == null) {
//		        HttpSession ses = req.getSession(false);
//		    	Map<String, Object> paramMap = RequestParamUtil.getParamMap(req);
//				sess.setAttribute("postData", paramMap);
//		    }
//		} else {
//	        HttpSession ses = req.getSession(false);
//	    	Map<String, Object> paramMap = RequestParamUtil.getParamMap(req);
//			sess.setAttribute("postData", paramMap);
//		}

	}
}
