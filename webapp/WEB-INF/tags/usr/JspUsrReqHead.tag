<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
      <div class="usr-request-header">
          <div class="login-usr-hd-logo">
              <img src="<c:url value='/resource/img/logo-login.png'/>" alt="NPPA CRS">
              <div class="login-title-txt">
                  <strong>NPPA<span>CRS</span></strong>
                  <span class="login-tit-notice"><spring:message code="login.usr.login.title1"/><br><em><spring:message code="login.usr.login.title2"/></em></span>
              </div>
              <div class="top-log">
<%--                <a href="<c:url value='/crsApplicant/myInfo.do'/>"><span>${alias}</span></a> --%>
                  <a href="<c:url value='/crsApplicant/myInfo.do'/>"><span>My Profile(${alias})</span></a><button type="button" class="request-logout" onclick="javascript:logout();">로그아웃</button>
                  <span><spring:message code="login.new.logout"/></span>
					<div class="form-group lang-inline-group">
					</div>
              </div>
          </div>
      </div>      <!--컨텐츠영역-->
<script>
	$(function () {
	   $('.js-action-a').click(function (e) {
			e.preventDefault();
		});
	});
   function logout(){
	   var form = document.createElement("form");
       form.setAttribute("charset", "UTF-8");
       form.setAttribute("method", "Post");  //Post 방식
       form.setAttribute("action", "<c:url value='/logout.do'/>"); //요청 보낼 주소
       document.body.appendChild(form);
       form.submit();
   }
   function changeLang(){
	   var lang = $("#multi_lang_sel option:selected").val();
	   var url = contextPath+"/crsUsr/changeLanguage.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : {lang:lang},
		    dataType: "json" ,
		    success : function(e){
		    	location.reload();
		    },
		    error : function(e){
		    	alert("fail");
		    }
		});
	   
   }

</script>
      