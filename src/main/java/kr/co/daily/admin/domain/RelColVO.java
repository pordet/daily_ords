package kr.co.daily.admin.domain;

import java.util.List;

public class RelColVO {
	private String id_meta_rel;
	private String id_rel_m;
	private String id_rel;
	private String id;
	private List<String> srcIdMetas;
	private List<String> trgIdMetas;
	public String getId_meta_rel() {
		return id_meta_rel;
	}
	public void setId_meta_rel(String id_meta_rel) {
		this.id_meta_rel = id_meta_rel;
	}
	public String getId_rel_m() {
		return id_rel_m;
	}
	public void setId_rel_m(String id_rel_m) {
		this.id_rel_m = id_rel_m;
	}
	public String getId_rel() {
		return id_rel;
	}
	public void setId_rel(String id_rel) {
		this.id_rel = id_rel;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getSrcIdMetas() {
		return srcIdMetas;
	}
	public void setSrcIdMetas(List<String> srcIdMetas) {
		this.srcIdMetas = srcIdMetas;
	}
	public List<String> getTrgIdMetas() {
		return trgIdMetas;
	}
	public void setTrgIdMetas(List<String> trgIdMetas) {
		this.trgIdMetas = trgIdMetas;
	}
}
