package kr.co.daily.front.common.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CustomMap;

@Repository
public interface CodeDAO {

	List<CustomMap> selectCodeByIdCodeGroup(Map<String, Object> map);

}
