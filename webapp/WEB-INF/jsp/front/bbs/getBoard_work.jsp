<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.BBS_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<c:set var="title" value="${bbs.bbs_nm}"/>
<front:FrontDynamicHead title="${title}">
<c:choose>
	<c:when test="${!is_guest}">
		<script>
			const dt_str = '${GlobalConst.DT_STR}';
			const cal_str = '${GlobalConst.DT_CALC}';
			const file_download_url = contextPath+'/adm/bbs/file_down.do';
			var dlt_1=${GlobalConst.DLT_1};
		</script>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/bbs/board_input.js'/>"></script>
	</c:when>
	<c:otherwise>
		<script>
			const file_download_url = contextPath+'/front/bbs/file_down.do';
		</script>
		<script charset="UTF-8" src="<c:url value='/resource/js/front/bbs/board_input.js'/>"></script>
	</c:otherwise>
</c:choose>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
			<div class="page-title-wrap">
			<c:choose>
				<c:when test="${empty param.id_meta_row}">
					<span id="curr_title">${title} 등록</span>
				</c:when>
				<c:otherwise>
					<span id="curr_title">${title} 수정</span>
				</c:otherwise>
			</c:choose>
			</div>
			
			<div class="page-content-wrap">
<!-- 				<form id="listFrm" name="listFrm" method="post"> -->
<%--                     <input type="hidden" name="search_col" value="${param.search_col}"> --%>
<%-- 				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}"> --%>
<%--                     <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}"> --%>
<%--                     <input type="hidden" name="id_meta_row" id="id_meta_row" value="${param.id_meta_row}"> --%>
<%--                     <input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"> --%>
<%-- 				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"> --%>
<%-- 					<input type="hidden" name="page" value="${param.page}"> --%>
<%-- 					<input type="hidden" name="sidx" value="${param.sidx}"/> --%>
<%-- 					<input type="hidden" name="sord" value="${param.sord}"/> --%>
<!-- 				</form> -->
				<form id="frm" name="frm" method="post">
                    <input type="hidden" name="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" value="${param.search_keyword}">
                    <input type="hidden" name="sys_code" id="sys_code" value="${param.sys_code}">
                    <input type="hidden" name="id_meta_row" id="id_meta_row" value="${param.id_meta_row}">
                    <input type="hidden" name="id_meta_m" value="${param.id_meta_m}">
				  	<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="page" value="${param.page}">
					<input type="hidden" name="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" value="${param.sord}"/>
					<input type="hidden" name="is_guest" value="${(is_guest? 'Y':'N')}"/>
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(meta_row eq null ? 'Y':'N')}">
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="20%">
								<col width="80%">
							</colgroup>
							<tbody>
                               	<c:forEach var="row" items="${dataList}" varStatus="i">
								<c:choose>
									<c:when test="${is_guest}">
 		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<span class="in_padd">${row.val}</span>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && (row.len_type eq 2 || row.len_type eq 3)}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<textarea data-meta-id="${row.id_meta}" style="width:100%;height:200px;" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" placeholder="${row.meta_nm}을 입력하세요." disabled>${row.val}</textarea>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<span class="in_padd">${row.val}</span>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<span class="in_padd">${row.val}</span>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_ATCH_FILE || row.typ eq GlobalConst.DT_MULTI_ATCH_FILE }">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<div id="file_btn_group">
															<c:if test="${not empty addFileList}">
		                            							<ul id="ul_group_${row.id_meta}" class="list-line">
										                            <c:forEach var="map" items="${addFileList}">
										                            <li>
																	<div class="file_down_div">
<%-- 																		<a href="javascript:void(0);" onclick="javascript:viewData(${map.id_board_file});">${map.file_nm}</a> --%>
																		<a href="<c:url value='/front/bbs/file_down.do'/>?id_board_file=${map.id_board_file}"><span>${map.file_nm}</span><img src="<c:url value='/resource/img/ico_download.png'/>" class="file_delete" alt="delete"/></a>
																	</div>
<%-- 																	<div class="search-result-text txt_post">${map.comment}</div> --%>
																	</li>
										                            </c:forEach>
																</ul>
															</c:if>
														</div>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<tr>
														<th colspan="2">${row.meta_nm}</th>
													</tr>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
														<span class="in_padd">${row.val}</span>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<span class="in_padd">${row.val}</span>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												</c:if>
											</c:when>
											<c:otherwise>
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<span class="in_padd">${row.val}</span>
													</td> 
												</tr>
											</c:otherwise>
		                                </c:choose>
		            
									</c:when>
									<c:otherwise>
 		                                <c:choose>
											<c:when test="${row.typ eq GlobalConst.DT_STR && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_STR && (row.len_type eq 2 || row.len_type eq 3)}">
												<tr>
													<th>${row.meta_nm}
<%-- 													[\\][${curr_low_num}] --%>
													</th>
													<td>
														<textarea data-meta-id="${row.id_meta}" style="width:100%;height:200px;" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" placeholder="${row.meta_nm}을 입력하세요.">${row.val}</textarea>
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_CALC && row.len_type eq 1}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" disabled placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_FLOAT || row.typ eq GlobalConst.DT_NUM}">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" onfocusout="checkNumberFormat(this,${row.decimal_len},${row.precision_len});" placeholder="${row.meta_nm}은 자동계산합니다.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_ATCH_FILE || row.typ eq GlobalConst.DT_MULTI_ATCH_FILE }">
												<tr>
													<th>${row.meta_nm}
													</th>
													<td id="file_div_${row.id_meta}">
		                            							<ul id="ul_group_${row.id_meta}">
															<c:if test="${not empty addFileList}">
										                            <c:forEach var="map" items="${addFileList}">
										                            <li id="atch_li_${row.id_meta}_${map.id_board_file}">
																	<div class="file_down_div">
																		<a href="<c:url value='/adm/bbs/file_down.do'/>?id_board_file=${map.id_board_file}"><span class="in_padd">${map.file_nm}</span><img src="<c:url value='/resource/img/ico_download.png'/>" class="file_delete" alt="delete"/></a>
																		<a href="javascript:void(0);" class="ico_delete" onClick="delAttach('${map.id_board_file}','${row.id_meta}')"><img src="<c:url value='/resource/img/ico_delete.png'/>" class="file_delete" alt="delete"/></a>
																	</div>
																	</li>
										                            </c:forEach>
															</c:if>
																</ul>
															<div id="file_group_${row.id_meta}_1">
							                                    <div class="attach-btn">
							                                      <input type="text" id="fileName_${row.id_meta}_1" data-add="1" class="attach1" style="width:300px;" disabled>
							                                      
							                                      <button type="button" class="btn attach1" onClick="javascript:openFilePicker('${row.id_meta}_1');"><span class="btn_attach"></span>첨부파일</button>
							                                      <input type="file" id="attach_file_${row.id_meta}_1" name="attach_file_${row.id_meta}_1" class="file_input_hidden" onchange="javascript: displayFileName('${row.id_meta}_1')"/>
							                                      <a href="javascript:void(0);" onClick="javascript:delFile('${row.id_meta}_1');"><img src="<c:url value='/resource/img/ico_delete.png'/>" class="file_delete" alt="delete"/></a>
							                                      <c:if test="${row.typ eq GlobalConst.DT_MULTI_ATCH_FILE}">
							                                      <button type="button" id="btn_add_file_${row.id_meta}"  class="btn attach1" onClick="javascript:addFile('${row.id_meta}_1');"><span class="btn_attach"></span>파일 추가</button>
							                                      </c:if>
							                                    </div>
							                            	</div>
													</td> 
												</tr>
											</c:when>
											<c:when test="${row.typ eq GlobalConst.DT_GRP_HEAD}">
												<c:if test="${row.meta_depth eq '1'}">
													<tr>
														<th colspan="2">${row.meta_nm}</th>
													</tr>
													<c:forEach var="child" items="${row.child_list}"  varStatus="i">
														<c:choose>
										                    <c:when test="${i.count % 2 == 1}">
										                        <tr>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                    </c:when>
										                    <c:otherwise>
										                        	<th>${child.meta_nm}</th>
										                        	<td>
																		<input type="text" data-meta-id="${child.id_meta}" data-meta-val="" id="id_meta_${child.id_meta}" name="id_meta_${child.id_meta}" value="${child.val}" placeholder="${child.meta_nm}을 입력하세요.">
																		<c:if test="${not empty meta_row and not empty row.val}">
																			<input type="hidden" data-meta-id="${child.id_meta}" data-meta-val="" id="i_id_meta_${child.id_meta}" name="i_id_meta_${child.id_meta}" value="${child.val}">
																			<input type="hidden" data-meta-id="${child.id_data}" data-meta-val="" id="i_id_data_${child.id_meta}" name="i_id_data_${child.id_meta}" value="${child.id_data}">
																		</c:if>
										                        	</td>
										                        </tr>
										                    </c:otherwise>
										                </c:choose>
													</c:forEach>
												</c:if>
											</c:when>
											<c:otherwise>
												<tr>
													<th>${row.meta_nm}
													</th>
													<td>
														<input type="text" data-meta-id="${row.id_meta}" data-meta-val="" id="id_meta_${row.id_meta}" name="id_meta_${row.id_meta}" value="${row.val}" placeholder="${row.meta_nm}을 입력하세요.">
														<c:if test="${not empty meta_row and not empty row.val}">
															<input type="hidden" data-meta-id="${row.id_meta}" data-meta-val="" id="i_id_meta_${row.id_meta}" name="i_id_meta_${row.id_meta}" value="${row.val}">
															<input type="hidden" data-meta-id="${row.id_data}" data-meta-val="" id="i_id_data_${row.id_meta}" name="i_id_data_${row.id_meta}" value="${row.id_data}">
														</c:if>
													</td> 
												</tr>
											</c:otherwise>
		                                </c:choose>
									</c:otherwise>
									</c:choose>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<c:if test="${!is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" id="btnInit">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete" onclick="javascript:delMeta();">
									삭제
								</button>
								<button type="button" class="btn-primary" onclick="javascript:register();">
									저장
								</button>
							</div>
						</c:if>
						<c:if test="${is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
						</c:if>
					</div>
				</form>
			</div>
	<script>
	$(function () {
		$("#btnInit").click(function(){
			initForm();
			$("#insert_yn").val('Y');
			$("#id_meta_row").val('');
 		});
	});
	function initForm(){
		$("[id^=ul_group_]").remove();
		$("[id^=file_group_]").remove();
		$("[id^=i_id_meta_]").remove();
		$("[id^=i_id_data_]").remove();
		
       	<c:forEach var="row" items="${dataList}" varStatus="i">
		$("#id_meta_${row.id_meta}").val('');
		<c:if test="${row.typ eq GlobalConst.DT_ATCH_FILE || row.typ eq GlobalConst.DT_MULTI_ATCH_FILE }">
		addFile("${row.id_meta}_1");		
		</c:if>
       	</c:forEach>
		$("textarea").val('');
	}
	function doList(){
		var frm =document.frm;
		<c:choose>
		<c:when test="${is_guest && empty param.from_home}">
			<c:set var="link" value="/front/bbs/boardList.do"/>
		</c:when>
		<c:when test="${!is_guest && empty param.from_home}">
			<c:set var="link" value="/adm/bbs/boardList.do"/>
		</c:when>
		<c:otherwise>
			<c:set var="link" value="/main.do"/>
		</c:otherwise>
		</c:choose>
			frm.action=contextPath+'${link}';
		processing();
		frm.submit();
	}
	function delAttach(id,id_meta){
		var atch_id = "atch_li_"+id_meta;
		$("#"+atch_id+"_"+id).remove();
	    var fileCnt = $("[id^="+atch_id+"_]").length;
	    if(fileCnt==0){
	    	$("#"+"ul_group_"+id_meta).remove();
	    }
		var html = '<input type="hidden" name="atch_del_id" id="atch_del_'+id+'" value="'+id+'">';
		$("#frm").append(html);
	}
	</script>

</front:FrontDynamicBody>
</html>