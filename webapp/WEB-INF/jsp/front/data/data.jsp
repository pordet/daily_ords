<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="data" tagdir="/WEB-INF/tags/data" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<data:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
<c:choose>
	<c:when test="${!is_guest}">
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_draw.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_register.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/data_input.js'/>"></script>
	</c:when>
	<c:otherwise>
		<script charset="UTF-8" src="<c:url value='/resource/js/data/guest_data_input.js'/>"></script>
	</c:otherwise>
</c:choose>
</data:DataHead>
<data:DataAccordionBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:if test='${param.id_master eq 1}'>
					<span>공지사항</span>
				</c:if>
				<c:if test='${param.id_master eq 2}'>
					<span>글로벌 교통 연구보고서</span>
				</c:if>
			</div>
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="get" onsubmit="return false;">
					<input type="hidden" name="attach_yn" value="${master.attach_yn}">
					<input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
					<input type="hidden" name="id_master" value="${master.id_master}">
					<input type="hidden" name="id_board_master" id="id_board_master" value="${master.id_board_master}">
					<input type="hidden" name="id_parent_board" id="id_parent_board">
					<input type="hidden" name="id_board" id="id_board">
					<input type="hidden" name="ord_str" value="${master.ord_str}">
					<input type="hidden" name="is_insert" id="is_insert" value="${(empty param.is_insert?'':param.is_insert)}">
					<input type="hidden" name="id_kind" id="id_kind" value="${(empty param.id_kind?'':param.id_kind)}">
					<input type="hidden" name="is_more" id="is_more" value="${(empty paramMap.is_more?'N':paramMap.is_more)}">
					<input type="hidden" name="more_page_rows" id="more_page_rows" value="${paramMap.more_page_rows}">
					<div class="search-group-area">
						<div class="search-input-group">
							<div class="search-input-front">
								<input type="text" id="search_keyword" name="search_keyword" value="${(not empty param.search_keyword ?param.search_keyword:'')}" onkeyup="enterKey();">
								<button type="button" class="btn-primary" title="검색" onclick="javascript:search();">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</div>
							<div class="search-input-rear">
								<c:if test="${!is_guest}">
									<button type="button" class="btn-default" onclick="javascript:registerData();">
										등록
									</button>
								</c:if>
							</div>
						</div>
					</div>
				
					<div class="search-state-area" id="result_header" style="display:${(total_cnt gt 0?'block':'none')}">
						<span>
							<c:choose>
								<c:when test="${not empty param.search_keyword}">
									<span id="searchKeywordSpan">&quot;${param.search_keyword}&quot;</span>에 대해 총 <strong class="result totalCntArea" id="mainTotalCnt">${total_cnt}</strong>건이 검색되었습니다.
								</c:when>
								<c:otherwise>
									전체에서 총 <strong class="totalCntArea" id="mainTotalCnt">${total_cnt}</strong>건이 검색되었습니다.
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				
					<div>
						<div class="tab-btn-area with-option tabBtnArea">
							<ul>
								<li class="${(empty param.id_kind or param.id_kind eq '' ?'active':'')}" >
									<a class="tabBtn" href="#tab_layer_a" onclick="javascript:showList('');">
										<span id="mainTotalCnt">전체 (${total_cnt})</span>
									</a>
								</li>
								<c:forEach var="map" items="${kindList}">
									<li ${(param.id_kind eq map.id_kind ?' class="active"':'')}>
										<a class="tabBtn" href="#tab_layer_${map.id_kind}" onclick="javascript:showList('${map.id_kind}');">
											<span id="main_${map.id_kind}_cnt">${map.kind_nm} (${map.kind_cnt})</span>
										</a>
									</li>
								</c:forEach>
							</ul>
						</div>
						
						<div class="search-option-area">
							<select title="정렬기준" name="ord" id="ord" onchange="javascript:search();">
								<option value="11" ${(param.ord eq '11'?' selected':'')}>등록일자순</option>
								<option value="10" ${(param.ord eq '10'?' selected':'')}>조회순</option>
							</select>
							<c:choose>
								<c:when test="${empty paramMap.id_kind || paramMap.id_kind eq ''}">
									<select title="행수" name="rows" id="row_length" onchange="javascript:searchByCombo();">
										<option value="5" ${(param.rows eq '5'?' selected':'')}>5개씩</option>
									</select>
								</c:when>
								<c:otherwise>
									<select title="행수" name="page_rows" id="page_rows" onchange="javascript:searchByComboPage(this);">
										<option value="5" ${(paramMap.more_page_rows eq '5'?' selected':'')}>5개씩</option>
										<option value="10" ${(empty paramMap.more_page_rows || paramMap.more_page_rows eq '10'?' selected':'')}>10개씩</option>
										<option value="20" ${(paramMap.more_page_rows eq '20'?' selected':'')}>20개씩</option>
									</select>
								</c:otherwise>
							</c:choose>
						</div>
					
						<div class="tab-content-area tabContentArea">
							<div id="tab_layer_a" class="tab-content tabContent ${(empty param.id_kind or param.id_kind eq '' ?'active':'')}">
								<c:forEach var="map" items="${kindList}">
									<div class="list-wrap" style="display:${(map.total_cnt gt 0?'block':'none')}">
										<div class="list-title">
											<span>${map.kind_nm}(<fmt:formatNumber value="${map.kind_cnt}" pattern="#,###" />건)</span>
											<c:if test="${paramMap.is_more eq 'N'}">
												<div class="list-btn-area">
													<button type="button" class="btn-blank" onclick="showMoreKind(${map.id_kind})">더보기 <i class="fa fa-angle-down"></i></button>
												</div>
											</c:if>
										</div>
										<div class="list-line">
											<c:choose>
												<c:when test="${map.kind_cnt gt 0}">
													<ul id="ul_a_${map.id_kind}">
														<c:forEach var="item" items="${list}">
															<c:if test="${map.id_kind eq item.id_kind}">
																<li>
																	<div class="tag-area">
																		<div class="label-set brown">
																			<span>${item.parent_board_nm}</span>
																		</div>
																		<div class="label-set red">
																			<span>${item.board_nm}</span>
																		</div>
																	</div>
																	<dl class="list-content-area">
																		<dt> 
																			<c:forEach var="file" items="${item.filelist}">
																				<a class="file-icon" href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}">
																					<span class="${file.file_sh_ext}">${file.file_ext}</span>
																				</a>
																			</c:forEach>
																			<a href="javascript:void(0);" onclick="javascript:viewDataDetail(${item.id_board_master},${item.id_board},'${item.ord_str}');">
																				<span>${item.title}</span>
																			</a>
																		</dt>
																		<dd>${item.comment}</dd>
																	</dl>
																	<div class="info-data">
																		<div>
																			<span>수정일 </span>
																			<span>${item.reg_date}</span>
																		</div>
																		<div>
																			<span>조회수 </span>
																			<span>${item.view_cnt}</span>
																		</div>
																	</div>
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</c:when>
												<c:otherwise>
													<ul>
														<li>
															<div class="no-list">
																<span>검색결과가 없습니다.</span>
															</div>
														</li>
													</ul>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</c:forEach>
							</div>
							
							<c:forEach var="map" items="${kindList}">
								<div id="tab_layer_${map.id_kind}" class="tab-content tabContent ${(param.id_kind eq map.id_kind ?'active':'')}">
									<div class="list-wrap"  style="display:block;">
										<div class="list-title">
											<span class="">${map.kind_nm}(<fmt:formatNumber value="${map.kind_cnt}" pattern="#,###" />건)</span>
										</div>
										<div class="list-line">
											<c:choose>
												<c:when test="${map.kind_cnt gt 0}">
													<ul id="ul_a_${map.id_kind}">
														<c:forEach var="item" items="${list}">
															<c:if test="${map.id_kind eq item.id_kind}">
																<li>
																	<div class="listItem">
																		<div class="thumbArea"></div>
																		<div class="list-content-wrap">
																			<div class="tag-area">
																				<div class="label-set brown">
																					<span>${item.parent_board_nm}</span>
																				</div>
																				<div class="label-set red">
																					<span>${item.board_nm}</span>
																				</div>
																			</div>
																			<dl class="list-content-area">
																				<dt> 
																					<c:forEach var="file" items="${item.filelist}">
																						<a class="file-icon" href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}">
																							<span class="${file.file_sh_ext}">${file.file_ext}</span>
																						</a>
																					</c:forEach>
																					<a href="javascript:void(0);" onclick="javascript:viewDataDetail(${item.id_board_master},${item.id_board},'${item.ord_str}');">
																						<span class="title">${item.title}</span>
																					</a>
																				</dt>
																				<dd class="commentArea">${item.comment}</dd>
																			</dl>
																			<div class="info-data">
																				<div>
																					<span>수정일 </span>
																					<span>${item.reg_date}</span>
																				</div>
																				<div>
																					<span>조회수 </span>
																					<span>${item.view_cnt}</span>
																				</div>
																			</div>
																		</div>
																	</div>
																	
																</li>
															</c:if>
														</c:forEach>
													</ul>
												</c:when>
												<c:otherwise>
													<ul id="ul_a_${map.id_kind}">
														<li>
															<div class="no-list">
																<span class="txt">검색결과가 없습니다.</span>
															</div>
														</li>
													</ul>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</c:forEach>
							
						</div>
					
						
					</div>
					<c:if test="${not empty paginator}">
						<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
						<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
						<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
						<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
						<input type="hidden" name="page" id="page" value="${param.page}"/>
					</c:if>
				</form>
			
			</div>
		</div>
	</div>
</data:DataAccordionBody>
<script>
var listItem = document.querySelectorAll(".listItem");
for(var i=0; i<listItem.length; i++) {
	var boardItemArea = listItem[i];
	var thumbArea = boardItemArea.querySelector(".thumbArea");
	var videoElement = boardItemArea.querySelectorAll("[name=player]");
	if(videoElement.length > 0) {
		var firstVideo = videoElement[0];
		
		var videoString = firstVideo.innerHTML;
		var videoUrlArr = videoString.split('/');
		var videoUrl = videoUrlArr[videoUrlArr.length -1];
		var thumbUrl = "https://img.youtube.com/vi/"+videoUrl+"/mqdefault.jpg";
		var newThumb = document.createElement("img");
		newThumb.setAttribute("src", thumbUrl);
		newThumb.setAttribute("z-Index", "10");
		
		thumbArea.style.marginRight = "20px";
		thumbArea.appendChild(newThumb);
	}
}

function enterKey(){
	if(window.event.keyCode == 13){
		search();
	}
}
$(".tabBtn").on('click', function(e){
	console.log()
	e.preventDefault();
	var targetId = $(this).attr('href');
	var target = $("" + targetId);
	$(this).closest('li').addClass('active').siblings('li').removeClass('active');
	
	if(target.length > 0){
		$(""+targetId).closest('.tabContentArea').find('.tabContent').removeClass('active');
		$(""+targetId).addClass('active');
	}
});
</script>
</html>