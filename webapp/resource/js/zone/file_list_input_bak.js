function openInput(){
	setInputTitle('I');	
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_year").val($("#search_year").val());
	$("#previewModal").modal('show');	
}

function setInputTitle(type){
	var title = "";
	
	if(nanToStr($("#id_category").val())!=''){
		title +=$("#id_category option:selected").text()+' ';
	}
	if(nanToStr($("#id_nat_area").val())!=''){
		title +=$("#id_nat_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_city_area").val())!=''){
		title +=$("#id_city_area option:selected").text()+' ';
	}
	if(nanToStr($("#id_meta").val())!=''){
		title +=$("#id_meta option:selected").text()+' ';
	}
	if(type=='I'){
		title+='데이터 등록/수정';
	}else{
		title+='데이터 수정';
	}
	$("#data_input_pop_title").html(title);
    
}

function openAjaxInput(){
	setInputTitle('I');	
	var id_meta = nanToStr($("#id_meta").val());
	var id_category = $("#id_category").val();
	
	if($("#id_city_area").val()!=''){
		$("#reg_id_city_area").val($("#id_city_area").val());
		$("#reg_id_nat_area").val($("#id_nat_area").val());
		if($("#id_meta").val()!=''){
			$("#reg_id_meta").val($("#id_meta").val());
		}
		
	}else if($("#id_nat_area").val()==''){
		alertOpen('도시, 국가를 선택하십시오.');
		return;
	}else if($("#id_meta").val()==''){
		alertOpen('세부항목를 선택하십시오.');
		return;
	}else{
		$("#reg_id_nat_area").val($("#id_nat_area").val());
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}else{
		$("#reg_id_category").val($("#id_category").val());
	}
	$("#reg_id_zone_area").val($("#id_zone").val());
	$("#reg_year").val($("#search_year").val());
	var params = $("#reg_frm").serialize();
	var url = contextPath+"/getNetworkAjax.do";
	var sel_year=$("#search_year").val();
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
    			var html="";
	    		if(e.rowList.length!=0){
	    			var rsize = e.rowList.length;
	    			$.each(e.rowList,function(i,d){
		    			if(i==0){
		    				html+='<tr>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">순서</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <a href="javascript:void(0);" onClick="javascript:down_order();"><img src="'+contextPath+'/resource/img/down_black.png" alt="추가" class="ico_plus"/></a>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="40%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" id="fileName'+d.ord+'" data-add="1" class="form-control mh-form-control" disabled value="'+d.file_nm+'">';
		    				html+='				  <div class="attach-btn">';
		    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
		    				html+='				  	<input type="file" name="file_'+d.ord+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+d.ord+'\').value = this.value"/>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="26%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" name="file_desc_'+d.ord+'" data-add="'+d.ord+'" class="form-control mh-form-control" disabled value="'+d.file_desc+'">';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="10%">';
	    				    html+='		  	   <div class="col-12">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group" style="margin-left:8px;">';
		    				html+='       	      <a href="'+d.file+'" download="'+d.file_nm+'">';
		    				html+='				  	<img src="'+contextPath+'/resource/img/ico_download.png" alt="추가" style="width:38px;height:20px;"/></a>';
		    				html+='               </a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
	    				    html+='		  	   <div class="col-12" style="margin-left:13px;">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
		    				html+='       	      <div class="tit_cont">삭제</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete" class="ico_delete" /></a>';
		    				html+='               </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='</tr>';	    				
		    			}else{
		    				html+='<tr>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">순서</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <a href="javascript:void(0);" onClick="javascript:down_order();"><img src="'+contextPath+'/resource/img/down_black.png" alt="추가" class="ico_plus"/></a>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="40%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" id="fileName_'+d.ord+'" data-add="1" class="form-control mh-form-control" disabled value="'+d.file_nm+'">';
		    				html+='				  <div class="attach-btn">';
		    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
		    				html+='				  	<input type="file" name="file_'+d.ord+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+d.ord+'\').value = this.value"/>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="26%">';
		    				html+='		   <div class="col-12">';
	    				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
		    				html+='       	   <div class="form-group inline-group">';
		    				html+='				  <input type="text" name="file_desc_'+d.ord+'" data-add="'+d.ord+'" class="form-control mh-form-control" disabled value="'+d.file_desc+'">';
		    				html+='            </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="10%">';
	    				    html+='		  	   <div class="col-12">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group" style="margin-left:8px;">';
		    				html+='       	      <a href="'+d.file+'" download="'+d.file_nm+'">';
		    				html+='				  	<img src="'+contextPath+'/resource/img/ico_download.png" alt="추가" style="width:38px;height:20px;"/></a>';
		    				html+='               </a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
	    				    html+='		  	   <div class="col-12" style="margin-left:13px;">';
		    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
		    				html+='               </div>';
		    				html+='            </div>';
		    				html+='	   </td>';
		    				html+='	   <td width="8%">';
		    				html+='		   <div class="col-12">';
		    				html+='       	      <div class="tit_cont">삭제</div>';
		    				html+='       	      <div class="form-group">';
		    				html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+d.ord+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete" class="ico_delete" /></a>';
		    				html+='               </div>';
		    				html+='        </div>';
		    				html+='	   </td>';
		    				html+='</tr>';
				    	}
	    			});
	    			
    				html+='<tr>';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">순서</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <a href="javascript:void(0);" onClick="javascript:down_order();"><img src="'+contextPath+'/resource/img/down_black.png" alt="추가" class="ico_plus"/></a>';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="40%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" id="fileName'+(rsize+1)+'" data-add="'+(rsize+1)+'" class="form-control mh-form-control" disabled>';
    				html+='				  <div class="attach-btn">';
    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
    				html+='				  	<input type="file" name="file_nm_'+(rsize+1)+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+(rsize+1)+'\').value = this.value"/>';
    				html+='               </div>';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="26%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" id="file_desc_'+(rsize+1)+'" data-add="1" class="form-control mh-form-control">';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="col-12" style="margin-left:13px;">';
    				html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
    				html+='       	      <div class="form-group">';
    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+(rsize+1)+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
    				html+='               </div>';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
    				html+='       	      <div class="tit_cont">삭제</div>';
    				html+='       	      <div class="form-group">';
    				html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+(rsize+1)+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete" class="ico_delete" /></a>';
    				html+='               </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='</tr>';
    				html+='';
    				html+='';
    				html+='';
    				html+='';
    				html+='';
	    				

	    			$("#file_list_tab").empty();
	    			$("#file_list_tab").html(html);
	    			$("#previewModal").modal('show');	
	    		}else{
	    			
    				html+='<tr id="addFile_in_div_1">';
    				html+='	   <td width="8%">';
    				html+='		   <div class="col-12">';
					html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="40%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" id="fileName_1" data-add="1" class="form-control mh-form-control" disabled>';
    				html+='				  <div class="attach-btn">';
    				html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
    				html+='				  	<input type="file" name="file_nm_1" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName_1\').value = this.value"/>';
    				html+='               </div>';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="26%">';
    				html+='		   <div class="col-12">';
				    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
    				html+='       	   <div class="form-group inline-group">';
    				html+='				  <input type="text" name="file_desc_1" data-add="1" class="form-control mh-form-control">';
    				html+='            </div>';
    				html+='        </div>';
    				html+='	   </td>';
    				html+='	   <td width="8%">';
				    html+='		  <div class="col-12" style="margin-left:13px;">';
				    html+='       	  <div class="tit_cont" style="margin-top:2px;">추가</div>';
    				html+='       	  <div class="form-group">';
    				html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile(1);"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
    				html+='           </div>';
    				html+='       </div>';
    				html+='	   </td>';
    				html+='</tr>';	    
    				
	    			$("#file_list_tab").empty();
	    			$("#file_list_tab").html(html);
	    			$("#previewModal").modal('show');	
	    		}
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
let arr = [1];
function addFile(idx){
	var bef_file_num = idx;
    var cur_file_num = idx + 1;
    arr.push(cur_file_num);
	var html ='';
	html+='<tr id="addFile_in_div_'+cur_file_num+'">';
	html+='	   <td width="8%">';
	html+='		   <div class="col-12">';
    html+='		  	   <div class="tit_cont">순서</div>';
	html+='       	   <div class="form-group inline-group">';
	html+='				  <img src="'+contextPath+'/resource/img/updown_black.png" alt="추가" class="ico_plus" usemap="#trMap'+cur_file_num+'"/>';
	html+='					<map name="trMap" id="trMap'+cur_file_num+'">';
	html+='						<area shape="rect" coords="0,0,25,50" onclick="javascript:up_order();" onmouseover="document.body.style.cursor=\'pointer\';" onmouseout="document.body.style.cursor=\'default\';"/>';
	html+='                    	<area shape="rect" coords="25,0,50,50" onclick="javascript:down_order();" onmouseover="document.body.style.cursor=\'pointer\';" onmouseout="document.body.style.cursor=\'default\';"/>';
	html+='                 </map>';
	html+='            </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="40%">';
	html+='		   <div class="col-12">';
    html+='		  	   <div class="tit_cont">네트워크 파일</div>';
	html+='       	   <div class="form-group inline-group">';
	html+='				  <input type="text" id="fileName_'+cur_file_num+'" data-add="1" class="form-control mh-form-control" disabled>';
	html+='				  <div class="attach-btn">';
	html+='				  	<button type="button" class="btn attach"><span class="btn_attach"></span>첨부</button>';
	html+='				  	<input type="file" name="file_nm_'+cur_file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName_'+cur_file_num+'\').value = this.value"/>';
	html+='               </div>';
	html+='            </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="26%">';
	html+='		   <div class="col-12">';
    html+='		  	   <div class="tit_cont">네트워크 설명</div>';
	html+='       	   <div class="form-group inline-group">';
	html+='				  <input type="text" name="file_desc_'+cur_file_num+'" data-add="'+cur_file_num+'" class="form-control mh-form-control">';
	html+='            </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="8%">';
	html+='		   <div class="col-12">';
	html+='       	      <div class="tit_cont" style="margin-top:2px;">추가</div>';
	html+='       	      <div class="form-group">';
	html+='				  	<a href="javascript:void(0);" onClick="javascript:addFile('+cur_file_num+');"><img src="'+contextPath+'/resource/img/ico_plus.png" alt="추가" class="ico_plus"/></a>';
	html+='               </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='	   <td width="8%">';
	html+='		   <div class="col-12">';
	html+='       	      <div class="tit_cont">삭제</div>';
	html+='       	      <div class="form-group">';
	html+='				  	<a href="javascript:void(0);" onClick="javascript:delFile('+cur_file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="삭제" class="ico_delete"/></a>';
	html+='               </div>';
	html+='        </div>';
	html+='	   </td>';
	html+='</tr>';	    				
	var fileCnt = $("[id^=addFile_in_div_]").length;
	$("#addFile_in_div_"+bef_file_num).after(html);
}
function delFile(idx){
	const ix = arr.indexOf(idx);
	if (ix > -1) arr.splice(ix,1);
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==1){
		alertOpen('적어도 하나의 행은 있어야 합니다.')
	}
    $("#addFile_in_div_"+idx).remove();
}

