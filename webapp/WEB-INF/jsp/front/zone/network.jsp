<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="isAdmin" value="N"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<front:FrontHead title="">
	<script src="<c:url value='/resource/plugins/rwdImageMap/jquery.rwdImageMaps.js'/>"></script>
	<script src="<c:url value='/resource/plugins/echarts/echarts.min.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/content_change.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/zone/file_list_register.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/zone/file_list_input.js'/>"></script>

	<script charset="UTF-8" src="<c:url value='/resource/navi_map/graph_drawer.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/navi_map/d3/d3.min.js'/>"></script>
</front:FrontHead>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>글로벌 DB 콘텐츠</span>
			</div>
			
			<div class="page-content-wrap">
				<div class="tab-btn-area tabBtnArea">
					<ul>
						<li class="active">
							<a class="tabBtn" href="#tabContent1">
								<span>검색</span>
							</a>
						</li>
						<li class="">
							<a class="tabBtn" href="#tabContent2">
								<span>데이터맵</span>
							</a>
						</li>
					</ul>
				</div>
				
				<div class="map-tab-content-area tabContentArea">
					<div id="tabContent1" class="map-tab-content tabContent active">
						<form id="frm" name="frm" method="get">
							<input type="hidden" name="type" value="3">
							<input type="hidden" name="bef_id_category" value="${param.id_category}">
							<input type="hidden" name="bef_id_nat_area" value="${param.id_nat_area}">
							<input type="hidden" name="bef_id_city_area" value="${param.id_city_area}">
							<input type="hidden" name="map_cate" value="${param.map_cate}">
							
							<div class="search-group-area">
								<div class="search-group-wrap">
									<div class="search-group-front">
										<div>
											<div class="select-label">검색시작년도</div>
											<select title="검색 시작년도" name="st_year" id="st_year">
												<option value="">없음</option>
												<c:if test="${not empty searchBetween}">
													<c:forEach var="st_year" begin="${searchBetween.st_year}" end="${searchBetween.ed_year}">
														<option value="${st_year}" <c:if test="${(is_new eq 'Y'?searchBetween.st_year:(empty param.st_year?searchBetween.st_year:param.st_year))eq st_year}"> selected="selected"</c:if>>${st_year}</option>
													</c:forEach>
												</c:if>
											</select>
										</div>
										<div>
								    		<div class="select-label">검색종료년도</div>
											<select title="종료년도" name="ed_year" id="ed_year">
												<option value="">없음</option>
												<c:if test="${not empty searchBetween}">
													<c:forEach var="ed_year" begin="${searchBetween.st_year}" end="${searchBetween.ed_year}">
														<option value="${ed_year}" <c:if test="${(is_new eq 'Y'?searchBetween.ed_year:(empty param.ed_year?searchBetween.ed_year:param.ed_year)) eq ed_year}"> selected="selected"</c:if>>${ed_year}</option>
													</c:forEach>
												</c:if>
											</select>
										</div>
										<div>
											<div class="select-label">카테고리명</div>
											<select id="id_category" name="id_category" onchange="javascript:changeCategory(this);">
												<c:if test="${isAdmin eq 'Y'}">
													<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
												</c:if>
												<c:forEach var="map" items="${cateList}" varStatus="i">
													<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
												</c:forEach>
											</select>
										</div>
										<div>
											<div class="select-label">국가명</div>
											<select id="id_nat_area" name="id_nat_area" onchange="javascript:changeNation(this);">
												<c:if test="${isAdmin eq 'Y'}">
													<option value="" ${(empty param.id_nat_area ? 'selected':'')}>선택</option>
												</c:if>
												<c:set var="cur_nat_area" value="${(empty param.id_nat_area?firstNat.id_area:param.id_nat_area)}"/>
												<c:forEach var="map" items="${natList}" varStatus="i">
													<option value="${map.id_area}" ${(cur_nat_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
												</c:forEach>
											</select>
										</div>
										<div>
											<div class="select-label">도시명</div>
											<select id="id_city_area" name="id_city_area" onchange="javascript:changeCity(this);">
												<option value="" ${(empty param.id_city_area ? 'selected':'')}>선택</option>
												<c:forEach var="map" items="${cityList}" varStatus="i">
													<option value="${map.id_area}" ${(param.id_city_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
												</c:forEach>
											</select>
										</div>
										<div id="id_meta_select_div" style="display:${(not empty itemList?'':'none')}">
											<div class="select-label">세부항목</div>
											<select id="id_meta" name="id_meta">
												<option value="" ${((empty param.id_meta && param.id_category ne '4') ? 'selected':'')}>선택</option>
												<c:forEach var="map" items="${itemList}" varStatus="i">
													<option value="${map.id_meta}" ${(((param.id_meta eq map.id_meta) ||((param.id_category eq 4 && empty param.id_meta))) ? 'selected':'')}>${map.unit_nm}</option>
												</c:forEach>
											</select>
										</div>
										<button type="button" class="btn-primary" onclick="javascript:search();"><span class="btn_search" title="검색"></span> 검색</button>
										<!--
										<button type="button" class="btn-primary" onclick="javascript:goDataMap();" title="Datamap">Datamap</button>
										-->
									</div>
									<div class="search-group-rear">
										<c:if test="${!is_guest}">
											<button type="button" class="btn-default" onclick="javascript:openAjaxInput();">등록/수정</button>
										</c:if>
									</div>
								</div>
							</div>
						</form>
						<div class="dbBtnbox">
							<div class="l">
								<front:FrontNavi id_category="${param.id_category}" id_nat_area="${param.id_nat_area}" id_city_area="${param.id_city_area}" id_zone="${param.id_zone}"></front:FrontNavi>
							</div>
							<div class="network-tab-btn-group" id="network_btn_list">
								<c:if test="${fn:length(rowList)>0}">
									<c:forEach var="map" items="${rowList}" varStatus="i">
										<c:choose>
											<c:when test="${i.index == 0}">
												<div>
													<button type="button" class="btn-info active" onclick="show_map(${i.index});return false">${map.file_desc}</button>
													<input type="hidden" id="hdn_img_${i.index}" value="${map.file}">
												</div>
											</c:when>
											<c:otherwise>
												<div>
													<button type="button" class="btn-info" onclick="show_map(${i.index});return false">${map.file_desc}</button>
													<input type="hidden" id="hdn_img_${i.index}" value="${map.file}">
												</div>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
							</div>					
						</div>
						<div class="boardBox zone-map-wrap" id="network_img">
							<c:if test="${fn:length(rowList)>0}">
								<img src="${rowList[0].file}" id="netImg">
							</c:if>
						</div>
						
					</div>
					
					<div id="tabContent2" class="map-tab-content tabContent">
						<div class="search-group-area">
							<div class="search-group-wrap">
								<div class="search-group-front">
									<c:forEach var="cate" items="${cateList}">
										<div class="bar_button">
											<button class="map-tab-btn" id="category_btn_${cate.id_category}" title="${cate.code_nm}" onclick="startSearchOnMap(${cate.id_category});">
												${cate.code_nm}
											</button>
										</div>
									</c:forEach>
								</div>
							</div>
						</div>
						
						<div class="display_area">
							<div class="network_layer">
								<div class="spinner inner" id="graphSpinner" style="display:none;">
									<div class="in">
										<img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/>
									</div>
								</div>
								<div class="network_wrapper">
									<!-- 컨트롤바 영역 : 영역에 맞춤/확대/축소 -->
									<div class="network_control_bar_area">
										<button id="network_fit_button" title="화면에 맞춤">
											<span class="glyphicon glyphicon-resize-full"></span>
										</button>
										<button id="network_zoom_in" title="확대">
											<span class="glyphicon glyphicon-plus"></span>
										</button>
										<button id="network_zoom_out" title="축소">
											<span class="glyphicon glyphicon-minus"></span>
										</button>
									</div>
									
									<!-- graph영역 -->
									<div class="svg_container">
										<svg id="graph_area"></svg>
									</div>
									
								</div>
							</div>
							
							<div class="slide-doc-area on-map" id="slideDocument">
								<div class="slide-doc-layout">
									<div class="slide-doc-content-wrap">
										<div class="doc-header-area">
											<div id="mapDocHeaderTitle" class="doc-header-title"></div>
											<button class="closeButton popup-close"></button>
										</div>
										<div class="spinner on-slide" id="slideDocSpinner" style="display:none;">
											<div class="in">
												<img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/>
											</div>
										</div>
										<div class="doc-body-area">
											<div class="doc-body-title">
												<div class="map-doc-body-title-wrap">
													<span id="mapDocBodyTitle"></span>
												</div>
												<button class="btn-info" onclick="mapSlideChartShow()">분석</button>
											</div>
											<div class="map-doc-controller">
												<select id="mapDocStYear"></select>
												<div>
													<span> ~ </span>
												</div>
												<select id="mapDocEdYear"></select>
												<button class="btn-primary" onclick="mapSlideSearch()">
													<span class="glyphicon glyphicon-search"></span>
													조회
												</button>
											</div>
											
											<div class="doc-body-content">
												<div class="doc-body-content-wrap">
													<table class="" id="mapDocContent"></table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="slide-doc-area slide-graph" id="slideGraph">
								<div class="slide-doc-layout">
									<div class="slide-doc-content-wrap">
										<div class="doc-header-area">
											<div class="doc-header-title">데이터 분석</div>
											<button class="closeButton popup-close"></button>
										</div>
										<div class="spinner on-slide" id="slideGraphSpinner" style="display:none;">
											<div class="in">
												<img src="<c:url value='/resource/img/img_loading.gif'/>" alt="loading"/>
											</div>
										</div>
										<div class="doc-body-area">
											<div class="doc-body-content">
												<div class="doc-body-content-wrap">
													<div id="mapChartCon"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
    <div class="modal fade mh-modal" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModal" data-backdrop="static">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form id="reg_frm" name="reg_frm">
				<input type="hidden" id="reg_id_category" name="id_category">
				<input type="hidden" id="reg_id_nat_area" name="id_nat_area">
				<input type="hidden" id="reg_id_city_area" name="id_city_area">
				<input type="hidden" id="reg_id_meta" name="id_meta">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="data_input_pop_title">데이터입력</h3>
						<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<div class="modal-body-header">
							<div id="modalBodyTitle" class="modal-body-title"></div>
							<div class="modal-body-btn-group">
								<button type="button" class="btn-primary" onclick="javascript:addRow();">
									<span class="glyphicon glyphicon-plus"></span>
									추가
								</button>
							</div>
						</div>
						<div class="modal-input-area">
							<table class="file-list-table">
								<colgroup>
									<col width="40%">
									<col width="30%">
									<col width="30%">
								</colgroup>
								<tbody id="file_list_tab"></tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer" id="btnArea1">
						<button type="button" class="btn-primary" onclick="javascript:register();">저장</button>
					</div>
				</div>
			</form>

		</div>
	</div>
	
<script>
//공통부분 ==================================================================================//
//페이지 초기값 저장 --> 데이터 수정모달, 분석그래프 모달에 사용.
var iniParam = {
	zone : "",
	category : "",
	cateName : "",
	meta : "",
	metaName : "",
	cityArea : "",
	cityName : "",
	natArea : "",
	natName : "",
	startYear : "",
	endYear : "",
}
window.addEventListener('DOMContentLoaded', function() {
	var zone = document.querySelector("#id_zone");
	var category = document.querySelector("#id_category");
	var meta = document.querySelector("#id_meta");
	var cityArea = document.querySelector("#id_city_area");
	var natArea = document.querySelector("#id_nat_area");
	var startYear = document.querySelector("#st_year");
	var endYear = document.querySelector("#ed_year");

	var befCate = document.querySelector("input[name=bef_id_category]");
	var befNat = document.querySelector("input[name=bef_id_nat_area]");
	var befCity = document.querySelector("input[name=bef_id_city_area]");
	
	if(zone != null) {
		iniParam.zone = zone.value;
	}
	iniParam.category = category.value;
	iniParam.cateName = category.options[category.selectedIndex].innerText;
	iniParam.meta = meta.value;
	iniParam.metaName = meta.options[meta.selectedIndex].innerText;
	iniParam.cityArea = cityArea.value;
	iniParam.cityName = cityArea.options[cityArea.selectedIndex].innerText;
	iniParam.natArea = natArea.value;
	iniParam.natName = natArea.options[natArea.selectedIndex].innerText;
	iniParam.startYear = startYear.value;
	iniParam.endYear = endYear.value;
	
	befCate.value = category.value;
	befNat.value = natArea.value;
	befCity.value = cityArea.value;

	//탭기능
	$(".tabBtn").on('click', function(e){
		e.preventDefault();
		var targetId = $(this).attr('href');
		var target = $("" + targetId);
		$(this).closest('li').addClass('active').siblings('li').removeClass('active');
		
		if(target.length > 0){
			$(""+targetId).closest('.tabContentArea').find('.tabContent').removeClass('active');
			$(""+targetId).addClass('active');
		}
	});
	
	//맵 포커스...
	var mapCate = document.querySelector("input[name=map_cate]");
	if(mapCate.value == "" || mapCate.value == null || mapCate.value == undefined) {
		mapCate.value = Number(iniParam.category);
	}

	//맵 검색 작동...
	$(".closeButton").on("click", closeDocArea);
	startSearchOnMap(mapCate.value);
});

//슬라이드 문서 열기
function documentShow(e, element) {
	if(element.is(".on") === false) {
		element.addClass("on");
	}
}
//슬라이드 문서 닫기
function closeDocArea(e, element) {
	var thisTarget = element;
	if(element == null) {
		thisTarget = $(this).closest(".slide-doc-area");
	}
	if(thisTarget.is(".on") === true) {
		thisTarget.removeClass("on");
	}
}

/*데이터 맵 부분..*/
//1. 카테고리 선택
function startSearchOnMap(category) {
	//문서 초기화/숨기기
	closeDocArea(null, $("#slideGraph"));
	closeDocArea(null, $("#slideDocument"));
	
	//svg이하 내용 비우기
	$('#graph_area').empty();
	$('#graph_area').removeAttr("viewbox");
	
	//현재 카테고리 스타일 변경...
	var btns = $("[id^=category_btn_]");
	$.each(btns,function(i,d){
		$("#"+d.id).removeClass("active");
	})
	$("#category_btn_"+ category).addClass("active");
	
	//저장... --> 검색 탭 에서 검색 시 파라미터로 전송...
	var mapCate = document.querySelector("input[name=map_cate]");
	mapCate.value = category;
	
	requestToServer(category); //-> 현재페이지 url+keyword로 요청.

	//클릭이벤트 등록...
	$(".text.cate2,.text.cate3").on("click",function(){
		displayDataOnSlide($(this).attr("data-url"));
	});
}

//2. 데이터 요청
function requestToServer(_key_word) {
	$("#graphSpinner").css("display", "block");
	var _request_url = "onNetwork.do";
	//ajax통신.... 결과를 json으로 반환.
	var _ajax_result = "ajax failed";
	$.ajax({
		url : _request_url + "?keyword=" + _key_word,
		dataType : "json",
		//contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async : false,	// 동기식
		success : function(result) {
			_ajax_result = result;
			 if(typeof(_ajax_result) == 'object') {
				if(Object.keys(_ajax_result).length == 0) {
					var _message = "검색결과가 없습니다.";
					alertOpen(_message);
				} else if(_request_url == "onNetwork.do") {
					drawNetworkGraph(dataModifier(_ajax_result));
					$("#graphSpinner").css("display", "none");
				}
			} else {
				var _message = "검색에 실패하였습니다. 다시 시도해주세요.";
				alertOpen(_message);
			}
		}
	})
}

//3. 맵 내 요소 선택시 슬라이드에 표시할 데이터 요청
function displayDataOnSlide(id_area){
	var mapCate = document.querySelector("input[name=map_cate]");
	iniParam.mapArea = id_area;
	var params = {
		id_category : mapCate.value,
		id_area : id_area
	};
	var url = contextPath+"/contentAreaAjax.do";
	$("#slideDocSpinner").css("display", "block");
	documentShow(null, $("#slideDocument"));
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	} else {
	    		var currentArea = e.curr_area;
				var parentArea = currentArea.id_parent_area;
				if(parentArea == 0) {
					iniParam.mapNatArea = currentArea.id_area;
					iniParam.mapCityArea = ""
				} else {
					iniParam.mapNatArea = parentArea;
					iniParam.mapCityArea = currentArea.id_area;
				}

				iniParam.mapStart = e.searchBetween.st_year;
				iniParam.mapEnd = e.searchBetween.ed_year;
	    		
		    	drawSildeDocContent(e);
		    	$("#slideDocSpinner").css("display", "none");
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
	    		alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
	    		alertOpen('접근 권한이 없습니다.');
	    	}else{
	    		alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

//4. 슬라이드 내용 구성..
function drawSildeDocContent(data) {
	//슬라이드 문서 제목
	var slideTitle = data.category.code_nm + " 데이터";
	var mapDocHeaderTitle = document.querySelector("#mapDocHeaderTitle");
	mapDocHeaderTitle.innerHTML = slideTitle;
	//검색영역...
	var mapDocBodyTitle = document.querySelector("#mapDocBodyTitle");
	mapDocBodyTitle.innerHTML = data.curr_area.area_nm + " " + data.searchBetween.st_year + " ~ " + data.searchBetween.ed_year;

	var content = '';
	var start = '';
	var end = '';
	if(nanToStr(data.yd)==''){
		start = '';
		end = '';
	} else {
		start = Number(data.searchBetween.st_year);
		end = Number(data.searchBetween.ed_year);
	}
	var minYear = Number(data.yd.min_year);
	var maxYear = Number(data.yd.max_year);

	var startHtml='<option value="">없음</option>';
	var endHtml='<option value="">없음</option>';

	for(var year = minYear; year <= maxYear; year++) {
		if(year == start){
			startHtml += '<option value="'+year+'" selected>'+year+'</option>';
		} else {
			startHtml += '<option value="'+year+'">'+year+'</option>';
		}
		if(year == end){
			endHtml += '<option value="'+year+'" selected>'+year+'</option>';
		} else {
			endHtml += '<option value="'+year+'">'+year+'</option>';
		}
  }
	$("#mapDocStYear").html(startHtml);
	$("#mapDocEdYear").html(endHtml);

	if(data.rowList.length > 0 && data.totalList.length > 0){
		content += '<thead>';
		content += '	<tr>';
		content += '		<th class="" colspan="'+data.maxLvl+'">항목 </th>';
		for(var year = Number(start); year <= Number(end); year++){
			content += '	<th scope="col">'+year+'년</th>';
		}
		content += '	</tr>';
		content += '</thead>';
	}
	
	content += '<tbody>';
	if(data.rowList.length == 0 || data.totalList.length == 0){
		content += '<tr><td>검색결과없음</td></tr>';
	}
	if(data.rowList.length > 0 && data.totalList.length > 0){
		$.each(data.rowList,function(i,d){
			content += '<tr>';
			$.each(d.child,function(j,f){
				var colspan = 1;
				if(f.colspan !=''){
					colspan=f.colspan;
				}
				var rowspan = 1;
				if(nanToStr(f.rowspan) !=''){
					rowspan = nanToStr(f.rowspan);
				}
				if(nanToStr(f.head)!=''){
					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
						content += '<td class="row-header" colspan="'+colspan+'" rowspan="'+rowspan+'">';
						content += '	<input name="slideCheck" type="checkbox" value="'+f.id_meta+'" id="slideCheck_'+f.id_meta+'">';
						content += '	<span></span>';
						content += '	<label for="check_'+f.id_meta+'"></label>';
						content += 		f.unit_nm;
						content += '</td>';
					} else {
						content += '<td class="row-header" colspan="'+colspan+'" rowspan="'+rowspan+'">'+f.unit_nm+'</td>';
					}
				}
			});

			for(var year = Number(start); year <= Number(end); year++){
				var isEmpty = false;
				$.each(d.child, function(j,f){
					if(year == f.year) {
						isEmpty = true;
						if(f.data_type==3){
							if(nanToStr(f.addfilelist)!=''){
								content += '<td>';
								$.each(f.addfilelist, function(idx, d){
									content += '<a href="'+d.file+'" download="'+d.file_nm+'">';
									content += '	<span class="glyphicon glyphicon-download-alt"></span>';
									content += '</a>';
								})
								content += '</td>';
							}
						} else {
							content += '<td>'+f.val+'</td>';
						}
					}
				});
				if(isEmpty ==false){
					content += '<td></td>';
				}
			}
			content += '</tr>';
		});
	}
	content += '</tbody>';
	$("#mapDocContent").html(content);
}
//5. 슬라이드 문서 검색기능
function mapSlideSearch(){
	var mapCate = document.querySelector("input[name=map_cate]");
	var id_area = iniParam.mapArea;
	var startElement = document.querySelector("#mapDocStYear");
	var start = startElement.options[startElement.selectedIndex].value;
	iniParam.mapStart = start;
	var endElement = document.querySelector("#mapDocEdYear");
	var end = endElement.options[endElement.selectedIndex].value;
	iniParam.mapEnd = end;
	
	var params = {
		id_category : mapCate.value,
		id_area : id_area,
		st_year : start,
		ed_year : end
	};
	$("#slideDocSpinner").css("display", "block");
	var url = contextPath + "/contentAreaAjax.do";
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		drawSildeDocContent(e);
		    	$("#slideDocSpinner").css("display", "none");
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
	    		alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
	    		alertOpen('접근 권한이 없습니다.');
	    	}else{
	    		alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
//6. 슬라이드 그래프
function mapSlideChartShow(){
	var ids =  $("input[name=slideCheck]:checked");
	var list = [];
	var grChkCnt = 0;
	$.each(ids,function(){
		list.push(this.value);
		var rows = nanToStr($("#slideCheck_" + this.value).parent().attr("rowspan"));
		if(rows > 1) grChkCnt++;
	});
	if(grChkCnt>1){
		alertOpen('하위항목을 가진 항목은 하나 이상 선택할 수 없습니다.');
		return;
	}
	if(grChkCnt==1 && list.length>1){
		alertOpen('하위항목을 가진 항목과 단일항목을 동시에 선택할 수 없숩니다.');
		return;
	}
	if(nanToStr(list)=='' || list.length==0){
		alertOpen('차트에 사용할 항목을 선택하십시오.');
		return;
	}
	
	var ch_id_list ="";
	for(var i=0; i<list.length; i++){
		if(i == 0) {
			ch_id_list += list[i];
		} else {
			ch_id_list += "," + list[i];
		}
	}
	var mapCate = document.querySelector("input[name=map_cate]");
	
	var params = {
		id_category : ""+mapCate.value,
		id_nat_area : ""+iniParam.mapNatArea,
		id_city_area : ""+iniParam.mapCityArea,
		
		ch_id_list : ch_id_list,
		st_year : ""+iniParam.mapStart,
		ed_year : ""+iniParam.mapEnd,
	}
	$("#slideGraphSpinner").css("display", "block");
	documentShow(null, $("#slideGraph"));
	var url = contextPath + "/drawChartAjax.do";
	$("#mapChartCon").empty();
	var html='<div class="modal-chart-area" id="mapChartCont" style="width:720px;height:475px;"></div>';
	$("#mapChartCon").html(html);
	var myChart = echarts.init(document.getElementById("mapChartCont"));
	var seriesList = [];
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
		    	$.each(e.series,function(i,d){
		    		seriesList.push(
	                    {
	                    	name:d.name,
	                    	type:'line',
	                    	data:d.data
	                    }
	                )
		    	});
		    	myChart.setOption({
		    	    title: {
		    	        text: ' '
		    	    },
		    	    tooltip: {
		    	        trigger: 'axis',
		    	        axisPointer: {
		    	            type: 'shadow'
		    	        }
		    	    },
		    		legend: {
		                data: e.legend
		            },
		    	    grid: {
		    	        left: '3%',
		    	        right: '4%',
		    	        bottom: '3%',
		    	        containLabel: true
		    	    },
		    	    xAxis: {
		    	        type: 'category',
		    	        data: e.xAxis,
		    	        boundaryGap: false
		    	    },
		    	    yAxis: {
		    	        type: 'value',
		    	    },
		            series: seriesList
		            
		        });
		    	$("#slideGraphSpinner").css("display", "none");
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
	    		alertOpen('접근 권한이 없습니다.');
	    	}else{
	    		alertOpen('실패하였습니다.');
	    	}
	    }
	});
}

//end of 공통부분 =============================================================================//

function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/content.do";
   		$("#page").val(page);
    	$("#frm").submit();
}
function search(){
	doList(1);
}

function show_map(i){
	var thisBtn = event.target;
	
	//버튼 클래스 초기화
	var tabBtnList = document.querySelectorAll(".network-tab-btn-group > button");
	for(var k=0; k<tabBtnList.length; k++) {
		var item = tabBtnList[k];
		item.classList.remove("active");
	}
	
	//현재 버튼 클래스 추가
	thisBtn.classList.add("active");
	
	$("#netImg").attr("src",$("#hdn_img_"+i).val());
}
</script>
</front:FrontBody>
</html>