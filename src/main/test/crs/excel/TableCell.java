package crs.excel;

public class TableCell {
    private String id;
    private String parent_id;
    private int depth;
    private int ord;

    public TableCell(String id, String parent_id, int depth, int ord) {
        this.id = id;
        this.parent_id = parent_id;
        this.depth = depth;
        this.ord = ord;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getOrd() {
		return ord;
	}

	public void setOrd(int ord) {
		this.ord = ord;
	}
}
