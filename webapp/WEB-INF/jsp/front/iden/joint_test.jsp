<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="${map.meta_m_nm} 등록"/>
<front:FrontDynamicHead title="${title}">
    <script src="<c:url value='/resource/js/jointJs/lodash.min.js'/>"></script>
    <script src="<c:url value='/resource/js/jointJs/backbone-min.js'/>"></script>
    <script src="<c:url value='/resource/js/jointJs/joint.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resource/css/jointjs/joint.min.css'/>"/>
  <style>
    #container {
      display: flex;
    }
    #left {
      width: 30%;
      padding: 20px;
    }
    #right {
      width: 70%;
      padding: 20px;
    }
    #canvas {
      width: 100%;
      height: 100%;
      border: 1px solid #aaa;
    }
  </style>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
<div id="diagram"></div>

<script>
// 캔버스 생성
var graph = new joint.dia.Graph();

// 캔버스 렌더러 생성
var paper = new joint.dia.Paper({
    el: $('#diagram'),
    model: graph,
    width: 800,
    height: 600,
    gridSize: 10,
    drawGrid: true,
    background: {
        color: 'rgba(255, 255, 255, 0.5)'
    }
});

// 테이블 요소 생성 함수
function createTable(x, y, tableName, columns) {
    var table = new joint.shapes.standard.Rectangle({
        position: { x: x, y: y },
        size: { width: 160, height: 100 },
        attrs: {
            body: {
                fill: '#ffffff',
                stroke: '#000000',
                strokeWidth: 1,
                rx: 6,
                ry: 6
            },
            label: {
                text: tableName,
                fill: '#000000',
                fontSize: 12,
                fontWeight: 'bold',
                textWrap: { width: 140, height: 40, ellipsis: true }
            }
        }
    });

    // 테이블 컬럼 추가
    var offsetY = 20;
    _.each(columns, function(column) {
        var text = column.name + ' : ' + column.type;
        table.attr('.label/text', table.attr('.label/text') + '\n' + text);
    });

    return table;
}

// 테이블 생성
var table1 = createTable(100, 100, 'Table 1', [
    { name: 'id', type: 'INT' },
    { name: 'name', type: 'VARCHAR(50)' },
    { name: 'age', type: 'INT' }
]);

var table2 = createTable(400, 200, 'Table 2', [
    { name: 'id', type: 'INT' },
    { name: 'table1_id', type: 'INT' },
    { name: 'value', type: 'VARCHAR(100)' }
]);

// 테이블을 그래프에 추가
graph.addCells([table1, table2]);

// 테이블 간의 관계 생성
var link = new joint.dia.Link({
    source: { id: table1.id },
    target: { id: table2.id },
    attrs: {
        '.connection': { stroke: '#000000', 'stroke-width': 1 },
        '.marker-target': { d: 'M 10 0 L 0 5 L 10 10 z', fill: '#000000' }
    }
});

// 관계를 그래프에 추가
graph.addCell(link);

</script>
</front:FrontDynamicBody>
</html>