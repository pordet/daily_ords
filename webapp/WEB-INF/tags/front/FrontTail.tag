<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
                <div class="footer-content">
                    <span class="copyright-img">
<%--                     <img src="<c:url value='/resource/img/icon_gray.png'/>" alt=""/> --%>
                    <em>THE KOREA TRANSPORT INSTITUTE</em></span>
                    <span class="copyright-footer">Copyright &copy; 2019 2018. <br>THE KOREA TRANSPORT INSTITUTE<br>All Rights Reserved.</span>
                </div>
