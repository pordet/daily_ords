/*
function register(){
	var id_meta = nanToStr($("#id_meta").val());
	if(id_meta!=''){
		var formData = new FormData($('#reg_frm')[0]);
		var url = contextPath+"/adm/data/insertFileIncludeDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
				console.log("컨텐츠 화면 등록결과");
				console.log(e);
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
//			    			if(nanToStr(d.id_meta_data)!=''){
//				    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+nanToStr(d.val)+'"/>';
//				    			$("#m_"+d.year+"_"+d.id_meta).append(html);
//			    			}
			    			if(nanToStr(d.addfile)!=''){
				    				var file = d.addfile[0];
				    				var inHtml='';
					    				inHtml+='	<div class="col-6" id="fe_div_'+d.id_meta+'">';
					    				inHtml+='	  <div class="tit_cont">첨부된 파일</div>';
					    				inHtml+='   <div class="form-group">';
					    				inHtml+='         <input type="hidden" name="fi_'+d.year+'_'+d.id_meta_data+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
					    				inHtml+='         <a href="'+contextPath+'/file/download_add.do?id_data_file='+file.id_data_file+'">';
					    				inHtml+='         <img src="'+contextPath+'/resource/img/ico_download.png" alt="delete" style="width:28px;height:16px;"/>';
					    				inHtml+='         </a>';  
					    				inHtml+='   </div>';
					    				inHtml+='</div>';
					    				$("#file_div_"+d.id_meta).after(inHtml);
//				    			}else{
//				    				$("#fe_div_"+d.id_data_file).remove();
//				    				
//				    			}
			    			}
			    		});
		    		}
		    		if(e.updateFileList.length!=0){
			    		$.each(e.updateFileList,function(i,d){
			    			var html="";
			    			if(nanToStr(d.upfile)!=''){
				    				var file = d.upfile[0];
				    				var inHtml='';
					    				inHtml+='	<div class="tit_cont">첨부된 파일</div>';
					    				inHtml+='   <div class="form-group">';
					    				inHtml+='         <input type="hidden" name="fi_'+d.year+'_'+d.id_meta_data+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
					    				inHtml+='         <a href="'+file.file+'" download="'+file.file_nm+'">';
					    				inHtml+='         <img src="'+contextPath+'/resource/img/ico_download.png" alt="delete" style="width:28px;height:16px;"/>';
					    				inHtml+='         </a>';  
					    				inHtml+='   </div>';
					    				$("#fe_div_"+d.id_meta).html(inHtml);
//				    			}else{
//				    				$("#fe_div_"+d.id_data_file).remove();
//				    				
//				    			}
			    			}
			    		});
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
		                            var st_html='<option value="">없음</option>';
		                            var ed_html='<option value="">없음</option>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
					    	    					if(f.data_type==3){
					    	    						if(nanToStr(f.addfilelist)!=''){
					    	    							con+='<td class="l">';
			                                                var img_url = contextPath+"/resource/img/ico_download.png";
						    	    						$.each(f.addfilelist,function(idx,data){
						    	    							con+='<a href="'+contextPath+'/file/download_add.do?id_data_file='+nanToStr(data.id_data_file)+'">';
						    	    							con+='<img src="'+img_url+'" alt="delete" style="width:28px;height:16px;"/>';
						    	    							con+='</a>';
						    	    							
						    	    						})
					    	    							con+='</td>';
					    	    						}
					    	    					}else{
									    	    		con+='<td class="l">'+f.val+'</td>';
					    	    					}
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td></td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
					    	infoOpen('성공하였습니다.');
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
		
	}else{
		if($("#id_city_area").val()!=''){
			$("#reg_id_city_area").val($("#id_city_area").val());
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}else if($("#id_nat_area").val()==''){
			alertOpen('도시, 국가를 선택하십시오.');
			return;
		}else{
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}
		if($("#id_category").val()==''){
			alertOpen('카테고리를 선택하십시오.');
			return;
		}else{
			$("#reg_id_category").val($("#id_category").val());
		}
		$("#reg_year").val($("#search_year").val());
		var params = jQuery("#reg_frm").serialize();
		var url = contextPath+"/adm/data/insertDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+d.val+'"/>';
			    			$("#m_"+d.year+"_"+d.id_meta).append(html);
			    		});
			    		$.each(e.upDetailList,function(i,d){
			    			var html="";
			    			if(nanToStr(d.id_meta_unit)!=''){
				    			html+='<input type="hidden" name="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" id="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" value="'+d.val+'"/>';
				    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).append(html);
			    			}
			    		});
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.searchBetween.st_year;
	    	    			var ed_year = e.searchBetween.ed_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
								    	    		con+='<td class="l">'+f.val+'</td>';
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td></td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
					    	infoOpen('성공하였습니다.');
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
	}
}
function delData(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('존, 도시, 국가를 선택하십시오.');
		return;
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}
	var params = jQuery("#reg_frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.id_meta).remove();
	    		});
	    		if(nanToStr(e.delFileList)!=''){
	    			$("[id^=fe_div_]").remove();
	    		}
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
	    		$.each(e.delDetailList,function(i,d){
	    			$("#di_"+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit).remove();
	    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).val('');
	    		});
	    		if(nanToStr(e.searchBetween)!=''){
	    			changeBetween(e.searchBetween,e.yd);
	    		}
		    	var re_param = $("#frm").serialize();
		    	var re_url = contextPath+"/contentAjax.do";
		    	$.ajax({
		    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    	    type: "POST", // HTTP 요청 방식(GET, POST)
		    	    data : re_param,
		    	    dataType: "json" ,
		    	    success : function(e){
		    	    	var con = "";
	    	    		if(nanToStr(e.yd)==''){
		    	    		con+='<tbody>';
		    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}else{
		    	    			
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
				    	    			
			    	    			}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<tr>';
		    	    			$.each(e.rowList,function(i,d){
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
			    	    				var isEmpty =false;
				    	    			$.each(d.child,function(j,f){
				    	    				if(year == f.year){
				    	    					isEmpty = true;
				    	    					if(f.data_type==3){
				    	    						if(nanToStr(f.addfilelist)!=''){
				    	    							con+='<td class="l">';
		                                                var img_url = contextPath+"/resource/img/ico_download.png";
					    	    						$.each(f.addfilelist,function(idx,data){
					    	    							con+='<a href="'+contextPath+'/file/download_add.do?id_data_file='+nanToStr(data.id_data_file)+'">';
					    	    							con+='<img src="'+img_url+'" alt="delete" style="width:28px;height:16px;"/>';
					    	    							con+='</a>';
					    	    							
					    	    						})
				    	    							con+='</td>';
				    	    						}
				    	    					}else{
								    	    		con+='<td class="l">'+f.val+'</td>';
				    	    					}
				    	    				}
				    	    			});
				    	    			if(isEmpty ==false){
						    	    		con+='<td></td>';
				    	    			}
			    	    			}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}
		    	    },
		    	    error : function(e){
		    	    	endProcessing();
		    	    	if(e.status=='403'){
		    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	    	}else if(e.status=='401'){
		    			    alertOpen('접근 권한이 없습니다.');
		    	    	}else{
		    		    	alertOpen('실패하였습니다.');
		    	    	}
		    	    }
		    	});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function changeBetween(b,yd){
	var st_html='<option value="">없음</option>';
	var ed_html='<option value="">없음</option>';
	for(var i = yd.min_year;i<=yd.max_year;i++){
		if(i==b.st_year){
			st_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			st_html+='<option value="'+i+'">'+i+'</option>';
		}
		if(i==b.ed_year){
			ed_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			ed_html+='<option value="'+i+'">'+i+'</option>';
		}
	}
	$("#st_year").html(st_html);
	$("#ed_year").html(ed_html);
}
*/
function register(){
	var id_meta = nanToStr($("#id_meta").val());
	if(id_meta!=''){
		var formData = new FormData($('#reg_frm')[0]);
		var url = contextPath+"/adm/data/insertFileIncludeDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : formData,
		    enctype: 'multipart/form-data',
		    processData:false,
		    contentType:false,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
//			    			if(nanToStr(d.id_meta_data)!=''){
//				    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+nanToStr(d.val)+'"/>';
//				    			$("#m_"+d.year+"_"+d.id_meta).append(html);
//			    			}
			    			if(nanToStr(d.addfile)!=''){
				    				var file = d.addfile[0];
				    				var inHtml='';
					    				inHtml+='	<div class="col-6" id="fe_div_'+d.id_meta+'">';
					    				inHtml+='	  <div class="tit_cont">첨부된 파일</div>';
					    				inHtml+='   <div class="form-group">';
					    				inHtml+='         <input type="hidden" name="fi_'+d.year+'_'+d.id_meta_data+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
					    				inHtml+='         <a href="'+file.file+'" download="'+file.file_nm+'">';
					    				inHtml+='         <img src="'+contextPath+'/resource/img/ico_download.png" alt="delete" style="width:28px;height:16px;"/>';
					    				inHtml+='         </a>';  
					    				inHtml+='   </div>';
					    				inHtml+='</div>';
					    				$("#file_div_"+d.id_meta).after(inHtml);
//				    			}else{
//				    				$("#fe_div_"+d.id_data_file).remove();
//				    				
//				    			}
			    			}
			    		});
		    		}
		    		if(e.updateFileList.length!=0){
			    		$.each(e.updateFileList,function(i,d){
			    			var html="";
			    			if(nanToStr(d.upfile)!=''){
				    				var file = d.upfile[0];
				    				var inHtml='';
					    				inHtml+='	<div class="tit_cont">첨부된 파일</div>';
					    				inHtml+='   <div class="form-group">';
					    				inHtml+='         <input type="hidden" name="fi_'+d.year+'_'+d.id_meta_data+'" id="fi_'+d.year+'_'+d.id_meta_data+'" value="'+nanToStr(d.id_meta_data)+'"/>';
					    				inHtml+='         <a href="'+file.file+'" download="'+file.file_nm+'">';
					    				inHtml+='         <img src="'+contextPath+'/resource/img/ico_download.png" alt="delete" style="width:28px;height:16px;"/>';
					    				inHtml+='         </a>';  
					    				inHtml+='   </div>';
					    				$("#fe_div_"+d.id_meta).html(inHtml);
//				    			}else{
//				    				$("#fe_div_"+d.id_data_file).remove();
//				    				
//				    			}
			    			}
			    		});
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
		                            var st_html='<option value="">없음</option>';
		                            var ed_html='<option value="">없음</option>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
					    	    		if(year==st_year){
					    	    			st_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			st_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
					    	    		if(year==ed_year){
					    	    			ed_html+='<option value="'+year+'" selected>'+year+'</option>';
					    	    		}else{
					    	    			ed_html+='<option value="'+year+'">'+year+'</option>';
					    	    		}
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    		$("#st_year").html(st_html);
			    	    		$("#ed_year").html(ed_html);
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
					    	    					if(f.data_type==3){
					    	    						if(nanToStr(f.addfilelist)!=''){
					    	    							con+='<td class="l">';
			                                                var img_url = contextPath+"/resource/img/ico_download.png";
						    	    						$.each(f.addfilelist,function(idx,data){
						    	    							con+='<a href="'+data.file+'" download="'+data.file_nm+'">';
						    	    							con+='<img src="'+img_url+'" alt="delete" style="width:28px;height:16px;"/>';
						    	    							con+='</a>';
						    	    							
						    	    						})
					    	    							con+='</td>';
					    	    						}
					    	    					}else{
									    	    		con+='<td class="l">'+f.val+'</td>';
					    	    					}
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td>-</td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
					    	infoOpen('성공하였습니다.');
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
		
	}else{
		if($("#id_city_area").val()!=''){
			$("#reg_id_city_area").val($("#id_city_area").val());
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}else if($("#id_nat_area").val()==''){
			alertOpen('도시, 국가를 선택하십시오.');
			return;
		}else{
			$("#reg_id_nat_area").val($("#id_nat_area").val());
		}
		if($("#id_category").val()==''){
			alertOpen('카테고리를 선택하십시오.');
			return;
		}else{
			$("#reg_id_category").val($("#id_category").val());
		}
		$("#reg_year").val($("#search_year").val());
		var params = jQuery("#reg_frm").serialize();
		var url = contextPath+"/adm/data/insertDataAjax.do";
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(e.upList.length!=0){
			    		$.each(e.upList,function(i,d){
			    			var html="";
			    			html+='<input type="hidden" name="i_'+d.year+"_"+d.id_meta_data+'" id="i_'+d.year+"_"+d.id_meta_data+'" value="'+d.val+'"/>';
			    			$("#m_"+d.year+"_"+d.id_meta).append(html);
			    		});
			    		$.each(e.upDetailList,function(i,d){
			    			var html="";
			    			if(nanToStr(d.id_meta_unit)!=''){
				    			html+='<input type="hidden" name="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" id="di_'+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit+'" value="'+d.val+'"/>';
				    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).append(html);
			    			}
			    		});
		    		}
		    		if(nanToStr(e.searchBetween)!=''){
		    			changeBetween(e.searchBetween,e.yd);
		    		}
			    	var re_param = $("#frm").serialize();
			    	var re_url = contextPath+"/contentAjax.do";
			    	$.ajax({
			    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
			    	    type: "POST", // HTTP 요청 방식(GET, POST)
			    	    data : re_param,
			    	    dataType: "json" ,
			    	    success : function(e){
			    	    	var con = "";
//	 	    	    		var param_st_year = '${param.st_year}';
//	 	    	    		var param_ed_year = '${param.ed_year}';
	    	    			var st_year = e.searchBetween.st_year;
	    	    			var ed_year = e.searchBetween.ed_year;
	    	    			
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
//	 		    	    		if(param_st_year !='' && param_ed_year !=''){
//	 		    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 				    	    		con+='<th scope="col">'+year+'</th>';
//	 		    	    			}
//	 		    	    		}else{
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
			    	    			}
//	 		    	    		}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
		    	    			$.each(e.rowList,function(i,d){
				    	    		con+='<tr>';
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
//	 			    	    		if(param_st_year !='' && param_ed_year !=''){
//	 			    	    			for(var year=Number(param_st_year);year<=Number(param_ed_year);year++){
//	 			    	    				var isEmpty =false;
//	 				    	    			$.each(d.child,function(j,f){
//	 				    	    				if(year == f.year){
//	 				    	    					isEmpty = true;
//	 							    	    		con+='<td class="l">'+f.val+'</td>';
//	 				    	    				}
//	 				    	    			});
//	 				    	    			if(isEmpty ==false){
//	 						    	    		con+='<td></td>';
//	 				    	    			}
//	 			    	    			}
//	 			    	    		}else{
				    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
				    	    				var isEmpty =false;
					    	    			$.each(d.child,function(j,f){
					    	    				if(year == f.year){
					    	    					isEmpty = true;
								    	    		con+='<td class="l">'+f.val+'</td>';
					    	    				}
					    	    			});
					    	    			if(isEmpty ==false){
							    	    		con+='<td>-</td>';
					    	    			}
				    	    			}
//	 			    	    		}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
					    	infoOpen('성공하였습니다.');
			    	    },
			    	    error : function(e){
			    	    	endProcessing();
			    	    	if(e.status=='403'){
			    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
			    	    	}else if(e.status=='401'){
			    			    alertOpen('접근 권한이 없습니다.');
			    	    	}else{
			    		    	alertOpen('실패하였습니다.');
			    	    	}
			    	    }
			    	});

		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});		
	}
}
function delData(){
	if($("#id_zone_area").val()!=''){
		$("#id_area").val($("#id_zone_area").val());
	}else if($("#id_city_area").val()!=''){
		$("#id_area").val($("#id_city_area").val());
	}else if($("#id_nat_area").val()==''){
		alertOpen('존, 도시, 국가를 선택하십시오.');
		return;
	}
	if($("#id_category").val()==''){
		alertOpen('카테고리를 선택하십시오.');
		return;
	}
	var params = jQuery("#reg_frm").serialize();
	var url = contextPath+"/adm/data/deleteDataAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$.each(e.delList,function(i,d){
	    			$("#i_"+d.id_meta).remove();
	    		});
	    		if(nanToStr(e.delFileList)!=''){
	    			$("[id^=fe_div_]").remove();
	    		}
	    		$.each(e.metaList,function(i,d){
	    			$("#m_"+d.year+"_"+d.id_meta).val('');
	    		});
	    		$.each(e.delDetailList,function(i,d){
	    			$("#di_"+d.year+"_"+d.id_meta_data+"_"+d.id_meta_unit).remove();
	    			$("#d_"+d.year+"_"+d.id_meta+"_"+d.id_meta_unit).val('');
	    		});
	    		if(nanToStr(e.searchBetween)!=''){
	    			changeBetween(e.searchBetween,e.yd);
	    		}
		    	var re_param = $("#frm").serialize();
		    	var re_url = contextPath+"/contentAjax.do";
		    	$.ajax({
		    		url: re_url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    	    type: "POST", // HTTP 요청 방식(GET, POST)
		    	    data : re_param,
		    	    dataType: "json" ,
		    	    success : function(e){
		    	    	var con = "";
	    	    		if(nanToStr(e.yd)==''){
		    	    		con+='<tbody>';
		    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}else{
		    	    			
	    	    			var st_year = e.yd.min_year;
	    	    			var ed_year = e.yd.max_year;
			    	    	if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<thead>';
			    	    		con+='<tr>';
			    	    		con+='<th class="first" colspan="'+e.maxLvl+'">항목 </th>';
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
					    	    		con+='<th scope="col">'+year+'년</th>';
				    	    			
			    	    			}
			    	    		con+='</tr>';
			    	    		con+='</thead>';
			    	    	}
		    	    		con+='<tbody>';
		    	    		if(e.rowList.length==0 || e.totalList.length==0){
			    	    		con+='<tr><td>검색결과없음</td></tr>';
		    	    		}
		    	    		if(e.rowList.length>0 && e.totalList.length>0){
			    	    		con+='<tr>';
		    	    			$.each(e.rowList,function(i,d){
			    	    			$.each(d.child,function(j,f){
			    	    				var colspan = 1;
			    	    				if(f.colspan !=''){
			    	    					colspan=f.colspan;
			    	    				}
			    	    				var rowspan = 1;
			    	    				if(nanToStr(f.rowspan) !=''){
			    	    					rowspan=nanToStr(f.rowspan);
			    	    				}
			    	    				if(nanToStr(f.head)!=''){
			    	    					if((nanToStr(f.colspan) !='' || nanToStr(f.rowspan) !='') && f.data_type !='3'){
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;" >';
				    		    	    		con+='<input name="check" type="checkbox" value="'+f.id_meta+'" id="check_'+f.id_meta+'">';
				    		    	    		con+='<span class="mh-check"></span>';
				    		    	    		con+='<label class="form-check-label" for="check_'+f.id_meta+'"></label>';
				    		    	    		con+=f.unit_nm+'</td>';
			    	    					}else{
				    		    	    		con+='<td class="l" colspan="'+colspan+'" rowspan="'+rowspan+'"  style="text-align:left;padding-left:35px;" >'+f.unit_nm+'</td>';
			    	    					}
			    	    				}
			    	    			});
			    	    			for(var year=Number(st_year);year<=Number(ed_year);year++){
			    	    				var isEmpty =false;
				    	    			$.each(d.child,function(j,f){
				    	    				if(year == f.year){
				    	    					isEmpty = true;
							    	    		con+='<td class="l">'+f.val+'</td>';
				    	    				}
				    	    			});
				    	    			if(isEmpty ==false){
						    	    		con+='<td>-</td>';
				    	    			}
			    	    			}
				    	    		con+='</tr>';
		    	    			});
		    	    		}
		    	    		con+='</tbody>';
		    	    		$("#result_table").html(con);
	    	    		}
		    	    },
		    	    error : function(e){
		    	    	endProcessing();
		    	    	if(e.status=='403'){
		    		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	    	}else if(e.status=='401'){
		    			    alertOpen('접근 권한이 없습니다.');
		    	    	}else{
		    		    	alertOpen('실패하였습니다.');
		    	    	}
		    	    }
		    	});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function changeBetween(b,yd){
	var st_html='<option value="">없음</option>';
	var ed_html='<option value="">없음</option>';
	for(var i = yd.min_year;i<=yd.max_year;i++){
		if(i==b.st_year){
			st_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			st_html+='<option value="'+i+'">'+i+'</option>';
		}
		if(i==b.ed_year){
			ed_html+='<option value="'+i+'" selected>'+i+'</option>';
		}else{
			ed_html+='<option value="'+i+'">'+i+'</option>';
		}
	}
	$("#st_year").html(st_html);
	$("#ed_year").html(ed_html);
}
