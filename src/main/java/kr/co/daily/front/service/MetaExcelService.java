package kr.co.daily.front.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import daily.common.util.AddBatchUtil;
import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.dao.MetaExcelDAO;

@Service
public class MetaExcelService {
	@Autowired private MetaService service;
	@Autowired
	private MetaExcelDAO dao;

	@Value("#{global['openapi.addbatch.cnt']}")
	private int addBatchCnt;

	public CustomMap getMetaExcel(CommandMap map) {
		return dao.getMetaExcel(map.getMap());
	}
	public CustomMap selectMetaExcelList(CommandMap map) {
		return dao.selectMetaExcelList(map.getMap());
	}

	public void insertMetaExcel(CommandMap map) {
		dao.insertMetaExcel(map.getMap());
	}

	public void updateMetaExcel(CommandMap map) {
		dao.updateMetaExcel(map.getMap());
	}

	public void deleteMetaExcel(CommandMap map) {
		dao.deleteMetaExcel(map.getMap());
	}
	public int getMaxDepthMetaByKey(CommandMap map) {
		return dao.getMaxDepthMetaByKey(map.getMap());
	}
	public List<CustomMap> selectMetaOrderList(CommandMap map) {
		return dao.selectMetaOrderList(map.getMap());
	}
	public List<CustomMap> selectMetaOrderListForExcel(CommandMap map) {
		return dao.selectMetaOrderListForExcel(map.getMap());
	}
	public void insertMetaExcelData(List<Map<CommandMap, List<CommandMap>>> realDataList, CommandMap param, Map<Integer, Integer> idMetaMAtDepth) throws Exception {
		List<CommandMap> insList = new ArrayList<CommandMap>();
		int parent_id_meta_row = 0;
		int bef_depth=0;
		int curr_depth = 1;
		Map<Integer,Integer> curr_parent_id_meta_rowMap = new HashMap<>();
		for(Map<CommandMap,List<CommandMap>> data:realDataList) {
			for(CommandMap key:data.keySet()) {
				if(data.get(key)!=null && data.get(key).size()>0) {
					List<CommandMap> valList = data.get(key);
					int id_meta_m = idMetaMAtDepth.get(key.getInt("depth"));
					CommandMap rowMap = new CommandMap();
					if(key.getInt("id_parent",0)!=0) {
						rowMap.put("parent_id_meta_row", parent_id_meta_row);
						int data_depth = key.getInt("depth");
						if(curr_depth==data_depth) {
							parent_id_meta_row = curr_parent_id_meta_rowMap.get(data_depth);
							rowMap.put("parent_id_meta_row", parent_id_meta_row);
						}else {
							rowMap.put("parent_id_meta_row", parent_id_meta_row);
						}
						rowMap.put("id_meta_m", id_meta_m);
						curr_depth = key.getInt("depth");
						curr_parent_id_meta_rowMap.put(data_depth, parent_id_meta_row);
					}else {
						rowMap.put("id_meta_m", id_meta_m);
						curr_depth=1;
						for(int rowMap_depth:curr_parent_id_meta_rowMap.keySet()) {
							if(rowMap_depth>1) {
								curr_parent_id_meta_rowMap.remove(rowMap_depth);
							}
						}
					}
					dao.insertMetaDataRowForExcelUpload(rowMap.getMap());
		    		CommandMap metaParam = new CommandMap();
					metaParam.put("trg_id_meta_m",id_meta_m);
					List<CustomMap> colList = service.selectMeta(metaParam );
		    		if(colList==null || colList.size()==0) {
		    			throw new Exception("매칭되는 정보를 가져오지 못했습니다.");
		    		}
					
					parent_id_meta_row =rowMap.getInt("id_meta_row",0);
					List<Integer> calMetaIdList = new ArrayList<>();
					for(CustomMap o:colList) {
						int al_meta_m = o.getInt("id_meta", 0);
						if(al_meta_m>0) {
							String al_typ = o.getString("typ","").trim();
							if(GlobalConst.DT_CALC.equals(al_typ)) {
								calMetaIdList.add(al_meta_m);
							}
						}
					}
			        CommandMap calParam = new CommandMap();
			        calParam.put("cal_meta_m_list", calMetaIdList);
			        calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
			        List<CustomMap> calColInfoList = service.selectCalcuListByParam(calParam);

					List<CommandMap> calColList = MetaUtil.calculateRowData(colList,valList,calColInfoList,rowMap.getInt("id_meta_row"));
//					for(CommandMap val:valList) {
//						int id_meta = val.getInt("id_meta", 0);
//						if(id_meta>0) {
//							val.put("id_meta_row", rowMap.getInt("id_meta_row"));
//							if(!"".equals(val.getString("val",""))) {
//								insList.add(val);
//							}
//						}
//					}
					for(CommandMap val:calColList) {
						int id_meta = val.getInt("id_meta", 0);
						if(id_meta>0) {
							val.put("id_meta_row", rowMap.getInt("id_meta_row"));
							if(!"".equals(val.getString("val",""))) {
								insList.add(val);
							}
						}
					}
				}
			}
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, insList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			for(Map map:inInputList) {
				System.out.println("map의 id_meta_row:"+map.get("id_meta_row")+",id_meta"+map.get("id_meta"));
			}
			dao.insertMetaDataByGroupForExcelUpload(inInputList);
		}

	}
}
