<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메타 마스터"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle} 관리"/>
	
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle} 관리</span>
			</div>
			
			<div class="page-content-wrap">
				<div class="dbBtnbox">
					<div class="l"></div>
					<div class="r">
						<button type="button" class="btn-primary" onClick="javascript:openReg();">
							${pageTitle} 등록
						</button>
						<button type="button" class="btn-danger" onClick="javascript:deleteMetaList();">
							${pageTitle} 삭제
						</button>
					</div>
				</div>
				
				<form name="frm" id="frm" method="post">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox" id="allCheck" onclick="allCheck();">
							</div>
							<div class="header-unit">
								<a href="#">
									${pageTitle} ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									${pageTitle} 이름
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									${pageTitle} 설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont"  id="div${map.id_meta_m}" ondblclick="javascript:openReg('${map.id_meta_m}')">
									<div class="check-unit">
										<input type="checkbox" name="cb_id_meta_m_${map.id_meta_m}" value="${map.id_meta_m}">
									</div>
									<div class="cont-unit">${map.id_meta_m}</div>
									<div class="cont-unit">${map.meta_m_nm}</div>
									<div class="cont-unit desc-unit">${map.meta_m_doc}</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_meta_m" id="id_meta_m" value="${param.id_meta_m}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
	var act_type = "${param.act_type}";
	var ret = "${param.ret}";
	if(act_type =='D'){
		if(ret==0){
			infoOpen('성공하였습니다.');
		}else if(ret==1){
    		alertOpen('실패하였습니다.');
		}
	}
});
function openReg(id_role){
	var frm =document.frm;
	frm.action=contextPath+"/adm/meta/regMetaM.do";
	if(id_role){
		$("#id_meta_m").val(id_role);
	}else{
		$("#id_meta_m").val('');
	}
	$("#frm").submit();
}
function allCheck(){
	var checkboxes = document.getElementsByName('cb_id_meta_m'); // 모든 체크박스 요소를 가져옵니다.
    var allCheckElement = document.getElementById('allCheck'); // 전체 선택 체크박스 요소를 가져옵니다.
    // 전체 선택 체크박스의 체크 여부에 따라 모든 체크박스를 선택하거나 선택 해제합니다.
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = allCheckElement.checked;
    }
}

function deleteMetaList(){
	confirmOpen('해당 메타를 삭제하시면 관련 하위 메타들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteData");
}
function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/meta/deleteMetaMListAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
// 	    	}else if(e.ret==8){
//     			alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
//     			return;
	    	}else{
	    		$.each(e.del_key_list,function(i,d){
					$("#div"+d).remove();
				});
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}

function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/meta/metaMList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
</script>
</html>
