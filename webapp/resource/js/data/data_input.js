var cur_keyword_num=1;
function registerData(){
	$("#is_insert").val('Y');
	$("#id_board_master").val(id_board_master);
	var frm = document.frm;
	frm.action=contextPath+"/data/regData.do";
	frm.submit();
}
function showMoreKind(id){
	$("#id_kind").val(id);
	$("#is_more").val('Y');
	var frm = document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}
function showList(id){
	$("#id_kind").val(id);
	if(id==''){
		$("#is_more").val('N');
	}
	var frm = document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}

function changeRegChildData(that){
	if($(that).val()==''){
		$("#id_board_master").val($("#sel_child_board option:selected").val());
	}else{
		$("#id_board_master").val($(that).val());
	}
	//changeRegTitle('Y');
}

function changeRegData(that){
	var sel_ord_str = $("#sel_parent_board option:selected").val();
//	$("#sel_ord_str").val(sel_ord_str);
	redrawDataBoard($(that).val());
}
function redrawDataBoard(sel_ord_str){
	$.ajax({
		url: contextPath+"/data/changeDataRootAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: {sel_ord_str:sel_ord_str},                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	//changeRegTitle();
	    	if(e.ret==1){
		    	alertOpen('게시판 정보 조회시 오류가 발생했습니다.');
	    	}else{
    			var html1 ='';
    			var fp = nanToStr(e.first_child);
	    		$.each(e.childList,function(i,d){
	    			if(fp!='' && fp.id_board_master == d.id_board_master){
		    			//html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
		    			html1+='<option value="'+d.id_board_master+'" selected>'+d.board_nm+'</option>';
		    			$("#id_board_master").val(fp.id_board_master);
	    			}else{
		    			//html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
		    			html1+='<option value="'+d.id_board_master+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#sel_child_board").html(html1);
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	});
}

function viewDataBoard(id_board_master,id_board,ord_str){
	$("#id_board_master").val(id_board_master);
	$("#id_board").val(id_board);
	$("#ord_str").val(ord_str);
	$("#is_insert").val('N');
	var frm = document.frm;
	frm.action=contextPath+"/data/regData.do";
	frm.submit();
}

function fn_return(){
	$("#main_content1").show();
	$("#subConcent").hide();
}
function search(){
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	doList(1);
}
function searchByCombo(){
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	doList(1);
}
function searchByComboPage(that){
	$("#more_page_rows").val($(that).val());
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	doList(1);
}

function search_keyword_init(){
	$("#sel_ord_str").val('');
	$("#search_keyword").val('');
	$("#search_init").hide();
}
function doList(page){
	if(nanToStr(page)!=''){
		$("#page").val(page);
	}
	var frm =document.frm;
	frm.action=contextPath+"/data/data.do";
	frm.submit();
}


/* ================================================================================*/
function viewDataDetail(id_board_master,id_board,ord_str){
	if(!(id_board_master == null || id_board_master == '' || id_board_master == undefined)){
		$("#id_board_master").val(id_board_master);
	}
	if(!(id_board == null || id_board == '' || id_board == undefined)){
		$("#id_board").val(id_board);
	}
	if(!(ord_str == null || ord_str == '' || ord_str == undefined)){
		$("#ord_str").val(ord_str);
	}
	
	$("#is_insert").val('N');
	var frm = document.frm;
	frm.action=contextPath+"/data/dataDetail.do";
	frm.submit();
}


function fileAttachBtn() {
	var btnElement = event.target;
	if(btnElement.tagName != 'BUTTON') {
		btnElement = btnElement.parentNode;
		if(btnElement.tagName != 'BUTTON') return;
	}
	var fileElement = btnElement.nextElementSibling;
	fileElement.click();
}

function createNewList(oldList) {
	for(var i=0; i<oldList.length; i++) {
		var item = oldList[i];
		var newObj = {};
		newObj.id_board_file = item.id_board_file;
		newObj.file_sh_ext = item.file_sh_ext;
		newObj.file_ext = item.file_ext;
		newObj.file_nm = item.file_nm;
		newObj.mode = 'initial';
		newList.push(newObj);
	}
}
function displayFileList() {
	console.log("여기")
	console.log(newList)
	var html = '';
	for(var i=0; i<newList.length; i++) {
		var item = newList[i];
		html += '<div class="file-set">';
		html += '	<a class="file-icon" href="'+ contextPath +'/file/data_download.do?id_board_file=' + item.id_board_file +'">';
		html += '		<span class="'+ item.file_sh_ext +'">'+item.file_ext+'</span>';
		html += '	</a>';
		if(item.mode == 'initial') {
			html += '<a class="file-name" href="'+ contextPath +'/file/data_download.do?id_board_file=' + item.id_board_file +'">';
			html += '	<span>'+item.file_nm+'</span>';
			html += '</a>';
			html += '<button class="file-remove btn-blank" onclick="fileModeChange('+ i +')">';
			html += '	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			html += '</button>';
		} else if(item.mode == 'delete') {
			html += '<a class="file-name delete" href="'+ contextPath +'/file/data_download.do?id_board_file=' + item.id_board_file +'">';
			html += '	<span>'+item.file_nm+'</span>';
			html += '</a>';
			html += '<button class="file-revert btn-blank" onclick="fileModeChange('+ i +')">';
			html += '	<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>';
			html += '</button>';
		} 
		html += '</div>';
	}
	$("#attachedFiles").empty();
	$("#attachedFiles").append(html);
}
function fileModeChange(index) {
	var item = newList[index];
	if(item.mode == 'initial') item.mode = 'delete';
	else if(item.mode == 'delete') item.mode = 'initial';
	displayFileList();
}

function addNewFile(item) {
	var location = $("#newFiles");
	var nowItems = location.children();
	var lastIdx = 0;
	for(var i=0; i<nowItems.length; i++) {
		var thisIdx = Number(nowItems[i].getAttribute("data-index"));
		if(thisIdx > lastIdx) lastIdx = thisIdx;
	}
	
	//하단에 아이콘 + inputFile 등록..
	var newFile = item.files; //fileList보안상 생성/수정불가. 그대로 전달필요..
	var html = '';
	
	lastIdx = lastIdx + 1;
	var fileNm = newFile[0].name;
	var nameArr = fileNm.split(".");
	var fileExt = nameArr[nameArr.length - 1];
	var fileShExt = fileExt.substring(0,3).toLowerCase();
	
	html += '<div class="file-set" data-index="'+ lastIdx +'">';
	html += '	<a class="file-icon" href="#" onclick="return false;">';
	html += '		<span class="'+ fileShExt +'">'+fileExt+'</span>';
	html += '	</a>'
	html += '	<a class="file-name no-link" href="#" onclick="return false;">';
	html += '		<span>'+ fileNm +'</span>';
	html += '	</a>'
	html += '	<button class="file-remove btn-blank" onclick="deleteNewFile(this);">'
	html += '		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
	html += '	</button>';
	html += '	<input type="file" class="file-input-hidden" name="new_file_'+ lastIdx +'">';
	html += '</div>';
	
	location.append(html);

	//파일데이터 삽입...
	var target = document.querySelector('input[name="new_file_'+ lastIdx +'"]');
	target.files = newFile;
	
	//초기화 : 현재 태그 삭제, 새로 삽입
	var parentNode = $(item).parent();
	$(item).remove();
	var newInput = '<input type="file" class="file-input-hidden" onchange="javascript:addNewFile(this);"/>';
	parentNode.append(newInput);
}
function deleteNewFile(element) {
	var target = element.parentNode;
	$(target).remove();
}
function toDetailView(id_master, id_board_master, id_board, ord_str){
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "get");  //Post 방식
    form.setAttribute("action", contextPath+"/data/dataDetail.do?");

	var idMaster = document.createElement("input");
	idMaster.setAttribute("name", "id_master");
	idMaster.setAttribute("value", id_master);
	form.appendChild(idMaster);
	var idBoardMaster = document.createElement("input");
	idBoardMaster.setAttribute("name", "id_board_master");
	idBoardMaster.setAttribute("value", id_board_master);
	form.appendChild(idBoardMaster);
	
	var idBoard = document.createElement("input");
	idBoard.setAttribute("name", "id_board");
	idBoard.setAttribute("value", id_board);
	form.appendChild(idBoard);
	
	var ordStr = document.createElement("input");
	ordStr.setAttribute("name", "ord_str");
	ordStr.setAttribute("value", ord_str);
	form.appendChild(ordStr);
	
    document.body.appendChild(form);
	form.submit();
}
function toBoard(id_master, search_keyword) {
	var form = document.createElement("form");
    form.setAttribute("charset", "UTF-8");
    form.setAttribute("method", "get");  //Post 방식
    form.setAttribute("action", contextPath+"/data/data.do?");

	var idMaster = document.createElement("input");
	idMaster.setAttribute("name", "id_master");
	idMaster.setAttribute("value", id_master);
	form.appendChild(idMaster);
	
	var searchKeyword = document.createElement("input");
	searchKeyword.setAttribute("name", "search_keyword");
	searchKeyword.setAttribute("value", search_keyword);
	form.appendChild(searchKeyword);

    document.body.appendChild(form);
	form.submit();
}

function addVideoStringBtn() {
	var masterNode = document.querySelector(".videoInputMaster");
	var width = 0;
	var height = 0;
	var newWidth = masterNode.querySelector(".videoWidthVal").value;		
	var newHeight = masterNode.querySelector(".videoHeightVal").value;
	if(newWidth != undefined || newWidth != null) {
		width = newWidth;
	}
	if(newHeight != undefined || newHeight != null) {
		height = newHeight;
	}
	var url = masterNode.querySelector(".videoUrlString").value;
	
	var newVideoString = '<div name="player" width="'+width+'" height="'+height+'">'+url+'</div>';
	
	var commentEle = document.querySelector("#comment");
	var nowInputPos = commentEle.selectionStart;
	
	var comment = commentEle.value;
	var newComment = comment.slice(0, nowInputPos) +newVideoString+ comment.slice(nowInputPos)

	commentEle.value = newComment;
}