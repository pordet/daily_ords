<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메뉴 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
	<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/menu_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
			
				<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_parent_menu" id="id_parent_menu"/>
					
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
									<select onChange="javascript:changeMenu();" name="search_parent_menu" id="search_parent_menu">
										<option value="" <c:if test="${param.search_parent_menu == ''}">selected</c:if>>상위메뉴</option>
										<c:forEach var="code" items="${codeList}">
											<option value="${code.id_menu}" <c:if test="${param.search_parent_menu == code.id_menu}">selected</c:if>>${code.menu_nm}</option>
										</c:forEach>
									</select>
									<select name="search_col" id="search_col">
										<option value="nm" ${(param.search_col eq 'nm'? 'selected':'')}>메뉴명</option>
										<option value="pn" ${(param.search_col eq 'pn'? 'selected':'')}>상위 메뉴명</option>
										<option value="url" ${(param.search_col eq 'url'? 'selected':'')}>메뉴 URL</option>
									</select>
									<input type="text" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="trg_menu_id" id="trg_menu_id" value="${param.trg_menu_id}"/>
				</form>
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button class="btn-primary" onClick="javascript:openMenu('${param.id_menu}');">메뉴 등록</button>
							<button class="btn-danger" onClick="javascript:deleteList();">메뉴 삭제</button>
						</div>					
					</div>
					
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit" style="width:15%;">
								<a href="">
									메뉴명
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit" style="width:15%;">
								<a href="">
									상위메뉴명
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit" style="width:40%;">
								<a href="">
									메뉴 URL
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit" style="width:15%;">
								<a href="">
									메뉴 Depth
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit" style="width:15%;">
								<a href="">
									사용여부
									<ctrl:orderMark val="5" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<form id="listFrm" name="listFrm">
								<input type="hidden" name="id_parent_menu" id="list_id_parent_menu" value="${param.id_parent_menu}"/>
								<input type="hidden" name="search_col" id="list_search_col" value="${param.search_col}"/>
								<input type="hidden" name="search_keyword" id="list_search_keyword" value="${param.search_keyword}"/>
								<input type="hidden" name="id_menu" id="list_id_menu" value="${param.id_menu}"/>
								<input type="hidden" name="trg_menu_id" id="list_trg_menu_id" value="${param.trg_menu_id}"/>
								<input type="hidden" name="sidx" id="list_sidx" value="${param.sidx}"/>
								<input type="hidden" name="sord" id="list_sord" value="${param.sord}"/>
								<input type="hidden" name="page" id="list_page" value="${param.page}"/>
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="openMenu('${param.id_menu}','${map.id_menu}','${map.id_parent_menu}')">
									<input type="hidden" id="id_menu${i.count}" value="${map.id_menu}">
									<div class="check-unit">
										<input type="checkbox" value="${map.id_menu}" name="del_code" id="del_code${i.count}">
									</div>
									<div class="cont-unit" style="width:15%;">${map.menu_nm}</div>
									<div class="cont-unit" style="width:15%;">${(not empty map.parent_menu_nm?map.parent_menu_nm:'없음')}</div>
									<div class="cont-unit" style="width:40%; text-align:left">${map.menu_url}</div>
									<div class="cont-unit" style="width:15%;">${map.menu_depth}</div>
									<div class="cont-unit" style="width:15%;">${map.use_yn}</div>
								</div>
							</c:forEach>
							</form>
						</div>
					</div>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					

			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
    function changeMenu(){
    	var id_menu = $("#id_menu").val();
    	var id_parent_menu = $("#search_parent_menu").val();
    	if(id_parent_menu){
    		location.href="<c:url value='/adm/menu/menuList.do'/>?search_parent_menu="+id_parent_menu+"&id_menu="+id_menu;
    	}else{
    		location.href="<c:url value='/adm/menu/menuList.do'/>?id_menu="+id_menu;
    	}
    }
    function openMenu(id_menu,key,id_parent_menu){
    	var frm =document.frm;
    		$("#trg_menu_id").val(nanToStr(key));
    		if(nanToStr(id_parent_menu)==''){
        		$("#id_parent_menu").val($("#search_parent_menu").val());
    		}else{
        		$("#id_parent_menu").val(id_parent_menu);
    		}
    		frm.action=contextPath+"/adm/menu/regMenu.do";
//    		frm.action="/excelDownload.do";
    		frm.submit();

    }
//     function openMenu1(id_menu,key){
//     	var id_parent_menu = $("#id_parent_menu").val();
//     	if(key){
//         	if(id_parent_menu){
//         		location.href="<c:url value='/adm/menu/regMenu.do'/>?id_parent_menu="+id_parent_menu+"&trg_id_menu="+key+"&id_menu="+id_menu;
//         	}else{
//         		location.href="<c:url value='/adm/menu/regMenu.do'/>?trg_id_menu="+key+"&id_menu="+id_menu;
//         	}
//     	}else{
//         	if(id_parent_menu){
//         		location.href="<c:url value='/adm/menu/regMenu.do'/>?id_parent_menu="+id_parent_menu+"&id_menu="+id_menu;
//         	}else{
//         		location.href="<c:url value='/adm/menu/regMenu.do'/>?id_menu="+id_menu;
//         	}
//     	}
//     }
	function doList(page){
		processing();
		var frm =document.frm;
			frm.action=contextPath+"/adm/menu/menuList.do";
	   		$("#page").val(page);
	//    	frm.action="/excelDownload.do";
	    	$("#frm").submit();
	}
    function search(){
    	doList(1);
    }
    function initSearchForm(){
    	$("#search_keyword").val('');
    }
    function deleteList(){
    	if($("input:checkbox[name=del_code]:checked").length==0){
    		alertOpen('삭제할 항목을 선택하십시오.');
    	}else{
        	confirmOpen('삭제하시면 관련 권한이 삭제됩니다.<br>삭제시 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
    	}
    }
    function deleteConfirm(){
    	    $("#list_search_col").val($("#search_col").val());
    	    $("#list_search_keyword").val($("#search_keyword").val());
    		var frm =document.listFrm;
    		var page = $("#page").val();
    		frm.action=contextPath+"/adm/menu/deleteMenuList.do";
 //   		frm.page.value=page;
    	//frm.action="/excelDownload.do";
    	    frm.submit();
    }
    
</script>
</html>
