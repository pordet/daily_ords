package kr.co.daily.common.controller;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.front.controller.BbsController;

@Controller
public class FileDownloadController {
	
	private Logger log = LoggerFactory.getLogger(FileDownloadController.class);
	
	@Autowired ServletContext context; 
	private static String FILE_DOWNLOAD_PATH;
    @Value("#{global['meta.excel.down']}")
	public void setFileUploadPath(String fileUploadPath) {
    	FILE_DOWNLOAD_PATH = fileUploadPath;
	}
	
	
	@RequestMapping(value = "/file/download.do")
	public ModelAndView callDownload(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res) throws Exception {

//		param.put("fileId", fileId);
//		CustomMap map = historyService.getData(param);
		CustomMap map = new CustomMap();
	    String fileFullPath=map.getString("file_path");
		File srcFile = new File(fileFullPath);
		File downloadFile = new File(map.getString("file_nm"));
		FileCopyUtils.copy(srcFile,downloadFile);
	    if(!downloadFile.canRead()){
	        throw new Exception("File can't read(File not found)");
	    }
	    return new ModelAndView("fileDownloadView", "downloadFile",downloadFile);

	}
}
