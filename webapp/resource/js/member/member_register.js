var mberIdDuplChkAt = false;
/**
     * 아이디 입력확인
     */
function fn_validationMberId(){
    var mberId = $("#id_user").val();

    if(fn_empty(mberId)){

        $(".id-input-warning").show();
        $("#id_user").focus();
        return false;
    }else{
        // 아이디 값 확인
        if(!(fn_regExp("id")).test($("#id_user").val())) {
            $(".id-input-warning").hide();
            $(".duplChkIdResult").empty();
            $(".duplChkIdResult").removeClass("input-success");
            $(".duplChkIdResult").append("아이디 형식을 확인해주세요.");
            $(".duplChkIdResult").addClass("input-warning");
            $(".duplChkIdResult").show();
            $("#id_user").focus();
            return false;
        }
    }

    return true;
}

/**
 * 아이디 중복확인
 */
function fn_duplChkMberId(){

    if(!fn_validationMberId()) return;

    $(".input-warning").hide();

    $.ajax({
        type:"POST",
        url: contextPath+"/member/checkDblId.do",
        data : $("#frm").serialize(),
        dataType : "json",
        success: function(e){
            if(e.ret==0){

                //아이디 중복 확인 결과 : 아이디 사용 불가능
                if(e.cnt>0){
                    mberIdDuplChkAt = false;
                    //$("#id_user").prop("readonly", false);
                    $(".id-input-warning").hide();
                    $(".duplChkIdResult").empty();
                    $(".duplChkIdResult").append("사용할 수 없는 아이디입니다");
                    $(".duplChkIdResult").removeClass("input-success");
                    $(".duplChkIdResult").addClass("input-warning");
                    $(".duplChkIdResult").show();
                    $("#btnDuplChkMberId").show();
                    $("#btnChangeMberId").hide();

                    //아이디 중복 확인 결과 : 아이디 사용가능
                }else{
                    mberIdDuplChkAt = true;
                    //$("#id_user").prop("readonly", true);
                    $(".id-input-warning").hide();
                    $(".duplChkIdResult").empty();
                    $(".duplChkIdResult").append("사용 가능한 아이디입니다");
                    $(".duplChkIdResult").addClass("input-success");
                    $(".duplChkIdResult").removeClass("input-warning");
                    $(".duplChkIdResult").show();
                    $("#btnDuplChkMberId").hide();
                    $("#btnChangeMberId").show();
                }

            }else{
                alertOpen("조회에 실패하였습니다.");
            }
        },
        error: function(xhr, status, error) {
            alertOpen("조회에 실패하였습니다.");
        }
    });
}

/**
 * 비밀번호 입력확인
 */
function fn_pswrdInputChk(){

    var pwd = $("#pwd").val();
    var pwdChk = $("#pwd_chk").val();

    if(!fn_empty(pwd) && !fn_empty(pwdChk)){
        if(pwd == pwdChk){
            $(".pswrdChkAt").hide();
        }else{
            $(".pswrdChkAt").show();
        }

        $("#pwd").next(".input-util").find(".input-warning").hide();
    }
}

/**
 * 취소 버튼 클릭
 */
function fn_cancel(){
	/*
    if(!confirmOpen("회원 가입을 취소하고 메인화면으로 돌아가시겠습니까?")) return;
    window.location.href="/";
	*/
    if(!confirmOpen("회원 가입을 취소하고 메인화면으로 돌아가시겠습니까?", "returnToMain")) return;
}


/**
 * 입력항목 확인
 */
function fn_validation(){

    $(".input-warning").hide();
    //아이디 중복 확인여부 확인
    if(!mberIdDuplChkAt){
        $("#id_user").focus();
        $(".duplChkIdResult-warning").show();
        return false;
    }

    //비밀번호 입력 확인
    if(fn_empty($("#pwd").val()) ){
        $("#pwd").next(".input-util").find(".input-warning").empty();
        $("#pwd").next(".input-util").find(".input-warning").append("비밀번호를 입력해주세요.");
        $("#pwd").next(".input-util").find(".input-warning").show();
        $("#pwd").focus();
        return false;

    }else if(fn_empty($("#pwd_chk").val())){
        $("#pwd_chk").next(".input-util").find(".input-warning").empty();
        $("#pwd_chk").next(".input-util").find(".input-warning").append("비밀번호가 일치하지 않습니다.");
        $("#pwd_chk").next(".input-util").find(".input-warning").show();
        $("#pwd_chk").focus();
        return false;
    }else{

        //비밀번호 입력 확인
        if($("#pwd").val() != $("#pwd_chk").val()){
            $(".pswrdChkAt").show();
            $("#pwd_chk").focus();
            return false;
        }
        //비밀번호 형식 확인
        var regExp = fn_regExp("password");
        if ( !regExp.test($("#pwd").val()) ) {
            $("#pwd").next(".input-util").find(".input-warning").empty();
            $("#pwd").next(".input-util").find(".input-warning").append("비밀번호 형식을 확인해주세요.");
            $("#pwd").next(".input-util").find(".input-warning").show();
            $("#pwd").focus();
            return false;
        }
    }
    //이메일 형식 확인
    if(fn_empty($("#email").val())){
        $("#email_input_util").find(".input-warning").empty();
        $("#email_input_util").find(".input-warning").append("이메일 형식을 확인해주세요.");
        $("#email_input_util").find(".input-warning").show();
        $("#email").focus();
        return false;
    }else{
        var regExp = fn_regExp("email");
        if ( !regExp.test($("#email").val()) ) {
            $("#email_input_util").find(".input-warning").empty();
            $("#email_input_util").find(".input-warning").append("이메일 형식을 확인해주세요.");
            $("#email_input_util").find(".input-warning").show();
            $("#email").focus();
            return false;
        }
    }

    //휴대전화번호 형식 확인
    if(fn_empty($("#mbl_phone_num").val())){
		/*
        $("#mbl_phone_num ~ .input-util").find(".input-warning").empty();
        $("#mbl_phone_num ~ .input-util").find(".input-warning").append("휴대전화번호를 입력해주세요.");
        $("#mbl_phone_num ~ .input-util").find(".input-warning").show();
        $("#mbl_phone_num").focus();
        */
		
        $("#phone_input_util").find(".input-warning").empty();
        $("#phone_input_util").find(".input-warning").append("휴대전화번호를 입력해주세요.");
        $("#phone_input_util").find(".input-warning").show();
        $("#mbl_phone_num").focus();
		return false;
    }else{
        var regExp = fn_regExp("mbtl");
        if ( !regExp.test($("#mbl_phone_num").val()) ) {
			/*
            $("#mbl_phone_num ~ .input-util").find(".input-warning").empty();
            $("#mbl_phone_num ~ .input-util").find(".input-warning").append("휴대전화번호 형식을 확인해주세요.");
            $("#mbl_phone_num ~ .input-util").find(".input-warning").show();
            $("#mbl_phone_num").focus();
			*/
			$("#phone_input_util").find(".input-warning").empty();
            $("#phone_input_util").find(".input-warning").append("휴대전화번호 형식을 확인해주세요.");
            $("#phone_input_util").find(".input-warning").show();
            $("#mbl_phone_num").focus();
      		return false;
        }
    }

    return true;
}

/**
 * 작성 완료 버튼 클릭
 */
function fn_save(){

    $(".input-warning").hide();
    $(".input-success").hide();

//    $("#confirm_func_nm").val("save_start");
    
    //입력항목 확인
    if(!fn_validation()) return;

     
    if(!confirmOpen("회원 가입을 완료하시겠습니까?","save_start")) return;

}
function save_start(){
    $.ajax({
        type:"POST",
        url: contextPath+"/member/insertUserAjx.do",
        data : $("#frm").serialize(),
        dataType : "json",
        success: function(e){
            if(e.ret==0){

                alertOpen("회원 가입을 완료하였습니다.");

                $("#frm").attr("onSubmit","");
                $("#frm").attr("action", contextPath+"/member/joinCompleteView.do").submit();

            }else{
                alertOpen("회원 가입 처리 중 오류가 발생하였습니다.");
            }
        },
        error: function(xhr, status, error) {
            alertOpen("회원 가입 처리 중 오류가 발생하였습니다.");
        }
    });
	
}
//id 값 체크(중복+ 정규식)
function fn_checkMberId(){
    var status = true;
    //id값 형식확인
    var idValidation = fn_validationMberId();
    //id값 중복확인
    if(idValidation && !mberIdDuplChkAt){
        $("#id_user").focus();
        $(".duplChkIdResult-warning").show();
        $(".id-input-warning").hide();
        $(".duplChkIdResult").hide();
        status = false;
    }
    return status;
}

function fn_pswrdCheck(){
    var status = true;

    if(status && $("#pwd").val() == ""){
        $("#pwd").next(".input-util").find(".input-warning").empty();
        $("#pwd").next(".input-util").find(".input-warning").append("비밀번호를 입력해주세요.");
        $("#pwd").next(".input-util").find(".input-warning").show();
        $("#pwd").focus();
        status = false;
    }

    //비밀번호 형식 확인
    var regExp = fn_regExp("password");
    if ( !regExp.test($("#pwd").val()) ) {
        $("#pwd").next(".input-util").find(".input-warning").empty();
        $("#pwd").next(".input-util").find(".input-warning").append("비밀번호 형식을 확인해주세요.");
        $("#pwd").next(".input-util").find(".input-warning").show();
        //$("#pwd").focus();
        status = false;
    }else{
        $("#pwd").next(".input-util").find(".input-warning").hide();
    }

    //비밀번호와 비밀번호확인 일치 확인
    if(status && $("#pwd").val() != $("#pwd_chk").val()){
        $(".pswrdChkAt").show();
        $("#pwd_chk").focus();
        status = false;
    }else{
        $(".pswrdChkAt").hide();
        $("#pwd_chk").focusout();
    }
    return status;
}

function fn_pswrdmatchCheck(){
    var status = true;

    if(status && $("#pwd").val() == ""){
        $("#pwd").next(".input-util").find(".input-warning").empty();
        $("#pwd").next(".input-util").find(".input-warning").append("비밀번호를 입력해주세요.");
        $("#pwd").next(".input-util").find(".input-warning").show();
        $("#pwd").focus();
        status = false;
    }

    //비밀번호 형식 확인
    var regExp = fn_regExp("password");
    if ( !regExp.test($("#pwd").val()) ) {
        $("#pwd").next(".input-util").find(".input-warning").empty();
        $("#pwd").next(".input-util").find(".input-warning").append("비밀번호 형식을 확인해주세요.");
        $("#pwd").next(".input-util").find(".input-warning").show();
        $("#pwd").focus();
        status = false;
    }else{
        $("#pwd").next(".input-util").find(".input-warning").hide();
    }

    return status;
}