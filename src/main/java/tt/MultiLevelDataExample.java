package tt;

import java.util.ArrayList;
import java.util.List;

public class MultiLevelDataExample {
    public static void main(String[] args) {
        // 원본 데이터 생성
        List<CommandMap> originalData = new ArrayList<>();
        originalData.add(new CommandMap(1, "부모1"));
        originalData.add(new CommandMap(2, "자식1"));
        originalData.add(new CommandMap(3, "손자1"));
        originalData.add(new CommandMap(3, "손자2"));
        originalData.add(new CommandMap(2, "자식2"));
        originalData.add(new CommandMap(1, "부모2"));
        originalData.add(new CommandMap(1, "부모3"));
        originalData.add(new CommandMap(1, "부모4"));
        originalData.add(new CommandMap(2, "자식4-1"));
        originalData.add(new CommandMap(2, "자식4-2"));

        // 부모-자식 관계를 유지하면서 데이터를 포함할 리스트 생성
        List<CommandMap> mergedData = new ArrayList<>();
        int currentDepth = 0; // 현재 depth 초기화

        for (CommandMap data : originalData) {
            if (data.getDepth() == 1) {
                currentDepth = 1;
                mergedData.add(data); // 부모 행 추가
            } else {
                data.setDepth(currentDepth + 1); // 현재 depth + 1로 설정
                mergedData.add(data); // 자식 행 추가
            }
        }

        // 결과 출력
        System.out.println("병합된 데이터:");
        for (CommandMap data : mergedData) {
            System.out.println("Depth: " + data.getDepth() + ", Value: " + data.getValue());
        }
    }
}

class CommandMap {
    private int depth;
    private String value;

    public CommandMap(int depth, String value) {
        this.depth = depth;
        this.value = value;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String getValue() {
        return value;
    }
}
