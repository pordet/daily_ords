<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="코드그룹 관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>코드그룹 관리</span>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
                  <input type="hidden" id="doubleYn" value="Y">
                  <input type="hidden" id="checkYn" value="N">
				  <input type="hidden" name="id_code_group" id="id_code_group" value="${param.id_code_group}">
				  <input type="hidden" name="bef_code_group" id="bef_code_group" value="${param.code_group}">
				  <input type="hidden" name="search_col" id="search_col" value="${param.search_col}">
				  <input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
				  <input type="hidden" name="page" id="page" value="${param.page}">
				  <input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
				  <input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>코드 그룹 코드</td>
									<td>
										<div class="single-row">
											<input type="text" id="code_group" name="code_group" value="${map.code_group}">
											<button type="button" class="btn-primary" onclick="javascript:checkDouble();" id="checkBtn"
												<c:if test="${param.insert_yn eq 'N'}"> disabled</c:if>>
												중복체크
											</button>
										</div>
									</td>
								</tr>
								<tr>
									<td>코드그룹명</td>
									<td>
										<input type="text"  name="code_group_nm" id="code_group_nm" value="${map.code_group_nm}">
									</td>
								</tr>
								<tr>
									<td>코드그룹 설명</td>
									<td>
										<input type="text"  name="code_group_desc" id="code_group_desc" value="${map.code_group_desc}">
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="ra1" ${((empty map.use_yn || map.use_yn eq 'Y')?'checked':'')}/>
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="ra2" ${(map.use_yn ne 'Y'?'checked':'')}/>
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<c:if test="${!is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" id="btnInit">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete">
									삭제
								</button>
								<button type="button" class="btn-primary" onclick="javascript:register();">
									저장
								</button>
							</div>
						</c:if>
						<c:if test="${is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
						</c:if>
					</div>
				</form>
			</div>
		</div>
	</div>


</adm:AdmBody>
<script>
$(function () {
	$("#btnDelete").click(function(){
		confirmOpen('해당 코드그룹을 삭제하시면 코드그룹에 포함된 코드들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteData");
	});
	$("#btnInit").click(function(){
		initForm();
	});
});
function initForm(){
	$("#insert_yn").val('Y');
	$("#doubleYn").val('Y');
	$("#checkYn").val('N');
	$("#id_code_group").val('');
	$("#code_group").val('');
	$("#code_group_nm").val('');
	$("#code_group_desc").val('');
    $("#ra1").attr("checked",false);
    $("#ra2").attr("checked",true);
}
function checkDouble(){
// 	var insert_yn = $("#insert_yn").val();
// 	if(insert_yn =='Y'){
		$("#doubleYn").val('Y');
		var code_group = $("#code_group").val();
		processing();
		$.ajax({
			url: contextPath+"/adm/code/codeGroupCheckAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: {code_group : code_group},                // HTTP 요청과 함께 서버로 보낼 데이터
	//	    data: {DocumentNumber : docNum , KeyPhrase : 'JDZytH@qDQJ^B!5wNWThYV7vkS&t*gyzHP8@s#4t^6AeM+&f_!V!&hRWySLUC-_HdrE8WDvhVm6HR&FWTyGk9#yHQP4L86yMF*eW3w*NLCbFP%wQyAv+m8q&?Ge*x@A$' },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "POST",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	endProcessing();
		    	if(e.map){
		    		alertOpen('기존에 사용중인 ID입니다.');
		    	}else{
		    		alertOpen('사용 가능한 ID입니다.');
		    		$("#checkYn").val('Y');
		    		$("#doubleYn").val('N');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
// 	}	
}
function register(){
	if($.trim($("#code_group").val())==''){
		alertOpen("코드 그룹 코드을 입력하십시오.");
		return;
	}
	if($.trim($("#code_group_nm").val())==''){
		alertOpen("코드 그룹명을 입력하십시오.");
		return;
	}
	var insert_yn = $("#insert_yn").val();
	var isNeedCheck = false;
	if(insert_yn=='Y'){
		if($("#doubleYn").val()!='N'){
			isNeedCheck = true;
		}
	}else{
		if($("#checkYn").val()=='N'){
			if($("#bef_code_group").val()!=$("#code_group").val()){
				isNeedCheck = true;
			}
		}
	}
	if(isNeedCheck){
	    alertOpen('중복체크가 필요합니다.');
	    return;
	}
	var params = jQuery("#frm").serialize();
	var url = '';
	if(insert_yn =='Y'){
		url = contextPath+"/adm/code/insertCodeGroupAjax.do";
	}else{
		url = contextPath+"/adm/code/updateCodeGroupAjax.do";
	}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		if(insert_yn =='Y'){
		    		$("#id_code_group").val(e.id_code_group);
		    		$("#insert_yn").val('N');
	    		}
	    		$("#bef_code_group").val($("#code_group").val());
	    		$("#checkYn").val('N');
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
function doList(){
	var page = $("#page").val();
	var frm =document.frm;
		frm.action=contextPath+"/adm/code/codeGroupList.do?id_menu=${param.id_menu}";
	if(page!="")
		frm.page.value=page;
	else frm.page.value=1;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
}
function confirmDeleteData(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/code/deleteCodeGroupAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}
</script>
</html>
