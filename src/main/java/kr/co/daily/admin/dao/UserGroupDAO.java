package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface UserGroupDAO {
	
	PageList<CustomMap> selectUserGroupList(Map<String, Object> map, PageBounds pageBounds);

	List<CustomMap> selectGroupList(Map<String, Object> map);
	
	void insertUsrGroup(Map<String, Object> map);

	void modifyUsrGroup(Map<String, Object> map);

	void deleteUsrGroup(Map<String, Object> map);

	void deleteAllUsrInUserGroup(Map<String, Object> map);

	CustomMap getUserGroup(Map<String, Object> map);

	CustomMap getUserDefaultMenuByIdUser(Map<String, Object> map);

	CustomMap getMainMenuUrlByIdUser(String id_user);

}
