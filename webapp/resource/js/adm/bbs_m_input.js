var cur_keyword_num=1;
function registerData(){
	$("#modi_order_key").val($("modi_order_key").val());
	$("#modi_idx").val($("modi_idx").val());
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/getBoardPathAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
		    	alertOpen('게시판 정보 조회시 오류가 발생했습니다.');
	    	}else{
	    		if(nanToStr(e.child_ord_str)==''){
	    			$("#modi_sel_ord_str").val(e.parent_ord_str);
	    		}else{
	    			$("#modi_sel_ord_str").val(e.child_ord_str);
	    		}
    			var html ='';
	    		$.each(e.pList,function(i,d){
	    			if(e.parent_ord_str ==d.ord_str){
		    			html+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_master").html(html);
    			var html1 ='<option value="">선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			if(nanToStr(e.child_ord_str ==d.ord_str)){
		    			html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_child").html(html1);	
	    		$("#board_reg_type").html(e.board_path+" 등록");
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
	regBoardInit();
// 	var frm =document.frm;
// 	frm.action=contextPath+"/regHistory.do";
// //	frm.action="/excelDownload.do";
// 	$("#frm").submit();
}
function changeRegChildBoard(that){
	if($(that).val()==''){
		$("#modi_sel_ord_str").val($("#modi_sel_master option:selected").val());
	}else{
		$("#modi_sel_ord_str").val($(that).val());
	}
	changeRegTitle('Y');
}
function changeRegTitle(has_parent){
	var reg_title = '';
	if($("#modi_sel_master option:selected").text()!=''){
		reg_title=$("#modi_sel_master option:selected").text();
	}
	if(nanToStr(has_parent)!=''){
		if($("#modi_sel_child").val()!=''){
			reg_title+='>>'+$("#modi_sel_child option:selected").text();
		}
	}
	if($("#modi_id").val()!=''){
		reg_title+=' 조회/수정';
	}else{
		reg_title+=' 입력';
	}
	$("#board_reg_type").html(reg_title);
}
function changeRegBoard(that){
	var sel_ord_str = $("#modi_sel_master option:selected").val();
	$("#modi_sel_ord_str").val(sel_ord_str);
	$.ajax({
		url: contextPath+"/changeBoardRootAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: {sel_ord_str:$(that).val()},                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	changeRegTitle();
	    	if(e.ret==1){
		    	alertOpen('게시판 정보 조회시 오류가 발생했습니다.');
	    	}else{
    			var html1 ='<option value="" selected>선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    		});
	    		$("#modi_sel_child").html(html1);
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	});
	
}
function regBoardInit(){
	$("#modi_id").val('');
	$("#modi_title").val('');
	$("#modi_comment").val('');
	$("#modi_keyword").val('');
//    var khtml = '<div class="col-8 line" id="addKeyword_in_div_1">';
//	    khtml+='	<div class="form-group">';
//	    khtml+='		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_1" value="">';
//	    khtml+='	</div>';
//	    khtml+='</div>';
//	    khtml+='<div class="col-4 line" id="delKeyword_in_div_1" style="text-align:right;">';
//	    khtml+='	<a href="javascript:void(0);" class="ico_delete" onclick="javascript:delKeyword(1);"><img src="/traf/resource/img/ico_delete.png" alt="delete"></a>';
//	    khtml+='</div>';
	$("[id^=addKeyword_in_div_]").remove();
	$("[id^=delKeyword_in_div_]").remove();
	$("[id^=keyword_]").remove();
	var html = '';
	html+= '<div class="col-8 line" id="addKeyword_in_div_1">';
	html+= '	<div class="form-group">';
	html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_1" value="">';
	html+= '	</div>';
	html+= '</div>';
	html+= '<div class="col-4 line" id="delKeyword_in_div_1" style="text-align:right;">';
	html+= '	<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delKeyword(1);"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
	html+= '</div>';
	html+= '';
	$("#key_btn_group").after(html);
	$("[id^=addFile_in_div_]").remove();
	$("[id^=delFile_in_div_]").remove();
	$("[id^=attach_]").remove();
	addFile();
	changeRegTitle('Y');
	$("#dataModal").modal('show');		
}

function addKeyword(){
	var bef_keyword_num = cur_keyword_num;
    cur_keyword_num=~~cur_keyword_num + 1;
    var keyword_num = cur_keyword_num;
	var html = '<div class="col-8 line" id="addKeyword_in_div_'+keyword_num+'">';
		html+= '	<div class="form-group">';
		html+= '		<input type="text" class="form-control mh-form-control" name="keyword" id="keyword_'+keyword_num+'" value="">';
		html+= '	</div>';
		html+= '</div>';
		html+= '<div class="col-4 line" id="delKeyword_in_div_'+keyword_num+'" style="text-align:right;">';
		html+= '	<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delKeyword('+keyword_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png" alt="delete"/></a>';
		html+= '</div>';
	var fileCnt = $("[id^=addKeyword_in_div_]").length;
	if(fileCnt==0){
		$("#key_btn_group").after(html);
	}else{
		$("#delKeyword_in_div_"+bef_keyword_num).after(html);
	}
}

function viewData(id_master,id,ord_str){
	$("#modi_order_key").val($("order_key").val());
	$("#modi_idx").val($("idx").val());
	
	$("#modi_id").val(id);
	$("#modi_id_master").val(id_master);
	$("#modi_sel_ord_str").val(ord_str);
	$("#modi_id").val(id);
	$("[id^=addKeyword_in_div_]").remove();
	$("[id^=delKeyword_in_div_]").remove();
	$("[id^=keyword_]").remove();
	$("[id^=attach_]").remove();
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/getBoardAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret==1){
		    	alertOpen('키워드 목록 조회시 오류가 발생했습니다.');
	    	}else{
    			var html ='';
	    		$.each(e.pList,function(i,d){
	    			var id_parent = '';
	    			if(nanToStr(e.has_id_parent)==''){
	    				id_parent = e.master.id_parent;
	    			}else{
//	    				id_parent = e.master.id_parent;
	    				id_parent = e.master.id_master;
	    			}
	    			if(id_parent ==d.id_master){
		    			html+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    		$("#modi_sel_master").html(html);
    			var html1 ='<option value="">선택없음</option>';
	    		$.each(e.childList,function(i,d){
	    			if(nanToStr(e.master.id_master ==d.id_master)){
		    			html1+='<option value="'+d.ord_str+'" selected>'+d.board_nm+'</option>';
	    			}else{
		    			html1+='<option value="'+d.ord_str+'">'+d.board_nm+'</option>';
	    			}
	    		});
	    	   $("#modi_sel_child").html(html1);
	     	   loadData(e,true,false);
	     	   $("#modi_title").val(nanToStr(e.map.title));
	     	   $("#modi_comment").val(nanToStr(e.map.comment));
	     		$("#modi_keyword").val(nanToStr(e.map.keyword));
	   		   $("#btnDelete").show();
	   		   addKeyword();
//	   		$('#modal_body').scrollTop(0);
	     	   $("#dataModal").modal('show');	
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}

function search(){
	if(nanToStr($(".seled").attr("id"))!=''){
		$("#"+$(".seled").attr("id")).removeClass("seled");
	}
	$("#modi_sel_ord_str").val('');
	$("#bef_search_keyword").val($("#search_keyword").val());
	$("#is_search").val('Y');
	selInit();
	doList(1,false);
	if($.trim($("#search_keyword").val())!=''){
		$("#search_init").show();
	}
}
function search_keyword_init(){
	$("#modi_sel_ord_str").val('');
	$("#search_keyword").val('');
	$("#search_init").hide();
}
function doList(page,isTreeClicked){
		$("#page").val(page);
		$("#modi_order_key").val($("#order_key").val());
		$("#modi_idx").val($("#idx").val());
		$("#modi_page").val(page);
		if($("#is_search").val()=='N'){
			$("#search_keyword").val('');
		}
		var params = $("#modi_frm").serialize();
		var url = contextPath+"/historyAjax.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    dataType: "json" ,
		    success : function(e){
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		list_redraw(e);
		    		draw_paging_by_ajax(e.paginator);
		    		if(nanToStr(isTreeClicked)!='' && !isTreeClicked){
	   			    	infoOpen('성공하였습니다.');
		    		}
		    	}
		    },
		    error : function(e){
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
}
var cur_file_num=1;
function addFile(){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
	var html ='<div class="col-8 line" id="addFile_in_div_'+file_num+'">';
	html+='		<div class="form-group inline-group">';
	html+='		   <input type="text" id="fileName'+file_num+'" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>';
	html+='		   <div class="attach-btn">';
	html+='			<button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>';
	html+='		    <input type="file" name="file_'+file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+file_num+'\').value = this.value"/>';
	html+='		   </div>';
	html+='		</div>';
	html+='		</div>';
	html+='		   <div class="col-4 line" id="delFile_in_div_'+file_num+'" style="text-align:right;">';
	html+='			<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile('+file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png'+'" alt="delete"/></a>';
	html+='		  </div>';
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==0){
		$("#file_btn_group").after(html);
	}else{
		$("#delFile_in_div_"+bef_file_num).after(html);
	}
}

