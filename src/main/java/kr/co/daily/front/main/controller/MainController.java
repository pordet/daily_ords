package kr.co.daily.front.main.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.UserAuthUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.main.service.MainService;
import kr.co.daily.front.service.BbsService;

@Controller
public class MainController extends AbstractControllerBase {
	private Logger log = LoggerFactory.getLogger(MainController.class);

	@Autowired MainService mainService;
	@Autowired BbsService bbsService;
	
	@RequestMapping("/main.do")
	public String main(CommandMap commandMap, Model model) throws Exception {
		CommandMap map = new CommandMap();
		map.put("id_meta_m", 1);
		map.put("id_view", 1);
		List<CustomMap> infoHeaderList = new ArrayList<>();
		List<CustomMap> infoMakeOrdList = new ArrayList<>();
		List<CustomMap> infoMakeList = new ArrayList<>();
		
		List<CustomMap> notiHeaderList = new ArrayList<>();
		List<CustomMap> notiMakeOrdList = new ArrayList<>();
		List<CustomMap> notiMakeList = new ArrayList<>();

		CustomMap boardNoMap = new CustomMap();
		boardNoMap.put("meta_nm", "번호");
		boardNoMap.put("meta_depth", "1");
		boardNoMap.put("meta_cd", "row_num");
		boardNoMap.put("ord", "0");
		boardNoMap.put("typ", GlobalConst.DT_NUM);
		boardNoMap.put("decimal_len", "10");
		boardNoMap.put("precision_len", "0");

		CustomMap updViewCntMap = new CustomMap();
		updViewCntMap.put("meta_nm", "조회수");
		updViewCntMap.put("meta_depth", "1");
		updViewCntMap.put("meta_cd", "view_cnt");
		updViewCntMap.put("ord", "100");
		updViewCntMap.put("typ", GlobalConst.DT_NUM);
		updViewCntMap.put("decimal_len", "10");
		updViewCntMap.put("precision_len", "0");
		
		infoHeaderList.add(boardNoMap);
		notiHeaderList.add(boardNoMap);
		
		CommandMap infoParam = new CommandMap();
		infoParam.put("id_meta_m", 1);
		infoParam.put("rows", 6);
		infoParam.put("short_yn", "Y");

		infoMakeList.add(boardNoMap);
		
		infoMakeOrdList.add(updViewCntMap);
		infoMakeList.addAll(infoMakeOrdList);

		List<CustomMap> infoHeadList = bbsService.selectBbsHeaderList(infoParam);
		infoHeaderList.addAll(infoHeadList);
		infoHeaderList.addAll(infoMakeOrdList);
		model.addAttribute("infoHeaderList", infoHeaderList);
		
		PageList<CustomMap> infoList = bbsService.selectBbsList(infoParam,infoMakeList);
		model.addAttribute("infoList", infoList);
		
		
		CommandMap notiParam = new CommandMap();
		notiParam.put("id_meta_m", 2);
		notiParam.put("rows", 6);
		notiParam.put("short_yn", "Y");
		
		notiMakeList.add(boardNoMap);
		
		notiMakeOrdList.add(updViewCntMap);
		notiMakeList.addAll(notiMakeOrdList);

		List<CustomMap> notiHeadList = bbsService.selectBbsHeaderList(notiParam);
		notiHeaderList.addAll(notiHeadList);
		notiHeaderList.addAll(notiMakeOrdList);
		model.addAttribute("notiHeaderList", notiHeaderList);
		
		PageList<CustomMap> notiList = bbsService.selectBbsList(notiParam,notiMakeList);
		model.addAttribute("notiList", notiList);
		
		return "front/main/main";
	}
	
	@RequestMapping("/main1.do")
	public String main_1(CommandMap commandMap, Model model) throws Exception {
		
		return "front/main/main1";
	}
	
	@RequestMapping("/search.do")
	public String boardSearch(CommandMap commandMap, ModelMap model) throws Exception {
		return "front/main/search_list";
	}
	
@RequestMapping("/redrawContentAjax.do")
	public String redrawContentAjax(CommandMap commandMap, ModelMap model) throws Exception {
		return "jsonView";
	}
	@RequestMapping("/content.do")
	public String content(CommandMap commandMap, ModelMap model) throws Exception {
		log.info("받은 값");
		log.info(commandMap.toString());
		/*
		if(!commandMap.getString("is_search").equals("Y")) {
			commandMap.put("is_search", "N");
		}
		*/

		boolean isR = UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH);
		log.info("권한");
		log.info(""+isR);
		return "front/main/content";
	}
	

}
