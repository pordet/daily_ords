<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<his:DataHead title="">
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_register.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/history/history_input.js'/>"></script>
</his:DataHead>
<his:DataTreeBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" hasLeft="true" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
                 <form id="frm" name="frm" method="post" style="margin-top: -80px;">
<%--                  <input type="hidden" name="id_category" value="${(not empty param.id_category?param.id_category:1)}"> --%>
	                 <input type="hidden" name="id_master" value="${master.id_master}">
	                 <input type="hidden" name="id_board_master" id="id_board_master" value="${master.id_board_master}">
	                 <input type="hidden" name="id_board" id="id_board">
	                 <input type="hidden" name="attach_yn" value="${master.attach_yn}">
	                 <input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
	                 <input type="hidden" name="ord_str" value="${master.ord_str}">
	                 <input type="hidden" name="sel_ord_str" id="sel_ord_str">
                    	<div class="row">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
		                              <div class="form-group inline-group" style="width: 300px">
		                                  <div class="tit_cont">검색어</div>
		                                  <input type="text" class="form-control mh-form-control" name="search_keyword" id="search_keyword" value="${(not empty param.search_keyword ?param.search_keyword:'')}">
		                              </div>
	                              	<button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
		                       		<div class="fr">
		                              <button type="button" class="btn h34" onclick="javascript:registerData();"  style="margin-top:20px;">등록</button>
		                            </div>
	                            </div>
	                    </div>
	                </div>
	                       <div class="row">
								<div class="col-12">
				                    <div class="grid-area">
				                        <!-- 그리드 해더영역 -->
				                        <div class="grid-header">
<!-- 											<div class="header-unit header-check" style="width:5%;"> -->
<!-- 												<a href=""> -->
<!-- 													<div class="form-check mh-form-checkbox"> -->
<!-- 														<input class="form-check-input" type="checkbox" value="" id="defaultCheck1"/> -->
<!-- 														<span class="mh-check"></span> -->
<!-- 														<label class="form-check-label" for="defaultCheck1" title=""></label> -->
<!-- 													</div> -->
<!-- 												</a> -->
<!-- 											</div>각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:10%;text-align: center;"><a href="">순서<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:10%;text-align: center;"><a href="">자료실명<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
				                            <div class="header-unit1" style="width:40%;text-align: center;"><a href="">제목<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
				                            <div class="header-unit1" style="width:10%;text-align: center;"><a href="">첨부파일수<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
				                            <div class="header-unit1" style="width:10%;text-align: center;"><a href="">조회수<ctrl:orderMark val="5" sidx="${param.sidx}" sord="${param.sord}"/></a></div><!--a테그 안에 span은 기본, span.sorting_asc 위로, 네무.sorting_desc 아래로 -->
				                            <div class="header-unit1" style="width:20%;text-align: center;"><a href="">등록일<ctrl:orderMark val="6" sidx="${param.sidx}" sord="${param.sord}"/></a></div>
				                        </div>
				                        <!-- //그리드 해더영역 -->
				                        <!-- 그리드 컨텐츠영역 -->
				                        <div class="grid-contents1">
				                            <!--loof-->
				                            <c:forEach var="map" items="${list}" varStatus="i">
<%-- 				                             --%>
				                            <div class="grid-loof-cont">
				                            <a href="javascript:void(0)" ondblclick="javascript:viewData(${map.id_board_master},${map.id_board});">
				                                <div class="cont-unit" style="width:10%;text-align: center;">${map.rownum}</div>
				                                <div class="cont-unit" style="width:10%">${map.board_nm}</div>
				                                <div class="cont-unit" style="width:40%">${map.title}</div>
				                                <div class="cont-unit" style="width:10%;text-align: center;">${map.file_cnt}</div>
				                                <div class="cont-unit" style="width:10%;text-align: center;">${map.view_cnt}</div>
				                                <div class="cont-unit" style="width:20%;text-align: center;">${map.reg_date}</div>
				                            </a>
				                            </div>
<!-- 				                             -->
				                            </c:forEach>
				                            <!--//loof-->
				                        </div>
				                        <!-- //그리드 컨텐츠영역 -->
				                    </div>
				                    <!--//그리드영역-->
				                    </div>
				                    <!-- //그리드 컨텐츠영역 -->
				                </div>
				                    <!-- paging -->
									<pageList:ajax_page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
									<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
									<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
									<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
									<input type="hidden" name="page" id="page" value="${param.page}"/>
				                    <!-- //paging -->
			</form>
  <div class="modal fade mh-modal type05" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModal" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="board_reg_type">등록</h3>
				<button type="button" class="popup-close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<form name="modi_frm" id="modi_frm">
			<input type="hidden" id="modi_id_master" name="id_master" value="${(not empty param.id_master?param.id_master:1)}"/>
			<input type="hidden" id="modi_id_board" name="id_board" value=""/>
			<input type="hidden" id="modi_id_board_master" name="id_board_master"/>
			<input type="hidden" id="modi_sel_ord_str" name="sel_ord_str"/>
			<input type="hidden" id="modi_page" name="page"/>
			<input type="hidden" id="modi_search_keyword" name="search_keyword"/>
			<div class="modal-body">
                      <div class="row">
                          <div class="col-12">
                          <div class="tit_cont">제목</div>
                            <div class="form-group">
                              <input type="text" class="form-control mh-form-control" name="title" id="modi_title" value="${map.title}">
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                          <div class="tit_cont">내용</div>
                            <div class="form-group">
								<textarea class="form-control mh-form-control" name="comment" id="modi_comment" rows="6" style="height:240px;">${map.comment}</textarea>
                            </div>
                          </div>
                      </div>
                      <div class="row">
						 <h3 class="h3_tit col-12" id="key_btn_group">
							키워드 목록
							<a href="javascript:void(0);" class="btn_add" onClick="javascript:addKeyword();"><img src="<c:url value='/resource/img/ico_add.png'/>" alt=""/>추가</a>
						 </h3>
                      </div>
                <div class="row" id="file_list_div">
					 <h3 class="h3_tit col-12" id="file_btn_group">
						첨부파일 리스트
						<a href="javascript:void(0);" class="btn_add" onClick="javascript:addFile();"><img src="<c:url value='/resource/img/ico_add.png'/>" alt=""/>추가</a>
					 </h3>
                    <div class="col-8 line" id="addFile_in_div_1">
                        <div class="form-group inline-group">
                            <input type="text" id="fileName1" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>
                            <div class="attach-btn">
                              <button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>
                              <input type="file" name="file_1" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value"/>
                            </div>
                        </div>
                    </div>
					<div class="col-4 line" id="delFile_in_div_1" style="text-align:right;">
	                             <a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile(1);"><img src="<c:url value='/resource/img/ico_delete.png'/>" alt="delete"/></a>
	                 </div>
                </div>
            <div class="btn-area mb-0p">
                <button type="button" class="btn back" id="btnDelete" onclick="delData();" style="display:${(not empty map.id_board?'block':'none')};">삭제</button>
                <button type="button" class="btn" id="btnSave" onclick="register();">저장</button>
                <button type="button" class="btn list" id="btnList" onclick="close_reg();">닫기</button>
            </div>	
			</div>
			</form>
		</div>
	</div>
  </div>
</his:DataTreeBody>
<script>
$('#dataModal').on('hidden.bs.modal', function () {
	close_reg();
});
function close_reg(){
	$("#dataModal").modal('hide');	
}
var cur_file_num=1;
function addFile(){
	var bef_file_num = cur_file_num;
    cur_file_num=~~cur_file_num + 1;
    var file_num = cur_file_num;
	var html ='<div class="col-8 line" id="addFile_in_div_'+file_num+'">';
	html+='		<div class="form-group inline-group">';
	html+='		   <input type="text" id="fileName'+file_num+'" data-add="1" class="form-control mh-form-control" style="height:40px;" disabled>';
	html+='		   <div class="attach-btn">';
	html+='			<button type="button" class="btn attach1"><span class="btn_attach"></span>첨부</button>';
	html+='		    <input type="file" name="file_'+file_num+'" class="file_input_hidden" onchange="javascript: document.getElementById(\'fileName'+file_num+'\').value = this.value"/>';
	html+='		   </div>';
	html+='		</div>';
	html+='		</div>';
	html+='		   <div class="col-4 line" id="delFile_in_div_'+file_num+'" style="text-align:right;">';
	html+='			<a href="javascript:void(0);" class="ico_delete" onClick="javascript:delFile('+file_num+');"><img src="'+contextPath+'/resource/img/ico_delete.png'+'" alt="delete"/></a>';
	html+='		  </div>';
	var fileCnt = $("[id^=addFile_in_div_]").length;
	if(fileCnt==0){
		$("#file_btn_group").after(html);
	}else{
		$("#delFile_in_div_"+bef_file_num).after(html);
	}
}

var cur_keyword_num=1;
var attach_idx=0;
function delAttach(idx){
	attach_idx=idx;
	confirmOpen('삭제시 복구할 수 없습니다.',delAttachFile);
}
function delAttachFile(id_board_file){
	var params = {id_board_file : id_board_file};
	processing();
	$.ajax({
		url: contextPath+"/deleteHistoryFileAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret == '${GlobalConst.ACT_SUCCESS}'){
	     	   $("#attach_"+id_board_file).remove();
	    	   $("#attach_"+id_board_file+"_add").remove();
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}
function delAttach(idx){
	   $("#addFile_in_div_"+idx).remove();
	   $("#delFile_in_div_"+idx).remove();
}
var keyword_idx=0;
function delKeyword(idx){
	keyword_idx=idx;
	if($("#keyword_"+idx).val()==''){
		delKeywordAttach(idx);
	}else{
		confirmOpen('삭제시 복구할 수 없습니다.',delKeywordNum);
	}
}
function delKeywordNum(id_keyword){
	var params = {id_board_keyword : id_keyword};
	processing();
	$.ajax({
		url: contextPath+"/history/deleteKeywordAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret == '${GlobalConst.ACT_SUCCESS}'){
	     	   $("#keyword_"+id_keyword).remove();
	    	   $("#keyword_"+id_keyword+"_add").remove();
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}
function delKeywordAttach(idx){
	   $("#addKeyword_in_div_"+idx).remove();
	   $("#delKeyword_in_div_"+idx).remove();
}

$(function() {
	$(".grid-loof-cont").mouseover(function(e){
 		$(this).find(".cont-unit").css("background-color","rgb(211,234,255)");
 		e.preventDefault();
 	});	
 	$(".grid-loof-cont").mouseout(function(e){
 		$(this).find(".cont-unit").css("background-color","white");
 		e.preventDefault();
 	});	
});

function doList(page,isTreeClicked){
   		$("#page").val(page);
   		var params = $("#frm").serialize();
   		var url = contextPath+"/historyAjax.do";
   		processing();
   		$.ajax({
   			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
   		    type: "POST", // HTTP 요청 방식(GET, POST)
   		    data : params,
   		    dataType: "json" ,
   		    success : function(e){
   		    	endProcessing();
   		    	if(e.ret==1){
   		    		alertOpen('실패하였습니다.');
   		    	}else{
   		    		$(".grid-contents1").empty();
   		    		var html='';
   		    		$.each(e.list,function(i,d){
   		    			html+='<div class="grid-loof-cont">';
   		    			html+='<a href="javascript:void(0)" ondblclick="javascript:viewData('+d.id_board_master+','+d.id_board+');">';
   		    			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.rownum+'</div>';
   		    			html+='<div class="cont-unit" style="width:10%">'+nanToStr(d.board_nm)+'</div>';
   		    			html+='<div class="cont-unit" style="width:40%">'+nanToStr(d.title)+'</div>';
   		    			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.file_cnt+'</div>';
   		    			html+='<div class="cont-unit" style="width:10%;text-align: center;">'+d.view_cnt+'</div>';
   		    			html+='<div class="cont-unit" style="width:20%;text-align: center;">'+nanToStr(d.reg_date)+'</div>';
   		    			html+='</a>';
   		    			html+='</div>';
   		    		});
   		    		$(".grid-contents1").html(html);
   		    		draw_paging_by_ajax(e.paginator);
   		    		if(nanToStr(isTreeClicked)!='' && !isTreeClicked){
   	   			    	infoOpen('성공하였습니다.');
   		    		}
   		    	}
   		    },
   		    error : function(e){
   		    	endProcessing();
   		    	if(e.status=='403'){
   			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
   		    	}else if(e.status=='401'){
   				    alertOpen('접근 권한이 없습니다.');
   		    	}else{
   			    	alertOpen('실패하였습니다.');
   		    	}
   		    }
   		});
}
function search(){
	$("#sel_ord_str").val('');
	var frm =document.frm;
	frm.action=contextPath+"/history.do";
	$("#page").val(1);
//	frm.action="/excelDownload.do";
	$("#frm").submit();
}
</script>
</html>