<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="daily.common.util.SpringPropertiesUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<!DOCTYPE html>
<html>
<front:FrontDynamicHead title="">
	<!--
	<script charset="UTF-8" src="value='/resource/js/cmm/ui_common.js'"></script>
	-->
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/member/member_find_input.js'/>"></script>
</front:FrontDynamicHead>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer w70">
			<div class="page-title-wrap">
				<span>아이디/비밀번호 찾기</span>
			</div>
			<div class="find-page-wrap">
				<div class="tab-btn-area tabBtnArea">
					<ul>
						<li class="active">
							<a class="tabBtn" href="#tabContent1">
								<span>아이디 찾기</span>
							</a>
						</li>
						<li class="">
							<a class="tabBtn" href="#tabContent2">
								<span>비밀번호 찾기</span>
							</a>
						</li>
					</ul>
				</div>
			
				<div class="tab-content-area tabContentArea">
					<div id="tabContent1" class="tab-content tabContent active">
						<form id="id_frm" name="id_frm" method="post" onsubmit="return false;">
							<div class="form-content-wrap">
								<div class="sub-description">
									<span>가입시 입력하신 이메일로 가입여부를 확인합니다.</span>
								</div>
								
								<div class="form-content-main">
									<table>
										<colgroup>
											<col width="20%">
											<col width="80%">
										</colgroup>
										<tbody>
											<tr class="main-row">
												<td>
													<span>이름</span>
												</td>
												<td>
													<input type="text" title="이름 입력" id="mberNm_id" name="user_nm" onkeyup="fn_inputKey('4', this);" maxlength="20">
												</td>
											</tr>
											<tr>
												<td></td>
												<td class="input-util">
													<div class="input-warning" style="display: none;">이름을 입력해주세요.</div>
												</td>
											</tr>
											<tr class="main-row">
												<td>
													<span>이메일</span>
												</td>
												<td>
													<input type="text" title="이메일 주소 입력" id="mberEmail_id" name="email" placeholder="sample@sample.com"
													 onkeyup="fn_inputKey('9', this);" onkeydown="javascript:if (event.keyCode == 13) { fn_search('id'); }" maxlength="40">
												</td>
											</tr>
											<tr>
												<td></td>
												<td class="input-util">
													<div class="input-warning" style="display: none;">이메일 주소를 다시 확인해주세요.</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</form>
						<div class="form-button-wrap">
							<button type="button" onclick="javascript:fn_search('id');" class="btn-primary">확인</button>
						</div>
	
						<div class="info-find-result result-area foundMberIdInfo" style="display: none;">
							<p class="reset result">회원님의 아이디는 <strong class="color-blue foundMberId"></strong>입니다.</p>
							<a href="<c:url value='/loginPage.do'/>" class="btn-link">로그인 페이지로 이동 <i class="iconset ico-arr-more big"></i></a>
						</div>
						
						<div class="info-find-result result-area doubleMberIdInfo" style="display: none;">
							<p class="reset result">동명이인으로 같은 이메일을 사용하는 유저가 있습니다.</p>
							<a href="<c:url value='/loginPage.do'/>" class="btn-link">로그인 페이지로 이동 <i class="iconset ico-arr-more big"></i></a>
						</div>
						<div class="info-find-result result-area notFoundMberIdInfo" style="display: none;">
							<p class="reset result">회원님의 가입정보가 없습니다.</p>
							<a href="<c:url value='/join.do'/>" class="btn-link">회원가입 페이지로 이동 <i class="iconset ico-arr-more big"></i></a>
						</div>
					</div>
					
					<div id="tabContent2" class="tab-content tabContent">
						<form id="pwd_frm" name="pwd_frm" method="post" onsubmit="return false;">
							<input type="hidden" name="pwd_reset" value="pw">
							<div class="form-content-wrap">
								<div class="sub-description">
									<span>가입시 입력한 아이디 및 이메일로 가입여부를 확인합니다.</span>
									<span>아이디가 기억나지 않는 경우 ‘아이디 찾기’를 먼저 진행해주시기 바랍니다.</span>
								</div>
								
								<div class="form-content-main">
									<table>
										<colgroup>
											<col width="20%">
											<col width="80%">
										</colgroup>
										<tbody>
											<tr class="main-row">
												<td>
													<span>이름</span>
												</td>
												<td>
													<input type="text" title="이름 입력" id="mberNm_pw" name="user_nm" onkeyup="fn_inputKey('4', this);" maxlength="20">
												</td>
											</tr>
											<tr>
												<td></td>
												<td class="input-util">
													<div class="input-warning" style="display: none;">이름을 입력해주세요.</div>
												</td>
											</tr>
											<tr class="main-row">
												<td>
													<span>아이디</span>
												</td>
												<td>
													<input type="text" title="아이디 입력" id="mberId_pw" name="id_user" class="input-text" style="ime-mode:disabled"
													 onkeyup="fn_inputKey('3', this);" maxlength="20">
												</td>
											</tr>
											<tr>
												<td></td>
												<td class="input-util">
													<div class="input-warning" style="display: none;">아이디를 입력해주세요.</div>
												</td>
											</tr>
											<tr class="main-row">
												<td>
													<span>이메일</span>
												</td>
												<td>
													<input type="text" title="이메일 주소 입력" id="mberEmail_pw" placeholder="sample@sample.com" name="email"
													 onkeyup="fn_inputKey('9', this);" onkeydown="javascript:if (event.keyCode == 13) { fn_search('pw'); }" maxlength="40">
												</td>
											</tr>
											<tr>
												<td></td>
												<td class="input-util">
													<div class="input-warning" style="display: none;">이메일 주소를 다시 확인해주세요.</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</form>
						<div class="form-button-wrap">
							<button type="button" onclick="javascript:fn_search('pw');" class="btn-primary">확인</button>
						</div>
	
						<div class="info-find-result result-area notFoundMberPwInfo" style="display: none;">
							<p class="reset result">회원님의 가입정보가 없습니다.</p>
							<a href="<c:url value='/join.do'/>';" class="btn-link">회원가입 페이지로 이동 <i class="iconset ico-arr-more big"></i></a>
						</div>
						
						<div class="info-find-result result-area foundMberPwInfo2" style="display: none;">
							<p class="reset result">E-mail로 임시 비밀번호가 전송되었습니다.<br>전송된 임시 비밀번호로 로그인 후 비밀번호를 재설정 해주시기 바랍니다.</p>
							<a href="<c:url value='/loginPage.do'/>" class="btn-link">로그인 페이지로 이동 <i class="iconset ico-arr-more big"></i></a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</front:FrontDynamicBody>
<script>
/*
$(function(){
	var hash = location.hash;
	if(!fn_empty(hash)){
		$(hash+"_a").click();
	}
});
*/
$(".tabBtn").on('click', function(e){
	e.preventDefault();
	var targetId = $(this).attr('href');
	var target = $("" + targetId);
	$(this).closest('li').addClass('active').siblings('li').removeClass('active');
	
	if(target.length > 0){
		$(""+targetId).closest('.tabContentArea').find('.tabContent').removeClass('active');
		$(""+targetId).addClass('active');
	}
});

</script>
</html>
