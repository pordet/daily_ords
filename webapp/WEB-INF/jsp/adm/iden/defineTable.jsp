<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="메타 화면 구성"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle} 관리">
<style>
#canvas-content {
    overflow: auto; /* 내부 컨텐츠에 스크롤 적용 */
    width: 100%; /* 내부 컨텐츠 너비 100% 설정 */
    height: 100%; /* 내부 컨텐츠 높이 100% 설정 */
    position:absolute;
    box-sizing: border-box; /* 패딩이 컨텐츠 크기에 포함되도록 설정 */
}
.hidden-table { 
    width:auto; 
    display:none;
}
.td_sel {
	color : white;
	background-color : rgba(8, 143, 220, 1);
}
.hidden-table tr td {
	border: 1px solid red;
    width: auto; /* 너비를 100px로 설정합니다. */
    height: 20px; /* 높이를 50px로 설정합니다. */
}
.hidden-table tr th {
	border: 1px solid blue;
    width: auto; /* 너비를 100px로 설정합니다. */
    height: 20px; /* 높이를 50px로 설정합니다. */
}
.th-split-container {
    display: flex;
    align-items: center;
    width: 100%;
}

.th-left-content {
    flex-grow: 1; /* 좌측 요소가 남은 공간을 모두 차지하도록 설정 */
    margin-right: 10px; /* 좌우 요소 사이의 간격 조정 */
}

.th-right-content {
    background-image: url('<c:url value='/resource/img/iden/iden-del-icon1.png'/>'); /* 이미지 경로 */
    background-repeat: no-repeat;
    background-size: contain; /* 이미지를 버튼 크기에 맞게 조절 */
    width: 18px; /* 버튼 너비 */
    height: 18px; /* 버튼 높이 */
    border: none; /* 테두리 없애기 */
    cursor: pointer; /* 마우스 커서 모양 변경 */
}
</style>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/iden/relation_input.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/iden/relation_register.js'/>"></script>
</adm:AdmHead>
	
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle} 관리</span>
			</div>
				<form name="frm" id="frm">
					<input type="hidden" name="search_col" id="list_search_col" value="${param.search_col}"/>
					<input type="hidden" name="search_keyword" id="list_search_keyword" value="${param.search_keyword}"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="id_rel_m" id="id_rel_m" value="${param.id_rel_m}"/>
					<input type="hidden" name="sidx" id="list_sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="list_sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="list_page" value="${param.page}"/>
				</form>
			
			<div class="page-content-wrap">
				<input type="hidden" name="insert_yn" id="insert_yn" value="Y"/>
				<div id="page-content-wrap-top">
					<c:set var="mListSize" value="${fn:length(list)}"/>
					<c:set var="mLowSize" value="${(mListSize/4)+1}"/>
					<c:set var="mLastCellIdx" value="${mListSize%4}"/>
					<c:set var="mCurrLowNum" value="1"/>
					<table>
						<colgroup>
							<col width="25%">
							<col width="25%">
							<col width="25%">
							<col width="25%">
						</colgroup>
						<tbody>
	                    <c:forEach var="row" items="${list}" varStatus="j">
			                <c:set var="i" value="${j.count}"/>
			                <c:choose>
			                    <c:when test="${i%4 eq 1}">
			                        <tr class="grid-loof-cont">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                    </c:when>
			                    <c:when test="${mCurrLowNum ne mLowSize && i%4 eq 0}">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                        </tr>
			                    </c:when>
			                    <c:when test="${mCurrLowNum eq mLowSize && i%4 eq 0}">
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                        </tr>
			                        <c:set var="mCurrLowNum" value="${mCurrLowNum+1}"/>
			                    </c:when>
			                    <c:otherwise>
			                            <td class="cont-unit draggable" id="h_id_meta_m_${row.id_meta_m}">${row.meta_m_nm}</td>
			                    </c:otherwise>
			                </c:choose>
			            </c:forEach>
						</tbody>
					</table>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
					<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
					<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
				</div>
		        <div id="page-content-wrap-bottom">
		            	<div id="canvas">
		            		<div id="canvas-content">
								<table class="hidden-table" id="hidden_table">
					                <tr><th><div class="th-split-container"><div class="th-left-content"></div><button class="th-right-content" alt="삭제" onclick="eraseMetaM(this);"></buttion></div></th></tr>
					                <tr><td></td></tr>
					                <tr><td></td></tr>
					            </table>
								<c:forEach var="tab" items="${tabList}" varStatus="i">
								<table class="hidden-table" id="t_id_meta_m_${tab.id_posi}" style="display: block; position: absolute; left: ${tab.left_posi}px; top: ${tab.top_posi}px; width: auto;">
									<tr>
										<th>
											<div class="th-split-container">
												<div id="h_meta_m_${tab.id_posi}" class="th-left-content">${tab.meta_m_nm}</div>
												<button class="th-right-content" alt="삭제" onclick="eraseMetaM(this);"></button>
											</div>
										</th>
									</tr>
									<c:forEach var="meta" items="${tabColMap[tab.id_meta_m]}"  varStatus="i">
										<tr>
											<td><div id="d_meta_${tab.id_posi}_${meta.id_meta}">${meta.meta_nm}</div></td>
										</tr>
									</c:forEach>
								</table>
								</c:forEach>
								<c:forEach var="svg" items="${svgList}" varStatus="i">
								<svg id="svg_${svg.id_svg}" width="${svg.width}" height="${svg.height}" style="position: absolute; left: ${svg.left_posi}px; top: ${svg.top_posi}px;">
									<defs>
									<c:if test="${not empty svg.angs}">
										<c:set var="trans" value='transform="${svg.angs}"'/>
									</c:if>
									<c:choose>
										<c:when test="${svg.right_yn eq 'Y'}">
									<marker id="arrowhead" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto">
									<polygon points="0 0, 10 3.5, 0 7" fill="black" ${(not empty trans ? trans:'')}></polygon>
									</marker>
										</c:when>
										<c:otherwise>
									<marker id="arrowhead" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
									<polygon points="0 0, 10 3.5, 0 7" fill="black" ${(not empty trans ? trans:'')}></polygon>
									</marker>
										</c:otherwise>
									</c:choose>
									</defs>
									<c:set var="ln" value="${svgMap[svg.id_svg]}"/>
									<line id="ln_${ln.id_line}" class="ui-draggable ui-draggable-handle" x1="${ln.x1}" y1="${ln.y1}" x2="${ln.x2}" y2="${ln.y2}"
										stroke="black" stroke-width="2" marker-end="url(#arrowhead)"
										onclick="lineClicked(this)" onmouseover="lineOvered(this)"
										onmouseout="lineOutedd(this)" style="cursor: pointer;"></line>
								</svg>
								</c:forEach>
							</div>
		            	</div>
		        </div>
				<div class="board-bottom-group">
					<c:if test="${!is_guest}">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
						</div>
						<div class="board-bottom-group-rear">
							<button type="button" id="btnInit">
								초기화
							</button>
							<button type="button" class="btn-danger" id="btnDelete" onclick="javascript:delMeta();">
								삭제
							</button>
							<button type="button" class="btn-primary" onclick="javascript:register();">
								저장
							</button>
						</div>
					</c:if>
					<c:if test="${is_guest}">
						<div class="board-bottom-group-front">
							<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
var relColMap = {};
var dragIdMap = {};
var relIdMap = {};
function init(){
	<c:forEach var="tab" items="${tabList}" varStatus="i">
	$("#t_id_meta_m_${tab.id_posi}").draggable();
	var draw_id = '${tab.id_posi}';
	var idArr = draw_id.split("_");
	var draw_tbl_id = 'h_id_meta_m_'+idArr[0];
	var ver = idArr[1].replace("c","");
    dragIdMap[draw_tbl_id] = {
            length: ver
    };
	</c:forEach>	
	<c:forEach var="svg" items="${svgList}" varStatus="i">
// 	$("#svg_${svg.id_svg}").draggable();
	var draw_svg_id = '${svg.id_svg}';
	var idSvgArr = draw_id.split("_");
	var draw_svg_id = 'svg_'+idSvgArr[0];
	var svg_ver = idSvgArr[1].replace("c","");
	relIdMap[draw_svg_id] = {
            length: svg_ver
    };
	</c:forEach>	
	<c:forEach var="line" items="${lineList}" varStatus="i">
// 	$("#line_${line.line}").draggable();
// 	var draw_id = '${tab.id_posi}';
// 	var idArr = draw_id.split("_");
// 	var draw_tbl_id = 'h_id_meta_m_'+idArr[0];
// 	var ver = idArr[1].replace("c","");
//     dragIdMap[draw_tbl_id] = {
//             length: ver
//     };
	</c:forEach>	
}
$(function () {
// 	 $(".draggable").draggable({
// 	        helper: 'clone'
//     });
	init();
    $( ".draggable" ).draggable({
        helper: 'clone',
        stop: function(event, ui) {
            var pos = $('#canvas').position();
            var canvasWidth = $('#canvas').width(); // 부모 요소인 canvas의 초기 너비를 저장합니다.
            var canvasContentPos = $('#canvas-content').position(); // canvas-content의 위치를 가져옵니다.
            var canvasContentWidth = $('#canvas-content').width(); // canvas-content의 초기 너비를 저장합니다.
            var drag_id = $(this).attr("id");
            //
            if (dragIdMap.hasOwnProperty(drag_id)) {
                // relColMap에서 기존 데이터를 업데이트합니다.
                dragIdMap[drag_id].length++;
            } else {
                // relColMap에 새로운 데이터를 추가합니다.
                dragIdMap[drag_id] = {
                    length: 1
                };
            }
            //
            var tbl_h_id = drag_id+"_c"+dragIdMap[drag_id].length;
            var tbl_id = "t_id_meta_m_";
            var id_meta_m = '';
//             if(ui.position.top < $('#canvas').position().top){
//             	$(this).draggable("option", "revert", true); // 드롭 취소
//                 return; // 드롭 취소 후 함수 종료
//             }
            if (tbl_h_id.startsWith("h_id_meta_m_")) {
            	var key = tbl_h_id.substring("h_id_meta_m_".length); // "drag_"의 길이만큼 제거
            	tbl_id +=key;
            	
            	var keyArr = key.split("_");
            	var id_meta_m = keyArr[0];
            	var col_ver = keyArr[1];
            	drawTable(id_meta_m,col_ver,key);
                // remaining에는 "drag_"를 제외한 나머지 문자열이 포함됨
            } else {
                console.log("ID does not start with 'drag_'");
            }
            var newTable = $('#hidden_table').clone().attr('id', tbl_id).css({
															                'display': 'block',
															                'position': 'absolute',
															                'left': ui.position.left,
														                    'top': ui.position.top - $('#canvas').position().top, // newTable의 top 위치를 ui가 놓여진 위치의 top 좌표에서 canvas-content의 top 좌표를 뺀 값으로 설정합니다.
															                'width': 'auto'
															            });
            if(ui.position.top < pos.top){
            	newTable.css('top',pos.top+10);
            }
            var newTableWidth = $('#hidden_table').width()+ui.position.left;
            if(newTableWidth > canvasWidth){
            	$('#canvas-content').css('width', newTableWidth + 50);
// 				$('#canvas-content').css('overflow', 'auto');
            }
            $('#canvas-content').append(newTable);
            newTable.draggable({
                containment: '#canvas-content'
            });
        }
    });
});
$(document).on("drop", "[id^='d_meta_']", function(event, ui) {
    console.log("드롭 이벤트 발생:", this);
});
//부모 요소에 이벤트 위임을 통해 동적으로 추가된 요소에 droppable을 적용
$(document).on('mouseenter', '.droppable', function(event) {
    if (!$(this).hasClass('ui-droppable')) {
        $(this).droppable({
            drop: function(event, ui) {
            	droppableFunc(event,ui);
            }
        });
    }
});
$(document).on('mousedown', '.td_sel', function() {
    $(this).draggable({
        helper: 'clone'
    });
});
$(document).on('drop', '.droppable', function(event, ui) {
	$(this).droppable({
        drop: function(event, ui) {
            // 드롭된 요소 처리
			droppableFunc(event,ui);
		 }
    });
});
function droppableFunc(event,ui){
    // 드롭된 요소 처리
	var dropId = event.target; // 드롭된 요소의 DOM 요소
	var trgParentId = $(dropId).closest("table").attr("id");
	console.log("td_sel 드롭된 요소:", trgParentId);
    var dragId = ui.helper; // 드래그된 요소의 복제본(드래그 헬퍼)
    var srcParentId = $(dragId).closest("table").attr("id");
    console.log("td_sel 드래그된 요소:", srcParentId);
    
    var srcTable = $("#" + srcParentId);
    var trgTable = $("#" + trgParentId);
    
    if(srcParentId == trgParentId){
//     	alertOpen("자기 자신과 연결할 수 없습니다.");
    	return;
    }
    
    // srcParentId 테이블이 왼쪽에 있고, trgParentId 테이블이 오른쪽에 있으면 화살표 그리기
    if (srcTable.offset().left < trgTable.offset().left) {
        drawArrowFromLeftToRight(srcTable, trgTable);
    }
    // srcParentId 테이블이 오른쪽에 있고, trgParentId 테이블이 왼쪽에 있으면 화살표 그리기
    else {
        drawArrowFromRightToLeft(srcTable, trgTable);
    }		
}
function calRelId(srcTable,trgTable){
	var srcKey = srcTable.attr("id").replace("t_id_meta_m_", "");
	var srcKeyArr = srcKey.split("_");
	var trgKey = trgTable.attr("id").replace("t_id_meta_m_", "");
	var trgKeyArr = trgKey.split("_");
	var rel_id = srcKeyArr[0]+"_"+trgKeyArr[0];
    if (relIdMap.hasOwnProperty(rel_id)) {
        // relColMap에서 기존 데이터를 업데이트합니다.
        relIdMap[rel_id].length++;
    } else {
        // relColMap에 새로운 데이터를 추가합니다.
        relIdMap[rel_id] = {
            length: 1
        };
    }
    //
    var final_rel_id = rel_id+"_c"+relIdMap[rel_id].length;
		
	return final_rel_id;	
}
function relationSave(srcTable,trgTable,rel_id,arrow){
    var srcIdMetas = $("#" + srcTable.attr("id")).find(".td_sel").map(function() {
    	if(nanToStr($(this).attr("id"))!=""){
    		if (!($(this).hasClass('ui-draggable-disabled'))) {
    	        $(this).draggable('disable');
    		}
            $(this).removeClass();
            var idMetaArr = this.id.replace("d_meta_","").split("_");
            return idMetaArr[0];
    	}
    }).get();

    console.log("td_sel 클래스를 가진 태그의 ID:", srcIdMetas);
    
    var trgIdMetas = $("#" + trgTable.attr("id")).find(".td_sel").map(function() {
    	if(nanToStr($(this).attr("id"))!=""){
    		if (!($(this).hasClass('ui-draggable-disabled'))) {
    	        $(this).draggable('disable');
    		}
            $(this).removeClass();
            var idMetaArr = this.id.replace("d_meta_","").split("_");
            return idMetaArr[0];
    	}
    }).get();
	
	checkTdSel(srcTable.attr("id"));
	checkTdSel(trgTable.attr("id"));
    if (relColMap.hasOwnProperty(rel_id)) {
        // rel_id가 이미 존재하는 경우
        console.log("rel_id", rel_id, "exists in relColMap. Replacing arrow.");

        // 기존의 arrow를 찾아서 대체합니다.
        var existingArrow = $("#svg_" + rel_id);
        existingArrow.replaceWith(arrow);

        // relColMap에서 기존 데이터를 업데이트합니다.
        relColMap[rel_id] = {
            srcIdMetas: srcIdMetas,
            trgIdMetas: trgIdMetas
        };
    } else {
        // rel_id가 존재하지 않는 경우
        console.log("rel_id", rel_id, "does not exist in relColMap. Adding arrow.");

        // arrow를 추가합니다.
        $('#canvas-content').append(arrow);

        // relColMap에 새로운 데이터를 추가합니다.
        relColMap[rel_id] = {
            srcIdMetas: srcIdMetas,
            trgIdMetas: trgIdMetas
        };
    }
    console.log("td_sel 클래스를 가진 태그의 ID:", trgIdMetas);
}

function lineClicked(that){
	
}
function lineOvered(that){
    $(that).css("cursor", "pointer"); // 드래그 불가능하게 만듦
	
}
function lineOutedd(that){
    $(that).css("cursor", "arrow"); // 드래그 불가능하게 만듦
	
}
function eraseMetaM(button) {
    var table = button.closest('table'); // 버튼의 가장 가까운 부모 테이블 요소를 찾음
    if (table) {
        table.remove(); // 테이블 삭제
    } else {
        console.error('테이블을 찾을 수 없습니다.');
    }
}
function checkTdSel(parentId){
    var counter = $("#" + parentId + " .td_sel").length; // 부모 테이블 내 td_sel 클래스를 가진 요소들의 개수를 세어 카운터에 저장

    // 부모 테이블의 드래그 가능 여부를 설정
    if (counter > 0) {
        $("#" + parentId).draggable("disable"); // 드래그 불가능하게 만듦
        $("#" + parentId).css("cursor", "arrow"); // 드래그 불가능하게 만듦
    } else {
        $("#" + parentId).draggable("enable"); // 드래그 가능하게 만듦
        $("#" + parentId).css("cursor", "pointer"); // 드래그 불가능하게 만듦
    }
	
}
$(document).on("click", "[id^='d_meta_']", function() {
    // 클릭된 요소에 class 속성을 추가합니다.
    if($(this).hasClass("td_sel")){
    	$(this).draggable("disable");
        $(this).removeClass();
    }else{
         $(this).addClass("draggable").addClass("droppable").addClass("td_sel");
         $(this).draggable();
         $(this).draggable("enable");
    }

	var parentId = $(this).closest("table").attr("id"); // 클릭된 요소의 부모 테이블의 아이디를 가져옴
	checkTdSel(parentId);
});
$(document).on("dblclick", "[id^='d_meta_']", function() {
    var currentText = $(this).text();
    $(this).html("<input type='text' value='" + currentText + "'>");

    // input 태그에 포커스를 설정합니다.
    $(this).find("input").focus();

    // input 태그에서 포커스가 빠져나가면 값을 저장하고 다시 텍스트로 표시합니다.
    $(this).find("input").blur(function() {
        var newText = $(this).val();
        $(this).parent().text(newText);
    });
});

function doList(page){
	var frm =document.frm;
		frm.action=contextPath+"/adm/meta/relationList.do";
		if(page){
	   		$("#page").val(page);
		}
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}

function aa(){
	   // 더블 클릭 이벤트 핸들러를 추가합니다.
 $(this).dblclick(function() {
     // 현재 요소의 내용을 input 태그로 교체합니다.
     var currentText = $(this).text();
     $(this).html("<input type='text' value='" + currentText + "'>");

     // input 태그에 포커스를 설정합니다.
     $(this).find("input").focus();

     // input 태그에서 포커스가 빠져나가면 값을 저장하고 다시 텍스트로 표시합니다.
     $(this).find("input").blur(function() {
         var newText = $(this).val();
         $(this).parent().text(newText);
     });
 });

 // 포인터를 올릴 때 텍스트로 바꾸는 이벤트 핸들러를 추가합니다.
 $(this).hover(function() {
     // 현재 요소의 내용을 input 태그로 교체합니다.
     var currentText = $(this).text();
     $(this).html("<input type='text' value='" + currentText + "'>");

     // input 태그에 포커스를 설정합니다.
     $(this).find("input").focus();

     // input 태그에서 포커스가 빠져나가면 값을 저장하고 다시 텍스트로 표시합니다.
     $(this).find("input").blur(function() {
         var newText = $(this).val();
         $(this).parent().text(newText);
     });
 }, function() {
     // 포인터가 요소를 벗어나면 텍스트로 변경합니다.
     var newText = $(this).find("input").val();
     $(this).text(newText);
 });
	
}
</script>
</html>
