package kr.co.daily.common.exception;

public class OfficerChangeException extends Exception {
	
	private static final long serialVersionUID = 2412563802883029843L;

	public OfficerChangeException(String msg){
        super(msg);
    }       

}
