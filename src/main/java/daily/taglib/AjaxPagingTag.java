package daily.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.miemiedev.mybatis.paginator.domain.Paginator;

@Component
public class AjaxPagingTag extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int page;
	private int firstPage;
	private int lastPage;
	private int totPage;
	private String pageLinkUrl;
	private boolean method;
	private Paginator paginator;
	private int range;
	
    private static int RANGE_SIZES;
    
    @Value("#{global['mybatis.pagination.rangesize.per.page']}")
	public void setRangeSize(int rangeSize) {
		RANGE_SIZES = rangeSize;
	}
   
	@Override
	public int doEndTag() throws JspException {
		StringBuilder pageTag = new StringBuilder();
		totPage=paginator.getTotalPages(); //paginator.getStartRow();paginator.getNextPage();paginator.getPrePage();
		page = paginator.getPage();
		range = (page-1)/RANGE_SIZES;
        firstPage = range*RANGE_SIZES +1;
        lastPage = (range+1)*RANGE_SIZES;
        if(totPage<lastPage) {
        	lastPage = totPage;
        }
		if (!method) {
			pageTag.append("<div class=\"table-paging\">");
			pageTag.append("<ul>");
			if(totPage>RANGE_SIZES) {
				pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "(1);\">First</a></li>");
			}
			if (firstPage != 1)
                // 이전 페이지
				pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "&amp;page=" + (firstPage - 1) + "\">&#171;</a></li>");
			for (int i = firstPage; i < lastPage + 1; i++) {
				// 현재 페이지는 <strong>으로 감싸서 표현
				if (page == i)
					pageTag.append("      <li class=\"on\"><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "&amp;page="
							+ i + "\">" + i + "</a></li>");
				else
					pageTag.append("      <li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "&amp;page="
							+ i + "\">" + i + "</a></li>");
			}
			if (lastPage < totPage) {
                // 다음 페이지
				pageTag.append("      <li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "&amp;page="
						+ (lastPage + 1) + "\">&#187;</a></li>");
			    pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("+totPage+")\">Last</a></li>");
			}
			pageTag.append("	</ul>");
			pageTag.append("</div>");
		} else {
			pageTag.append("<div class=\"table-paging\">");
			pageTag.append("<ul>");
			if(totPage>RANGE_SIZES) {
				pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "(1);\">First</a></li>");
			}
			if (firstPage != 1)
	               // 이전 페이지
				pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("
						+ (firstPage - 1) + ");\">&#171;</a></li>");
			for (int i = firstPage; i < lastPage + 1; i++) {
				// 현재 페이지는 <strong>으로 감싸서 표현
				if (page == i)
					pageTag.append("      <li class=\"on\"><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("
							+ i + ");\">" + i + "</a></li>");
				else
					pageTag.append("      <li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("
							+ i + ");\">" + i + "</a></li>");

			}

			if (lastPage < totPage) {
                // 다음 페이지
				pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("

						+ (lastPage + 1) + ");\">&#187;</a></li>");
			    pageTag.append("<li><a href=\"javascript:void(0)\" onclick=\"" + pageLinkUrl + "("+totPage+")\">Last</a></li>");
			}

			pageTag.append("	</ul>");
			pageTag.append("</div>");

		}

		try {

			pageContext.getOut().write(pageTag.toString());

		} catch (IOException e) {

			e.printStackTrace();

		}

		return EVAL_PAGE;

	}

//	public void setPage(int page) {
//
//		this.page = page;
//
//	}
//
//	public void setRange(int range) {
//
//		this.range = range;
//
//	}
//
//
//    
    
	public void setPageLinkUrl(String pageLinkUrl) {

		this.pageLinkUrl = pageLinkUrl;

	}

	public void setMethod(boolean method) {

		this.method = method;

	}

//	public Paginator getPaginator() {
//		return paginator;
//	}
//
	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}
	
}
