package daily.common.vo;

import org.apache.commons.collections4.map.ListOrderedMap;

import daily.common.util.StrUtil;

@SuppressWarnings({ "serial", "rawtypes" })
public class CustomMap extends ListOrderedMap {
	@SuppressWarnings("unchecked")
	@Override
	public Object put(Object key, Object value) {
		return super.put(StrUtil.getLowerCase((String) key), value);
	}
	public Object putStringRaw(Object key,Object value) {
		return super.put((String) key, value);
	}
	public String getStringRaw(Object key){
		return StrUtil.getString(super.get(key));
	}
	public String getStringRaw(Object key, String intVal) {
    	if(super.get(key) == null) return intVal;
    	String val = super.get(key).toString();
    	return val.isEmpty() ? intVal : val;
	}
	public String getString(Object key){
		return StrUtil.getString(super.get(key));
	}
	public Integer getInt(Object key){
		return Integer.parseInt(StrUtil.getString(super.get(key)));
	}
	public String getString(String key){
    	return getString(key, "");
	}
    public String getString(String key, String intVal) {
    	if(super.get(key) == null) return intVal;
    	String val = super.get(key).toString();
    	return val.isEmpty() ? intVal : val;
    }
    /**
     * Long 타입으로 값 얻기
     * @param key
     * @return 존재하지 않으면 0 리턴.
     */
    public double getDouble(String key) {
    	return getDouble(key, 0);
    }

    /**
     * Long 타입으로 값 얻기
     * @param key
     * @return 존재하지 않으면 initVal 리턴.
     */
    public double getDouble(String key, long intVal) {
    	if(super.get(key) == null) return intVal;
    	try {
    		return Double.parseDouble(super.get(key).toString()); 
    	} catch(Exception e) {
    		return intVal;
    	}
    }
    /**
     * 정수 타입으로 값 얻기
     * @param key
     * @return 존재하지 않으면 0 리턴.
     */
    public int getInt(String key) {
    	return getInt(key, 0);
    }
    /**
     * 정수 타입으로 값 얻기
     * @param key
     * @return 존재하지 않으면 initVal 리턴.
     */
    public int getInt(String key, int intVal) {
    	if(super.get(key) == null) return intVal;
    	try {
    		return Integer.parseInt(super.get(key).toString()); 
    	} catch(Exception e) {
    		return intVal;
    	}
    }
	@SuppressWarnings("unchecked")
	public Object putByString(Object key, Object value) {
		return super.put(key, value);
	}

}
