package kr.co.daily.front.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.IdentityManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.service.BidSearchService;
import kr.co.daily.front.service.IdentityService;

@Controller
public class IdentityController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private IdentityService service;
	
	@RequestMapping(value = {"/front/bid/identityList.do","/front/guest/identityList.do"})
	public String identityList(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,"search_srt_dt","search_end_dt",commandMap.getInt("type_term",1));
		commandMap.put("id_service", commandMap.get("sys_code"));
		CustomMap idenMap = service.getIdentityMap(commandMap);
		model.addAttribute("idenMap", idenMap);
		CustomMap childMap = service.getIdentityRel(commandMap);
		model.addAttribute("childMap", childMap);
		List<CustomMap> headerList = service.selectBidIdentity(commandMap);
//		CommandMap specialMap = new CommandMap();
		if(!"".equals(commandMap.getString("sidx", ""))) {
			commandMap.put("order_yn", "Y");
		}
		PageList<CustomMap> todayList = null;
		try {
			todayList = service.selectBidIdentityListContents(commandMap,null);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
		//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
		List<Integer> rawKey = new ArrayList<Integer>();
		for(CustomMap cm : todayList) {
			int key = cm.getInt("id_bid_noti",0);
			if(key!=0) {
				rawKey.add(key);
				valMap.put(key, cm);
			}
		}
		if(rawKey.size()>0) {
			CommandMap searchParam = new CommandMap();
			searchParam.put("id_identity_m", commandMap.getInt("id_identity_m"));
			searchParam.put("list", rawKey);
			List<CustomMap> rawList = service.selectBidNotiColForIdentity(searchParam);
			for(CustomMap cm : todayList) {
				int key = cm.getInt("id_bid_noti",0);
				for(CustomMap rm:rawList) {
					if(rm.getInt("id_bid_noti",0)==key) {
						cm.putByString(rm.getString("meta_cd", ""), rm.getString("conts",""));
					}
				}
			}
		}
		putModelListPage(todayList, model);
		model.addAttribute("headerList", headerList);
		model.addAttribute("paramMap", commandMap.getMap());
		return "front/bid/identityList";
	}
	@RequestMapping(value = {"/front/bid/viewIdentity.do","/front/guest/viewIdentity.do"})
	public String viewIdentity(CommandMap commandMap, ModelMap model) throws Exception {
		CustomMap idenMap = service.getIdentityRel(commandMap);
		model.addAttribute("idenMap", idenMap);
//		selectIdentityRelList
		CommandMap childParam = new CommandMap();
		childParam.put("id_identity_m", idenMap.getInt("id_meta_m", 0));
		List<CustomMap> colList = service.selectBidIdentity(childParam);
		Map<String,String> keyMap = CollectionUtil.getKeyListByCommandMap(commandMap, "key_");
    	Map<String,String> keyValMap = new HashMap<String,String>();
		for(String col_nm : keyMap.keySet()) {
			String val = keyMap.get(col_nm);
			keyValMap.put(col_nm, val);
		}
		childParam.put("keyValMap", keyValMap);
		childParam.put("sys_code", commandMap.getString("sys_code"));
		List<CustomMap> rawList = service.selectBidNotiColByKeyList(childParam);
		Map<String,CustomMap> colMap = new HashMap<>();
		Map<String,CustomMap> rowMap = new HashMap<>();
		for(CustomMap col:colList) {
			colMap.put(col.getString("meta_cd", ""), col);
		}
		for(CustomMap row:rawList) {
			rowMap.put(row.getString("id_bid",""), row);
		}
		for(CustomMap col:colList) {
			String col_meta_cd = col.getString("meta_cd");
			CustomMap inCol = colMap.get(col_meta_cd);
			String id_bid = inCol.getString("refer_id_meta", "");
			if("".equals(id_bid)) {
				String val = keyValMap.get(col_meta_cd);
				col.put("val", val);
			}else {
				CustomMap row = rowMap.get(id_bid);
				String row_link_yn = row.getString("link_yn","");
				if(!"".equals(row_link_yn) && "Y".equals(row_link_yn)) {
					inCol.put("link_yn", row_link_yn);
				}
				inCol.put("val",row.getString("conts"));
			}
		}
		model.addAttribute("colList", colList);
		return "front/bid/viewIdentity";
	}

}
