package kr.co.daily.front.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import daily.common.base.AbstractControllerBase;
import daily.common.util.RandomUtil;
import daily.common.util.SpringPropertiesUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.common.service.SimpleSendMailService;
import kr.co.daily.front.service.MemberService;

@Controller
public class MemberController extends AbstractControllerBase {
	@Autowired private MemberService service;
    private static String SENDER_MAIL;
	@Autowired private SimpleSendMailService mailService;
	@Autowired SessionLocaleResolver localeResolver;
    
	private PasswordEncoder  encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	
    @Value("#{global['email.smtp.account.id']}")
	public void setRangeSize(String rangeSize) {
    	SENDER_MAIL = rangeSize;
	}
	
	@RequestMapping("/join.do")
	public String join(CommandMap commandMap, Model model) throws Exception {
		return "member/join";
	}

	@RequestMapping("/member/joinCompleteView.do")
	public String joinComplete(CommandMap commandMap, Model model) throws Exception {
		return "member/join_complete";
	}

	@RequestMapping("/member/find_user_info.do")
	public String find_user_info(CommandMap commandMap, Model model) throws Exception {
		return "member/find_user_info";
	}

	@RequestMapping("/member/findInfoAjax.do")
	public String findInfoAjax(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = this.addParamByLoginInfo(commandMap);
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			List<CustomMap> sameUserList = service.selectUserByNmAndEmail(commandMap);
			if(sameUserList.size()<2) {
				CustomMap usr = service.getUser(commandMap);
				model.addAttribute("usr",usr);
				if(!"".equals(commandMap.getString("pwd_reset", "")) && usr !=null) {
					String pwd = RandomUtil.randomCharAndNum(8);
					String encPassword = encoder.encode(pwd);
					commandMap.put("password", encPassword);
					commandMap.put("mod_user", usr.getInt("user_seq",0));
					service.updatePwd(commandMap);
					CommandMap mailMap = new CommandMap();
					mailMap.put("subject","암호 초기화");
					mailMap.put("ConfirmString",pwd);
					mailMap.put("senderNm","ADMIN");
					mailMap.put("senderMail",SENDER_MAIL);
					mailMap.put("receivorMail",usr.getString("email"));
					mailMap.put("receivorNm",usr.getString("user_nm", ""));
			        Map<String, String> input = new HashMap<String, String>();
			        input.put("ConfirmString", pwd);
					Locale locale = localeResolver.resolveLocale(req);
			        input.put("mailTitle", SpringPropertiesUtil.getProperty("mail.reset.title",locale));
			        input.put("mailRow1", SpringPropertiesUtil.getProperty("mail.reset.row1",locale));
			        input.put("mailRow2", SpringPropertiesUtil.getProperty("mail.reset.row2",locale));
			        input.put("mailRow3", SpringPropertiesUtil.getProperty("mail.reset.row3",locale));
			        input.put("mailRow4", SpringPropertiesUtil.getProperty("mail.reset.row4",locale));
			        input.put("resetLabel", SpringPropertiesUtil.getProperty("mail.reset.lbl1",locale));
			        input.put("ConfirmString", pwd);
			        input.put("TimeOfIssue", StringDateUtil.getDateByFormat("yyyy/MM/dd, HH:mm z",locale));
					mailService.sendMailFromResetHtml(mailMap,input);
					model.addAttribute("info",commandMap.getMap());
				}
			}else {
				model.addAttribute("same",true);
			}
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/member/checkDblId.do")
	public String checkDblId(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			CustomMap cnt = service.getCntUserByIdUser(commandMap);
			model.addAttribute("cnt", cnt.getInt("cnt",0));
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/member/insertUserAjx.do")
	public String historyAjax(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = this.addParamByLoginInfo(commandMap);
		int ret = GlobalConst.ACT_SUCCESS;
		try {
			String encPassword = encoder.encode(commandMap.getString("user_pwd", ""));
			commandMap.put("password", encPassword);
			service.insertMember(commandMap);
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
}
