package kr.co.daily.common.exception;

public class ExistEmailInPersonalException  extends RuntimeException{

	private static final long serialVersionUID = -5944115009137659174L;

	public ExistEmailInPersonalException(String msg){
        super(msg);
    }       

}
