package test.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import daily.common.vo.CustomMap;

public class MapTestUtil {
	public static List<CustomMap> generateSampleData() {
        List<CustomMap> mapList = new ArrayList<>();
        
        // 데이터 생성 및 Map에 추가
        mapList.add(createData(1, 0, 1,1,"Y"));
        mapList.add(createData(2, 0, 1,2,"N"));
        
        mapList.add(createData(3, 1, 2,1,"Y"));
        mapList.add(createData(4, 1, 2,2,"Y"));
        mapList.add(createData(5, 2, 2,3,"Y"));
        mapList.add(createData(6, 2, 2,4,"Y"));
        
        mapList.add(createData(7, 3, 3,5,"Y"));
        mapList.add(createData(8, 3, 3,6,"Y"));
        mapList.add(createData(9, 5, 3,7,"Y"));
        mapList.add(createData(10, 5, 3, 8, "Y"));
        mapList.add(createData(11, 6, 3, 9, "N"));
        mapList.add(createData(12, 6, 3, 10, "N"));
        
        mapList.add(createData(13, 7, 4, 11, "N"));
        mapList.add(createData(14, 7, 4, 12, "N"));
        return mapList;
    }

	public static List<CustomMap> generateChildData() {
        List<CustomMap> mapList = new ArrayList<>();
        
        // 데이터 생성 및 Map에 추가
        mapList.add(createData(15, 0, 1,1,"Y"));
        mapList.add(createData(16, 0, 1,2,"N"));
        mapList.add(createData(17, 0, 1,3,"Y"));
        mapList.add(createData(18, 0, 1,4,"Y"));
        return mapList;
    }
	public static List<CustomMap> generateFkData() {
        List<CustomMap> mapList = new ArrayList<>();
        
        // 데이터 생성 및 Map에 추가
        mapList.add(createFkData(1, 8, 2,15,1));
        mapList.add(createFkData(1, 8, 2,16,2));
        return mapList;
    }
    private static CustomMap createData(int id, int parent, int depth,int ord,String use_yn) {
        CustomMap map = new CustomMap();
        map.put("id_meta", id);
        map.put("id_parent_meta", parent);
        map.put("meta_depth", depth);
        map.put("row_num", ord);
        map.put("use_yn", use_yn);
        return map;
    }
    private static CustomMap createFkData(int id_meta_m, int id_meta, int id_slv_meta_m,int id_slv_meta,int slv_ord) {
        CustomMap map = new CustomMap();
        map.put("id_meta", id_meta);
        map.put("id_slv_meta_m", id_slv_meta_m);
        map.put("id_meta_m", id_meta_m);
        map.put("slv_ord", slv_ord);
        return map;
    }
}
