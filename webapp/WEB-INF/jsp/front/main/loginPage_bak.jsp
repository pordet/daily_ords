<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="daily.common.security.LoginManager"%>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<% LoginManager loginManager = LoginManager.getInstance(); %>
<!DOCTYPE html>
<base:JspHead title=""/>
<%
   int lgnCnt = loginManager.getUserCount();
%>
<c:set var="cnt" value="<%=lgnCnt%>"/>
<base:JspBody currLocale="${locale}" hasPop="true">
<%-- <form id="f" action="<c:url value='/j_spring_security_check'/>" method="post"> --%>
<form id="f" name="f" action="<c:url value='/loginAction.do'/>" method="post">
	<input type="hidden" name="loginRedirect" value="${loginRedirect}"/>
	<input type="hidden" name="reset" id="reset" value="N"/>
    <input type="hidden" name="user_type" value="${GlobalConst.USER_TYPE_APPLICANT}">
	<label style="width:50px;">id : </label><input type="text" name="id" id="id"><br />
	<label style="width:50px;">pwd :</label><input type="password" name="pwd" id="pwd"><br />
	<input type="button" value="login" onclick="javascript:login();">
</form>
<c:if test="${not empty msgNm}">
   <font color="red">login fail</font>
   <p>${msgNm}</p>
</c:if>
<h3 align="center">현재 접속자 수 :
<c:out value="${cnt}"/> 명
</h3>s
<a href="<c:url value='/task/taskList.do'/>">테스트  Page</a>
</base:JspBody>
<script>
   function login(){
	   var id = $("#id").val();
	   $.ajax({
			url: contextPath+"/login/checkUsr.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
		    data: { id: id },                // HTTP 요청과 함께 서버로 보낼 데이터
		    type: "GET",                             // HTTP 요청 방식(GET, POST)
		    dataType: "json" ,
		    success : function(e){
		    	if(e.cnt>0){
		    		var yn = confirm("기존에 로그인한 정보가 존재합니다. 다시 로그인하시겠습니까?");
		    		if(yn == true){
		    			$("#f").submit();
		    		}
		    	}else{
	    			$("#f").submit();
		    	}
		    },
		    error : function(e){
		    	alertOpen('실패하였습니다.');
		    }
		   
	   })
   }
</script>