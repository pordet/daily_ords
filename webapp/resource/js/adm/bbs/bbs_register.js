function delData(){
	var params = $("#frm").serialize();
	var url = contextPath+"/adm/bbs/deleteBoardAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$("#modi_id_board").val('');
	    		$("#modi_title").val('');
	    		$("#modi_comment").val('');
	    		$("#modi_keyword").val('');
	    		$("[id^=attach_]").remove();
	    		$("[id^=keyword_]").remove();
	    		$("#btnDelete").hide();
	    		$("#board_reg_type").html(nanToStr(e.board_path)+" 등록");
//	    		addKeyword();
	    		$('#modal_body').scrollTop(0);
	       		list_redraw(e);
	    		draw_paging_by_ajax(e.paginator);
	    		$("#modi_title").focus();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else if(e.status=='408'){
			    alertOpen('업로드 할 수 있는 파일 크기는 500M 입니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});
}
var attach_idx=0;
function delAttach(idx){
	attach_idx=idx;
	confirmOpen('삭제시 복구할 수 없습니다.',delAttachFile);
}
function delAttachFile(id_board_file){
	$("#modi_id_board_file").val(id_board_file);
	var params = $("#modi_frm").serialize();
	$.ajax({
		url: contextPath+"/deleteHistoryFileAjax.do", // 클라이언트가 요청을 보낼 서버의 URL 주소
	    data: params,                // HTTP 요청과 함께 서버로 보낼 데이터
	    type: "POST",                             // HTTP 요청 방식(GET, POST)
	    dataType: "json" ,
	    success : function(e){
	    	if(e.ret == '0'){
	     	   $("#attach_"+id_board_file).remove();
	    	   $("#attach_"+id_board_file+"_add").remove();
	       		list_redraw(e);
	    	}else{
		    	alertOpen('첨부파일 삭제시 오류가 발생했습니다.');
	    	}
	    },
	    error : function(e){
	    	if(e.status=='403'){
		    	alertOpen('세션이 종료되었습니다.');
	    	}else if(e.status=='401'){
			    alertOpen('접근권한이 없습니다.');
	    	}else{
		    	alertOpen('알수 없는 오류가 발생하였습니다.');
	    	}
	    }
	})
}
function delAttach(idx){
	   $("#addFile_in_div_"+idx).remove();
	   $("#delFile_in_div_"+idx).remove();
}
