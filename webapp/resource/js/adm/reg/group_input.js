$(function () {
	$("#btnSave").click(function(){
//		checkStringFormatAndNull("#group_desc",250);
		$('input[name=hdn_id_role]').remove();
		var dForm = document.frm;
		$('input:checkbox[name=id_role]').each(function() {
			   var input = document.createElement("input");
			   input.setAttribute("type","hidden");
			   input.setAttribute("name","hdn_id_role");
			   input.setAttribute("value",$(this).val());
			   dForm.appendChild(input);
	     });		
		var params = jQuery("#frm").serialize();
		var insert_yn = $("#insert_yn").val();
		var url = '';
		if(insert_yn =='Y'){
			url = contextPath+"/adm/user/insertUsrGroupAjax.do";
		}else{
			url = contextPath+"/adm/user/modifyUsrGroupAjax.do";
		}
		processing();
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : params,
		    success : function(e){
		    	endProcessing();
		    	if(e.ret==1){
		    		alertOpen('실패하였습니다.');
		    	}else{
		    		if(insert_yn =='Y'){
						$("#cur_title").html('사용자 그룹 수정');
			    		$("#id_group").val(e.id_group);
			    		$("#insert_yn").val('N');
		    		}
			    	infoOpen('성공하였습니다.');
		    	}
		    },
		    error : function(e){
		    	endProcessing();
		    	if(e.status=='403'){
			    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
		    	}else if(e.status=='401'){
				    alertOpen('접근 권한이 없습니다.');
		    	}else{
			    	alertOpen('실패하였습니다.');
		    	}
		    }
		});
	});
});
function initForm(){
	$("#cur_title").html('사용자 그룹 등록');
	$("#insert_yn").val('Y');
	$("#group_nm").val('');
	$("#group_desc").val('');
    $("#id_main_menu option:eq(0)").attr("selected","selected");
    $("#id_role").empty();
    $("#groupRoleList").empty();
    $.each(menuArr,function(i,d) {
    	createRoleElement("delete", d[0], d[1]);
    });
    $("#ra1").attr("checked",false);
    $("#ra2").attr("checked",true);
}

// ===================================================================================
function addRole(){
	$('#id_role input:checkbox').each(function() {
		if($(this).is(':checked')){
			var id_role = $(this).val();
			var role_nm = $("label[for='role_"+ id_role +"']").html();
			$("#id_role #role_li_" + id_role).remove();

			createRoleElement("add", id_role, role_nm);
		}
	});
}
function removeRole(){
	$('#groupRoleList input:checkbox').each(function() {
		if($(this).is(':checked')){
			var id_role = $(this).val();
			var role_nm = $("label[for='role_"+ id_role +"']").html();
			$("#groupRoleList #role_li_" + id_role).remove();

			createRoleElement("delete", id_role, role_nm);
		}
	});
}
function createRoleElement(type, roleNo, roleNm) {
	//type : add(가능 -> 등록) / delete(등록 -> 가능)
	var html = '';
	html+=' <li id="role_li_'+ roleNo +'">';
	html+='		<div>';
	html+='			<input type="checkbox" value="'+roleNo+'" id="role_'+ roleNo +'"';
	if(type == "add") {
		html+=' 	name="id_role"';
	}
	html+='			>';

	html+='			<label for="role_'+ roleNo +'">' + roleNm + '</label>';
	html+='		</div>';
	html+='	</li>';

	if(type == "add") {
		$("#groupRoleList").append(html);
	} else if(type == "delete") {
		$("#id_role").append(html);
	}
}
function deleteUserGroup(){
	confirmOpen('사용자 그룹을 삭제하시면 관련 그룹 사용자들도 삭제됩니다.<br>주의하십시오.',"confirmDeleteUserGroup");
}
function confirmDeleteUserGroup(){
	var formData = new FormData($('#frm')[0]);
	var url = contextPath+"/adm/user/deleteUsrGroupAjax.do";
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : formData,
	    contentType:false ,
	    processData:false ,
	    success : function(e){
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		initForm();
		    	infoOpen('성공하였습니다.');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
    			return;
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
    			return;
	    	}else{
		    	alertOpen('실패하였습니다.');
    			return;
	    	}
	    }
	});
}
