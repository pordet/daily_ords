package daily.common.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class InterceptorHandler implements HandlerInterceptor {

	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		HttpSession sess= req.getSession(); 
		if(!req.getRequestURI().contains("loginPage.do")) {
//		if(session.getAttribute("authUser")!=null) {
			if(req.getMethod().equalsIgnoreCase("POST")) {
				Map<String, Object> parameterMap = req.getParameterMap();
				Map<String, Object> paramMap = new HashMap<>();
				for(String param_nm:parameterMap.keySet()) {
					Object obj=parameterMap.get(param_nm);
					if(obj instanceof String) {
						String str = (String)obj;
						paramMap.put(param_nm, str);
					}else if(obj instanceof String[]) {
						String[] strArr = (String[])obj;
						if(strArr.length==1) {
							paramMap.put(param_nm, strArr[0]);
						}else {
							paramMap.put(param_nm, strArr);
						}
					}
				}
				sess.setAttribute("postData", paramMap);
			}
//			else {
//				session.removeAttribute("postData");
//			}
			sess.setAttribute("method", req.getMethod());
			StringBuffer reqUrl = req.getRequestURL();
			sess.setAttribute("prevPage",reqUrl.toString());
		}
	}

}
