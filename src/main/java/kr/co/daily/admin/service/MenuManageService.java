package kr.co.daily.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.CollectionUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.AuthManageDAO;
import kr.co.daily.admin.dao.MenuManageDAO;
import kr.co.daily.admin.dao.ResourceDAO;
import kr.co.daily.admin.dao.UserManageDAO;

@Service
public class MenuManageService {

	@Autowired MenuManageDAO dao;
	@Autowired UserManageDAO userDao;
	@Autowired AuthManageDAO authDao;
	@Autowired ResourceDAO rsrcDao;

	public PageList<CustomMap> selectMenuList(CommandMap map) {
		return dao.selectMenuList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public List<CustomMap> selectMenu(CommandMap map) {
		return dao.selectMenu(map.getMap());
	}

	public CustomMap getMenu(CommandMap map) {
		return dao.getMenu(map.getMap());
	}

	public void insertMenu(CommandMap map) {
		dao.insertMenu(map.getMap());
	}

	public void deleteMenu(CommandMap map) {
//		List<String> list = (List<String>)map.getMap().get("del_code_list");
//		for(String id_resource : list) {
//			CommandMap param = new CommandMap();
//			param.put("id_resource", id_resource);
//			CustomMap resource = dao.getResourceIdByIdMenu(param.getMap());
//			if(resource == null) continue;
//			if("url".equals(resource.getString("resource_type"))) {
//				dao.deleteResourceRole(param.getMap());
//				dao.deleteResource(param.getMap());
//			}else {
//				CustomMap childResource = dao.getChildResource(param.getMap());
//				if(childResource!=null) {
//				CommandMap childParam = CollectionUtil.getCommandMapByCustomMap(childResource);
//					dao.deleteResourceRole(childParam.getMap());
//					dao.deleteResource(childParam.getMap());
//				}
//				dao.deleteResourceRole(param.getMap());
//				dao.deleteResource(param.getMap());
//			}
//		}
		//list 변수에 데이터형이 맞는지 확인 필요
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>)map.getMap().get("del_code_list");
		for(String id_menu : list) {
			CommandMap param = new CommandMap();
			param.put("resource_pattern", id_menu);
			CustomMap resource = rsrcDao.getResourceIdByIdMenu(param.getMap());
			if(resource!=null) {
				CommandMap resourceParam = CollectionUtil.getCommandMapByCustomMap(resource);
				CommandMap param1 = new CommandMap();
				param1.put("id_parent_resource", resource.getInt("id_resource"));
				CustomMap childResource = rsrcDao.getResourceIdByParentResource(param1.getMap());
				if(childResource!=null) {
					CommandMap childParam = CollectionUtil.getCommandMapByCustomMap(childResource);
					authDao.deleteResourceRole(childParam.getMap());
					rsrcDao.deleteResource(childParam.getMap());
				}
				authDao.deleteResourceRole(resourceParam.getMap());
				rsrcDao.deleteResource(resourceParam.getMap());
			}
			dao.deleteMenu(map.getMap());
		}
	}

	public void modifyMenu(CommandMap map) {
//		map.put("resource_pattern", map.getString("id_menu"));
//		CustomMap parentResource = rsrcDao.getResourceIdByIdMenu(map.getMap());
//		if(parentResource!=null) {
//			map.put("id_resource", parentResource.getString("id_resource"));
//			map.put("resource_pattern", map.getString("menu_url"));
//			rsrcDao.modifyResourceByMenuUrl(map.getMap());
//		}
		dao.modifyMenu(map.getMap());
	}

	public int getMaxOrder(CommandMap map) {
		return dao.getMaxOrder(map.getMap());
	}

	public List<CustomMap> selectMenuByParentId(CommandMap map) {
		return dao.selectMenuByParentId(map.getMap());
	}

	public List<CustomMap> selectMenuCodeByParentId(CommandMap map) {
		return dao.selectMenuCodeByParentId(map.getMap());
	}

	public CustomMap getMenuByResourcePattern(CommandMap map) {
		return dao.getMenuByResourcePattern(map.getMap());
	}

	public List<CustomMap> selectProgramList(CommandMap map) {
		return dao.selectProgramList(map.getMap());
	}

	public List<CustomMap> selectUnregisterMenuList(CommandMap map) {
		return dao.selectUnregisterMenuList(map.getMap());
	}

	public List<CustomMap> selectStartPageList(CommandMap map) {
		return dao.selectStartPageList(map.getMap());
	}


}
