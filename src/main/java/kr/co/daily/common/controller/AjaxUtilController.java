package kr.co.daily.common.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import daily.common.base.AbstractCommonBase;
import daily.common.constant.DateEnum;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import kr.co.daily.common.GlobalConst;

@Controller
public class AjaxUtilController extends AbstractCommonBase {

	@RequestMapping(value = {"/util/getDateTermAjax.do","/util/getDateTermApplicantAjax.do"})
	public String selectSyncResult(CommandMap commandMap, ModelMap model) throws Exception {
		commandMap = addParamByBetween(commandMap,commandMap.getString("start_dt_id"),commandMap.getString("end_dt_id"),commandMap.getInt("type_term"));
        model.put("ret",commandMap.getMap());
//        model.put("paginator", paginator);
		return "jsonView";
	}

	@RequestMapping("/util/getNationDateTermAjax.do")
	public String getNationDateTerm(CommandMap commandMap, ModelMap model,HttpServletRequest req) throws Exception {
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(req);
		Locale locale = localeResolver.resolveLocale(req);
		commandMap.put("locale", locale.toString());
		int type = commandMap.getInt("type_term",1);
//		if(map.getString(startFieldNm, "").equals("")) {
		String stDate = "";
		if(type==1) {
			stDate = StringDateUtil.getMinusFromToday(GlobalConst.LOCAL_DATE_FORMAT, DateEnum.WEEK, 1);
		}else if(type==2) {
			stDate = StringDateUtil.getMinusFromToday(GlobalConst.LOCAL_DATE_FORMAT, DateEnum.MONTH, 1);
		}else if(type==3) {
			stDate = StringDateUtil.getMinusFromToday(GlobalConst.LOCAL_DATE_FORMAT, DateEnum.MONTH, 3);
		}
//			map.put(startFieldNm, stDate);
//		}
//		if(map.getString(endFieldNm, "").equals("")) {
//			String stDate = StringDateUtil.getToday(GlobalConst.BASIC_DATE_FORMAT);
//			map.put(endFieldNm, stDate);
//		}
//		return map;
		model.addAttribute("", "");
		model.addAttribute("stDate", stDate);
		return "jsonView";
	}

}
