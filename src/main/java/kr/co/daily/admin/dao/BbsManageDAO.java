package kr.co.daily.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface BbsManageDAO {

	List<CustomMap> selectDataMaster(Map<String, Object> map);

	PageList<CustomMap> selectBbsMaster(Map<String, Object> map, PageBounds pageBounds);

	void insertBbsMaster(Map<String, Object> map);

	void updateBbsMaster(Map<String, Object> map);

	void deleteBbsMaster(Map<String, Object> map);

	void updateOrdStrAfterInsertBbsMaster(Map<String, Object> map);

	void updateOrdStrAtFirstBoard(Map<String, Object> map);

	List<CustomMap> selectDataBbsMaster(Map<String, Object> map);

	CustomMap getBbsMaster(Map<String, Object> map);

	CustomMap getParentBbsMaster(Map<String, Object> map);

	CustomMap getBbsM(Map<String, Object> map);

	void insertBoardMeta(Map<String, Object> map);

	void insertBoardMetaByStand(Map<String, Object> map);

	List<CustomMap> selectBoardMetaByStand(Map<String, Object> map);

	void deleteBbsData(Map<String, Object> map);

	void deleteBbsFile(Map<String, Object> map);

	void deleteBbsRow(Map<String, Object> map);

	void deleteBbsMeta(Map<String, Object> map);

	void updateBoardMetaByStand(Map<String, Object> map);

}
