package kr.co.daily.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.UsrDAO;

@Service
public class UsrService {

	@Autowired UsrDAO dao;
	
	public void insertUsr(CommandMap map) {
		dao.insertUsr(map.getMap());
	}

	public void deleteUsr(CommandMap map) {
		dao.deleteUsr(map.getMap());
	}
	public void insertUsrTran(CommandMap map) {
		dao.insertUsr(map.getMap());
		CommandMap map1 = new CommandMap();
		map1.put("id","test1");
		map1.put("pwd", "1");
		dao.insertUsr(map1.getMap());
	}
	
	public CustomMap getUsr(CommandMap map) {
		CustomMap ret = dao.getUser(map.getMap());
		return ret;
	}

	public PageList<CustomMap> selectUsrList(CommandMap map) throws Exception {
		return dao.selectUsrList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

}
