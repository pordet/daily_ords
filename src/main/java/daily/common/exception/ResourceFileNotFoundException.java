package daily.common.exception;

public class ResourceFileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5882425299040052590L;

    private String message = "File isn't exist";

    public ResourceFileNotFoundException() {
        super();
    }

    public ResourceFileNotFoundException(final String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
