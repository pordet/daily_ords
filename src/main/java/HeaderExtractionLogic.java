import java.util.HashMap;
import java.util.List;
import java.util.Map;

import crs.excel.TableCell;
import crs.excel.TableData;
import crs.excel.TableRow;

public class HeaderExtractionLogic {
    public static void main(String[] args) {
        // 테이블 데이터 생성
        TableData tableData = createSampleTable();

        // 헤더 추출
        Map<String, String> headerMap = extractHeader(tableData);

        // 출력
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            System.out.println("ID: " + entry.getKey() + ", Header: " + entry.getValue());
        }
    }

    public static Map<String, String> extractHeader(TableData tableData) {
        Map<String, String> headerMap = new HashMap<>();

        List<TableRow> rows = tableData.getRows();
        for (TableRow row : rows) {
            List<TableCell> cells = row.getCells();
            for (TableCell cell : cells) {
//                String id = cell.getId();
//                String data = cell.getData();
//
//                // ID가 공백이 아니면서 헤더가 아닌 경우만 추출
//                if (!id.isEmpty() && !data.isEmpty() && !headerMap.containsKey(id)) {
//                    headerMap.put(id, data);
//                }
            }
        }

        return headerMap;
    }

    public static TableData createSampleTable() {
        TableData tableData = new TableData();


        return tableData;
    }
}
