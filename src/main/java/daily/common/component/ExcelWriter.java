package daily.common.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.i18n.LocaleContextHolder;

import daily.common.util.SpringPropertiesUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

public class ExcelWriter {
	private Workbook workbook;
	private Map<String, Object> model;
	private HttpServletResponse response;
	// 테이블 헤더용 스타일
	private CellStyle headStyle;
    // 데이터용 스타일
	private CellStyle bodyStyle;
	private List<Integer> colmnWidthList = new ArrayList<Integer>(); 
	Locale locale = LocaleContextHolder.getLocale();

	public ExcelWriter(Workbook workbook, Map<String, Object> model, HttpServletResponse response) {
		this.workbook = workbook;
		this.model = model;
		this.response = response;
		this.headStyle = workbook.createCellStyle();
	    // 가는 경계선을 가집니다.
	    headStyle.setBorderTop(BorderStyle.THIN);
	    headStyle.setBorderBottom(BorderStyle.THIN);
	    headStyle.setBorderLeft(BorderStyle.THIN);
	    headStyle.setBorderRight(BorderStyle.THIN);

	    // 배경색은 노란색입니다.
	    headStyle.setFillForegroundColor(HSSFColorPredefined.YELLOW.getIndex());
	    headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

	    // 데이터는 가운데 정렬합니다.
	    headStyle.setAlignment(HorizontalAlignment.CENTER);

	    // 데이터용 경계 스타일 테두리만 지정
	    this.bodyStyle = workbook.createCellStyle();
	    bodyStyle.setBorderTop(BorderStyle.THIN);
	    bodyStyle.setBorderBottom(BorderStyle.THIN);
	    bodyStyle.setBorderLeft(BorderStyle.THIN);
	    bodyStyle.setBorderRight(BorderStyle.THIN);
	    
		CommandMap param = (CommandMap)model.get("param");
		String colmnWthkey = "excel."+param.getString("keyNm")+".columnWidth";
		String colmnWth = SpringPropertiesUtil.getProperty(colmnWthkey);
		String[] colmnWthArr = colmnWth.split(";");
		for(String wth:colmnWthArr) {
			colmnWidthList.add(Integer.parseInt(wth));
		}
	}

	public void create() {


		setFileName(response, mapToFileName());

		Sheet sheet = workbook.createSheet();
		


		createHead(sheet, mapToHeadList());

		createBody(sheet, mapToBodyList());
//		Row row = sheet.getRow(0);
//		for(int colNum = 0; colNum<row.getLastCellNum();colNum++)   
//			workbook.getSheetAt(0).autoSizeColumn(colNum);
	}

	private String mapToFileName() {
		CommandMap param = (CommandMap)model.get("param");
		return param.getString("fileNm");
	}

	private List<String> mapToHeadList() {
		CommandMap param = (CommandMap)model.get("param");
		String key = "excel."+param.getString("keyNm")+".head."+locale;
		String head = SpringPropertiesUtil.getProperty(key);
		String[] colArr = head.split(";");
		List<String> colList = new ArrayList<String>();
		for(String colNm : colArr) {
			colList.add(colNm);
		}
		return colList;
	}

	@SuppressWarnings("unchecked")
	private List<List<String>> mapToBodyList() {
		CommandMap param = (CommandMap)model.get("param");
		String key = "excel."+param.getString("keyNm")+".body."+locale;
		String headColmn = SpringPropertiesUtil.getProperty(key);
		String[] headColArr = headColmn.split(";");
		List<String> colList = new ArrayList<String>();
		for(String colNm : headColArr) {
			colList.add(colNm);
		}
		List<CustomMap> result = (List<CustomMap>)model.get("result");
		List<List<String>> ret = new ArrayList<List<String>>();
		for(CustomMap row : result) {
			List<String> rowDat = new ArrayList<String>();
			for(String col:colList) {
				rowDat.add(row.get(col).toString());
			}
			ret.add(rowDat);
		}
		return ret;
	}

	private void setFileName(HttpServletResponse response, String fileName) {
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + getFileExtension(fileName) + "\"");
	}

	private String getFileExtension(String fileName) {
		if (workbook instanceof XSSFWorkbook) {
			fileName += ".xlsx";
		}
		if (workbook instanceof SXSSFWorkbook) {
			fileName += ".xlsx";
		}
		if (workbook instanceof HSSFWorkbook) {
			fileName += ".xls";
		}

		return fileName;
	}

	private void createHead(Sheet sheet, List<String> headList) {
		createRow(sheet, headList, 0);
	}

	private void createBody(Sheet sheet, List<List<String>> bodyList) {
		int rowSize = bodyList.size();
		for (int i = 0; i < rowSize; i++) {
			createRow(sheet, bodyList.get(i), i + 1);
		}
	}

	private void createRow(Sheet sheet, List<String> cellList, int rowNum) {
		int size = cellList.size();
		Row row = sheet.createRow(rowNum);

		for (int i = 0; i < size; i++) {
			Cell cell = row.createCell(i);
			if(i==0) {
				cell.setCellStyle(headStyle);
			}else {
				cell.setCellStyle(bodyStyle);
			}
			cell.setCellValue(cellList.get(i));
		}
	}

}
