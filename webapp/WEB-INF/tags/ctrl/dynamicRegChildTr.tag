<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>


<%@ attribute name="child_list" required="true" type="java.util.List" %>
<%@ attribute name="curr_row_num" required="true" type="java.lang.Integer" %>		
<%@ attribute name="curr_col_num" required="true" type="java.lang.Integer" %>		
					<%-- 리턴 값 받을 변수명 --%>
	<c:forEach var="child" items="${child_list}"  varStatus="i">
	<commc:DynamicRegTrTag paramRowNum="${curr_row_num}" paramColNum="1" row="${child}">
	---------[${curr_row_num }]/[${curr_col_num }]
		<ctrl:dynamicRegTc col="${col}" curr_row_num="${currRowNum}" curr_col_num="${currColNum}"/>
<%-- 		[${col}] --%>
	</commc:DynamicRegTrTag>
	<c:set var="curr_row_num" value="${currRowNum}"/>
	<c:set var="curr_col_num" value="${currColNum}"/>
	++++++++[${curr_row_num }]/[${curr_col_num }]
	</c:forEach>
