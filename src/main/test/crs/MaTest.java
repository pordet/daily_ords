package crs;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import daily.common.security.util.CustomRequestMatcher;

public class MaTest {

	@Test
	public void patternTest() throws MalformedURLException {
		String urlPattern = "/front/**/ad.do";
        
        // RequestMatcher를 생성하여 URL 패턴 지정
		CustomRequestMatcher matcher = new CustomRequestMatcher(urlPattern); 

        
        // 매칭할 URL 문자열
        String url = "/front/adag/dadfd.do/ad.do";
        
        
        boolean matches1 = matcher.matches(url);
        
        // 매칭 결과 출력
        if (matches1) {
            System.out.println("URL matches the pattern.");
        } else {
            System.out.println("URL does not match the pattern.");
        }		
	}
}
