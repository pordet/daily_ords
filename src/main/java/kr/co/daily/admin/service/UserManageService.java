package kr.co.daily.admin.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.security.ReloadableFilterInvocationSecurityMetadataSource;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.AuthManageDAO;
import kr.co.daily.admin.dao.MenuManageDAO;
import kr.co.daily.admin.dao.ResourceDAO;
import kr.co.daily.admin.dao.UserGroupDAO;
import kr.co.daily.admin.dao.UserManageDAO;
import kr.co.daily.admin.dao.UserRoleDAO;

@Service
public class UserManageService {

	@Autowired UserManageDAO dao;
	@Autowired UserGroupDAO userGroupDao;
	@Autowired UserRoleDAO roleDao;
	@Autowired AuthManageDAO authDao;
	@Autowired MenuManageDAO menuDao;
	@Autowired ResourceDAO rsrcDao;
	@Resource(name="reloadableFilterInvocationSecurityMetadataSource") 
	ReloadableFilterInvocationSecurityMetadataSource securitySrc;

	public PageList<CustomMap> selectUserList(CommandMap map) {
		return dao.selectUserList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public PageList<CustomMap> selectUserGroupList(CommandMap map) {
		return userGroupDao.selectUserGroupList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public void insertUsr(CommandMap map) {
		dao.insertUsr(map.getMap());
	}

	public void modifyUsr(CommandMap map) {
		dao.modifyUsr(map.getMap());
	}

	public void deleteUsr(CommandMap map) {
		dao.deleteUsr(map.getMap());
	}

	public void insertUsrGroup(CommandMap map, List<String> rolelist) throws Exception {
		userGroupDao.insertUsrGroup(map.getMap());
		for(String id_role:rolelist) {
			CommandMap roleMap = new CommandMap();
			roleMap.put("id_group",map.getString("id_group"));
			roleMap.put("id_role", id_role);
			authDao.insertGroupRole(roleMap.getMap());
		}
		securitySrc.reload();
	}

	public void modifyUsrGroup(CommandMap map,List<String> delList,List<String> insList) throws Exception {
		userGroupDao.modifyUsrGroup(map.getMap());
		for(String id_role:insList) {
			CommandMap roleMap = new CommandMap();
			roleMap.put("id_group",map.getString("id_group"));
			roleMap.put("id_role", id_role);
			authDao.insertGroupRole(roleMap.getMap());
		}
		for(String id_group_role:delList) {
			CommandMap roleMap = new CommandMap();
			roleMap.put("id_group_role",id_group_role);
			authDao.deleteGroupRole(roleMap.getMap());
		}
		securitySrc.reload();
	}

	public void deleteUsrGroup(CommandMap map) throws Exception {
		userGroupDao.deleteAllUsrInUserGroup(map.getMap());
		userGroupDao.deleteUsrGroup(map.getMap());
		securitySrc.reload();
	}

	public CustomMap getUserGroup(CommandMap map) {
		return userGroupDao.getUserGroup(map.getMap());
	}

	public CustomMap getUser(CommandMap map) {
		return dao.getUser(map.getMap());
	}

	public CustomMap getUserByAlias(CommandMap map) {
		return dao.getUserByAlias(map.getMap());
	}

	public List<CustomMap> selectGroupList(CommandMap  map) {
		return userGroupDao.selectGroupList(map.getMap());
	}

}
