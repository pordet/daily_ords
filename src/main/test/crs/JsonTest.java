package crs;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import daily.common.util.StrUtil;

public class JsonTest {
	@Test
	public void jsonTest() {
		JSONParser parser = new JSONParser();

		try {
			FileReader reader = new FileReader("d:/test/test.json");
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			
			reader.close();
			Map resMap = (Map)jsonObject.get("response");
			Map resHeadMap = (Map)resMap.get("header");
			Map resBodyMap = (Map)resMap.get("body");
			try {
				Integer totCnt = Integer.parseInt(resBodyMap.get("totalCount").toString());
				int totPage = totCnt / 10;
				int restRow = totCnt % 10;
				if(restRow!=0){
					totPage++;
				}
//				for(int i=0;i<totPage;i++) {
//					System.out.println("페이지 번호:"+(i+1));
//				}
				System.out.println("총 페이지:"+totPage);

				JSONArray arr = (JSONArray)resBodyMap.get("items");
				List<String> keywordList = new ArrayList<String>();
				keywordList.add("구입");
				keywordList.add("계약");
				keywordList.add("고등 구매");
				for(Object ino : arr) {
					if(ino instanceof JSONObject) {
						JSONObject json = (JSONObject)ino;
						if(json.get("bsnsDivNm").toString().trim().equals("물품")) {
							System.out.println("bsnsDivNm:"+json.get("bsnsDivNm"));
							String cntrctNm = json.get("cntrctNm").toString().trim();
							System.out.println("cntrctNm:"+cntrctNm);
							if(StrUtil.containsMidString(keywordList,cntrctNm)) {
								String matchingKeyword = StrUtil.matchingKeywordByContainsMidString(keywordList,cntrctNm);
								System.out.println("매칭된 키워드 : "+matchingKeyword+",계약명번호:"+json.get("cntrctNo")+",계약명:"+json.get("cntrctNm"));
								for(Object key:json.keySet()) {
									if(key instanceof String) {
										System.out.println(key+":"+json.get(key));
									}
								}
							}
						}
					}else {
						System.out.print("ino Class명:"+ino.getClass());
					}
				}
			}catch(Exception e) {
				System.out.print("오류발생:"+e.getClass().getName());
				e.printStackTrace();
			}
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
}
