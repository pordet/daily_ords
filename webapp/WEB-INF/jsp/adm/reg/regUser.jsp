<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자관리" />
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
	<script charset="UTF-8" src="<c:url value='/resource/js/cmm/string_util.js'/>"></script>
	<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/user_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<c:choose>
					<c:when test="${empty param.user_seq}">
						<span id="cur_title">사용자 등록</span>
					</c:when>
					<c:otherwise>
						<span id="cur_title">사용자 수정</span>
					</c:otherwise>
				</c:choose>
			</div>
			
			<div class="page-content-wrap">
				<form id="frm" name="frm" method="post">
					<input type="hidden" id="doubleYn" value="Y">
					<input type="hidden" name="insert_yn" id="insert_yn" value="${(map eq null ? 'Y':'N')}">
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}">
					<input type="hidden" name="user_seq" id="user_seq" value="${param.user_seq}">
					<input type="hidden" name="page" id="page" value="${param.page}">
				  	<input type="hidden" name="search_col" id="search_col" value="${param.search_col}">
				  	<input type="hidden" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
					<div class="adm-register-form">
						<table>
							<colgroup>
								<col width="150px;">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<td>사용자 ID</td>
									<td>
										<div class="single-row">
											<input type="text" id="id_user" name="id_user" value="${map.id}" onkeyup="fn_inputKey('7', this);" placeholder="영문,숫자만 입력하십시오.">
											<button type="button" class="btn-primary" onclick="javascript:checkDouble();" id="checkBtn"
												<c:if test="${param.insert_yn eq 'N'}"> disabled</c:if>>
												중복체크
											</button>
										</div>
									</td>
								</tr>
								<tr>
									<td>암호</td>
									<td>
										<input type="password"  name="user_pwd" id="user_pwd" value="" placeholder="${(empty map.id?'':'암호 수정시에만 암호 입력')}">
									</td>
								</tr>
								<tr>
									<td>암호확인</td>
									<td>
										<input type="password"  id="user_pwd1" value="" value="" placeholder="${(empty map.id?'':'암호 수정시에만 암호확인 입력')}">
									</td>
								</tr>
								<tr>
									<td>사용자 명</td>
									<td>
										<input type="text"  name="user_nm" id="user_nm" value="${map.name}">
									</td>
								</tr>
								<tr>
									<td>사용자 그룹</td>
									<td>
										<select name="id_group" id="id_group">
											<c:forEach var="cg" items="${groupList}">
												<c:choose>
													<c:when test="${map.id_group eq cg.id_group}">
														<option value="${cg.id_group}" selected>${cg.group_nm}</option>
													</c:when>
													<c:otherwise>
														<option value="${cg.id_group}">${cg.group_nm}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<td>이메일</td>
									<td>
										<input type="text"  name="email" id="email" value="${map.email}">
									</td>
								</tr>
								<tr>
									<td>휴대폰 번호</td>
									<td>
										<input type="text"  name="cell_phone_num" id="cell_phone_num" value="${map.cell_phone_num}" onkeyup="fn_inputKey('1', this);" placeholder="숫자만 입력하십시오.">
									</td>
								</tr>
								<tr>
									<td>부서명</td>
									<td>
										<input type="text"  name="dept_nm" id="dept_nm" value="${map.dept_nm}">
									</td>
								</tr>
								<tr>
									<td>직책명</td>
									<td>
										<input type="text"  name="posi_nm" id="posi_nm" value="${map.posi_nm}">
									</td>
								</tr>
								<tr>
									<td>사용여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="use_yn" value="Y" id="ra1" ${(map.use_yn eq 'Y'?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="use_yn" value="N" id="ra2" ${(map.use_yn ne 'Y'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>승인여부</td>
									<td>
										<div class="single-row">
											<div>
												<input type="radio" name="user_status" value="Y" id="ra11" ${(map.user_status eq 'Y'?'checked':'')} />
												<label for="ra1">예</label>
											</div>
											<div>
												<input type="radio" name="user_status" value="N" id="ra12" ${(map.user_status ne 'Y'?'checked':'')} />
												<label for="ra2">아니요</label>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="board-bottom-group">
						<c:if test="${!is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
							<div class="board-bottom-group-rear">
								<button type="button" id="btnInit" onclick="initForm();">
									초기화
								</button>
								<button type="button" class="btn-danger" id="btnDelete" onclick="delData();">
									삭제
								</button>
								<button type="button" class="btn-primary" onclick="javascript:register();">
									저장
								</button>
							</div>
						</c:if>
						<c:if test="${is_guest}">
							<div class="board-bottom-group-front">
								<button type="button" class="btn-default" onclick="javascript:doList();">목록</button>						
							</div>
						</c:if>
					</div>
				</form>
			</div>
		</div>
	</div>


</adm:AdmBody>
<script>
/*
$(function () {
	$("#tab_layer_1").click(function(){
		location.href="<c:url value='/adm/user/userList.do'/>?id_menu=12";
	});
	$("#tab_layer_2").click(function(){
		location.href="<c:url value='/adm/user/userGroupList.do'/>?id_menu=9";
	});
	$("#tab_layer_3").click(function(){
		location.href="<c:url value='/adm/user/roleList.do'/>?id_menu=10";
	});
	$("#tab_layer_4").click(function(){
		location.href="<c:url value='/adm/user/permissionList.do'/>?id_menu=11";
	});
});
*/
</script>
</html>
