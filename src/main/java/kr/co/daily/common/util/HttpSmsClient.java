package kr.co.daily.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Value;

public class HttpSmsClient {
	
	@Value("#{global['sms.api.sendUrl']}")
	public void setSendUri(String val) {
	}
	@Value("#{global['sms.api.username']}")
	public void setUser(String val) {
	}
	@Value("#{global['sms.api.password']}")
	public void setPwd(String val) {
	}
	@Value("#{global['sms.api.smsc']}")
	public void setSmsc(String val) {
	}
	@Value("#{global['sms.api.receipt.uri']}")
	public void setReceiptUri(String val) {
	}
	static final String PATH= "";
	//static final String DLRURL = "http://10.1.21.236:80/receivedlr";
	static final String SMSID = "123456789";
	
	private String path;
	private String username;
	private String password;
	private String smsc;
	private String dlrUrl; //delivery receipt handling URI 
	
	public HttpSmsClient(String path,String username, String password, String smsc, String dlrUrl) {
		this.path = path;
		this.username = username;
		this.password = password;
		this.smsc = smsc;
		this.dlrUrl = dlrUrl;
	}
	
	public String getUrl(String to, String body) throws UnsupportedEncodingException {
		StringBuilder url = new StringBuilder();
		url.append(path)
			.append("username=").append(URLEncoder.encode(username, "UTF-8"))
			.append("&password=").append(URLEncoder.encode(password, "UTF-8"))
			.append("&smsc=").append(URLEncoder.encode(smsc, "UTF-8"))
			.append("&from=nppa")
			.append("&to=").append(URLEncoder.encode(to, "UTF-8"))
			.append("&text=").append(body)
			.append("&smsid=").append(URLEncoder.encode(SMSID, "UTF-8"))
			.append("&dlr-url=").append(URLEncoder.encode(dlrUrl, "UTF-8"));
		
		return url.toString();
	}
	
	public boolean send(String to, String body) throws IOException {
		
		String url = getUrl(to, URLEncoder.encode(body, "UTF-8"));
		System.out.println(url);
		URL obj = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		con.setRequestMethod("GET");
		
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
			return true;
		}else {
			return false;
		}
	}
}
