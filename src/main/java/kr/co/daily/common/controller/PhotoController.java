package kr.co.daily.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.service.FileService;

@Controller
public class PhotoController {
	
	@Autowired private FileService fileService;
	@RequestMapping(value="/comm/getByteImage.do")
	public ResponseEntity<byte[]> getByteImage(String key) {
		CommandMap param = new CommandMap();
		param.put("key", key);
		CustomMap map = fileService.getPhotoInRequestInfo(param);
	    byte[] imageContent = (byte[]) map.get("photo");
	    final HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
	}

}
