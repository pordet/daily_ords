package kr.co.daily.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import daily.common.base.AbstractControllerBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.CodeManageService;

@Controller
public class CodeController extends AbstractControllerBase {

	@Autowired private CodeManageService codeService;
	
	@RequestMapping(value = {"/guest/selectCodeAjax.do","/crsApplicant/selectCodeAjax.do"})
	public String selectGuestCodeAjax(CommandMap commandMap, Model model) throws Exception {
		commandMap = addParamByLoginInfo(commandMap);
		
//		commandMap.put("code_group", GlobalConst.CG_NATION);
		List<CustomMap> codeList = codeService.selectCodeComboByLocale(commandMap);
		model.addAttribute("codeList",codeList);
		return "jsonView";
	}
	
	@RequestMapping("/guest/deleteCodeAjax.do")
	public String deleteCode1Ajax(CommandMap commandMap, Model model) throws Exception {
		commandMap = addParamByLoginInfo(commandMap);
		
//		commandMap.put("code_group", GlobalConst.CG_NATION);
		List<CustomMap> codeList = codeService.selectCodeComboByLocale(commandMap);
		model.addAttribute("codeList",codeList);
		return "jsonView";
	}
	
	@RequestMapping("/comm/selectCodeAjax.do")
	public String selectCodeAjax(CommandMap commandMap, Model model) throws Exception {
		commandMap = addParamByLoginInfo(commandMap);
		
//		commandMap.put("code_group", GlobalConst.CG_NATION);
		List<CustomMap> codeList = codeService.selectCodeComboByLocale(commandMap);
		model.addAttribute("codeList",codeList);
		return "jsonView";
	}
	
	@RequestMapping("/comm/deleteCodeAjax.do")
	public String deleteCodeAjax(CommandMap commandMap, Model model) throws Exception {
		codeService.deleteCode(commandMap);
		return "jsonView";
	}
	
}
