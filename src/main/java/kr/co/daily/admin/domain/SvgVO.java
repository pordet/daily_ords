package kr.co.daily.admin.domain;

public class SvgVO {
	private String id_meta_rel;
	private String id;
	private String id_rel;
	private float top;
	private float left;
	private float width;
	private float height;
	public String getId_meta_rel() {
		return id_meta_rel;
	}
	public void setId_meta_rel(String id_meta_rel) {
		this.id_meta_rel = id_meta_rel;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getTop() {
		return top;
	}
	public String getId_rel() {
		return id_rel;
	}
	public void setId_rel(String id_rel) {
		this.id_rel = id_rel;
	}
	public void setTop(float top) {
		this.top = top;
	}
	public float getLeft() {
		return left;
	}
	public void setLeft(float left) {
		this.left = left;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}

}
