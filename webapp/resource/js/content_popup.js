function showMultiFile(id_meta_data){
	var url = contextPath+"/selectMultiFilesAjax.do";
	var params = {id_meta_data:id_meta_data}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
			console.log("컨텐츠 화면 등록결과");
			console.log(e);
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		var html = '';
				$.each(e.fileList,function(i,d){
//					html+='<tr>';
//					html+='	<td><div class="col-12"><a href="'+contextPath+'/file/multi_file_download.do?id_data_file='+nanToStr(d.id_data_file)+'">'+nanToStr(d.file_nm)+'</a>';
//					html+='	</div></td>';
//					html+='	<td><div class="col-12">'+nanToStr(d.file_desc);
//					html+='	</div></td>';
//					html+='<tr>';
					html+='<tr>';
					html+='	<td tooltip="'+nanToStr(d.file_nm)+'"><a href="'+contextPath+'/file/multi_file_download.do?id_data_file='+nanToStr(d.id_data_file)+'">'+nanToStr(d.file_nm)+'</a>';
					html+='	</td>';
					html+='	<td tooltip="'+nanToStr(d.file_desc)+'">'+nanToStr(d.file_desc);
					html+='	</td>';
					html+='</tr>';
				});
	    		$("#multi_file_list_tab").html(html);
    			$("#multiFileModal").modal('show');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});		
}
function showPopup(id_meta){
	var url = contextPath+"/getMetaDescAjax.do";
	var params = {id_meta:id_meta}
	processing();
	$.ajax({
		url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
	    type: "POST", // HTTP 요청 방식(GET, POST)
	    data : params,
	    dataType: "json" ,
	    success : function(e){
			console.log("컨텐츠 화면 등록결과");
			console.log(e);
	    	endProcessing();
	    	if(e.ret==1){
	    		alertOpen('실패하였습니다.');
	    	}else{
	    		$("#metaDescMsg").html(e.descMap.unit_desc);
    			$("#metaDescPopup").modal('show');
	    	}
	    },
	    error : function(e){
	    	endProcessing();
	    	if(e.status=='403'){
		    	alertOpen('로그아웃된 상태입니다. 다시 로그인하십시오.');
	    	}else if(e.status=='401'){
			    alertOpen('접근 권한이 없습니다.');
	    	}else{
		    	alertOpen('실패하였습니다.');
	    	}
	    }
	});		
}
