package crs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import crs.excel.TableData;

public class PoiTest {
	
	@Test
    public void testReadExcelFile() throws IOException {
        // 엑셀 파일 경로
        String filePath = "D:\\meta\\upload\\연구프로젝트.xlsx";
     // 파일이 존재하는지 확인
        File file = new File(filePath);
        assertTrue(file.exists(), "파일이 존재하지 않습니다.");

        // 파일 크기 확인
        long fileSize = file.length();
        System.out.println("파일 크기: " + fileSize + " bytes");

        // 엑셀 파일을 읽어서 Workbook 객체로 로드
        FileInputStream inputStream = new FileInputStream(file);

        // 엑셀 파일을 읽어서 Workbook 객체로 로드
        Workbook workbook = WorkbookFactory.create(inputStream);

        // 시트 선택 (첫 번째 시트를 선택하는 예제)
        Sheet sheet = workbook.getSheetAt(0);

        // 원하는 셀(예: A1)의 내용을 읽어옴
        Cell cell = sheet.getRow(0).getCell(0);
        String cellValue = cell.getStringCellValue();

        // 특정 값과 비교하여 테스트 검증
        assertEquals("Expected Value", cellValue);

        // Workbook 및 스트림을 닫아야 합니다.
        workbook.close();
        inputStream.close();
    }
	@Test
    public void testExcelFile() throws IOException {
        String filePath = "D:\\meta\\upload\\연구프로젝트.xlsx";
        File file = new File(filePath);

        // 파일이 존재하는지 확인
        assertTrue(file.exists(), "파일이 존재하지 않습니다.");

        // 엑셀 파일을 읽어서 FileInputStream 객체로 로드
        try (FileInputStream inputStream = new FileInputStream(file)) {
            // FileInputStream을 사용하여 엑셀 파일을 처리하는 코드를 작성
            // 예: Workbook 객체로 파일을 읽고 데이터 처리
        	System.out.println("aa");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	@Test
    public void testExcelFile11() throws IOException {
        String filePath = "D:\\meta\\upload\\부사업자 프로젝트.xlsx";
        File file = new File(filePath);

        // 파일이 존재하는지 확인
        assertTrue(file.exists(), "파일이 존재하지 않습니다.");

        // 엑셀 파일을 읽어서 FileInputStream 객체로 로드
        try (FileInputStream inputStream = new FileInputStream(file)) {
            Workbook workbook = WorkbookFactory.create(inputStream);

            // 시트 선택 (첫 번째 시트를 선택하는 예제)
            Sheet sheet = workbook.getSheetAt(0);

            // 원하는 셀(예: A1)의 내용을 읽어옴
            Cell cell = sheet.getRow(0).getCell(0);
            String cellValue = cell.getStringCellValue();

            // 특정 값과 비교하여 테스트 검증
            assertEquals("Expected Value", cellValue);

            // Workbook 및 스트림을 닫아야 합니다.
            workbook.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	@Test
    public void testTableData() {
        TableData tableData = new TableData();

        // 예시 데이터 추가
	}
}