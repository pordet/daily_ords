<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<!DOCTYPE html>
<c:set var="title" value="입찰정보 Open Api 조회"/>
<front:FrontHead title="${title}"/>
<front:FrontDynamicBody currLocale="${locale}" hasPop="true" clazz="newPage" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${title}</span>
			</div>
			
			<div class="page-content-wrap">
			<form name="frm" id="frm" method="post">
				<input type="hidden" id="id_bid_noti" name="id_bid_noti"/>
				<input type="hidden" id="reg_yn" name="reg_yn"/>
				<input type="hidden" id="id_meta_row" name="id_meta_row"/>
				<input type="hidden" id="trg_id_meta_m" name="trg_id_meta_m" value="${param.trg_id_meta_m}"/>
				<input type="hidden" id="sys_code" name="sys_code" value="${param.sys_code}"/>
		     	<div class="search-group-area">
					<div class="search-group-wrap">
						<div class="search-group-single">
							<div class="search-group-single-input">
				              	<div class="tit_cont">검색조건</div>
					          	<select class="form-control mh-form-control" name="search_col">
				          			<option value="" ${(param.search_col eq ''? 'selected':'')}>전체</option>
					          	<c:forEach var="map" items="${headerList}" varStatus="i">
					                <option value="${map.id_bid}" ${(param.search_col eq map.id_bid? 'selected':'')}>${map.bid_col_short_nm}</option>
					          	</c:forEach>
					       		</select>
		              			<div class="tit_cont">검색어</div>
<%-- 		              			<input type="text" class="form-control mh-form-control" name="search_keyword" value="${(empty param.search_keyword ?'':param.search_keyword)}"> --%>
								<input type="text" class="form-control mh-form-control" name="search_keyword" id="search_keyword" value="${paramMap.search_keyword}">
		              			<div class="tit_cont">검색기간</div>
		              			<input type="date" class="form-control mh-form-control date_input" name="search_srt_dt" value="${paramMap.search_srt_dt}">
		              			-
		              			<input type="date" class="form-control mh-form-control date_input" name="search_end_dt" value="${paramMap.search_end_dt}">
								<button type="button" class="btn-primary" onclick="javascript:search();">
									<span class="btn_search"></span>
									검색
								</button>
			        		</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
			   		</div>
			   	</div>
			   </div>
				<div class="dbBtnbox">
					<div class="l">
					</div>
					<div class="r">
						<button type="button" class="btn h34 fl" onClick="javascript:openRegContreg();"><span class="btn_add"></span>수동입력</button>
					</div>					
				</div>
	            <div class="grid-area">
                    <!-- 그리드 해더영역 -->
                    <div class="grid-header">
                        <c:forEach var="map" items="${headerList}" varStatus="i">
                        <div class="header-unit">
                        <input type="hidden" id="h_${map.bid_col_cd}" value="${map.id_bid}">
                        ${map.bid_col_short_nm}<ctrl:orderMark val="${map.id_bid}" sidx="${param.sidx}" sord="${param.sord}"/>
                        </div><!-- 각 div별로 style: 사이즈 잡아줍니다.  -->
                        </c:forEach>
                    </div>
                    <!-- //그리드 해더영역 -->
	                <!-- 그리드 컨텐츠영역 -->
                    <div class="grid-contents">
                    <c:forEach var="low" items="${list}" varStatus="i">
                        <div class="grid-loof-cont" id="row_${i.count}" ondblclick="javascript:openRegContreg('${low.id_service}','${low.id_bid_noti}','${low.reg_yn}','${low.id_meta_row}')">
                        <c:forEach var="col" items="${headerList}" varStatus="j">
                        	<c:set var="col_nm" value="${col.bid_col_cd}"/>
                         <div class="cont-unit">
                         <c:choose>
                         	<c:when test="${empty col.link_yn || col.link_yn ne 'Y'}">
                         	${low[col_nm]}
                         	</c:when>
                         	<c:otherwise>
                         	<a href="${low[col_nm]}" target="blank">${low[col_nm]}</a>
                         	</c:otherwise>
                         </c:choose>
           				 </div>
                    	</c:forEach>
                        </div>
                    </c:forEach>
                    </div>
	                        <!-- //그리드 컨텐츠영역 -->
	            </div>
                   <!--//그리드영역-->
                   <!-- paging -->
				<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
				<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
				<input type="hidden" name="sidx" id="sidx" value="${(empty param.sidx?'':param.sidx)}"/>
				<input type="hidden" name="sord" id="sord" value="${(empty param.sord?'':param.sidx)}"/>
				<input type="hidden" name="page" id="page" value="${(empty param.page?1:param.page)}"/>
                   <!-- //paging -->
			</form>
	    </div>
	</div>
</div>
<script>
function doList(page){
	$("#page").val(page);
	var frm =document.frm;
		frm.action=contextPath+"/front/bid/bidNotiList.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
function search(){
	$("#sidx").val('');
	doList(1);
}
function initSearchForm(){
	$("#search_col option:eq(0)").attr("selected","selected");
	$("#search_keyword").val('');
}
function openRegContreg(id_service,id_bid_noti,reg_yn,id_meta_row){
	$("#id_service").val(id_service);
	$("#id_bid_noti").val(id_bid_noti);
	$("#reg_yn").val(reg_yn);
	$("#id_meta_row").val(id_meta_row);
	var frm =document.frm;
		frm.action=contextPath+"/front/meta/regMetaData.do";
// 	if(page!="")
// 		frm.page.value=page;
//	frm.action="/excelDownload.do";
	processing();
	frm.submit();
// 	var frm =document.frm;
// 	var url = contextPath+"/adm/menu/menuList.do?id_menu=${param.id_menu}";
// 	var id_parent_menu = "${param.id_parent_menu}";
// 	if(id_parent_menu){
// 		url+="&id_parent_menu="+id_parent_menu;
// 	}
// 	location.href=url;
}
</script>
</front:FrontDynamicBody>
</html>