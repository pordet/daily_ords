<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="권한 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
		<script charset="UTF-8" src="<c:url value='/resource/js/adm/reg/perm_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="newPage" pageTitle="${pageTitle}" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
									<select name="search_col" id="search_col">
										<option value="ug" ${(param.search_col eq 'ug'? 'selected':'')}>URL</option>
										<option value="mn" ${(param.search_col eq 'mn'? 'selected':'')}>메뉴명</option>
										<option value="em" ${(param.search_col eq 'em'? 'selected':'')}>Role 이름</option>
									</select>
									<input type="text" class="" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
					<input type="hidden" name="id_resource_role" id="id_resource_role"/>
					<input type="hidden" name="id_resource" id="id_resource"/>
				</form>
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button class="btn-primary" onClick="javascript:openReg();">권한 등록</button>
							<button class="btn-danger" onClick="javascript:deleteList();">권한 삭제</button>
						</div>					
					</div>
					
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									URL
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									URL/MENU
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									권한 적용 범위
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									Role 이름
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
	                    <!-- //그리드 해더영역 -->
		                <!-- 그리드 컨텐츠영역 -->
						<div class="grid-contents">
						<form id="listFrm" name="listFrm">
						
							<input type="hidden" name="search_col" id="list_search_col" value="${param.search_col}"/>
							<input type="hidden" name="search_keyword" id="list_search_keyword" value="${param.search_keyword}"/>
							<input type="hidden" name="id_menu" id="list_id_menu" value="${param.id_menu}"/>
							<input type="hidden" name="sidx" id="list_sidx" value="${param.sidx}"/>
							<input type="hidden" name="sord" id="list_sord" value="${param.sord}"/>
							<input type="hidden" name="page" id="list_page" value="${param.page}"/>
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_resource_role}','${map.id_resource}')">
									<div class="check-unit">
										<input type="checkbox" value="${map.id_resource}" name="del_code" id="del_code${i.count}">
									</div>
									<c:choose>
										<c:when test="${map.resource_type eq 'menu'}">
									<div class="cont-unit desc-unit">${map.menu_depth} Depth / 메뉴명 : ${map.resource_pattern}</div>
										</c:when>
										<c:otherwise>
									<div class="cont-unit desc-unit">${map.resource_pattern}</div>
										</c:otherwise>
									</c:choose>
									<div class="cont-unit">${map.resource_type}</div>
									<div class="cont-unit">${map.sort_order}</div>
									<div class="cont-unit">${map.role_nm}</div>
								</div>
							</c:forEach>
						</form>
						</div>
					</div>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>

			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
$(function () {
// 	var act_type = "${param.act_type}";
// 	var ret = "${param.ret}";
// 	if(act_type =='D'){
// 		if(ret==0){
// 			infoOpen('성공하였습니다.');
// 		}else if(ret==1){
//     		alertOpen('실패하였습니다.');
// 		}
// 	}
});
function openReg1(id_resource_role,id_resource){
	if(id_resource_role){
		location.href="<c:url value='/adm/auth/regResourceRole.do'/>?id_menu=${param.id_menu}&id_resource_role="+id_resource_role+"&id_resource="+id_resource;
	}else{
		location.href="<c:url value='/adm/auth/regResourceRole.do'/>?id_menu=${param.id_menu}";
	}
}
function initSearchForm(){
	$("#search_keyword").val('');
}
function openReg(id_resource_role,id_resource){
	var frm =document.frm;
	if(id_resource_role){
		$("#id_resource_role").val(id_resource_role);
	}
	if(id_resource){
		$("#id_resource").val(id_resource);
	}
	
	frm.action=contextPath+"/adm/auth/regResourceRole.do";
//frm.action="/excelDownload.do";
	frm.submit();
}

function search(){
	doList(1);
}
function doList(page){
	processing();
	var frm =document.frm;
	frm.action=contextPath+"/adm/auth/permissionList.do";
		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
</script>
</html>
