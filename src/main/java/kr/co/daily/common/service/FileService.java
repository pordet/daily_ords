package kr.co.daily.common.service;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import daily.common.base.AbstractServiceBase;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.common.dao.FileDAO;

@Service
public class FileService extends AbstractServiceBase {
	@Autowired private FileDAO fileDao;
	public void insertFileInfo(CommandMap param) {
		fileDao.insertFileInfo(param.getMap());
	}
	
	public void deleteFileInfo(CommandMap param) {
		CustomMap real = fileDao.getRealFileInfo(param.getMap());
		String phsicalLocation = real.getString("phsicalLocation");
		String phsicalFilename = real.getString("phsicalFilename");
		File file = new File(phsicalLocation,phsicalFilename);
		if(file.delete()) {
			fileDao.deleteFileInfo(param.getMap());
		}
	}
	
	public List<CustomMap> selectDeleteFileList(CommandMap param){
		return fileDao.selectDeleteFileList(param.getMap());
	}

	public CustomMap getPhotoInRequestInfo(CommandMap param) {
		return fileDao.getPhotoInRequestInfo(param.getMap());
	}

	public CustomMap getRealFileInfo(CommandMap param) {
		return fileDao.getRealFileInfo(param.getMap());
	}
}
