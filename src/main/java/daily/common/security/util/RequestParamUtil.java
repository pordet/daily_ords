package daily.common.security.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class RequestParamUtil {
	public static Map<String, Object> getParamMap(HttpServletRequest req) {
		Map<String, Object> parameterMap = req.getParameterMap();
		Map<String, Object> paramMap = new HashMap<>();
		for(String param_nm:parameterMap.keySet()) {
			Object obj=parameterMap.get(param_nm);
			if(obj instanceof String) {
				String str = (String)obj;
				paramMap.put(param_nm, str);
			}else if(obj instanceof String[]) {
				String[] strArr = (String[])obj;
				if(strArr.length==1) {
					paramMap.put(param_nm, strArr[0]);
				}else {
					paramMap.put(param_nm, strArr);
				}
			}
		}
		return paramMap;

	}
}
