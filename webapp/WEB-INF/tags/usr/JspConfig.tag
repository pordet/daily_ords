<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<c:set var="userinfo" value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userMap}"  scope="request"/>
<c:set var="id_user" value="${userinfo.id_user}" scope="request" />
<c:set var="alias" value="${userinfo.id}" scope="request" />
<c:set var="req_url" value="${pageContext.request.requestURI}" scope="request" />
