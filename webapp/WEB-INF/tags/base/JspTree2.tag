<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<commc:RecordTree user_id="${user_id}" locale="${locale}" listName="nodeList"/>
<%@ attribute name="openTreeId" required="true" %>
<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: JspMenu.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>
						<ul class="comm-tree">
						    <c:forEach var="node" items="${nodeList}" varStatus="i">
							<li class="comm-treeview" id="tree_${i.count}">
								<div><a href="#" id="t_${node.nodeCode}" class="js-action-a"><i class="icon_folder0${i.count}"></i>${node.nodeNm}</a></div>
								<c:choose>
									<c:when test="${node.childNodeCnt>0}">
									<ul class="comm-treeview-list">
									    <c:forEach var="child" items="${node.childList}">
									    	<c:choose>
									    		<c:when test="${child.childNodeCnt>0}">
													<li><div><a href="#" id="t_${child.nodeCode}" class="js-action-a"><i class="icon_keyboard"></i>${child.nodeNm}</a></div>
														<ul class="comm-treeview-list">
															<c:forEach var="grandChild" items="${child.childList}">
															<c:choose>
															   <c:when test="${not empty grandChild.nodeVal}">
															       <li id="t_${grandChild.nodeCode}" onclick="javascript:focusField('${grandChild.nodeCode}');">${grandChild.nodeNm} : ${grandChild.nodeVal}</li>
															   </c:when>
															   <c:otherwise>
															       <li id="t_${grandChild.nodeCode}" onclick="javascript:focusField('${grandChild.nodeCode}');">${grandChild.nodeNm}</li>
															   </c:otherwise>
															</c:choose>
															</c:forEach>
														</ul>
													</li>
									    		</c:when>
									    		<c:otherwise>
															<c:choose>
															   <c:when test="${not empty grandChild.nodeVal}">
															   	   <li><div><a href="#" id="t_${child.nodeCode}" class="js-action-a" onclick="javascript:focusField('${child.nodeCode}');"><i class="icon_keyboard"></i>${child.nodeNm} : ${child.nodeVal}</a></div></li>
															   </c:when>
															   <c:otherwise>
															       <li><div><a href="#" id="t_${child.nodeCode}" class="js-action-a" onclick="javascript:focusField('${child.nodeCode}');"><i class="icon_keyboard"></i>${child.nodeNm}</a></div></li>
															   </c:otherwise>
															</c:choose>
													
									    		</c:otherwise>
									    	</c:choose>
									    
									    </c:forEach>
									</ul>
									</c:when>
								</c:choose>
						    </c:forEach>
						</ul>
<%--
						    <c:forEach var="node" items="${nodeList}" varStatus="i">
						    ${node}
							</c:forEach>
--%>

<script>
function focusField(id){
	$("#id").focus();
}
function setLabel(id,nm,tgtId){
	if($.trim($("#"+tgtId).val())==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+$("#"+tgtId).val());
	}
}
function setDateLabel(id,nm,tgtId,dateText){
	if(dateText==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+dateText);
	}
}
function setComboLabel(id,nm,tgtId){
	if($.trim($("#"+tgtId+ " option:selected").val())==''){
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm);
	}else{
		$("#"+id).html("<i class=\"icon_keyboard\"></i>"+nm+ " : "+$("#"+tgtId+ " option:selected").text());
	}
}
$(function () {
	treeMenuInit();
	function treeMenuInit(){
		$('.comm-tree li').each(function(){
			if($(this).find($('.comm-treeview-list')).length>0){
				$('.comm-treeview-list',this).hide();
				$(this).prepend('<button type="button" class="comm-tree-expander"></button>')
			};
		});

		$(document).on('click', '.comm-tree .comm-tree-expander',function(){
			var thisBtn, thisItem;
			if($(this).hasClass('comm-tree-expander')){
				thisBtn = $(this);
			}else{
				thisBtn = $(this).closest('li').children('.comm-tree-expander');
			}
			 thisItem = thisBtn.parent();
			
			if (thisItem.is('.tree-open')) {
				thisBtn.add(thisItem.find('.tree-open').find('.comm-tree-expander'))
				thisItem.add(thisItem.find('.tree-open')).removeClass('tree-open').find('.comm-treeview-list').hide();
			} else {
				thisItem.children('.comm-treeview-list').stop().show();
				thisItem.addClass('tree-open');
			}
		});
	};
	$("#tree_${openTreeId}").children('.comm-treeview-list').stop().show();
	$("#tree_${openTreeId}").addClass('tree-open');

});

</script>
