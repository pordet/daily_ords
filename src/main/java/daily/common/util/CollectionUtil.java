package daily.common.util;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Comparator;
import java.util.stream.Collectors;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

public class CollectionUtil {

	public static List<String> getKeyListByMapList(List<CustomMap> list,String key) throws Exception{
		Set<String> rs = new LinkedHashSet<String>();
		List<String> ret = new ArrayList<String>();
		for(CustomMap map:list) {
			rs.add(map.getString(key));
		}
		for(String lhsKey:rs) {
			ret.add(lhsKey);
		}
		return ret;
	}

	public static List<String> getKeyListByMapListAddPrefix(List<CustomMap> list,String key,String prefix) throws Exception{
		Set<String> rs = new LinkedHashSet<String>();
		List<String> ret = new ArrayList<String>();
		for(CustomMap map:list) {
			rs.add(prefix+map.getString(key));
		}
		for(String lhsKey:rs) {
			ret.add(lhsKey);
		}
		return ret;
	}

	public static List<String> getKeyListByMapListAddSuffix(List<CustomMap> list,String key,String suffix) throws Exception{
		Set<String> rs = new LinkedHashSet<String>();
		List<String> ret = new ArrayList<String>();
		for(CustomMap map:list) {
			rs.add(map.getString(key)+suffix);
		}
		for(String lhsKey:rs) {
			ret.add(lhsKey);
		}
		Collections.sort(ret);
		return ret;
	}

	/**
	 * CustomMap List에서 key로 지정한 컬럼값에서 키로 CustomMap의 Map을 가져오는 함수
	 * @param list
	 * @param key
	 * @return
	 */
	public static Map<String,CustomMap> getKeyMapByMapList(List<CustomMap> list,String key){
//		Set<String> rs = new LinkedHashSet<String>();
		Map<String,CustomMap> ret = new HashMap<String,CustomMap>();
		for(CustomMap map:list) {
			ret.put(map.getString(key),map);
		}
		return ret;
	}

	public static Map<String,CustomMap> getKeyMapByMapList(List<CustomMap> list,String key1,String key2){
//		Set<String> rs = new LinkedHashSet<String>();
		Map<String,CustomMap> ret = new HashMap<String,CustomMap>();
		for(CustomMap map:list) {
			ret.put(map.getString(key1)+"_"+map.getString(key2),map);
		}
		return ret;
	}

	public static MultiValueMap<String,String> getMultValueMapToMap(Map<String, Object> map) {
		MultiValueMap<String,String> ret = new LinkedMultiValueMap<String,String>();
		for(String key : map.keySet()) {
			ret.add(key, String.valueOf(map.get(key)));
		}
		return ret;
	}

	public static CommandMap getCommandMapByMultiValueMap(MultiValueMap<String, String> param) {
		CommandMap ret = new CommandMap();
		for(String key : param.keySet()) {
			if(param.get(key)!=null)
				ret.put(key, param.get(key).get(0));
			else
				ret.put(key, "");
		}
		return ret;
	}
	public static CommandMap getCommandMapByCustomMap(CustomMap param) {
		CommandMap ret = new CommandMap();
		if(param!=null) {
			for(Object key : param.keySet()) {
				if(param.get(key)!=null)
					ret.put(String.valueOf(key), param.get(key));
				else
					ret.put(String.valueOf(key), "");
			}
		}
		return ret;
	}
	
	public static CommandMap getCommandMapByMap(Map<String, Object> param) {
		CommandMap ret = new CommandMap();
		if(param != null) {
			for(Object key : param.keySet()) {
				if(param.get(key)!=null)
					ret.put(String.valueOf(key), param.get(key));
				else
					ret.put(String.valueOf(key), "");
			}
		}
		return ret;
	}

	public static List<String> getCheckedListByCommandMap(CommandMap commandMap, String inkey) throws Exception{
		Map<String, Object> map = commandMap.getMap();
		List<String> ret = new ArrayList<String>();
		for(String key : map.keySet()) {
			if(key.startsWith(inkey)) {
				ret.add(String.valueOf(map.get(key)));
			}
		}
		return ret;
	}

	public static Map<String,String> getKeyListByCommandMap(CommandMap commandMap, String inkey) {
		Map<String, Object> map = commandMap.getMap();
		Map<String,String> ret = new HashMap<String,String>();
		for(String key : map.keySet()) {
			if(key.startsWith(inkey)) {
				String newKey = key.substring(inkey.length());
				ret.put(newKey, String.valueOf(map.get(key)));
			}
		}
		return ret;
	}

	public static CustomMap getCustomMapByCommandMap(CommandMap commandMap) {
		Map<String, Object> map = commandMap.getMap();
		CustomMap ret = new CustomMap();
		for(Object key : map.keySet()) {
			if(map.get(key)!=null)
				ret.put(String.valueOf(key), map.get(key));
			else
				ret.put(String.valueOf(key), "");
		}
		return ret;
	}
	public static LinkedHashMap<String, CommandMap> sortMapByKey(Map<String, CommandMap> map) {
	    List<Map.Entry<String, CommandMap>> entries = new LinkedList<>(map.entrySet());
	    Collections.sort(entries, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));

	    LinkedHashMap<String, CommandMap> result = new LinkedHashMap<>();
	    for (Map.Entry<String, CommandMap> entry : entries) {
	        result.put(entry.getKey(), entry.getValue());
	    }
	    return result;
	}

	public static Map<String, List<CustomMap>> getKeyMapByKeyUsingTwoCustomMap(List<CustomMap> keyList,
			List<CustomMap> cdList, String key_nm) {
		Map<String, List<CustomMap>> ret = new HashMap<String,List<CustomMap>>();
		return ret;
	}
	public static List<?> transSortedListUsingMap(Map<?, ?> map) {
        return map.entrySet()
            .stream()
            .sorted(Comparator.comparing(entry -> entry.getKey().toString())) // 키를 문자열로 변환하여 정렬
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());
    }
	public static <K, V> List<V> getSortedListUsingMap(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Comparator.comparing(entry -> entry.getKey().toString())) // 키를 문자열로 변환하여 정렬
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
	// data가 List<Map<CommandMap, List<CommandMap>>> 형태인 경우 출력하는 함수
//	public static void printDataBasedOnType(Object data, String listTitle) {
//	    System.out.println("--------------------" + listTitle + " 출력");
//
//	    if (data instanceof List) {
//	        List<?> dataList = (List<?>) data;
//	        for (Object obj : dataList) {
//	            printDataBasedOnType(obj, "Inner " + listTitle); // 재귀 호출
//	        }
//	    } else if (data instanceof Map) {
//	        Map<?, ?> mapData = (Map<?, ?>) data;
//	        for (Map.Entry<?, ?> entry : mapData.entrySet()) {
//	            if (entry.getKey() instanceof Main && entry.getValue() instanceof List) {
//	                Main key = (Main) entry.getKey();
//	                List<?> value = (List<?>) entry.getValue();
//	                System.out.println("id_parent: " + key.getInt("id_parent", 0) + ", id: " + key.getInt("id", 0) + ", depth: " + key.getInt("depth", 0));
//	                for (Object commandMap : value) {
//	                    if (commandMap instanceof Map) {
//	                        Map<?, ?> commandMapAsMap = (Map<?, ?>) commandMap;
//	                        for (Map.Entry<?, ?> en : commandMapAsMap.entrySet()) {
//	                            String enkey = String.valueOf(en.getKey());
//	                            String enVal = String.valueOf(en.getValue());
//	                            System.out.print("Key:" + enkey + ", Value:" + enVal + " , ");
//	                        }
//	                    }
//	                }
//	            }
//	        }
//	    }
//
//	    System.out.println();
//	    System.out.println("--------------------" + listTitle + " 출력 종료");
//	}
	// data가 List<Map<CommandMap, List<CommandMap>>> 형태인 경우 출력하는 함수
	public static void printDataBasedOnType(Object data, String listTitle) {
	    System.out.println("--------------------" + listTitle + " 출력");

	    if (data instanceof List) {
	        List<?> dataList = (List<?>) data;
	        for (Object obj : dataList) {
	            printDataBasedOnType(obj, "Inner " + listTitle); // 재귀 호출
	        }
	    } else if (data instanceof Map) {
	        Map<?, ?> mapData = (Map<?, ?>) data;
	        for (Map.Entry<?, ?> entry : mapData.entrySet()) {
	            if (entry.getKey() instanceof List && entry.getValue() instanceof Map) {
	                // Case 1: entry.getKey() is List and entry.getValue() is Map
	                List<?> keyList = (List<?>) entry.getKey();
	                Map<?, ?> valueMap = (Map<?, ?>) entry.getValue();

	                // 출력 keyList
	                for (Object keyItem : keyList) {
	                    if (keyItem instanceof Map) {
	                        Map<?, ?> keyMap = (Map<?, ?>) keyItem;
	                        for (Map.Entry<?, ?> keyEntry : keyMap.entrySet()) {
	                            String key = String.valueOf(keyEntry.getKey());
	                            String value = String.valueOf(keyEntry.getValue());
	                            System.out.println("Key:" + key + ", Value:" + value);
	                        }
	                    }
	                }

	                // 출력 valueMap
	                for (Map.Entry<?, ?> en : valueMap.entrySet()) {
	                    String enkey = String.valueOf(en.getKey());
	                    String enVal = String.valueOf(en.getValue());
	                    System.out.print("Key:" + enkey + ", Value:" + enVal + " , ");
	                }
	            } else if (entry.getKey() instanceof Map && entry.getValue() instanceof Map) {
	                // Case 2: entry.getKey() is Map and entry.getValue() is Map
	                Map<?, ?> keyMap = (Map<?, ?>) entry.getKey();
	                Map<?, ?> valueMap = (Map<?, ?>) entry.getValue();

	                // 출력 keyMap
	                for (Map.Entry<?, ?> keyEntry : keyMap.entrySet()) {
	                    String key = String.valueOf(keyEntry.getKey());
	                    String value = String.valueOf(keyEntry.getValue());
	                    System.out.println("Key:" + key + ", Value:" + value);
	                }

	                // 출력 valueMap
	                for (Map.Entry<?, ?> en : valueMap.entrySet()) {
	                    String enkey = String.valueOf(en.getKey());
	                    String enVal = String.valueOf(en.getValue());
	                    System.out.print("Key:" + enkey + ", Value:" + enVal + " , ");
	                }
	            } else if (entry.getKey() instanceof Map && entry.getValue() instanceof List) {
	                // Case 3: entry.getKey() is Map and entry.getValue() is List
	                Map<?, ?> keyMap = (Map<?, ?>) entry.getKey();
	                List<?> valueList = (List<?>) entry.getValue();

	                // 출력 keyMap
	                for (Map.Entry<?, ?> keyEntry : keyMap.entrySet()) {
	                    String key = String.valueOf(keyEntry.getKey());
	                    String value = String.valueOf(keyEntry.getValue());
	                    System.out.println("Key:" + key + ", Value:" + value);
	                }

	                // 출력 valueList
	                for (Object valueItem : valueList) {
	                    if (valueItem instanceof Map) {
	                        Map<?, ?> valueMap = (Map<?, ?>) valueItem;
	                        for (Map.Entry<?, ?> en : valueMap.entrySet()) {
	                            String enkey = String.valueOf(en.getKey());
	                            String enVal = String.valueOf(en.getValue());
	                            System.out.print("Key:" + enkey + ", Value:" + enVal + " , ");
	                        }
	                    }
	                }
	            } else if (entry.getKey() instanceof List && entry.getValue() instanceof List) {
	                // Case 4: entry.getKey() is List and entry.getValue() is List
	                List<?> keyList = (List<?>) entry.getKey();
	                List<?> valueList = (List<?>) entry.getValue();

	                // 출력 keyList
	                for (Object keyItem : keyList) {
	                    if (keyItem instanceof Map) {
	                        Map<?, ?> keyMap = (Map<?, ?>) keyItem;
	                        for (Map.Entry<?, ?> keyEntry : keyMap.entrySet()) {
	                            String key = String.valueOf(keyEntry.getKey());
	                            String value = String.valueOf(keyEntry.getValue());
	                            System.out.println("Key:" + key + ", Value:" + value);
	                        }
	                    }
	                }

	                // 출력 valueList
	                for (Object valueItem : valueList) {
	                    if (valueItem instanceof Map) {
	                        Map<?, ?> valueMap = (Map<?, ?>) valueItem;
	                        for (Map.Entry<?, ?> en : valueMap.entrySet()) {
	                            String enkey = String.valueOf(en.getKey());
	                            String enVal = String.valueOf(en.getValue());
	                            System.out.print("Key:" + enkey + ", Value:" + enVal + " , ");
	                        }
	                    }
	                }
	            }
	        }
	    }

	    System.out.println();
	    System.out.println("--------------------" + listTitle + " 출력 종료");
	}

	/**
	 * CommandMap에 있는 값을 새로운 CommandMap에 추가하는 static 함수
	 * @param commandMap
	 * @return
	 */
	public static CommandMap getCommandMapFromCommandMap(CommandMap commandMap) {
		Map<String, Object> map = commandMap.getMap();
		CommandMap ret = new CommandMap();
		for(String key : map.keySet()) {
			ret.put(key, map.get(key));
		}
		return ret;
	}

	public static Map<Integer, List<CustomMap>> getKeyMapByList(String key_col, List<CustomMap> cdList) {
		Map<Integer,List<CustomMap>> cdMap = new HashMap<Integer,List<CustomMap>>();
		for(CustomMap cd:cdList) {
			int key = cd.getInt(key_col, 0);
			List<CustomMap> cdL = cdMap.get(key);
			if(cdL==null) {
				List<CustomMap> li = new ArrayList<CustomMap>();
				li.add(cd);
				cdMap.put(key,li);
			}else {
				cdL.add(cd);
				cdMap.put(key, cdL);
			}
		}
		return cdMap;
	}
	public static List<CommandMap> getCommandListByCommandMapAndPrefix(List<CustomMap> baselist,String key,CommandMap in,String prefix,String attrNm){
		List<CommandMap> ret = new ArrayList<CommandMap>();
		for(CustomMap map:baselist) {
			String idMata = map.getString(key,"");
			if(!"".equals(idMata)) {
				String val = in.getString(prefix+idMata,"");
				if(!"".equals(val)) {
					CommandMap cmap = CollectionUtil.getCommandMapByCustomMap(map);
					cmap.put(attrNm, val);
					ret.add(cmap);
				}
			}
		}
		return ret;
	}



}
