package crs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SampleDataToMapList {
    public static void main(String[] args) {
        List<Map<String, Object>> mapList = generateSampleData();
        
        // List<Map> 출력
        for (Map<String, Object> map : mapList) {
            System.out.println(map);
        }
    }

    public static List<Map<String, Object>> generateSampleData() {
        List<Map<String, Object>> mapList = new ArrayList<>();
        
        // 데이터 생성 및 Map에 추가
        Map<String, Object> map1 = new HashMap<>();
        map1.put("id", 1);
        map1.put("parent", 0);
        map1.put("depth", 1);
        mapList.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("id", 2);
        map2.put("parent", 1);
        map2.put("depth", 2);
        mapList.add(map2);

        Map<String, Object> map3 = new HashMap<>();
        map3.put("id", 3);
        map3.put("parent", 1);
        map3.put("depth", 2);
        mapList.add(map3);

        Map<String, Object> map4 = new HashMap<>();
        map4.put("id", 4);
        map4.put("parent", 2);
        map4.put("depth", 3);
        mapList.add(map4);

        Map<String, Object> map5 = new HashMap<>();
        map5.put("id", 5);
        map5.put("parent", 2);
        map5.put("depth", 3);
        mapList.add(map5);

        Map<String, Object> map6 = new HashMap<>();
        map6.put("id", 6);
        map6.put("parent", 3);
        map6.put("depth", 3);
        mapList.add(map6);

        Map<String, Object> map7 = new HashMap<>();
        map7.put("id", 7);
        map7.put("parent", 4);
        map7.put("depth", 4);
        mapList.add(map7);

        Map<String, Object> map8 = new HashMap<>();
        map8.put("id", 8);
        map8.put("parent", 5);
        map8.put("depth", 4);
        mapList.add(map8);

        Map<String, Object> map9 = new HashMap<>();
        map9.put("id", 9);
        map9.put("parent", 6);
        map9.put("depth", 4);
        mapList.add(map9);

        return mapList;
    }
}
