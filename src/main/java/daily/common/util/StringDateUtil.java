package daily.common.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import daily.common.constant.DateEnum;

public class StringDateUtil {

	public static String getToday(String format) {
		String nowDate = LocalDate.now().format(DateTimeFormatter.ofPattern(format));
		
		return nowDate;
	}
	public static String getTodayTime(String format) {
		LocalDateTime cur = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String formatted = cur.format(formatter);
        System.out.println("Current: " + formatted);
		return formatted;
	}
	public static String getPlusTimeFromToday(String format,DateEnum type,long term) {
		LocalDateTime cur = null;
		if(type == DateEnum.DAY) {
			cur = LocalDateTime.now().plusDays(term);
		}else if (type == DateEnum.WEEK){		
			cur = LocalDateTime.now().plusWeeks(term);
		}else if (type == DateEnum.MONTH){		
//			LocalDate aa = LocalDate.now().minusMonths(1);
			cur = LocalDateTime.now().plusMonths(term);
		}else {
			cur = LocalDateTime.now().plusYears(term);
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String formatted = cur.format(formatter);
		return formatted;
	}
	public static String getMinusFromToday(String format,DateEnum type,long term) {
		String ret = null;
		if(type == DateEnum.DAY) {
			ret = LocalDate.now().minusDays(term).format(DateTimeFormatter.ofPattern(format));
		}else if (type == DateEnum.WEEK){		
			ret = LocalDate.now().minusWeeks(term).format(DateTimeFormatter.ofPattern(format));
		}else if (type == DateEnum.MONTH){		
			ret = LocalDate.now().minusMonths(term).format(DateTimeFormatter.ofPattern(format));
		}else {
			ret = LocalDate.now().minusYears(term).format(DateTimeFormatter.ofPattern(format));
		}
		return ret;
	}
	
	public static String getPlusFromToday(String format,DateEnum type,long term) {
		String ret = null;
		if(type == DateEnum.DAY) {
			ret = LocalDate.now().plusDays(term).format(DateTimeFormatter.ofPattern(format));
		}else if (type == DateEnum.WEEK){		
			ret = LocalDate.now().plusWeeks(term).format(DateTimeFormatter.ofPattern(format));
		}else if (type == DateEnum.MONTH){		
//			LocalDate aa = LocalDate.now().minusMonths(1);
			ret = LocalDate.now().plusMonths(term).format(DateTimeFormatter.ofPattern(format));
		}else {
			ret = LocalDate.now().plusYears(term).format(DateTimeFormatter.ofPattern(format));
		}
		return ret;
	}
	
	public static String getDateByFormat(String format,Locale local) {
		 SimpleDateFormat sdf=new SimpleDateFormat(format,local);
		 Date date=new Date();
		 String dateStr=sdf.format(date);
		 return dateStr;		
	}
	public static List<String> getPeriodDateListByFormat(String st_date,String ed_date,String format) {
	    List<String> dateRange = new ArrayList<String>();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(format);
        LocalDate currentDate = LocalDate.parse(st_date, dateFormatter);;
        LocalDate endDate = LocalDate.parse(ed_date, dateFormatter);;
        
        while (!currentDate.isAfter(endDate)) {
            String currDateStr = currentDate.format(dateFormatter);
            dateRange.add(currDateStr);
            currentDate = currentDate.plusDays(1);
            
        }		 
		 return dateRange;		
	}
	public static String getNewFormatByFormat(String st_date,String src_format,String trg_format) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(src_format);
        DateTimeFormatter newDateFormatter = DateTimeFormatter.ofPattern(trg_format);
        LocalDate currentDate = LocalDate.parse(st_date, dateFormatter);
        String currDateStr = currentDate.format(newDateFormatter);
		return currDateStr;		
	}
}
