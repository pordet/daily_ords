package daily.common.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import daily.common.security.service.SecuredObjectService;

public class ReloadableFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	private Logger log = LoggerFactory.getLogger(ReloadableFilterInvocationSecurityMetadataSource.class);
	@Autowired SecuredObjectService service;
	
	private final Map<RequestMatcher,Collection<ConfigAttribute>> requestMap;
	
	public ReloadableFilterInvocationSecurityMetadataSource(LinkedHashMap<RequestMatcher,Collection<ConfigAttribute>> requestMap) {
		this.requestMap = requestMap;
	}
	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		HttpServletRequest req = ((FilterInvocation)object).getRequest(); 
		Collection<ConfigAttribute> result = null;
//		String uriToMatch = req.getRequestURI();
//		String contextPath = req.getContextPath(); // 컨텍스트 루트를 가져옴
//
//		// 컨텍스트 루트를 제외한 URI를 얻기 위해, 컨텍스트 루트 길이만큼 substr 사용
//		String uriWithoutContextPath = uriToMatch.substring(contextPath.length());
//		AntPathRequestMatcher matcher = new AntPathRequestMatcher(uriWithoutContextPath);
//		정확하게 매칭하지 않으면 찾지 못함
//		Collection<ConfigAttribute> resi = requestMap.get(matcher);
		for(Map.Entry<RequestMatcher,Collection<ConfigAttribute>> entry : requestMap.entrySet()) {
			if(entry.getKey().matches(req)) {
				result = entry.getValue();
				break;
			}
		}
		return result;
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		Set<ConfigAttribute> allAttributes = new HashSet<ConfigAttribute>();
		for(Map.Entry<RequestMatcher,Collection<ConfigAttribute>> entry : requestMap.entrySet()) {
			allAttributes.addAll(entry.getValue());
		}
		return allAttributes;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return FilterInvocation.class.isAssignableFrom(clazz);
	}
	
	public void reload() throws Exception{
		log.debug("ZZZZZ:reload() Start");
		LinkedHashMap<RequestMatcher,List<ConfigAttribute>> reloadedMap = service.getRolesAndUrl();
		Iterator<Entry<RequestMatcher,List<ConfigAttribute>>> it = reloadedMap.entrySet().iterator();
		
		requestMap.clear();
		
		while(it.hasNext()) {
			Entry<RequestMatcher,List<ConfigAttribute>> entry = it.next();
			requestMap.put(entry.getKey(), entry.getValue());
		}
	}

}
