package kr.co.daily.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.base.AbstractControllerBase;
import daily.common.util.CollectionUtil;
import daily.common.util.MetaUtil;
import daily.common.util.StrUtil;
import daily.common.util.StringDateUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import kr.co.daily.admin.service.IdentityManageService;
import kr.co.daily.admin.service.MetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.common.service.CodeService;

@Controller
public class IdentityManageController extends AbstractControllerBase {
	@Autowired SessionLocaleResolver localeResolver;
	@Autowired private IdentityManageService service;
	@Autowired private CodeService codeService;

	private static String FILE_DOWNLOAD_PATH;
	private static String FILE_UPLOAD_PATH;
	
    @Value("#{global['identity.excel.down']}")
	public void setFileDownloadPath(String fileUploadPath) {
    	FILE_DOWNLOAD_PATH = fileUploadPath;
	}
	
    @Value("#{global['identity.excel.upload']}")
	public void setFileUploadPath(String fileUploadPath) {
    	FILE_UPLOAD_PATH = fileUploadPath;
	}
	@RequestMapping("/adm/meta/identityMList.do")
	public String identityMList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_IDEN);
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		List<CustomMap> srchParamList = service.selectSearchParamList(commandMap);
		CustomMap idenMap = service.getIdentityM(commandMap);
		model.addAttribute("idenMap", idenMap);
		if(srchParamList!=null) {
			model.addAttribute("srchParamList", srchParamList);
		}
		List<CustomMap> idenCdList = service.selectIdentityCdList(commandMap);
		List<CustomMap> cdList = service.selectCdListForIdentity(commandMap);
		Map<String,List<CustomMap>> cdMap = CollectionUtil.getKeyMapByKeyUsingTwoCustomMap(idenCdList,cdList,"id_meta");
		if(cdList!=null) {
			model.addAttribute("cdList", cdList);
		}
		List<CustomMap> headerList = service.selectIdentityListForHead(commandMap);
		model.addAttribute("headerList", headerList);
		PageList<CustomMap> resultList = service.selectIdentityMList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/identityMList";
	}
	@RequestMapping("/adm/meta/identityList.do")
	public String identityList(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		commandMap.put("meta_m_type", GlobalConst.META_M_TYPE_IDEN);
		CommandMap map = new CommandMap();
		Locale locale = localeResolver.resolveLocale(req);
		map.put("locale", locale.toString());
		List<CustomMap> srchParamList = service.selectSearchParamList(commandMap);
		CustomMap idenMap = service.getIdentityM(commandMap);
		model.addAttribute("idenMap", idenMap);
		if(srchParamList!=null) {
			model.addAttribute("srchParamList", srchParamList);
		}
		List<CustomMap> idenCdList = service.selectIdentityCdList(commandMap);
		List<CustomMap> cdList = service.selectCdListForIdentity(commandMap);
		Map<String,List<CustomMap>> cdMap = CollectionUtil.getKeyMapByKeyUsingTwoCustomMap(idenCdList,cdList,"id_meta");
		if(cdList!=null) {
			model.addAttribute("cdList", cdList);
		}
		List<CustomMap> headerList = service.selectIdentityListForHead(commandMap);
		model.addAttribute("headerList", headerList);
		PageList<CustomMap> resultList = service.selectIdentityMList(commandMap);
	    this.putModelListPage(resultList, model);
		return "/adm/identityList";
	}
	@RequestMapping("/adm/meta/regIdentityM.do")
	public String regIdentityM(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
		Map<String,CustomMap> inputMap = new HashMap<String,CustomMap>();
		CommandMap trgMap = new CommandMap();
		trgMap.put("id_meta_m", commandMap.getString("trg_id_meta_m"));
		CustomMap map = service.getIdentityM(trgMap);
		model.addAttribute("map", map);
		List<CustomMap> colList = service.selectIdentity(commandMap);
		List<CustomMap> headerList = new ArrayList<>();
		//Map<id_parent_meta,List<child>
		Map<Integer,List<CustomMap>> childMap = new HashMap<>();
		Map<Integer,CustomMap> curMap = new HashMap<>();
		for(CustomMap col:colList) {
			if(col.getInt("meta_depth")==1) {
				headerList.add(col);
				curMap.put(col.getInt("id_meta"), col);
			}else {
				int id_parent_meta = col.getInt("id_parent_meta");
				List<CustomMap> childList = childMap.get(col.getInt("id_parent_meta"));
				if(childList==null) {
					childList = new ArrayList<>();
					childList.add(col);
					childMap.put(col.getInt("id_parent_meta"), childList);
				}else {
					childList.add(col);
				}
				curMap.get(id_parent_meta).put("child_list", childList);
			}
		}
		model.addAttribute("colList", headerList);
		List<CustomMap> cdList = service.selectIdentityCdListByIdMetaM(commandMap);
		Map<Integer,List<CustomMap>> cdMap = CollectionUtil.getKeyMapByList("id_meta",cdList);
		model.addAttribute("cdMap", cdMap);
		return "/adm/reg/regIdentityM";
	}
//	@RequestMapping("/adm/meta/regIdentityAjax.do")
//	public String regIdentityAjax(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		int ret = GlobalConst.ACT_SUCCESS;
//		try {
//	        commandMap = addParamByLoginInfo(commandMap);
//	        if(commandMap.getInt("login_usr_seq",0)==0) {
//				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
//	        }else {
//	        	service.insertIdentity(commandMap);
//	        }
//		}catch(Exception e) {
//			ret = GlobalConst.ACT_FAIL;
//		}
//		model.addAttribute("ret",ret);
//		return "jsonView";
//	}
	@RequestMapping("/adm/meta/insertIdentityMAjax.do")
	public String insertIdentityM(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	List<CustomMap> idnList = service.selectIdentity(commandMap);
	        	Map idnMap = CollectionUtil.getKeyMapByMapList(idnList, "id_meta");
	        	Map<String, CustomMap> metaMap = CollectionUtil.getKeyMapByMapList(service.selectIdentity(commandMap), "id_meta");
	        	Map<String,String> idMetaMap = CollectionUtil.getKeyListByCommandMap(commandMap,"id_meta_");
	        	Map<String,Object> insMap = new HashMap<String,Object>();
	        	List<String> keys = new ArrayList<String>();
	        	List<String> vals = new ArrayList<String>();
	        	for(String id_meta:idMetaMap.keySet()) {
	        		if(StrUtil.isInteger(id_meta)) {
	        			if(!"".equals(idMetaMap.get(id_meta))) {
	        				CustomMap metaCus = metaMap.get(id_meta);
	        				if(metaCus!=null) {
	        					keys.add(metaCus.getString("meta_cd"));
	        					vals.add(idMetaMap.get(id_meta));
	        					insMap.put(metaCus.getString("meta_cd"), idMetaMap.get(id_meta));
	        				}
	        			}
	        		}
	        	}
	        	insMap.put("key_list",keys);
	        	insMap.put("vals",vals);
	        	insMap.put("login_usr_seq",commandMap.getInt("login_usr_seq"));
	        	service.insertIdentityM(insMap);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/modifyIdentityMAjax.do")
	public String modifyIdentityM(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
//				List<String> codelist = new ArrayList<String>();
//				codelist.add(commandMap.getString("cb_id_group",""));
//				commandMap.put("del_code_list", codelist);
	        	Map<String,Object> updMap = new HashMap<String,Object>();
	        	List<CustomMap> idnList = service.selectIdentity(commandMap);
	        	Map<String, CustomMap> metaMap = CollectionUtil.getKeyMapByMapList(idnList, "id_meta");
	        	Map<String,String> idMetaMap = CollectionUtil.getKeyListByCommandMap(commandMap,"id_meta_");
	        	Map<String,String> idMetaValMap = CollectionUtil.getKeyListByCommandMap(commandMap,"i_id_meta_");
	        	Map pkMap = new HashMap<>();
//	        	List<String> conditions = new ArrayList<String>();
//	        	Map<String,Object> updCondsMap = new HashMap<String,Object>();
//	        	List<String> condsKeys = new ArrayList<String>();
//	        	List<String> condsVals = new ArrayList<String>();
	        	for(String id_meta:metaMap.keySet()) {
	        		if(StrUtil.isInteger(id_meta)) {
	        			if(!"".equals(metaMap.get(id_meta))) {
	        				CustomMap metaCus = metaMap.get(id_meta);
	        				if(metaCus!=null) {
	        					if("N".equals(metaCus.get("null_yn"))) {
//	        						condsKeys.add(metaCus.getString("meta_cd"));
//	        						condsVals.add(idMetaMap.get(id_meta));
	        						pkMap.put(id_meta, "N");
	        					}
	        				}
	        			}
	        		}
	        	}
//	        	updCondsMap.put("key_list",condsKeys);
//	        	updCondsMap.put("vals",condsVals);
//	        	updMap.put("conditions",updCondsMap);
	        	
	        	Map<String,Object> updDataMap = new HashMap<String,Object>(); 
	        	for(String id_meta:idMetaMap.keySet()) {
	        		if(StrUtil.isInteger(id_meta)) {
	        			if(!"".equals(idMetaMap.get(id_meta))) {
	        				CustomMap metaCus = metaMap.get(id_meta);
	        				if(pkMap.get(id_meta)==null && metaCus!=null) {
	        					if(!idMetaMap.get(id_meta).equals(idMetaValMap.get(id_meta))) {
	        						updDataMap.put(metaCus.getString("meta_cd"), idMetaMap.get(id_meta));
	        					}
	        				}
	        			}
	        		}
	        	}
	        	if(updDataMap.size()>0) {
		        	updMap.put("updates",updDataMap);
		        	updMap.put("trg_id_meta_m",commandMap.getInt("trg_id_meta_m",0));
		        	updMap.put("login_usr_seq",commandMap.getInt("login_usr_seq"));
					service.modifyIdentityM(updMap);
	        	}else {
	    			ret = GlobalConst.ACT_NO_UPD_TRGT;
	        	}
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteIdentityMAjax.do")
	public String deleteIdentityMAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codeList = new ArrayList<String>();
				if(!"".equals(commandMap.getString("trg_id_meta_m",""))) {
					codeList.add(commandMap.getString("trg_id_meta_m"));
					service.deleteIdentityM(codeList);
				}
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	/**
	 * MetaManageController의 regMeta 메서드 복사해서 테이블에 맞게 수정
	 * @param req
	 * @param commandMap
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/adm/meta/regIdentity.do")
	public String regIdentity(HttpServletRequest req,CommandMap commandMap, ModelMap model) throws Exception {
//		CustomMap map = service.selectMeta(commandMap);
		CommandMap trgMap = new CommandMap();
		trgMap.put("id_meta_m", commandMap.getString("trg_id_meta_m"));
		CustomMap map = service.getIdentityM(trgMap);
		if(map!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("map", map);
		}
		CommandMap typParam =new CommandMap();
		typParam.put("id_code_group", GlobalConst.CG_META_DATA_TY);
		typParam.put("id_meta_m", commandMap.getString("trg_id_meta_m"));
		List<CustomMap> metaList = service.selectIdentityByIdMetaM(typParam);
		List<CustomMap> typCodeList = codeService.selectCodeByIdCodeGroup(typParam);
		model.addAttribute("typCodeList", typCodeList);
		CommandMap strTypParam =new CommandMap();
		strTypParam.put("id_code_group", GlobalConst.CG_STR_LEN_TY);
		List<CustomMap> lenTypeList = codeService.selectCodeByIdCodeGroup(strTypParam);
		model.addAttribute("lenTypeList", lenTypeList);
		if(metaList!=null) {
//			String parent_id = map.getString("parent_id","");
			model.addAttribute("metaList", metaList);
		}
		return "/adm/reg/regIdentity";
	}
	@RequestMapping("/adm/meta/getIdentityAjax.do")
	public String getIdentityAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	    		CustomMap meta = service.getIdentityByIdMetaMAndIdMeta(commandMap);
	    		CommandMap pMetaParam = CollectionUtil.getCommandMapFromCommandMap(commandMap);
	    		pMetaParam.put("id_parent_meta",meta.getInt("id_meta"));
	    		List<CustomMap> childMetaList = service.selectChildMetaListByIdParentMeta(pMetaParam);
	    		model.addAttribute("metaData", meta);
	    		model.addAttribute("childMetaList", childMetaList);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/insertIdentityAjax.do")
	public String insertIdentityAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
	        	Map data = service.insertIdentity(commandMap);
	    		model.addAttribute("data",data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/modifyIdentityAjax.do")
	public String modifyIdentityAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
		try {
	        commandMap = addParamByLoginInfo(commandMap);
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
//				List<String> codelist = new ArrayList<String>();
//				codelist.add(commandMap.getString("cb_id_group",""));
//				commandMap.put("del_code_list", codelist);
				CustomMap data = service.modifyIdentity(commandMap);
	    		model.addAttribute("data",data);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping("/adm/meta/deleteIdentityAjax.do")
	public String deleteIdentityAjax(CommandMap commandMap, ModelMap model) throws Exception {
		int ret = GlobalConst.ACT_SUCCESS;
        commandMap = addParamByLoginInfo(commandMap);
		try {
	        if(commandMap.getInt("login_usr_seq",0)==0) {
				ret = GlobalConst.NOT_FOUND_LOGIN_INFO;
	        }else {
				List<String> codelist = new ArrayList<String>();
				codelist.add(commandMap.getString("chk_id_meta",""));
				commandMap.put("del_code_list", codelist);
				List<String> del_code_list = service.deleteIdentityByList(commandMap);
				model.addAttribute("del_code_list",del_code_list);
	        }
		}catch(Exception e) {
			ret = GlobalConst.ACT_FAIL;
		}
//		commandMap.put("code_group", GlobalConst.CG_LANGU);
//		PageList<CustomMap> resultList = service.selectUserList(commandMap);
//		putModelListPage(resultList,model);
		model.addAttribute("ret",ret);
		return "jsonView";
	}
	@RequestMapping(value = "/adm/meta/downloadAjax.do")
	public String downloadAjax(CommandMap param,HttpServletRequest req, 
	            HttpServletResponse res, ModelMap model) throws Exception {

	    try {
			param.put("meta_m_type", GlobalConst.META_M_TYPE_META);
			CustomMap map = service.getIdentityMetaExcel(param);
			String filePath = FILE_DOWNLOAD_PATH; // 파일 경로 설정

		    if (!"".equals(map.getString("excel_path", "").trim())) {
		        filePath += map.getString("excel_path", "").trim();
		    }

		    // 파일 객체 생성
		    File file = new File(filePath, map.getString("excel_file_nm", "") + ".xlsx");
		    String downloadFileName = map.getString("excel_file_nm", "") + "_" + StringDateUtil.getTodayTime("yyyyMMddHHmmssSSS") + ".xlsx";

		 // 엑셀파일 읽기 및 Base64 인코딩
	        FileInputStream fis = new FileInputStream(file);
	        byte[] fileBytes = IOUtils.toByteArray(fis);
	        String base64EncodedFileContent = Base64.getEncoder().encodeToString(fileBytes);

		    fis.close();
		    System.out.println("원본 파일명: " + file.getName());
		    System.out.println("다운로드 파일명: " + downloadFileName);
		    model.addAttribute("fileNm", downloadFileName);
		    model.addAttribute("content", base64EncodedFileContent);
	    }catch(Exception e) {
	    	// 다운로드 중 오류가 발생하면 오류 메시지를 출력하고 예외를 처리합니다.
	        e.printStackTrace();
	        res.setContentType("text/plain");
	        res.getWriter().write("파일 다운로드 중 오류가 발생했습니다.");
	    }
	    return "jsonView";
	}
	
}
