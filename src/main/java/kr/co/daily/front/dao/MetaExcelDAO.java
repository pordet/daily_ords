package kr.co.daily.front.dao;

import java.util.List;
import java.util.Map;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

public interface MetaExcelDAO {

	CustomMap selectMetaExcelList(Map<String, Object> map);

	void insertMetaExcel(Map<String, Object> map);

	void updateMetaExcel(Map<String, Object> map);

	void deleteMetaExcel(Map<String, Object> map);

	CustomMap getMetaExcel(Map<String, Object> map);

	int getMaxDepthMetaByKey(Map<String, Object> map);

	List<CustomMap> selectMetaOrderList(Map<String, Object> map);

	List<CustomMap> selectMetaOrderListForExcel(Map<String, Object> map);

	CustomMap getMetaM(Map<String, Object> map);

	void insertMetaDataByGroupForExcelUpload(List<Map> inInputList);

	void insertMetaDataRowForExcelUpload(Map<String, Object> map);

}
