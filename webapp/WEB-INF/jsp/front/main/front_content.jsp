<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<c:set var="isAdmin" value="N"/>
<!DOCTYPE html>
<front:FrontHead title=""/>
<front:FrontBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" hasImgTop="true">
                 <form id="frm" name="frm" method="post">
                 <input type="hidden" name="type" value="3">
                    <div class="row" style="margin-top:-80px;">
                        <div class="col-12">
	                         	<div class="adm-searchbox">
	                                <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">검색시작년도</div>
	                                  <select class="form-control mh-form-control" id="search_category" name="id_category" onchange="javascript:changeCategory(this);">
<%-- 										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
	                                    		</c:if>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                                <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">검색종료년도</div>
	                                  <select class="form-control mh-form-control" id="search_category" name="id_category" onchange="javascript:changeCategory(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
	                                    		</c:if>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                                <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">카테고리명</div>
	                                  <select class="form-control mh-form-control" id="search_category" name="id_category" onchange="javascript:changeCategory(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_category ? 'selected':'')}>선택</option>
	                                    		</c:if>
				                            <c:forEach var="map" items="${cateList}" varStatus="i">
	                                    		<option value="${map.id_category}" ${(param.id_category eq map.id_category? 'selected':'')}>${map.code_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">국가명</div>
	                                  <select class="form-control mh-form-control" id="id_nat_area" name="id_nat_area" onchange="javascript:changeNation(this);">
	                                  			<c:if test="${isAdmin eq 'Y'}">
	                                    		<option value="" ${(empty param.id_nat_area ? 'selected':'')}>선택</option>
	                                    		</c:if>
	                                    		<c:set var="cur_nat_area" value="${(empty param.id_nat_area?firstNat.id_area:param.id_nat_area)}"/>
				                            <c:forEach var="map" items="${natList}" varStatus="i">
	                                    		<option value="${map.id_area}" ${(cur_nat_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
	                                  		</c:forEach>
	                                  </select>
	                              </div>
	                              <div class="form-group inline-group" style="width: 140px">
	                                  <div class="tit_cont">도시명</div>
	                                  <select class="form-control mh-form-control" id="id_city_area" name="id_city_area" onchange="javascript:changeCity(this);">
	                                     		<option value="" ${(empty param.id_city_area ? 'selected':'')}>선택</option>
	                                 <c:forEach var="map" items="${cityList}" varStatus="i">
                                   		<option value="${map.id_area}" ${(param.id_city_area eq map.id_area? 'selected':'')}>${map.area_nm}</option>
                               		</c:forEach>
	                                  </select>
	                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:search();"><span class="btn_search"></span>검색</button>
<!-- 	                              <button type="button" class="btn h34" onclick="javascript:data_map();">Data Map</button> -->
	                       		<div class="fr">
	                                <div class="form-group inline-group" style="width: 80px">
	                                  <div class="tit_cont">년도</div>
	                                  <select class="form-control mh-form-control" id="search_year" name="year">
	                                    	<option value="2020" ${(param.year eq '2020'? 'selected':'')}>2020</option>
	                                    	<option value="2019" ${(param.year eq '2019'? 'selected':'')}>2019</option>
	                                    	<option value="2018" ${(param.year eq '2018'? 'selected':'')}>2018</option>
	                                    	<option value="2017" ${(param.year eq '2017'? 'selected':'')}>2017</option>
	                                    	<option value="2016" ${(param.year eq '2016'? 'selected':'')}>2016</option>
	                                    	<option value="2015" ${(param.year eq '2015'? 'selected':'')}>2015</option>
	                                    	<option value="2014" ${(param.year eq '2014'? 'selected':'')}>2014</option>
	                                  </select>
	                              </div>
	                              <button type="button" class="btn h34" onclick="javascript:openAjaxInput();">등록</button>
	                            </div>
	                       </div>
	                      </div>
	                     </div>
<!-- 				<div class="searchBox"> -->
<!-- 					<div class="t"> -->
<!-- 						<table class="search"> -->
<%-- 							<colgroup> --%>
<%-- 								<col style="width:120px"> --%>
<%-- 								<col style="width:auto"> --%>
<%-- 							</colgroup> --%>
<!-- 							<tbody> -->
<!-- 								<tr> -->
<!-- 									<th scope="row">연도 선택</th> -->
<!-- 									<td> -->
<!-- 										<select title="검색 시작년도" name="st_year" id="st_year"> -->
<%-- 										<c:forEach var="st_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${st_year}" <c:if test="${st_year eq yd.min_year}"> selected="selected"</c:if>>${st_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 											</select> ~ -->
<!-- 										<select title="검색 종료년도" name="ed_year" id="ed_year"> -->
<%-- 										<c:forEach var="ed_year" begin="${yd.min_year}" end="${yd.max_year}"> --%>
<%-- 											<option value="${ed_year}" <c:if test="${ed_year eq yd.max_year}"> selected="selected"</c:if>>${ed_year}</option> --%>
<%-- 										</c:forEach> --%>
<!-- 										</select> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 							</tbody> -->
<!-- 						</table> -->
<!-- 					</div> -->
<!-- 					<div class="btnBox"><button type="submit">선택조건검색</button></div> -->
<!-- 				</div> -->
	                    </form>
				<div class="dbBtnbox">
					<div class="l">
<!-- 						<a href="#none" onclick="fn_excelDown();return false;" class="midBtn excel">엑셀저장</a> -->
						<!-- <a href="#none" onclick="fn_regMyGtdb(); return false;" class="midBtn btnLblue" id="regMyGtdbBtn">맞춤교통DB등록</a> -->
					</div>
					<div class="r">
<!-- 						<a href="#none" class="midBtn btnEme" onclick="filebox($(this));return false">Excel 다운로드</a> -->
					</div>					
				</div>
				<div class="boardBox" tabindex="0">
					<table class="tb03">
						<thead>
	      					<tr>	
	      						<th class="first" colspan="${maxLvl}">항목 </th>
										<c:forEach var="year" begin="${(empty param.st_year ? yd.min_year : param.st_year)}" end="${(empty param.ed_year ? yd.max_year : param.ed_year)}">
								<th scope="col">${year}</th>	
										</c:forEach>
							</tr>
						</thead>
						<tbody>		
<%-- 							<c:forEach var="row" items="${list}">	 --%>
<!-- 							<tr> -->
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${row.data_type eq 1}""> --%>
<%-- 								<td class="l" colspan="${row.max_lvl}"   style="text-align:center;" >${row.unit_nm}</td>	 --%>
<%-- 									</c:when> --%>
<%-- 									<c:when test="${row.data_type eq 2 }"> --%>
<%-- 								<td rowspan="${row.child_cnt}">${row.unit_nm}</td> --%>
<%-- 								<c:forEach var="child_unit" items="${row.child_unit_list}">	 --%>
<%-- 								<td>${child_unit.unit_nm}</td> --%>
<%-- 								</c:forEach> --%>
<%-- 									</c:when> --%>
<%-- 								</c:choose> --%>
<%-- 								<c:forEach var="child" items="${row.child}">	 --%>
<%-- 								<td>${child.val}</td> --%>
<%-- 								</c:forEach> --%>
<!-- 							</tr> -->
<%-- 							</c:forEach> --%>
							<c:forEach var="row" items="${rowList}" varStatus="i">	
							<tr>
								<c:forEach var="c" items="${row.child}" varStatus="j">	
								    <c:if test="${not empty c.head}">
										<td class="l" colspan="${(not empty c.colspan? c.colspan:1)}" rowspan="${(not empty c.rowspan? c.rowspan:1)}"  style="text-align:center;" >${c.unit_nm}</td>	 
								    </c:if>
						   		</c:forEach>
								<c:forEach var="year" begin="${(empty param.st_year ? yd.min_year : param.st_year)}" end="${(empty param.ed_year ? yd.max_year : param.ed_year)}">
									<c:set var="isEmpty" value="false"/>
									<c:forEach var="c" items="${row.child}" varStatus="j">	
									  	<c:if test="${year eq c.year}">
											<c:set var="isEmpty" value="true"/>
											<td class="l">${c.val}</td>	 
									  	</c:if>
									</c:forEach>
									<c:if test="${isEmpty eq false}">
										<td></td>
									</c:if>
								</c:forEach>
							</tr>
							</c:forEach>
							</tbody>
						</table>
				</div>
<script>
function changeNation(that){
	var new_val = $(that).val();
// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val($("#id_nat_area").val());
// 	}
// 	if($("#search_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
	search();
	
}
function changeCategory(that){
	var new_val = $(that).val();
// 	if($("#id_zone_area").val()!=''){
// 		$("#id_area").val($("#id_zone_area").val());
// 	}else if($("#id_city_area").val()!=''){
// 		$("#id_area").val($("#id_city_area").val());
// 	}else if($("#id_nat_area").val()==''){
// 		alertOpen('존, 도시, 국가를 선택하십시오.');
// 		return;
// 	}else{
// 		$("#id_area").val($("#id_nat_area").val());
// 	}
// 	if($("#search_category").val()==''){
// 		alertOpen('카테고리를 선택하십시오.');
// 		return;
// 	}
	search();
	
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/content.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
</script>
</front:FrontBody>
</html>