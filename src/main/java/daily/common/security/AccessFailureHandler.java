package daily.common.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import daily.common.util.StrUtil;
import kr.co.daily.common.GlobalConst;

public class AccessFailureHandler implements AccessDeniedHandler {
	
	private String errorPage; 

	public AccessFailureHandler() {
	}

	public AccessFailureHandler(String errorPage) {
		this.errorPage = errorPage;
	}

	public String getErrorPage() {
		return errorPage;
	}

	public void setErrorPage(String errorPage) {
		this.errorPage = errorPage;
	}

	@Override
	public void handle(HttpServletRequest req, HttpServletResponse resp, AccessDeniedException deniedExcepton)
			throws IOException, ServletException {
		String accept = req.getHeader("accept");

//		String error = "true"; 
//		String message = "로그인실패하였습니다."; 
//		if(denied instanceof 
		String ret = GlobalConst.NOT_ACCESS_AUTH.toString();
		if( StrUtil.indexOf(accept, "html") > -1 ) { 
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			if (errorPage != null) { 
				req.getRequestDispatcher(errorPage).forward(req,resp);
			} 
		} else if( StrUtil.indexOf(accept, "xml") > -1 ) { 
			resp.setContentType("application/xml"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<ret>", ret ,"</ret>" }); 
			PrintWriter out = resp.getWriter(); out.print(data); out.flush(); out.close(); 
		} else if( StrUtil.indexOf(accept, "json") > -1 ) { 
			resp.setContentType("application/json"); 
			resp.setCharacterEncoding("utf-8"); 
			String data = StrUtil.join(new String[] {
					"{ \"ret\" : ",  ret , "}" }); 
			PrintWriter out = resp.getWriter(); 
			out.print(data); 
			out.flush(); 
			out.close();
		}
	}

}
