package daily.common.security.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

@Repository
public interface SecuredObjectDAO {

	public List<CustomMap> getRolesAndUrl() throws Exception;

	public List<CustomMap> getRolesAndMethod() throws Exception;

	public String getHierarchicalRoles();

	public List<CustomMap> getRolesAndPointcut() throws Exception;

	public List<CustomMap> getRegexMatchedRequestMapping(CommandMap paramMap);

	public List<CustomMap> selectRolesAndUrlList(Map<String, Object> map);

}
