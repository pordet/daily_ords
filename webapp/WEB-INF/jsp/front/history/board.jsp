<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="his" tagdir="/WEB-INF/tags/history" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ page import="daily.common.util.UserAuthUtil" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:choose>
	<c:when test="${UserAuthUtil.hasRole(GlobalConst.DATA_INPUTER_AUTH)}">
		<c:set var="is_guest" value="flase"/>
	</c:when>
	<c:otherwise>
		<c:set var="is_guest" value="true"/>
	</c:otherwise>
</c:choose>
<c:set var="pageTitle" value="데이터 입력"/>
<!DOCTYPE html>
<html lang="ko">
<his:DataHead title="">
<c:choose>
	<c:when test="${!is_guest}">
		<script charset="UTF-8" src="<c:url value='/resource/js/board/board_draw.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/board/board_register.js'/>"></script>
		<script charset="UTF-8" src="<c:url value='/resource/js/board/board_input.js'/>"></script>
	</c:when>
	<c:otherwise>
		<script charset="UTF-8" src="<c:url value='/resource/js/board/guest_board_input.js'/>"></script>
	</c:otherwise>
</c:choose>
</his:DataHead>
<his:BoardAccordionBody currLocale="${locale}" hasPop="true" clazz="mh-quickbar-close" id_master="${param.id_master}" sel_ord_str="${master.ord_str}" hasLeft="true" type="${(empty param.type?'3':param.type)}" title="${master.title}" keyword="${param.search_keyword}">
		<section class="col-10">
			<div style="display:block;width:99%;" id="main_content1">
                 <form id="frm" name="frm" method="post"  onsubmit="return false;">
	                 <input type="hidden" name="id_master" value="${master.id_master}">
	                 <input type="hidden" name="id_board_master" id="id_board_master" value="${master.id_board_master}">
	                 <input type="hidden" name="id_board" id="id_board">
	                 <input type="hidden" name="attach_yn" value="${master.attach_yn}">
	                 <input type="hidden" name="multi_attach_yn" value="${master.multi_attach_yn}">
	                 <input type="hidden" name="ord_str" value="${master.ord_str}">
	                  <div class="row">
	                        <div class="col-12">
			                    <div class="form-group inline-group search">
			                                  <input type="text" class="form-control mh-form-control" style="width:100%" id="search_keyword" value="${(not empty param.search_keyword ?param.search_keyword:'')}" onkeyup="enterKey();">
<!-- 		                                  <span class="fa fa-refresh" aria-hidden="true"></span> -->
<!-- 		                                  <input type="image" class="fa fa-refresh" aria-hidden="true" alt="검색"> -->
		                                  <input type="image" src="<c:url value='/resource/img/search_bg.gif'/>" alt="검색" onclick="javascript:search();">
	                    		</div>
		                  </div>
		             </div>
                   	 <div class="row">
                        <div class="col-12">
		                    <div class="form-group form-inline">
							<label class="control-label">정렬</label>
							<select class="form-control mh-form-control" style="width:100px;font-size:13px;height:30px;" title="정렬" name="order_key" id="order_key">
								<option value="reg_date">등록일</option>
								<option value="title">제목</option>
							</select>
<!-- 							</div> -->
<!-- 							<div class="form-group form-inline"> -->
							&nbsp;&nbsp;<label class="control-label">순서</label>
							<select class="form-control mh-form-control" style="width:100px;font-size:13px;height:30px;" title="순서" name="idx" id="idx">
								<option value="ASC">오름차순</option>
								<option value="DESC" selected>내림차순</option>
							</select>
							</div>
	                    </div>
	                </div>
                   	 <div class="row">
                        <div class="col-4">
<!--                             <div class="form-group inline-group"> -->
	                              <button type="button" class="list-btn h34" onclick="javascript:search();"><img src="<c:url value='/resource/img/search_bg.png'/>" style="width:24px;">검색</button>
<!-- 	                              <button type="button" class="list-btn h34" id="search_init" onclick="search_keyword_init();">초기화</button>  -->
<!-- 	                              style="display:none;" -->
	                              
<!--                             </div> -->
	                    </div>
	                    <div class="col-8">
	                       		<div class="pos-fr">
	                       		<c:if test="${!is_guest}">
	                              <button type="button" class="list-btn h34" onclick="javascript:registerData();">
<!-- 	                              <span class="write-icon-png"></span> -->
										<img src="<c:url value='/resource/img/write_btn.png'/>" style="width:24px;">
	                              		등록</button>
	                            </c:if>
	                            </div>
	                    </div>
	                </div>
                    <div class="row" id="result_header" style="margin-top:5px;" style="display:${(fn:length(list) gt 0?'block':'none')}">
                        <div class="col-4">
                            <div class="form-group inline-group">
                            	<c:forEach var="item" items="${list}" varStatus="i">
                            		<c:if test="${i.count eq 1}">
                            			<c:set var="result_first" value="${item.rownum}"/>
                             		</c:if>
                            		<c:if test="${i.count eq fn:length(list)}">
                            			<c:set var="result_last" value="${item.rownum}"/>
                             		</c:if>
                            	</c:forEach>
	                              <span style="font-size: 14px;" id="result_summary">검색결과  ${paginator.totalCount} 개 중 ${result_first} ~ ${result_last}</span>
                            </div>
	                    </div>
	                </div>
                      	<div class="row" id="result_list" style="display:${(fn:length(list) gt 0?'block':'none')}">
						<div class="col-12">
		                        <div class="listboard-contents1">
		                            <!--loof-->
		                            <ul class="list-line">
			                            <c:forEach var="map" items="${list}">
			                            <li>
			                            <div class="meta">${map.board_path}, ${map.reg_date}</div>
										<div class="search-result-title"><a href="javascript:void(0);" onclick="javascript:viewData(${map.id_board_master},${map.id_board},'${map.ord_str}');">${map.title}</a>
											<div class="file-format inline-group"> 
											<c:forEach var="file" items="${map.filelist}">
												<a href="<c:url value='/file/data_download.do'/>?id_board_file=${file.id_board_file}"><span class="${file.file_sh_ext}">${file.file_ext}</span></a>
											</c:forEach>
											</div>
										</div>
										<div class="search-result-text txt_post">${map.comment}</div>
										</li>
			                            </c:forEach>
		                            </ul>
		                            <!--//loof-->
		                         </div>
		                    <!-- //그리드 컨텐츠영역 -->
			                    <!-- paging -->
								<pageList:ajax_page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
								<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
<%-- 										<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/> --%>
								<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
								<input type="hidden" name="page" id="page" value="${param.page}"/>
			                    <!-- //paging -->
		                    </div>
		                    <!--//그리드영역-->
		              </div>
               	<div class="row" id="result_no" style="display:${(fn:length(list) eq 0?'block':'none')}">
               		<div class="col-12">
               		검색결과가 없습니다.
               		</div>
               	</div>
                   </form>
			</div>
			<div style="display:none;width:100%;" class="empty_class" id="subConcent">
				<form name="modi_frm" id="modi_frm">
                    <div style="display:none">
					<input type="hidden" id="hdn_modi_user_nm" value="${userinfo.name}"/>
					<input type="hidden" id="modi_order_key" name="order_key"/>
					<input type="hidden" id="modi_idx" name="idx"/>
					<input type="hidden" id="modi_page" name="page"/>
	                <input type="hidden" id="bef_search_keyword" name="search_keyword" value="">
	                <input type="hidden" id="is_search" value="N">
					
					<input type="hidden" id="modi_id_master" name="id_master" value="${(not empty param.id_master?param.id_master:1)}"/>
					<input type="hidden" id="modi_id_board" name="id_board" value=""/>
					<input type="hidden" id="modi_id_board_master" name="id_board_master"/>
					<input type="hidden" id="modi_id_board_file" name="id_board_file"/>
					<input type="hidden" id="modi_sel_ord_str" name="sel_ord_str"/>
                    </div>
					<h2 id="board_reg_type">해양안전 속보</h2>
					
					<div class="tbl_Box">
						<table class="table_write" summary="해양안전 속보 상세내용을 보실 수 있습니다.">
					    	<caption></caption>
							<colgroup>
								<col width="15%">
								<col width="17%">
								<col width="15%">
								<col width="18%">
								<col width="15%">
								<col width="15%">
					        </colgroup>
					        <c:choose>
					        	<c:when test="${!is_guest}">
						    <tbody>
								<tr>
									<th>게시판</th>
								  	<td colspan="5" class="tit">
		                    			<div class="form-group form-inline">
										<select class="form-control mh-form-control sel w200p" title="상위게시판" name="ord_str" id="modi_sel_board_master" onchange="changeRegBoard(this);">
										</select>
										<select class="form-control mh-form-control sel w200p" title="하위게시판" name="child_ord_str" id="modi_sel_child_board" onchange="changeRegChildBoard(this);">
										</select>
										</div>
								  	</td>
								</tr>
								<tr>
									<th>제목</th>
								  	<td colspan="5" class="tit" >
								  	<input type="text" class="form-control mh-form-control ht30 w100p" name="title" id="modi_title">
								  	</td>
								</tr>
						        <tr>
									<th>작성자</th>
									<td>
                             			<input type="text" class="form-control mh-form-control ht30 w100p" id="modi_user_nm" readonly value="${map.reg_user_nm}">
									</td>
									<th>작성일</th>
									<td>
                             			<input type="text" class="form-control mh-form-control ht30 w100p" id="modi_date" readonly value="${map.reg_date}">
									</td>
								</tr>
						        <tr>
									<th>키워드</th>
								  	<td colspan="5">
                             			<input type="text" class="form-control mh-form-control ht30 w100p" name="keyword" id="modi_keyword" value="${map.keyword}">
 								  	</td>
						        </tr>
						        <tr>
									<th>첨부파일</th>
								  	<td colspan="5">
								  	<div id="new_file_div">
		                       	 	<div class="form-group inline-group" id="new_file_div_1">
		                                <input type="text" id="fileName1" class="form-control mh-form-control ht30 w100p"  disabled>
			                            <div class="attach-btn">
			                              <button type="button" class="btn attach1 ht30"><span class="btn_attach"></span>첨부</button>
			                              <input type="file" id="new_file_1" name="new_file_1" data-id="1" class="file_input_hidden" onchange="javascript:add_file(this);"/>
<!-- 			                                -->
			                            </div>
		                        	</div>
		                        	</div>
		                        	<div id="file_list_div">
<!-- 										<div class="file-format inline-group"> -->
<!-- 			                        		<a href="/traf/file/data_download.do?id_board_file=62"><span class="pdf">pdf</span></a> -->
<!-- 			                        		<a href="/traf/file/data_download.do?id_board_file=62"><span class="pad10">add</span></a> -->
<!-- 			                        		<a href="javascript:delAttachFile(1000);" ><i class="fa fa-minus-circle del-file" aria-hidden="true"></i></a> -->
<!-- 			                        	</div> -->
			                        </div>
								  	</td>
						        </tr>
						     	<tr>
						    		<td colspan="6" class="cnt">
										<textarea class="form-control mh-form-control" name="comment" id="modi_comment" rows="6" style="height:200px;">${map.comment}</textarea>
						    		</td>
						     	</tr>
							</tbody>
					        	</c:when>
					        	<c:otherwise>
						    <tbody>
								<tr>
									<th>제목</th>
								  	<td colspan="5" class="tit" id="modi_title">
								  	[속보] 홍해에서 후티 반군 기뢰에 선박 피격
								  	</td>
								</tr>
						        <tr>
									<th>작성자</th>
									<td>상황관리실</td>
									<th>작성일</th>
									<td>2020-12-28</td>
								</tr>
						        <tr>
									<th>키워드</th>
								  	<td colspan="5" id="modi_keyword">
								  	</td>
						        </tr>
						        <tr>
									<th>첨부파일</th>
								  	<td colspan="5">
		                        	<div id="file_list_div">
										<div class="file-format inline-group">
			                        		<a href="/traf/file/data_download.do?id_board_file=62"><span class="pdf">pdf</span></a>
			                        		<a href="/traf/file/data_download.do?id_board_file=62"><span class="pad10">add</span></a>
			                        	</div>
			                        </div>
								  	</td>
						        </tr>
						     	<tr>
						    		<td colspan="6" class="cnt" id="modi_comment">2020. 12. 26.(토) 연합뉴스에서 보도한 홍해 후티 반군 기뢰 선박 피격 사건 관련, 보도 내용을 아래(링크)와 같이 공유하오니,<br>홍해 및 인근을 항해하는 선박은 안전에 유의하여 주시기 바랍니다.<br><br><br>https://www.yna.co.kr/view/AKR20201226021100009</td>
						     	</tr>
							</tbody>
					        	</c:otherwise>
					        </c:choose>
      					</table>
					</div>
					<!-- button -->
					<div>
					<div class="btnBox">
                   		<c:if test="${!is_guest}">
                            <button type="button" class="btn basic btn01 fl" id="btnInit" onclick="regBoardInit();">
<!-- 	                              <span class="write-icon-png"></span> -->
<%-- 							<img src="<c:url value='/resource/img/write_btn.png'/>" style="width:24px;"> --%>
							초기화</button>
							<div class="fr">
                            <button type="button" class="btn basic btn01" id="btnDelete" onclick="delData();">
<!-- 	                              <span class="write-icon-png"></span> -->
<%-- 							<img src="<c:url value='/resource/img/write_btn.png'/>" style="width:24px;"> --%>
							삭제</button>
                            <button type="button" class="btn basic btn02" onclick="javascript:register();">
<!-- 	                              <span class="write-icon-png"></span> -->
<%-- 							<img src="<c:url value='/resource/img/write_btn.png'/>" style="width:24px;"> --%>
							저장</button>
							<button type="button" class="btn basic btn02" onclick="javascript:fn_return();">목록</button>						
							</div>
                        </c:if>
                   		<c:if test="${is_guest}">
							<button type="button" class="btn basic btn02 fr" onclick="javascript:fn_return();">목록</button>						
                        </c:if>
			        </div>
			        </div>
			    </form>
			</div>
		</section>
</his:BoardAccordionBody>
<script>
$('#dataModal').on('hidden.bs.modal', function () {
	close_reg();
//    $('#modal_body').animate({ scrollTop: 0 }, 'slow');
//	$('#modal_body').scrollTop(0);

});
$('#dataModal').on('shown.bs.modal', function () {
//     $('#modal_body').animate({ scrollTop: 0 }, 'slow');
	$('#modal_body').scrollTop(0);
});
function enterKey(){
	if(window.event.keyCode == 13){
		search();
	}
}
function close_reg(){
	$("#dataModal").modal('hide');	
}
$(function() {
    <c:forEach var="map" items="${countList}">
    $("#span_${map.ord_str}").html("(${map.board_cnt})");
    </c:forEach>
	$(".grid-loof-cont").mouseover(function(e){
 		$(this).find(".cont-unit").css("background-color","rgb(211,234,255)");
 		e.preventDefault();
 	});	
 	$(".grid-loof-cont").mouseout(function(e){
 		$(this).find(".cont-unit").css("background-color","white");
 		e.preventDefault();
 	});	

});

</script>
</html>