<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="base" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<%@ taglib prefix="ctrl" tagdir="/WEB-INF/tags/ctrl" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="사용자 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}"/>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" pageTitle="${pageTitle}" hasMenu="true" hasPop="true" hasImgTop="true" hasLeft="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
				<form name="frm" id="frm">
					<input type="hidden" name="confirm_yn" id="confirm_yn"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
					<input type="hidden" name="user_seq" id="user_seq"/>
					
					<div class="search-group-area">
						<div class="search-group-wrap">
							<div class="search-group-single">
								<div class="search-group-single-input">
									<select name="search_col">
										<option value="nm" ${(param.search_col eq 'nm'? 'selected':'')}>사용자명</option>
										<option value="ug" ${(param.search_col eq 'ug'? 'selected':'')}>그룹명</option>
									</select>
									<input type="text" class="" name="search_keyword" id="search_keyword" value="${param.search_keyword}">
									<button type="button" class="btn-primary" onclick="javascript:search();">
										<span class="btn_search"></span>
										검색
									</button>
								</div>
								<div class="search-group-single-div">
									<div class="l">
									</div>
									<div class="r">
										<button class="btn-primary" onclick="javascript:initSearchForm();return false">초기화</button>
									</div>					
								</div>
							</div>
						</div>
					</div>
				</form>
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button class="btn-primary" onclick="javascript:openRegUser();return false">사용자 등록</button>
							<button class="btn-danger" onclick="javascript:deleteList();return false">사용자 삭제</button>
						</div>					
					</div>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									사용자 ID
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용자명
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용자 그룹명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									승인여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="5" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
						<form id="listFrm" name="listFrm">
						
							<input type="hidden" name="search_col" id="list_search_col" value="${param.search_col}"/>
							<input type="hidden" name="search_keyword" id="list_search_keyword" value="${param.search_keyword}"/>
							<input type="hidden" name="id_menu" id="list_id_menu" value="${param.id_menu}"/>
							<input type="hidden" name="sidx" id="list_sidx" value="${param.sidx}"/>
							<input type="hidden" name="sord" id="list_sord" value="${param.sord}"/>
							<input type="hidden" name="page" id="list_page" value="${param.page}"/>
							<c:choose>
								<c:when test="${fn:length(list) == 0 }">
								</c:when>
								<c:otherwise>
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openRegUser('${map.user_seq}');">
									<div class="check-unit">
										<input type="checkbox" value="${map.user_seq}" name="del_code" id="del_code${i.count}">
									</div>
									<div class="cont-unit desc-unit">${map.id}</div>
									<div class="cont-unit">${map.name}</div>
									<div class="cont-unit">${(not empty map.group_nm? map.group_nm:'&nbsp;')}</div>
									<div class="cont-unit">
										<c:choose>
											<c:when test="${map.user_status eq 'Y'}">
												<div>승인 완료</div>
											</c:when>
											<c:otherwise>
												<div>
													<a href="javascript:void(0);" onclick="javascript:regChild(${map.id_master},${map.id_board_master},'${map.ord_str}');"><span class="btn_lbl_type02">승인대기</span></a>
												</div>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="cont-unit">${map.use_yn}</div>
								</div>
							</c:forEach>
								</c:otherwise>
							</c:choose>
						</form>
						</div>
					</div>
					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					
			</div>
		</div>
	</div>
	
</adm:AdmBody>
<script>
/*
$(function () {
	$("#tab_layer_1").click(function(){
		location.href="<c:url value='/adm/user/userList.do'/>?id_menu=12";
	});
	$("#tab_layer_2").click(function(){
		location.href="<c:url value='/adm/user/userGroupList.do'/>?id_menu=9";
	});
	$("#tab_layer_3").click(function(){
		location.href="<c:url value='/adm/user/roleList.do'/>?id_menu=10";
	});
	$("#tab_layer_4").click(function(){
		location.href="<c:url value='/adm/user/permissionList.do'/>?id_menu=11";
	});
});
*/
function openRegUser(key){
	var frm =document.frm;
	frm.action=contextPath+"/adm/user/regUser.do";
	if(key){
		$("#user_seq").val(key);
	}else{
		$("#user_seq").val('');
	}
//	frm.action="/excelDownload.do";
	$("#frm").submit();
}
function initSearchForm(){
	$("#search_keyword").val('');
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/adm/user/userList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}
function deleteList(){
	if($("input:checkbox[name=del_code]:checked").length==0){
		alertOpen('삭제할 항목을 선택하십시오.');
	}else{
		confirmOpen('삭제하시면 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
	}
}
function deleteConfirm(){
	processing();
// 	if($("#confirm_yn").val()=='Y'){
// 	    $("#confirm_yn").val('N');
		var frm =document.listFrm;
		var page = $("#page").val();
		frm.action=contextPath+"/adm/user/deleteUserList.do";
		frm.page.value=page;
	//frm.action="/excelDownload.do";
	    frm.submit();
// 	}
}
</script>
</html>
