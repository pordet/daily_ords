package kr.co.daily.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.BatchManageDAO;

@Service
public class BatchManageService {
	@Autowired private BatchManageDAO dao;
	public PageList<CustomMap> selectBatchResultList(CommandMap map) {
		return dao.selectBatchResultList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}
	public List<CustomMap> selectBatchDetailByIdBatch(CommandMap map) {
		return dao.selectBatchDetailByIdBatch(map.getMap());
	}

}
