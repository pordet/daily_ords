package kr.co.daily.front.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.util.AddBatchUtil;
import daily.common.util.CollectionUtil;
import daily.common.util.MetaUtil;
import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;
import daily.mybatis.util.MybatisPagingUtil;
import kr.co.daily.admin.dao.MetaManageDAO;
import kr.co.daily.admin.service.MetaManageService;
import kr.co.daily.common.GlobalConst;
import kr.co.daily.front.dao.MetaDAO;

@Service
public class MetaService {

	@Value("#{global['openapi.addbatch.cnt']}")
	private int addBatchCnt;
	
	@Autowired private MetaDAO dao;
	@Autowired private MetaManageDAO adminDao;
	@Autowired private MetaManageService metaManageService;
	@Autowired private BidSearchService bidService;
	
	public CustomMap getMetaM(CommandMap map) {
		return dao.getMetaM(map.getMap());
	}

	public CustomMap getMetaRowByKey(CommandMap map) {
		return dao.getMetaRowByKey(map.getMap());
	}

	public List<CustomMap> selectMeta(CommandMap map) {
		return dao.selectMeta(map.getMap());
	}

	public List<CustomMap> selectMetaForList(CommandMap map) {
		return dao.selectMetaForList(map.getMap());
	}
	
	public PageList<CustomMap> selectMetaRow(CommandMap map) {
		return dao.selectMetaRow(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public List<CustomMap> selectMetaDataForSearch(List<Integer> lowKey) {
		return dao.selectMetaDataForSearch(lowKey);
	}

	public List<CustomMap> selectMetaListForUsing11(CommandMap map) throws Exception {
		List<CustomMap> ret = new ArrayList<CustomMap>();
		int max_depth = dao.getMaxDepthAtMeta(map.getMap());
		List<CustomMap> treeList = dao.selectMetaListForUsing(map.getMap());
		Map<Integer,Integer> currDepthMap = new HashMap<>();
		for(CustomMap meta:treeList) {
			int curr_lvl = meta.getInt("lvl",0);
			if(curr_lvl==1) {
				ret.add(meta);
				currDepthMap = null;
			}else if(curr_lvl>1) {
				if(ret.size()>0) {
					CustomMap lastRow = ret.get(ret.size()-1);
					int lvlCnt = lastRow.getInt("lvl_"+curr_lvl+"_cnt",0);
					if(lvlCnt==0) {
						lastRow.put("lvl_"+curr_lvl+"_child", currDepthMap);
						lvlCnt++;
						lastRow.put("lvl_"+curr_lvl+"_cnt",lvlCnt);
					}else {
						List<CustomMap> childList = (List<CustomMap>)lastRow.get("lvl_"+curr_lvl+"_child");
						childList.add(meta);
						lvlCnt++;
						lastRow.put("lvl_"+curr_lvl+"_cnt",lvlCnt);
					}
				}else {
					throw new Exception("lvl이 1보다 크면서 lvl이 1인 부모행을 안 가질 수 없습니다.");
				}
			}else {
				throw new Exception("lvl은 0보다 작거나 없을 수 없습니다.");
			}
		}
		for(CustomMap firstLvl:treeList) {
			int lvlCnt = firstLvl.getInt("lvl_1_cnt",0);
			if(lvlCnt==0) {
				firstLvl.put("rowspan", max_depth);
			}
		}
		return ret;
	}

	/**
	 * 헤더 리스트를 가져오는 메서드
	 * @param map
	 * @param max_meta_depth
	 * @return subColList , aloneList ,headerList 값을 가지는 map
	 * @throws Exception
	 */
	public Map<String,Object> selectMetaListForUsing(CommandMap map, int max_meta_depth) throws Exception {
	    List<CustomMap> topList = dao.selectTopDepthMetaListForHeader(map.getMap());
	    List<CustomMap> slaveList = dao.selectDepthMetaListRemoveTopDepthForHeader(map.getMap());
	    Map<String,Object> ret = null;
		List<CustomMap> subList = adminDao.selectMetaRelByParentIdMetaM(map.getMap());
		List<Integer> subKeyList = new ArrayList<>();
		Map<Integer,CustomMap> subMap = new HashMap<>();
		for(CustomMap sub:subList) {
			int key = sub.getInt("child_id_meta_m",0);
			subKeyList.add(key);
			subMap.put(key, sub);
		}
		if(subKeyList.size()>0) {
			List<CustomMap> fkColList = adminDao.selectMetaFKBySlaveMeta(subKeyList);
			Map<Integer,List<CustomMap>> fkColMap = new HashMap<>();
			for(CustomMap sub:fkColList) {
				List<CustomMap> depthSubColList = fkColMap.computeIfAbsent(sub.getInt("id_meta_m"), k -> new ArrayList<>());
				depthSubColList.add(sub);
			}
		    ret = MetaUtil.generateDataAndHeaderLists(topList,slaveList,fkColMap);
			List<CustomMap> subColList = adminDao.selectSubMetaColByKeyList(subKeyList);
			Map<Integer,List<CustomMap>> depthSubColMap = new HashMap<>();
			List<CustomMap> list = (List<CustomMap>) ret.get("aloneList");
			for(CustomMap alone:list) {
				for(CustomMap sub:subList) {
					if(alone.getStringRaw("meta_cd","").equals(sub.getStringRaw("meta_cd"))) {
						List<CustomMap> depthSubColList = depthSubColMap.computeIfAbsent(alone.getInt("id_meta_m"), k -> new ArrayList<>());
						depthSubColList.add(sub);
						depthSubColMap.put(sub.getInt("id_meta_m"), subColList);
					}
				}
			}
			ret.put("subColList", depthSubColMap);
		}else {
		    ret = MetaUtil.generateDataAndHeaderLists(topList,slaveList);
		}
	    return ret;
	}

	public PageList<CustomMap> selectMetaDataListContents(CommandMap map) {
		return dao.selectMetaDataListContents(map.getMap());
	}

	public List<CustomMap> selectMetaForSearch(List<Integer> rawKey) {
		return dao.selectMetaForSearch(rawKey);
	}

	public void insertMetaRow(CommandMap map) {
		dao.insertMetaRow(map.getMap());
	}

	public ModelMap insertMetaData(CommandMap commandMap, ModelMap model) throws Exception {
        this.insertMetaRow(commandMap);
        if(!"".equals(commandMap.getString("id_meta_row", ""))) {
    		List<CustomMap> colList = this.selectMeta(commandMap);
    		if(colList==null || colList.size()==0) {
    			throw new Exception("매칭되는 정보를 가져오지 못했습니다.");
    		}
        	List<CustomMap> insDataList = this.insertMetaData(colList,commandMap);
			model.addAttribute("insDataList",insDataList);
        }
		model.addAttribute("id_meta_row",commandMap.getString("id_meta_row",""));
		/**
		 * Open Api 서비스 코드가 있을 때 
		 */
		if(!"".equals(commandMap.getString("sys_code","")) && !"".equals(commandMap.getString("id_bid_noti",""))) {
			bidService.updateBidServiceInputState(commandMap);
		}
		return model;
	}
	public List<CustomMap> insertMetaData(List<CustomMap> colList,CommandMap map) {
		List<Integer> calMetaIdList = new ArrayList<>();
		for(CustomMap o:colList) {
			int al_meta_m = o.getInt("id_meta", 0);
			if(al_meta_m>0) {
				String al_typ = o.getString("typ","").trim();
				if(GlobalConst.DT_CALC.equals(al_typ)) {
					calMetaIdList.add(al_meta_m);
				}
			}
		}
        CommandMap calParam = new CommandMap();
        calParam.put("cal_meta_m_list", calMetaIdList);
        calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
        List<CustomMap> calColInfoList = this.selectCalcuListByParam(calParam);
		List<CommandMap> inList = this.getCommandListByCommandMapAndPrefix(colList, "id_meta", map, "id_meta_","val");
		for(CommandMap inMap : inList) {
			inMap.put("id_meta_row", map.getInt("id_meta_row"));
		}

		List<CommandMap> calColList = MetaUtil.calculateRowData(colList,inList,calColInfoList,map.getInt("id_meta_row"));
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, calColList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertMetaDataByGroup(inInputList);
		}
		List<CustomMap> dataList = dao.selectMetaDataByIdRow(map.getInt("id_meta_row"));
		return dataList;
	}

	public List<CommandMap> getCommandListByCommandMapAndPrefix(List<CustomMap> baselist,String key,CommandMap in,String prefix,String attrNm){
		List<CommandMap> ret = new ArrayList<CommandMap>();
		for(CustomMap map:baselist) {
			String idMata = map.getString(key,"");
			if(!"".equals(idMata)) {
				String val = in.getString(prefix+idMata,"");
				if(!"".equals(val)) {
					CommandMap cmap = CollectionUtil.getCommandMapByCustomMap(map);
					cmap.put(attrNm, val);
					ret.add(cmap);
				}
			}
		}
		return ret;
	}

	public void modifyMetaRow(CommandMap map) {
		dao.modifyMetaRow(map.getMap());
	}

	
	public Map<String,Object> modifyMetaData(List<CustomMap> colList, CommandMap map) {
		List<Integer> calMetaIdList = new ArrayList<>();
		for(CustomMap o:colList) {
			int al_meta_m = o.getInt("id_meta", 0);
			if(al_meta_m>0) {
				String al_typ = o.getString("typ","").trim();
				if(GlobalConst.DT_CALC.equals(al_typ)) {
					calMetaIdList.add(al_meta_m);
				}
			}
		}
        CommandMap calParam = new CommandMap();
        calParam.put("cal_meta_m_list", calMetaIdList);
        calParam.put("meta_m_type", GlobalConst.META_M_TYPE_META);
        List<CustomMap> calColInfoMap = this.selectCalcuListByParam(calParam);
		
		List<CommandMap> inList = MetaUtil.getModifyCommandListByCommandMapAndPrefix(colList,calColInfoMap, "id_meta", map, "id_meta_","i_id_meta_","i_id_data_","id_data" ,"val");
		List<CommandMap> insDataList = new ArrayList<>();
		List<CommandMap> modDataList = new ArrayList<>();
		List<CustomMap> insList = new ArrayList<>();
		List<Integer> delList = new ArrayList<>();
		List<CustomMap> modList = new ArrayList<>();
		List<CommandMap> modValList = new ArrayList<>();
		Map<Integer,CommandMap> modColMap = new HashMap<>();
		for(CommandMap inMap : inList) {
			if(!"".equals(inMap.getString("modify_type",""))) {
				String modify_type = inMap.getString("modify_type","");
				if("D".equals(modify_type)) {
				    int id_data = inMap.getInt("id_data",0);
				    if(id_data>0) {
				    	delList.add(id_data);
				    }
				}else if("I".equals(modify_type)) {
					insDataList.add(inMap);
					insList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}else if("M".equals(modify_type)) {
					inMap.put("id_data", inMap.getInt("id_data",0));
					modDataList.add(inMap);
					modList.add(CollectionUtil.getCustomMapByCommandMap(inMap));
					modValList.add(inMap);
					modColMap.put(inMap.getInt("id_meta"), inMap);
				}
			}else {
				modValList.add(inMap);
				modColMap.put(inMap.getInt("id_meta"), inMap);
			}
		}
		Map<Integer,List<Map>> addBatchInputMap = AddBatchUtil.getBatchDataMap(addBatchCnt, insDataList);
		Set<Integer> idxKeySet = addBatchInputMap.keySet();
		List<Integer> idxKeyList = new ArrayList<>(idxKeySet);
		Collections.sort(idxKeyList);
		for(Integer idxKey:idxKeyList) {
			List<Map> inInputList = addBatchInputMap.get(idxKey);
			dao.insertMetaDataByGroup(inInputList);
		}
		if(modList.size()>0) {
			for(CommandMap inMap :modDataList) {
				dao.updateMetaData(inMap.getMap());
			}
		}
		if(delList.size()>0) {
			dao.deleteMetaDataByList(delList);
		}
		for(Integer m:modColMap.keySet()) {
			modColMap.get(m);
		}
		Map<String,Object> ret = new HashMap<>();
		ret.put("delList", delList);
		ret.put("modList", modList);
		ret.put("insList", insList);
		return ret;
	}

	public List<CustomMap> selectCalcuListByParam(CommandMap map) {
		return dao.selectCalcuListByParam(map.getMap());
	}

	public PageList<CustomMap> selectMetaMList(CommandMap map) {
		return dao.selectMetaMList(map.getMap(), MybatisPagingUtil.pageBounds(map));
	}

	public List<CustomMap> selectMetaKeyM(CommandMap map) {
		return dao.selectMetaKeyM(map.getMap());
	}

	/**구현안됨
	 * 
	 * @param map
	 * @return
	 */
	public List<CustomMap> selectContsKeyList(CommandMap map) {
		return dao.selectContsKeyList(map.getMap());
	}

	public int getMaxDepthAtMeta(CommandMap map) {
		return dao.getMaxDepthAtMeta(map.getMap());
	}

	public List<CustomMap> selectMetaOrderList(CommandMap map) {
		return dao.selectMetaOrderList(map.getMap());
	}

	public Map<String, Object> selectSubMetaListForUsing(CommandMap map, List<CustomMap> list) throws Exception {
		Map<String, Object> ret = new HashMap<>();
		List<CustomMap> subList = adminDao.selectMetaRelByParentIdMetaM(map.getMap());
		List<Integer> subKeyList = new ArrayList<>();
		Map<Integer,CustomMap> subMap = new HashMap<>();
		for(CustomMap sub:subList) {
			int key = sub.getInt("child_id_meta_m",0);
			subKeyList.add(key);
			subMap.put(key, sub);
		}
		List<CustomMap> fkColList = adminDao.selectMetaFKBySlaveMeta(subKeyList);
		Map<Integer,List<CustomMap>> fkColMap = new HashMap<>();
		for(CustomMap sub:fkColList) {
			List<CustomMap> depthSubColList = fkColMap.computeIfAbsent(sub.getInt("id_meta_m"), k -> new ArrayList<>());
			depthSubColList.add(sub);
		}
		
		List<CustomMap> subColList = adminDao.selectSubMetaColByKeyList(subKeyList);
		Map<Integer,List<CustomMap>> depthSubColMap = new HashMap<>();
		for(CustomMap alone:list) {
			for(CustomMap sub:subList) {
				if(alone.getStringRaw("meta_cd","").equals(sub.getStringRaw("meta_cd"))) {
					List<CustomMap> depthSubColList = depthSubColMap.computeIfAbsent(alone.getInt("id_meta_m"), k -> new ArrayList<>());
					depthSubColList.add(sub);
					depthSubColMap.put(sub.getInt("id_meta_m"), subColList);
				}
			}
		}
		ret.put("fkColMap", fkColMap);
		ret.put("depthSubColMap", depthSubColMap);
		return ret;
	}

	public List<CustomMap> selectMetaDataByIdMetaRow(CommandMap map) {
		return dao.selectMetaDataByIdMetaRow(map.getMap());
	}

	public PageList<CustomMap> selectMetaDataContents(CommandMap map, CommandMap spMap) {
		return dao.selectMetaDataContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}

	public List<CustomMap> getDateTimeColByIdMetaM(CommandMap map) {
		return dao.getDateTimeColByIdMetaM(map.getMap());
	}

	public PageList<CustomMap> selectTreeMetaDataContents(CommandMap map, CommandMap spMap) {
		return dao.selectTreeMetaDataContents(map.getMap(), MybatisPagingUtil.pageBounds(map,spMap));
	}

	public List<CustomMap> deleteMetaData(CommandMap map) {
		dao.deleteMetaData(map.getMap());
		dao.deleteMetaRow(map.getMap());
		List<CustomMap> colList = dao.selectMeta(map.getMap());
		List<CustomMap> headerList = new ArrayList<>();
		//Map<id_parent_meta,List<child>
		Map<Integer,List<CustomMap>> childMap = new HashMap<>();
		Map<Integer,CustomMap> curMap = new HashMap<>();
		for(CustomMap col:colList) {
			if(col.getInt("meta_depth")==1) {
				headerList.add(col);
				curMap.put(col.getInt("id_meta"), col);
			}else {
				int id_parent_meta = col.getInt("id_parent_meta");
				List<CustomMap> childList = childMap.get(col.getInt("id_parent_meta"));
				if(childList==null) {
					childList = new ArrayList<>();
					childList.add(col);
					childMap.put(col.getInt("id_parent_meta"), childList);
				}else {
					childList.add(col);
				}
				curMap.get(id_parent_meta).put("child_list", childList);
			}
		}
		return headerList;
	}

	public ModelMap selectMataList(CommandMap commandMap, ModelMap model) {
			CustomMap metaM = null;
			commandMap.put("trg_id_meta_m", commandMap.getInt("id_meta_m"));
			try {
				metaM = this.getMetaM(commandMap);
				model.addAttribute("metaM", metaM);
				if(!"".equals(commandMap.getString("id_meta_m", ""))) {
					CommandMap dtParam = new CommandMap();
					dtParam.put("id_meta_m", commandMap.getString("id_meta_m", ""));
					List<String> dtTypList = new ArrayList<>();
					dtTypList.add(GlobalConst.DT_DATE);
					dtTypList.add(GlobalConst.DT_DATETIME);
					dtParam.put("typ", dtTypList);
					List<CustomMap> dtList = this.getDateTimeColByIdMetaM(dtParam);
					model.addAttribute("dtSearchList", dtList);
				}
		//		CommandMap metaKey = CollectionUtil.getCommandMapByCustomMap(metaM);
				int max_meta_depth = this.getMaxDepthAtMeta(commandMap);
				Map<String, Object> infoMap = this.selectMetaListForUsing(commandMap,max_meta_depth);
				List<List<CustomMap>> headerList = (List<List<CustomMap>>) infoMap.get("headerList");
	//			Map<String,Object> subInfoMap = service.selectSubMetaListForUsing(commandMap,(List<CustomMap>) infoMap.get("aloneList"));
				model.addAttribute("max_meta_depth", max_meta_depth);
				model.addAttribute("headerList", headerList);
				CommandMap param = new CommandMap();
				param.put("id_meta_m", commandMap.get("id_meta_m"));
				List<CustomMap> aloneList = (List<CustomMap>) infoMap.get("aloneList");
				model.addAttribute("aloneList", aloneList);
	    		CommandMap metaKeyParam = new CommandMap();
	    		metaKeyParam.put("id_meta_m",param.getInt("id_meta_m"));
	//    		metaKeyParam.put("key_using_type", GlobalConst.KEY_USE_TYPE_EXCEL_UPLOAD);
	    		//rel > key_m > key 컬럼순으로 사용
				List<CustomMap> childList = metaManageService.selectMetaRelByParentIdMetaM(metaKeyParam);
				
				PageList<CustomMap> todayList = null;
				try {
					if(childList.size()==0) {
						todayList = this.selectMetaDataContents(commandMap,null);
					}else {
						todayList = this.selectTreeMetaDataContents(commandMap,null);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				Map<Integer,CustomMap> valMap = new HashMap<Integer,CustomMap>();
				//BidNotiCol 값들을 조회하기 위한 키 추가 및 PageList에 키를 넣기 위한 코드
				List<Integer> rawKey = new ArrayList<Integer>();
				for(CustomMap cm : todayList) {
					int key = cm.getInt("id_meta_row",0);
					if(key!=0) {
						rawKey.add(key);
						valMap.put(key, cm);
					}
				}
				if(valMap.size()>0) {
					List<CustomMap> rawList = this.selectMetaDataForSearch(rawKey);
					for(CustomMap cm : todayList) {
						if(childList.size()==0) {
							cm.put("id_meta_m", commandMap.get("id_meta_m"));
						}
						int key = cm.getInt("id_meta_row",0);
						for(CustomMap rm:rawList) {
							if(rm.getInt("id_meta_row",0)==key) {
								if(rm.getInt("id_data",0)==3735) {
									System.out.println("debug rawList id_data:"+rm.getInt("id_data",0));
								}
								System.out.println("rawList id_data:"+rm.getInt("id_data",0));
								if(!"".equals(rm.getString("val",""))) {
									rm = MetaUtil.transNumberingByCustomMap(rm);
								}
								cm.putByString(rm.getString("meta_cd", ""), rm.getString("val",""));
							}
						}
					}
				}
//				putModelListPage(todayList, model);
				model.addAttribute("todayList", todayList);
				model.addAttribute("headerList", headerList);
			}catch(Exception e) {
				e.printStackTrace();
			}
			return model;
		}


}
