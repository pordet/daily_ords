package kr.co.daily.usr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import daily.common.vo.CustomMap;

@Repository
public interface GuestDAO {

	CustomMap loginAction(Map<String, Object> map);

	void insertUsr(Map<String, Object> map);

	void deleteUsr(Map<String, Object> map);

	CustomMap getUsr(Map<String, Object> map);
	
	CustomMap getcrsRequestAdm(Map<String, Object> map);

	PageList<CustomMap> selectUsrList(Map<String, Object> map, PageBounds pageBounds);

	int checkTmpUsrByEmail(Map<String, Object> map);

	CustomMap confirmTmpUsr(Map<String, Object> map);

	void insertConfirmInfo(Map<String, Object> map);

	void updateConfirmInfo(Map<String, Object> map);

	CustomMap guestLoginAction(Map<String, Object> map);

	void insertApplicant(Map<String, Object> map);
	
	void insertApplicantDetail(Map<String, Object> map);

	void insertPersonNation(Map<String, Object> map);

	void insertPersonProfession(Map<String, Object> map);

	void insertPersonOccupation(Map<String, Object> map);

	CustomMap getGuestPassword(Map<String, Object> map);

	void deleteConfirmInfo(Map<String, Object> map);

	void insertGuestReset(Map<String, Object> map);

	CustomMap getGuestReset(Map<String, Object> map);

	void updatePersonalPassword(Map<String, Object> map);

	List<CustomMap> getUserAuth(Map<String, Object> map);

	void updatePwd(Map<String, Object> map);

	void deleteGuestReset(Map<String, Object> map);

	CustomMap getCheckApplicatByNidNumber(Map<String, Object> map);

	void updateApplicant(Map<String, Object> map);

	void updateApplicantDetail(Map<String, Object> map);

	void updateApplicantDetailByPassport(Map<String, Object> map);

	void insertApplicantByPassport(Map<String, Object> map);

	int getApplicantCntByEmail(Map<String, Object> map);

	int getApplicantCntByNid(Map<String, Object> map);

	CustomMap getPersonNation(Map<String, Object> map);

	CustomMap getPersonProfession(Map<String, Object> map);

	CustomMap getPersonOccupation(Map<String, Object> map);

}
