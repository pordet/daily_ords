package daily.common.util;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import kr.co.daily.domain.CodeVO;

public abstract class SimpleVoTagSupport extends SimpleTagSupport {

	private String voName;

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().setAttribute( voName, this.getVo() );
	}

	public abstract CodeVO getVo();

	public void setVoName( String voName ) {
		this.voName = voName;
	}

	public String getVoName() {
		return voName;
	}
}
