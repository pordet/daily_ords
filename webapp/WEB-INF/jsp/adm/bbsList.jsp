<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="adm" tagdir="/WEB-INF/tags/adm" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="front" tagdir="/WEB-INF/tags/front" %>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="kr.co.daily.common.GlobalConst" %>
<%@ taglib prefix="pageList" uri="/WEB-INF/tlds/page-taglib.tld" %>
<c:set var="locale" value="<%=LocaleContextHolder.getLocale() %>"/>
<c:set var="pageTitle" value="자료실 관리"/>
<!DOCTYPE html>
<html lang="ko">
<adm:AdmHead title="${pageTitle}">
	<script charset="UTF-8" src="<c:url value='/resource/js/adm/bbs_m_input.js'/>"></script>
</adm:AdmHead>
<adm:AdmBody currLocale="${locale}" clazz="mh-theme mh-quickbar-close" hasLeft="true" hasPop="true" hasImgTop="true">
	<div class="page-wrap">
		<div class="page-layer">
			<div class="page-title-wrap">
				<span>${pageTitle}</span>
			</div>
			
			<div class="page-content-wrap">
			
				<form id="frm" name="frm">
					<input type="hidden" name="id_master" id="id_master">
					<input type="hidden" name="id_bbs_m" id="id_bbs_m">
					<input type="hidden" name="ord_str" id="ord_str">
					
					<div class="dbBtnbox">
						<div class="l">
						</div>
						<div class="r">
							<button type="button" class="btn-primary" onclick="javascript:openReg();">게시판 등록</button>
							<button type="button" class="btn-danger" onclick="javascript:delBbs();">게시판 삭제</button>
							
						</div>					
					</div>
					<div class="grid-area">
						<div class="grid-header">
							<div class="check-unit check-all">
								<input type="checkbox">
							</div>
							<div class="header-unit">
								<a href="#">
									자료실 id
									<ctrl:orderMark val="1" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									자료실명
									<ctrl:orderMark val="2" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									설명
									<ctrl:orderMark val="3" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit desc-unit">
								<a href="#">
									사용여부
									<ctrl:orderMark val="4" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
							<div class="header-unit">
								<a href="#">
									키워드
									<ctrl:orderMark val="5" sidx="${param.sidx}" sord="${param.sord}"/>
								</a>
							</div>
						</div>
						<div class="grid-contents">
							<c:forEach var="map" items="${list}" varStatus="i">
								<div class="grid-loof-cont" ondblclick="javascript:openReg('${map.id_bbs_m}')">
									<div class="check-unit">
										<input type="checkbox" name="del_code" value="${map.id_bbs_m}">
									</div>
									<div class="cont-unit">${map.id_bbs_m}</div>
									<div class="cont-unit">${map.bbs_nm}</div>
									<div class="cont-unit">${map.comment}</div>
									<div class="cont-unit desc-unit">${map.use_yn}</div>
									<div class="cont-unit">${map.keyword_yn}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					

					<pageList:page pageLinkUrl="javascript:doList" paginator="${paginator}" method="true"/>
					<input type="hidden" name="id_menu" id="id_menu" value="${param.id_menu}"/>
					<input type="hidden" name="sidx" id="sidx" value="${param.sidx}"/>
					<input type="hidden" name="sord" id="sord" value="${param.sord}"/>
					<input type="hidden" name="page" id="page" value="${param.page}"/>
				</form>
			
			</div>
		</div>
	</div>
</adm:AdmBody>
<script>
/*
function registerData(){
	$("#dataModal").modal('show');	
}

function changeCategory(that){
	var new_val = $(that).val();
	search();
}

$('#dataModal').on('hidden.bs.modal', function () {
	refresh();
});
$('#dataViewModal').on('hidden.bs.modal', function () {
	refresh();
});
*/
function refresh(){
	var frm =document.frm;
	frm.action=contextPath+"/manageHistory.do";
		$("#page").val(page);
//	frm.action="/excelDownload.do";
	$("#frm").submit();
	
}
function doList(page){
	processing();
	var frm =document.frm;
		frm.action=contextPath+"/adm/bbs/bbsList.do";
   		$("#page").val(page);
//    	frm.action="/excelDownload.do";
    	$("#frm").submit();
}
function search(){
	doList(1);
}

function openReg(id_bbs_m){
	var frm =document.frm;
	frm.action=contextPath+"/adm/bbs/regBbsMaster.do";
	if(id_bbs_m){
		$("#id_bbs_m").val(id_bbs_m);
	}else{
		$("#id_bbs_m").val('');
	}
	$("#frm").submit();
}

function delBbs(){
	if($("input:checkbox[name=del_code]:checked").length==0){
		alertOpen('삭제할 항목을 선택하십시오.');
	}else{
    	confirmOpen('삭제하시면 게시핀이 삭제됩니다.<br>삭제시 복구가 불가능합니다.주의하십시오.',"deleteConfirm");
	}
}
function deleteConfirm(){
	var frm =document.frm;
	var page = $("#page").val();
	frm.action=contextPath+"/adm/bbs/deleteBbsMaster.do";  
   	frm.page.value=page;
    frm.submit();
}


</script>
</html>