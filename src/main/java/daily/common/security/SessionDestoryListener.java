package daily.common.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import daily.common.service.SystemUserService;
import daily.common.vo.CommandMap;
import kr.co.daily.domain.User;

@Component
public class SessionDestoryListener implements ApplicationListener<SessionDestroyedEvent> {

	@Autowired private SystemUserService systemUserService;
	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		List<SecurityContext> contexts = event.getSecurityContexts();
		if(!contexts.isEmpty()) {
			for(SecurityContext ctx : contexts) {
				User cu = (User) ctx.getAuthentication().getPrincipal();
				String userId = cu.getUsername();
				LoginManager manager = LoginManager.getInstance();
		        if(manager.isUsing(userId)) {
			        manager.removeSession(cu.getUsername());
					CommandMap loginInfo = new CommandMap();
					loginInfo.put("loginedId", userId);
					systemUserService.deleteLoginUserInfo(loginInfo);
		        }
			}
		}
	}

}
