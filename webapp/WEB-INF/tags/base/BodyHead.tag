<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="commc" uri="/WEB-INF/tlds/commc.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="currLocale" required="true" %>
<%@ attribute name="hasHeader" required="false" %>
<c:set var="now" value="<%=new java.util.Date()%>" />

<%--
=========================================================================================
	프로그램명		: 표준 HEAD Page  
	프로그램파일	: BodyHead.tag
	작성자			: P.H
	작성일			: 2019.06.12
	수정자 		    : 
	수정일			: 
=========================================================================================
--%>

        <!-- header -->
        <div class="mh-header">
            <!-- logo -->
            <a href="<c:url value='/'/>" class="mh-logo">
              <h1>
<%--                 <img src="<c:url value='/resource/img/logo.png'/>" alt="NPPA CRS"> --%>
                </h1>
            </a>
            <!-- // logo -->
            <!-- 상단 nav -->
            <nav class="navbar mh-top-nav">
                <div class="top-area-data">
                </div>
                <div class="top-log">
                    <span>${alias_name}</span> 
                    <button type="button" class="logout" onclick="javascript:logout();">로그아웃</button>
                     <span>로그아웃</span>
                </div>
            </nav>
            <!-- // 상단 nav -->
        </div>
        <!-- // header -->
<script>
   function logout(){
	   var form = document.createElement("form");
       form.setAttribute("charset", "UTF-8");
       form.setAttribute("method", "Post");  //Post 방식
       form.setAttribute("action", "<c:url value='/logout.do'/>"); //요청 보낼 주소
       document.body.appendChild(form);
       form.submit();
   }
   function changeLang(){
	   var lang = $("#multi_lang_sel option:selected").val();
	   var url = contextPath+"/usr/changeLanguage.do";
		$.ajax({
			url: url, // 클라이언트가 요청을 보낼 서버의 URL 주소
		    type: "POST", // HTTP 요청 방식(GET, POST)
		    data : {lang:lang},
		    dataType: "json" ,
		    success : function(e){
		    	location.reload();
		    },
		    error : function(e){
		    	alert("fail");
		    }
		});
	   
   }

</script>
