package daily.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daily.common.vo.CommandMap;
import daily.common.vo.CustomMap;

public class AddBatchUtil {
	
	public static Map<Integer,List<Map>> getBatchDataMap(int addBatchCnt,List<CommandMap> inputRealList){
		int ord = 1;
		int ins_ord = 0;
		Map<Integer,List<Map>> addBatchInputMap = new HashMap<>();
		for(CommandMap map:inputRealList) {
			System.out.println("map의 id_meta_row:"+map.getInt("id_meta_row",0)+",id_meta"+map.getInt("id_meta",0));
		}
		for(CommandMap map:inputRealList) {
			if(ord%addBatchCnt==0) {
				ins_ord++;
			}
			List<Map> inInputList = addBatchInputMap.get(ins_ord);
			if(inInputList==null) {
				inInputList = new ArrayList<Map>();
				inInputList.add(map.getMap());
				addBatchInputMap.put(ins_ord, inInputList);
			}else {
				inInputList.add(map.getMap());
			}
			ord++;
		}
		for(int bat_ord : addBatchInputMap.keySet()) {
			List<Map> val = addBatchInputMap.get(bat_ord);
			for(Map map:val) {
				System.out.println("map의 id_meta_row:"+map.get("id_meta_row")+",id_meta"+map.get("id_meta"));
			}
		};
		return addBatchInputMap;
	}
	public static Map<Integer,List<Map>> getCustomListMap(int addBatchCnt,List<CustomMap> inputRealList){
		int ord = 1;
		int ins_ord = 0;
		Map<Integer,List<Map>> addBatchInputMap = new HashMap<>();
		for(CustomMap map:inputRealList) {
			System.out.println("map의 id_meta_row:"+map.getInt("id_meta_row",0)+",id_meta"+map.getInt("id_meta",0));
		}
		for(CustomMap map:inputRealList) {
			if(ord%addBatchCnt==0) {
				ins_ord++;
			}
			List<Map> inInputList = addBatchInputMap.get(ins_ord);
			if(inInputList==null) {
				inInputList = new ArrayList<Map>();
				inInputList.add(map);
				addBatchInputMap.put(ins_ord, inInputList);
			}else {
				inInputList.add(map);
			}
			ord++;
		}
		for(int bat_ord : addBatchInputMap.keySet()) {
			List<Map> val = addBatchInputMap.get(bat_ord);
			for(Map map:val) {
				System.out.println("map의 id_meta_row:"+map.get("id_meta_row")+",id_meta"+map.get("id_meta"));
			}
		};
		return addBatchInputMap;
	}
	public List<Map> getListByBatchDataMap(Map<Integer,List<Map>> dataMap){
		List<Map> ret = new ArrayList<Map>();
		return ret;
	}
}
